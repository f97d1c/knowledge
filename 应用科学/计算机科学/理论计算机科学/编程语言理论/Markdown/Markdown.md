> TODO: 体系结构待优化

<!-- TOC -->

- [数学符号&公式](#数学符号公式)
- [常用语法](#常用语法)
  - [转义字符](#转义字符)
  - [引用](#引用)
    - [单一层级](#单一层级)
    - [多层级](#多层级)
  - [行内标记](#行内标记)
  - [代码块](#代码块)
  - [链接](#链接)
    - [内链式](#内链式)
    - [引用式](#引用式)
  - [表格](#表格)
  - [内嵌css样式](#内嵌css样式)
  - [语义标记/标签](#语义标记标签)
    - [*斜体*](#斜体)
    - [**加粗**](#加粗)
    - [***斜体+粗体***](#斜体粗体)
    - [~~删除线~~](#删除线)
    - [_突出_](#_突出_)
  - [脚注](#脚注)
  - [自动链接](#自动链接)
  - [插入图像](#插入图像)
  - [有序列表](#有序列表)
  - [转义符号](#转义符号)
  - [空&nbsp;格&emsp;符](#空nbsp格emsp符)
  - [注释](#注释)
  - [流程图](#流程图)

<!-- /TOC -->

# 数学符号&公式

符号|代码|描述
-|-|-
$\sqrt{12345}$|\$\sqrt{12345}\$|根号
$\sum$|\$\sum\$|求和公式
$\sum_{i=0}^n$|\$\sum_{i=0}^n\$|求和上下标
$\times$|\$\times\$|乘号
$\pm$|\$\pm\$|正负号
$\div$|\$\div\$|除号
$\mid$|\$\mid\$|竖线
$\cdot$|\$\cdot\$|点
$\circ$|\$\circ\$|圈
$\ast	$|\$\ast	\$|星号
$\bigotimes$|\$\bigotimes\$|克罗内克积
$\bigoplus$|\$\bigoplus\$|异或
$\leq$|\$\leq\$|小于等于
$\geq$|\$\geq\$|大于等于
$\neq$|\$\neq\$|不等于
$\approx$|\$\approx\$|约等于
$\prod$|\$\prod\$|N元乘积
$\coprod$|\$\coprod\$|N元余积
$\cdots$|\$\cdots\$|省略号
$\int$|\$\int\$|积分
$\iint$|\$\iint\$|双重积分
$\oint$|\$\oint\$|曲线积分
$\infty$|\$\infty\$|无穷
$\nabla$|\$\nabla\$|梯度
$\because$|\$\because\$|因为
$\therefore$|\$\therefore\$|所以
$\forall$|\$\forall\$|任意
$\exists$|\$\exists\$|存在
$\neq$|\$\neq\$|不等于
$\leq$|\$\leq\$|小于等于
$\geq$|\$\geq\$|大于等于
$\emptyset$|\$\emptyset\$|空集
$\in$|\$\in\$|属于
$\notin$|\$\notin\$|不属于
$\subset$|\$\subset\$|子集
$\subseteq$|\$\subseteq\$|真子集
$\bigcup$|\$\bigcup\$|并集
$\bigcap$|\$\bigcap\$|交集
$\bigvee$|\$\bigvee\$|逻辑或
$\bigwedge$|\$\bigwedge\$|逻辑与
$\biguplus$|\$\biguplus\$|多重集
$\bigsqcup$|\$\bigsqcup\$|
$\hat{y}$|\$\hat{y}\$|期望值
$\check{y}$|\$\check{y}\$|
$\breve{y}$|\$\breve{y}\$|
$\overline{a+b+c+d}$|\$\overline{a+b+c+d}\$|平均值
$\underline{a+b+c+d}$|\$\underline{a+b+c+d}\$|
$\uparrow$|\$\uparrow\$|向上
$\downarrow$|\$\downarrow\$|向下
$\Uparrow$|\$\Uparrow\$|
$\Downarrow$|\$\Downarrow\$|
$\rightarrow$|\$\rightarrow\$|向右
$\leftarrow$|\$\leftarrow\$|向左
$\Rightarrow$|\$\Rightarrow\$|向右箭头
$\Longleftarrow$|\$\Longleftarrow\$|向左长箭头
$\longleftarrow$|\$\longleftarrow\$|向左单箭头
$\longrightarrow$|\$\longrightarrow\$|向右长箭头
$\Longrightarrow$|\$\Longrightarrow\$|向右箭头
$\alpha$|\$\alpha\$|
$\beta$|\$\beta\$|
$\gamma$|\$\gamma\$|
$\Gamma$|\$\Gamma\$|
$\delta$|\$\delta\$|
$\Delta$|\$\Delta\$|
$\epsilon$|\$\epsilon\$|
$\varepsilon$|\$\varepsilon\$|
$\zeta$|\$\zeta\$|
$\eta$|\$\eta\$|
$\theta$|\$\theta\$|
$\Theta$|\$\Theta\$|
$\vartheta$|\$\vartheta\$|
$\iota$|\$\iota\$|
$\pi$|\$\pi\$|
$\phi$|\$\phi\$|
$\Phi$|\$\Phi\$|
$\psi$|\$\psi\$|
$\Psi$|\$\Psi\$|
$\omega$|\$\omega\$|
$\Omega$|\$\Omega\$|
$\rho$|\$\rho\$|
$\omicron$|\$\omicron\$|
$\sigma$|\$\sigma\$|
$\Sigma$|\$\Sigma\$|
$\nu$|\$\nu\$|
$\xi$|\$\xi\$|
$\tau$|\$\tau\$|
$\lambda$|\$\lambda\$|
$\Lambda$|\$\Lambda\$|
$\partial$|\$\partial\$|
$\lbrace \rbrace$|\$\lbrace \rbrace\$|
$\overline{a}$|\$\overline{a}\$|


# 常用语法
## 转义字符
由于markdown支持html语法,当需要html标签原样展示时,可以使用一下转义字符.
显示结果|描述|实体名称|实体编号
-|-|-|-
&nbsp; |空格|\&nbsp;|\&#160;
<|小于号|\&lt;|\&#60;
\>|大于号|\&gt;|\&#62;
&|与号|\&amp;|\&#38;
"|号|\&quot;|\&#34;
‘|撇号|\&apos;|\&#39;

## 引用
### 单一层级
回车表示引用结束
```markdown
> hello world<br>
end

end
```
> hello world<br>
end

end

### 多层级

```markdown
> 外层
>> 里层
>>> 最内层
```

> 外层
>> 里层
>>> 最内层

## 行内标记

```markdown
`标记内` 标记外
```
`标记内` 标记外

## 代码块
` ```语言名称 ` 可根据不同的语法进行着色<br>

```html
<body>
</body>
```

## 链接

### 内链式

```markdown
[谷歌](www.google.com '碾压百度')
```
[谷歌](www.google.com '碾压百度')


### 引用式
```markdown
[baidu]: http://www.baidu.com '百度一下'
[百度][baidu]
```

[baidu]: http://www.baidu.com '百度一下'
[百度][baidu]

## 表格
**:** 代表对齐方式

```markdown
居中|左对齐|右对齐
:-:|:-|-:
居中|左对齐|右对齐|
===============|===============|===============

```
居中|左对齐|右对齐
:-:|:-|-:
居中|左对齐|右对齐|
===============|===============|===============

## 内嵌css样式
```markdown
<p style="color: #AD5D0F;font-size: 18px; font-family: '宋体';">
内嵌样式
</p>
```
<p style="color: #AD5D0F;font-size: 18px; font-family: '宋体';">
内嵌样式
</p>

## 语义标记/标签
### *斜体*

```markdown
*斜体*
```

### **加粗**
```markdown
**加粗**
```
### ***斜体+粗体***

```markdown
***斜体+粗体***
```
### ~~删除线~~

```markdown
 ~~删除线~~
```
### _突出_

```markdown
_突出_
```

## 脚注

```markdown
我<sup>[1]</sup>

[1]: 这里指我自己
```
我<sup>[1]</sup>

[1]: 这里指我自己

## 自动链接

```markdown
<http://google.com>

<ff4c00@gmail.com>
```

<http://google.com>

<ff4c00@gmail.com>

## 插入图像
和创建超链接的方式类似,区别在于感叹号`!`
```markdown
![who am I][who]

[who]:https://raw.githubusercontent.com/ff4c00/markdown/master/images/life/849156087.jpg "who am I"
```
![who am I][who]

[who]:https://raw.githubusercontent.com/ff4c00/markdown/master/images/life/849156087.jpg "who am I"


> [知呼 | Markdown中插入图片有什么技巧?](https://www.zhihu.com/question/21065229)
## 有序列表
这里之所以都选0,是因为如果顺序需要调整,代码不需要任何改动.

```markdown
0. 首先
0. 其次
0. 然后
0. 最后
```

0. 首先
0. 其次
0. 然后
0. 最后

## 转义符号

```markdown
\*斜体\*
```

\*斜体\*

## 空&nbsp;格&emsp;符

```markdown
空&nbsp;格&emsp;符
```

## 注释

```markdown
<!-- html注释语法 -->
```
## 流程图

语法|说明
-|-
graph TB|top->botom 上->下
graph BT|botom->top 下->上
graph LR|left->right 左->右
graph RL|right->left 右->左
-->|在流程图中显示——>
---|在流程图中显示——

```mermaid
graph LR;
　　A-->|A指向B|B;
　　B---|B与C相连|C;
```

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

```mermaid
graph LR;
　　Portal-->|发布/更新配置|Apollo配置中心;
　　Apollo配置中心-->|实时推送|App;
　　App-->|实时查询|Apollo配置中心;
```

```mermaid
graph TB
　　client-->|2 findConfigServices|LoadBalancer;
　　LoadBalancer-->|3 findService|metaServer;
　　metaServer-->Eureka;
　　client-->|4 access via ip:port/client load balance/error retry|ConfigService;
　　ConfigService-->|1 register/cancel|Eureka;
　　ConfigService-->|read/write|ConfigDB;
```

```mermaid
graph LR;
　　client---core;
　　client---common;
　　core---common;
　　common---portal;
　　common---Biz;
　　Biz---ConfigService;
　　Biz---AdminService;
```

<hr>

> [简书 | 欧薇娅](https://www.jianshu.com/p/b03a8d7b1719)

> [为Markdown文件自动生成目录](https://www.jianshu.com/p/4721ddd27027)

> [CSDN | 半美人的博客 | VSCode编辑流程图、时序图等的环境配置](https://blog.csdn.net/m0_37639589/article/details/77684333)

> [Github | mermaid文档](https://mermaidjs.github.io/)

> [博客园 | Markdown中使用mermaid画流程图](https://www.cnblogs.com/nanqiang/p/8244309.html)

> [CSDN | Markdown数学符号&公式](https://blog.csdn.net/Katherine_hsr/article/details/79179622)