
<!-- TOC -->

- [说明](#说明)
  - [Scrapy和Django](#scrapy和django)
  - [蜘蛛(Spiders)](#蜘蛛spiders)
  - [选择器](#选择器)
    - [CSS与Xpath](#css与xpath)
  - [项目和管道](#项目和管道)
- [Scrapy爬取豆瓣电影Top250榜单](#scrapy爬取豆瓣电影top250榜单)
  - [scrapy.cfg](#scrapycfg)
  - [douban/](#douban)
  - [douban/items.py](#doubanitemspy)
  - [douban/pipelines.py](#doubanpipelinespy)
  - [douban/settings.py](#doubansettingspy)
  - [douban/spiders/](#doubanspiders)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## Scrapy和Django

Scrapy允许定义数据结构、编写数据提取器,并带有可用于提取数据的内置CSS和xpath选择器、scrapyshell以及内置的JSON、CSV和XML输出.<br>
还有一个内置的FormRequest类,它允许模拟登录并且易于开箱即用.

网站往往有防止过多请求的对策,因此Scrapy默认随机化每个请求之间的时间,这有助于避免被禁止.<br>
Scrapy也可用于自动化测试和监控.

Django有一个集成的管理员,可以轻松访问数据库.<br>
再加上易于过滤和排序数据以及导入/导出库以允许导出数据.

![](https://blog.theodo.com/static/26754251ba3cae7afc8bbf94a2053b95/50383/Screenshot-2019-01-15-at-09.27.43.png)

## 蜘蛛(Spiders)

蜘蛛是Scrapy的核心.<br>
它向定义的URL发出请求,解析响应,并从中提取信息以在项目中进行处理.

Scrapy有一个start_requests方法,它生成一个带有URL的请求.<br>
当Scrapy根据请求抓取网站时,它会将响应解析到请求对象中指定的回调方法.<br>
回调方法可以从响应数据生成一个项目或生成另一个请求.

幕后发生了什么?每次启动一个Scrapy任务时,都会启动一个爬虫来完成它.<br>
Spider定义了如何执行爬行(即跟随链接).<br>
履带车有一个引擎来驱动它的流动.<br>
当一个爬虫启动时,它会从它的队列中获取蜘蛛,这意味着爬虫可以有一个以上的蜘蛛.<br>
然后下一个蜘蛛会被爬虫启动,并被引擎安排爬取网页.<br>
引擎中间件驱动爬虫的流程.<br>
中间件被组织成链来处理请求和响应.


## 选择器

选择器可用于解析网页以生成项目.<br>
它们选择由xpath或css表达式指定的html文档部分.<br>
Xpath在XML文档(也可以在HTML文档中使用)中选择节点,而CSS是一种将样式应用于HTML文档的语言.<br>
CSS选择器使用HTML类和id标签名称来选择标签内的数据.<br>
Scrapy在后台使用cssselect库将这些CSS选择器转换为xpath选择器.

### CSS与Xpath

```py
data = response.css("div.st-about-employee-pop-up")
data = response.xpath("//div[@class='team-popup-wrap st-about-employee-pop-up']")
```

在处理类、ID和标签名称时,使用CSS选择器.<br>
如果没有类名而只知道标签的内容,请使用xpath选择器.<br>
无论哪种方式,chrome开发工具都可以提供帮助:复制元素唯一css选择器的选择器,或者可以复制其xpath选择器.<br>
这是给一个基础,可能要调整它!另外两个辅助工具是[XPath helper](https://chrome.google.com/webstore/detail/xpath-helper/hgimnogjllphhhkhlmebbmlgjoejdpjl)和[这个](https://devhints.io/xpath)备忘单.


## 项目和管道

项目产生输出.<br>
它们用于构建蜘蛛解析的数据.<br>
项目管道是从蜘蛛中提取项目后处理数据的地方.<br>
在这里,可以运行诸如验证和在数据库中存储项目之类的任务.

# Scrapy爬取豆瓣电影Top250榜单

创建项目:

```sh
scrapy startproject douban
```


## scrapy.cfg

项目的配置文件.

## douban/

该项目的python模块.<br>
之后将在此加入代码.

## douban/items.py

项目中的item文件.<br>
定义所要爬取的信息的相关属性.

## douban/pipelines.py

项目中的pipelines文件.<br>
当数据被爬虫爬取下来后,它会被发送到itempipelines中,<br>
每个itempipelines组件(有时称为 *项目管道*)是一个实现简单方法的Python类.<br>
收到一个项目并对其执行操作,还决定该项目是否应该继续通过管道或被丢弃并且不再被处理.

## douban/settings.py

项目的设置文件.

## douban/spiders/

放置spider代码的目录.

# 参考资料

> [theodo | ScrapingwithScrapyandDjangoIntegration](https://blog.theodo.com/2019/01/data-scraping-scrapy-django-integration/)