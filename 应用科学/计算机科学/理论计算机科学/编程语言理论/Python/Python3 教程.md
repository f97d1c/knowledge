
<!-- TOC -->

- [基础内容](#基础内容)
  - [简介](#简介)
    - [Python发展历史](#python发展历史)
    - [Python特点](#python特点)
    - [Python应用](#python应用)
  - [环境搭建](#环境搭建)
    - [Python安装](#python安装)
      - [Unix & Linux](#unix--linux)
      - [MAC](#mac)
    - [pip](#pip)
      - [下载安装包](#下载安装包)
      - [移除软件包](#移除软件包)
      - [查看已经安装的软件包](#查看已经安装的软件包)
    - [环境变量配置](#环境变量配置)
      - [Unix/Linux](#unixlinux)
    - [Python环境变量](#python环境变量)
    - [运行Python](#运行python)
      - [交互式解释器](#交互式解释器)
      - [命令行脚本](#命令行脚本)
      - [集成开发环境](#集成开发环境)
        - [VSCode](#vscode)
  - [基础语法](#基础语法)
    - [编码](#编码)
    - [标识符](#标识符)
    - [保留字](#保留字)
    - [注释](#注释)
    - [行与缩进](#行与缩进)
    - [多行语句](#多行语句)
    - [空行](#空行)
    - [同一行显示多条语句](#同一行显示多条语句)
    - [多个语句构成代码组](#多个语句构成代码组)
    - [print输出](#print输出)
    - [del语句](#del语句)
    - [end关键字](#end关键字)
  - [基本数据类型](#基本数据类型)
    - [数据类型转换](#数据类型转换)
    - [多个变量赋值](#多个变量赋值)
    - [标准数据类型](#标准数据类型)
    - [数字(Number)类型](#数字number类型)
      - [isinstance和type的区别](#isinstance和type的区别)
      - [数值运算](#数值运算)
      - [数字类型转换](#数字类型转换)
      - [数学函数](#数学函数)
      - [随机数函数](#随机数函数)
      - [三角函数](#三角函数)
      - [数学常量](#数学常量)
    - [字符串(String)](#字符串string)
      - [字符串的截取](#字符串的截取)
      - [转义字符](#转义字符)
      - [格式化](#格式化)
      - [三引号](#三引号)
      - [f-string](#f-string)
      - [Unicode字符串](#unicode字符串)
      - [内建函数](#内建函数)
        - [字符串长度](#字符串长度)
        - [字符替换](#字符替换)
        - [首字母大写](#首字母大写)
        - [大小写互换](#大小写互换)
        - [转大写](#转大写)
        - [补齐](#补齐)
        - [词频](#词频)
        - [解码](#解码)
        - [编码](#编码-1)
        - [结尾符检查](#结尾符检查)
        - [tab转空格](#tab转空格)
        - [查找子字符串位置(find)](#查找子字符串位置find)
        - [查找子字符串位置(index)](#查找子字符串位置index)
        - [同一构成元素](#同一构成元素)
        - [是否存在非数字元素](#是否存在非数字元素)
        - [是否只包含数字](#是否只包含数字)
        - [是否均为小写](#是否均为小写)
        - [是否只包含数字](#是否只包含数字-1)
        - [是否为空](#是否为空)
        - [标题化](#标题化)
        - [是否标题化](#是否标题化)
        - [是否均为大写](#是否均为大写)
        - [分隔符拼接](#分隔符拼接)
        - [左对齐](#左对齐)
        - [大写转小写](#大写转小写)
        - [截取字符串](#截取字符串)
        - [字符映射的转换表](#字符映射的转换表)
        - [最大字母](#最大字母)
        - [最小字母](#最小字母)
        - [右find](#右find)
        - [右index](#右index)
        - [右对齐](#右对齐)
        - [删除末尾匹配字符](#删除末尾匹配字符)
        - [截取字符串](#截取字符串-1)
        - [换行](#换行)
        - [匹配开头](#匹配开头)
        - [strip](#strip)
        - [translate](#translate)
        - [截取指定长度](#截取指定长度)
        - [十进制检测](#十进制检测)
    - [列表/序列(List)](#列表序列list)
      - [函数&方法](#函数方法)
        - [下标默认值](#下标默认值)
        - [遍历(enumerate)](#遍历enumerate)
        - [多序列遍历(zip)](#多序列遍历zip)
        - [反向遍历(reversed)](#反向遍历reversed)
        - [删除列表元素](#删除列表元素)
        - [列表元素个数](#列表元素个数)
        - [返回列表元素最大值](#返回列表元素最大值)
        - [返回列表元素最小值](#返回列表元素最小值)
        - [将元组转换为列表](#将元组转换为列表)
        - [在列表末尾添加新的对象](#在列表末尾添加新的对象)
        - [统计某个元素在列表中出现的次数](#统计某个元素在列表中出现的次数)
        - [在列表末尾一次性追加另一个序列中的多个值(用新列表扩展原来的列表)](#在列表末尾一次性追加另一个序列中的多个值用新列表扩展原来的列表)
        - [从列表中找出某个值第一个匹配项的索引位置](#从列表中找出某个值第一个匹配项的索引位置)
        - [将对象插入列表](#将对象插入列表)
        - [移除列表中的一个元素(默认最后一个元素),并且返回该元素的值](#移除列表中的一个元素默认最后一个元素并且返回该元素的值)
        - [移除列表中某个值的第一个匹配项](#移除列表中某个值的第一个匹配项)
        - [反向列表中元素](#反向列表中元素)
        - [对原列表进行排序](#对原列表进行排序)
        - [清空列表](#清空列表)
        - [复制列表](#复制列表)
    - [Tuple(元组)](#tuple元组)
      - [0个或1个元素的元组](#0个或1个元素的元组)
      - [修改元组](#修改元组)
      - [内置函数](#内置函数)
        - [计算元组元素个数](#计算元组元素个数)
        - [返回元组中元素最大值](#返回元组中元素最大值)
        - [返回元组中元素最小值](#返回元组中元素最小值)
        - [将可迭代系列转换为元组](#将可迭代系列转换为元组)
      - [关于元组是不可变的](#关于元组是不可变的)
    - [Set(集合)](#set集合)
      - [函数&方法](#函数方法-1)
        - [添加元素](#添加元素)
        - [移除元素](#移除元素)
        - [计算集合元素个数](#计算集合元素个数)
        - [清空集合](#清空集合)
        - [判断元素是否在集合中存在](#判断元素是否在集合中存在)
        - [拷贝一个集合](#拷贝一个集合)
        - [返回多个集合的差集](#返回多个集合的差集)
        - [移除集合中的元素,该元素在指定的集合也存在](#移除集合中的元素该元素在指定的集合也存在)
        - [删除集合中指定的元素](#删除集合中指定的元素)
        - [返回集合的交集](#返回集合的交集)
        - [返回集合的交集](#返回集合的交集-1)
        - [判断两个集合是否包含相同的元素,如果没有返回True,否则返回False](#判断两个集合是否包含相同的元素如果没有返回true否则返回false)
        - [判断指定集合是否为该方法参数集合的子集](#判断指定集合是否为该方法参数集合的子集)
        - [判断该方法的参数集合是否为指定集合的子集](#判断该方法的参数集合是否为指定集合的子集)
        - [随机移除元素](#随机移除元素)
        - [移除指定元素](#移除指定元素)
        - [返回两个集合中不重复的元素集合](#返回两个集合中不重复的元素集合)
        - [移除当前集合中在另外一个指定集合相同的元素,并将另外一个指定集合中不同的元素插入到当前集合中](#移除当前集合中在另外一个指定集合相同的元素并将另外一个指定集合中不同的元素插入到当前集合中)
        - [返回两个集合的并集](#返回两个集合的并集)
        - [给集合添加元素](#给集合添加元素)
    - [Dictionary(字典)](#dictionary字典)
      - [函数&方法](#函数方法-2)
        - [转列表(遍历)](#转列表遍历)
        - [键值对序列转建字典(dict())](#键值对序列转建字典dict)
        - [元素个数](#元素个数)
        - [输出字典](#输出字典)
        - [删除字典内所有元素](#删除字典内所有元素)
        - [浅复制](#浅复制)
        - [格式化键](#格式化键)
        - [获取键值](#获取键值)
        - [是否存在键](#是否存在键)
        - [视图对象](#视图对象)
        - [setdefault](#setdefault)
        - [update](#update)
        - [values](#values)
        - [pop](#pop)
        - [popitem](#popitem)
  - [解释器](#解释器)
    - [交互式编程](#交互式编程)
  - [运算符](#运算符)
    - [逻辑运算符](#逻辑运算符)
    - [成员运算符](#成员运算符)
    - [身份运算符](#身份运算符)
      - [is与 == 区别](#is与--区别)
    - [运算符优先级](#运算符优先级)
  - [条件控制](#条件控制)
    - [if语句](#if语句)
  - [循环语句](#循环语句)
    - [while循环](#while循环)
      - [无限循环](#无限循环)
      - [while循环使用else语句](#while循环使用else语句)
      - [简单语句组](#简单语句组)
    - [for语句](#for语句)
      - [range()函数](#range函数)
      - [break和continue](#break和continue)
      - [else语句](#else语句)
      - [pass语句](#pass语句)
  - [迭代器与生成器](#迭代器与生成器)
    - [迭代器](#迭代器)
    - [StopIteration](#stopiteration)
    - [生成器](#生成器)
  - [函数](#函数)
    - [定义一个函数](#定义一个函数)
    - [参数传递](#参数传递)
      - [可更改(mutable)与不可更改(immutable)对象](#可更改mutable与不可更改immutable对象)
    - [return语句](#return语句)
    - [参数](#参数)
      - [必需参数](#必需参数)
      - [关键字参数](#关键字参数)
      - [默认参数](#默认参数)
      - [不定长参数](#不定长参数)
      - [匿名函数](#匿名函数)
        - [强制位置参数](#强制位置参数)
  - [模块](#模块)
    - [import与from...import](#import与fromimport)
    - [深入模块](#深入模块)
    - [__name__属性](#__name__属性)
    - [dir() 函数](#dir-函数)
    - [标准模块](#标准模块)
    - [包](#包)
    - [从一个包中导入*](#从一个包中导入)
  - [输入和输出](#输入和输出)
    - [输出格式美化](#输出格式美化)
    - [旧式字符串格式化](#旧式字符串格式化)
    - [读取键盘输入](#读取键盘输入)
  - [File](#file)
    - [读和写文件](#读和写文件)
    - [文件打开类型](#文件打开类型)
    - [文件对象的方法](#文件对象的方法)
      - [读取文件内容(read())](#读取文件内容read)
      - [读取单独的一行(readline())](#读取单独的一行readline)
      - [读取所有行(readlines())](#读取所有行readlines)
      - [写入文件(write())](#写入文件write)
      - [当前位置(tell())](#当前位置tell)
    - [改变当前的位置(seek())](#改变当前的位置seek)
    - [释放资源(close())](#释放资源close)
    - [pickle模块](#pickle模块)
      - [刷新文件内部缓冲(flush())](#刷新文件内部缓冲flush)
      - [获取文件描述符(fileno())](#获取文件描述符fileno)
      - [是否使用中(isatty())](#是否使用中isatty)
      - [字符截取(truncate([size]))](#字符截取truncatesize)
      - [写入序列字符串(writelines(sequence))](#写入序列字符串writelinessequence)
  - [文件/目录方法(OS)](#文件目录方法os)
    - [检验权限模式](#检验权限模式)
    - [改变当前工作目录](#改变当前工作目录)
    - [设置路径的标记为数字标记](#设置路径的标记为数字标记)
    - [更改权限](#更改权限)
    - [更改文件所有者](#更改文件所有者)
    - [改变当前进程的根目录](#改变当前进程的根目录)
    - [关闭文件描述符fd](#关闭文件描述符fd)
    - [关闭所有文件描述符](#关闭所有文件描述符)
    - [复制文件描述符fd](#复制文件描述符fd)
    - [将一个文件描述符fd复制到另一个fd2](#将一个文件描述符fd复制到另一个fd2)
    - [通过文件描述符改变当前工作目录](#通过文件描述符改变当前工作目录)
    - [改变一个文件的访问权限](#改变一个文件的访问权限)
    - [修改一个文件的所有权](#修改一个文件的所有权)
    - [强制将文件写入磁盘](#强制将文件写入磁盘)
    - [通过文件描述符fd创建一个文件对象](#通过文件描述符fd创建一个文件对象)
    - [返回一个打开的文件的系统配置信息](#返回一个打开的文件的系统配置信息)
    - [返回文件描述符fd的状态](#返回文件描述符fd的状态)
    - [返回包含文件描述符fd的文件的文件系统的信息](#返回包含文件描述符fd的文件的文件系统的信息)
    - [强制将文件描述符为fd的文件写入硬盘](#强制将文件描述符为fd的文件写入硬盘)
    - [裁剪文件描述符fd对应的文件](#裁剪文件描述符fd对应的文件)
    - [返回当前工作目录](#返回当前工作目录)
    - [返回一个当前工作目录的Unicode对象](#返回一个当前工作目录的unicode对象)
    - [是否与设备连接](#是否与设备连接)
    - [设置路径的标记为数字标记](#设置路径的标记为数字标记-1)
    - [修改连接文件权限](#修改连接文件权限)
    - [更改文件所有者](#更改文件所有者-1)
    - [创建硬链接](#创建硬链接)
    - [名称列表](#名称列表)
    - [设置文件描述符](#设置文件描述符)
    - [lstat](#lstat)
    - [从原始的设备号中提取设备major号码](#从原始的设备号中提取设备major号码)
    - [以major和minor设备号组成一个原始设备号](#以major和minor设备号组成一个原始设备号)
    - [递归文件夹创建函数](#递归文件夹创建函数)
    - [从原始的设备号中提取设备minor号码](#从原始的设备号中提取设备minor号码)
    - [以数字mode的mode创建一个名为path的文件夹](#以数字mode的mode创建一个名为path的文件夹)
    - [创建命名管道](#创建命名管道)
    - [打开一个文件](#打开一个文件)
    - [打开一个新的伪终端对](#打开一个新的伪终端对)
    - [返回相关文件的系统配置信息](#返回相关文件的系统配置信息)
    - [创建一个管道](#创建一个管道)
    - [从一个command打开一个管道](#从一个command打开一个管道)
    - [读取文件内容](#读取文件内容)
    - [返回软链接所指向的文件](#返回软链接所指向的文件)
    - [删除文件](#删除文件)
    - [递归删除目录](#递归删除目录)
    - [重命名文件或目录](#重命名文件或目录)
    - [递归地对目录进行更名](#递归地对目录进行更名)
    - [删除path指定的空目录](#删除path指定的空目录)
    - [获取path指定的路径的信息](#获取path指定的路径的信息)
    - [获取指定路径的文件系统统计信息](#获取指定路径的文件系统统计信息)
    - [创建一个软链接](#创建一个软链接)
    - [tcgetpgrp](#tcgetpgrp)
    - [tcsetpgrp](#tcsetpgrp)
    - [ttyname](#ttyname)
    - [删除文件路径](#删除文件路径)
    - [返回指定的path文件的访问和修改的时间](#返回指定的path文件的访问和修改的时间)
    - [walk](#walk)
    - [写入内容](#写入内容)
    - [获取文件的属性信息](#获取文件的属性信息)
    - [获取当前目录的父目录](#获取当前目录的父目录)
  - [错误和异常](#错误和异常)
    - [try/except/else/finally](#tryexceptelsefinally)
    - [抛出异常](#抛出异常)
    - [自定义异常](#自定义异常)
    - [预定义的清理行为](#预定义的清理行为)
  - [面向对象](#面向对象)
    - [类定义](#类定义)
    - [类对象](#类对象)
    - [构造方法](#构造方法)
    - [self](#self)
    - [继承](#继承)
    - [多继承](#多继承)
    - [方法重写](#方法重写)
    - [类属性与方法](#类属性与方法)
      - [类的私有属性(__private_attrs)](#类的私有属性__private_attrs)
      - [类的方法](#类的方法)
      - [类的私有方法(__private_method)](#类的私有方法__private_method)
      - [类的专有方法](#类的专有方法)
      - [运算符重载](#运算符重载)
  - [命名空间/作用域](#命名空间作用域)
    - [命名空间](#命名空间)
      - [三种命名空间](#三种命名空间)
      - [查找顺序](#查找顺序)
      - [生命周期](#生命周期)
    - [作用域](#作用域)
      - [四种作用域](#四种作用域)
    - [全局变量和局部变量](#全局变量和局部变量)
    - [global和nonlocal关键字](#global和nonlocal关键字)
  - [标准库概览](#标准库概览)
    - [操作系统接口(os)](#操作系统接口os)
    - [文件通配符(glob)](#文件通配符glob)
    - [sys](#sys)
      - [命令行参数](#命令行参数)
      - [错误输出重定向](#错误输出重定向)
      - [程序终止](#程序终止)
    - [字符串正则匹配(re)](#字符串正则匹配re)
    - [数学(math)](#数学math)
      - [随机数(random)](#随机数random)
    - [访问互联网](#访问互联网)
    - [Url处理(urllib)](#url处理urllib)
    - [电子邮件处理(smtplib)](#电子邮件处理smtplib)
    - [日期和时间(datetime)](#日期和时间datetime)
    - [数据压缩](#数据压缩)
    - [性能度量](#性能度量)
    - [测试模块](#测试模块)
  - [实例](#实例)
    - [HelloWorld实例](#helloworld实例)
    - [数字求和](#数字求和)
    - [平方根](#平方根)
    - [二次方程](#二次方程)
    - [计算三角形的面积](#计算三角形的面积)
    - [计算圆的面积](#计算圆的面积)
    - [随机数生成](#随机数生成)
    - [摄氏温度转华氏温度](#摄氏温度转华氏温度)
    - [交换变量](#交换变量)
    - [if语句](#if语句-1)
    - [判断字符串是否为数字](#判断字符串是否为数字)
    - [判断奇数偶数](#判断奇数偶数)
    - [判断闰年](#判断闰年)
    - [获取最大值函数](#获取最大值函数)
    - [质数判断](#质数判断)
    - [输出指定范围内的素数](#输出指定范围内的素数)
    - [阶乘实例](#阶乘实例)
    - [九九乘法表](#九九乘法表)
    - [斐波那契数列](#斐波那契数列)
    - [阿姆斯特朗数](#阿姆斯特朗数)
    - [十进制转二进制、八进制、十六进制](#十进制转二进制八进制十六进制)
    - [ASCII码与字符相互转换](#ascii码与字符相互转换)
    - [最大公约数算法](#最大公约数算法)
    - [最小公倍数算法](#最小公倍数算法)
    - [简单计算器实现](#简单计算器实现)
    - [生成日历](#生成日历)
    - [使用递归斐波那契数列](#使用递归斐波那契数列)
    - [文件IO](#文件io)
    - [字符串判断](#字符串判断)
    - [字符串大小写转换](#字符串大小写转换)
    - [计算每个月天数](#计算每个月天数)
    - [获取昨天日期](#获取昨天日期)
    - [list常用操作](#list常用操作)
    - [约瑟夫生者死者小游戏](#约瑟夫生者死者小游戏)
    - [五人分鱼](#五人分鱼)
    - [实现秒表功能](#实现秒表功能)
    - [计算n个自然数的立方和](#计算n个自然数的立方和)
    - [计算数组元素之和](#计算数组元素之和)
    - [数组翻转指定个数的元素](#数组翻转指定个数的元素)
    - [将列表中的头尾两个元素对调](#将列表中的头尾两个元素对调)
    - [将列表中的指定位置的两个元素对调](#将列表中的指定位置的两个元素对调)
    - [翻转列表](#翻转列表)
    - [判断元素是否在列表中存在](#判断元素是否在列表中存在)
    - [清空列表](#清空列表-1)
    - [复制列表</a>](#复制列表a)
    - [计算元素在列表中出现的次数](#计算元素在列表中出现的次数)
    - [计算列表元素之和](#计算列表元素之和)
    - [计算列表元素之积](#计算列表元素之积)
    - [查找列表中最小元素](#查找列表中最小元素)
    - [查找列表中最大元素](#查找列表中最大元素)
    - [移除字符串中的指定位置字符](#移除字符串中的指定位置字符)
    - [判断字符串是否存在子字符串](#判断字符串是否存在子字符串)
    - [判断字符串长度](#判断字符串长度)
    - [使用正则表达式提取字符串中的URL](#使用正则表达式提取字符串中的url)
    - [将字符串作为代码执行](#将字符串作为代码执行)
    - [字符串翻转](#字符串翻转)
    - [对字符串切片及翻转](#对字符串切片及翻转)
    - [按键(key)或值(value)对字典进行排序](#按键key或值value对字典进行排序)
    - [计算字典值之和](#计算字典值之和)
    - [移除字典点键值(key/value)对](#移除字典点键值keyvalue对)
    - [合并字典](#合并字典)
    - [将字符串的时间转换为时间戳](#将字符串的时间转换为时间戳)
    - [获取几天前的时间](#获取几天前的时间)
    - [将时间戳转换为指定格式日期](#将时间戳转换为指定格式日期)
    - [打印自己设计的字体](#打印自己设计的字体)
    - [二分查找](#二分查找)
    - [线性查找](#线性查找)
    - [插入排序](#插入排序)
    - [快速排序](#快速排序)
    - [选择排序](#选择排序)
    - [冒泡排序](#冒泡排序)
    - [归并排序](#归并排序)
    - [堆排序](#堆排序)
    - [计数排序](#计数排序)
    - [希尔排序](#希尔排序)
    - [拓扑排序](#拓扑排序)
- [高级教程](#高级教程)
  - [正则表达式](#正则表达式)
    - [正则表达式修饰符-可选标志(flags)](#正则表达式修饰符-可选标志flags)
    - [match函数](#match函数)
    - [search方法](#search方法)
    - [match与search的区别](#match与search的区别)
    - [检索和替换](#检索和替换)
    - [repl参数是一个函数](#repl参数是一个函数)
    - [compile函数](#compile函数)
    - [findall](#findall)
    - [finditer](#finditer)
    - [split](#split)
  - [CGI编程](#cgi编程)
  - [MySQL(mysql-connector)](#mysqlmysql-connector)
    - [创建数据库连接](#创建数据库连接)
    - [执行sql语句(execute)](#执行sql语句execute)
    - [插入数据](#插入数据)
    - [批量插入](#批量插入)
    - [查询数据](#查询数据)
  - [MySQL(PyMySQL)](#mysqlpymysql)
  - [网络编程](#网络编程)
  - [SMTP发送邮件](#smtp发送邮件)
  - [多线程](#多线程)
    - [使用线程方式](#使用线程方式)
      - [函数式](#函数式)
      - [线程模块](#线程模块)
        - [使用threading模块创建线程](#使用threading模块创建线程)
        - [线程同步](#线程同步)
    - [线程优先级队列(Queue)](#线程优先级队列queue)
  - [XML解析](#xml解析)
  - [JSON](#json)
    - [类型转换对应表](#类型转换对应表)
      - [Python编码为JSON](#python编码为json)
      - [JSON解码为Python](#json解码为python)
  - [日期和时间](#日期和时间)
    - [时间元组(struct_time)](#时间元组struct_time)
    - [获取当前时间](#获取当前时间)
    - [获取格式化的时间](#获取格式化的时间)
    - [格式化日期](#格式化日期)
    - [获取某月日历(Calendar)](#获取某月日历calendar)
    - [Time模块](#time模块)
      - [格林威治偏移秒数(altzone)](#格林威治偏移秒数altzone)
      - [格式化字符串(asctime([tupletime]))](#格式化字符串asctimetupletime)
      - [CPU时间(clock())](#cpu时间clock)
      - [ctime([secs])](#ctimesecs)
      - [gmtime([secs])](#gmtimesecs)
      - [localtime([secs])](#localtimesecs)
      - [mktime(tupletime)](#mktimetupletime)
      - [sleep(secs)](#sleepsecs)
      - [格式化本地时间(strftime(fmt[,tupletime]))](#格式化本地时间strftimefmttupletime)
      - [strptime](#strptime)
      - [当前时间戳(time())](#当前时间戳time)
      - [重新初始化时间设置(tzset())](#重新初始化时间设置tzset)
      - [系统运行时间(perf_counter())](#系统运行时间perf_counter)
      - [进程执行CPU的时间总和(process_time())](#进程执行cpu的时间总和process_time)
    - [日历(Calendar)模块](#日历calendar模块)
      - [calendar.calendar(year,w=2,l=1,c=6)](#calendarcalendaryearw2l1c6)
      - [calendar.firstweekday( )](#calendarfirstweekday-)
      - [calendar.isleap(year)](#calendarisleapyear)
      - [calendar.leapdays(y1,y2)](#calendarleapdaysy1y2)
      - [calendar.month(year,month,w=2,l=1)](#calendarmonthyearmonthw2l1)
      - [calendar.monthcalendar(year,month)](#calendarmonthcalendaryearmonth)
      - [calendar.monthrange(year,month)](#calendarmonthrangeyearmonth)
      - [calendar.prcal(year, w=0, l=0, c=6, m=3)](#calendarprcalyear-w0-l0-c6-m3)
      - [calendar.prmonth(theyear, themonth, w=0, l=0)](#calendarprmonththeyear-themonth-w0-l0)
      - [calendar.setfirstweekday(weekday)](#calendarsetfirstweekdayweekday)
      - [calendar.timegm(tupletime)](#calendartimegmtupletime)
      - [calendar.weekday(year,month,day)](#calendarweekdayyearmonthday)
  - [内置函数](#内置函数-1)
    - [abs()](#abs)
    - [dict()](#dict)
    - [help()](#help)
    - [min()](#min)
    - [setattr()](#setattr)
    - [all()](#all)
    - [dir()](#dir)
    - [hex()](#hex)
    - [next()](#next)
    - [slice()](#slice)
    - [any()](#any)
    - [divmod()](#divmod)
    - [id()](#id)
    - [object()](#object)
    - [sorted()](#sorted)
    - [ascii()](#ascii)
    - [enumerate()](#enumerate)
    - [input()](#input)
    - [oct()](#oct)
    - [staticmethod()](#staticmethod)
    - [bin()](#bin)
    - [eval()](#eval)
    - [int()](#int)
    - [open()](#open)
    - [str()](#str)
    - [bool()](#bool)
    - [exec()](#exec)
    - [isinstance()](#isinstance)
    - [ord()](#ord)
    - [sum()](#sum)
    - [bytearray()](#bytearray)
    - [filter()](#filter)
    - [issubclass()](#issubclass)
    - [pow()](#pow)
    - [super()](#super)
    - [bytes()](#bytes)
    - [float()](#float)
    - [iter()](#iter)
    - [print()](#print)
    - [tuple()](#tuple)
    - [callable()](#callable)
    - [format()](#format)
    - [len()](#len)
    - [property()](#property)
    - [type()](#type)
    - [chr()](#chr)
    - [frozenset()](#frozenset)
    - [list()](#list)
    - [range()](#range)
    - [vars()](#vars)
    - [classmethod()](#classmethod)
    - [getattr()](#getattr)
    - [locals()](#locals)
    - [repr()](#repr)
    - [zip()](#zip)
    - [compile()](#compile)
    - [globals()](#globals)
    - [map()](#map)
    - [reversed()](#reversed)
    - [__import__()](#__import__)
    - [complex()](#complex)
    - [hasattr()](#hasattr)
    - [max()](#max)
    - [round()](#round)
    - [delattr()](#delattr)
    - [hash()](#hash)
    - [memoryview()](#memoryview)
    - [set()](#set)
  - [MongoDB](#mongodb)
  - [urllib](#urllib)
    - [request](#request)
    - [读取文件的一行内容(readline())](#读取文件的一行内容readline)
    - [读取文件的全部内容(readlines())](#读取文件的全部内容readlines)
    - [获取网页状态码(getcode())](#获取网页状态码getcode)
    - [编码与解码](#编码与解码)
    - [模拟头部信息(request.Request)](#模拟头部信息requestrequest)
    - [异常(error)](#异常error)
    - [解析URL(parse)](#解析urlparse)
    - [解析robots协议(robotparser)](#解析robots协议robotparser)
  - [PythonuWSGI安装配置](#pythonuwsgi安装配置)
- [参考资料](#参考资料)

<!-- /TOC -->

# 基础内容

> Python的3.0版本,常被称为Python3000,或简称Py3k.<br>
相对于Python的早期版本,这是一个较大的升级.<br>
为了不带入过多的累赘,Python 3.0在设计的时候没有考虑向下兼容.

## 简介

> Python是一个高层次的结合了解释性、编译性、互动性和面向对象的脚本语言.<br>
Python的设计具有很强的可读性,相比其语言经常使用英文关键字,其语言的一些标点符号,它具有比其语言更有特色语法结构.

* Python是一种解释型语言: 这意味着开发过程中没有了编译这个环节.<br>
类似于PHP和Perl语言.
* Python是交互式语言: 这意味着,可以在一个Python提示符 >>> 后直接执行代码.
* Python是面向对象语言: 这意味着Python支持面向对象的风格或代码封装在对象的编程技术.
* Python是初学者的语言:Python对初级程序员而言,是一种伟大的语言,它支持广泛的应用程序开发,从简单的文字处理到WWW浏览器再到游戏.

### Python发展历史

Python是由GuidovanRossum在八十年代末和九十年代初,在荷兰国家数学和计算机科学研究所设计出来的.

Python本身也是由诸多其语言发展而来的,这包括ABC、Modula-3、C、C++、Algol-68、SmallTalk、Unixshell和其脚本语言等等.

像Perl语言一样,Python源代码同样遵循GPL(GNU General Public License)协议.

现在Python是由一个核心开发团队在维护,Guido vanRossum仍然占据着至关重要的作用,指导其进展.

Python 2.0于2000年10月16日发布,增加了实现完整的垃圾回收,并且支持Unicode.

Python 3.0于2008年12月3日发布,此版不完全兼容之前的Python源代码.<br>
不过,很多新特性后来也被移植到旧的Python 2.6/2.7版本.

Python 3.0版本,常被称为Python3000,或简称Py3k.<br>
相对于Python的早期版本,这是一个较大的升级.

Python 2.7被确定为最后一个Python2.x版本,它除了支持Python2.x语法外,还支持部分Python3.1语法.

### Python特点

0. 易于学习:Python有相对较少的关键字,结构简单,和一个明确定义的语法,学习起来更加简单.
0. 易于阅读:Python代码定义的更清晰.
0. 易于维护:Python的成功在于它的源代码是相当容易维护的.
0. 一个广泛的标准库:Python的最大的优势之一是丰富的库,跨平台的,在UNIX,Windows和Macintosh兼容很好.
0. 互动模式:互动模式的支持,可以从终端输入执行代码并获得结果的语言,互动的测试和调试代码片断.
0. 可移植:基于其开放源代码的特性,Python已经被移植(也就是使其工作)到许多平台.
0. 可扩展:如果需要一段运行很快的关键代码,或者是想要编写一些不愿开放的算法,可以使用C或C++完成那部分程序,然后从Python程序中调用.
0. 数据库:Python提供所有主要的商业数据库的接口.
0. GUI编程:Python支持GUI可以创建和移植到许多系统调用.
0. 可嵌入: 可以将Python嵌入到C/C++程序,程序的用户获得 *脚本化* 的能力.

### Python应用

* Youtube - 视频社交网站
* Reddit - 社交分享网站
* Dropbox - 文件分享服务
* 豆瓣网 - 图书、唱片、电影等文化产品的资料数据库网站
* 知乎 - 一个问答网站
* 果壳 - 一个泛科技主题网站
* Bottle - Python微Web框架
* EVE - 网络游戏EVE大量使用Python进行开发
* Blender - 使用Python作为建模工具与GUI语言的开源3D绘图软件
* Inkscape - 一个开源的SVG矢量图形编辑器

## 环境搭建

### Python安装

#### Unix & Linux

打开WEB浏览器访问https://www.python.org/downloads/source/ 选择适用于Unix/Linux的源码压缩包.

下载及解压压缩包Python-3.x.x.tgz,3.x.x为下载的对应版本号.

```sh
tar -zxvf Python-3.6.1.tgz
cd Python-3.6.1
./configure
make && make install

# 检查Python3是否正常可用:

python3 -V
#=> Python 3.6.1
```

如果需要自定义一些选项修改Modules/Setup

#### MAC

MAC系统都自带有Python2.7环境,可以在链接https://www.python.org/downloads/mac-osx/ 上下载最新版安装Python3.x.<br>
也可以参考源码安装的方式来安装.


### pip

pip是Python包管理工具,该工具提供了对Python包的查找、下载、安装、卸载的功能.<br>
软件包也可以在https://pypi.org/ 中找到.<br>
目前最新的Python版本已经预装了pip.

查看是否已经安装pip可以使用以下命令:

```sh
pip --version
```

#### 下载安装包

```sh
pip install some-package-name

# 例如安装numpy包:
pip install numpy
```

#### 移除软件包

```sh
pip uninstall some-package-name

# 例如移除numpy包:
pip uninstall numpy
```

#### 查看已经安装的软件包

```py
pip list
```

### 环境变量配置

#### Unix/Linux

```sh
export PATH="$PATH:/usr/local/bin/python"
```

### Python环境变量

下面几个重要的环境变量,它应用于Python:

变量名|描述
-|-
PYTHONPATH|PYTHONPATH是Python搜索路径,默认import的模块都会从PYTHONPATH里面寻找.
PYTHONSTARTUP|Python启动后,先寻找PYTHONSTARTUP环境变量,然后执行此变量指定的文件中的代码.
PYTHONCASEOK|加入PYTHONCASEOK的环境变量, 就会使python导入模块的时候不区分大小写.
PYTHONHOME|另一种模块搜索路径.<br>它通常内嵌于的PYTHONSTARTUP或PYTHONPATH目录中,使得两个模块库更容易切换.

### 运行Python

#### 交互式解释器

可以通过命令行窗口进入Python并开在交互式解释器中开始编写Python代码.

可以在Unix、DOS或任何其提供了命令行或者shell的系统进行Python编码工作.

```sh
python
```

Python命令行参数:

选项|描述
-|-
-d|在解析时显示调试信息
-O|生成优化代码 ( .pyo文件 )
-S|启动时不引入查找Python路径的位置
-V|输出Python版本号
-X|从1.6版本之后基于内建的异常(仅仅用于字符串)已过时.
-c cmd|执行Python脚本,并将运行结果作为cmd字符串.
file|在给定的python文件执行python脚本.

#### 命令行脚本

在应用程序中通过引入解释器可以在命令行中执行Python脚本,如下所示:

```sh
python script.py
```

#### 集成开发环境

#####  VSCode

## 基础语法

### 编码

默认情况下,Python3源码文件以UTF-8编码,所有字符串都是unicode字符串.<br>
当然也可以为源码文件指定不同的编码:


```py
# -*- coding: cp-1252 -*-
```

上述定义允许在源文件中使用Windows-1252字符集中的字符编码,对应适合语言为保加利亚语、白罗斯语、马其顿语、俄语、塞尔维亚语.

### 标识符

0. 第一个字符必须是字母表中字母或下划线_.<br>
标识符的其部分由字母、数字和下划线组成.
0. 标识符对大小写敏感.
0. 在Python3中,可以用中文作为变量名,非ASCII标识符也是允许的了.

### 保留字

Python的标准库提供了一个keyword模块,可以输出当前版本的所有关键字:


```py
import keyword
keyword.kwlist
# => ['False', 'None', 'True', 'and', 'as', 'assert', 'break', 'class', 'continue', 'def', 'del', 'elif', 'else', 'except', 'finally', 'for', 'from', 'global', 'if', 'import', 'in', 'is', 'lambda', 'nonlocal', 'not', 'or', 'pass', 'raise', 'return', 'try', 'while', 'with', 'yield']
```

### 注释


```py
# 单行注释

# 多行注释可以用多个 # 号,还有 ''' 和 """:

# 第一个注释
# 第二个注释

'''
第三注释
第四注释
'''

"""
第五注释
第六注释
"""
```


### 行与缩进

***python最具特色的就是使用缩进来表示代码块,不需要使用大括号{} *** .

**缩进的空格数是可变的,但是同一个代码块的语句必须包含相同的缩进空格数.**

```py
if True:
print ("True")
else:
print ("False")
```

### 多行语句

Python通常是一行写完一条语句,但如果语句很长,可以使用反斜杠 \ 来实现多行语句,例如:

```py
total = item_one + \
item_two + \
item_three
```

在 [], {}, 或 () 中的多行语句,不需要使用反斜杠 \,例如:

```py
total = ['item_one', 'item_two', 'item_three',
'item_four', 'item_five']
```


### 空行

函数之间或类的方法之间用空行分隔,表示一段新的代码的开始.<br>
类和函数入口之间也用一行空行分隔,以突出函数入口的开始.

空行与代码缩进不同,空行并不是Python语法的一部分.<br>
书写时不插入空行,Python解释器运行也不会出错.<br>
但是空行的作用在于分隔两段不同功能或含义的代码,便于日后代码的维护或重构.

记住:***空行也是程序代码的一部分 *** .

### 同一行显示多条语句

同一行中使用多条语句,语句之间使用分号 ; 分割.

```py
#!/usr/bin/python3

import sys; x = 'runoob'; sys.stdout.write(x + '\n')
```

### 多个语句构成代码组

缩进相同的一组语句构成一个代码块,称之代码组.

像if、while、def和class这样的复合语句,首行以关键字开始,以冒号( : )结束,该行之后的一行或多行代码构成代码组.

将首行及后面的代码组称为一个子句(clause).

如下实例:

```py
if expression :
suite
elif expression :
suite
else :
suite
```

### print输出

print默认输出是换行的,如果要实现不换行需要在变量末尾加上end="":

```py
#!/usr/bin/python3

x="a"
y="b"
# 换行输出
print( x )
print( y )

print('---------')
# 不换行输出
print( x, end=" " )
print( y, end=" " )
print()
```

### del语句

也可以使用del语句删除一些对象引用.

```py
del var1[,var2[,var3[....,varN]]]
del var
del var_a, var_b
```

### end关键字

关键字end可以用于将结果输出到同一行,或者在输出的末尾添加不同的字符,实例如下:

```py
#!/usr/bin/python3

# Fibonacci series: 斐波纳契数列
# 两个元素的总和确定了下一个数
a, b = 0, 1
while b < 1000:
print(b, end=',')
a, b = b, a+b
# => 1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,
```

## 基本数据类型

Python中的变量不需要声明.<br>
每个变量在使用前都必须赋值,变量赋值以后该变量才会被创建.

在Python中,变量就是变量,它没有类型,所说的"类型"是变量所指的内存中对象的类型.

等号(=)用来给变量赋值.

等号(=)运算符左边是一个变量名,等号(=)运算符右边是存储在变量中的值.

### 数据类型转换

有时候,需要对数据内置的类型进行转换,数据类型的转换,只需要将数据类型作为函数名即可.

以下几个内置的函数可以执行数据类型之间的转换.<br>
这些函数返回一个新的对象,表示转换的值.


函数|描述
-|-
int(x [,base])|将x转换为一个整数
float(x)|将x转换到一个浮点数
complex(real [,imag])|创建一个复数
str(x)|将对象x转换为字符串
repr(x)|将对象x转换为表达式字符串
eval(str)|用来计算在字符串中的有效Python表达式,并返回一个对象
tuple(s)|将序列s转换为一个元组
list(s)|将序列s转换为一个列表
set(s)|转换为可变集合
dict(d)|创建一个字典.<br>d必须是一个 (key, value)元组序列.
frozenset(s)|转换为不可变集合
chr(x)|将一个整数转换为一个字符
ord(x)|将一个字符转换为它的整数值
hex(x)|将一个整数转换为一个十六进制字符串
oct(x)|将一个整数转换为一个八进制字符串

### 多个变量赋值

Python允许同时为多个变量赋值.例如:

```py
a = b = c = 1
```

以上实例,创建一个整型对象,值为1,从后向前赋值,三个变量被赋予相同的数值.

也可以为多个对象指定多个变量.例如:

```py
a, b, c = 1, 2, "runoob"
```

以上实例,两个整型对象1和2的分配给变量a和b,字符串对象 "runoob" 分配给变量c.


### 标准数据类型

Python3中有六个标准的数据类型:

0. Number(数字)
0. String(字符串)
0. List(列表)
0. Tuple(元组)
0. Set(集合)
0. Dictionary(字典)

Python3的六个标准数据类型中:

不可变数据(3个):Number(数字)、String(字符串)、Tuple(元组)

可变数据(3个):List(列表)、Dictionary(字典)、Set(集合).

string、list和tuple都属于sequence(序列).

### 数字(Number)类型

python中数字有四种类型:整数、布尔型、浮点数和复数.<br>
在Python 3里,只有一种整数类型int,表示为长整型,没有python2中的Long.

0. int (整数), 如1, 只有一种整数类型int,表示为长整型,没有python2中的Long.
0. bool (布尔), 如True.
0. float (浮点数), 如1.23、3E-2
0. complex (复数), 如1+ 2j、1.1 + 2.2j

```py
内置的type() 函数可以用来查询变量所指的对象类型.

a, b, c, d = 20, 5.5, True, 4+3j
print(type(a), type(b), type(c), type(d))
<class 'int'> <class 'float'> <class 'bool'> <class 'complex'>

# 此外还可以用isinstance来判断:

a = 111
isinstance(a, int)
True
```

#### isinstance和type的区别

>Python3中,bool是int的子类,<br>
True和False可以和数字相加, True==1、False==0会返回True,但可以通过is来判断类型.

* type()不会认为子类是一种父类类型.
* isinstance()会认为子类是一种父类类型.

#### 数值运算

0. 数值的除法包含两个运算符:/ 返回一个浮点数,// 返回一个整数.
0. 在混合计算时,Python会把整型转换成为浮点数.

```py
2 / 4  # 除法,得到一个浮点数
0.5
2 // 4 # 除法,得到一个整数
0
```

#### 数字类型转换

有时候,需要对数据内置的类型进行转换,数据类型的转换,只需要将数据类型作为函数名即可.

0. int(x) 将x转换为一个整数.
0. float(x) 将x转换到一个浮点数.
0. complex(x) 将x转换到一个复数,实数部分为x,虚数部分为0.
0. complex(x, y) 将x和y转换到一个复数,实数部分为x,虚数部分为y.x和y是数字表达式.

#### 数学函数

函数|返回值 ( 描述 )
-|-|
abs(x)|返回数字的绝对值,如abs(-10) 返回10
ceil(x)|返回数字的上入整数,如math.ceil(4.1) 返回5
exp(x)|返回e的x次幂(ex),如math.exp(1) 返回2.718281828459045
fabs(x)|返回数字的绝对值,如math.fabs(-10) 返回10.0
floor(x)|返回数字的下舍整数,如math.floor(4.9)返回4
log(x)|如math.log(math.e)返回1.0,math.log(100,10)返回2.0
log10(x)|返回以10为基数的x的对数,如math.log10(100)返回2.0
max(x1, x2,...)|返回给定参数的最大值,参数可以为序列.
min(x1, x2,...)|返回给定参数的最小值,参数可以为序列.
modf(x)|返回x的整数部分与小数部分,两部分的数值符号与x相同,整数部分以浮点型表示.
pow(x, y)|x**y运算后的值.
round(x [,n])|返回浮点数x的四舍五入值,如给出n值,则代表舍入到小数点后的位数.<br>其实准确的说是保留值将保留到离上一位更近的一端.
sqrt(x)|返回数字x的平方根.

#### 随机数函数

函数|描述
-|-|
choice(seq)|从序列的元素中随机挑选一个元素,比如random.choice(range(10)),从0到9中随机挑选一个整数.
randrange ([start,] stop [,step])|从指定范围内,按指定基数递增的集合中获取一个随机数,基数默认值为1
random()|随机生成下一个实数,它在[0,1)范围内.
seed([x])|改变随机数生成器的种子seed.<br>如果不了解其原理,不必特别去设定seed,Python会帮选择seed.
shuffle(lst)|将序列的所有元素随机排序
uniform(x, y)|随机生成下一个实数,它在[x,y]范围内.

#### 三角函数

函数|描述
-|-|
acos(x)|返回x的反余弦弧度值.
asin(x)|返回x的反正弦弧度值.
atan(x)|返回x的反正切弧度值.
atan2(y, x)|返回给定的X及Y坐标值的反正切值.
cos(x)|返回x的弧度的余弦值.
hypot(x, y)|返回欧几里德范数sqrt(x*x + y*y).
sin(x)|返回的x弧度的正弦值.
tan(x)|返回x弧度的正切值.
degrees(x)|将弧度转换为角度,如degrees(math.pi/2) , 返回90.0
radians(x)|将角度转换为弧度

#### 数学常量

常量|描述
-|-|
pi|数学常量pi(圆周率,一般以π来表示)
e|数学常量e,e即自然常数(自然常数).

### 字符串(String)

0. python中单引号和双引号使用完全相同.
0. 使用三引号(''' 或 """)可以指定一个多行字符串.
0. 转义符 \
0. 反斜杠可以用来转义,使用r(r指raw,即rawstring)可以让反斜杠不发生转义.<br> 如r"this is a line with \n" 则\n会显示,并不是换行.
0. 按字面意义级联字符串,如"this " "is " "string"会被自动转换为this is string.
0. 字符串可以用 + 运算符连接在一起,用 * 运算符重复.
0.Python中的字符串有两种索引方式,从左往右以0开始,从右往左以 -1开始.
0. Python中的字符串不能改变.
0.Python没有单独的字符类型,一个字符就是长度为1的字符串.
0. 字符串的截取的语法格式如下:变量[头下标:尾下标:步长]

```py
word = '字符串'
sentence = "这是一个句子.<br>
"
paragraph = """这是一个段落,
可以由多行组成"""

print('\n')       # 输出空行
#=>
print(r'\n')      # 输出 \n
#=> \n
```

#### 字符串的截取

索引值以0为开始值,-1为从末尾的开始位置:

![](https://static.runoob.com/wp-content/uploads/123456-20200923-1.svg)

```py
#!/usr/bin/python3

str='123456789'

print(str)                 # 输出字符串
print(str[0:-1])           # 输出第一个到倒数第二个的所有字符
print(str[0])              # 输出字符串第一个字符
print(str[2:5])            # 输出从第三个开始到第五个的字符
print(str[2:])             # 输出从第三个开始后的所有字符
print(str[1:5:2])          # 输出从第二个开始到第五个且每隔一个的字符(步长为2)
print(str * 2)             # 输出字符串两次
print(str + '好')         # 连接字符串

print('------------------------------')

print('hello\nrunoob')      # 使用反斜杠(\)+n转义特殊字符
print(r'hello\nrunoob')     # 在字符串前面添加一个r,表示原始字符串,不会发生转义
```

与C字符串不同的是,Python字符串不能被改变.<br>
向一个索引位置赋值,会导致错误.

#### 转义字符

转义字符|描述
-|-
\(在行尾时)|续行符
\\\ |反斜杠符号
\\'|单引号
\\"|双引号
\a|响铃
\b|退格(Backspace)
\000|空
\n|换行
\v|纵向制表符
\t|横向制表符
\r|回车,将 \r后面的内容移到字符串开头,并逐一替换开头部分的字符,直至将 \r后面的内容完全替换完成.
\f|换页
\yyy|八进制数,y代表0~7的字符,例如:\012代表换行.
\xyy|十六进制数,以 \x开头,y代表的字符,例如:\x0a代表换行
\other|其它的字符以普通格式输出

#### 格式化

```py
#!/usr/bin/python3

print ("叫 %s今年 %d岁!" % ('小明', 10))
```

字符串格式化符号:

符号|描述
-|-
%c|格式化字符及其ASCII码
%s|格式化字符串
%d|格式化整数
%u|格式化无符号整型
%o|格式化无符号八进制数
%x|格式化无符号十六进制数
%X|格式化无符号十六进制数(大写)
%f|格式化浮点数字,可指定小数点后的精度
%e|用科学计数法格式化浮点数
%E|作用同%e,用科学计数法格式化浮点数
%g| %f和%e的简写
%G| %f和 %E的简写
%p|用十六进制数格式化变量的地址

格式化操作符辅助指令:

符号|功能
-|-
*|定义宽度或者小数点精度
-|用做左对齐
+|在正数前面显示加号( + )
\<sp>|在正数前面显示空格
\#|在八进制数前面显示零('0'),在十六进制前面显示'0x'或者'0X'(取决于用的是'x'还是'X')
0|显示的数字前面填充'0'而不是默认的空格
%|'%%'输出一个单一的'%'
(var)|映射变量(字典参数)
m.n.|m是显示的最小总宽度,n是小数点后的位数(如果可用的话)

#### 三引号

三引号允许一个字符串跨多行,字符串中可以包含换行符、制表符以及其特殊字符.<br>
实例如下:

```py
#!/usr/bin/python3

para_str = """这是一个多行字符串的实例
多行字符串可以使用制表符
TAB ( \t ).
也可以使用换行符 [ \n ].
"""
print (para_str)
```

以上实例执行结果为:

```
这是一个多行字符串的实例
多行字符串可以使用制表符
TAB (    ).
也可以使用换行符 [
].
```

三引号让程序员从引号和特殊字符串的泥潭里面解脱出来,自始至终保持一小块字符串的格式是所谓的WYSIWYG(所见即所得)格式的.

一个典型的用例是,当需要一块HTML或者SQL时,这时用字符串组合,特殊字符串转义将会非常的繁琐.


```py
errHTML = '''
<HTML><HEAD><TITLE>
Friends CGI Demo</TITLE></HEAD>
<BODY><H3>ERROR</H3>
<B>%s</B><P>
<FORM><INPUT TYPE=button VALUE=Back
ONCLICK="window.history.back()"></FORM>
</BODY></HTML>
'''
cursor.execute('''
CREATE TABLE users (
login VARCHAR(8),
uid INTEGER,
prid INTEGER)
''')
```

#### f-string

f-string是python3.6之后版本添加的,称之为字面量格式化字符串,是新的格式化字符串的语法.<br>
之前习惯用百分号 (%):


```py
name = 'Runoob'
'Hello %s' % name
'Hello Runoob'
```

f-string格式化字符串以f开头,后面跟着字符串,字符串中的表达式用大括号{}包起来,它会将变量或表达式计算后的值替换进去,实例如下:


```py
name = 'Runoob'
f'Hello{name}'  # 替换变量
'Hello Runoob'
f'{1+2}'         # 使用表达式
'3'

w = {'name': 'Runoob', 'url': 'www.runoob.com'}
f'{w["name"]}: {w["url"]}'
'Runoob: www.runoob.com'
```

用了这种方式明显更简单了,不用再去判断使用 %s,还是 %d.<br>
在Python3.8的版本中可以使用 = 符号来拼接运算表达式与结果:


```py
x = 1
print(f'{x+1}')   # Python 3.6
2

x = 1
print(f'{x+1=}')   # Python 3.8
x+1=2
```

#### Unicode字符串

在Python2中,普通字符串是以8位ASCII码进行存储的,而Unicode字符串则存储为16位unicode字符串,这样能够表示更多的字符集.<br>
使用的语法是在字符串前面加上前缀u.

在Python3中,所有的字符串都是Unicode字符串.


#### 内建函数


##### 字符串长度

返回字符串长度

```py
len(string)
```

##### 字符替换

把将字符串中的old替换成new,如果max指定,则替换不超过max次.

```py
replace(old, new [, max])
```


##### 首字母大写

将字符串的第一个字符转换为大写

```py
capitalize()
```

##### 大小写互换

将字符串中大写转换为小写,小写转换为大写

```py
swapcase()
```

##### 转大写

转换字符串中的小写字母为大写

```py
upper()
```


##### 补齐

返回一个指定的宽度width居中的字符串,fillchar为填充的字符,默认为空格.

```py
center(width, fillchar)
```

##### 词频

返回str在string里面出现的次数,如果beg或者end指定则返回指定范围内str出现的次数.

```py
count(str, beg= 0,end=len(string))
```

##### 解码

Python3中没有decode方法,但可以使用bytes对象的decode() 方法来解码给定的bytes对象,这个bytes对象可以由str.encode() 来编码返回.

```py
bytes.decode(encoding="utf-8", errors="strict")
```

##### 编码

以encoding指定的编码格式编码字符串,如果出错默认报一个ValueError的异常,除非errors指定的是'ignore'或者'replace'.

```py
encode(encoding='UTF-8',errors='strict')
```

##### 结尾符检查

检查字符串是否以obj结束,如果beg或者end指定则检查指定的范围内是否以obj结束,如果是,返回True,否则返回False.

```py
endswith(suffix, beg=0, end=len(string))
```


##### tab转空格

把字符串string中的tab符号转为空格,tab符号默认的空格数是8.

```py
expandtabs(tabsize=8)
```

##### 查找子字符串位置(find)

检测str是否包含在字符串中,如果指定范围beg和end,则检查是否包含在指定范围内,如果包含返回开始的索引值,否则返回-1.

```py
find(str, beg=0, end=len(string))
```

##### 查找子字符串位置(index)

跟find()方法一样,只不过如果str不在字符串中会报一个异常.

```py
index(str, beg=0, end=len(string))
```

##### 同一构成元素

如果字符串至少有一个字符并且所有字符都是字母或数字则返回True,否则返回False

```py
isalnum()
```

##### 是否存在非数字元素

如果字符串至少有一个字符并且所有字符都是字母或中文字则返回True, 否则返回False

```py
isalpha()
```

##### 是否只包含数字

如果字符串只包含数字则返回True否则返回False..

```py
isdigit()
```

##### 是否均为小写

如果字符串中包含至少一个区分大小写的字符,并且所有这些(区分大小写的)字符都是小写,则返回True,否则返回False

```py
islower()
```

##### 是否只包含数字

如果字符串中只包含数字字符,则返回True,否则返回False

```py
isnumeric()
```

##### 是否为空

如果字符串中只包含空白,则返回True,否则返回False.

```py
isspace()
```

##### 标题化

返回"标题化"的字符串,就是说所有单词都是以大写开始,其余字母均为小写(见istitle())

```py
title()
```

##### 是否标题化

如果字符串是标题化的(title())则返回True,否则返回False

```py
istitle()
```

##### 是否均为大写

如果字符串中包含至少一个区分大小写的字符,并且所有这些(区分大小写的)字符都是大写,则返回True,否则返回False

```py
isupper()
```

##### 分隔符拼接

以指定字符串作为分隔符,将seq中所有的元素(的字符串表示)合并为一个新的字符串

```py
join(seq)
```

##### 左对齐

```py
ljust(width[, fillchar])
```


返回一个原字符串左对齐,并使用fillchar填充至长度width的新字符串,fillchar默认为空格.

##### 大写转小写

转换字符串中所有大写字符为小写.

```py
lower()
```

##### 截取字符串

截掉字符串左边的空格或指定字符.

```py
lstrip()
```

##### 字符映射的转换表

创建字符映射的转换表,对于接受两个参数的最简单的调用方式,第一个参数是字符串,表示需要转换的字符,第二个参数也是字符串表示转换的目标.

```py
maketrans()
```

##### 最大字母

返回字符串str中最大的字母.

```py
max(str)
```

##### 最小字母

返回字符串str中最小的字母.

```py
min(str)
```

##### 右find

类似于find()函数,不过是从右边开始查找.

```py
rfind(str, beg=0,end=len(string))
```

##### 右index

类似于index(),不过是从右边开始.

```py
rindex( str, beg=0, end=len(string))
```

##### 右对齐

返回一个原字符串右对齐,并使用fillchar(默认空格)填充至长度width的新字符串

```py
rjust(width,[, fillchar])
```

##### 删除末尾匹配字符

删除字符串末尾的空格或指定字符.

```py
rstrip()
```

##### 截取字符串

以str为分隔符截取字符串,如果num有指定值,则仅截取num+1个子字符串

```py
split(str="", num=string.count(str))
```

##### 换行

按照行('\r', '\r\n', \n')分隔,返回一个包含各行作为元素的列表,如果参数keepends为False,不包含换行符,如果为True,则保留换行符.

```py
splitlines([keepends])
```

##### 匹配开头

检查字符串是否是以指定子字符串substr开头,是则返回True,否则返回False.<br>
如果beg和end指定值,则在指定范围内检查.


```py
startswith(substr, beg=0,end=len(string))
```

##### strip

在字符串上执行lstrip()和rstrip()

```py
strip([chars])
```


##### translate

根据str给出的表(包含256个字符)转换string的字符, 要过滤掉的字符放到deletechars参数中

```py
translate(table, deletechars="")
```

##### 截取指定长度

返回长度为width的字符串,原字符串右对齐,前面填充0

```py
zfill (width)
```

##### 十进制检测

检查字符串是否只包含十进制字符,如果是返回true,否则返回false.

```py
isdecimal()
```


### 列表/序列(List)

![](https://www.runoob.com/wp-content/uploads/2014/05/positive-indexes-1.png)

List(列表) 是Python中使用最频繁的数据类型.

```py
list1 = ['Google', 'Runoob', 1997, 2000]
list2 = [1, 2, 3, 4, 5 ]
list3 = ["a", "b", "c", "d"]
list4 = ['red', 'green', 'blue', 'yellow', 'white', 'black']
```

列表可以完成大多数集合类的数据结构实现.<br>
列表中元素的类型可以不相同,它支持数字,字符串甚至可以包含列表(所谓嵌套).

列表是写在方括号 [] 之间、用逗号分隔开的元素列表.

和字符串一样,列表同样可以被索引和截取,列表被截取后返回一个包含所需元素的新列表.

列表截取的语法格式如下:

```
变量[头下标:尾下标]
```

![](https://www.runoob.com/wp-content/uploads/2014/08/list_slicing1_new1.png)

与字符串不一样的是,列表中的元素是可以改变的.


#### 函数&方法

##### 下标默认值

```python
myList = [1,2,3,4]
n=3
next(iter(myList[n:n+1]), None)
# => 4

n=5
next(iter(myList[n:n+1]), None)
# => 

next(iter(myList[n:n+1]), 'None')
# => 'None'
```

##### 遍历(enumerate)

```py
for i, v in enumerate(['tic', 'tac', 'toe']):
print(i, v)
```

##### 多序列遍历(zip)

```py
questions = ['name', 'quest', 'favorite color']
answers = ['lancelot', 'the holy grail', 'blue']
for q, a in zip(questions, answers):
print('What isyour{0}?  Itis{1}.'.format(q, a))
```

##### 反向遍历(reversed)

```py
for i in reversed(range(1, 10, 2)):
print(i)
```

##### 删除列表元素

```py
del list[index]
```

##### 列表元素个数

```py
len(list)
```

##### 返回列表元素最大值

```py
max(list)
```

##### 返回列表元素最小值

```py
min(list)
```

##### 将元组转换为列表

```py
list(seq)
```

##### 在列表末尾添加新的对象

```py
list.append(obj)
```

##### 统计某个元素在列表中出现的次数

```py
list.count(obj)
```

##### 在列表末尾一次性追加另一个序列中的多个值(用新列表扩展原来的列表)

```py
list.extend(seq)
```

##### 从列表中找出某个值第一个匹配项的索引位置

```py
list.index(obj)
```

##### 将对象插入列表

```py
list.insert(index, obj)
```

##### 移除列表中的一个元素(默认最后一个元素),并且返回该元素的值

```py
list.pop([index=-1])
```

##### 移除列表中某个值的第一个匹配项

```py
list.remove(obj)
```

##### 反向列表中元素

```py
list.reverse()
```

##### 对原列表进行排序

```py
list.sort( key=None, reverse=False)
```

##### 清空列表

```py
list.clear()
```

##### 复制列表

```py
list.copy()
```

### Tuple(元组)

![](https://www.runoob.com/wp-content/uploads/2016/04/tup-2020-10-27-10-26-2.png)

```py
tup1 = ('Google', 'Runoob', 1997, 2000)
tup2 = (1, 2, 3, 4, 5 )
tup3 = "a", "b", "c", "d"   #  不需要括号也可以
```

元组(tuple)与列表类似,不同之处在于 **元组的元素不能修改** .<br>
元组写在小括号 () 里,元素之间用逗号隔开.

元组中的元素类型也可以不相同:

```py
#!/usr/bin/python3

tuple = ( 'abcd', 786 , 2.23, 'runoob', 70.2  )
tinytuple = (123, 'runoob')

print (tuple)             # 输出完整元组
print (tuple[0])          # 输出元组的第一个元素
print (tuple[1:3])        # 输出从第二个元素开始到第三个元素
print (tuple[2:])         # 输出从第三个元素开始的所有元素
print (tinytuple * 2)     # 输出两次元组
print (tuple + tinytuple) # 连接元组
```

元组与字符串类似,可以被索引且下标索引从0开始,-1为从末尾开始的位置.<br>
也可以进行截取.

其实,可以把字符串看作一种特殊的元组.

#### 0个或1个元素的元组

虽然tuple的元素不可改变,但它可以包含可变的对象,比如list列表.

构造包含0个或1个元素的元组比较特殊,所以有一些额外的语法规则:

```py
tup1 = ()    # 空元组
tup2 = (20,) # 一个元素,需要在元素后添加逗号
```

#### 修改元组

元组中的元素值是不允许修改的,但可以对元组进行连接组合,如下实例:

```py
#!/usr/bin/python3

tup1 = (12, 34.56)
tup2 = ('abc', 'xyz')

# 以下修改元组元素操作是非法的.

# tup1[0] = 100

# 创建一个新的元组
tup3 = tup1 + tup2
```

#### 内置函数

##### 计算元组元素个数

```py
len(tuple)
```

##### 返回元组中元素最大值

```py
max(tuple)
```

##### 返回元组中元素最小值

```py
min(tuple)
```

##### 将可迭代系列转换为元组

```py
tuple(iterable)
```

#### 关于元组是不可变的

所谓元组的不可变指的是元组所指向的内存中的内容不可变.

```py
tup = ('r', 'u', 'n', 'o', 'o', 'b')
tup[0] = 'g'     # 不支持修改元素
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
TypeError: 'tuple' object does not support item assignment
id(tup)     # 查看内存地址
4440687904
tup = (1,2,3)
id(tup)
4441088800    # 内存地址不一样了
```

从以上实例可以看出,重新赋值的元组tup,绑定到新的对象了,不是修改了原来的对象.

### Set(集合)

集合(set)是由一个或数个形态各异的大小整体组成的,构成集合的事物或对象称作元素或是成员.

集合是一个 **无序不重复** 元素的集.<br>
基本功能包括关系测试和消除重复元素.

可以使用大括号{}或者set() 函数创建集合,注意:创建一个空集合必须用set() 而不是{},因为{}是用来创建一个空字典.

创建格式:

```py
parame = {value01,value02,...}
# 或者
set(value)
```

```py
#!/usr/bin/python3

sites = {'Google', 'Taobao', 'Runoob', 'Facebook', 'Zhihu', 'Baidu'}

print(sites)   # 输出集合,重复的元素被自动去掉

# 成员测试
if 'Runoob' in sites :
print('Runoob在集合中')
else :
print('Runoob不在集合中')

# set可以进行集合运算
a = set('abracadabra')
b = set('alacazam')

print(a)

print(a - b)     # a和b的差集

print(a|b)     # a和b的并集

print(a & b)     # a和b的交集

print(a ^ b)     # a和b中不同时存在的元素
```

以上实例输出结果:

```sh
{'Zhihu', 'Baidu', 'Taobao', 'Runoob', 'Google', 'Facebook'}
Runoob在集合中
{'b', 'c', 'a', 'r', 'd'}
{'r', 'b', 'd'}
{'b', 'c', 'a', 'z', 'm', 'r', 'l', 'd'}
{'c', 'a'}
{'z', 'b', 'm', 'r', 'l', 'd'}
```

#### 函数&方法

##### 添加元素

将元素x添加到集合s中,如果元素已存在,则不进行任何操作.x可以有多个,用逗号分开.

```py
s.add( x )
```

##### 移除元素

将元素x从集合s中移除,如果元素不存在,则会发生错误.

```py
s.remove( x )
```

此外还有一个方法也是移除集合中的元素,且如果元素不存在,不会发生错误.


```py
s.discard( x )
```

也可以设置随机删除集合中的一个元素,set集合的pop方法会对集合进行无序的排列,然后将这个无序排列集合的左面第一个元素进行删除.

```py
s.pop()
```

##### 计算集合元素个数


```py
len(s)
```

##### 清空集合


```py
s.clear()
```

##### 判断元素是否在集合中存在

判断元素x是否在集合s中,存在返回True,不存在返回False.

```py
x in s
```


##### 拷贝一个集合

```py
copy()
```

##### 返回多个集合的差集

```py
difference()
```

##### 移除集合中的元素,该元素在指定的集合也存在

```py
difference_update()
```

##### 删除集合中指定的元素

```py
discard()
```

##### 返回集合的交集

```py
intersection()
```

##### 返回集合的交集

```py
intersection_update()
```

##### 判断两个集合是否包含相同的元素,如果没有返回True,否则返回False

```py
isdisjoint()
```

##### 判断指定集合是否为该方法参数集合的子集

```py
issubset()
```

##### 判断该方法的参数集合是否为指定集合的子集

```py
issuperset()
```

##### 随机移除元素

```py
pop()
```

##### 移除指定元素

```py
remove()
```

##### 返回两个集合中不重复的元素集合

```py
symmetric_difference()
```

##### 移除当前集合中在另外一个指定集合相同的元素,并将另外一个指定集合中不同的元素插入到当前集合中

```py
symmetric_difference_update()
```

##### 返回两个集合的并集

```py
union()
```

##### 给集合添加元素

```py
update()
```

### Dictionary(字典)

![](https://www.runoob.com/wp-content/uploads/2016/04/py-dict-3.png)

![](https://www.runoob.com/wp-content/uploads/2016/04/py-dict-2.png)

字典(dictionary)是Python中另一个非常有用的内置数据类型.

列表是有序的对象集合,字典是无序的对象集合.<br>
两者之间的区别在于:字典当中的元素是通过键来存取的,而不是通过偏移存取.

字典是一种映射类型,字典用{}标识,它是一个无序的键(key) : 值(value) 的集合.

字典值可以是任何的python对象,既可以是标准的对象,也可以是用户定义的,但键不行.<br>
键(key)必须使用不可变类型.

在同一个字典中,键(key)必须是唯一的.

**如果用字典里没有的键访问数据,会报错.**

```py
#!/usr/bin/python3

dict = {}
dict['one'] = "1 - 菜鸟教程"
dict[2]     = "2 - 菜鸟工具"

tinydict = {'name': 'runoob','code':1, 'site': 'www.runoob.com'}


print (dict['one'])       # 输出键为 'one' 的值
print (dict[2])           # 输出键为2的值
print (tinydict)          # 输出完整的字典
print (tinydict.keys())   # 输出所有键
print (tinydict.values()) # 输出所有值

'''
1 - 菜鸟教程
2 - 菜鸟工具
{'name': 'runoob', 'code': 1, 'site': 'www.runoob.com'}
dict_keys(['name', 'code', 'site'])
dict_values(['runoob', 1, 'www.runoob.com']
'''
```

#### 函数&方法

##### 转列表(遍历)

以列表返回一个视图对象

```py
radiansdict.items()
```

```py
knights = {'gallahad': 'the pure', 'robin': 'the brave'}
for k, v in knights.items():
print(k, v)
```

##### 键值对序列转建字典(dict())

构造函数dict() 可以直接从键值对序列中构建字典如下:

```py
dict([('Runoob', 1), ('Google', 2), ('Taobao', 3)])
{'Runoob': 1, 'Google': 2, 'Taobao': 3}
{x: x**2 for x in (2, 4, 6)}
{2: 4, 4: 16, 6: 36}
dict(Runoob=1, Google=2, Taobao=3)
{'Runoob': 1, 'Google': 2, 'Taobao': 3}
```

##### 元素个数

计算字典元素个数,即键的总数.

```py
len(dict)
```

##### 输出字典

输出字典,可以打印的字符串表示.

```py
str(dict)
```

##### 删除字典内所有元素

```py
radiansdict.clear()
```

##### 浅复制

返回一个字典的浅复制

```py
radiansdict.copy()
```

##### 格式化键

创建一个新字典,以序列seq中元素做字典的键,val为字典所有键对应的初始值

```py
radiansdict.fromkeys()
```

##### 获取键值

返回指定键的值,如果键不在字典中返回default设置的默认值

```py
radiansdict.get(key, default=None)
```

##### 是否存在键

如果键在字典dict里返回true,否则返回false

```py
key in dict
```

##### 视图对象

返回一个视图对象

```py
radiansdict.keys()
```

##### setdefault

和get类似, 但如果键不存在于字典中,将会添加键并将值设为default

```py
radiansdict.setdefault(key, default=None)
```

##### update

把字典dict2的键/值对更新到dict里

```py
radiansdict.update(dict2)
```

##### values

返回一个视图对象

```py
radiansdict.values()
```

##### pop

删除字典给定键key所对应的值,返回值为被删除的值.<br>
key值必须给出.<br>
否则,返回default值.

```py
pop(key[,default])
```

##### popitem

随机返回并删除字典中的最后一对键和值.

```py
popitem()
```

## 解释器

Linux/Unix的系统上,一般默认的python版本为2.x,可以将python3.x安装在 /usr/local/python3目录中.

安装完成后,可以将路径 /usr/local/python3/bin添加到Linux/Unix操作系统的环境变量中,这样就可以通过shell终端输入下面的命令来启动Python3.

```sh
PATH=$PATH:/usr/local/python3/bin/python3    # 设置环境变量
python3 --version
Python 3.4.0
```

### 交互式编程

可以在命令提示符中输入"Python"命令来启动Python解释器:

```sh
$ python3
Python 3.4.0 (default, Apr 112014, 13:05:11)
[GCC 4.8.2] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

## 运算符

Python语言支持以下类型的运算符:

0. 算术运算符
0. 比较(关系)运算符
0. 赋值运算符
0. 逻辑运算符
0. 位运算符
0. 成员运算符
0. 身份运算符
0. 运算符优先级

### 逻辑运算符

假设变量a为10, b为20:

运算符|逻辑表达式|描述|实例
-|-|-|-|
and|x and y|布尔"与" - 如果x为False,x andy返回x的值,否则返回y的计算值.|(a and b) 返回20.
or|x or y|布尔"或" - 如果x是True,它返回x的值,否则它返回y的计算值.|(a or b) 返回10.
not|not x|布尔"非" - 如果x为True,返回False.如果x为False,它返回True.|not(a and b) 返回False

### 成员运算符

运算符|描述|实例
-|-|-|
in|如果在指定的序列中找到值返回True,否则返回False.|x在y序列中 , 如果x在y序列中返回True.
not in|如果在指定的序列中没有找到值返回True,否则返回False.|x不在y序列中 , 如果x不在y序列中返回True.

```py
#!/usr/bin/python3

a = 10
b = 20
list = [1, 2, 3, 4, 5 ]

if ( a in list ):
print ("1 - 变量a在给定的列表中list中")
else:
print ("1 - 变量a不在给定的列表中list中")

if ( b not in list ):
print ("2 - 变量b不在给定的列表中list中")
else:
print ("2 - 变量b在给定的列表中list中")
```

### 身份运算符

身份运算符用于比较两个对象的存储单元.

运算符|描述|实例
-|-|-|
is|is是判断两个标识符是不是引用自一个对象|x is y, 类似id(x) == id(y) , 如果引用的是同一个对象则返回True,否则返回False
is not|is not是判断两个标识符是不是引用自不同对象|x is not y , 类似id(a) != id(b).如果引用的不是同一个对象则返回结果True,否则返回False.


```py
#!/usr/bin/python3

a = 20
b = 20

if ( a is b ):
print ("1 -a和b有相同的标识")
else:
print ("1 -a和b没有相同的标识")

if ( id(a) == id(b) ):
print ("2 -a和b有相同的标识")
else:
print ("2 -a和b没有相同的标识")
```

id() 函数用于获取对象内存地址.

#### is与 == 区别

is用于判断两个变量引用对象是否为同一个, == 用于判断引用变量的值是否相等.

### 运算符优先级

运算符|描述
-|-|
**|指数 (最高优先级)
~ + -|按位翻转, 一元加号和减号 (最后两个的方法名为 +@ 和 -@)
\* / % //|乘,除,求余数和取整除
\+ -|加法减法
\>> <<|右移,左移运算符
&|位 'AND'
^ ||位运算符
<= < > >=|比较运算符
== !=|等于运算符
= %= /= //= -= += *= **=|赋值运算符
is is not|身份运算符
in not in|成员运算符
not and or|逻辑运算符


## 条件控制

### if语句

![](https://www.runoob.com/wp-content/uploads/2013/11/if-condition.jpg)

Python中用elif代替了elseif,所以if语句的关键字为:if–elif–else.

0. 每个条件后面要使用冒号 :,表示接下来是满足条件后要执行的语句块.
0. 使用缩进来划分语句块,相同缩进数的语句在一起组成一个语句块.
0. 在Python中没有switch–case语句.

```py
if condition_1:
statement_block_1
elif condition_2:
statement_block_2
else:
statement_block_3
```

## 循环语句

### while循环

![](https://www.runoob.com/wp-content/uploads/2013/11/886A6E10-58F1-4A9B-8640-02DBEFF0EF9A.jpg)

```py
while condition :
执行语句(statements)
```

```py
#!/usr/bin/env python3

n = 100

sum = 0
counter = 1
while counter <= n:
sum = sum + counter
counter += 1

print("1到 %d之和为: %d" % (n,sum))
```

#### 无限循环

可以通过设置条件表达式永远不为false来实现无限循环,实例如下:

```py
#!/usr/bin/python3

var = 1
while var == 1 :  # 表达式永远为true
num = int(input("输入一个数字  :"))
print ("输入的数字是: ", num)

print ("Good bye!")
```

#### while循环使用else语句

如果while后面的条件语句为false时,则执行else的语句块.

expr条件语句为true则执行statement(s) 语句块,如果为false,则执行additional_statement(s).

```py
while <expr>:
<statement(s)>
else:
<additional_statement(s)>
```

#### 简单语句组

类似if语句的语法,如果while循环体中只有一条语句,可以将该语句与while写在同一行中, 如下所示:

```py
#!/usr/bin/python

flag = 1

while (flag): print ('欢迎访问菜鸟教程!')

print ("Good bye!")
```

### for语句

![](https://www.runoob.com/wp-content/uploads/2013/11/A71EC47E-BC53-4923-8F88-B027937EE2FF.jpg)

for循环可以遍历任何可迭代对象,如一个列表或者一个字符串.

```py
for <variable> in <sequence>:
<statements>
else:
<statements>
```

#### range()函数

如果需要遍历数字序列,可以使用内置range()函数.<br>
它会生成数列,例如:

```py
for i in range(5):
print(i)
```

也可以使用range指定区间的值:

```py
for i in range(5,9) :
print(i)
```

#### break和continue

![](https://www.runoob.com/wp-content/uploads/2014/09/E5A591EF-6515-4BCB-AEAA-A97ABEFC5D7D.jpg)

![](https://www.runoob.com/wp-content/uploads/2014/09/8962A4F1-B78C-4877-B328-903366EA1470.jpg)

#### else语句

循环语句可以有else子句,它在穷尽列表(以for循环)或条件变为false(以while循环)导致循环终止时被执行,但循环被break终止时不执行.

#### pass语句

pass是空语句,是为了保持程序结构的完整性.

pass不做任何事情,一般用做占位语句

```py
while True:
pass  # 等待键盘中断 (Ctrl+C)
```

## 迭代器与生成器

### 迭代器

迭代器是一个可以记住遍历的位置的对象.

迭代器对象从集合的第一个元素开始访问,直到所有的元素被访问完结束.<br>
迭代器只能往前不会后退.

迭代器有两个基本的方法:iter() 和next().

字符串,列表或元组对象都可用于创建迭代器:

```py
list=[1,2,3,4]
it = iter(list)    # 创建迭代器对象
print (next(it))   # 输出迭代器的下一个元素
1
print (next(it))
2
>>>
```

迭代器对象可以使用常规for语句进行遍历:


```py
#!/usr/bin/python3

list=[1,2,3,4]
it = iter(list)    # 创建迭代器对象
for x in it:
print (x, end=" ")
```

也可以使用next() 函数:

```py
#!/usr/bin/python3

import sys         # 引入sys模块

list=[1,2,3,4]
it = iter(list)    # 创建迭代器对象

while True:
try:
print (next(it))
except StopIteration:
sys.exit()
```

### StopIteration

StopIteration异常用于标识迭代的完成,防止出现无限循环的情况,在__next__() 方法中可以设置在完成指定循环次数后触发StopIteration异常来结束迭代.

### 生成器

使用了yield的函数被称为生成器(generator).


跟普通函数不同的是,生成器是一个返回迭代器的函数,只能用于迭代操作,更简单点理解生成器就是一个迭代器.

在调用生成器运行的过程中,每次遇到yield时函数会暂停并保存当前所有的运行信息,返回yield的值, 并在下一次执行next() 方法时从当前位置继续运行.


调用一个生成器函数,返回的是一个迭代器对象.

以下实例使用yield实现斐波那契数列:

```py
#!/usr/bin/python3

import sys

def fibonacci(n): # 生成器函数 - 斐波那契
a, b, counter = 0, 1, 0
while True:
if (counter > n):
return
yield a
a, b = b, a + b
counter += 1
f = fibonacci(10) # f是一个迭代器,由生成器返回生成

while True:
try:
print (next(f), end=" ")
except StopIteration:
sys.exit()
# => 011235813213455
```

## 函数

### 定义一个函数

![](https://www.runoob.com/wp-content/uploads/2014/05/py-tup-10-26-1.png)

定义一个由自己想要功能的函数,以下是简单的规则:

0. 函数代码块以def关键词开头,后接函数标识符名称和圆括号 ().
0. 任何传入参数和自变量必须放在圆括号中间,圆括号之间可以用于定义参数.
0. 函数的第一行语句可以选择性地使用文档字符串—用于存放函数说明.
0. 函数内容以冒号 : 起始,并且缩进.
0. return [表达式] 结束函数,选择性地返回一个值给调用方,不带表达式的return相当于返回None.

```py
#!/usr/bin/python3

def hello() :
print("Hello World!")

hello()
```

### 参数传递

在python中,类型属于对象,变量是没有类型的:

```py
a=[1,2,3]

a="Runoob"
```

以上代码中,[1,2,3] 是List类型,"Runoob" 是String类型,而变量a是没有类型,她仅仅是一个对象的引用(一个指针),可以是指向List类型对象,也可以是指向String类型对象.

#### 可更改(mutable)与不可更改(immutable)对象

在python中,strings, tuples, 和numbers是不可更改的对象,而list,dict等则是可以修改的对象.

0. 不可变类型:变量赋值a=5后再赋值a=10,这里实际是新生成一个int值对象10,再让a指向它,而5被丢弃,不是改变a的值,相当于新生成了a.

0. 可变类型:变量赋值la=[1,2,3,4] 后再赋值la[2]=5则是将listla的第三个元素值更改,本身la没有动,只是其内部的一部分值被修改了.

python函数的参数传递:

不可变类型:类似C++ 的值传递,如整数、字符串、元组.<br>
如fun(a),**传递的只是a的值,没有影响a对象本身**.<br>
如果在fun(a) 内部修改a的值,则是新生成一个a的对象.

可变类型:类似C++ 的引用传递,如列表,字典.<br>
如fun(la),则是将la真正的传过去,修改后fun外部的la也会受影响

python中一切都是对象,**严格意义不能说值传递还是引用传递,应该说传不可变对象和传可变对象**.

### return语句

return [表达式] 语句用于退出函数,选择性地向调用方返回一个表达式.<br>
**不带参数值的return语句返回None**.

```py
#!/usr/bin/python3

# 可写函数说明
def sum( arg1, arg2 ):
# 返回2个参数的和."
total = arg1 + arg2
print ("函数内 : ", total)
return total

# 调用sum函数
total = sum( 10, 20 )
print ("函数外 : ", total)
```

### 参数

以下是调用函数时可使用的正式参数类型:

0. 必需参数
0. 关键字参数
0. 默认参数
0. 不定长参数

#### 必需参数

必需参数须以正确的顺序传入函数.<br>
调用时的数量必须和声明时的一样.

调用printme() 函数,必须传入一个参数,不然会出现语法错误:

```py
#!/usr/bin/python3

#可写函数说明
def printme( str ):
"打印任何传入的字符串"
print (str)
return

# 调用printme函数,不加参数会报错
printme()
```

#### 关键字参数

关键字参数和函数调用关系紧密,函数调用使用关键字参数来确定传入的参数值.

使用关键字参数允许函数调用时参数的顺序与声明时不一致,因为Python解释器能够用参数名匹配参数值.


以下实例在函数printme() 调用时使用参数名:

```py
#!/usr/bin/python3

#可写函数说明
def printme( str ):
"打印任何传入的字符串"
print (str)
return

#调用printme函数
printme( str = "菜鸟教程")
```

以下实例中演示了函数参数的使用不需要使用指定顺序:

```py
#!/usr/bin/python3

#可写函数说明
def printinfo( name, age ):
"打印任何传入的字符串"
print ("名字: ", name)
print ("年龄: ", age)
return

#调用printinfo函数
printinfo( age=50, name="runoob" )
```

#### 默认参数

调用函数时,如果没有传递参数,则会使用默认参数.<br>
以下实例中如果没有传入age参数,则使用默认值:

```py
#!/usr/bin/python3

#可写函数说明
def printinfo( name, age = 35 ):
"打印任何传入的字符串"
print ("名字: ", name)
print ("年龄: ", age)
return

#调用printinfo函数
printinfo( age=50, name="runoob" )
print ("------------------------")
printinfo( name="runoob" )
```


#### 不定长参数

可能需要一个函数能处理比当初声明时更多的参数.<br>
这些参数叫做不定长参数,和上述2种参数不同,声明时不会命名.<br>
基本语法如下:

```py
def functionname([formal_args,] *var_args_tuple ):
"函数_文档字符串"
function_suite
return [expression]
```

加了星号 * 的参数会以元组(tuple)的形式导入,存放所有未命名的变量参数.

```py
#!/usr/bin/python3

# 可写函数说明
def printinfo( arg1, *vartuple ):
"打印任何传入的参数"
print ("输出: ")
print (arg1)
print (vartuple)

# 调用printinfo函数
printinfo( 70, 60, 50 )
```
如果在函数调用时没有指定参数,它就是一个空元组.<br>
也可以不向函数传递未命名的变量.<br>
如下实例:

```py
#!/usr/bin/python3

# 可写函数说明
def printinfo( arg1, *vartuple ):
"打印任何传入的参数"
print ("输出: ")
print (arg1)
for var in vartuple:
print (var)
return

# 调用printinfo函数
printinfo( 10 )
printinfo( 70, 60, 50 )
```

还有一种就是参数带两个星号 **基本语法如下:

```py
def functionname([formal_args,] **var_args_dict ):
"函数_文档字符串"
function_suite
return [expression]
```

加了两个星号 ** 的参数会以字典的形式导入.

```py
#!/usr/bin/python3

# 可写函数说明
def printinfo( arg1, **vardict ):
"打印任何传入的参数"
print ("输出: ")
print (arg1)
print (vardict)

# 调用printinfo函数
printinfo(1, a=2,b=3)
```

声明函数时,参数中星号 * 可以单独出现,例如:

```py
def f(a,b,*,c):
return a+b+c
```

如果单独出现星号 * 后的参数必须用关键字传入.


```py
def f(a,b,*,c):
return a+b+c

f(1,2,3)   # 报错
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
TypeError: f() takes 2 positional arguments but 3 were given
f(1,2,c=3) # 正常
6
```

#### 匿名函数

所谓匿名,意即不再使用def语句这样标准的形式定义一个函数.

```py
lambda [arg1 [,arg2,.....argn]]:expression
```

lambda只是一个表达式,函数体比def简单很多.<br>
lambda的主体是一个表达式,而不是一个代码块.<br>
仅仅能在lambda表达式中封装有限的逻辑进去.

lambda函数拥有自己的命名空间,且不能访问自己参数列表之外或全局命名空间里的参数.

虽然lambda函数看起来只能写一行,却不等同于C或C++的内联函数,后者的目的是调用小函数时不占用栈内存从而增加运行效率.

```py
#!/usr/bin/python3

# 可写函数说明
sum = lambda arg1, arg2: arg1 + arg2

# 调用sum函数
print ("相加后的值为 : ", sum( 10, 20 ))
print ("相加后的值为 : ", sum( 20, 20 ))
```

##### 强制位置参数

Python3.8新增了一个函数形参语法 / 用来指明函数形参必须使用指定位置参数,不能使用关键字参数的形式.

在以下的例子中,形参a和b必须使用指定位置参数,c或d可以是位置形参或关键字形参,而e和f要求为关键字形参:

```py
def f(a, b, /, c, d, *, e, f):
print(a, b, c, d, e, f)

f(10, 20, 30, d=40, e=50, f=60) # 正确
f(10, b=20, c=30, d=40, e=50, f=60)   # b不能使用关键字参数的形式
f(10, 20, 30, 40, 50, f=60)           # e必须使用关键字参数的形式
```

## 模块

模块是一个包含所有定义的函数和变量的文件,其后缀名是.py.<br>
模块可以被别的程序引入,以使用该模块中的函数等功能.<br>
这也是使用python标准库的方法.

```py
#!/usr/bin/python3
# 文件名: using_sys.py

#  importsys引入python标准库中的sys.py模块;这是引入某一模块的方法.
import sys

print('命令行参数如下:')
# sys.argv是一个包含命令行参数的列表
for i in sys.argv:
print(i)

# sys.path包含了一个Python解释器自动查找所需模块的路径的列表
print('\n\nPython路径为:', sys.path, '\n')
```

### import与from...import

```
import module1[, module2[,... moduleN]
```

在python用import或者from...import来导入相应的模块.

将整个模块(somemodule)导入,格式为:

```py
import somemodule
```

从某个模块中导入某个函数,格式为:

```py
from somemodule import somefunction
```

从某个模块中导入多个函数,格式为:

```py
from somemodule import firstfunc, secondfunc, thirdfunc
```

将某个模块中的全部函数导入,格式为:

```py
from somemodule import *
```

当解释器遇到import语句,如果模块在当前的搜索路径就会被导入.<br>
一个模块只会被导入一次,不管执行了多少次import.<br>
这样可以防止导入模块被一遍又一遍地执行.

### 深入模块

模块除了方法定义,还可以包括可执行的代码.<br>
这些代码一般用来初始化这个模块.<br>
这些代码只有在第一次被导入时才会被执行.

每个模块有各自独立的符号表,在模块内部为所有的函数当作全局符号表来使用.

所以,模块的作者可以放心大胆的在模块内部使用这些全局变量,而不用担心把其用户的全局变量搞混.

从另一个方面,当确实知道在做什么的话,也可以通过modname.itemname这样的表示法来访问模块内的函数.

模块是可以导入其模块的.<br>
在一个模块(或者脚本,或者其地方)的最前面使用import来导入一个模块,当然这只是一个惯例,而不是强制的.<br>
被导入的模块的名称将被放入当前操作的模块的符号表中.

还有一种导入的方法,可以使用import直接把模块内(函数,变量的)名称导入到当前操作模块.比如:

```py
from fibo import fib, fib2
fib(500)
# => 1123581321345589144233377
```

这种导入的方法不会把被导入的模块的名称放在当前的字符表中(所以在这个例子里面,fibo这个名称是没有定义的).

这还有一种方法,可以一次性的把模块中的所有(函数,变量)名称都导入到当前模块的字符表:

```py
from fibo import *
fib(500)
# => 1123581321345589144233377
```

这将把所有的名字都导入进来,但是那些由单一下划线(_)开头的名字不在此例.<br>
大多数情况,不使用这种方法,因为引入的其它来源的命名,很可能覆盖了已有的定义.

### __name__属性

一个模块被另一个程序第一次引入时,其主程序将运行.<br>
如果想在模块被引入时,模块中的某一程序块不执行,可以用__name__属性来使该程序块仅在该模块自身运行时执行.

每个模块都有一个__name__属性,当其值是'__main__'时,表明该模块自身在运行,否则是被引入.

说明:__name__与__main__底下是双下划线, __是这样去掉中间的那个空格.

```py
#!/usr/bin/python3
# Filename: using_name.py

if __name__ == '__main__':
print('程序自身在运行')
else:
print('来自另一模块')
```

### dir() 函数

内置的函数dir() 可以找到模块内定义的所有名称.<br>
以一个字符串列表的形式返回:

```py
import fibo, sys
dir(fibo)
# => ['__name__', 'fib', 'fib2']
dir(sys)
# => ['__displayhook__', '__doc__', '__excepthook__', '__loader__', '__name__','__package__', '__stderr__', '__stdin__', '__stdout__','_clear_type_cache', '_current_frames', '_debugmallocstats', '_getframe','_home', '_mercurial', '_xoptions', 'abiflags', 'api_version', 'argv','base_exec_prefix', 'base_prefix', 'builtin_module_names', 'byteorder','call_tracing', 'callstats', 'copyright', 'displayhook','dont_write_bytecode', 'exc_info', 'excepthook', 'exec_prefix','executable', 'exit', 'flags', 'float_info', 'float_repr_style','getcheckinterval', 'getdefaultencoding', 'getdlopenflags','getfilesystemencoding', 'getobjects', 'getprofile', 'getrecursionlimit','getrefcount', 'getsizeof', 'getswitchinterval', 'gettotalrefcount','gettrace', 'hash_info', 'hexversion', 'implementation', 'int_info','intern', 'maxsize', 'maxunicode', 'meta_path', 'modules', 'path','path_hooks', 'path_importer_cache', 'platform', 'prefix', 'ps1','setcheckinterval', 'setdlopenflags', 'setprofile', 'setrecursionlimit','setswitchinterval', 'settrace', 'stderr', 'stdin', 'stdout','thread_info', 'version', 'version_info', 'warnoptions']
```

如果没有给定参数,那么dir() 函数会罗列出当前定义的所有名称:

```py
a = [1, 2, 3, 4, 5]
import fibo
fib = fibo.fib
dir() # 得到一个当前模块中定义的属性列表
# => ['__builtins__', '__name__', 'a', 'fib', 'fibo', 'sys']
```

### 标准模块

Python本身带着一些标准的模块库,有些模块直接被构建在解析器里,<br>
这些虽然不是一些语言内置的功能,但是却能很高效的使用,甚至是系统级调用也没问题.

这些组件会根据不同的操作系统进行不同形式的配置,<br>
比如winreg这个模块就只会提供给Windows系统.


应该注意到这有一个特别的模块sys,它内置在每一个Python解析器中.<br>
变量sys.ps1和sys.ps2定义了主提示符和副提示符所对应的字符串:

```py
import sys
sys.ps1
'>>> '
sys.ps2
'... '
sys.ps1 = 'C> '
C> print('Runoob!')
Runoob!
C>
```

### 包

包是一种管理Python模块命名空间的形式,采用"点模块名称".

比如一个模块的名称是A.B, 那么表示一个包A中的子模块B.

就好像使用模块的时候,不用担心不同模块之间的全局变量相互影响一样,采用点模块名称这种形式也不用担心不同库之间的模块重名的情况.

这样不同的作者都可以提供NumPy模块,或者是Python图形库.

不妨假设想设计一套统一处理声音文件和数据的模块(或者称之为一个"包").

现存很多种不同的音频文件格式(基本上都是通过后缀名区分的,例如: .wav,:file:.aiff,:file:.au,),所以需要有一组不断增加的模块,用来在不同的格式之间转换.

并且针对这些音频数据,还有很多不同的操作(比如混音,添加回声,增加均衡器功能,创建人造立体声效果),所以还需要一组怎么也写不完的模块来处理这些操作.


这里给出了一种可能的包结构(在分层的文件系统中):

```
sound/                          顶层包
__init__.py               初始化sound包
formats/                  文件格式转换子包
__init__.py
wavread.py
wavwrite.py
aiffread.py
aiffwrite.py
auread.py
auwrite.py
...
effects/                  声音效果子包
__init__.py
echo.py
surround.py
reverse.py
...
filters/filters子包
__init__.py
equalizer.py
vocoder.py
karaoke.py
...
```

在导入一个包的时候,Python会根据sys.path中的目录来寻找这个包中包含的子目录.

目录只有包含一个叫做__init__.py的文件才会被认作是一个包,主要是为了避免一些滥俗的名字(比如叫做string)不小心的影响搜索路径中的有效模块.


最简单的情况,放一个空的 :file:__init__.py就可以了.<br>
当然这个文件中也可以包含一些初始化代码或者为(将在后面介绍的) __all__变量赋值.

用户可以每次只导入一个包里面的特定模块,比如:

```py
import sound.effects.echo
```

这将会导入子模块:sound.effects.echo.<br>
必须使用全名去访问:

```py
sound.effects.echo.echofilter(input, output, delay=0.7, atten=4)
```

还有一种导入子模块的方法是:

```py
from sound.effects import echo
```

这同样会导入子模块: echo,并且不需要那些冗长的前缀,所以可以这样使用:

```py
echo.echofilter(input, output, delay=0.7, atten=4)
```

还有一种变化就是直接导入一个函数或者变量:

```py
from sound.effects.echo import echofilter
```

同样的,这种方法会导入子模块: echo,并且可以直接使用echofilter() 函数:

```py
echofilter(input, output, delay=0.7, atten=4)
```

注意当使用from package import item这种形式的时候,<br>
对应的item既可以是包里面的子模块(子包),或者包里面定义的其名称,比如函数,类或者变量.

import语法会首先把item当作一个包定义的名称,如果没找到,再试图按照一个模块去导入.<br>
如果还没找到,抛出一个 :exc:ImportError异常.


反之,如果使用形如import item.subitem.subsubitem这种导入形式,除了最后一项,都必须是包,而最后一项则可以是模块或者是包,但是不可以是类,函数或者变量的名字.

### 从一个包中导入*

如果使用from sound.effects import * 会发生什么呢?

Python会进入文件系统,找到这个包里面所有的子模块,然后一个一个的把它们都导入进来.

但这个方法在Windows平台上工作的就不是非常好,因为Windows是一个不区分大小写的系统.

在Windows平台平台上,无法确定一个叫做ECHO.py的文件导入为模块是echo还是Echo,或者是ECHO.

为了解决这个问题,只需要提供一个精确包的索引.


导入语句遵循如下规则:如果包定义文件__init__.py存在一个叫做__all__的列表变量,<br>
那么在使用from package import * 的时候就把这个列表中的所有名字作为包内容导入.

作为包的作者,可别忘了在更新包之后保证__all__也更新了.

以下实例在file:sounds/effects/__init__.py中包含如下代码:

```py
__all__ = ["echo", "surround", "reverse"]
```

这表示当使用from sound.effects import *这种用法时,只会导入包里面这三个子模块.

如果__all__真的没有定义,那么使用from sound.effects import *这种语法的时候,就不会导入包sound.effects里的任何子模块.<br>
只是把包sound.effects和它里面定义的所有内容导入进来(可能运行__init__.py里定义的初始化代码).

这会把__init__.py里面定义的所有名字导入进来.<br>
并且不会破坏掉在这句话之前导入的所有明确指定的模块.<br>
看下这部分代码:

```py
import sound.effects.echo
import sound.effects.surround
from sound.effects import *
```

这个例子中,在执行from...import前,包sound.effects中的echo和surround模块都被导入到当前的命名空间中了.<br>
(当然如果定义了__all__就更没问题了)

通常并不主张使用 * 这种方法来导入模块,因为这种方法经常会导致代码的可读性降低.<br>
不过这样倒的确是可以省去不少敲键的功夫,而且一些模块都设计成了只能通过特定的方法导入.

记住,使用from Package import specific_submodule这种方法永远不会有错.<br>
事实上,这也是推荐的方法.<br>
除非是要导入的子模块有可能和其包的子模块重名.


如果在结构中包是一个子包(比如这个例子中对于包sound来说),<br>
而又想导入兄弟包(同级别的包)就得使用导入绝对的路径来导入.<br>
比如,如果模块sound.filters.vocoder要使用包sound.effects中的模块echo,就要写成fromsound.effects import echo.

```py
from . import echo
from .. import formats
from ..filters import equalizer
```

无论是隐式的还是显式的相对导入都是从当前模块开始的.<br>
主模块的名字永远是"__main__",一个Python应用程序的主模块,应当总是使用绝对路径引用.

包还提供一个额外的属性__path__.<br>
这是一个目录列表,里面每一个包含的目录都有为这个包服务的__init__.py,得在其__init__.py被执行前定义哦.<br>
可以修改这个变量,用来影响包含在包里面的模块和子包.

这个功能并不常用,一般用来扩展包里面的模块.

## 输入和输出

### 输出格式美化

Python两种输出值的方式: 表达式语句和print() 函数.

第三种方式是使用文件对象的write() 方法,标准输出文件可以用sys.stdout引用.

如果希望输出的形式更加多样,可以使用str.format() 函数来格式化输出值.

如果希望将输出的值转成字符串,可以使用repr() 或str() 函数来实现.

* str(): 函数返回一个用户易读的表达形式.
* repr(): 产生一个解释器易读的表达形式.


```py
s = 'Hello, Runoob'
str(s)
'Hello, Runoob'
repr(s)
"'Hello, Runoob'"
str(1/7)
'0.14285714285714285'
x = 10 * 3.25
y = 200 * 200
s = 'x的值为: ' + repr(x) + ',y的值为:' + repr(y) + '...'
print(s)
x的值为: 32.5,y的值为:40000...
#  repr() 函数可以转义字符串中的特殊字符
hello = 'hello, runoob\n'
hellos = repr(hello)
print(hellos)
'hello, runoob\n'
# repr() 的参数可以是Python的任何对象
repr((x, y, ('Google', 'Runoob')))
"(32.5, 40000, ('Google', 'Runoob'))"
```

这里有两种方式输出一个平方与立方的表:

```py
for x in range(1, 11):
print(repr(x).rjust(2), repr(x*x).rjust(3), end=' ')
# 注意前一行 'end' 的使用
print(repr(x*x*x).rjust(4))

111
248
3927
41664
525125
636216
749343
864512
981729
101001000

for x in range(1, 11):
print('{0:2d}{1:3d}{2:4d}'.format(x, x*x, x*x*x))

111
248
3927
41664
525125
636216
749343
864512
981729
101001000
```

在第一个例子中, 每列间的空格由print() 添加.


这个例子展示了字符串对象的rjust() 方法, 它可以将字符串靠右, 并在左边填充空格.

还有类似的方法, 如ljust() 和center().<br>
这些方法并不会写任何东西, 它们仅仅返回新的字符串.

另一个方法zfill(), 它会在数字的左边填充0,如下所示:

```py
'12'.zfill(5)
# => '00012'
'-3.14'.zfill(7)
# => '-003.14'
'3.14159265359'.zfill(5)
# => '3.14159265359'
```

str.format() 的基本使用如下:

```py
print('{}网址: "{}!"'.format('菜鸟教程', 'www.runoob.com'))
# => 菜鸟教程网址: "www.runoob.com!"
```

括号及其里面的字符 (称作格式化字段) 将会被format() 中的参数替换.

在括号中的数字用于指向传入对象在format() 中的位置,如下所示:

```py
print('{0}和{1}'.format('Google', 'Runoob'))
# =>Google和Runoob
print('{1}和{0}'.format('Google', 'Runoob'))
# =>Runoob和Google
```

如果在format() 中使用了关键字参数, 那么它们的值会指向使用该名字的参数.

```py
print('{name}网址: {site}'.format(name='菜鸟教程', site='www.runoob.com'))
# => 菜鸟教程网址: www.runoob.com
```

位置及关键字参数可以任意的结合:

```py
print('站点列表{0}, {1}, 和{other}.'.format('Google', 'Runoob', other='Taobao'))
# => 站点列表Google, Runoob, 和Taobao.
```

!a (使用ascii()), !s (使用str()) 和 !r (使用repr()) 可以用于在格式化某个值之前对其进行转化:

```py
import math
print('常量PI的值近似为: {}.'.format(math.pi))
# => 常量PI的值近似为: 3.141592653589793.
print('常量PI的值近似为: {!r}.'.format(math.pi))
# => 常量PI的值近似为: 3.141592653589793.
```

可选项 : 和格式标识符可以跟着字段名.<br>
这就允许对值进行更好的格式化.<br>
下面的例子将Pi保留到小数点后三位:

```py
import math
print('常量PI的值近似为{0:.3f}.'.format(math.pi))
# => 常量PI的值近似为3.142.
```

在 : 后传入一个整数, 可以保证该域至少有这么多的宽度.<br>
用于美化表格时很有用.

```py
table = {'Google': 1, 'Runoob': 2, 'Taobao': 3}
for name, number in table.items():
print('{0:10} ==> {1:10d}'.format(name, number))

# => Google     ==>          1
# => Runoob     ==>          2
# => Taobao     ==>          3
```

如果有一个很长的格式化字符串, 而不想将它们分开, 那么在格式化时通过变量名而非位置会是很好的事情.

最简单的就是传入一个字典, 然后使用方括号 [] 来访问键值:

```py
table = {'Google': 1, 'Runoob': 2, 'Taobao': 3}
print('Runoob: {0[Runoob]:d}; Google: {0[Google]:d}; Taobao: {0[Taobao]:d}'.format(table))
# => Runoob: 2; Google: 1; Taobao: 3
```

也可以通过在table变量前使用 ** 来实现相同的功能:

```py
table = {'Google': 1, 'Runoob': 2, 'Taobao': 3}
print('Runoob: {Runoob:d}; Google: {Google:d}; Taobao: {Taobao:d}'.format(**table))
# => Runoob: 2; Google: 1; Taobao: 3
```

### 旧式字符串格式化

% 操作符也可以实现字符串格式化.<br>
它将左边的参数作为类似sprintf() 式的格式化字符串, 而将右边的代入, 然后返回格式化后的字符串. 例如:

```py
import math
print('常量PI的值近似为:%5.3f.' % math.pi)
# => 常量PI的值近似为:3.142.
```

因为str.format() 是比较新的函数, 大多数的Python代码仍然使用 % 操作符.<br>
但是因为这种旧式的格式化最终会从该语言中移除, 应该更多的使用str.format().

### 读取键盘输入

Python提供了input() 内置函数从标准输入读入一行文本,默认的标准输入是键盘.

```py
#!/usr/bin/python3

str = input("请输入:");
print ("输入的内容是: ", str)
```

## File


### 读和写文件

open() 将会返回一个file对象,基本语法格式如下:

```py
open(filename, mode)
```

**使用open() 方法一定要保证关闭文件对象,即调用close() 方法.**

* filename:包含了要访问的文件名称的字符串值.
* mode:决定了打开文件的模式:只读,写入,追加等.<br>
所有可取值见如下的完全列表.<br>
这个参数是非强制的,默认文件访问模式为只读(r).

将字符串写入到文件foo.txt中:

```py
#!/usr/bin/python3

# 打开一个文件
f = open("/tmp/foo.txt", "w")

f.write( "Python是一个非常好的语言.\n是的,的确非常好!!\n" )

# 关闭打开的文件
f.close()
```

完整的语法格式为:

```py
open(file, mode='r', buffering=-1, encoding=None, errors=None, newline=None, closefd=True, opener=None)
```

参数说明:

0. file: 必需,文件路径(相对或者绝对路径).
0. mode: 可选,文件打开模式
0. buffering: 设置缓冲
0. encoding: 一般使用utf8
0. errors: 报错级别
0. newline: 区分换行符
0. closefd: 传入的file参数类型
0. opener: 设置自定义开启器,开启器的返回值必须是一个打开的文件描述符.




### 文件打开类型

模式|描述
-|-
t|文本模式 (默认).
x|写模式,新建一个文件,如果该文件已存在则会报错.
b|二进制模式.
+|打开一个文件进行更新(可读可写).
U|通用换行模式(Python3不支持).
r|以只读方式打开文件.<br>文件的指针将会放在文件的开头.<br>这是默认模式.
rb|以二进制格式打开一个文件用于只读.<br>文件指针将会放在文件的开头.<br>这是默认模式.<br>一般用于非文本文件如图片等.
r+|打开一个文件用于读写.<br>文件指针将会放在文件的开头.
rb+|以二进制格式打开一个文件用于读写.<br>文件指针将会放在文件的开头.<br>一般用于非文本文件如图片等.
w|打开一个文件只用于写入.<br>如果该文件已存在则打开文件,并从开头开始编辑,即原有内容会被删除.<br>如果该文件不存在,创建新文件.
wb|以二进制格式打开一个文件只用于写入.<br>如果该文件已存在则打开文件,并从开头开始编辑,即原有内容会被删除.<br>如果该文件不存在,创建新文件.<br>一般用于非文本文件如图片等.
w+|打开一个文件用于读写.<br>如果该文件已存在则打开文件,并从开头开始编辑,即原有内容会被删除.<br>如果该文件不存在,创建新文件.
wb+|以二进制格式打开一个文件用于读写.<br>如果该文件已存在则打开文件,并从开头开始编辑,即原有内容会被删除.<br>如果该文件不存在,创建新文件.<br>一般用于非文本文件如图片等.
a|打开一个文件用于追加.<br>如果该文件已存在,文件指针将会放在文件的结尾.<br>也就是说,新的内容将会被写入到已有内容之后.<br>如果该文件不存在,创建新文件进行写入.
ab|以二进制格式打开一个文件用于追加.<br>如果该文件已存在,文件指针将会放在文件的结尾.<br>也就是说,新的内容将会被写入到已有内容之后.<br>如果该文件不存在,创建新文件进行写入.
a+|打开一个文件用于读写.<br>如果该文件已存在,文件指针将会放在文件的结尾.<br>文件打开时会是追加模式.<br>如果该文件不存在,创建新文件用于读写.
ab+|以二进制格式打开一个文件用于追加.<br>如果该文件已存在,文件指针将会放在文件的结尾.<br>如果该文件不存在,创建新文件用于读写.

模式|r|r+|w|w+|a|a+
-|-|-|-|-|-|-
读|+|+||+||+
写||+|+|+|+|+
创建|||+|+|+|+
覆盖|||+|+||
指针在开始|+|+|+|+||
指针在结尾|||||+|+

### 文件对象的方法

```py

```


#### 读取文件内容(read())

为了读取一个文件的内容,调用f.read(size), 这将读取一定数目的数据, 然后作为字符串或字节对象返回.

size是一个可选的数字类型的参数.<br>
当size被忽略了或者为负, 那么该文件的所有内容都将被读取并且返回.

```py
#!/usr/bin/python3

# 打开一个文件
f = open("/tmp/foo.txt", "r")

str = f.read()
print(str)

# 关闭打开的文件
f.close()
```

#### 读取单独的一行(readline())

readline() 会从文件中读取单独的一行.<br>
换行符为 '\n'.<br>
readline() 如果返回一个空字符串, 说明已经已经读取到最后一行.

```py
#!/usr/bin/python3

# 打开一个文件
f = open("/tmp/foo.txt", "r")

str = f.readline()
print(str)

# 关闭打开的文件
f.close()
```

#### 读取所有行(readlines())

readlines() 将返回该文件中包含的所有行.<br>
如果设置可选参数sizehint, 则读取指定长度的字节, 并且将这些字节按行分割.

```py
#!/usr/bin/python3

# 打开一个文件
f = open("/tmp/foo.txt", "r")

str = f.readlines()
print(str)

# 关闭打开的文件
f.close()
```

#### 写入文件(write())

write(string) 将string写入到文件中, 然后返回写入的字符数.

```py
#!/usr/bin/python3

# 打开一个文件
f = open("/tmp/foo.txt", "w")

f.write( "Python是一个非常好的语言.\n是的,的确非常好!!\n" )
# 关闭打开的文件
f.close()
```

#### 当前位置(tell())

tell() 返回文件对象当前所处的位置, 它是从文件开头开始算起的字节数.

```py

```

### 改变当前的位置(seek())

如果要改变文件当前的位置, 可以使用f.seek(offset, from_what) 函数.

from_what的值, 如果是0表示开头, 如果是1表示当前位置,2表示文件的结尾,例如:

0. seek(x,0) : 从起始位置即文件首行首字符开始移动x个字符
0. seek(x,1) : 表示从当前位置往后移动x个字符
0. seek(-x,2):表示从文件的结尾往前移动x个字符


```py
f = open('/tmp/foo.txt', 'rb+')
f.write(b'0123456789abcdef')
# => 16
f.seek(5)     # 移动到文件的第六个字节
# => 5
f.read(1)
# => b'5'
f.seek(-3, 2) # 移动到文件的倒数第三字节
# => 13
f.read(1)
# => b'd'
```

### 释放资源(close())

在文本文件中 (那些打开文件的模式下没有b的), 只会相对于文件起始位置进行定位.<br>
当处理完一个文件后, 调用f.close() 来关闭文件并释放系统的资源,如果尝试再调用该文件,则会抛出异常.

当处理一个文件对象时, 使用with关键字是非常好的方式.<br>
在结束后, 它会帮正确的关闭文件.<br>
而且写起来也比try-finally语句块要简短:

```py
with open('/tmp/foo.txt', 'r') as f:
read_data = f.read()
f.closed
# => True
```

### pickle模块

python的pickle模块实现了基本的数据序列和反序列化.<br>
通过pickle模块的序列化操作能够将程序中运行的对象信息保存到文件中去,永久存储.<br>
通过pickle模块的反序列化操作,能够从文件中创建上一次程序保存的对象.

```py
pickle.dump(obj, file, [,protocol])
```

有了pickle这个对象, 就能对file以读取的形式打开:

```py
# 从file中读取一个字符串,并将它重构为原来的python对象.
x = pickle.load(file)
```

```py
#!/usr/bin/python3
import pickle

# 使用pickle模块将数据对象保存到文件
data1 = {'a': [1, 2.0, 3, 4+6j],
'b': ('string', u'Unicode string'),
'c': None}

selfref_list = [1, 2, 3]
selfref_list.append(selfref_list)

output = open('data.pkl', 'wb')

# Pickle dictionary using protocol 0.
pickle.dump(data1, output)

# Pickle the list using the highest protocol available.
pickle.dump(selfref_list, output, -1)

output.close()
```

```py
#!/usr/bin/python3
import pprint, pickle

# 使用pickle模块从文件中重构python对象
pkl_file = open('data.pkl', 'rb')

data1 = pickle.load(pkl_file)
pprint.pprint(data1)

data2 = pickle.load(pkl_file)
pprint.pprint(data2)

pkl_file.close()
```

#### 刷新文件内部缓冲(flush())

刷新文件内部缓冲,直接把内部缓冲区的数据立刻写入文件, 而不是被动的等待输出缓冲区写入.

```py

```

#### 获取文件描述符(fileno())

返回一个整型的文件描述符(file descriptorFD整型), 可以用在如os模块的read方法等一些底层操作上.

```py

```

#### 是否使用中(isatty())

如果文件连接到一个终端设备返回True,否则返回False.

```py

```

#### 字符截取(truncate([size]))

从文件的首行首字符开始截断,截断文件为size个字符,无size表示从当前位置截断;截断之后后面的所有字符被删除,其中windows系统下的换行代表2个字符大小.

```py

```

#### 写入序列字符串(writelines(sequence))

向文件写入一个序列字符串列表,如果需要换行则要自己加入每行的换行符.

```py

```

## 文件/目录方法(OS)

os模块提供了非常丰富的方法用来处理文件和目录.



### 检验权限模式

```py
os.access(path, mode)
```

### 改变当前工作目录

```py
os.chdir(path)
```

### 设置路径的标记为数字标记

```py
os.chflags(path, flags)
```

### 更改权限

```py
os.chmod(path, mode)
```

### 更改文件所有者

```py
os.chown(path, uid, gid)
```

### 改变当前进程的根目录

```py
os.chroot(path)
```

### 关闭文件描述符fd

```py
os.close(fd)
```

### 关闭所有文件描述符

关闭所有文件描述符,从fd_low(包含) 到fd_high(不包含), 错误会忽略

```py
os.closerange(fd_low, fd_high)
```

### 复制文件描述符fd

```py
os.dup(fd)
```

### 将一个文件描述符fd复制到另一个fd2

```py
os.dup2(fd, fd2)
```

### 通过文件描述符改变当前工作目录

```py
os.fchdir(fd)
```

### 改变一个文件的访问权限

改变一个文件的访问权限,该文件由参数fd指定,参数mode是Unix下的文件访问权限.

```py
os.fchmod(fd, mode)
```

### 修改一个文件的所有权

修改一个文件的所有权,这个函数修改一个文件的用户ID和用户组ID,该文件由文件描述符fd指定.

```py
os.fchown(fd, uid, gid)
```

### 强制将文件写入磁盘

强制将文件写入磁盘,该文件由文件描述符fd指定,但是不强制更新文件的状态信息.

```py
os.fdatasync(fd)
```

### 通过文件描述符fd创建一个文件对象

通过文件描述符fd创建一个文件对象,并返回这个文件对象

```py
os.fdopen(fd[, mode[, bufsize]])
```

### 返回一个打开的文件的系统配置信息

name为检索的系统配置的值,它也许是一个定义系统值的字符串,这些名字在很多标准中指定(POSIX.1, Unix 95, Unix 98, 和其它).

```py
os.fpathconf(fd, name)
```

### 返回文件描述符fd的状态

```py
os.fstat(fd)
```

### 返回包含文件描述符fd的文件的文件系统的信息

Python 3.3相等于statvfs().

```py
os.fstatvfs(fd)
```

### 强制将文件描述符为fd的文件写入硬盘

```py
os.fsync(fd)
```

### 裁剪文件描述符fd对应的文件

最大不能超过文件大小

```py
os.ftruncate(fd, length)
```

### 返回当前工作目录

```py
os.getcwd()
```

### 返回一个当前工作目录的Unicode对象

```py
os.getcwdb()
```

### 是否与设备连接

如果文件描述符fd是打开的,同时与tty(-like)设备相连,则返回true, 否则False.

```py
os.isatty(fd)
```

### 设置路径的标记为数字标记

类似chflags(),但是没有软链接

```py
os.lchflags(path, flags)
```

### 修改连接文件权限

```py
os.lchmod(path, mode)
```

### 更改文件所有者

类似chown,但是不追踪链接.

```py
os.lchown(path, uid, gid)
```

### 创建硬链接

名为参数dst,指向参数src

```py
os.link(src, dst)
```

### 名称列表

返回path指定的文件夹包含的文件或文件夹的名字的列表

```py
os.listdir(path)
```

### 设置文件描述符

fd当前位置为pos, how方式修改:SEEK_SET或者0设置从文件开始的计算的pos; SEEK_CUR或者1则从当前位置计算; os.SEEK_END或者2则从文件尾部开始. 在unix,Windows中有效.

```py
os.lseek(fd, pos, how)
```

### lstat

像stat(),但是没有软链接

```py
os.lstat(path)
```

### 从原始的设备号中提取设备major号码

使用stat中的st_dev或者st_rdev field

```py
os.major(device)
```

### 以major和minor设备号组成一个原始设备号

```py
os.makedev(major, minor)
```

### 递归文件夹创建函数

像mkdir(), 但创建的所有intermediate-level文件夹需要包含子文件夹.

```py
os.makedirs(path[, mode])
```

### 从原始的设备号中提取设备minor号码

使用stat中的st_dev或者st_rdev field

```py
os.minor(device)
```

### 以数字mode的mode创建一个名为path的文件夹

默认的mode是0777(八进制)

```py
os.mkdir(path[, mode])
```

### 创建命名管道

mode为数字,默认为0666(八进制)

```py
os.mkfifo(path[, mode])
```


### 打开一个文件

设置需要的打开选项,mode参数是可选的创建一个名为filename文件系统节点(文件,设备特别文件或者命名pipe).

```py
os.open(file, flags[, mode])os.mknod(filename[, mode=0600, device])
```

### 打开一个新的伪终端对

返回pty和tty的文件描述符.

```py
os.openpty()
```

### 返回相关文件的系统配置信息

```py
os.pathconf(path, name)
```

### 创建一个管道

返回一对文件描述符(r, w) 分别为读和写

```py
os.pipe()
```

### 从一个command打开一个管道

```py
os.popen(command[, mode[, bufsize]])
```

### 读取文件内容

从文件描述符fd中读取最多n个字节,返回包含读取字节的字符串,文件描述符fd对应文件已达到结尾, 返回一个空字符串.

```py
os.read(fd, n)
```

### 返回软链接所指向的文件

```py
os.readlink(path)
```

### 删除文件

删除路径为path的文件.<br>
如果path是一个文件夹,将抛出OSError; 查看下面的rmdir()删除一个directory.

```py
os.remove(path)
```

### 递归删除目录

```py
os.removedirs(path)
```

### 重命名文件或目录

从src到dst

```py
os.rename(src, dst)
```

### 递归地对目录进行更名

也可以对文件进行更名

```py
os.renames(old, new)
```

### 删除path指定的空目录

如果目录非空,则抛出一个OSError异常

```py
os.rmdir(path)
```

### 获取path指定的路径的信息

功能等同于C API中的stat()系统调用.

```py
os.stat(path)
```


### 获取指定路径的文件系统统计信息

获取指定路径的文件系统统计信息决定stat_result是否以float对象显示时间戳.

```py
os.statvfs(path)os.stat_float_times([newvalue])
```

### 创建一个软链接

```py
os.symlink(src, dst)
```

### tcgetpgrp

返回与终端fd(一个由os.open()返回的打开的文件描述符)关联的进程组

```py
os.tcgetpgrp(fd)
```

### tcsetpgrp

设置与终端fd(一个由os.open()返回的打开的文件描述符)关联的进程组为pg.

```py
os.tcsetpgrp(fd, pg)
```

### ttyname

返回一个字符串,它表示与文件描述符fd关联的终端设备.<br>
如果fd没有与终端设备关联,则引发一个异常.

```py
os.ttyname(fd)
```

### 删除文件路径

```py
os.unlink(path)
```

### 返回指定的path文件的访问和修改的时间

```py
os.utime(path, times)
```

### walk

输出在文件夹中的文件名通过在树中游走,向上或者向下.

```py
os.walk(top[, topdown=True[, onerror=None[, followlinks=False]]])
```

### 写入内容

写入字符串到文件描述符fd中. 返回实际写入的字符串长度

```py
os.write(fd, str)
```

### 获取文件的属性信息

```py
os.path模块
```

### 获取当前目录的父目录

以字符串形式显示目录名.

```py
os.pardir()
```

## 错误和异常

### try/except/else/finally

![](https://www.runoob.com/wp-content/uploads/2019/07/try_except.png)

try/except语句还有一个可选的else子句,如果使用这个子句,那么必须放在所有的except子句之后.

else子句将在try子句没有发生任何异常的时候执行.

![](https://www.runoob.com/wp-content/uploads/2019/07/try_except_else.png)

finally语句无论是否发生异常都将执行最后的代码.

![](https://www.runoob.com/wp-content/uploads/2019/07/try_except_else_finally.png)


### 抛出异常

raise唯一的一个参数指定了要被抛出的异常.<br>
它必须是一个异常的实例或者是异常的类(也就是Exception的子类).

```py
raise [Exception [, args [, traceback]]]
```

```py
x = 10
if x > 5:
raise Exception('x不能大于5.<br>
x的值为: {}'.format(x))
```

### 自定义异常

可以通过创建一个新的异常类来拥有自己的异常.<br>
异常类继承自Exception类,可以直接继承,或者间接继承,例如:

```py
class MyError(Exception):
def __init__(self, value):
self.value = value
def __str__(self):
return repr(self.value)

try:
raise MyError(2*2)
except MyError as e:
print('My exception occurred, value:', e.value)

# => My exception occurred, value: 4
```

### 预定义的清理行为

一些对象定义了标准的清理行为,无论系统是否成功的使用了它,一旦不需要它了,那么这个标准的清理行为就会执行.

关键词with语句就可以保证诸如文件之类的对象在使用完之后一定会正确的执行清理方法:

```py
with open("myfile.txt") as f:
for line in f:
print(line, end="")
```

以上这段代码执行完毕后,就算在处理过程中出问题了,文件f总是会关闭.

## 面向对象

### 类定义

```py
class ClassName:
<statement-1>
.
.
.
<statement-N>
```

### 类对象

类对象支持两种操作:属性引用和实例化.<br>
属性引用使用和Python中所有的属性引用一样的标准语法:obj.name.<br>
类对象创建后,类命名空间中所有的命名都是有效属性名.<br>
所以如果类定义是这样:

```py
#!/usr/bin/python3

class MyClass:
"""一个简单的类实例"""
i = 12345
def f(self):
return 'hello world'

# 实例化类
x = MyClass()

# 访问类的属性和方法
print("MyClass类的属性i为:", x.i)
print("MyClass类的方法f输出为:", x.f())
```

### 构造方法

类有一个名为__init__() 的特殊方法(构造方法),该方法在类实例化时会自动调用,像下面这样:

```py
def __init__(self):
self.data = []
```

__init__() 方法可以有参数,参数通过__init__() 传递到类的实例化操作上.例如:

```py
#!/usr/bin/python3

class Complex:
def __init__(self, realpart, imagpart):
self.r = realpart
self.i = imagpart
x = Complex(3.0, -4.5)
print(x.r, x.i)   # 输出结果:3.0 -4.5
```

### self

self代表类的实例,而非类.<br>
类的方法与普通的函数只有一个特别的区别——它们必须有一个 *额外的第一个参数名称* , 按照惯例它的名称是self.

```py
class Test:
def prt(self):
print(self)
print(self.__class__)

t = Test()
t.prt()
```

self不是python关键字,把换成runoob也是可以正常执行的:

```py
class Test:
def prt(runoob):
print(runoob)
print(runoob.__class__)

t = Test()
t.prt()
```

### 继承

```py
class DerivedClassName(BaseClassName):
<statement-1>
.
.
.
<statement-N>
```

子类(派生类DerivedClassName)会继承父类(基类BaseClassName)的属性和方法.<br>
BaseClassName(实例中的基类名)必须与派生类定义在一个作用域内.<br>
除了类,还可以用表达式,基类定义在另一个模块中时这一点非常有用:

```py
class DerivedClassName(modname.BaseClassName):
```

```py
#!/usr/bin/python3

#类定义
class people:
#定义基本属性
name = ''
age = 0
#定义私有属性,私有属性在类外部无法直接进行访问
__weight = 0
#定义构造方法
def __init__(self,n,a,w):
self.name = n
self.age = a
self.__weight = w
def speak(self):
print("%s说:  %d岁." %(self.name,self.age))

#单继承示例
class student(people):
grade = ''
def __init__(self,n,a,w,g):
#调用父类的构函
people.__init__(self,n,a,w)
self.grade = g
#覆写父类的方法
def speak(self):
print("%s说:  %d岁了,在读 %d年级"%(self.name,self.age,self.grade))



s = student('ken',10,60,3)
s.speak()
```

### 多继承

Python同样有限的支持多继承形式.<br>
多继承的类定义形如下例:

```py
class DerivedClassName(Base1, Base2, Base3):
<statement-1>
.
.
.
<statement-N>
```

需要注意圆括号中父类的顺序,若是父类中有相同的方法名,而在子类使用时未指定,python从左至右搜索即 **方法在子类中未找到时,从左到右查找父类中是否包含方法** .

```py
#!/usr/bin/python3

#类定义
class people:
#定义基本属性
name = ''
age = 0
#定义私有属性,私有属性在类外部无法直接进行访问
__weight = 0
#定义构造方法
def __init__(self,n,a,w):
self.name = n
self.age = a
self.__weight = w
def speak(self):
print("%s说:  %d岁." %(self.name,self.age))

#单继承示例
class student(people):
grade = ''
def __init__(self,n,a,w,g):
#调用父类的构函
people.__init__(self,n,a,w)
self.grade = g
#覆写父类的方法
def speak(self):
print("%s说:  %d岁了,在读 %d年级"%(self.name,self.age,self.grade))

#另一个类,多重继承之前的准备
class speaker():
topic = ''
name = ''
def __init__(self,n,t):
self.name = n
self.topic = t
def speak(self):
print("叫 %s,是一个演说家,演讲的主题是 %s"%(self.name,self.topic))

#多重继承
class sample(speaker,student):
a =''
def __init__(self,n,a,w,g,t):
student.__init__(self,n,a,w,g)
speaker.__init__(self,n,t)

test = sample("Tim",25,80,4,"Python")
test.speak()   #方法名同,默认调用的是在括号中排前地父类的方法
```

### 方法重写

如果父类方法的功能不能满足需求,可以在子类重写父类的方法,实例如下:

```py
#!/usr/bin/python3

class Parent:        # 定义父类
def myMethod(self):
print ('调用父类方法')

class Child(Parent): # 定义子类
def myMethod(self):
print ('调用子类方法')

c = Child()          # 子类实例
c.myMethod()         # 子类调用重写方法
super(Child,c).myMethod() #用子类对象调用父类已被覆盖的方法
```

super() 函数是用于调用父类(超类)的一个方法.


### 类属性与方法

#### 类的私有属性(__private_attrs)

两个下划线开头,声明该属性为私有,不能在类的外部被使用或直接访问.<br>
在类内部的方法中使用时self.__private_attrs.


#### 类的方法

在类的内部,使用def关键字来定义一个方法,与一般函数定义不同,类方法必须包含参数self,且为第一个参数,self代表的是类的实例.

self的名字并不是规定死的,也可以使用this,但是最好还是按照约定使用self.

#### 类的私有方法(__private_method)

两个下划线开头,声明该方法为私有方法,只能在类的内部调用 ,不能在类的外部调用.<br>
self.__private_methods.

#### 类的专有方法

方法名|含义
-|-|
__init__|构造函数,在生成对象时调用
__del__|析构函数,释放对象时使用
__repr__|打印,转换
__setitem__|按照索引赋值
__getitem__|按照索引获取值
__len__|获得长度
__cmp__|比较运算
__call__|函数调用
__add__|加运算
__sub__|减运算
__mul__|乘运算
__truediv__|除运算
__mod__|求余运算
__pow__|乘方

#### 运算符重载

Python同样支持运算符重载,可以对类的专有方法进行重载,实例如下:

```py
#!/usr/bin/python3

class Vector:
def __init__(self, a, b):
self.a = a
self.b = b

def __str__(self):
return 'Vector (%d, %d)' % (self.a, self.b)

def __add__(self,other):
return Vector(self.a + other.a, self.b + other.b)

v1 = Vector(2,10)
v2 = Vector(5,-2)
print (v1 + v2)
```

## 命名空间/作用域

### 命名空间

命名空间(Namespace)是从名称到对象的映射,大部分的命名空间都是通过Python字典来实现的.

命名空间提供了在项目中避免名字冲突的一种方法.<br>
各个命名空间是独立的,没有任何关系的,所以一个命名空间中不能有重名,<br>
但不同的命名空间是可以重名而没有任何影响.

举一个计算机系统中的例子,一个文件夹(目录)中可以包含多个文件夹,每个文件夹中不能有相同的文件名,但不同文件夹中的文件可以重名.

![](https://www.runoob.com/wp-content/uploads/2019/09/0129A8E9-30FE-431D-8C48-399EA4841E9D.jpg)

#### 三种命名空间

0. 内置名称(built-in names),Python语言内置的名称,比如函数名abs、char和异常名称BaseException、Exception等等.
0. 全局名称(global names),模块中定义的名称,记录了模块的变量,包括函数、类、其它导入的模块、模块级的变量和常量.
0. 局部名称(local names),函数中定义的名称,记录了函数的变量,包括函数的参数和局部定义的变量(类中定义的也是).

![](https://www.runoob.com/wp-content/uploads/2014/05/types_namespace-1.png)


#### 查找顺序

假设要使用变量runoob,则Python的查找顺序为:局部的命名空间去 -> 全局命名空间 -> 内置命名空间.<br>
如果找不到变量runoob,它将放弃查找并引发一个NameError异常.

#### 生命周期

命名空间的生命周期取决于对象的作用域,如果对象执行完成,则该命名空间的生命周期就结束.<br>
因此,无法从外部命名空间访问内部命名空间的对象.


```py
# var1是全局名称
var1 = 5
def some_func():

# var2是局部名称
var2 = 6
def some_inner_func():

# var3是内嵌的局部名称
var3 = 7
```

![](https://www.runoob.com/wp-content/uploads/2014/05/namespaces.png)

### 作用域

作用域就是一个Python程序可以直接访问命名空间的正文区域.<br>
在一个python程序中,直接访问一个变量,会从内到外依次访问所有的作用域直到找到,否则会报未定义的错误.<br>
Python中,程序的变量并不是在哪个位置都可以访问的,访问权限决定于这个变量是在哪里赋值的.

#### 四种作用域

变量的作用域决定了在哪一部分程序可以访问哪个特定的变量名称.<br>
Python的作用域一共有4种,分别是:

有四种作用域:

0. L(Local):最内层,包含局部变量,比如一个函数/方法内部.
0. E(Enclosing):包含了非局部(non-local)也非全局(non-global)的变量.<br>
比如两个嵌套函数,一个函数(或类)A里面又包含了一个函数B,那么对于B中的名称来说A中的作用域就为nonlocal.
0. G(Global):当前脚本的最外层,比如当前模块的全局变量.
0. B(Built-in): 包含了内建的变量/关键字等,最后被搜索.

规则顺序:L–>E–>G–> B.

在局部找不到,便会去局部外的局部找(例如闭包),再找不到就会去全局找,再者去内置中找.

![](https://www.runoob.com/wp-content/uploads/2014/05/1418490-20180906153626089-1835444372.png)

```py
g_count = 0  # 全局作用域
def outer():
o_count = 1  # 闭包函数外的函数中
def inner():
i_count = 2  # 局部作用域
```

内置作用域是通过一个名为builtin的标准模块来实现的,但是这个变量名自身并没有放入内置作用域内,所以必须导入这个文件才能够使用它.<br>
在Python3.0中,可以使用以下的代码来查看到底预定义了哪些变量:

```py
import builtins
dir(builtins)
```

Python中只有模块(module),类(class)以及函数(def、lambda)才会引入新的作用域,其它的代码块(如if/elif/else/、try/except、for/while等)是不会引入新的作用域的,也就是说这些语句内定义的变量,外部也可以访问,如下代码:

```py
if True:
msg = 'I am from Runoob'
msg
'I am from Runoob'
```

实例中msg变量定义在if语句块中,但外部还是可以访问的.

如果将msg定义在函数中,则它就是局部变量,外部不能访问:

```py
def test():
msg_inner = 'I am from Runoob'

msg_inner
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
NameError: name 'msg_inner' is not defined
```

### 全局变量和局部变量

定义在函数内部的变量拥有一个局部作用域,定义在函数外的拥有全局作用域.<br>
局部变量只能在其被声明的函数内部访问,而全局变量可以在整个程序范围内访问.<br>
调用函数时,所有在函数内声明的变量名称都将被加入到作用域中.<br>
如下实例:

```py
#!/usr/bin/python3

total = 0 # 这是一个全局变量
# 可写函数说明
def sum( arg1, arg2 ):
#返回2个参数的和."
total = arg1 + arg2 # total在这里是局部变量.
print ("函数内是局部变量 : ", total)
return total

#调用sum函数
sum( 10, 20 )
print ("函数外是全局变量 : ", total)

# => 函数内是局部变量 :  30
# => 函数外是全局变量 :  0
```

### global和nonlocal关键字

当内部作用域想修改外部作用域的变量时,就要用到global和nonlocal关键字了.

以下实例修改全局变量num:

```py
#!/usr/bin/python3

num = 1
def fun1():
global num  # 需要使用global关键字声明
print(num)
num = 123
print(num)
fun1()
print(num)

# => 1
# => 123
# => 123
```

如果要修改嵌套作用域(enclosing作用域,外层非全局作用域)中的变量则需要nonlocal关键字了,如下实例:

```py
#!/usr/bin/python3

def outer():
num = 10
def inner():
nonlocal num   # nonlocal关键字声明
num = 100
print(num)
inner()
print(num)
outer()

# => 100
# => 100
```

## 标准库概览

### 操作系统接口(os)

os模块提供了不少与操作系统相关联的函数.<br>
建议使用 "import os" 风格而非 "from os import *".<br>
这样可以保证随操作系统不同而有所变化的os.open() 不会覆盖内置函数open().

### 文件通配符(glob)

glob模块提供了一个函数用于从目录通配符搜索中生成文件列表:

```py
import glob
glob.glob('*.py')
# => ['primes.py', 'random.py', 'quote.py']
```

### sys

#### 命令行参数

通用工具脚本经常调用命令行参数.<br>
这些命令行参数以链表形式存储于sys模块的argv变量.<br>
例如在命令行中执行 "python demo.py one two three" 后可以得到以下输出结果:

```py
import sys
print(sys.argv)
# => ['demo.py', 'one', 'two', 'three']
```

#### 错误输出重定向

sys还有stdin,stdout和stderr属性,即使在stdout被重定向时,后者也可以用于显示警告和错误信息.

```py
sys.stderr.write('Warning, log file not found starting a new one\n')
# => Warning, log file not found starting a new one
```

#### 程序终止

大多脚本的定向终止都使用:

```py
sys.exit()
```

### 字符串正则匹配(re)

re模块为高级字符串处理提供了正则表达式工具.<br>
对于复杂的匹配和处理,正则表达式提供了简洁、优化的解决方案:

```py
import re
re.findall(r'\bf[a-z]*', 'which foot or hand fell fastest')
# => ['foot', 'fell', 'fastest']
re.sub(r'(\b[a-z]+) \1', r'\1', 'cat in the the hat')
# => 'cat in the hat'
```

如果只需要简单的功能,应该首先考虑字符串方法,因为它们非常简单,易于阅读和调试:

```py
'tea for too'.replace('too', 'two')
# => 'tea for two'
```


### 数学(math)

math模块为浮点运算提供了对底层C函数库的访问:

```py
import math
math.cos(math.pi / 4)
# => 0.70710678118654757
math.log(1024, 2)
# => 10.0
```

#### 随机数(random)

```py
import random
random.choice(['apple', 'pear', 'banana'])
# => 'apple'
random.sample(range(100), 10)   # sampling without replacement
[30, 83, 16, 4, 8, 81, 41, 50, 18, 33]
random.random()    # random float
# => 0.17970987693706186
random.randrange(6)    # random integer chosen from range(6)
# => 4
```

### 访问互联网

### Url处理(urllib)

```py
from urllib.request import urlopen
for line in urlopen('http://tycho.usno.navy.mil/cgi-bin/timer.pl'):
line = line.decode('utf-8')  # Decoding the binary data to text.
if 'EST' in line or 'EDT' in line:  # look for Eastern Time
print(line)
```

### 电子邮件处理(smtplib)

```py
# 需要本地有一个在运行的邮件服务器
import smtplib
server = smtplib.SMTP('localhost')
server.sendmail('soothsayer@example.org', 'jcaesar@example.org',
"""To: jcaesar@example.org
From: soothsayer@example.org

Beware the Ides of March.
""")
server.quit()
```

### 日期和时间(datetime)

datetime模块为日期和时间处理同时提供了简单和复杂的方法.<br>
支持日期和时间算法的同时,实现的重点放在更有效的处理和格式化输出.<br>
该模块还支持时区处理:

```py
# dates are easily constructed and formatted
from datetime import date
now = date.today()
datetime.date(2003, 12, 2)
now.strftime("%m-%d-%y. %d %b %Y is a %A on the %d day of %B.")
# => '12-02-03. 02 Dec 2003 is a Tuesday on the 02 day of December.'

# dates support calendar arithmetic
birthday = date(1964, 7, 31)
age = now - birthday
age.days
# => 14368

from datetime import datetime

datetime.now()
# => datetime.datetime(2021, 11, 16, 18, 56, 36, 340316)
```

### 数据压缩

以下模块直接支持通用的数据打包和压缩格式:zlib,gzip,bz2,zipfile,以及tarfile.

```py
import zlib
s = b'witch which has which witches wrist watch'
len(s)
# => 41
t = zlib.compress(s)
len(t)
# => 37
zlib.decompress(t)
b'witch which has which witches wrist watch'
zlib.crc32(s)
# => 226805979
```

### 性能度量

有些用户对了解解决同一问题的不同方法之间的性能差异很感兴趣.<br>
Python提供了一个度量工具,为这些问题提供了直接答案.

例如,使用元组封装和拆封来交换元素看起来要比使用传统的方法要诱人的多,timeit证明了现代的方法更快一些.

```py
from timeit import Timer
Timer('t=a; a=b; b=t', 'a=1; b=2').timeit()
# => 0.57535828626024577
Timer('a,b = b,a', 'a=1; b=2').timeit()
# => 0.54962537085770791
```

相对于timeit的细粒度,:mod:profile和pstats模块提供了针对更大代码块的时间度量工具.

### 测试模块

开发高质量软件的方法之一是为每一个函数开发测试代码,并且在开发过程中经常进行测试.<br>
doctest模块提供了一个工具,扫描模块并根据程序中内嵌的文档字符串执行测试.<br>
测试构造如同简单的将它的输出结果剪切并粘贴到文档字符串中.<br>
通过用户提供的例子,它强化了文档,允许doctest模块确认代码的结果是否与文档一致:

```py
def average(values):
"""Computes the arithmetic mean of a list of numbers.

print(average([20, 30, 70]))
40.0
"""
return sum(values) / len(values)

import doctest
doctest.testmod()   # 自动验证嵌入测试
```

unittest模块不像doctest模块那么容易使用,不过它可以在一个独立的文件里提供一个更全面的测试集:

```py
import unittest

class TestStatisticalFunctions(unittest.TestCase):

def test_average(self):
self.assertEqual(average([20, 30, 70]), 40.0)
self.assertEqual(round(average([1, 5, 7]), 1), 4.3)
self.assertRaises(ZeroDivisionError, average, [])
self.assertRaises(TypeError, average, 20, 30, 70)

unittest.main() # Calling from the command line invokes all tests
```

## 实例
### HelloWorld实例

[实例地址](www.runoob.com/python3/python3-helloworld.html)

### 数字求和

[实例地址](www.runoob.com/python3-add-number.html)

### 平方根

[实例地址](www.runoob.com/python3-square-root.html)

### 二次方程

[实例地址](www.runoob.com/python3-quadratic-roots.html)

### 计算三角形的面积

[实例地址](www.runoob.com/python3-area-triangle.html)

### 计算圆的面积

[实例地址](www.runoob.com/python3/python3-area-of-a-circle.html)

### 随机数生成

[实例地址](www.runoob.com/python3-random-number.html)

### 摄氏温度转华氏温度

[实例地址](www.runoob.com/python3-celsius-fahrenheit.html)


### 交换变量

[实例地址](www.runoob.com/python3-swap-variables.html)

### if语句

[实例地址](www.runoob.com/python3-if-example.html)

### 判断字符串是否为数字

[实例地址](www.runoob.com/python3-check-is-number.html)

### 判断奇数偶数

[实例地址](www.runoob.com/python3-odd-even.html)

### 判断闰年

[实例地址](www.runoob.com/python3-leap-year.html)

### 获取最大值函数

[实例地址](www.runoob.com/python3-largest-number.html)

### 质数判断

[实例地址](www.runoob.com/python3-prime-number.html)

### 输出指定范围内的素数

[实例地址](www.runoob.com/python3-prime-number-intervals.html)

### 阶乘实例

[实例地址](www.runoob.com/python3-factorial.html)

### 九九乘法表

[实例地址](www.runoob.com/python3-99-table.html)

### 斐波那契数列

[实例地址](www.runoob.com/python3-fibonacci-sequence.html)

### 阿姆斯特朗数

[实例地址](www.runoob.com/python3-armstrong-number.html)

### 十进制转二进制、八进制、十六进制

[实例地址](www.runoob.com/python3-conversion-binary-octal-hexadecimal.html)

### ASCII码与字符相互转换

[实例地址](www.runoob.com/python3-ascii-character.html)

### 最大公约数算法

[实例地址](www.runoob.com/python3-hcf.html)

### 最小公倍数算法

[实例地址](www.runoob.com/python3-lcm.html)

### 简单计算器实现

[实例地址](www.runoob.com/python3-calculator.html)

### 生成日历

[实例地址](www.runoob.com/python3-calendar.html)

### 使用递归斐波那契数列

[实例地址](www.runoob.com/python3-fibonacci-recursion.html)

### 文件IO

[实例地址](www.runoob.com/python3-file-io.html)

### 字符串判断

[实例地址](www.runoob.com/python3-check-string.html)

### 字符串大小写转换

[实例地址](www.runoob.com/python3-upper-lower.html)

### 计算每个月天数

[实例地址](www.runoob.com/python3-month-days.html)

### 获取昨天日期

[实例地址](www.runoob.com/python3-get-yesterday.html)

### list常用操作

[实例地址](www.runoob.com/python3-list-operator.html)

### 约瑟夫生者死者小游戏

[实例地址](www.runoob.com/python3/python-joseph-life-dead-game.html)

### 五人分鱼

[实例地址](www.runoob.com/python3/python-five-fish.html)


### 实现秒表功能

[实例地址](www.runoob.com/python3/python-simplestopwatch.html)


### 计算n个自然数的立方和

[实例地址](www.runoob.com/python3/python-cube-sum.html)


### 计算数组元素之和

[实例地址](www.runoob.com/python3/python3-sum-array.html)


### 数组翻转指定个数的元素

[实例地址](www.runoob.com/python3/python3-array-rotation.html)


### 将列表中的头尾两个元素对调

[实例地址](www.runoob.com/python3/python-list-interchange.html)


### 将列表中的指定位置的两个元素对调

[实例地址](www.runoob.com/python3/python3-list-swap-two-elements.html)

### 翻转列表

[实例地址](www.runoob.com/python3/python-reversing-list.html)


### 判断元素是否在列表中存在

[实例地址](www.runoob.com/python3/python-check-element-exists-in-list.html)


### 清空列表

[实例地址](www.runoob.com/python3/python-clear-list.html)


### 复制列表</a>

[实例地址](www.runoob.com/python3/python-copy-list.html)
### 计算元素在列表中出现的次数

[实例地址](www.runoob.com/python3/python-count-occurrences-element-list.html)

### 计算列表元素之和

[实例地址](www.runoob.com/python3/python-sum-list.html)

### 计算列表元素之积

[实例地址](www.runoob.com/python3/python-multiply-list.html)

### 查找列表中最小元素

[实例地址](www.runoob.com/python3/python-min-list-element.html)

### 查找列表中最大元素

[实例地址](www.runoob.com/python3/python-max-list-element.html)

### 移除字符串中的指定位置字符

[实例地址](www.runoob.com/python3/pyhton-remove-ith-character-from-string.html)

### 判断字符串是否存在子字符串

[实例地址](www.runoob.com/python3/python-check-substring-present-given-string.html)

### 判断字符串长度

[实例地址](www.runoob.com/python3/python-string-length.html)

### 使用正则表达式提取字符串中的URL

[实例地址](www.runoob.com/python3/python-find-url-string.html)

### 将字符串作为代码执行

[实例地址](www.runoob.com/python3/python-execute-string-code.html)

### 字符串翻转

[实例地址](www.runoob.com/python3/python-string-reverse.html)

### 对字符串切片及翻转

[实例地址](www.runoob.com/python3/python-slicing-rotate-string.html)

### 按键(key)或值(value)对字典进行排序

[实例地址](www.runoob.com/python3/python-sort-dictionaries-by-key-or-value.html)

### 计算字典值之和

[实例地址](www.runoob.com/python3/python-sum-dictionary.html)

### 移除字典点键值(key/value)对

[实例地址](www.runoob.com/python3/python-remove-a-key-from-dictionary.html)

### 合并字典

[实例地址](www.runoob.com/python3/python-merging-two-dictionaries.html)

### 将字符串的时间转换为时间戳

[实例地址](www.runoob.com/python3/python-str-timestamp.html)

### 获取几天前的时间

[实例地址](www.runoob.com/python3/python-get-dayago.html)

### 将时间戳转换为指定格式日期

[实例地址](www.runoob.com/python3/python-timstamp-str.html)

### 打印自己设计的字体

[实例地址](www.runoob.com/python3/python-your-font.html)

### 二分查找

[实例地址](www.runoob.com/python3/python-binary-search.html)

### 线性查找

[实例地址](www.runoob.com/python3/python-linear-search.html)

### 插入排序

[实例地址](www.runoob.com/python3/python-insertion-sort.html)

### 快速排序

[实例地址](www.runoob.com/python3/python-quicksort.html)

### 选择排序

[实例地址](www.runoob.com/python3/python-selection-sort.html)

### 冒泡排序

[实例地址](www.runoob.com/python3/python-bubble-sort.html)

### 归并排序

[实例地址](www.runoob.com/python3/python-merge-sort.html)

### 堆排序

[实例地址](www.runoob.com/python3/python-heap-sort.html)

### 计数排序

[实例地址](www.runoob.com/python3/python-counting-sort.html)

### 希尔排序

[实例地址](www.runoob.com/python3/python-shellsort.html)

### 拓扑排序

[实例地址](www.runoob.com/python3/python-topological-sorting.html)


# 高级教程

## 正则表达式

Python自1.5版本起增加了re模块,它提供Perl风格的正则表达式模式.<br>
re模块使Python语言拥有全部的正则表达式功能.<br>
compile函数根据一个模式字符串和可选的标志参数生成一个正则表达式对象.<br>
该对象拥有一系列方法用于正则表达式匹配和替换.<br>
re模块也提供了与这些方法功能完全一致的函数,这些函数使用一个模式字符串做为它们的第一个参数.

### 正则表达式修饰符-可选标志(flags)

正则表达式可以包含一些可选标志修饰符来控制匹配的模式.<br>
修饰符被指定为一个可选的标志.<br>
多个标志可以通过按位OR(|) 它们来指定.<br>
如re.I|re.M被设置成I和M标志:

修饰符|描述
-|-|
re.I|使匹配对大小写不敏感
re.L|做本地化识别(locale-aware)匹配
re.M|多行匹配,影响 ^ 和 $
re.S|使 . 匹配包括换行在内的所有字符
re.U|根据Unicode字符集解析字符.<br>这个标志影响 \w, \W, \b, \B.
re.X|该标志通过给予更灵活的格式以便将正则表达式写得更易于理解.

### match函数

re.match尝试从字符串的起始位置匹配一个模式,如果不是起始位置匹配成功的话,match()就返回none.


```py
re.match(pattern, string, flags=0)
```


参数|描述
-|-|
pattern|匹配的正则表达式
string|要匹配的字符串.
flags|标志位,用于控制正则表达式的匹配方式,如:是否区分大小写,多行匹配等等.参见:[正则表达式修饰符 - 可选标志](https://www.runoob.com/python3/python3-reg-expressions.html#flags)

匹配成功re.match方法返回一个匹配的对象,否则返回None.<br>
可以使用group(num) 或groups() 匹配对象函数来获取匹配表达式.

```py
#!/usr/bin/python

import re
print(re.match('www', 'www.runoob.com').span())  # 在起始位置匹配
# => (0, 3)
print(re.match('com', 'www.runoob.com'))         # 不在起始位置匹配
# => None
```

匹配对象方法|描述
-|-
group(num=0)|匹配的整个表达式的字符串,group() 可以一次输入多个组号,在这种情况下它将返回一个包含那些组所对应值的元组.
groups()|返回一个包含所有小组字符串的元组,从1到所含的小组号.

```py
#!/usr/bin/python3
import re

line = "Cats are smarter than dogs"
# .* 表示任意匹配除换行符(\n、\r)之外的任何单个或多个字符
# (.*?) 表示"非贪婪"模式,只保存第一个匹配到的子串
matchObj = re.match( r'(.*) are (.*?) .*', line, re.M|re.I)

if matchObj:
print ("matchObj.group() : ", matchObj.group())
print ("matchObj.group(1) : ", matchObj.group(1))
print ("matchObj.group(2) : ", matchObj.group(2))
else:
print ("No match!!")

# => matchObj.group() :  Cats are smarter than dogs
# => matchObj.group(1) :  Cats
# => matchObj.group(2) :  smarter
```

### search方法

re.search扫描整个字符串并返回第一个成功的匹配.


```py
re.search(pattern, string, flags=0)
```

```py
#!/usr/bin/python3

import re

print(re.search('www', 'www.runoob.com').span())  # 在起始位置匹配
# => (0, 3)
print(re.search('com', 'www.runoob.com').span())         # 不在起始位置匹配
# => (11, 14)
```

### match与search的区别

re.match只匹配字符串的开始,如果字符串开始不符合正则表达式,则匹配失败,函数返回None,而re.search匹配整个字符串,直到找到一个匹配.

### 检索和替换

```py
re.sub(pattern, repl, string, count=0, flags=0)
```

前三个为必选参数,后两个为可选参数.

0. pattern : 正则中的模式字符串.
0. repl : 替换的字符串,也可为一个函数.
0. string : 要被查找替换的原始字符串.
0. count : 模式匹配后替换的最大次数,默认0表示替换所有的匹配.
0. flags : 编译时用的匹配模式,数字形式.

```py
#!/usr/bin/python3
import re

phone = "2004-959-559 # 这是一个电话号码"

# 删除注释
num = re.sub(r'#.*$', "", phone)
print ("电话号码 : ", num)

# 移除非数字的内容
num = re.sub(r'\D', "", phone)
print ("电话号码 : ", num)
```

### repl参数是一个函数

```py
#!/usr/bin/python

import re

# 将匹配的数字乘于2
def double(matched):
value = int(matched.group('value'))
return str(value * 2)

s = 'A23G4HFD567'
print(re.sub('(?P<value>\d+)', double, s))

# => A46G8HFD1134
```

### compile函数

compile函数用于编译正则表达式,生成一个正则表达式( Pattern )对象,供match() 和search() 这两个函数使用.

```py
re.compile(pattern[, flags])
```

```py
import re
pattern = re.compile(r'([a-z]+) ([a-z]+)', re.I)   # re.I表示忽略大小写
m = pattern.match('Hello World Wide Web')
print( m )                            # 匹配成功,返回一个Match对象
# => <_sre.SRE_Match object at 0x10bea83e8>
m.group(0)                            # 返回匹配成功的整个子串
# => 'Hello World'
m.span(0)                             # 返回匹配成功的整个子串的索引
# => (0, 11)
m.group(1)                            # 返回第一个分组匹配成功的子串
# => 'Hello'
m.span(1)                             # 返回第一个分组匹配成功的子串的索引
# => (0, 5)
m.group(2)                            # 返回第二个分组匹配成功的子串
# => 'World'
m.span(2)                             # 返回第二个分组匹配成功的子串索引
# => (6, 11)
m.groups()                            # 等价于 (m.group(1), m.group(2), ...)
# => ('Hello', 'World')
```

### findall

**match和search是匹配一次findall匹配所有.**

在字符串中找到正则表达式所匹配的所有子串,并返回一个列表,如果没有找到匹配的,则返回空列表.

```py
re.findall(pattern, string, flags=0)
pattern.findall(string[, pos[, endpos]])
```

0. pattern: 匹配模式.
0. string: 待匹配的字符串.
0. pos: 可选参数,指定字符串的起始位置,默认为0.
0. endpos: 可选参数,指定字符串的结束位置,默认为字符串的长度.

```py
import re

result1 = re.findall(r'\d+','runoob 123 google 456')

pattern = re.compile(r'\d+')   # 查找数字
result2 = pattern.findall('runoob 123 google 456')
result3 = pattern.findall('run88oob123google456', 0, 10)

print(result1)
# => ['123', '456']
print(result2)
# => ['123', '456']
print(result3)
# => ['88', '12']
```

### finditer

和findall类似,在字符串中找到正则表达式所匹配的所有子串,并把它们作为一个迭代器返回.

```py
re.finditer(pattern, string, flags=0)
```

```py
import re

it = re.finditer(r"\d+","12a32bc43jf3")
for match in it:
print (match.group() )
```

### split

split方法按照能够匹配的子串将字符串分割后返回列表,它的使用形式如下:

```py
re.split(pattern, string[, maxsplit=0, flags=0])
```

* maxsplit: 分割次数,maxsplit=1分割一次,默认为0,不限制次数.

```py
import re
re.split('\W+', 'runoob, runoob, runoob.')
# => ['runoob', 'runoob', 'runoob', '']
re.split('(\W+)', ' runoob, runoob, runoob.')
# => ['', ' ', 'runoob', ', ', 'runoob', ', ', 'runoob', '.', '']
re.split('\W+', ' runoob, runoob, runoob.', 1)
# => ['', 'runoob, runoob, runoob.']

re.split('a*', 'hello world')   # 对于一个找不到匹配的字符串而言,split不会对其作出分割
# => ['hello world']
```

## CGI编程

CGI目前由NCSA维护,NCSA定义CGI如下:

> CGI(Common Gateway Interface),通用网关接口,它是一段程序,运行在服务器上如:HTTP服务器,提供同客户端HTML页面的接口.

为了更好的了解CGI是如何工作的,可以从在网页上点击一个链接或URL的流程:

0. 使用浏览器访问URL并连接到HTTPweb服务器.
0. Web服务器接收到请求信息后会解析URL,并查找访问的文件在服务器上是否存在,如果存在返回文件的内容,否则返回错误信息.
0. 浏览器从服务器上接收信息,并显示接收的文件或者错误信息.

CGI程序可以是Python脚本,PERL脚本,SHELL脚本,C或者C++程序等.

![](https://www.runoob.com/wp-content/uploads/2013/11/Cgi01.png)


## MySQL(mysql-connector)

> mysql-connector是MySQL官方提供的驱动器.

可以使用pip命令来安装mysql-connector:

```py
python -m pip install mysql-connector
```

导入模块:

```py
import mysql.connector
```

### 创建数据库连接

```py
import mysql.connector

mydb = mysql.connector.connect(
host="localhost",       # 数据库主机地址
user="yourusername",    # 数据库用户名
passwd="yourpassword"   # 数据库密码
database="runoob_db"    # 连接数据库名称(可选)
)
```

### 执行sql语句(execute)

```py
import mysql.connector

mydb = mysql.connector.connect(
host="localhost",
user="root",
passwd="123456"
)

mycursor = mydb.cursor()

mycursor.execute("CREATE DATABASE runoob_db")
```

### 插入数据

```py
import mysql.connector

mydb = mysql.connector.connect(
host="localhost",
user="root",
passwd="123456",
database="runoob_db"
)
mycursor = mydb.cursor()

sql = "INSERT INTO sites (name, url) VALUES (%s, %s)"
val = ("RUNOOB", "https://www.runoob.com")
mycursor.execute(sql, val)

mydb.commit()    # 数据表内容有更新,必须使用到该语句
```

### 批量插入

批量插入使用executemany() 方法,该方法的第二个参数是一个元组列表,包含了要插入的数据:

```py
import mysql.connector

mydb = mysql.connector.connect(
host="localhost",
user="root",
passwd="123456",
database="runoob_db"
)
mycursor = mydb.cursor()

sql = "INSERT INTO sites (name, url) VALUES (%s, %s)"
val = [
('Google', 'https://www.google.com'),
('Github', 'https://www.github.com'),
('Taobao', 'https://www.taobao.com'),
('stackoverflow', 'https://www.stackoverflow.com/')
]

mycursor.executemany(sql, val)

mydb.commit()    # 数据表内容有更新,必须使用到该语句
```

如果想在数据记录插入后,获取该记录的ID,可以使用以下代码:

```py
# ...
mydb.commit()

print("1条记录已插入, ID:", mycursor.lastrowid)
```

### 查询数据

```py
import mysql.connector

mydb = mysql.connector.connect(
host="localhost",
user="root",
passwd="123456",
database="runoob_db"
)
mycursor = mydb.cursor()

mycursor.execute("SELECT * FROM sites")

myresult = mycursor.fetchall()     # fetchall() 获取所有记录

# myresult = mycursor.fetchone() # 如果只想读取一条数据,可以使用fetchone() 方法

for x in myresult:
print(x)
```

## MySQL(PyMySQL)

PyMySQL是在Python3.x版本中用于连接MySQL服务器的一个库,Python2中则使用mysqldb.<br>
PyMySQL遵循Python数据库APIv2.0规范,并包含了pure-PythonMySQL客户端库.


## 网络编程

Python提供了两个级别访问的网络服务:

0. 低级别的网络服务支持基本的Socket,它提供了标准的BSDSockets API,可以访问底层操作系统Socket接口的全部方法.
0. 高级别的网络服务模块SocketServer, 它提供了服务器中心类,可以简化网络服务器的开发.

## SMTP发送邮件

SMTP(Simple Mail Transfer Protocol)即简单邮件传输协议,它是一组用于由源地址到目的地址传送邮件的规则,由它来控制信件的中转方式.<br>
python的smtplib提供了一种很方便的途径发送电子邮件.<br>
它对smtp协议进行了简单的封装.

Python创建SMTP对象语法如下:

```py
import smtplib

smtpObj = smtplib.SMTP( [host [, port [, local_hostname]]] )
```

0. host:SMTP服务器主机.<br>
可以指定主机的ip地址或者域名如:runoob.com,这个是可选参数.
0. port: 如果提供了host参数, 需要指定SMTP服务使用的端口号,一般情况下SMTP端口号为25.
0. local_hostname: 如果SMTP在本机上,只需要指定服务器地址为localhost即可.



Python SMTP对象使用sendmail方法发送邮件,语法如下:

```py
SMTP.sendmail(from_addr, to_addrs, msg[, mail_options, rcpt_options]
```

0. from_addr: 邮件发送者地址.
0. to_addrs: 字符串列表,邮件发送地址.
0. msg: 发送消息

这里要注意一下第三个参数,msg是字符串,表示邮件.<br>
知道邮件一般由标题,发信人,收件人,邮件内容,附件等构成,发送邮件的时候,要注意msg的格式.<br>
这个格式就是smtp协议中定义的格式.


```py
#!/usr/bin/python3

import smtplib
from email.mime.text import MIMEText
from email.header import Header

# 第三方SMTP服务
mail_host="smtp.XXX.com"  #设置服务器
mail_user="XXXX"    #用户名
mail_pass="XXXXXX"   #口令


sender = 'from@runoob.com'
receivers = ['429240967@qq.com']  # 接收邮件,可设置为QQ邮箱或者其邮箱

message = MIMEText('Python邮件发送测试...', 'plain', 'utf-8')
message['From'] = Header("菜鸟教程", 'utf-8')
message['To'] =  Header("测试", 'utf-8')

subject = 'PythonSMTP邮件测试'
message['Subject'] = Header(subject, 'utf-8')


try:
smtpObj = smtplib.SMTP()
smtpObj.connect(mail_host, 25)    # 25为SMTP端口号
smtpObj.login(mail_user,mail_pass)
smtpObj.sendmail(sender, receivers, message.as_string())
print ("邮件发送成功")
except smtplib.SMTPException:
print ("Error: 无法发送邮件")
```

使用三个引号来设置邮件信息,标准邮件需要三个头部信息: From, To, 和Subject,每个信息直接使用空行分割.

通过实例化smtplib模块的SMTP对象smtpObj来连接到SMTP访问,并使用sendmail方法来发送信息.

## 多线程

多线程类似于同时执行多个不同程序,多线程运行有如下优点:

0. 使用线程可以把占据长时间的程序中的任务放到后台去处理.
0. 用户界面可以更加吸引人,比如用户点击了一个按钮去触发某些事件的处理,可以弹出一个进度条来显示处理的进度.
0. 程序的运行速度可能加快.
0. 在一些等待的任务实现上如用户输入、文件读写和网络收发数据等,线程就比较有用了.<br>
在这种情况下可以释放一些珍贵的资源如内存占用等等.

每个独立的线程有一个程序运行的入口、顺序执行序列和程序的出口.<br>
但是线程不能够独立执行,必须依存在应用程序中,由应用程序提供多个线程执行控制.

每个线程都有自己的一组CPU寄存器,称为线程的上下文,<br>
该上下文反映了线程上次运行该线程的CPU寄存器的状态.

指令指针和堆栈指针寄存器是线程上下文中两个最重要的寄存器,线程总是在进程得到上下文中运行的,这些地址都用于标志拥有线程的进程地址空间中的内存.

线程可以被抢占(中断).

在其线程正在运行时,线程可以暂时搁置(也称为睡眠) -- 这就是线程的退让.

线程可以分为:

0. 内核线程:由操作系统内核创建和撤销.
0. 用户线程:不需要内核支持而在用户程序中实现的线程.

Python3线程中常用的两个模块为:

0. _thread
0. threading(推荐使用)

thread模块已被废弃.用户可以使用threading模块代替.<br>
所以,在Python3中不能再使用"thread" 模块.<br>
为了兼容性,Python3将thread重命名为 "_thread".

### 使用线程方式

Python中使用线程有两种方式:函数或者用类来包装线程对象.

#### 函数式

调用_thread模块中的start_new_thread()函数来产生新线程.语法如下:

```py
_thread.start_new_thread ( function, args[, kwargs] )
```

0. function: 线程函数.
0. args: 传递给线程函数的参数,必须是个tuple类型.
0. kwargs: 可选参数.

```py
#!/usr/bin/python3

import _thread
import time

# 为线程定义一个函数
def print_time( threadName, delay):
count = 0
while count < 5:
time.sleep(delay)
count += 1
print ("%s: %s" % ( threadName, time.ctime(time.time()) ))

# 创建两个线程
try:
_thread.start_new_thread( print_time, ("Thread-1", 2, ) )
_thread.start_new_thread( print_time, ("Thread-2", 4, ) )
except:
print ("Error: 无法启动线程")

while 1:
pass

# => Thread-1: Wed Apr  611:36:312016
# => Thread-1: Wed Apr  611:36:332016
# => Thread-2: Wed Apr  611:36:332016
# => Thread-1: Wed Apr  611:36:352016
# => Thread-1: Wed Apr  611:36:372016
# => Thread-2: Wed Apr  611:36:372016
# => Thread-1: Wed Apr  611:36:392016
# => Thread-2: Wed Apr  611:36:412016
# => Thread-2: Wed Apr  611:36:452016
# => Thread-2: Wed Apr  611:36:492016
```

#### 线程模块

_thread提供了低级别的、原始的线程以及一个简单的锁,它相比于threading模块的功能还是比较有限的.

threading模块除了包含_thread模块中的所有方法外,还提供的其方法:

0. threading.currentThread(): 返回当前的线程变量.
0. threading.enumerate(): 返回一个包含正在运行的线程的list.<br>
正在运行指线程启动后、结束前,不包括启动前和终止后的线程.
0. threading.activeCount(): 返回正在运行的线程数量,与len(threading.enumerate())有相同的结果.

除了使用方法外,线程模块同样提供了Thread类来处理线程,Thread类提供了以下方法:

0. run(): 用以表示线程活动的方法.
0. start():启动线程活动.
0. join([time]): 等待至线程中止.<br>
这阻塞调用线程直至线程的join() 方法被调用中止-正常退出或者抛出未处理的异常-或者是可选的超时发生.
0. isAlive(): 返回线程是否活动的.
0. getName(): 返回线程名.
0. setName(): 设置线程名.

##### 使用threading模块创建线程

可以通过直接从threading.Thread继承创建一个新的子类,并实例化后调用start() 方法启动新线程,即它调用了线程的run() 方法:

```py
#!/usr/bin/python3

import threading
import time

exitFlag = 0

class myThread (threading.Thread):
def __init__(self, threadID, name, counter):
threading.Thread.__init__(self)
self.threadID = threadID
self.name = name
self.counter = counter
def run(self):
print ("开始线程:" + self.name)
print_time(self.name, self.counter, 5)
print ("退出线程:" + self.name)

def print_time(threadName, delay, counter):
while counter:
if exitFlag:
threadName.exit()
time.sleep(delay)
print ("%s: %s" % (threadName, time.ctime(time.time())))
counter -= 1

# 创建新线程
thread1 = myThread(1, "Thread-1", 1)
thread2 = myThread(2, "Thread-2", 2)

# 开启新线程
thread1.start()
thread2.start()
thread1.join()
thread2.join()
print ("退出主线程")

# => 开始线程:Thread-1
# => 开始线程:Thread-2
# => Thread-1: Wed Apr  611:46:462016
# => Thread-1: Wed Apr  611:46:472016
# => Thread-2: Wed Apr  611:46:472016
# => Thread-1: Wed Apr  611:46:482016
# => Thread-1: Wed Apr  611:46:492016
# => Thread-2: Wed Apr  611:46:492016
# => Thread-1: Wed Apr  611:46:502016
# => 退出线程:Thread-1
# => Thread-2: Wed Apr  611:46:512016
# => Thread-2: Wed Apr  611:46:532016
# => Thread-2: Wed Apr  611:46:552016
# => 退出线程:Thread-2
# => 退出主线程
```

##### 线程同步

如果多个线程共同对某个数据修改,则可能出现不可预料的结果,为了保证数据的正确性,需要对多个线程进行同步.

使用Thread对象的Lock和Rlock可以实现简单的线程同步,这两个对象都有acquire方法和release方法,<br>
对于那些需要每次只允许一个线程操作的数据,可以将其操作放到acquire和release方法之间.

多线程的优势在于可以同时运行多个任务(至少感觉起来是这样).<br>
但是当线程需要共享数据时,可能存在数据不同步的问题.

考虑这样一种情况:一个列表里所有元素都是0,线程"set"从后向前把所有元素改成1,而线程"print"负责从前往后读取列表并打印.
那么,可能线程"set"开始改的时候,线程"print"便来打印列表了,输出就成了一半0一半1,这就是数据的不同步.

为了避免这种情况,引入了锁的概念.<br>锁有两种状态——锁定和未锁定.<br>
每当一个线程比如"set"要访问共享数据时,必须先获得锁定;如果已经有别的线程比如"print"获得锁定了,那么就让线程"set"暂停,也就是同步阻塞;<br>
等到线程"print"访问完毕,释放锁以后,再让线程"set"继续.

经过这样的处理,打印列表时要么全部输出0,要么全部输出1,不会再出现一半0一半1的尴尬场面.

```py
#!/usr/bin/python3

import threading
import time

class myThread (threading.Thread):
def __init__(self, threadID, name, counter):
threading.Thread.__init__(self)
self.threadID = threadID
self.name = name
self.counter = counter
def run(self):
print ("开启线程: " + self.name)
# 获取锁,用于线程同步
threadLock.acquire()
print_time(self.name, self.counter, 3)
# 释放锁,开启下一个线程
threadLock.release()

def print_time(threadName, delay, counter):
while counter:
time.sleep(delay)
print ("%s: %s" % (threadName, time.ctime(time.time())))
counter -= 1

threadLock = threading.Lock()
threads = []

# 创建新线程
thread1 = myThread(1, "Thread-1", 1)
thread2 = myThread(2, "Thread-2", 2)

# 开启新线程
thread1.start()
thread2.start()

# 添加线程到线程列表
threads.append(thread1)
threads.append(thread2)

# 等待所有线程完成
for t in threads:
t.join()
print ("退出主线程")

# => 开启线程: Thread-1
# => 开启线程: Thread-2
# => Thread-1: Wed Apr  611:52:572016
# => Thread-1: Wed Apr  611:52:582016
# => Thread-1: Wed Apr  611:52:592016
# => Thread-2: Wed Apr  611:53:012016
# => Thread-2: Wed Apr  611:53:032016
# => Thread-2: Wed Apr  611:53:052016
# => 退出主线程
```

### 线程优先级队列(Queue)

Python的Queue模块中提供了同步的、线程安全的队列类,包括FIFO(先入先出)队列Queue,LIFO(后入先出)队列LifoQueue,和优先级队列PriorityQueue.<br>


这些队列都实现了锁原语,能够在多线程中直接使用,可以使用队列来实现线程间的同步.<br>


Queue模块中的常用方法:

0. Queue.qsize() 返回队列的大小
0. Queue.empty() 如果队列为空,返回True,反之False
0. Queue.full() 如果队列满了,返回True,反之False
0. Queue.full与maxsize大小对应
0. Queue.get([block[, timeout]])获取队列,timeout等待时间
0. Queue.get_nowait() 相当Queue.get(False)
0. Queue.put(item) 写入队列,timeout等待时间
0. Queue.put_nowait(item) 相当Queue.put(item, False)
0. Queue.task_done() 在完成一项工作之后,Queue.task_done()函数向任务已经完成的队列发送一个信号
0. Queue.join() 实际上意味着等到队列为空,再执行别的操作

```py
#!/usr/bin/python3

import queue
import threading
import time

exitFlag = 0

class myThread (threading.Thread):
def __init__(self, threadID, name, q):
threading.Thread.__init__(self)
self.threadID = threadID
self.name = name
self.q = q
def run(self):
print ("开启线程:" + self.name)
process_data(self.name, self.q)
print ("退出线程:" + self.name)

def process_data(threadName, q):
while not exitFlag:
queueLock.acquire()
if not workQueue.empty():
data = q.get()
queueLock.release()
print ("%s processing %s" % (threadName, data))
else:
queueLock.release()
time.sleep(1)

threadList = ["Thread-1", "Thread-2", "Thread-3"]
nameList = ["One", "Two", "Three", "Four", "Five"]
queueLock = threading.Lock()
workQueue = queue.Queue(10)
threads = []
threadID = 1

# 创建新线程
for tName in threadList:
thread = myThread(threadID, tName, workQueue)
thread.start()
threads.append(thread)
threadID += 1

# 填充队列
queueLock.acquire()
for word in nameList:
workQueue.put(word)
queueLock.release()

# 等待队列清空
while not workQueue.empty():
pass

# 通知线程是时候退出
exitFlag = 1

# 等待所有线程完成
for t in threads:
t.join()
print ("退出主线程")

# => 开启线程:Thread-1
# => 开启线程:Thread-2
# => 开启线程:Thread-3
# => Thread-3 processing One
# => Thread-1 processing Two
# => Thread-2 processing Three
# => Thread-3 processing Four
# => Thread-1 processing Five
# => 退出线程:Thread-3
# => 退出线程:Thread-2
# => 退出线程:Thread-1
# => 退出主线程
```

## XML解析

## JSON

Python3中可以使用json模块来对JSON数据进行编解码,它包含了两个函数:

0. json.dumps(): 对数据进行编码.
0. json.loads(): 对数据进行解码.

![](https://www.runoob.com/wp-content/uploads/2016/04/json-dumps-loads.png)

### 类型转换对应表

在json的编解码过程中,Python的原始类型与json类型会相互转换,具体的转化对照如下:

#### Python编码为JSON

Python|JSON
-|-|
dict|object
list, tuple|array
str|string
int, float, int- & float-derived Enums|number
True|true
False|false
None|null

#### JSON解码为Python

JSON|Python
-|-|
object|dict
array|list
string|str
number (int)|int
number (real)|float
true|True
false|False
null|None

## 日期和时间

Python提供了一个time和calendar模块可以用于格式化日期和时间.<br>


时间间隔是以秒为单位的浮点小数.<br>


每个时间戳都以自从1970年1月1日午夜(历元)经过了多长时间来表示.<br>


Python的time模块下有很多函数可以转换常见日期格式.<br>
如函数time.time() 用于获取当前时间戳, 如下实例:

```py
#!/usr/bin/python3

import time  # 引入time模块

ticks = time.time()
print ("当前时间戳为:", ticks)
```

时间戳单位最适于做日期运算.<br>
但是1970年之前的日期就无法以此表示了.<br>
太遥远的日期也不行,UNIX和Windows只支持到2038年.

### 时间元组(struct_time)

很多Python函数用一个元组装起来的9组数字处理时间:

序号|字段|值
-|-|-|
0|4位数年|2008
1|月|1到12
2|日|1到31
3|小时|0到23
4|分钟|0到59
5|秒|0到61 (60或61是闰秒)
6|一周的第几日|0到6 (0是周一)
7|一年的第几日|1到366 (儒略历)
8|夏令时|-1, 0, 1, -1是决定是否为夏令时的旗帜

### 获取当前时间

从返回浮点数的时间戳方式向时间元组转换,只要将浮点数传递给如localtime之类的函数.<br>


```py
#!/usr/bin/python3

import time

localtime = time.localtime(time.time())
print ("本地时间为 :", localtime)
# => 本地时间为 : time.struct_time(tm_year=2016, tm_mon=4, tm_mday=7, tm_hour=10, tm_min=28, tm_sec=49, tm_wday=3, tm_yday=98, tm_isdst=0)
```

### 获取格式化的时间

```py
#!/usr/bin/python3

import time

localtime = time.asctime( time.localtime(time.time()) )
print ("本地时间为 :", localtime)
# => 本地时间为 : Thu Apr  710:29:132016
```

### 格式化日期

```py
time.strftime(format[, t])

#!/usr/bin/python3

import time

# 格式化成2016-03-2011:45:39形式
print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
# => 2016-04-0710:29:46

# 格式化成Sat Mar 2822:24:242016形式
print (time.strftime("%a %b %d %H:%M:%S %Y", time.localtime()))
# => Thu Apr 0710:29:462016

# 将格式字符串转换为时间戳
a = "Sat Mar 2822:24:242016"
print (time.mktime(time.strptime(a,"%a %b %d %H:%M:%S %Y")))
# => 1459175064.0
```

python中时间日期格式化符号:

符号|含义
-|-|
%y|两位数的年份表示(00-99)
%Y|四位数的年份表示(000-9999)
%m|月份(01-12)
%d|月内中的一天(0-31)
%H|24小时制小时数(0-23)
%I|12小时制小时数(01-12)
%M|分钟数(00=59)
%S|秒(00-59)
%a|本地简化星期名称
%A|本地完整星期名称
%b|本地简化的月份名称
%B|本地完整的月份名称
%c|本地相应的日期表示和时间表示
%j|年内的一天(001-366)
%p|本地A.M.或P.M.的等价符
%U|一年中的星期数(00-53)星期天为星期的开始
%w|星期(0-6),星期天为星期的开始
%W|一年中的星期数(00-53)星期一为星期的开始
%x|本地相应的日期表示
%X|本地相应的时间表示
%Z|当前时区的名称
%%|%号本身

### 获取某月日历(Calendar)

Calendar模块有很广泛的方法用来处理年历和月历,例如打印某月的月历:

```py
#!/usr/bin/python3

import calendar

cal = calendar.month(2016, 1)
```


### Time模块

Time模块包含了以下内置函数,既有时间处理的,也有转换时间格式的.


#### 格林威治偏移秒数(altzone)

返回格林威治西部的夏令时地区的偏移秒数.<br>
如果该地区在格林威治东部会返回负值(如西欧,包括英国).<br>
对夏令时启用地区才能使用.

```py
import time
print ("time.altzone %d " % time.altzone)
time.altzone -28800
```

#### 格式化字符串(asctime([tupletime]))

接受时间元组并返回一个可读的形式为"Tue Dec 1118:07:142008"(2008年12月11日周二18时07分14秒)的24个字符的字符串.

```py
import time
t = time.localtime()
print ("time.asctime(t): %s " % time.asctime(t))
time.asctime(t): Thu Apr  710:36:202016
```

#### CPU时间(clock())

用以浮点数计算的秒数返回当前的CPU时间.<br>
用来衡量不同程序的耗时,比time.time()更有用.

由于该方法依赖操作系统,在Python3.3以后不被推荐,而在3.8版本中被移除,需使用下列两个函数替代.

```py
time.perf_counter()  # 返回系统运行时间
time.process_time()  # 返回进程运行时间
```

#### ctime([secs])

作用相当于asctime(localtime(secs)),未给参数相当于asctime()

```py
import time
print ("time.ctime() : %s" % time.ctime())
time.ctime() : Thu Apr  710:51:582016
```

#### gmtime([secs])

接收时间戳(1970纪元后经过的浮点秒数)并返回格林威治天文时间下的时间元组t.<br>
注:t.tm_isdst始终为0.

```py
import time
print ("gmtime :", time.gmtime(1455508609.34375))
gmtime : time.struct_time(tm_year=2016, tm_mon=2, tm_mday=15, tm_hour=3, tm_min=56, tm_sec=49, tm_wday=0, tm_yday=46, tm_isdst=0)
```

#### localtime([secs])

接收时间戳(1970纪元后经过的浮点秒数)并返回当地时间下的时间元组t(t.tm_isdst可取0或1,取决于当地当时是不是夏令时).

```py
import time
print ("localtime(): ", time.localtime(1455508609.34375))
localtime():  time.struct_time(tm_year=2016, tm_mon=2, tm_mday=15, tm_hour=11, tm_min=56, tm_sec=49, tm_wday=0, tm_yday=46, tm_isdst=0)
```

#### mktime(tupletime)

接受时间元组并返回时间戳(1970纪元后经过的浮点秒数).

#### sleep(secs)

推迟调用线程的运行,secs指秒数.

```py
#!/usr/bin/python3
import time

print ("Start : %s" % time.ctime())
time.sleep( 5 )
print ("End : %s" % time.ctime())
```

#### 格式化本地时间(strftime(fmt[,tupletime]))

接收以时间元组,并返回以可读字符串表示的当地时间,格式由fmt决定.

```py
import time
print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
2016-04-0711:18:05
```

#### strptime

根据fmt的格式把一个时间字符串解析为时间元组.

```py
import time
struct_time = time.strptime("30 Nov 00", "%d %b %y")
print ("返回元组: ", struct_time)
返回元组:  time.struct_time(tm_year=2000, tm_mon=11, tm_mday=30, tm_hour=0, tm_min=0, tm_sec=0, tm_wday=3, tm_yday=335, tm_isdst=-1)
```

#### 当前时间戳(time())

返回当前时间的时间戳(1970纪元后经过的浮点秒数).

```py
import time
print(time.time())
1459999336.1963577
```

#### 重新初始化时间设置(tzset())

根据环境变量TZ重新初始化时间相关设置.

#### 系统运行时间(perf_counter())

返回计时器的精准时间(系统的运行时间),包含整个系统的睡眠时间.<br>
由于返回值的基准点是未定义的,所以,只有连续调用的结果之间的差才是有效的.

#### 进程执行CPU的时间总和(process_time())

返回当前进程执行CPU的时间总和,不包含睡眠时间.<br>
由于返回值的基准点是未定义的,所以,只有连续调用的结果之间的差才是有效的.

### 日历(Calendar)模块

星期一是默认的每周第一天,星期天是默认的最后一天.<br>
更改设置需调用calendar.setfirstweekday()函数.<br>
模块包含了以下内置函数:


#### calendar.calendar(year,w=2,l=1,c=6)

返回一个多行字符串格式的year年年历,3个月一行,间隔距离为c.<br>
每日宽度间隔为w字符.<br>
每行长度为21* W+18+2* C.<br>
l是每星期行数.

#### calendar.firstweekday( )

返回当前每周起始日期的设置.<br>
默认情况下,首次载入caendar模块时返回0,即星期一.

#### calendar.isleap(year)

是闰年返回True,否则为false.

```py
import calendar
print(calendar.isleap(2000))
True
print(calendar.isleap(1900))
False
```

#### calendar.leapdays(y1,y2)

返回在Y1,Y2两年之间的闰年总数.

#### calendar.month(year,month,w=2,l=1)

返回一个多行字符串格式的year年month月日历,两行标题,一周一行.<br>
每日宽度间隔为w字符.<br>
每行的长度为7* w+6.<br>
l是每星期的行数.

#### calendar.monthcalendar(year,month)

返回一个整数的单层嵌套列表.<br>
每个子列表装载代表一个星期的整数.<br>
Year年month月外的日期都设为0;范围内的日子都由该月第几日表示,从1开始.

#### calendar.monthrange(year,month)

返回两个整数.<br>
第一个是该月的星期几,第二个是该月有几天.<br>
星期几是从0(星期一)到6(星期日).

```py
import calendar
calendar.monthrange(2014, 11)
# => (5, 30) # 5表示2014年11月份的第一天是周六,30表示2014年11月份总共有30天.
```

#### calendar.prcal(year, w=0, l=0, c=6, m=3)

相当于print(calendar.calendar(year, w=0, l=0, c=6, m=3)).

#### calendar.prmonth(theyear, themonth, w=0, l=0)

相当于print(calendar.month(theyear, themonth, w=0, l=0)).

#### calendar.setfirstweekday(weekday)

设置每周的起始日期码.<br>
0(星期一)到6(星期日).

#### calendar.timegm(tupletime)

和time.gmtime相反:接受一个时间元组形式,返回该时刻的时间戳(1970纪元后经过的浮点秒数).

#### calendar.weekday(year,month,day)

返回给定日期的日期码.<br>
0(星期一)到6(星期日).<br>
月份为1(一月) 到12(12月).

## 内置函数

### abs()



### dict()



### help()



### min()



### setattr()



### all()



### dir()



### hex()



### next()



### slice()



### any()



### divmod()



### id()



### object()



### sorted()



### ascii()



### enumerate()



### input()



### oct()



### staticmethod()



### bin()



### eval()



### int()



### open()



### str()



### bool()



### exec()



### isinstance()



### ord()



### sum()



### bytearray()



### filter()



### issubclass()



### pow()



### super()



### bytes()



### float()



### iter()



### print()



### tuple()



### callable()



### format()



### len()



### property()



### type()



### chr()



### frozenset()



### list()



### range()



### vars()



### classmethod()



### getattr()



### locals()



### repr()



### zip()



### compile()



### globals()



### map()



### reversed()



### __import__()



### complex()



### hasattr()



### max()



### round()



### delattr()



### hash()



### memoryview()



### set()



## MongoDB


## urllib

Pythonurllib库用于操作网页URL,并对网页的内容进行抓取处理.

urllib包包含以下几个模块:

0. urllib.request - 打开和读取URL.
0. urllib.error - 包含urllib.request抛出的异常.
0. urllib.parse - 解析URL.
0. urllib.robotparser - 解析robots.txt文件.

![](https://www.runoob.com/wp-content/uploads/2021/04/ulrib-py3.svg)

### request

urllib.request定义了一些打开URL的函数和类,包含授权验证、重定向、浏览器cookies等.

urllib.request可以模拟浏览器的一个请求发起过程.

可以使用urllib.request的urlopen方法来打开一个URL,语法格式如下:

```py
urllib.request.urlopen(url, data=None, [timeout, ]*, cafile=None, capath=None, cadefault=False, context=None)
```

0. url:url地址.
0. data:发送到服务器的其数据对象,默认为None.
0. timeout:设置访问超时时间.
0. cafile和capath:cafile为CA证书,capath为CA证书的路径,使用HTTPS需要用到.
0. cadefault:已经被弃用.
0. context:ssl.SSLContext类型,用来指定SSL设置.

```py
from urllib.request import urlopen

myURL = urlopen("https://www.runoob.com/")
print(myURL.read())
```

read() 是读取整个网页内容,可以指定读取的长度:

```py
from urllib.request import urlopen

myURL = urlopen("https://www.runoob.com/")
print(myURL.read(300))
```

除了read() 函数外,还包含以下两个读取网页内容的函数:

### 读取文件的一行内容(readline())

```py
from urllib.request import urlopen

myURL = urlopen("https://www.runoob.com/")
print(myURL.readline()) #读取一行内容
```

### 读取文件的全部内容(readlines())

读取文件的全部内容,它会把读取的内容赋值给一个列表变量.

```py
from urllib.request import urlopen

myURL = urlopen("https://www.runoob.com/")
lines = myURL.readlines()
for line in lines:
print(line)
```

### 获取网页状态码(getcode())

```py
import urllib.request

myURL1 = urllib.request.urlopen("https://www.runoob.com/")
print(myURL1.getcode())   # 200

try:
myURL2 = urllib.request.urlopen("https://www.runoob.com/no.html")
except urllib.error.HTTPError as e:
if e.code == 404:
print(404)   # 404
```

### 编码与解码

```py
import urllib.request

encode_url = urllib.request.quote("https://www.runoob.com/")  # 编码
print(encode_url)
# => https%3A//www.runoob.com/

unencode_url = urllib.request.unquote(encode_url)    # 解码
print(unencode_url)
# => https://www.runoob.com/
```

### 模拟头部信息(request.Request)

抓取网页一般需要对headers(网页头信息)进行模拟,这时候需要使用到urllib.request.Request类:

```py
class urllib.request.Request(url, data=None, headers={}, origin_req_host=None, unverifiable=False, method=None)
```

0. url:url地址.
0. data:发送到服务器的其数据对象,默认为None.
0. headers:HTTP请求的头部信息,字典格式.
0. origin_req_host:请求的主机地址,IP或域名.
0. unverifiable:很少用整个参数,用于设置网页是否需要验证,默认是False.
0. method:请求方法, 如GET、POST、DELETE、PUT等.

```py
import urllib.request
import urllib.parse

url = 'https://www.runoob.com/?s='  # 菜鸟教程搜索页面
keyword = 'Python教程'
key_code = urllib.request.quote(keyword)  # 对请求进行编码
url_all = url+key_code
header = {
'User-Agent':'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
}   #头部信息
request = urllib.request.Request(url_all,headers=header)
reponse = urllib.request.urlopen(request).read()

fh = open("./urllib_test_runoob_search.html","wb")    # 将文件写入到当前目录中
fh.write(reponse)
fh.close()
```

### 异常(error)

urllib.error模块为urllib.request所引发的异常定义了异常类,基础异常类是URLError.

urllib.error包含了两个方法,URLError和HTTPError.

URLError是OSError的一个子类,用于处理程序在遇到问题时会引发此异常(或其派生的异常),包含的属性reason为引发异常的原因.

HTTPError是URLError的一个子类,用于处理特殊HTTP错误例如作为认证请求的时候,<br>
包含的属性code为HTTP的状态码,reason为引发异常的原因,<br>
headers为导致HTTPError的特定HTTP请求的HTTP响应头.

对不存在的网页抓取并处理异常:

```py
import urllib.request
import urllib.error

myURL1 = urllib.request.urlopen("https://www.runoob.com/")
print(myURL1.getcode())   # 200

try:
myURL2 = urllib.request.urlopen("https://www.runoob.com/no.html")
except urllib.error.HTTPError as e:
if e.code == 404:
print(404)   # 404
```

### 解析URL(parse)

```py
urllib.parse.urlparse(urlstring, scheme='', allow_fragments=True)
```

0. urlstring: 字符串的url地址
0. scheme: 协议类型.
0. allow_fragments: 参数为false,则无法识别片段标识符.<br>
相反,它们被解析为路径,参数或查询组件的一部分,并fragment在返回值中设置为空字符串.

```py
from urllib.parse import urlparse

o = urlparse("https://www.runoob.com/?s=python+%E6%95%99%E7%A8%8B")

print(o)
# => ParseResult(scheme='https', netloc='www.runoob.com', path='/', params='', query='s=python+%E6%95%99%E7%A8%8B', fragment='')
```

属性|索引|值|值(如果不存在)
-|-|-|-|
scheme|0|URL协议|scheme参数
netloc|1|网络位置部分|空字符串
path|2|分层路径|空字符串
params|3|最后路径元素的参数|空字符串
query|4|查询组件|空字符串
fragment|5|片段识别|空字符串
username||用户名|None
password||密码|None
hostname||主机名(小写)|None
port||端口号为整数(如果存在)|None

### 解析robots协议(robotparser)

0. urllib.robotparser用于解析robots.txt文件.
0. robots.txt(统一小写)是一种存放于网站根目录下的robots协议,它通常用于告诉搜索引擎对网站的抓取规则.
urllib.robotparser提供了RobotFileParser类,语法如下:

```py
class urllib.robotparser.RobotFileParser(url='')
```

这个类提供了一些可以读取、解析robots.txt文件的方法:

0. set_url(url): 设置robots.txt文件的URL.
0. read(): 读取robots.txtURL并将其输入解析器.
0. parse(lines): 解析行参数.
0. can_fetch(useragent, url): 如果允许useragent按照被解析robots.txt文件中的规则来获取url则返回True.
0. mtime(): 返回最近一次获取robots.txt文件的时间.<br>
这适用于需要定期检查robots.txt文件更新情况的长时间运行的网页爬虫.
0. modified(): 将最近一次获取robots.txt文件的时间设置为当前时间.
0. crawl_delay(useragent): 为指定的useragent从robots.txt返回Crawl-delay形参.<br>
如果此形参不存在或不适用于指定的useragent或者此形参的robots.txt条目存在语法错误,则返回None.
0. request_rate(useragent): 以namedtuple RequestRate(requests, seconds) 的形式从robots.txt返回Request-rate形参的内容.<br>
如果此形参不存在或不适用于指定的useragent或者此形参的robots.txt条目存在语法错误,则返回None.
0. site_maps(): 以list() 的形式从robots.txt返回Sitemap形参的内容.<br>
如果此形参不存在或者此形参的robots.txt条目存在语法错误,则返回None.

```py
import urllib.robotparser
rp = urllib.robotparser.RobotFileParser()
rp.set_url("http://www.musi-cal.com/robots.txt")
rp.read()
rrate = rp.request_rate("*")
rrate.requests
# => 3
rrate.seconds
# => 20
rp.crawl_delay("*")
# => 6
rp.can_fetch("*", "http://www.musi-cal.com/cgi-bin/search?city=San+Francisco")
# => False
rp.can_fetch("*", "http://www.musi-cal.com/")
# => True
```

## PythonuWSGI安装配置

# 参考资料

> [菜鸟教程 | Python3教程](https://www.runoob.com/python3/python3-tutorial.html)

> [stackoverflow | How to get the nth element of a python list or a default if not available](https://stackoverflow.com/questions/2492087/how-to-get-the-nth-element-of-a-python-list-or-a-default-if-not-available)