
<!-- TOC -->

- [提供样式](#提供样式)
  - [通过百分比限制宽度(w-25)](#通过百分比限制宽度w-25)
  - [间距](#间距)
- [常用扩展](#常用扩展)
  - [Markdown](#markdown)
    - [bootstrap-markdown](#bootstrap-markdown)
- [参考资料](#参考资料)

<!-- /TOC -->

# 提供样式

## 通过百分比限制宽度(w-25)

仅提供了以下几个class, 并非任意数值均可:

```
.w-25, .w-50, .w-75, .w-100, .w-auto
```

## 间距

Bootstrap 4具有广泛的响应边距和填充实用程序类。它们适用于所有断点:xs(<= 576px),sm(> = 576px),md(> = 768px),lg(> = 992px)或xl(> = 1200px)):

这些类的格式使用:{property}{sides}-{size}对于xs和{property}{sides}-{breakpoint}-{size}为sm,md,lg,和xl。

其中property是以下之一:

标记|含义
-|-
m|margin
p|padding

sides其中边是以下之一:

标记|含义
-|-
t|margin-top或padding-top
b|margin-bottom或padding-bottom
l|margin-left或padding-left
r|margin-right或padding-right
x|padding-left和padding-right或margin-left与margin-right
y|padding-top和padding-bottom或margin-top与margin-bottom
空白|在元素的所有4侧设置margin或padding

size其中大小是以下之一:

标记|含义
-|-
0|margin或padding以0
1|设置为margin或设置padding为.25rem(如果font-size为16px,则为4px)
2|设置为margin或设置padding为.5rem(如果字体大小为16px,则为8px)
3|设置为margin或设置padding为1rem(如果font-size为16px,则为16px)
4|集margin或padding到1.5rem(如果字体大小为16px,则为24px)
5|设置为margin或设置padding为3rem(如果字体大小为16px,则为48px)
auto|设置margin为自动

注意:边距也可以为负数,方法是在size前面添加"n":

标记|含义
-|-
n1|设置margin为-.25rem(如果字体大小为16px,则为-4px)
n2|设置margin为-.5rem(如果字体大小为16px,则为-8px)
n3|设置margin为-1rem(如果字体大小为16px,则为-16px)
n4|设置margin为-1.5rem(如果字体大小为16px,则为-24px)
n5|设置margin为-3rem(如果字体大小为16px,则为-48px)

例
```html
<div class="pt-4"></div>
<div class="p-5"></div>
<div class="m-5"></div>
```

# 常用扩展

## Markdown 

### bootstrap-markdown



# 参考资料

> [Github | bootstrap-markdown](https://github.com/toopay/bootstrap-markdown)

> [CSDN | bootstrap-markdown使用](https://blog.csdn.net/hai4321/article/details/75204267)

> [stackoverflow | How to set up fixed width for <td>?](https://stackoverflow.com/questions/15115052/how-to-set-up-fixed-width-for-td/34987484#34987484)