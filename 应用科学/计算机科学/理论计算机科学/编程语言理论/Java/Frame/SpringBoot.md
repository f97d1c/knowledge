<!-- TOC -->

- [简介](#简介)
  - [目录结构](#目录结构)
    - [基础结构](#基础结构)
    - [包声明命名约定](#包声明命名约定)
  - [与Springcloud的区别](#与springcloud的区别)
  - [SpringBoot和SpringMVC区别](#springboot和springmvc区别)
  - [环境要求](#环境要求)
- [注解解释](#注解解释)
- [配置文件](#配置文件)
  - [application.properties](#applicationproperties)
    - [设置启动端口](#设置启动端口)
- [如何访问静态资源](#如何访问静态资源)
- [渲染Web页面](#渲染web页面)
  - [Thymeleaf](#thymeleaf)
    - [POM](#pom)
    - [配置文件](#配置文件-1)
    - [后台](#后台)
    - [前台](#前台)
  - [Freemarker(自由标记)](#freemarker自由标记)
    - [POM](#pom-1)
    - [配置文件](#配置文件-2)
    - [后台](#后台-1)
    - [前台](#前台-1)
  - [JSP](#jsp)
    - [POM](#pom-2)
    - [配置文件](#配置文件-3)
    - [后台](#后台-2)
    - [前台](#前台-2)
  - [Spring Webflux](#spring-webflux)
    - [POM](#pom-3)
    - [官方实例](#官方实例)
    - [Controller层](#controller层)
    - [Service 层](#service-层)
    - [路由器类 Router](#路由器类-router)
    - [@Async](#async)
- [数据访问](#数据访问)
  - [整合JdbcTemplate](#整合jdbctemplate)
  - [配置文件](#配置文件-4)
  - [后台](#后台-3)
    - [创建一个Service](#创建一个service)
  - [整合Mybatis](#整合mybatis)
    - [POM](#pom-4)
    - [配置文件](#配置文件-5)
    - [后台](#后台-4)
      - [创建一个Dao(Mapper 代码)](#创建一个daomapper-代码)
      - [创建service](#创建service)
    - [整合分页插件PageHelper](#整合分页插件pagehelper)
      - [POM](#pom-5)
      - [配置文件](#配置文件-6)
    - [常见问题](#常见问题)
      - [Cannot load driver class: com.mysql.cj.jdbc.Driver](#cannot-load-driver-class-commysqlcjjdbcdriver)
      - [代码](#代码)
  - [整合SpringJPA](#整合springjpa)
    - [POM](#pom-6)
    - [配置文件](#配置文件-7)
    - [Domain](#domain)
    - [Dao层](#dao层)
  - [整合Elasticsearch](#整合elasticsearch)
    - [关于Spring Data](#关于spring-data)
    - [POM](#pom-7)
    - [application.properties](#applicationproperties-1)
    - [elasticsearch.yml](#elasticsearchyml)
    - [实体类注解映射](#实体类注解映射)
      - [@Document](#document)
      - [@Id](#id)
      - [@Field](#field)
        - [type](#type)
- [多数据源](#多数据源)
  - [配置文件](#配置文件-8)
  - [添加配置](#添加配置)
  - [Dao](#dao)
- [事物管理](#事物管理)
  - [声明式事务](#声明式事务)
  - [分布式事物管理](#分布式事物管理)
    - [类事务管理器Atomikos](#类事务管理器atomikos)
      - [POM](#pom-8)
      - [配置文件](#配置文件-9)
      - [读取配置文件信息](#读取配置文件信息)
      - [创建数据源](#创建数据源)
      - [如何启动](#如何启动)
- [定时任务](#定时任务)
  - [Spring Schedule](#spring-schedule)
    - [启动](#启动)
  - [Quartz](#quartz)
    - [POM](#pom-9)
    - [配置文件](#配置文件-10)
    - [配置类](#配置类)
    - [实现类](#实现类)
- [日志管理](#日志管理)
  - [log4j](#log4j)
    - [POM](#pom-10)
    - [配置文件](#配置文件-11)
    - [使用](#使用)
  - [使用AOP统一处理Web请求日志](#使用aop统一处理web请求日志)
    - [POM](#pom-11)
  - [代码](#代码-1)
  - [lombok 插件](#lombok-插件)
    - [POM](#pom-12)
    - [代码](#代码-2)
    - [其他用法](#其他用法)
- [拦截器](#拦截器)
  - [自定义拦截器](#自定义拦截器)
    - [注册拦截器](#注册拦截器)
- [缓存](#缓存)
  - [EhCache](#ehcache)
    - [POM](#pom-13)
    - [配置信息介绍](#配置信息介绍)
    - [关于注解和代码使用](#关于注解和代码使用)
    - [清除缓存](#清除缓存)
    - [启动](#启动-1)
  - [Redis](#redis)
    - [使用自带驱动器连接](#使用自带驱动器连接)
      - [POM](#pom-14)
      - [配置文件](#配置文件-12)
        - [单机](#单机)
        - [集群或哨兵模式](#集群或哨兵模式)
          - [redis集群配置](#redis集群配置)
      - [配置类](#配置类-1)
      - [Dao](#dao-1)
  - [使用Jedis连接](#使用jedis连接)
    - [POM](#pom-15)
    - [配置类](#配置类-2)
- [监控中心](#监控中心)
  - [Actuator](#actuator)
    - [POM](#pom-16)
    - [配置信息](#配置信息)
    - [Actuator访问路径](#actuator访问路径)
  - [Admin-UI分布式微服务监控中心](#admin-ui分布式微服务监控中心)
    - [POM](#pom-17)
    - [application.yml](#applicationyml)
- [性能优化](#性能优化)
  - [扫包优化](#扫包优化)
  - [SpringBoot JVM参数调优](#springboot-jvm参数调优)
    - [各种参数](#各种参数)
    - [调优策略](#调优策略)
  - [将Servlet容器从Tomcat变成Undertow](#将servlet容器从tomcat变成undertow)
    - [Undertow](#undertow)
      - [POM](#pom-18)
  - [Tomcat 优化](#tomcat-优化)
- [热部署](#热部署)
  - [原理](#原理)
  - [应用场景](#应用场景)
  - [Dev-tools](#dev-tools)
    - [POM](#pom-19)
    - [原理](#原理-1)
- [发布打包](#发布打包)
  - [Jar类型打包方式](#jar类型打包方式)
  - [war类型打包方式](#war类型打包方式)
  - [外部Tomcat运行](#外部tomcat运行)
  - [POM](#pom-20)
- [常见问题](#常见问题-1)
  - [Controller](#controller)
    - [Autowired cannot be resolved to a type](#autowired-cannot-be-resolved-to-a-type)
  - [Mapper](#mapper)
    - [Consider defining a bean of type * in your configuration.](#consider-defining-a-bean-of-type--in-your-configuration)
      - [添加@Mapper注解](#添加mapper注解)
    - [启动类中添加扫包路径](#启动类中添加扫包路径)
- [参考资料](#参考资料)

<!-- /TOC -->

# 简介

> Springboot是一个优秀的快速搭建框架,<br>
通过maven继承方式添加依赖来整合很多第三方工具,<br>
可以避免各种麻烦的配置,有各种内嵌容器简化Web项目,还能避免依赖的干扰,<br>
它内置tomcat,jetty容器,使用java app运行程序,而不是传统的用把war放在tomcat等容器中运行.

## 目录结构

> Spring Boot没有任何代码布局可供使用.<br>
但是,有一些最佳实践可以简化代码布局.

```bash
.
├── HELP.md
├── mvnw
├── mvnw.cmd
├── pom.xml # 项目依赖配置,类似Gemfile
├── src
│   ├── main
│   │   ├── java # 程序开发以及主程序入口
│   │   │   └── com
│   │   │       └── example
│   │   │           └── demo
│   │   │               ├── controller # 负责页面访问控制
│   │   │               │   └── SampleController.java
│   │   │               ├── domain # 用于实体(Entity)与数据访问层(Repository)
│   │   │               ├── service # 业务类代码
│   │   │               └── DemoApplication.java # 放在包根目录下面,主要用于做一些框架配置
│   │   └── resources # 配置文件
│   │       ├── application.properties
│   │       ├── static # 静态文件存放
│   │       └── templates
│   └── test
│       └── java # 测试程序
│           └── com
│               └── example
│                   └── demo
│                       └── DemoApplicationTests.java
└── target
    ├── classes
    │   ├── application.properties
    │   ├── com
    │   │   └── example
    │   │       └── demo
    │   │           └── DemoApplication.class
    │   └── static
    │       └── hello_spring_boot_2019_09_17.png
    └── test-classes
        └── com
            └── example
                └── demo
                    └── DemoApplicationTests.class

```

### 基础结构

Spring Boot的基础结构共三个文件:

0. src/main/java  程序开发以及主程序入口
0. src/main/resources 配置文件
0. src/test/java  测试程序

### 包声明命名约定

Java推荐的包声明命名约定是反向域名:co.imdo.ff4c00

## 与Springcloud的区别

Springboot里面包含了Springcloud,Springcloud只是Springboot里面的一个组件而已.

Springcloud提供了相当完整的微服务架构.<br>
而微服务架构,本质来说就是分布式架构,意味着你要将原来是一个整体的项目拆分成一个个的小型项目,<br>
然后利用某种机制将其联合起来,例如服务治理、通信框架等基础设施.

## SpringBoot和SpringMVC区别

SpringBoot的Web组件,默认集成的是SpringMVC框架.

## 环境要求

* Springboot 2.x 要求 JDK 1.8 环境及以上版本.<br>
另外,Springboot 2.x 只兼容 Spring Framework 5.0 及以上版本.
* Maven 为 Springboot 2.x 提供了相关依赖构建工具,版本需要 3.2 及以上版本.<br>
使用 Gradle 则需要 1.12 及以上版本.

# 注解解释

名称|含义
-|-
@RestController|修饰该Controller所有的方法返回JSON格式,直接可以编写Restful接口.<br>相当于@Controller+@ResponseBody这种实现
@SpringBootApplication|用于启动Springboot中,相当于@ComponentScan+@EnableAutoConfiguration+@Configuration
@ComponentScan("package.path")|控制器扫包范围.
@EnableAutoConfiguration|让 Spring Boot 根据应用所声明的依赖来对 Spring 框架进行自动配置.

# 配置文件

## application.properties

### 设置启动端口

```
server.port=8090
```

# 如何访问静态资源

静态资源一般放在: src/main/resources/static

如访问:src/main/resources/static/hello_spring_boot_2019_09_17.png,<br>
直接: http://localhost:8080/hello_spring_boot_2019_09_17.png 即可.

# 渲染Web页面

Springboot提供了多种模板引擎的默认配置支持,Springboot官方文档有如下推荐的模板引擎:

模板名称|优点|缺点
-|-|-
Thymeleaf|
FreeMarker|
Velocity|
Groovy|
Mustache|

***Springboot官方建议避免使用JSP,若一定要使用JSP将无法实现Spring Boot的多种特性.***

在Springboot中,默认的模板配置路径在:src/main/resources/templates.<br>
可以修改这个路径,具体如何修改,可在各模板引擎的配置属性中查询并修改.

## Thymeleaf

### POM

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
```

### 配置文件

在application.properties中添加:

```properties
# thymeleaf
spring.thymeleaf.prefix=classpath:/templates/
spring.thymeleaf.suffix=.html
spring.thymeleaf.cache=false
spring.thymeleaf.servlet.content-type=text/html
spring.thymeleaf.enabled=true
spring.thymeleaf.encoding=UTF-8
# 一代填 spring.thymeleaf.mode=HTML5
spring.thymeleaf.mode=HTML
```

### 后台

```java
@RequestMapping("/to_list")
public String list(Model model,MiaoshaUser user) {
	model.addAttribute("user", user);
	//查询商品列表
	List<GoodsVo> goodsList = goodsService.listGoodsVo();
	model.addAttribute("goodsList", goodsList);
    return "goods_list";
}
```

### 前台

在src/main/resources/创建一个templates文件夹,新网页后缀为*.html

Thymeleaf语法很像HTML,不同之处在标签加了一个th前缀:

```html
<div class="panel panel-default" >
  <div class="panel-heading">秒杀商品列表</div>
  <table class="table" id="goodslist">
    <tr>
      <td>商品名称</td>
      <td>商品图片</td>
      <td>商品原价</td>
      <td>秒杀价</td>
      <td>库存数量</td>
      <td>详情</td>
    </tr>
    <tr th:each="goods,goodsStat : ${goodsList}">
      <td th:text="${goods.goodsName}"></td>
      <td ><img th:src="@{${goods.goodsImg}}" width="100" height="100" /></td>
      <td th:text="${goods.goodsPrice}"></td>
      <td th:text="${goods.miaoshaPrice}"></td>
      <td th:text="${goods.stockCount}"></td>
      <td><a th:href="'/goods_detail.htm?goodsId='+${goods.id}">详情</a></td>
    </tr>
  </table>
</div>
```

## Freemarker(自由标记)

### POM

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-freemarker</artifactId>
</dependency>
```

### 配置文件

在application.properties中添加:

```properties
#Freemarker
spring.freemarker.allow-request-override=false
spring.freemarker.cache=true
spring.freemarker.check-template-location=true
spring.freemarker.charset=UTF-8
spring.freemarker.content-type=text/html
spring.freemarker.expose-request-attributes=false
spring.freemarker.expose-session-attributes=false
spring.freemarker.expose-spring-macro-helpers=false
#spring.freemarker.prefix=
#spring.freemarker.request-context-attribute=
#spring.freemarker.settings.*=
spring.freemarker.suffix=.ftl
spring.freemarker.template-loader-path=classpath:/templates/
#comma-separated list
spring.freemarker.view-names= # whitelist of view names that can be resolved
```

### 后台

在src/main/resources/创建一个templates文件夹,新网页后缀为*.ftl

```java
@RequestMapping("/freemarkerIndex")
public String index(Map<String, Object> result) {
  result.put("nickname", "tEngSHe789");
  result.put("old", "18");
  result.put("my Blog", "HTTPS://blog.tengshe789.tech/");
  List<String> listResult = new ArrayList<String>();
  listResult.add("guanyu");
  listResult.add("zhugeliang");
  result.put("listResult", listResult);
  return "index";
}
```

### 前台

```html
${nickname}
<#if old=="18">
<#elseif old=="21">
<#else>
</#if>
<#list userlist as user>
  ${user}
</#list>
```

## JSP

不建议用Springboot整合JSP,要的话一定要为war类型,否则会找不到页面.<br>
而且不要把JSP页面存放在resources/jsp, 这样访问不到.

### POM

```xml
<parent>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-parent</artifactId>
	<version>2.0.4.RELEASE</version>
</parent>
<dependencies>
	<!-- SpringBoot web 核心组件 -->
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-web</artifactId>
	</dependency>
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-tomcat</artifactId>
	</dependency>
<!-- SpringBoot 外部tomcat支持 -->
<dependency>
		<groupId>org.apache.tomcat.embed</groupId>
		<artifactId>tomcat-embed-jasper</artifactId>
	</dependency>
</dependencies>
```

### 配置文件

在application.properties中添加:

```properties
spring.mvc.view.prefix=/WEB-INF/jsp/
spring.mvc.view.suffix=.jsp
```

### 后台

在src/main/resources/创建一个templates文件夹,新网页后缀为*.jsp

```java
@Controller
public class IndexController {
	@RequestMapping("/index")
	public String index() {
		return "index";
	}
}
```

### 前台

略

## Spring Webflux

Spring Boot Webflux是基于Reactor实现的.<br>
SpringBoot2.0包括一个新的spring-webflux模块.<br>
该模块包含对响应式HTTP和WebSocket客户端的支持,以及对REST,HTML和WebSocket交互等程序的支持.<br>
一般来说,SpringMVC用于同步处理,SpringWebflux用于异步处理.

Spring Boot Webflux 有两种编程模型实现,一种类似 Spring MVC 注解方式,另一种是使用其功能性端点方式.

![](https://blog.tengshe789.tech/2018/08/04/springboot/8.jpg)

WebFlux 支持的容器有 Tomcat、Jetty(Non-Blocking IO API),<br>
也可以像Netty和Undertow的本身就支持异步容器.<br>
在容器中SpringWebFlux会将输入流适配成Mono或者Flux格式进行统一处理.

### POM

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-webflux</artifactId>
</dependency>
```

### 官方实例

```java
@RestController
public class PersonController {
	private final PersonRepository repository;

  public PersonController(PersonRepository repository) {
    this.repository = repository;
  }

  @PostMapping("/person")
  Mono<Void> create(@RequestBody Publisher<Person> personStream) {
    return this.repository.save(personStream).then();
  }

  @GetMapping("/person")
  Flux<Person> list() {
    return this.repository.findAll();
  }

  @GetMapping("/person/{id}")
  Mono<Person> findById(@PathVariable String id) {
    return this.repository.findOne(id);
  }
}
```

### Controller层

Spring Boot 2.0 这里有两条不同的线分别是:

Spring Web MVC -> Spring Data
Spring WebFlux -> Spring Data Reactive

如果使用 Spring Data Reactive ,原来的Spring针对SpringData(JDBC等)的事务管理会不起作用.<br>
因为原来的Spring事务管理(SpringDataJPA)都是基于ThreadLocal传递事务的,其本质是基于阻塞IO模型,不是异步的.

但Reactive是要求异步的,不同线程里面ThreadLocal肯定取不到值了.<br>
自然,我们得想想如何在使用Reactive编程是做到事务,有一种方式是回调方式,一直传递conn:newTransaction(conn->{})

因为每次操作数据库也是异步的,所以connection在Reactive编程中无法靠ThreadLocal传递了,只能放在参数上面传递.<br>
虽然会有一定的代码侵入行.<br>
进一步,也可以kotlin协程,去做到透明的事务管理,即把conn放到协程的局部变量中去.

那Spring Data Reactive Repositories不支持MySQL,进一步也不支持MySQL事务,怎么办？

答案是,这个问题其实和第一个问题也相关.<br>
为啥不支持MySQL,即JDBC不支持.<br>
大家可以看到JDBC是所属Spring Data的.<br>
所以可以等待Spring DataReactive Repositories升级IO模型,去支持MySQL.<br>
也可以和上面也讲到了,如何使用Reactive编程支持事务.

如果应用只能使用不强依赖数据事务,依旧使用MySQL,可以使用下面的实现,代码如下:

### Service 层

```java
public interface CityService {

  /**
    * 获取城市信息列表
    *
    * @return
    */
  List<City> findAllCity();

  /**
    * 根据城市 ID,查询城市信息
    *
    * @param id
    * @return
    */
  City findCityById(Long id);

  /**
    * 新增城市信息
    *
    * @param city
    * @return
    */
  Long saveCity(City city);

  /**
    * 更新城市信息
    *
    * @param city
    * @return
    */
  Long updateCity(City city);

  /**
    * 根据城市 ID,删除城市信息
    *
    * @param id
    * @return
    */
  Long deleteCity(Long id);
}
```

具体案例在作者的 [Github](https://github.com/JeffLi1993/springboot-learning-example)

### 路由器类 Router

创建一个 Route 类来定义 RESTful HTTP 路由

请参考聊聊 [Spring Boot 2.x 那些事儿](https://gitbook.cn/gitchat/activity/59e09f788c3a1a4f21a5b445).

### @Async

需要执行异步方法时,在方法上加上@Async之后,底层使用多线程技术.<br>
启动加上需要@EnableAsync.

# 数据访问

## 整合JdbcTemplate

使用这个需要spring-boot-starter-parent版本要在1.5以上
POM

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
	 <artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
```

## 配置文件

在application.properties中添加:

```properties
# jdbc模板
spring.datasource.url=jdbc:mysql://localhost:3306/test
spring.datasource.username=root
spring.datasource.password=123456
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
```

## 后台

### 创建一个Service

```java
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void createUser(String name, Integer age) {
		jdbcTemplate.update("insert into users values(null,?,?);", name, age);
	}
}
```

## 整合Mybatis

### POM

```xml
<dependency>
  <groupId>org.mybatis.spring.boot</groupId>
  <artifactId>mybatis-spring-boot-starter</artifactId>
  <version>1.3.2</version>
</dependency>
```

### 配置文件

在application.properties中添加:

```properties
#mybatis
mybatis.type-aliases-package=cn.tengshe789.domain
mybatis.configuration.map-underscore-to-camel-case=true
mybatis.configuration.default-fetch-size=100
mybatis.configuration.default-statement-timeout=3000
mybatis.mapperLocations = classpath:cn/tengshe789/dao/*.xml
```

### 后台

#### 创建一个Dao(Mapper 代码)

```java
@Mapper
@Component
public interface GoodsDao {

    @Select("select g.*,mg.stock_count, mg.start_date, mg.end_date,mg.miaosha_price from miaosha_goods mg left join goods g on mg.goods_id = g.id")
    public List<GoodsVo> listGoodsVo();
}
```

#### 创建service

```java
@Service
public class GoodsService {

    @Autowired
    GoodsDao goodsDao;

    /*
     * 展示商品列表
     */
    public List<GoodsVo> listGoodsVo() {
        return goodsDao.listGoodsVo();
    }
}
```

### 整合分页插件PageHelper

PageHelper 是一款好用的开源免费的 Mybatis 第三方物理分页插件

#### POM

```xml
<dependency>
	<groupId>com.github.pagehelper</groupId>
	<artifactId>pagehelper-spring-boot-starter</artifactId>
	<version>1.2.5</version>
</dependency>
```

#### 配置文件

在application.properties中添加:

```properties
# 配置日志
logging.level.cn.tengshe789.dao=DEBUG
# Pagehelper
pagehelper.helperDialect=mysql
pagehelper.reasonable=true
pagehelper.supportMethodsArguments=true
pagehelper.params=count=countSql
pagehelper.page-size-zero=true
```

或者在application.yml中添加:

```yml
# 与mybatis整合
mybatis:
  config-location: classpath:mybatis.xml
  mapper-locations:
  - classpath:cn/tengshe789/dao/*.xml

# 分页配置
pagehelper:
  helper-dialect: mysql
  reasonable: true
  support-methods-arguments: true
  params: count=countSql
```


### 常见问题

#### Cannot load driver class: com.mysql.cj.jdbc.Driver

0. maven里面没有指定依赖: mysql-connector-java
0. 检查末尾是不是存在空格:<br>spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver


#### 代码

实体层面

```java
@Data
public class User {
	private Integer id;
	private String name;
}
```

Dao层

```java
public interface UserDao {
	@Select("SELECT * FROM USERS ")
	List<User> findUserList();
}
```

Service层

```java
@Service
public class UserService {
	@Autowired
	private UserMapper userDao;

	/**
	 * page 当前页数<br>
	 * size 当前展示的数据<br>
	 */
	public PageInfo<User> findUserList(int page, int size) {
		// 开启分页插件,放在查询语句上面
		PageHelper.startPage(page, size);
		List<User> listUser = userDao.findUserList();
		// 封装分页之后的数据
		PageInfo<User> pageInfoUser = new PageInfo<User>(listUser);
		return pageInfoUser;
	}
}
```

## 整合SpringJPA

spring-data-jpa三个步骤:

0. 声明持久层的接口,该接口继承 Repository(或Repository的子接口,其中定义了一些常用的增删改查,以及分页相关的方法).
0. 在接口中声明需要的业务方法.Spring Data 将根据给定的策略生成实现代码.
0. 在 Spring 配置文件中增加一行声明,让 Spring 为声明的接口创建代理对象.配置了 jpa:repositories 后,Spring 初始化容器时将会扫描 base-package 指定的包目录及其子目录,为继承 Repository 或其子接口的接口创建代理对象,并将代理对象注册为 Spring Bean,业务层便可以通过 Spring 自动封装的特性来直接使用该对象.

详情:[JPA官方网站](http://projects.spring.io/spring-data-jpa/)

### POM

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
```

### 配置文件

Springboot 默认使用hibernate作为JPA的实现 .需要在application.properties中添加:

```properties
# hibernate
spring.datasource.url=jdbc:mysql://localhost:3306/test?useSSL=false
spring.datasource.username=root
spring.datasource.password=root
spring.datasource.tomcat.max-active=100
spring.datasource.tomcat.max-idle=200
spring.datasource.tomcat.initialSize=20
spring.jpa.database-platform=org.hibernate.dialect.MySQL5Dialect
```

### Domain

```java
@Data
@Entity(name = "users")
public class UserEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "age")
	private Integer age;
}
```

注解的意思:

@Entity会被spring扫描并加载,

@Id注解在主键上

@Column name="call_phone" 指该字段对应的数据库的字段名,如果相同就不需要定义.<br>
数据库下划线间隔和代码中的驼峰法视为相同,<br>
如数据库字段create_time等价于Java类中的createTime,因此不需要用@Column注解.

### Dao层

此时需要继承Repository接口~

```java
public interface UserDao extends JpaRepository<User, Integer> {
}

Controller

@RestController
public class IndexController {
	@Autowired
	private UserDao userDao;

	@RequestMapping("/jpaFindUser")
	public Object jpaIndex(User user) {
		Optional<User> userOptional = userDao.findById(user.getId());
		User result = userOptional.get();
		return reusltUser == null ? "没有查询到数据" : result;
	}
}
```

## 整合Elasticsearch

### 关于Spring Data

> Spring Data 的使命是给各种数据访问提供统一的编程接口,<br>
不管是关系型数据库(如MySQL),还是非关系数据库(如Redis),<br>
或者类似Elasticsearch这样的索引数据库.<br>
从而简化开发人员的代码,提高开发效率.

Spring Data Elasticsearch即Elasticsearch的Spring Data模块.

### POM

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-data-elasticsearch</artifactId>
  <scope>compile</scope>
</dependency>
```

### application.properties

```properties
# Elasticsearch配置文件(必须)
# 该配置和Elasticsearch的elasticsearch.yml中的配置信息有关

spring.data.elasticsearch.cluster-name=my-application
spring.data.elasticsearch.cluster-nodes=127.0.0.1:9200
```

### elasticsearch.yml

> 该yml文件是指Elasticsearch软件下: ./config/elasticsearch.yml

```yml
# 上面配置的cluster-name对应此项
cluster.name: my-application
node.name: node-1
network.host: 0.0.0.0
http.port: 9200

http.cors.enabled: true
http.cors.allow-origin: "*"
node.master: true
node.data: true
```

### 实体类注解映射

Spring Data通过注解来声明字段的映射属性:

#### @Document

> 作用在类,标记实体类为文档对象

属性|作用
-|-
indexName|对应索引库名称
type|对应在索引库中的类型
shards|分片数量,默认5
replicas|副本数量,默认1


#### @Id

> 作用在成员变量,标记一个字段作为id主键

#### @Field

> 作用在成员变量,标记为文档的字段,并指定字段映射属性

属性|作用
-|-
type|字段类型
index|是否索引,布尔类型,默认是true
store|是否存储,布尔类型,默认是false
analyzer|分词器名称,这里的ik_max_word即使用ik分词器

##### type

> 字段类型,是枚举:FieldType,可以是text、long、short、date、integer、object等.

属性值|作用
-|-
text|存储数据时候,会自动分词,并生成索引
keyword|存储数据时候,不会分词建立索引
Numerical|数值类型,分两类<br>基本数据类型:long、interger、short、byte、double、float、half_float<br>浮点数的高精度类型:scaled_float,需要指定一个精度因子,比如10或100.<br>elasticsearch会把真实值乘以这个因子后存储,取出时再还原.
Date|日期类型,elasticsearch可以对日期格式化为字符串存储,但是建议存储为毫秒值,存储为long,节省空间.



# 多数据源

很多公司都会使用多数据库,一个数据库存放共同的配置或文件,另一个数据库是放垂直业务的数据.

原理是根据不同包名,加载不同数据源.

## 配置文件

在application.properties中添加:

```properties
# datasource1
spring.datasource.test1.driver-class-name = com.mysql.jdbc.Driver
spring.datasource.test1.jdbc-url =jdbc:mysql://localhost:3306/test01?useUnicode=true&characterEncoding=utf-8
spring.datasource.test1.username = root
spring.datasource.test1.password = 123456
# datasource2
spring.datasource.test2.driver-class-name = com.mysql.jdbc.Driver
spring.datasource.test2.jdbc-url =jdbc:mysql://localhost:3306/test02?useUnicode=true&characterEncoding=utf-8
spring.datasource.test2.username = root
spring.datasource.test2.password = 123456
```

## 添加配置

数据库1的:

```java
//DataSource01
@Configuration // 注册到springboot容器中
@MapperScan(basePackages = "tech.tengshe789.test01", sqlSessionFactoryRef = "test1SqlSessionFactory")
public class DataSource1Config {

	/**
	 * @methodDesc: 功能描述:(配置test1数据库)
	 * @author: tEngSHe789
	 */
	@Bean(name = "test1DataSource")
	@ConfigurationProperties(prefix = "spring.datasource.test1")
	@Primary
	public DataSource testDataSource() {
		return DataSourceBuilder.create().build();
	}

	/**
	 * @methodDesc: 功能描述:(test1 sql会话工厂)
	 */
	@Bean(name = "test1SqlSessionFactory")
	@Primary
	public SqlSessionFactory testSqlSessionFactory(@Qualifier("test1DataSource") DataSource dataSource)
			throws Exception {
		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
		bean.setDataSource(dataSource);
        //加载mapper(不需要)
		bean.setMapperLocations(
		 new PathMatchingResourcePatternResolver().getResources("classpath:mybatis/mapper/test1/*.xml"));
		return bean.getObject();
	}

	/**
	 *
	 * @methodDesc: 功能描述:(test1 事物管理)
	 */
	@Bean(name = "test1TransactionManager")
	@Primary
	public DataSourceTransactionManager testTransactionManager(@Qualifier("test1DataSource") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}

	@Bean(name = "test1SqlSessionTemplate")
	@Primary
	public SqlSessionTemplate testSqlSessionTemplate(
			@Qualifier("test1SqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}

}
```

数据库2的同理.

## Dao

```java
public interface User1Dao {
	@Insert("insert into users values(null,#{name},#{age});")
	public int addUser(@Param("name") String name, @Param("age") Integer age);
}
```

在多数据源的情况下,使用@Transactional注解时,<br>
应该指定事务管理者@Transactional(transactionManager = "test1TransactionManager")

# 事物管理

## 声明式事务

找到service实现类,加上@Transactional 注解就行,<br>
此@Transactional注解来自org.springframework.transaction.annotation包 ,不是来自javax.transaction .<br>
而且@Transactional不仅可以注解在方法上,也可以注解在类上.<br>
当注解在类上的时候意味着此类的所有public方法都是开启事务的.<br>
如果类级别和方法级别同时使用了@Transactional注解,则使用在类级别的注解会重载方法级别的注解.

注意:Springboot提供了一个@EnableTransactionManagement注解在配置类上来开启声明式事务的支持.<br>
注解@EnableTransactionManagement是默认打开的,想要关闭事务管理,想要在程序入口将这个注解改为false.

## 分布式事物管理

比如在执行一个业务逻辑的时候有两步分别操作A数据源和B数据源,<br>
当在A数据源执行数据更改后,<br>
在B数据源执行时出现运行时异常,<br>
那么必须要让B数据源的操作回滚,<br>
并回滚对A数据源的操作.<br>

这种情况在支付业务时常常出现,比如买票业务在最后支付失败,那之前的操作必须全部回滚,<br>
如果之前的操作分布在多个数据源中,那么这就是典型的分布式事务回滚.

分布式事务在java的解决方案就是JTA(即Java Transaction API).

springboot官方提供了 Atomikos , Bitronix ,Narayana 的类事务管理器.

### 类事务管理器Atomikos

#### POM

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-jta-atomikos</artifactId>
</dependency>
```

#### 配置文件

```properties
# Mysql 1
mysql.datasource.test1.url = jdbc:mysql://localhost:3306/test01?useUnicode=true&characterEncoding=utf-8
mysql.datasource.test1.username = root
mysql.datasource.test1.password = 123456

mysql.datasource.test1.minPoolSize = 3
mysql.datasource.test1.maxPoolSize = 25
mysql.datasource.test1.maxLifetime = 20000
mysql.datasource.test1.borrowConnectionTimeout = 30
mysql.datasource.test1.loginTimeout = 30
mysql.datasource.test1.maintenanceInterval = 60
mysql.datasource.test1.maxIdleTime = 60

# Mysql 2
mysql.datasource.test2.url =jdbc:mysql://localhost:3306/test02?useUnicode=true&characterEncoding=utf-8
mysql.datasource.test2.username =root
mysql.datasource.test2.password =123456

mysql.datasource.test2.minPoolSize = 3
mysql.datasource.test2.maxPoolSize = 25
mysql.datasource.test2.maxLifetime = 20000
mysql.datasource.test2.borrowConnectionTimeout = 30
mysql.datasource.test2.loginTimeout = 30
mysql.datasource.test2.maintenanceInterval = 60
mysql.datasource.test2.maxIdleTime = 60
```

#### 读取配置文件信息

以下是读取数据库1的配置文件
```java
@Data
@ConfigurationProperties(prefix = "mysql.datasource.test1")
public class DBConfig1 {
	private String url;
	private String username;
	private String password;
	private int minPoolSize;
	private int maxPoolSize;
	private int maxLifetime;
	private int borrowConnectionTimeout;
	private int loginTimeout;
	private int maintenanceInterval;
	private int maxIdleTime;
	private String testQuery;
}
```

读取数据库2的配置文件略

#### 创建数据源

数据源1:

```java
@Configuration
// basePackages 最好分开配置 如果放在同一个文件夹可能会报错
@MapperScan(basePackages = "tech.tengshe789.test01", sqlSessionTemplateRef = "testSqlSessionTemplate")
public class MyBatisConfig1 {

	// 配置数据源
	@Primary
	@Bean(name = "testDataSource")
	public DataSource testDataSource(DBConfig1 testConfig) throws SQLException {
		MysqlXADataSource mysqlXaDataSource = new MysqlXADataSource();
		mysqlXaDataSource.setUrl(testConfig.getUrl());
		mysqlXaDataSource.setPinGlobalTxToPhysicalConnection(true);
		mysqlXaDataSource.setPassword(testConfig.getPassword());
		mysqlXaDataSource.setUser(testConfig.getUsername());
		mysqlXaDataSource.setPinGlobalTxToPhysicalConnection(true);

		AtomikosDataSourceBean xaDataSource = new AtomikosDataSourceBean();
		xaDataSource.setXaDataSource(mysqlXaDataSource);
		xaDataSource.setUniqueResourceName("testDataSource");

		xaDataSource.setMinPoolSize(testConfig.getMinPoolSize());
		xaDataSource.setMaxPoolSize(testConfig.getMaxPoolSize());
		xaDataSource.setMaxLifetime(testConfig.getMaxLifetime());
		xaDataSource.setBorrowConnectionTimeout(testConfig.getBorrowConnectionTimeout());
		xaDataSource.setLoginTimeout(testConfig.getLoginTimeout());
		xaDataSource.setMaintenanceInterval(testConfig.getMaintenanceInterval());
		xaDataSource.setMaxIdleTime(testConfig.getMaxIdleTime());
		xaDataSource.setTestQuery(testConfig.getTestQuery());
		return xaDataSource;
	}

	@Primary
	@Bean(name = "testSqlSessionFactory")
	public SqlSessionFactory testSqlSessionFactory(@Qualifier("testDataSource") DataSource dataSource) throws Exception {
		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
		bean.setDataSource(dataSource);
		return bean.getObject();
	}

	@Primary
	@Bean(name = "testSqlSessionTemplate")
	public SqlSessionTemplate testSqlSessionTemplate(
    @Qualifier("testSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}
}
```

数据库2略

#### 如何启动

```java
@EnableConfigurationProperties(value = { DBConfig1.class, DBConfig2.class })
@SpringBootApplication
public class MainApplication {
  public static void main(String[] args) {
      SpringApplication.run(MainApplication.class, args);
  }
 }
```

# 定时任务

spring支持多种定时任务的实现.

## Spring Schedule

Spring Schedule 实现定时任务有两种方式:

0. 使用XML配置定时任务.
0. 使用 @Scheduled 注解.


固定等待时间 @Scheduled(fixedDelay = 时间间隔 )

固定间隔时间 @Scheduled(fixedRate = 时间间隔 )

```java
@Component
public class ScheduleJobs {
  public final static long SECOND = 1 * 1000;
  FastDateFormat fdf = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss");


  @Scheduled(fixedDelay = SECOND * 2)
  public void fixedDelayJob() throws InterruptedException {
      TimeUnit.SECONDS.sleep(2);
      System.out.println("[FixedDelayJob Execute]"+fdf.format(new Date()));
  }
}
```

Corn表达式 @Scheduled(cron = Corn表达式)

```java
@Component
public class ScheduleJobs {
  public final static long SECOND = 1 * 1000;
  FastDateFormat fdf = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss");


  @Scheduled(cron = "0/4 * * * * ?")
  public void cronJob() {
      System.out.println("[CronJob Execute]"+fdf.format(new Date()));
  }
}
```

### 启动

要在主方法上加上@EnableScheduling

## Quartz

### POM

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-quartz-starter</artifactId>
</dependency>
```

### 配置文件

```properties
# spring boot 2.x 已集成Quartz,无需自己配置
spring.quartz.job-store-type=jdbc
spring.quartz.properties.org.quartz.scheduler.instanceName=clusteredScheduler
spring.quartz.properties.org.quartz.scheduler.instanceId=AUTO
spring.quartz.properties.org.quartz.jobStore.class=org.quartz.impl.jdbcjobstore.JobStoreTX
spring.quartz.properties.org.quartz.jobStore.driverDelegateClass=org.quartz.impl.jdbcjobstore.StdJDBCDelegate
spring.quartz.properties.org.quartz.jobStore.tablePrefix=QRTZ_
spring.quartz.properties.org.quartz.jobStore.isClustered=true
spring.quartz.properties.org.quartz.jobStore.clusterCheckinInterval=10000
spring.quartz.properties.org.quartz.jobStore.useProperties=false
spring.quartz.properties.org.quartz.threadPool.class=org.quartz.simpl.SimpleThreadPool
spring.quartz.properties.org.quartz.threadPool.threadCount=10
spring.quartz.properties.org.quartz.threadPool.threadPriority=5
spring.quartz.properties.org.quartz.threadPool.threadsInheritContextClassLoaderOfInitializingThread=true
```

### 配置类

```java
@Configuration
public class QuartzConfig {
    @Bean
    public JobDetail uploadTaskDetail() {
        return JobBuilder.newJob(UploadTask.class).withIdentity("uploadTask").storeDurably().build();
    }

    @Bean
    public Trigger uploadTaskTrigger() {
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule("*/5 * * * * ?");
        return TriggerBuilder.newTrigger().forJob(uploadTaskDetail())
                .withIdentity("uploadTask")
                .withSchedule(scheduleBuilder)
                .build();
    }
}
```

### 实现类

创建一个配置类,分别制定具体任务类和触发的规则

```java
@Configuration
@DisallowConcurrentExecution
public class UploadTask extends QuartzJobBean {
  @Resource
  private TencentYunService tencentYunService;
  @Override
  protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
      System.out.println("任务开始");
      try {
          Thread.sleep(6000);
      } catch (InterruptedException e) {
          e.printStackTrace();
      }
      System.out.println("任务结束");
  }
}
```

@DisallowConcurrentExecution禁止并发执行

并发执行方面,系统默认为true,即第一个任务还未执行完整,第二个任务如果到了执行时间,则会立马开启新线程执行任务,<br>
这样如果是从数据库读取信息,两次重复读取可能出现重复执行任务的情况,所以需要将这个值设置为false,<br>
这样第二个任务会往后推迟,只有在第一个任务执行完成后才会执行第二个任务.

# 日志管理

## log4j

### POM

```xml
<!-- spring boot start -->
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter</artifactId>
    <exclusions>
      <!-- 排除自带的logback依赖 -->
      <exclusion>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-logging</artifactId>
      </exclusion>
    </exclusions>
  </dependency>

  <!-- springboot-log4j -->
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-log4j</artifactId>
    <version>1.3.8.RELEASE</version>
  </dependency>
```

### 配置文件

文件名称: log4j.properties

```properties
# log4j.rootLogger=CONSOLE,info,error,DEBUG
log4j.rootLogger=info,error,CONSOLE,DEBUG
log4j.appender.CONSOLE=org.apache.log4j.ConsoleAppender
log4j.appender.CONSOLE.layout=org.apache.log4j.PatternLayout
log4j.appender.CONSOLE.layout.ConversionPattern=%d{yyyy-MM-dd-HH-mm} [%t] [%c] [%p] - %m%n
log4j.logger.info=info
log4j.appender.info=org.apache.log4j.DailyRollingFileAppender
log4j.appender.info.layout=org.apache.log4j.PatternLayout
log4j.appender.info.layout.ConversionPattern=%d{yyyy-MM-dd-HH-mm} [%t] [%c] [%p] - %m%n
log4j.appender.info.datePattern='.'yyyy-MM-dd
log4j.appender.info.Threshold = info
log4j.appender.info.append=true
# log4j.appender.info.File=/home/admin/pms-api-services/logs/info/api_services_info
log4j.appender.info.File=/Users/dddd/Documents/testspace/pms-api-services/logs/info/api_services_info
log4j.logger.error=error
log4j.appender.error=org.apache.log4j.DailyRollingFileAppender
log4j.appender.error.layout=org.apache.log4j.PatternLayout
log4j.appender.error.layout.ConversionPattern=%d{yyyy-MM-dd-HH-mm} [%t] [%c] [%p] - %m%n
log4j.appender.error.datePattern='.'yyyy-MM-dd
log4j.appender.error.Threshold = error
log4j.appender.error.append=true
# log4j.appender.error.File=/home/admin/pms-api-services/logs/error/api_services_error
log4j.appender.error.File=/Users/dddd/Documents/testspace/pms-api-services/logs/error/api_services_error
log4j.logger.DEBUG=DEBUG
log4j.appender.DEBUG=org.apache.log4j.DailyRollingFileAppender
log4j.appender.DEBUG.layout=org.apache.log4j.PatternLayout
log4j.appender.DEBUG.layout.ConversionPattern=%d{yyyy-MM-dd-HH-mm} [%t] [%c] [%p] - %m%n
log4j.appender.DEBUG.datePattern='.'yyyy-MM-dd
log4j.appender.DEBUG.Threshold = DEBUG
log4j.appender.DEBUG.append=true
# log4j.appender.DEBUG.File=/home/admin/pms-api-services/logs/debug/api_services_debug
log4j.appender.DEBUG.File=/Users/dddd/Documents/testspace/pms-api-services/logs/debug/api_services_debug
```

### 使用

```java
private static final Logger logger = LoggerFactory.getLogger(IndexController.class);
```

## 使用AOP统一处理Web请求日志

### POM

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-aop</artifactId>
</dependency>
```

## 代码

```java
@Aspect
@Component
public class WebLogAspect {

	private static final Logger logger = LoggerFactory.getLogger(WebLogAspect.class);

	@Pointcut("execution(public * tech.tengshe789.controller.*.*(..))")
	public void webLog() {
	}

	@Before("webLog()")
	public void doBefore(JoinPoint joinPoint) throws Throwable {
		// 接收到请求,记录请求内容
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = attributes.getRequest();
		// 记录下请求内容
		logger.info("URL : " + request.getRequestURL().toString());
		logger.info("HTTP_METHOD : " + request.getMethod());
		logger.info("IP : " + request.getRemoteAddr());
		Enumeration<String> enu = request.getParameterNames();
		while (enu.hasMoreElements()) {
			String name = (String) enu.nextElement();
			logger.info("name:{},value:{}", name, request.getParameter(name));
		}
	}

	@AfterReturning(returning = "ret", pointcut = "webLog()")
	public void doAfterReturning(Object ret) throws Throwable {
		// 处理完请求,返回内容
		logger.info("RESPONSE : " + ret);
	}
}
```

## lombok 插件

非常简单的办法

### POM

```xml
<dependency>
  <groupId>org.projectlombok</groupId>
  <artifactId>lombok</artifactId>
  <version>1.18.0</version>
</dependency>
```

### 代码

类中添加@Slf4j 注解即可.使用是直接输入log全局变量.

### 其他用法

标签名称|作用
-|-
@Data|生成getter/setter toString()等方法
@NonNull|让你不在担忧并且爱上NullPointerException
@CleanUp|自动资源管理|不用再在finally中添加资源的close方法
@Setter/@Getter|自动生成set和get方法
@ToString|自动生成toString方法
@EqualsAndHashcode|从对象的字段中生成hashCode和equals的实现
@NoArgsConstructor<br>@RequiredArgsConstructor<br>@AllArgsConstructor|自动生成构造方法
@Data|自动生成set/get方法,toString方法,equals方法,hashCode方法,不带参数的构造方法
@Value|用于注解final类
@Builder|产生复杂的构建器api类
@SneakyThrows|异常处理(谨慎使用)
@Synchronized|同步方法安全的转化
@Getter(lazy=true)|
@Log|支持各种logger对象,使用时用对应的注解,如:@Log4

# 拦截器

在AOP(Aspect-Oriented Programming)中用于在某个方法或字段被访问之前,进行拦截,然后在之前或之后加入某些操作.<br>拦截是AOP的一种实现策略.

0. 拦截器是基于java的反射机制的,而过滤器是基于函数回调.
0. 拦截器不依赖于servlet容器,而过滤器依赖于servlet容器.
0. 拦截器只能对Controller请求起作用,而过滤器则可以对几乎所有的请求起作用.
0. 在Controller的生命周期中,拦截器可以多次被调用,而过滤器只能在容器初始化时被调用一次.

过滤器(filter)和拦截器(interceptor)是有区别的,的执行顺序: 先filter 后 interceptor

名称|应用场景
-|-
过滤器|设置编码字符、过滤铭感字符
拦截器|拦截未登陆用户、审计日志

## 自定义拦截器

### 注册拦截器

```java
@Configuration
public class WebAppConfig {
	@Autowired
	private LoginIntercept loginIntercept;

	@Bean
	public WebMvcConfigurer WebMvcConfigurer() {
		return new WebMvcConfigurer() {
			public void addInterceptors(InterceptorRegistry registry) {
				registry.addInterceptor(loginIntercept).addPathPatterns("/*");
			};
		};
	}

}
```

创建模拟登录拦截器,验证请求是否有token参数

```java
@Slf4j
@Component
public class LoginIntercept implements HandlerInterceptor {

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		log.info("开始拦截登录请求....");
		String token = request.getParameter("token");
		if (StringUtils.isEmpty(token)) {
			response.getWriter().println("not found token");
			return false;
		}
		return true;
	}
}
```

# 缓存

在 Spring Boot中,通过@EnableCaching注解自动化配置合适的缓存管理器(CacheManager),<br>
Spring Boot根据下面的顺序去侦测缓存提供者:

0. Generic
0. JCache (JSR-107)
0. EhCache 2.x
0. Hazelcast
0. Infinispan
0. Redis
0. Guava
0. Simple

## EhCache

### POM

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-cache</artifactId>
</dependency>
```

新建ehcache.xml 文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ehcache xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://ehcache.org/ehcache.xsd" updateCheck="false">
	<diskStore path="java.io.tmpdir/Tmp_EhCache" />

	<!-- 默认配置 -->
	<defaultCache maxElementsInMemory="5000" eternal="false"
		timeToIdleSeconds="120" timeToLiveSeconds="120"
		memoryStoreEvictionPolicy="LRU" overflowToDisk="false" />

	<cache name="baseCache" maxElementsInMemory="10000"
		maxElementsOnDisk="100000" />

</ehcache>
```

### 配置信息介绍

名称|作用
-|-
name|缓存名称.
maxElementsInMemory|缓存最大个数.
eternal|对象是否永久有效,一但设置了,timeout将不起作用.
timeToIdleSeconds|设置对象在失效前的允许闲置时间(单位:秒).仅当eternal=false对象不是永久有效时使用,可选属性,默认值是0,也就是可闲置时间无穷大.
timeToLiveSeconds|设置对象在失效前允许存活时间(单位|秒).最大时间介于创建时间和失效时间之间.仅当eternal=false对象不是永久有效时使用,默认是0.,也就是对象存活时间无穷大.
overflowToDisk|当内存中对象数量达到maxElementsInMemory时,Ehcache将会对象写到磁盘中.
diskSpoolBufferSizeMB|这个参数设置DiskStore(磁盘缓存)的缓存区大小.默认是30MB.每个Cache都应该有自己的一个缓冲区.
maxElementsOnDisk|硬盘最大缓存个数.
diskPersistent|是否缓存虚拟机重启期数据 Whether the disk store persists between restarts of the Virtual Machine. The default value is false.
diskExpiryThreadIntervalSeconds|磁盘失效线程运行时间间隔,默认是120秒.
memoryStoreEvictionPolicy|当达到maxElementsInMemory限制时,Ehcache将会根据指定的策略去清理内存.默认策略是LRU(最近最少使用).你可以设置为FIFO(先进先出)或是LFU(较少使用).
clearOnFlush|内存数量最大时是否清除.

### 关于注解和代码使用

```java
@CacheConfig(cacheNames = "baseCache")
public interface UserDao {
	@Select("select * from users where name=#{name}")
	@Cacheable
	UserEntity findName(@Param("name") String name);
}
```

### 清除缓存

```java
@Autowired
private CacheManager cacheManager;
@RequestMapping("/remoKey")
public void remoKey() {
	cacheManager.getCache("baseCache").clear();
}
```

### 启动

主方法启动时加上@EnableCaching即可

## Redis

### 使用自带驱动器连接

使用RedisTemplate 连接

#### POM

```properties
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

#### 配置文件

##### 单机

```properties
#redis
# Redis数据库索引(默认为0)
spring.redis.database=0
# Redis服务器地址
spring.redis.host=127.0.0.1
# Redis服务器连接端口
spring.redis.port=6379
# Redis服务器连接密码(默认为空)
spring.redis.password=
# 连接池最大连接数(使用负值表示没有限制)
spring.redis.pool.max-active=8
# 连接池最大阻塞等待时间(使用负值表示没有限制)
spring.redis.pool.max-wait=-1
# 连接池中的最大空闲连接
spring.redis.pool.max-idle=8
# 连接池中的最小空闲连接
spring.redis.pool.min-idle=0
# 连接超时时间(毫秒)
spring.redis.timeout=0
```

##### 集群或哨兵模式

```properties
# Matser的ip地址
redis.hostName=192.168.177.128
# 端口号
redis.port=6382
# 如果有密码
redis.password=
# 客户端超时时间单位是毫秒 默认是2000
redis.timeout=10000

# 最大空闲数
redis.maxIdle=300
# 连接池的最大数据库连接数.设为0表示无限制,如果是jedis 2.4以后用redis.maxTotal
# redis.maxActive=600
# 控制一个pool可分配多少个jedis实例,用来替换上面的redis.maxActive,如果是jedis 2.4以后用该属性
redis.maxTotal=1000
# 最大建立连接等待时间.如果超过此时间将接到异常.设为-1表示无限制.
redis.maxWaitMillis=1000
# 连接的最小空闲时间 默认1800000毫秒(30分钟)
redis.minEvictableIdleTimeMillis=300000
# 每次释放连接的最大数目,默认3
redis.numTestsPerEvictionRun=1024
# 逐出扫描的时间间隔(毫秒) 如果为负数,则不运行逐出线程, 默认-1
redis.timeBetweenEvictionRunsMillis=30000
# 是否在从池中取出连接前进行检验,如果检验失败,则从池中去除连接并尝试取出另一个
redis.testOnBorrow=true
# 在空闲时检查有效性, 默认false
redis.testWhileIdle=true
```

###### redis集群配置

```properties
spring.redis.cluster.nodes=192.168.177.128:7001,192.168.177.128:7002,192.168.177.128:7003,192.168.177.128:7004,192.168.177.128:7005,192.168.177.128:7006
spring.redis.cluster.max-redirects=3

#哨兵模式
#redis.sentinel.host1=192.168.177.128
#redis.sentinel.port1=26379

#redis.sentinel.host2=172.20.1.231
#redis.sentinel.port2=26379
```

#### 配置类

```java
@Configuration
@EnableCaching
public class RedisConfig extends CachingConfigurerSupport{

    @Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.port}")
    private int port;
    @Value("${spring.redis.timeout}")
    private int timeout;

    //自定义缓存key生成策略
//    @Bean
//    public KeyGenerator keyGenerator() {
//        return new KeyGenerator(){
//            @Override
//            public Object generate(Object target, java.lang.reflect.Method method, Object... params) {
//                StringBuffer sb = new StringBuffer();
//                sb.append(target.getClass().getName());
//                sb.append(method.getName());
//                for(Object obj:params){
//                    sb.append(obj.toString());
//                }
//                return sb.toString();
//            }
//        };
//    }
    //缓存管理器
    @Bean
    public CacheManager cacheManager(@SuppressWarnings("rawtypes") RedisTemplate redisTemplate) {
        RedisCacheManager cacheManager = new RedisCacheManager(redisTemplate);
        //设置缓存过期时间
        cacheManager.setDefaultExpiration(10000);
        return cacheManager;
    }
    @Bean
    public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory factory){
      StringRedisTemplate template = new StringRedisTemplate(factory);
      setSerializer(template);//设置序列化工具
      template.afterPropertiesSet();
      return template;
    }
     private void setSerializer(StringRedisTemplate template){
        @SuppressWarnings({ "rawtypes", "unchecked" })
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        template.setValueSerializer(jackson2JsonRedisSerializer);
     }
}
```

#### Dao

```java
@Mapper
@CacheConfig(cacheNames = "users")
public interface UserMapper {

  @Insert("insert into user(name,age) values(#{name},#{age})")
  int addUser(@Param("name")String name,@Param("age")String age);

  @Select("select * from user where id =#{id}")
  @Cacheable(key ="#p0")
  User findById(@Param("id") String id);

  @CachePut(key = "#p0")
  @Update("update user set name=#{name} where id=#{id}")
  void updataById(@Param("id")String id,@Param("name")String name);

  //如果指定为 true,则方法调用后将立即清空所有缓存
  @CacheEvict(key ="#p0",allEntries=true)
  @Delete("delete from user where id=#{id}")
  void deleteById(@Param("id")String id);

}
```

@Cacheable将查询结果缓存到redis中,(key=”#p0”)指定传入的第一个参数作为redis的key.

@CachePut,指定key,将更新的结果同步到redis中

@CacheEvict,指定key,删除缓存数据,allEntries=true,方法调用后将立即清除缓存

## 使用Jedis连接

要注意,redis在5.0版本以后不支持Jedis

### POM

```xml
<dependency>
  <groupId>redis.clients</groupId>
  <artifactId>jedis</artifactId>
</dependency>
```

### 配置类

```java
@Data
@Component
@ConfigurationProperties(prefix="redis")
public class RedisConfig {
  private String host;
  private int port;
  private int timeout;//秒
  private String password;
  private int poolMaxTotal;
  private int poolMaxIdle;
  private int poolMaxWait;//秒
}

@Service
public class RedisPoolFactory {

  @Autowired
  RedisConfig redisConfig;

  @Bean
  public JedisPool edisPoolFactory() {
    JedisPoolConfig poolConfig = new JedisPoolConfig();
    poolConfig.setMaxIdle(redisConfig.getPoolMaxIdle());
    poolConfig.setMaxTotal(redisConfig.getPoolMaxTotal());
    poolConfig.setMaxWaitMillis(redisConfig.getPoolMaxWait() * 1000);
    JedisPool jp = new JedisPool(poolConfig, redisConfig.getHost(), redisConfig.getPort(),
            redisConfig.getTimeout()*1000, redisConfig.getPassword(), 0);
    return jp;
  }

}
```

# 监控中心

Springboot监控中心是针对微服务的服务状态、Http请求资源进行监控:

0. 可以看到服务器内存变化(堆内存、线程、日志管理)
0. 可以检测服务配置连接地址是否可用(模拟访问,懒加载)
0. 可以统计有多少Bean有什么单例多例
0. 可以统计SpringMVC有多少@RequestMapping

## Actuator

Actuator是spring boot的一个附加功能,可帮助你在应用程序生产环境时监视和管理应用程序.

可以使用HTTP的各种请求来监管,审计,收集应用的运行情况.返回的是json

缺点:没有可视化界面.

在springboot2.0中,Actuator的端点(endpoint)现在默认映射到/application,比如,/info 端点现在就是在/application/info.<br>
但可以使用management.context-path来覆盖此默认值.

### POM

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

### 配置信息

```yml
# Actuator 通过下面的配置启用所有的监控端点,默认情况下,这些端点是禁用的;
management:
  endpoints:
    web:
      exposure:
        include: "*"
spring:
  profiles:
    active: prod
  datasource:
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/test
    username: root
    password: 123456
```

### Actuator访问路径

通过actuator/+端点名就可以获取相应的信息.

路径|作用
-|-
/actuator/beans|显示应用程序中所有Spring bean的完整列表.
/actuator/configprops|显示所有配置信息.
/actuator/env|陈列所有的环境变量.
/actuator/mappings|显示所有@RequestMapping的url整理列表.
/actuator/health|显示应用程序运行状况信息 up表示成功 down失败
/actuator/info|查看自定义应用信息

## Admin-UI分布式微服务监控中心

Admin-UI底层使用actuator,实现监控信息 的界面

### POM

```xml
<!--服务端-->
<dependency>
	<groupId>de.codecentric</groupId>
	<artifactId>spring-boot-admin-starter-server</artifactId>
	<version>2.0.0</version>
</dependency>
<!--客户端-->
<dependency>
	<groupId>de.codecentric</groupId>
	<artifactId>spring-boot-admin-starter-client</artifactId>
	<version>2.0.0</version>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
<dependency>
	<groupId>org.jolokia</groupId>
	<artifactId>jolokia-core</artifactId>
</dependency>
<dependency>
	<groupId>com.googlecode.json-simple</groupId>
	<artifactId>json-simple</artifactId>
	<version>1.1</version>
```

### application.yml

```yml
//服务端
spring:
  application:
    name: spring-boot-admin-server
//客户端
spring:
  boot:
    admin:
      client:
        url: http://localhost:8080
server:
  port: 8081

management:
  endpoints:
    web:
      exposure:
        include: "*"
  endpoint:
    health:
      show-details: ALWAYS
```

# 性能优化

## 扫包优化

默认情况下,会使用 @SpringBootApplication 注解来自动获取应用的配置信息,但这样也会给应用带来一些副作用.<br>
使用这个注解后,会触发自动配置( auto-configuration )和 组件扫描 ( component scanning ),这跟使用 @Configuration、@EnableAutoConfiguration 和 @ComponentScan 三个注解的作用是一样的.<br>
这样做给开发带来方便的同时,也会有三方面的影响:

0. 会导致项目启动时间变长.当启动一个大的应用程序,或将做大量的集成测试启动应用程序时,影响会特别明显.
0. 会加载一些不需要的多余的实例(beans).
0. 会增加 CPU 消耗.

针对以上三个情况,可以移除 @SpringBootApplication 和 @ComponentScan 两个注解来禁用组件自动扫描,然后在需要的 bean 上进行显式配置.

## SpringBoot JVM参数调优

### 各种参数

参数名称|含义|默认值|
-|-|-
-Xms|初始堆大小|物理内存的1/64(<1GB)|默认(MinHeapFreeRatio参数可以调整)空余堆内存小于40%时,JVM就会增大堆直到-Xmx的最大限制.
-Xmx|最大堆大小|物理内存的1/4(<1GB)|默认(MaxHeapFreeRatio参数可以调整)空余堆内存大于70%时,JVM会减少堆直到 -Xms的最小限制
-Xmn|年轻代大小(1.4or lator)|	注意:此处的大小是(eden+ 2 survivor space).与jmap -heap中显示的New gen是不同的. 整个堆大小=年轻代大小 + 年老代大小 + 持久代大小. 增大年轻代后,将会减小年老代大小.此值对系统性能影响较大,Sun官方推荐配置为整个堆的3/8
-XX:NewSize|设置年轻代大小(for 1.3/1.4)|
-XX:MaxNewSize|年轻代最大值(for 1.3/1.4)|
-XX:PermSize|设置持久代(perm gen)初始值|物理内存的1/64|
-XX:MaxPermSize|设置持久代最大值|物理内存的1/4|
-Xss|每个线程的堆栈大小|	JDK5.0以后每个线程堆栈大小为1M,以前每个线程堆栈大小为256K.更具应用的线程所需内存大小进行 调整.在相同物理内存下,减小这个值能生成更多的线程.但是操作系统对一个进程内的线程数还是有限制的,不能无限生成,经验值在3000~5000左右 一般小的应用, 如果栈不是很深, 应该是128k够用的 大的应用建议使用256k.这个选项对性能影响比较大,需要严格的测试.(校长) 和threadstacksize选项解释很类似,官方文档似乎没有解释,在论坛中有这样一句话:”” -Xss is translated in a VM flag named ThreadStackSize” 一般设置这个值就可以了.
-XX:ThreadStackSize|Thread Stack Size|	(0 means use default stack size) [Sparc: 512; Solaris x86: 320 (was 256 prior in 5.0 and earlier); Sparc 64 bit: 1024; Linux amd64: 1024 (was 0 in 5.0 and earlier); all others 0.]
-XX:NewRatio|年轻代(包括Eden和两个Survivor区)与年老代的比值(除去持久代)|	-XX:NewRatio=4表示年轻代与年老代所占比值为1:4,年轻代占整个堆栈的1/5 Xms=Xmx并且设置了Xmn的情况下,该参数不需要进行设置.
-XX:SurvivorRatio|Eden区与Survivor区的大小比值|	设置为8,则两个Survivor区与一个Eden区的比值为2:8,一个Survivor区占整个年轻代的1/10
-XX:LargePageSizeInBytes|内存页的大小不可设置过大, 会影响Perm的大小|	=128m
-XX:+UseFastAccessorMethods|原始类型的快速优化|
-XX:+DisableExplicitGC|关闭System.gc()|	这个参数需要严格的测试
-XX:MaxTenuringThreshold|垃圾最大年龄|	如果设置为0的话,则年轻代对象不经过Survivor区,直接进入年老代. 对于年老代比较多的应用,可以提高效率.如果将此值设置为一个较大值,则年轻代对象会在Survivor区进行多次复制,这样可以增加对象再年轻代的存活 时间,增加在年轻代即被回收的概率 该参数只有在串行GC时才有效.
-XX:+AggressiveOpts|加快编译|
-XX:+UseBiasedLocking|锁机制的性能改善|
-Xnoclassgc|禁用垃圾回收|
-XX:SoftRefLRUPolicyMSPerMB|每兆堆空闲空间中SoftReference的存活时间|1s|softly reachable objects will remain alive for some amount of time after the last time they were referenced. The default value is one second of lifetime per free megabyte in the heap
-XX:PretenureSizeThreshold|对象超过多大是直接在旧生代分配|0|单位字节 新生代采用Parallel Scavenge GC时无效 另一种直接在旧生代分配的情况是大的数组对象,且数组中无外部引用对象.
-XX:TLABWasteTargetPercent|TLAB占eden区的百分比|1%|
-XX:+CollectGen0First|FullGC时是否先YGC|false

### 调优策略

0. 初始化堆内存和最大堆相同
0. 减少垃圾回收次数

## 将Servlet容器从Tomcat变成Undertow

Undertow是一个采用Java开发的灵活的高性能Web服务器,提供包括阻塞和基于NIO的非堵塞机制.<br>
Undertow是红帽公司的开源产品,是JBoss默认的Web服务器.

### Undertow

#### POM

首先,从依赖信息里移除 Tomcat 配置

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
	<exclusions>
		<exclusion>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
		</exclusion>
	</exclusions>
</dependency>
```

然后添加 Undertow:

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-undertow</artifactId>
</dependency>
```

## Tomcat 优化

见 [Spring Boot Memory Performance](https://spring.io/blog/2015/12/10/spring-boot-memory-performance)

# 热部署

热部署,就是在应用程序在不停止的情况下,自动实现新的部署

## 原理

使用类加载器classroad来检测字节码文件,然后重新加载到jvm内存中.

0. 检测本地.class文件变动(版本号,修改时间不一样)
0. 自动监听,实现部署

## 应用场景

本地开发时,可以提高运行环境.

## Dev-tools

spring-boot-devtools 是一个为开发者服务的一个模块,其中最重要的功能就是自动应用代码更改到最新的App上面去

### POM

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-devtools</artifactId>
	<optional>true</optional>
	<scope>true</scope>
</dependency>
```

### 原理

0. devtools会监听classpath下的文件变动,并且会立即重启应用(发生在保存时机),因为其采用的虚拟机机制,该项重启是很快的.
0. devtools可以实现页面热部署(即页面修改后会立即生效,这个可以直接在application.properties文件中配置spring.thymeleaf.cache=false来实现(注意:不同的模板配置不一样).


# 发布打包

## Jar类型打包方式

0. 使用mvn clean package 打包
0. 使用java –jar 包名

## war类型打包方式

0. 使用mvn celan package 打包
0. 使用java –jar 包名

## 外部Tomcat运行

0. 使用mvn celan package 打包
0. 将war包 放入到tomcat webapps下运行即可.

注意:springboot2.0内置tomcat8.5.25,建议使用外部Tomcat9.0版本运行即可,否则报错版本不兼容.

## POM

```xml
<build>
  <plugins>
    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-compiler-plugin</artifactId>
      <configuration>
        <source>1.8</source>
        <target>1.8</target>
      </configuration>
    </plugin>
    <plugin>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-maven-plugin</artifactId>
      <configuration>
        <maimClass>com.itmayiedu.app.App</maimClass>
      </configuration>
      <executions>
        <execution>
          <goals>
            <goal>repackage</goal>
          </goals>
        </execution>
      </executions>

    </plugin>
  </plugins>
</build>
```

# 常见问题

## Controller

### Autowired cannot be resolved to a type

> \* cannot be resolved to a type 都是缺少依赖的表现

引入:

```java
import org.springframework.beans.factory.annotation.Autowired;
```

## Mapper

### Consider defining a bean of type * in your configuration.

#### 添加@Mapper注解

Mapper中

```java
@Mapper
public interface MessageMapper {}
```

### 启动类中添加扫包路径

```java
@MapperScan("co.imdo.ff4c00.mapper")
public class Application {}
```

# 参考资料

> [tEngSHe789の小站 | 这是一篇优雅的Springboot2.0使用手册](https://blog.tengshe789.tech/2018/08/04/springboot/?hmsr=toutiao.io&utm_medium=toutiao.io&utm_source=toutiao.io)

> [SpringIO](https://spring.io/guides)

> [Spring Boot 中文索引](http://springboot.fun/)

> [易百教程 | Spring Boot教程](https://www.yiibai.com/spring-boot/)

> [Github | spring-boot-examples](https://github.com/ityouknow/spring-boot-examples)

> [Github | awesome-spring-boot](https://github.com/ityouknow/awesome-spring-boot)

> [segmentfault | Springboot 之 引入Thymeleaf](https://segmentfault.com/a/1190000011149325)

> [简书 | spring boot 项目在启动时执行指定sql文件](https://www.jianshu.com/p/88125f1cf91c)

> [CSDN | SpringBoot整合Elasticsearch](https://blog.csdn.net/chen_2890/article/details/83895646)