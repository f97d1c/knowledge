> JSAPI支付是用户在微信中打开商户的H5页面,<br>
商户在H5页面通过调用微信支付提供的JSAPI接口调起微信支付模块完成支付.

应用场景有:

0. 用户在微信公众账号内进入商家公众号,打开某个主页面,完成支付.
0. 用户的好友在朋友圈、聊天窗口等分享商家页面连接,用户点击链接打开商家页面,完成支付.
0. 将商户页面转换成二维码,用户扫描二维码后在微信浏览器中打开页面后完成支付.

<!-- TOC -->

- [获取微信版本号](#获取微信版本号)
- [接口规则](#接口规则)
  - [协议规则](#协议规则)
  - [参数规定](#参数规定)
    - [交易金额](#交易金额)
    - [交易类型](#交易类型)
    - [货币类型](#货币类型)
    - [时间](#时间)
    - [时间戳](#时间戳)
    - [商户订单号](#商户订单号)
    - [body字段格式](#body字段格式)
    - [银行类型](#银行类型)
  - [安全规范](#安全规范)
    - [签名算法](#签名算法)
    - [签名生成的通用步骤](#签名生成的通用步骤)
      - [参数名ASCII码排序](#参数名ascii码排序)
      - [MD5运算](#md5运算)
      - [示例](#示例)
    - [生成随机数算法](#生成随机数算法)
    - [API证书](#api证书)
      - [获取API证书](#获取api证书)
      - [使用API证书](#使用api证书)
      - [API证书安全](#api证书安全)
      - [商户回调API安全](#商户回调api安全)
- [业务流程](#业务流程)
  - [业务角色](#业务角色)
  - [时序图](#时序图)
  - [文字版](#文字版)
    - [生成预付单](#生成预付单)
    - [发起支付](#发起支付)
    - [支付后流程](#支付后流程)
- [统一下单接口](#统一下单接口)
  - [请求参数](#请求参数)
  - [返回结果](#返回结果)
  - [错误码](#错误码)
  - [notify_url填写注意事项](#notify_url填写注意事项)
- [支付结果通知接口](#支付结果通知接口)
  - [接口链接](#接口链接)
  - [通知参数](#通知参数)
  - [返回参数](#返回参数)
  - [notify_url处理注意事项](#notify_url处理注意事项)
    - [返回报文格式规范](#返回报文格式规范)
    - [回调处理逻辑注意事项](#回调处理逻辑注意事项)
- [查询订单接口](#查询订单接口)
  - [应用场景](#应用场景)
  - [需要调用查询接口的情况](#需要调用查询接口的情况)
  - [接口链接](#接口链接-1)
  - [请求参数](#请求参数-1)
  - [返回结果](#返回结果-1)
  - [错误码](#错误码-1)
- [参考资料](#参考资料)

<!-- /TOC -->

# 获取微信版本号

微信5.0版本后加入微信支付模块,低版本用户调用微信支付功能将无效.<br>
因此,可通过HTTP头中user agent来确定用户当前的版本号后再调用支付接口.

以iPhone版本为例,可以通过user agent可获取如下微信版本示例信息:

```
"Mozilla/5.0(iphone;CPU iphone OS 5_1_1 like Mac OS X) AppleWebKit/534.46(KHTML,like Geocko) Mobile/9B206 MicroMessenger/5.0"
```

其中5.0为用户安装的微信版本号.

# 接口规则

## 协议规则

商户接入微信支付,调用API必须遵循以下规则:

&nbsp;|&nbsp;
-|-
传输方式|为保证交易安全性,采用HTTPS传输
提交方式|采用POST方法提交
数据格式|提交和返回数据都为XML格式,根节点名为xml
字符编码|统一采用UTF-8字符编码
签名算法|MD5/HMAC-SHA256
签名要求|请求和接收数据均需要校验签名,<br>详细方法请参考安全规范-签名算法
证书要求|调用申请退款、撤销订单、红包接口等<br>需要商户api证书,各api接口文档均有说明.
判断逻辑|先判断协议字段返回,再判断业务返回,最后判断交易状态

***必须严格按照API的说明进行一单一支付,一单一红包,一单一付款,在未得到支付系统明确的回复之前不要换单,防止重复支付或者重复付款***

## 参数规定

### 交易金额

交易金额默认为人民币交易,接口中参数支付金额单位为 **分**,参数值不能带小数.<br>
对账单中的交易金额单位为 **元**.

外币交易的**支付金额精确到币种的最小单位**,参数值不能带小数点.

### 交易类型

trade_type|说明
-|-
JSAPI|JSAPI支付(或小程序支付)
NATIVE|Native支付
APP|app支付
MWEB|H5支付

不同trade_type决定了调起支付的方式,请根据支付产品正确上传

### 货币类型

境内商户号仅支持人民币

CNY:人民币

### 时间

标准北京时间,时区为东八区.<br>
如果商户的系统时间为非标准北京时间,参数值必须根据商户系统所在时区先换算成标准北京时间. 

### 时间戳

标准北京时间,时区为东八区,自1970年1月1日 0点0分0秒以来的秒数.<br>
注意:部分系统取到的值为毫秒级,需要转换成秒(10位数字).

### 商户订单号

商户支付的订单号由商户 **自定义生成**,仅支持使用英文半角字符的组合,**请勿使用汉字或全角等特殊字符**.<br>
微信支付要求商户订单号保持唯一性(建议根据当前系统时间加随机序列来生成订单号).<br>
重新发起一笔支付要使用原订单号,避免重复支付.<br>
已支付过或已调用关单、撤销的订单号不能重新发起支付.

### body字段格式

使用场景|支付模式|商品字段规则|样例|备注
-|-|-|-|-
PC网站|扫码支付|浏览器打开的网站主页title名 -商品概述|腾讯充值中心-QQ会员充值|
微信浏览器|公众号支付|商家名称-销售商品类目|腾讯-游戏|线上电商,商家名称必须为实际销售商品的商家
门店扫码|公众号支付|店名-销售商品类目|小张南山店-超市|线下门店支付
门店扫码|扫码支付|店名-销售商品类目|小张南山店-超市|线下门店支付
门店付款码|付款码支付|店名-销售商品类目|小张南山店-超市|线下门店支付
第三方手机浏览器|H5支付|浏览器打开的移动网页的主页title名-商品概述|腾讯充值中心-QQ会员充值|
第三方APP|APP支付|应用市场上的APP名字-商品概述|天天爱消除-游戏充值|

### 银行类型

字符型银行编码|银行名称
-|-
ICBC_DEBIT|工商银行(借记卡)
ICBC_CREDIT|工商银行(信用卡)
ABC_DEBIT|农业银行(借记卡)
ABC_CREDIT|农业银行(信用卡)
PSBC_DEBIT|邮政储蓄银行(借记卡)
PSBC_CREDIT|邮政储蓄银行(信用卡)
CCB_DEBIT|建设银行(借记卡)
CCB_CREDIT|建设银行(信用卡)
CMB_DEBIT|招商银行(借记卡)
CMB_CREDIT|招商银行(信用卡)
BOC_DEBIT|中国银行(借记卡)
BOC_CREDIT|中国银行(信用卡)
COMM_DEBIT|交通银行(借记卡)
COMM_CREDIT|交通银行(信用卡)
SPDB_DEBIT|浦发银行(借记卡)
SPDB_CREDIT|浦发银行(信用卡)
GDB_DEBIT|广发银行(借记卡)
GDB_CREDIT|广发银行(信用卡)
CMBC_DEBIT|民生银行(借记卡)
CMBC_CREDIT|民生银行(信用卡)
PAB_DEBIT|平安银行(借记卡)
PAB_CREDIT|平安银行(信用卡)
CEB_DEBIT|光大银行(借记卡)
CEB_CREDIT|光大银行(信用卡)
CIB_DEBIT|兴业银行(借记卡)
CIB_CREDIT|兴业银行(信用卡)
CITIC_DEBIT|中信银行(借记卡)
CITIC_CREDIT|中信银行(信用卡)
BOSH_DEBIT|上海银行(借记卡)
BOSH_CREDIT|上海银行(信用卡)
CRB_DEBIT|华润银行(借记卡)
HZB_DEBIT|杭州银行(借记卡)
HZB_CREDIT|杭州银行(信用卡)
BSB_DEBIT|包商银行(借记卡)
BSB_CREDIT|包商银行(信用卡)
CQB_DEBIT|重庆银行(借记卡)
SDEB_DEBIT|顺德农商行(借记卡)
SZRCB_DEBIT|深圳农商银行(借记卡)
SZRCB_CREDIT|深圳农商银行(信用卡)
HRBB_DEBIT|哈尔滨银行(借记卡)
BOCD_DEBIT|成都银行(借记卡)
GDNYB_DEBIT|南粤银行(借记卡)
GDNYB_CREDIT|南粤银行(信用卡)
GZCB_DEBIT|广州银行(借记卡)
GZCB_CREDIT|广州银行(信用卡)
JSB_DEBIT|江苏银行(借记卡)
JSB_CREDIT|江苏银行(信用卡)
NBCB_DEBIT|宁波银行(借记卡)
NBCB_CREDIT|宁波银行(信用卡)
NJCB_DEBIT|南京银行(借记卡)
QHNX_DEBIT|青海农信(借记卡)
ORDOSB_CREDIT|鄂尔多斯银行(信用卡)
ORDOSB_DEBIT|鄂尔多斯银行(借记卡)
BJRCB_CREDIT|北京农商(信用卡)
BHB_DEBIT|河北银行(借记卡)
BGZB_DEBIT|贵州银行(借记卡)
BEEB_DEBIT|鄞州银行(借记卡)
PZHCCB_DEBIT|攀枝花银行(借记卡)
QDCCB_CREDIT|青岛银行(信用卡)
QDCCB_DEBIT|青岛银行(借记卡)
SHINHAN_DEBIT|新韩银行(借记卡)
QLB_DEBIT|齐鲁银行(借记卡)
QSB_DEBIT|齐商银行(借记卡)
ZZB_DEBIT|郑州银行(借记卡)
CCAB_DEBIT|长安银行(借记卡)
RZB_DEBIT|日照银行(借记卡)
SCNX_DEBIT|四川农信(借记卡)
BEEB_CREDIT|鄞州银行(信用卡)
SDRCU_DEBIT|山东农信(借记卡)
BCZ_DEBIT|沧州银行(借记卡)
SJB_DEBIT|盛京银行(借记卡)
LNNX_DEBIT|辽宁农信(借记卡)
JUFENGB_DEBIT|临朐聚丰村镇银行(借记卡)
ZZB_CREDIT|郑州银行(信用卡)
JXNXB_DEBIT|江西农信(借记卡)
JZB_DEBIT|晋中银行(借记卡)
JZCB_CREDIT|锦州银行(信用卡)
JZCB_DEBIT|锦州银行(借记卡)
KLB_DEBIT|昆仑银行(借记卡)
KRCB_DEBIT|昆山农商(借记卡)
KUERLECB_DEBIT|库尔勒市商业银行(借记卡)
LJB_DEBIT|龙江银行(借记卡)
NYCCB_DEBIT|南阳村镇银行(借记卡)
LSCCB_DEBIT|乐山市商业银行(借记卡)
LUZB_DEBIT|柳州银行(借记卡)
LWB_DEBIT|莱商银行(借记卡)
LYYHB_DEBIT|辽阳银行(借记卡)
LZB_DEBIT|兰州银行(借记卡)
MINTAIB_CREDIT|民泰银行(信用卡)
MINTAIB_DEBIT|民泰银行(借记卡)
NCB_DEBIT|宁波通商银行(借记卡)
NMGNX_DEBIT|内蒙古农信(借记卡)
XAB_DEBIT|西安银行(借记卡)
WFB_CREDIT|潍坊银行(信用卡)
WFB_DEBIT|潍坊银行(借记卡)
WHB_CREDIT|威海商业银行(信用卡)
WHB_DEBIT|威海市商业银行(借记卡)
WHRC_CREDIT|武汉农商(信用卡)
WHRC_DEBIT|武汉农商行(借记卡)
WJRCB_DEBIT|吴江农商行(借记卡)
WLMQB_DEBIT|乌鲁木齐银行(借记卡)
WRCB_DEBIT|无锡农商(借记卡)
WZB_DEBIT|温州银行(借记卡)
XAB_CREDIT|西安银行(信用卡)
WEB_DEBIT|微众银行(借记卡)
XIB_DEBIT|厦门国际银行(借记卡)
XJRCCB_DEBIT|新疆农信银行(借记卡)
XMCCB_DEBIT|厦门银行(借记卡)
YNRCCB_DEBIT|云南农信(借记卡)
YRRCB_CREDIT|黄河农商银行(信用卡)
YRRCB_DEBIT|黄河农商银行(借记卡)
YTB_DEBIT|烟台银行(借记卡)
ZJB_DEBIT|紫金农商银行(借记卡)
ZJLXRB_DEBIT|兰溪越商银行(借记卡)
ZJRCUB_CREDIT|浙江农信(信用卡)
AHRCUB_DEBIT|安徽省农村信用社联合社(借记卡)
BCZ_CREDIT|沧州银行(信用卡)
SRB_DEBIT|上饶银行(借记卡)
ZYB_DEBIT|中原银行(借记卡)
ZRCB_DEBIT|张家港农商行(借记卡)
SRCB_CREDIT|上海农商银行(信用卡)
SRCB_DEBIT|上海农商银行(借记卡)
ZJTLCB_DEBIT|浙江泰隆银行(借记卡)
SUZB_DEBIT|苏州银行(借记卡)
SXNX_DEBIT|山西农信(借记卡)
SXXH_DEBIT|陕西信合(借记卡)
ZJRCUB_DEBIT|浙江农信(借记卡)
AE_CREDIT|AE(信用卡)
TACCB_CREDIT|泰安银行(信用卡)
TACCB_DEBIT|泰安银行(借记卡)
TCRCB_DEBIT|太仓农商行(借记卡)
TJBHB_CREDIT|天津滨海农商行(信用卡)
TJBHB_DEBIT|天津滨海农商行(借记卡)
TJB_DEBIT|天津银行(借记卡)
TRCB_DEBIT|天津农商(借记卡)
TZB_DEBIT|台州银行(借记卡)
URB_DEBIT|联合村镇银行(借记卡)
DYB_CREDIT|东营银行(信用卡)
CSRCB_DEBIT|常熟农商银行(借记卡)
CZB_CREDIT|浙商银行(信用卡)
CZB_DEBIT|浙商银行(借记卡)
CZCB_CREDIT|稠州银行(信用卡)
CZCB_DEBIT|稠州银行(借记卡)
DANDONGB_CREDIT|丹东银行(信用卡)
DANDONGB_DEBIT|丹东银行(借记卡)
DLB_CREDIT|大连银行(信用卡)
DLB_DEBIT|大连银行(借记卡)
DRCB_CREDIT|东莞农商银行(信用卡)
DRCB_DEBIT|东莞农商银行(借记卡)
CSRCB_CREDIT|常熟农商银行(信用卡)
DYB_DEBIT|东营银行(借记卡)
DYCCB_DEBIT|德阳银行(借记卡)
FBB_DEBIT|富邦华一银行(借记卡)
FDB_DEBIT|富滇银行(借记卡)
FJHXB_CREDIT|福建海峡银行(信用卡)
FJHXB_DEBIT|福建海峡银行(借记卡)
FJNX_DEBIT|福建农信银行(借记卡)
FUXINB_DEBIT|阜新银行(借记卡)
BOCDB_DEBIT|承德银行(借记卡)
JSNX_DEBIT|江苏农商行(借记卡)
BOLFB_DEBIT|廊坊银行(借记卡)
CCAB_CREDIT|长安银行(信用卡)
CBHB_DEBIT|渤海银行(借记卡)
CDRCB_DEBIT|成都农商银行(借记卡)
BYK_DEBIT|营口银行(借记卡)
BOZ_DEBIT|张家口市商业银行(借记卡)
CFT|零钱
BOTSB_DEBIT|唐山银行(借记卡)
BOSZS_DEBIT|石嘴山银行(借记卡)
BOSXB_DEBIT|绍兴银行(借记卡)
BONX_DEBIT|宁夏银行(借记卡)
BONX_CREDIT|宁夏银行(信用卡)
GDHX_DEBIT|广东华兴银行(借记卡)
BOLB_DEBIT|洛阳银行(借记卡)
BOJX_DEBIT|嘉兴银行(借记卡)
BOIMCB_DEBIT|内蒙古银行(借记卡)
BOHN_DEBIT|海南银行(借记卡)
BOD_DEBIT|东莞银行(借记卡)
CQRCB_CREDIT|重庆农商银行(信用卡)
CQRCB_DEBIT|重庆农商银行(借记卡)
CQTGB_DEBIT|重庆三峡银行(借记卡)
BOD_CREDIT|东莞银行(信用卡)
CSCB_DEBIT|长沙银行(借记卡)
BOB_CREDIT|北京银行(信用卡)
GDRCU_DEBIT|广东农信银行(借记卡)
BOB_DEBIT|北京银行(借记卡)
HRXJB_DEBIT|华融湘江银行(借记卡)
HSBC_DEBIT|恒生银行(借记卡)
HSB_CREDIT|徽商银行(信用卡)
HSB_DEBIT|徽商银行(借记卡)
HUNNX_DEBIT|湖南农信(借记卡)
HUSRB_DEBIT|湖商村镇银行(借记卡)
HXB_CREDIT|华夏银行(信用卡)
HXB_DEBIT|华夏银行(借记卡)
HNNX_DEBIT|河南农信(借记卡)
BNC_DEBIT|江西银行(借记卡)
BNC_CREDIT|江西银行(信用卡)
BJRCB_DEBIT|北京农商行(借记卡)
JCB_DEBIT|晋城银行(借记卡)
JJCCB_DEBIT|九江银行(借记卡)
JLB_DEBIT|吉林银行(借记卡)
JLNX_DEBIT|吉林农信(借记卡)
JNRCB_DEBIT|江南农商(借记卡)
JRCB_DEBIT|江阴农商行(借记卡)
JSHB_DEBIT|晋商银行(借记卡)
HAINNX_DEBIT|海南农信(借记卡)
GLB_DEBIT|桂林银行(借记卡)
GRCB_CREDIT|广州农商银行(信用卡)
GRCB_DEBIT|广州农商银行(借记卡)
GSB_DEBIT|甘肃银行(借记卡)
GSNX_DEBIT|甘肃农信(借记卡)
GXNX_DEBIT|广西农信(借记卡)
GYCB_CREDIT|贵阳银行(信用卡)
GYCB_DEBIT|贵阳银行(借记卡)
GZNX_DEBIT|贵州农信(借记卡)
HAINNX_CREDIT|海南农信(信用卡)
HKB_DEBIT|汉口银行(借记卡)
HANAB_DEBIT|韩亚银行(借记卡)
HBCB_CREDIT|湖北银行(信用卡)
HBCB_DEBIT|湖北银行(借记卡)
HBNX_CREDIT|湖北农信(信用卡)
HBNX_DEBIT|湖北农信(借记卡)
HDCB_DEBIT|邯郸银行(借记卡)
HEBNX_DEBIT|河北农信(借记卡)
HFB_DEBIT|恒丰银行(借记卡)
HKBEA_DEBIT|东亚银行(借记卡)
JCB_CREDIT|JCB(信用卡)
MASTERCARD_CREDIT|MASTERCARD(信用卡)
VISA_CREDIT|VISA(信用卡)
LQT|零钱通

## 安全规范

### 签名算法


### 签名生成的通用步骤

#### 参数名ASCII码排序

设所有发送或者接收到的数据为集合M,将集合M内非空参数值的参数按照参数名ASCII码从小到大排序(字典序).<br>
使用URL键值对的格式(即key1=value1&key2=value2…)拼接成字符串stringA.

特别注意以下重要规则:

0. 参数名ASCII码从小到大排序(字典序).
0. 如果参数的值为空不参与签名.
0. 参数名区分大小写.
0. 验证调用返回或微信主动通知签名时,传送的sign参数不参与签名,将生成的签名与该sign值作校验.
0. 微信接口可能增加字段,验证签名时必须支持增加的扩展字段.

#### MD5运算

在stringA最后拼接上key得到stringSignTemp字符串,并对stringSignTemp进行MD5运算.<br>
再将得到的字符串所有字符转换为大写,得到sign值signValue.

key设置路径:[微信商户平台](pay.weixin.qq.com)-->账户设置-->API安全-->密钥设置

#### 示例

假设传送的参数如下:

```
appid:wxd930ea5d5a258f4f
mch_id:10000100
device_info:1000
body:test
nonce_str:ibuaiVcKdpRxkhJA
```

第一步:对参数按照key=value的格式,并按照参数名ASCII字典序排序如下:

```
stringA="appid=wxd930ea5d5a258f4f&body=test&device_info=1000&mch_id=10000100&nonce_str=ibuaiVcKdpRxkhJA";
```

第二步:拼接API密钥:

```
stringSignTemp=stringA+"&key=192006250b4c09247ec02edce69f6a2d" //注:key为商户平台设置的密钥key

sign=MD5(stringSignTemp).toUpperCase()="9A0A8659F005D6984697E2CA0A9CF3B7" //注:MD5签名方式

sign=hash_hmac("sha256",stringSignTemp,key).toUpperCase()="6A9AE1657590FD6257D693A078E1C3E4BB6BA4DC30B23E0EE2496E54170DACD6" //注:HMAC-SHA256签名方式
```

最终得到最终发送的数据:

```xml
<xml>
<appid>wxd930ea5d5a258f4f</appid>
<mch_id>10000100</mch_id>
<device_info>1000</device_info>
<body>test</body>
<nonce_str>ibuaiVcKdpRxkhJA</nonce_str>
<sign>9A0A8659F005D6984697E2CA0A9CF3B7</sign>
</xml>
```

### 生成随机数算法

微信支付API接口协议中包含字段nonce_str,主要保证签名不可预测.<br>
推荐生成随机数算法如下:调用随机数函数生成,将得到的值转换为字符串.

### API证书

#### 获取API证书

[什么是api证书?如何升级?](http://kf.qq.com/faq/180824JvUZ3i180824YvMNJj.html)

微信支付接口中,涉及资金回滚的接口会使用到API证书,包括退款、撤销接口.<br>
商家在申请微信支付成功后,收到的相应邮件后,可以按照指引下载API证书,也可以按照以下路径下载:<br>
[微信商户平台](pay.weixin.qq.com)-->账户中心-->账户设置-->API安全.

证书文件说明如下:

证书附件|描述|使用场景|备注
-|-|-|-
pkcs12格式(apiclient_cert.p12)|包含了私钥信息的证书文件,为p12(pfx)格式,由微信支付签发给您用来标识和界定您的身份|撤销、退款申请API中调用|windows上可以直接双击导入系统,导入过程中会提示输入证书密码,证书密码默认为您的商户ID(如:10010000)

以下两个证书在PHP环境中使用:

证书附件|描述|使用场景|备注
-|-|-|-
证书pem格式(apiclient_cert.pem)|从apiclient_cert.p12中导出证书部分的文件,为pem格式,请妥善保管不要泄漏和被他人复制|PHP等不能直接使用p12文件,而需要使用pem,为了方便您使用,已为您直接提供|您也可以使用openssl命令来自己导出:openssl pkcs12 -clcerts -nokeys -in apiclient_cert.p12 -out apiclient_cert.pem
证书密钥pem格式(apiclient_key.pem)|从apiclient_key.pem中导出密钥部分的文件,为pem格式,请妥善保管不要泄漏和被他人复制|PHP等不能直接使用p12文件,而需要使用pem,为了方便您使用,已为您直接提供|您也可以使用openssl命令来自己导出:openssl pkcs12 -nocerts -in apiclient_cert.p12 -out apiclient_key.pem

#### 使用API证书

0. apiclient_cert.p12是商户证书文件,除PHP外的开发均使用此证书文件.
0. 商户如果使用.NET环境开发,请确认Framework版本大于2.0,必须在操作系统上双击安装证书apiclient_cert.p12后才能被正常调用.
0. API证书调用或安装需要使用到密码,该密码的值为微信商户号(mch_id)

#### API证书安全

0. 证书文件不能放在web服务器虚拟目录,应放在有访问权限控制的目录中,防止被他人下载.
0. 建议将证书文件名改为复杂且不容易猜测的文件名.
0. 商户服务器要做好病毒和木马防护工作,不被非法侵入者窃取证书文件.

#### 商户回调API安全

在普通的网络环境下,HTTP请求存在DNS劫持、运营商插入广告、数据被窃取,正常数据被修改等安全风险.<br>
商户回调接口使用HTTPS协议可以保证数据传输的安全性.<br>
所以微信支付建议商户提供给微信支付的各种回调采用HTTPS协议.

# 业务流程

## 业务角色

整个流程涉及到四个角色:

0. 用户
0. 客户端
0. 后台系统
0. 支付系统


## 时序图

![业务流程时序图
](https://pay.weixin.qq.com/wiki/doc/api/img/chapter7_4_1.png)

## 文字版

### 生成预付单

0. 系统后台生成支付入口
0. 用户进入支付入口
0. 微信端向**后台系统**请求生成支付订单
0. **后台系统**根据请求信息调用支付系统接口
0. 支付系统生成预付单并将预付单信息返回**后台系统**
0. 生成支付参数并签名返回给客户端
0. 预付单流程完毕

### 发起支付

0. 预付单页面用户发起支付
0. 客户端向支付系统发起支付请求
0. 支付系统进行校验并返回验证结果并要求客户端获取用户授权
0. 用户确认支付并向客户端进行授权
0. 客户端将授权信息发送给支付系统
0. 支付系统进行验证<br>A:支付系统将支付结果返回**后台系统**<br>&emsp;**后台系统**处理支付系统的支付结果<br>&emsp;**后台系统**通知支付系统对支付结果的处理结果<br>B:支付系统将支付结果返回客户端<br>**AB同时进行**
0. 支付流程完毕

### 支付后流程 

0. 客户端跳回H5页面并向**后台系统**查询支付结果
0. **后台系统**两种情况:<br>a: 未收到支付系统返回的支付结果:<br>&emsp;调查询接口向支付系统查询支付结果<br>b:已收到支付系统推送的支付结果:<br>&emsp;不做任何特殊操作<br>向客户端返回支付结果
0. 支付后H5展示页面
0. 支付后流程完毕

# 统一下单接口

> 相关支付流程

接口对应生成预付单流程中:<br>&emsp;后台系统根据请求信息调用支付系统接口<br>&emsp;支付系统生成预付单并将预付单信息返回后台系统

> 应用场景

后台系统先调用该接口在支付系统后台生成预支付交易单.<br>返回正确的预支付交易会话标识后再按JSAPI生成交易串调起支付.


> 接口链接

```
https://api.mch.weixin.qq.com/pay/unifiedorder
```

> 是否需要证书

否

## 请求参数

字段名|变量名|必填|类型|示例值|描述
-|-|-|-|-|-
公众账号ID|appid|是|String(32)|wxd678efh567hg6787|微信支付分配的公众账号ID(企业号corpid即为此appId)
商户号|mch_id|是|String(32)|1230000109|微信支付分配的商户号
设备号|device_info|否|String(32)|013467007045764|自定义参数,可以为终端设备号(门店号或收银设备ID),PC网页或公众号内支付可以传"WEB"
随机字符串|nonce_str|是|String(32)|5K8264ILTKCH16CQ2502SI8ZNMTM67VS|随机字符串,长度要求在32位以内.<br>推荐随机数生成算法
签名|sign|是|String(32)|C380BEC2BFD727A4B6845133519F3AD6|通过签名算法计算得出的签名值,详见签名生成算法
签名类型|sign_type|否|String(32)|MD5|签名类型,默认为MD5,支持HMAC-SHA256和MD5.
商品描述|body|是|String(128)|腾讯充值中心-QQ会员充值|商品简单描述,该字段请按照规范传递,具体请见参数规定
商品详情|detail|否|String(6000)| |商品详细描述,对于使用单品优惠的商户,改字段必须按照规范上传,详见"单品优惠参数说明"
附加数据|attach|否|String(127)|深圳分店|附加数据,在查询API和支付通知中原样返回,可作为自定义参数使用.
商户订单号|out_trade_no|是|String(32)|20150806125346|商户系统内部订单号,要求32个字符内,只能是数字、大小写字母_-\|* 且在同一个商户号下唯一.<br>详见商户订单号
标价币种|fee_type|否|String(16)|CNY|符合ISO 4217标准的三位字母代码,默认人民币:CNY,详细列表请参见货币类型
标价金额|total_fee|是|Int|88|订单总金额,单位为分,详见支付金额
终端IP|spbill_create_ip|是|String(64)|123.12.12.123|支持IPV4和IPV6两种格式的IP地址.<br>调用微信支付API的机器IP
交易起始时间|time_start|否|String(14)|20091225091010|订单生成时间,格式为yyyyMMddHHmmss,<br>如2009年12月25日9点10分10秒表示为20091225091010.<br>其他详见时间规则
交易结束时间|time_expire|否|String(14)|20091227091010|订单失效时间,格式为yyyyMMddHHmmss,<br>如2009年12月27日9点10分10秒表示为20091227091010.<br>订单失效时间是针对订单号而言的,由于在请求支付的时候有一个必传参数prepay_id只有两小时的有效期,<br>所以在重入时间超过2小时的时候需要重新请求下单接口获取新的prepay_id.<br>其他详见时间规则<br>建议:最短失效时间间隔大于1分钟
订单优惠标记|goods_tag|否|String(32)|WXG|订单优惠标记,使用代金券或立减优惠功能时需要的参数,<br>说明详见代金券或立减优惠
通知地址|notify_url|是|String(256)|http://www.weixin.qq.com/wxpay/pay.php|异步接收微信支付结果通知的回调地址,<br>通知url必须为外网可访问的url,不能携带参数.
交易类型|trade_type|是|String(16)|JSAPI|<br>JSAPI -JSAPI支付<br>NATIVE -Native支付<br>APP -APP支付<br>说明详见参数规定
商品ID|product_id|否|String(32)|12235413214070356458058|trade_type=NATIVE时,此参数必传.<br>此参数为二维码中包含的商品ID,商户自行定义.
指定支付方式|limit_pay|否|String(32)|no_credit|上传此参数no_credit--可限制用户不能使用信用卡支付
用户标识|openid|是|String(128)|oUpF8uMuAJO_M2pxb1Q9zNjWeS6o|trade_type=JSAPI时(即JSAPI支付),<br>此参数必传,此参数为微信用户在商户对应appid下的唯一标识.
电子发票入口开放标识|receipt|否|String(8)|Y|Y,传入Y时,支付成功消息和支付详情页将出现开票入口.<br>需要在微信支付商户平台或微信公众平台开通电子发票功能,传此字段才可生效
场景信息|scene_info|否|String(256)|{<br>&emsp;"store_info" : {<br>&emsp;&emsp;"id": "SZTX001",<br>&emsp;&emsp;"name": "腾大餐厅",<br>&emsp;&emsp;"area_code": "440305",<br>&emsp;&emsp;"address": "科技园中一路腾讯大厦" <br>&emsp;&emsp;}<br>}|该字段常用于线下活动时的场景信息上报,支持上报实际门店信息,商户也可以按需求自己上报相关信息.<br>该字段为JSON对象数据,对象格式为{"store_info":{"id": "门店ID","name": "名称","area_code": "编码","address": "地址" }} ,[字段详细说明](https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_1)

> 示例

```xml
<xml>
   <appid>wx2421b1c4370ec43b</appid>
   <attach>支付测试</attach>
   <body>JSAPI支付测试</body>
   <mch_id>10000100</mch_id>
   <detail><![CDATA[{ "goods_detail":[ { "goods_id":"iphone6s_16G", "wxpay_goods_id":"1001", "goods_name":"iPhone6s 16G", "quantity":1, "price":528800, "goods_category":"123456", "body":"苹果手机" }, { "goods_id":"iphone6s_32G", "wxpay_goods_id":"1002", "goods_name":"iPhone6s 32G", "quantity":1, "price":608800, "goods_category":"123789", "body":"苹果手机" } ] }]]></detail>
   <nonce_str>1add1a30ac87aa2db72f57a2375d8fec</nonce_str>
   <notify_url>http://wxpay.wxutil.com/pub_v2/pay/notify.v2.php</notify_url>
   <openid>oUpF8uMuAJO_M2pxb1Q9zNjWeS6o</openid>
   <out_trade_no>1415659990</out_trade_no>
   <spbill_create_ip>14.23.150.211</spbill_create_ip>
   <total_fee>1</total_fee>
   <trade_type>JSAPI</trade_type>
   <sign>0CB01533B8C1EF103065174F50BCA001</sign>
</xml>
```

参数值用XML转义即可,CDATA标签用于说明数据不被XML解析器解析.


## 返回结果

字段名|变量名|必填|类型|示例值|描述
-|-|-|-|-|-
返回状态码|return_code|是|String(16)|SUCCESS|SUCCESS/FAIL<br>此字段是通信标识,非交易标识,<br>交易是否成功需要查看result_code来判断
返回信息|return_msg|是|String(128)|OK|当return_code为FAIL时返回信息为错误原因

> 以下字段**在return_code为SUCCESS的时候**有返回

字段名|变量名|必填|类型|示例值|描述
-|-|-|-|-|-
公众账号ID|appid|是|String(32)|wx8888888888888888|调用接口提交的公众账号ID
商户号|mch_id|是|String(32)|1900000109|调用接口提交的商户号
设备号|device_info|否|String(32)|013467007045764|自定义参数,可以为请求支付的终端设备号等
随机字符串|nonce_str|是|String(32)|5K8264ILTKCH16CQ2502SI8ZNMTM67VS|微信返回的随机字符串
签名|sign|是|String(32)|C380BEC2BFD727A4B6845133519F3AD6|微信返回的签名值,详见签名算法
业务结果|result_code|是|String(16)|SUCCESS|SUCCESS/FAIL
错误代码|err_code|否|String(32)| |当result_code为FAIL时返回错误代码,详细参见下文错误列表
错误代码描述|err_code_des|否|String(128)| |当result_code为FAIL时返回错误描述,详细参见下文错误列表

> 以下字段**在return_code和result_code都为SUCCESS的时候**有返回

字段名|变量名|必填|类型|示例值|描述
-|-|-|-|-|-
交易类型|trade_type|是|String(16)|JSAPI|<br>JSAPI -JSAPI支付<br>NATIVE -Native支付<br>APP -APP支付
预支付交易会话标识|prepay_id|是|String(64)|wx201410272009395522657a690389285100|微信生成的预支付会话标识,用于后续接口调用中使用,<br>该值有效期为2小时
二维码链接|code_url|否|String(64)|weixin://wxpay/bizpayurl/up?pr=NwY5Mz9&groupid=00|trade_type=NATIVE时有返回,<br>此url用于生成支付二维码,然后提供给用户进行扫码支付.<br>注意:code_url的值并非固定,使用时按照URL格式转成二维码即可

> 示例

```xml
<xml>
   <return_code><![CDATA[SUCCESS]]></return_code>
   <return_msg><![CDATA[OK]]></return_msg>
   <appid><![CDATA[wx2421b1c4370ec43b]]></appid>
   <mch_id><![CDATA[10000100]]></mch_id>
   <nonce_str><![CDATA[IITRi8Iabbblz1Jc]]></nonce_str>
   <openid><![CDATA[oUpF8uMuAJO_M2pxb1Q9zNjWeS6o]]></openid>
   <sign><![CDATA[7921E432F65EB8ED0CE9755F0E86D72F]]></sign>
   <result_code><![CDATA[SUCCESS]]></result_code>
   <prepay_id><![CDATA[wx201411101639507cbf6ffd8b0779950874]]></prepay_id>
   <trade_type><![CDATA[JSAPI]]></trade_type>
</xml>
```

## 错误码

名称|描述|原因|解决方案
-|-|-|-
INVALID_REQUEST|参数错误|参数格式有误或者未按规则上传|订单重入时,要求参数值与原请求一致,请确认参数问题
NOAUTH|商户无此接口权限|商户未开通此接口权限|请商户前往申请此接口权限
NOTENOUGH|余额不足|用户帐号余额不足|用户帐号余额不足,请用户充值或更换支付卡后再支付
ORDERPAID|商户订单已支付|商户订单已支付,无需重复操作|商户订单已支付,无需更多操作
ORDERCLOSED|订单已关闭|当前订单已关闭,无法支付|当前订单已关闭,请重新下单
SYSTEMERROR|系统错误|系统超时|系统异常,请用相同参数重新调用
APPID_NOT_EXIST|APPID不存在|参数中缺少APPID|请检查APPID是否正确
MCHID_NOT_EXIST|MCHID不存在|参数中缺少MCHID|请检查MCHID是否正确
APPID_MCHID_NOT_MATCH|appid和mch_id不匹配|appid和mch_id不匹配|请确认appid和mch_id是否匹配
LACK_PARAMS|缺少参数|缺少必要的请求参数|请检查参数是否齐全
OUT_TRADE_NO_USED|商户订单号重复|同一笔交易不能多次提交|请核实商户订单号是否重复提交
SIGNERROR|签名错误|参数签名结果不正确|请检查签名参数和方法是否都符合签名算法要求
XML_FORMAT_ERROR|XML格式错误|XML格式错误|请检查XML参数格式是否正确
REQUIRE_POST_METHOD|请使用post方法|未使用post传递参数 |请检查请求参数是否通过post方法提交
POST_DATA_EMPTY|post数据为空|post数据不能为空|请检查post数据是否为空
NOT_UTF8|编码格式错误|未使用指定编码格式|请使用UTF-8编码格式

## notify_url填写注意事项 

0. notify_url需要填写后台系统的真实地址,不能填写接口文档或demo上的示例地址.
0. notify_url必须是以https://或http://开头的完整全路径地址,并且确保url中的域名和IP是外网可以访问的,不能填写localhost、127.0.0.1、192.168.x.x等本地或内网IP.
0. notify_url不能携带参数.
 
> 常见错误举例

错误描述:	

url中只有域名,缺少具体的路径:

```
http://www.weixin.qq.com
```

url不是以https://或http://开头,缺少域名或IP:

```
./PayNotify.aspx
```

url中填写了本地或者内网IP:

```
http://127.0.0.1/pay/notify.php
```

填写了不是url格式的字符串:

```
xxxxxxx,1234567,test
```

# 支付结果通知接口

> 应用场景

支付完成后,支付系统会把相关支付结果及用户信息通过数据流的形式发送给后台系统,后台系统需要接收处理,并按文档规范返回应答.

> 注意

0. 同样的通知可能会多次发送给商户系统<br>商户系统必须能够正确处理重复的通知.
0. 后台通知交互时,如果微信收到商户的应答不符合规范或超时,微信会判定本次通知失败,<br>重新发送通知,直到成功为止(在通知一直不成功的情况下,微信总共会发起10次通知,通知频率为15s/15s/30s/3m/10m/20m/30m/30m/30m/60m/3h/3h/3h/6h/6h - 总计 24h4m),<br>但微信不保证通知最终一定能成功.
0. 在订单状态不明或者没有收到微信支付结果通知的情况下,<br>建议主动调用支付系统的[查询订单接口](#查询订单接口)确认订单状态.

> 特别提醒

0. 后台系统对于支付结果通知的内容一定要做签名验证,<br>并校验返回的订单金额是否与商户侧的订单金额一致,<br>防止数据泄漏导致出现"假通知",造成资金损失.
0. 当收到通知进行处理时,首先检查对应业务数据的状态,<br>判断该通知是否已经处理过,如果没有处理过再进行处理,<br>如果处理过直接返回结果成功.<br>在对业务数据进行状态检查和处理之前,要采用数据锁进行并发控制,以避免函数重入造成的数据混乱.
0. 技术人员可登进微信商户后台扫描加入接口报警群,获取接口告警信息.

## 接口链接

该链接是通过[统一下单接口](#统一下单接口)中提交的参数notify_url设置,如果链接无法访问,商户将无法接收到微信通知.

通知url必须为直接可访问的url,不能携带参数.

示例:

```
notify_url:"https://pay.weixin.qq.com/wxpay/pay.action"
```

> 是否需要证书

否

## 通知参数

字段名|变量名|必填|类型|示例值|描述
-|-|-|-|-|-
返回状态码|return_code|是|String(16)|SUCCESS|SUCCESS
返回信息|return_msg|是|String(128)|OK|OK

> 以下字段**在return_code为SUCCESS的时候**有返回

字段名|变量名|必填|类型|示例值|描述
-|-|-|-|-|-
公众账号ID|appid|是|String(32)|wx8888888888888888|微信分配的公众账号ID(企业号corpid即为此appId)
商户号|mch_id|是|String(32)|1900000109|微信支付分配的商户号
设备号|device_info|否|String(32)|013467007045764|微信支付分配的终端设备号,
随机字符串|nonce_str|是|String(32)|5K8264ILTKCH16CQ2502SI8ZNMTM67VS|随机字符串,不长于32位
签名|sign|是|String(32)|C380BEC2BFD727A4B6845133519F3AD6|签名,详见签名算法
签名类型|sign_type|否|String(32)|HMAC-SHA256|签名类型,目前支持HMAC-SHA256和MD5,默认为MD5
业务结果|result_code|是|String(16)|SUCCESS|SUCCESS/FAIL
错误代码|err_code|否|String(32)|SYSTEMERROR|错误返回的信息描述
错误代码描述|err_code_des|否|String(128)|系统错误|错误返回的信息描述
用户标识|openid|是|String(128)|wxd930ea5d5a258f4f|用户在商户appid下的唯一标识
是否关注公众账号|is_subscribe|是|String(1)|Y|用户是否关注公众账号,Y-关注,N-未关注
交易类型|trade_type|是|String(16)|JSAPI|JSAPI、NATIVE、APP
付款银行|bank_type|是|String(16)|CMC|银行类型,采用字符串类型的银行标识,银行类型见银行列表
订单金额|total_fee|是|Int|100|订单总金额,单位为分
应结订单金额|settlement_total_fee|否|Int|100|应结订单金额=订单金额-非充值代金券金额,应结订单金额<=订单金额.
货币种类|fee_type|否|String(8)|CNY|货币类型,符合ISO4217标准的三位字母代码,默认人民币:CNY,其他值列表详见货币类型
现金支付金额|cash_fee|是|Int|100|现金支付金额订单现金支付金额,详见支付金额
现金支付货币类型|cash_fee_type|否|String(16)|CNY|货币类型,符合ISO4217标准的三位字母代码,默认人民币:CNY,其他值列表详见货币类型
总代金券金额|coupon_fee|否|Int|10|代金券金额<=订单金额,订单金额-代金券金额=现金支付金额,详见支付金额
代金券使用数量|coupon_count|否|Int|1|代金券使用数量
代金券类型|coupon_type_$n|否|String|CASH|<br>CASH--充值代金券 <br>NO_CASH---非充值代金券<br>并且订单使用了免充值券后有返回(取值:CASH、NO_CASH).$n为下标,从0开始编号,举例:coupon_type_0
代金券ID|coupon_id_$n|否|String(20)|10000|代金券ID,$n为下标,从0开始编号
单个代金券支付金额|coupon_fee_$n|否|Int|100|单个代金券支付金额,$n为下标,从0开始编号
微信支付订单号|transaction_id|是|String(32)|1217752501201407033233368018|微信支付订单号
商户订单号|out_trade_no|是|String(32)|1212321211201407033568112322|商户系统内部订单号,要求32个字符内,只能是数字、大小写字母_-|*@ ,且在同一个商户号下唯一.
商家数据包|attach|否|String(128)|123456|商家数据包,原样返回
支付完成时间|time_end|是|String(14)|20141030133525|支付完成时间,格式为yyyyMMddHHmmss,如2009年12月25日9点10分10秒表示为20091225091010.其他详见时间规则

> 示例

```xml
<xml>
  <appid><![CDATA[wx2421b1c4370ec43b]]></appid>
  <attach><![CDATA[支付测试]]></attach>
  <bank_type><![CDATA[CFT]]></bank_type>
  <fee_type><![CDATA[CNY]]></fee_type>
  <is_subscribe><![CDATA[Y]]></is_subscribe>
  <mch_id><![CDATA[10000100]]></mch_id>
  <nonce_str><![CDATA[5d2b6c2a8db53831f7eda20af46e531c]]></nonce_str>
  <openid><![CDATA[oUpF8uMEb4qRXf22hE3X68TekukE]]></openid>
  <out_trade_no><![CDATA[1409811653]]></out_trade_no>
  <result_code><![CDATA[SUCCESS]]></result_code>
  <return_code><![CDATA[SUCCESS]]></return_code>
  <sign><![CDATA[B552ED6B279343CB493C5DD0D78AB241]]></sign>
  <sub_mch_id><![CDATA[10000100]]></sub_mch_id>
  <time_end><![CDATA[20140903131540]]></time_end>
  <total_fee>1</total_fee>
<coupon_fee_0><![CDATA[10]]></coupon_fee_0>
<coupon_count><![CDATA[1]]></coupon_count>
<coupon_type><![CDATA[CASH]]></coupon_type>
<coupon_id><![CDATA[10000]]></coupon_id> 
  <trade_type><![CDATA[JSAPI]]></trade_type>
  <transaction_id><![CDATA[1004400740201409030005092168]]></transaction_id>
</xml>
```

## 返回参数

收到支付系统结果通知后,请严格按照示例返回参数给支付系统:

字段名|变量名|必填|类型|示例值|描述
-|-|-|-|-|-
返回状态码|return_code|是|String(16)|SUCCESS|请按示例值填写
返回信息|return_msg|是|String(128)|OK|请按示例值填写

> 示例

```xml
<xml>
  <return_code><![CDATA[SUCCESS]]></return_code>
  <return_msg><![CDATA[OK]]></return_msg>
</xml>
```

## notify_url处理注意事项 

### 返回报文格式规范 

返回报文必须是xml格式,字段名需与接口文档说明的一致,报文前后和各字段标签中间不能包含特殊字符. 

> 错误描述	

返回内容为空或者非xml格式字符串

```
ok,success,支付成功
```

返回的xml格式报文中间有其他字符

```
<xml>\n<return_code><![CDATA[SUCCESS]]></return_code> 
\n<return_msg><![CDATA[OK]]></return_msg>\n</xml>
```

返回的是json格式

```
{"return_code":"SUCCESS";"return_msg":"OK"}
```

返回的是整个页面的html代码

```
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" ><head><title>支付成功通知页面</title></head><body>success</body></html>
```
  
### 回调处理逻辑注意事项 

0. ***notify_url的代码处理逻辑不能做登录态校验***.
0. 商户系统收到支付结果通知,需要在5秒内返回应答报文,否则微信支付认为通知失败,后续会重复发送通知. 
0. 同样的通知可能会多次发送给商户系统,商户系统必须能够正确处理重复的通知.<br>如果已处理过,直接给微信支付返回成功.
0. 商户侧对微信支付回调IP有防火墙策略限制的,需要对以下IP段开通白名单:101.226.103.0/25<br>140.207.54.0/25<br>103.7.30.0/25<br>183.3.234.0/25<br>58.251.80.0/25

  
# 查询订单接口

## 应用场景

> 该接口提供所有微信支付订单的查询,后台系统可以通过查询订单接口主动查询订单状态,完成下一步的业务逻辑.

## 需要调用查询接口的情况

0. 当商户后台、网络、服务器等出现异常,商户系统最终未接收到支付通知.
0. 调用支付接口后,返回系统错误或未知交易状态情况.
0. 调用付款码支付API,返回USERPAYING的状态.
0. 调用关单或撤销接口API之前,需确认支付状态.

## 接口链接

```
https://api.mch.weixin.qq.com/pay/orderquery
```

> 是否需要证书

否

## 请求参数

字段名|变量名|必填|类型|示例值|描述
-|-|-|-|-|-
公众账号ID|appid|是|String(32)|wxd678efh567hg6787|微信支付分配的公众账号ID(企业号corpid即为此appId)
商户号|mch_id|是|String(32)|1230000109|微信支付分配的商户号
微信订单号|transaction_id|二选一|String(32)|1009660380201506130728806387|微信的订单号,建议优先使用
商户订单号|out_trade_no|二选一|String(32)|20150806125346|商户系统内部订单号,要求32个字符内,只能是数字、大小写字母_-|*@ ,且在同一个商户号下唯一.<br> 详见商户订单号
随机字符串|nonce_str|是|String(32)|C380BEC2BFD727A4B6845133519F3AD6|随机字符串,不长于32位.<br>推荐随机数生成算法
签名|sign|是|String(32)|5K8264ILTKCH16CQ2502SI8ZNMTM67VS|通过签名算法计算得出的签名值,详见签名生成算法
签名类型|sign_type|否|String(32)|HMAC-SHA256|签名类型,目前支持HMAC-SHA256和MD5,默认为MD5

> 示例

```
<xml>
   <appid>wx2421b1c4370ec43b</appid>
   <mch_id>10000100</mch_id>
   <nonce_str>ec2316275641faa3aacf3cc599e8730f</nonce_str>
   <transaction_id>1008450740201411110005820873</transaction_id>
   <sign>FDD167FAA73459FD921B144BAF4F4CA2</sign>
</xml>
```

## 返回结果

字段名|变量名|必填|类型|示例值|描述
-|-|-|-|-|-
返回状态码|return_code|是|String(16)|SUCCESS|SUCCESS/FAIL<br>此字段是通信标识,非交易标识,交易是否成功需要查看trade_state来判断
返回信息|return_msg|是|String(128)|OK|当return_code为FAIL时返回信息为错误原因

> 以下字段**在return_code为SUCCESS的时候**有返回

字段名|变量名|必填|类型|示例值|描述
-|-|-|-|-|-
公众账号ID|appid|是|String(32)|wxd678efh567hg6787|微信分配的公众账号ID
商户号|mch_id|是|String(32)|1230000109|微信支付分配的商户号
随机字符串|nonce_str|是|String(32)|5K8264ILTKCH16CQ2502SI8ZNMTM67VS|随机字符串,不长于32位.<br>推荐随机数生成算法
签名|sign|是|String(32)|C380BEC2BFD727A4B6845133519F3AD6|签名,详见签名生成算法
业务结果|result_code|是|String(16)|SUCCESS|SUCCESS/FAIL
错误代码|err_code|否|String(32)| |当result_code为FAIL时返回错误代码,详细参见下文错误列表
错误代码描述|err_code_des|否|String(128)| |当result_code为FAIL时返回错误描述

> 以下字段**在return_code 、result_code、trade_state都为SUCCESS时**有返回 ,如trade_state不为 SUCCESS,则只返回out_trade_no(必传)和attach(选传).

字段名|变量名|必填|类型|示例值|描述
-|-|-|-|-|-
设备号|device_info|否|String(32)|013467007045764|微信支付分配的终端设备号
用户标识|openid|是|String(128)|oUpF8uMuAJO_M2pxb1Q9zNjWeS6o|用户在商户appid下的唯一标识
是否关注公众账号|is_subscribe|是|String(1)|Y|用户是否关注公众账号,Y-关注,N-未关注
交易类型|trade_type|是|String(16)|JSAPI|调用接口提交的交易类型,取值如下:JSAPI,NATIVE,APP,MICROPAY,详细说明见参数规定
交易状态|trade_state|是|String(32)|SUCCESS|<br>SUCCESS—支付成功<br>REFUND—转入退款<br>NOTPAY—未支付<br>CLOSED—已关闭<br>REVOKED—已撤销(付款码支付)<br>USERPAYING--用户支付中(付款码支付)<br>PAYERROR--支付失败(其他原因,如银行返回失败)<br>支付状态机请见下单API页面
付款银行|bank_type|是|String(16)|CMC|银行类型,采用字符串类型的银行标识
标价金额|total_fee|是|Int|100|订单总金额,单位为分
应结订单金额|settlement_total_fee|否|Int|100|当订单使用了免充值型优惠券后返回该参数,应结订单金额=订单金额-免充值优惠券金额.
标价币种|fee_type|否|String(8)|CNY|货币类型,符合ISO 4217标准的三位字母代码,默认人民币:CNY,其他值列表详见货币类型
现金支付金额|cash_fee|是|Int|100|现金支付金额订单现金支付金额,详见支付金额
现金支付币种|cash_fee_type|否|String(16)|CNY|货币类型,符合ISO 4217标准的三位字母代码,默认人民币:CNY,其他值列表详见货币类型
代金券金额|coupon_fee|否|Int|100|"代金券"金额<=订单金额,订单金额-"代金券"金额=现金支付金额,详见支付金额
代金券使用数量|coupon_count|否|Int|1|代金券使用数量
代金券类型|coupon_type_$n|否|String|CASH|<br>CASH--充值代金券 <br>NO_CASH---非充值优惠券<br>开通免充值券功能,并且订单使用了优惠券后有返回(取值:CASH、NO_CASH).<br>$n为下标,从0开始编号,举例:coupon_type_$0
代金券ID|coupon_id_$n|否|String(20)|10000 |代金券ID, $n为下标,从0开始编号
单个代金券支付金额|coupon_fee_$n|否|Int|100|单个代金券支付金额, $n为下标,从0开始编号
微信支付订单号|transaction_id|是|String(32)|1009660380201506130728806387|微信支付订单号
商户订单号|out_trade_no|是|String(32)|20150806125346|商户系统内部订单号,要求32个字符内,只能是数字、大小写字母_-|*@ ,且在同一个商户号下唯一.
附加数据|attach|否|String(128)|深圳分店|附加数据,原样返回
支付完成时间|time_end|是|String(14)|20141030133525|订单支付时间,格式为yyyyMMddHHmmss,如2009年12月25日9点10分10秒表示为20091225091010.<br>其他详见时间规则
交易状态描述|trade_state_desc|是|String(256)|支付失败,请重新下单支付|对当前查询订单状态的描述和下一步操作的指引

> 示例

```xml
<xml>
   <return_code><![CDATA[SUCCESS]]></return_code>
   <return_msg><![CDATA[OK]]></return_msg>
   <appid><![CDATA[wx2421b1c4370ec43b]]></appid>
   <mch_id><![CDATA[10000100]]></mch_id>
   <device_info><![CDATA[1000]]></device_info>
   <nonce_str><![CDATA[TN55wO9Pba5yENl8]]></nonce_str>
   <sign><![CDATA[BDF0099C15FF7BC6B1585FBB110AB635]]></sign>
   <result_code><![CDATA[SUCCESS]]></result_code>
   <openid><![CDATA[oUpF8uN95-Ptaags6E_roPHg7AG0]]></openid>
   <is_subscribe><![CDATA[Y]]></is_subscribe>
   <trade_type><![CDATA[MICROPAY]]></trade_type>
   <bank_type><![CDATA[CCB_DEBIT]]></bank_type>
   <total_fee>1</total_fee>
   <fee_type><![CDATA[CNY]]></fee_type>
   <transaction_id><![CDATA[1008450740201411110005820873]]></transaction_id>
   <out_trade_no><![CDATA[1415757673]]></out_trade_no>
   <attach><![CDATA[订单额外描述]]></attach>
   <time_end><![CDATA[20141111170043]]></time_end>
   <trade_state><![CDATA[SUCCESS]]></trade_state>
</xml>
```

## 错误码

名称|描述|原因|解决方案
-|-|-|-|
ORDERNOTEXIST|此交易订单号不存在|查询系统中不存在此交易订单号|该API只能查提交支付交易返回成功的订单,请商户检查需要查询的订单号是否正确
SYSTEMERROR|系统错误|后台系统返回错误|系统异常,请再调用发起查询


# 参考资料

> [微信支付 | JSAPI支付](https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=1_1)