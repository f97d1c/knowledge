>本质上,webpack是一个用于现代JavaScript应用程序的静态模块打包工具.<br>
当webpack处理应用程序时,它会在内部构建一个依赖图(dependency graph),<br>
此依赖图对应映射到项目所需的每个模块,并生成一个或多个bundle.<br>

<!-- TOC -->

- [说明](#说明)
  - [为什么选择webpack](#为什么选择webpack)
    - [立即调用函数表达式(IIFE) - Immediately invoked function expressions](#立即调用函数表达式iife---immediately-invoked-function-expressions)
    - [感谢Node.js,JavaScript模块诞生了](#感谢nodejsjavascript模块诞生了)
    - [npm + Node.js + modules - 大规模分发模块](#npm--nodejs--modules---大规模分发模块)
    - [ESM -ECMAScript模块](#esm--ecmascript模块)
    - [依赖自动收集](#依赖自动收集)
  - [浏览器兼容性(browser compatibility)](#浏览器兼容性browser-compatibility)
  - [环境(environment)](#环境environment)
  - [模块(Modules)](#模块modules)
    - [何为webpack模块](#何为webpack模块)
    - [支持的模块类型](#支持的模块类型)
  - [模块解析(Module Resolution)](#模块解析module-resolution)
    - [webpack中的解析规则](#webpack中的解析规则)
      - [绝对路径](#绝对路径)
      - [相对路径](#相对路径)
      - [模块路径](#模块路径)
    - [解析loader](#解析loader)
    - [缓存](#缓存)
  - [配置(Configuration)](#配置configuration)
    - [基本配置](#基本配置)
  - [入口(entry)](#入口entry)
    - [对象语法](#对象语法)
      - [描述入口的对象](#描述入口的对象)
    - [常见场景](#常见场景)
      - [分离app(应用程序) 和vendor(第三方库) 入口](#分离app应用程序-和vendor第三方库-入口)
      - [多页面应用程序](#多页面应用程序)
  - [输出(output)](#输出output)
    - [多个入口起点](#多个入口起点)
    - [高级进阶](#高级进阶)
  - [loader](#loader)
    - [示例](#示例)
    - [使用loader](#使用loader)
      - [配置方式](#配置方式)
      - [内联方式](#内联方式)
    - [loader特性](#loader特性)
    - [解析loader](#解析loader-1)
  - [插件(plugin)](#插件plugin)
    - [剖析](#剖析)
    - [用法](#用法)
    - [配置方式](#配置方式-1)
    - [Node API方式](#node-api方式)
  - [模式(mode)](#模式mode)
  - [Module Federation](#module-federation)
    - [底层概念](#底层概念)
    - [高级概念](#高级概念)
    - [构建块(Building blocks)](#构建块building-blocks)
      - [ContainerPlugin (底层API)](#containerplugin-底层api)
      - [ContainerReferencePlugin (底层API)](#containerreferenceplugin-底层api)
      - [ModuleFederationPlugin (高级API)](#modulefederationplugin-高级api)
    - [概念目标](#概念目标)
    - [用例](#用例)
      - [每个页面单独构建](#每个页面单独构建)
      - [将组件库作为容器](#将组件库作为容器)
      - [动态远程容器](#动态远程容器)
      - [基于Promise的动态Remote](#基于promise的动态remote)
      - [动态Public Path](#动态public-path)
        - [提供一个host api以设置public Path{#offerahostapitosetthepublicPath}](#提供一个host-api以设置public-pathofferahostapitosetthepublicpath)
      - [Infer publicPath from script](#infer-publicpath-from-script)
    - [故障排除](#故障排除)
      - [Uncaught Error: Shared module is not available for eager consumption](#uncaught-error-shared-module-is-not-available-for-eager-consumption)
      - [Uncaught Error: Module "./Button" does not exist in container.](#uncaught-error-module-button-does-not-exist-in-container)
      - [Uncaught TypeError: fn is not a function](#uncaught-typeerror-fn-is-not-a-function)
  - [依赖图(dependency graph)](#依赖图dependency-graph)
  - [target](#target)
    - [用法](#用法-1)
    - [多target](#多target)
    - [资源](#资源)
  - [manifest](#manifest)
    - [runtime](#runtime)
    - [manifest](#manifest-1)
    - [问题](#问题)
  - [模块热替换(hot module replacement)](#模块热替换hot-module-replacement)
    - [这一切是如何运行的?](#这一切是如何运行的)
      - [在应用程序中](#在应用程序中)
      - [在compiler中](#在compiler中)
      - [在模块中](#在模块中)
      - [在runtime中](#在runtime中)
    - [起步](#起步)
  - [揭示内部原理](#揭示内部原理)
    - [主要部分](#主要部分)
    - [chunk](#chunk)
    - [output(输出)](#output输出)
  - [项目初始化](#项目初始化)
  - [制作仓库](#制作仓库)
- [参考资料](#参考资料)

<!-- /TOC -->


# 说明

## 为什么选择webpack

想要理解为什么要使用webpack,先回顾下历史,在打包工具出现之前,是如何在web中使用JavaScript的.<br>


在浏览器中运行JavaScript有两种方法.<br>
第一种方式,引用一些脚本来存放每个功能;此解决方案很难扩展,因为加载太多脚本会导致网络瓶颈.<br>
第二种方式,使用一个包含所有项目代码的大型 .js文件,但是这会导致作用域、文件大小、可读性和可维护性方面的问题.<br>


### 立即调用函数表达式(IIFE) - Immediately invoked function expressions

IIFE解决大型项目的作用域问题;当脚本文件被封装在IIFE内部时,可以安全地拼接或安全地组合所有文件,而不必担心作用域冲突.<br>


IIFE使用方式产生出Make, Gulp, Grunt,Broccoli或Brunch等工具.<br>
这些工具称为任务执行器,它们将所有项目文件拼接在一起.<br>


但是,修改一个文件意味着必须重新构建整个文件.<br>
拼接可以做到很容易地跨文件重用脚本,但是却使构建结果的优化变得更加困难.<br>
如何判断代码是否实际被使用?

即只用到lodash中的某个函数,也必须在构建结果中加入整个库,然后将它们压缩在一起.<br>
如何treeshake代码依赖?难以大规模地实现延迟加载代码块,这需要开发人员手动地进行大量工作.<br>


### 感谢Node.js,JavaScript模块诞生了

Node.js是一个JavaScript运行时,可以在浏览器环境之外的计算机和服务器中使用.<br>
webpack运行在Node.js中.<br>


当Node.js发布时,一个新的时代开始了,它带来了新的挑战.<br>
既然不是在浏览器中运行JavaScript,现在已经没有了可以添加到浏览器中的html文件和script标签.<br>
那么Node.js应用程序要如何加载新的代码chunk呢?

CommonJS问世并引入了require机制,它允许在当前文件中加载和使用某个模块.<br>
导入需要的每个模块,这一开箱即用的功能,帮助解决了作用域问题.<br>


### npm + Node.js + modules - 大规模分发模块

JavaScript已经成为一种语言、一个平台和一种快速开发和创建快速应用程序的方式,接管了整个JavaScript世界.<br>


但CommonJS没有浏览器支持.<br>
没有livebinding(实时绑定).<br>
循环引用存在问题.<br>
同步执行的模块解析加载器速度很慢.<br>
虽然CommonJS是Node.js项目的绝佳解决方案,但浏览器不支持模块,因而产生了Browserify,RequireJS和SystemJS等打包工具,允许编写能够在浏览器中运行的CommonJS模块.<br>


### ESM -ECMAScript模块

来自Web项目的好消息是,模块正在成为ECMAScript标准的官方功能.<br>
然而,浏览器支持不完整,版本迭代速度也不够快,目前还是推荐上面那些早期模块实现.<br>


### 依赖自动收集

传统的任务构建工具基于Google的Closure编译器都要求手动在顶部声明所有的依赖.<br>
然而像webpack一类的打包工具自动构建并基于所引用或导出的内容推断出依赖的图谱.<br>
这个特性与其它的如插件and加载器一道让开发者的体验更好.<br>


看起来都不是很好……
是否可以有一种方式,不仅可以编写模块,而且还支持任何模块格式(至少在到达ESM之前),并且可以同时处理资源和资产?

这就是webpack存在的原因.<br>
它是一个工具,可以打包JavaScript应用程序(支持ESM和CommonJS),可以扩展为支持许多不同的静态资源,例如:images,fonts和stylesheets.<br>


webpack关心性能和加载时间;它始终在改进或添加新功能,例如:异步地加载chunk和预取,以便为项目和用户提供最佳体验.<br>



## 浏览器兼容性(browser compatibility)

webpack支持所有符合ES5标准的浏览器(不支持IE8及以下版本).<br>
webpack的import() 和require.ensure() 需要Promise.<br>
如果想要支持旧版本浏览器,在使用这些表达式之前,还需要提前加载polyfill.

## 环境(environment)

webpack5运行于Node.js v10.13.0+ 的版本.

## 模块(Modules)

在模块化编程中,开发者将程序分解为功能离散的chunk,并称之为模块.

每个模块都拥有小于完整程序的体积,使得验证、调试及测试变得轻而易举.<br>
精心编写的模块提供了可靠的抽象和封装界限,使得应用程序中每个模块都具备了条理清晰的设计和明确的目的.

Node.js从一开始就支持模块化编程.<br>
然而,web的模块化正在缓慢支持中.<br>
在web界存在多种支持JavaScript模块化的工具,这些工具各有优势和限制.<br>
webpack从这些系统中汲取了经验和教训,并将模块的概念应用到项目的任何文件中.

### 何为webpack模块

与Node.js模块相比,webpack模块能以各种方式表达它们的依赖关系.<br>
下面是一些示例:

0. ES2015import语句
0. CommonJS require() 语句
0. AMDdefine和require语句
0. css/sass/less文件中的 @import语句.
0. stylesheet url(...) 或者HTML\<img src=...> 文件中的图片链接.

### 支持的模块类型

webpack天生支持如下模块类型:

0. ECMAScript模块
0. CommonJS模块
0. AMD模块
0. Assets
0. WebAssembly模块

通过loader可以使webpack支持多种语言和预处理器语法编写的模块.<br>
loader向webpack描述了如何处理非原生模块,并将相关依赖引入到bundles中.<br>
webpack社区已经为各种流行的语言和预处理器创建了loader,其中包括:

0. CoffeeScript
0. TypeScript
0. ESNext (Babel)
0. Sass
0. Less
0. Stylus
0. Elm

当然还有更多!总得来说,webpack提供了可定制,强大且丰富的API,允许在任何技术栈中使用,同时支持在开发、测试和生产环境的工作流中做到无侵入性.

关于loader的相关信息,请参考loader列表或自定义loader.

## 模块解析(Module Resolution)

resolver是一个帮助寻找模块绝对路径的库.<br>
一个模块可以作为另一个模块的依赖模块,然后被后者引用,如下:

```js
import foo from 'path/to/module';
// 或者
require('path/to/module');
```

所依赖的模块可以是来自应用程序的代码或第三方库.<br>
resolver帮助webpack从每个require/import语句中,找到需要引入到bundle中的模块代码.<br>
当打包模块时,webpack使用enhanced-resolve来解析文件路径.

### webpack中的解析规则

使用enhanced-resolve,webpack能解析三种文件路径:

#### 绝对路径

```js
import '/home/me/file';

import 'C:\\Users\\me\\file';
```

由于已经获得文件的绝对路径,因此不需要再做进一步解析.

#### 相对路径

```js
import '../src/file1';
import './file2';
```

在这种情况下,使用import或require的资源文件所处的目录,被认为是上下文目录.<br>
在import/require中给定的相对路径,会拼接此上下文路径,来生成模块的绝对路径.

#### 模块路径

```js
import 'module';
import 'module/lib/file';
```

在resolve.modules中指定的所有目录检索模块.<br>
可以通过配置别名的方式来替换初始模块路径,具体请参照resolve.alias配置选项.

如果package中包含package.json文件,<br>
那么在resolve.exportsFields配置选项中指定的字段会被依次查找,<br>
package.json中的第一个字段会根据package导出指南确定package中可用的export.<br>

一旦根据上述规则解析路径后,resolver将会检查路径是指向文件还是文件夹.<br>
如果路径指向文件:

如果文件具有扩展名,则直接将文件打包.<br>

否则,将使用resolve.extensions选项作为文件扩展名来解析,<br>
此选项会告诉解析器在解析中能够接受那些扩展名(例如 .js,.jsx).<br>

如果路径指向一个文件夹,则进行如下步骤寻找具有正确扩展名的文件:

如果文件夹中包含package.json文件,则会根据resolve.mainFields配置中的字段顺序查找,并根据package.json中的符合配置要求的第一个字段来确定文件路径.<br>

如果不存在package.json文件或resolve.mainFields没有返回有效路径,则会根据resolve.mainFiles配置选项中指定的文件名顺序查找,看是否能在import/require的目录下匹配到一个存在的文件名.<br>

然后使用resolve.extensions选项,以类似的方式解析文件扩展名.<br>

webpack会根据构建目标,为这些选项提供合理的默认配置.

### 解析loader

loader的解析规则也遵循特定的规范.<br>
但是resolveLoader配置项可以为loader设置独立的解析规则.

### 缓存

每次文件系统访问文件都会被缓存,以便于更快触发对同一文件的多个并行或串行请求.<br>
在watch模式下,只有修改过的文件会被从缓存中移出.<br>
如果关闭watch模式,则会在每次编译前清理缓存.

欲了解更多上述配置信息,请查阅ResolveAPI.

## 配置(Configuration)

可能已经注意到,很少有webpack配置看起来完全相同.<br>
这是因为webpack的配置文件是JavaScript文件,文件内导出了一个webpack配置的对象.<br>
webpack会根据该配置定义的属性进行处理.

由于webpack遵循CommonJS模块规范,因此,可以在配置中使用:

0. 通过require(...) 引入其文件
0. 通过require(...) 使用npm下载的工具函数
0. 使用JavaScript控制流表达式,例如 ?: 操作符
0. 对value使用常量或变量赋值
0. 编写并执行函数,生成部分配置

请在合适的场景,使用这些功能.

虽然技术上可行,但还是应避免如下操作:

0. 当使用webpackCLI工具时,访问CLI参数(应编写自己的CLI工具替代,或者使用 --env)
0. 导出不确定的结果(两次调用webpack应产生相同的输出文件)
0. 编写超长的配置(应将配置文件拆分成多个)

此文档中得出最重要的结论是,webpack的配置可以有许多不同的样式和风格.<br>
关键在于,为了易于维护和理解这些配置,需要在团队内部保证一致.

接下来的示例中,展示了webpack配置如何实现既可表达,又可灵活配置,这主要得益于配置即为代码:

### 基本配置

```js
// webpack.config.js

const path = require('path');

module.exports = {
  mode: 'development',
  entry: './foo.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'foo.bundle.js',
  },
};
```

## 入口(entry)

入口起点(entry point) 指示webpack应该使用哪个模块,来作为构建其内部依赖图(dependency graph) 的开始.<br>
进入入口起点后,webpack会找出有哪些模块和库是入口起点(直接和间接)依赖的.

默认值是 ./src/index.js,但可以通过在webpackconfiguration中配置entry属性,来指定一个(或多个)不同的入口起点.<br>
例如:

```js
// webpack.config.js

module.exports = {
  entry: './path/to/my/entry/file.js',
};
```

### 对象语法

```js
// webpack.config.js

module.exports = {
  entry: {
    app: './src/app.js',
    adminApp: './src/adminApp.js',
  },
};
```
对象语法会比较繁琐.<br>
然而,这是应用程序中定义入口的最可扩展的方式.<br>

*webpack配置的可扩展* 是指,这些配置可以重复使用,并且可以与其配置组合使用.<br>
这是一种流行的技术,用于将关注点从环境(environment)、构建目标(build target)、运行时(runtime)中分离.<br>
然后使用专门的工具(如webpack-merge)将它们合并起来.

#### 描述入口的对象

用于描述入口的对象.<br>
可以使用如下属性:

属性名|作用
-|-
dependOn|当前入口所依赖的入口.<br>它们必须在该入口被加载前被加载.
filename|指定要输出的文件名称.
import|启动时需加载的模块.
library|指定library选项,为当前entry构建一个library.
runtime|运行时chunk的名字.<br>如果设置了,就会创建一个以这个名字命名的运行时chunk,否则将使用现有的入口作为运行时.
publicPath|当该入口的输出文件在浏览器中被引用时,为它们指定一个公共URL地址.<br>请查看[output.publicPath](https://webpack.docschina.org/configuration/output/#outputpublicpath).

runtime和dependOn不应在同一个入口上同时使用,会抛出错误.

### 常见场景

以下列出一些入口配置和它们的实际用例:

#### 分离app(应用程序) 和vendor(第三方库) 入口

```js
// webpack.config.js

module.exports = {
  entry: {
    main: './src/app.js',
    vendor: './src/vendor.js',
  },
};
webpack.prod.js

module.exports = {
  output: {
    filename: '[name].[contenthash].bundle.js',
  },
};
webpack.dev.js

module.exports = {
  output: {
    filename: '[name].bundle.js',
  },
};
```

这是什么? 这是告诉webpack想要配置2个单独的入口点(例如上面的示例).

为什么? 这样就可以在vendor.js中存入未做修改的必要library或文件(例如Bootstrap, jQuery, 图片等),<br>
然后将它们打包在一起成为单独的chunk.<br>
内容哈希保持不变,这使浏览器可以独立地缓存它们,从而减少了加载时间.

在webpack<4的版本中,通常将vendor作为一个单独的入口起点添加到entry选项中,<br>
以将其编译为一个单独的文件(与CommonsChunkPlugin结合使用).

而在webpack4中不鼓励这样做.<br>
而是使用optimization.splitChunks选项,<br>
将vendor和app(应用程序) 模块分开,并为其创建一个单独的文件.<br>
不要为vendor或其不是执行起点创建entry.

#### 多页面应用程序

```js
webpack.config.js

module.exports = {
  entry: {
    pageOne: './src/pageOne/index.js',
    pageTwo: './src/pageTwo/index.js',
    pageThree: './src/pageThree/index.js',
  },
};
```

这是什么? 告诉webpack需要三个独立分离的依赖图(如上面的示例).

为什么? 在多页面应用程序中,server会拉取一个新的HTML文档给客户端.<br>
页面重新加载此新文档,并且资源被重新下载.<br>
然而,这给了特殊的机会去做很多事,<br>
例如使用optimization.splitChunks为页面间共享的应用程序代码创建bundle.<br>
由于入口起点数量的增多,多页应用能够复用多个入口起点之间的大量代码/模块,<br>
从而可以极大地从这些技术中受益.

## 输出(output)

output属性告诉webpack在哪里输出它所创建的bundle,以及如何命名这些文件.<br>
主要输出文件的默认值是 ./dist/main.js,其生成文件默认放置在 ./dist文件夹中.

可以通过在配置中指定一个output字段,来配置这些处理过程:

```js
// webpack.config.js

const path = require('path');

module.exports = {
  entry: './path/to/my/entry/file.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'my-first-webpack.bundle.js',
  },
};
```

在上面的示例中,通过output.filename和output.path属性,来告诉webpackbundle的名称,以及想要bundle生成(emit)到哪里.<br>
可能想要了解在代码最上面导入的path模块是什么,它是一个Node.js核心模块,用于操作文件路径.<br>

### 多个入口起点

如果配置中创建出多于一个 "chunk"(例如,使用多个入口起点或使用像CommonsChunkPlugin这样的插件),<br>
则应该使用占位符(substitutions) 来确保每个文件具有唯一的名称.

```js
module.exports = {
  entry: {
    app: './src/app.js',
    search: './src/search.js',
  },
  output: {
    filename: '[name].js',
    path: __dirname + '/dist',
  },
};

// 写入到硬盘:./dist/app.js, ./dist/search.js
```

### 高级进阶

以下是对资源使用CDN和hash的复杂示例:

```js
// config.js

module.exports = {
  //...
  output: {
    path: '/home/proj/cdn/assets/[fullhash]',
    publicPath: 'https://cdn.example.com/assets/[fullhash]/',
  },
};
```

如果在编译时,不知道最终输出文件的publicPath是什么地址,则可以将其留空,<br>
并且在运行时通过入口起点文件中的__webpack_public_path__动态设置.

```js
__webpack_public_path__ = myRuntimePublicPath;

// 应用程序入口的其余部分
```

## loader

loader用于对模块的源代码进行转换.<br>
loader可以在import或 "load(加载)" 模块时预处理文件.<br>
因此,loader类似于其构建工具中 *任务(task)* ,并提供了处理前端构建步骤的得力方式.<br>
loader可以将文件从不同的语言(如TypeScript)转换为JavaScript或将内联图像转换为dataURL.<br>
loader甚至允许直接在JavaScript模块中importCSS文件!


webpack只能理解JavaScript和JSON文件,这是webpack开箱可用的自带能力.<br>
loader让webpack能够去处理其类型的文件,并将它们转换为有效模块,以供应用程序使用,以及被添加到依赖图中.

注意,loader能够import导入任何类型的模块(例如 .css文件),<br>
这是webpack特有的功能,其打包程序或任务执行器的可能并不支持.<br>
这种语言扩展是很有必要的,因为这可以使开发人员创建出更准确的依赖关系图.

在更高层面,在webpack的配置中,loader有两个属性:

0. test属性,识别出哪些文件会被转换.
0. use属性,定义出在进行转换时,应该使用哪个loader.

```js
// webpack.config.js

const path = require('path');

module.exports = {
  output: {
    filename: 'my-first-webpack.bundle.js',
  },
  module: {
    rules: [{test: /\.txt$/, use: 'raw-loader' }],
  },
};
```

以上配置中,对一个单独的module对象定义了rules属性,里面包含两个必须属性:test和use.<br>
这告诉webpack编译器(compiler) 如下信息:

当碰到「在require()/import语句中被解析为 '.txt' 的路径」时,在对它打包之前,先use(使用) raw-loader转换一下.

在webpack配置中定义rules时,要定义在module.rules而不是rules中.<br>
为了便于理解,如果没有按照正确方式去做,webpack会给出警告.

使用正则表达式匹配文件时,不要为它添加引号.<br>
也就是说,/\.txt$/ 与 '/\.txt$/' 或 "/\.txt$/" 不一样.<br>
前者指示webpack匹配任何以 .txt结尾的文件,后者指示webpack匹配具有绝对路径 '.txt' 的单个文件.

### 示例

例如,可以使用loader告诉webpack加载CSS文件,<br>
或者将TypeScript转为JavaScript.<br>
为此,首先安装相对应的loader:

```bash
npm install --save-dev css-loader ts-loader
```

然后指示webpack对每个 .css使用css-loader,以及对所有 .ts文件使用ts-loader:

```js
// webpack.config.js

module.exports = {
  module: {
    rules: [
      {test: /\.css$/, use: 'css-loader' },
      {test: /\.ts$/, use: 'ts-loader' },
    ],
  },
};
```

### 使用loader

在应用程序中,有两种使用loader的方式:

0. 配置方式(推荐):在webpack.config.js文件中指定loader.
0. 内联方式:在每个import语句中显式指定loader.

#### 配置方式

module.rules允许在webpack配置中指定多个loader.<br>
这种方式是展示loader的一种简明方式,并且有助于使代码变得简洁和易于维护.<br>
同时对各个loader有个全局概览:

loader从 **右到左(或从下到上)** 地取值(evaluate)/执行(execute).<br>
在下面的示例中,从sass-loader开始执行,然后继续执行css-loader,最后以style-loader为结束.<br>
查看loader功能章节,了解有关loader顺序的更多信息.

```js
module.exports = {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          // [style-loader](/loaders/style-loader)
          {loader: 'style-loader' },
          // [css-loader](/loaders/css-loader)
          {
            loader: 'css-loader',
            options: {
              modules: true
            }
          },
          // [sass-loader](/loaders/sass-loader)
          {loader: 'sass-loader' }
        ]
      }
    ]
  }
};
```

#### 内联方式

可以在import语句或任何与 "import" 方法同等的引用方式中指定loader.<br>
使用 ! 将资源中的loader分开.<br>
每个部分都会相对于当前目录解析.

```js
import Styles from 'style-loader!css-loader?modules!./styles.css';
```

通过为内联import语句添加前缀,可以覆盖配置中的所有loader,preLoader和postLoader:

使用 ! 前缀,将禁用所有已配置的normalloader(普通loader)

```js
import Styles from '!style-loader!css-loader?modules!./styles.css';
```

使用 !! 前缀,将禁用所有已配置的loader(preLoader, loader, postLoader)

```js
import Styles from '!!style-loader!css-loader?modules!./styles.css';
```

使用 -! 前缀,将禁用所有已配置的preLoader和loader,但是不禁用postLoaders

```js
import Styles from '-!style-loader!css-loader?modules!./styles.css';
```

选项可以传递查询参数,例如 ?key=value&foo=bar,或者一个JSON对象,例如 ?{"key":"value","foo":"bar"}.

尽可能使用module.rules,因为这样可以减少源码中样板文件的代码量,并且可以在出错时,更快地调试和定位loader中的问题.

### loader特性

loader支持链式调用.<br>
链中的每个loader会将转换应用在已处理过的资源上.<br>
一组链式的loader将按照相反的顺序执行.<br>
链中的第一个loader将其结果(也就是应用过转换后的资源)传递给下一个loader,依此类推.<br>
最后,链中的最后一个loader,返回webpack所期望的JavaScript.<br>

loader可以是同步的,也可以是异步的.<br>

loader运行在Node.js中,并且能够执行任何操作.<br>

loader可以通过options对象配置(仍然支持使用query参数来设置选项,但是这种方式已被废弃).<br>

除了常见的通过package.json的main来将一个npm模块导出为loader,<br>
还可以在module.rules中使用loader字段直接引用一个模块.<br>

插件(plugin)可以为loader带来更多特性.<br>

loader能够产生额外的任意文件.<br>

可以通过loader的预处理函数,为JavaScript生态系统提供更多能力.<br>
用户现在可以更加灵活地引入细粒度逻辑,例如:压缩、打包、语言转译(或编译)和更多其特性.

### 解析loader

loader遵循标准[模块解析](https://webpack.docschina.org/concepts/module-resolution/)规则.<br>
多数情况下,loader将从[模块路径](https://webpack.docschina.org/concepts/module-resolution/#module-paths)加载(通常是从npm install,node_modules进行加载).

预期loader模块导出为一个函数,并且编写为Node.js兼容的JavaScript.<br>
通常使用npm进行管理loader,但是也可以将应用程序中的文件作为自定义loader.<br>
按照约定,loader通常被命名为xxx-loader(例如json-loader).<br>
更多详细信息,请查看[编写一个loader](https://webpack.docschina.org/contribute/writing-a-loader/).

## 插件(plugin)

插件是webpack的支柱功能.<br>
webpack自身也是构建于在webpack配置中用到的相同的插件系统之上.

插件目的在于解决loader无法实现的其事.

loader用于转换某些类型的模块,而插件则可以用于执行范围更广的任务.<br>
包括:打包优化,资源管理,注入环境变量.

想要使用一个插件,只需要require() 它,然后把它添加到plugins数组中.<br>
多数插件可以通过选项(option)自定义.<br>
也可以在一个配置文件中因为不同目的而多次使用同一个插件,这时需要通过使用new操作符来创建一个插件实例.

```js
// webpack.config.js

const HtmlWebpackPlugin = require('html-webpack-plugin'); // 通过npm安装
const webpack = require('webpack'); // 用于访问内置插件

module.exports = {
  module: {
    rules: [{test: /\.txt$/, use: 'raw-loader' }],
  },
  plugins: [new HtmlWebpackPlugin({template: './src/index.html' })],
};
```

在上面的示例中,html-webpack-plugin为应用程序生成一个HTML文件,并自动注入所有生成的bundle.<br>

如果在插件中使用了webpack-sources的package,<br>
请使用require('webpack').sources替代require('webpack-sources'),以避免持久缓存的版本冲突.

### 剖析

webpack插件是一个具有apply方法的JavaScript对象.<br>
apply方法会被webpackcompiler调用,并且在整个编译生命周期都可以访问compiler对象.

```js
// ConsoleLogOnBuildWebpackPlugin.js

const pluginName = 'ConsoleLogOnBuildWebpackPlugin';

classConsoleLogOnBuildWebpackPlugin{
  apply(compiler) {
    compiler.hooks.run.tap(pluginName, (compilation) => {
      console.log('webpack构建过程开始!');
    });
  }
}

module.exports = ConsoleLogOnBuildWebpackPlugin;
```

compilerhook的tap方法的第一个参数,应该是驼峰式命名的插件名称.<br>
建议为此使用一个常量,以便它可以在所有hook中重复使用.

### 用法

由于插件可以携带参数/选项,必须在webpack配置中,向plugins属性传入一个new实例.

取决于webpack用法,对应有多种使用插件的方式.

### 配置方式

```js
// webpack.config.js

const HtmlWebpackPlugin = require('html-webpack-plugin'); // 通过npm安装
const webpack = require('webpack'); // 访问内置的插件
const path = require('path');

module.exports = {
  entry: './path/to/my/entry/file.js',
  output: {
    filename: 'my-first-webpack.bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
      },
    ],
  },
  plugins: [
    new webpack.ProgressPlugin(),
    new HtmlWebpackPlugin({template: './src/index.html' }),
  ],
};
```

ProgressPlugin用于自定义编译过程中的进度报告,<br>
HtmlWebpackPlugin将生成一个HTML文件,<br>
并在其中使用script引入一个名为my-first-webpack.bundle.js的JS文件.

### Node API方式

在使用Node API时,还可以通过配置中的plugins属性传入插件.

```js
// some-node-script.js

const webpack = require('webpack'); // 访问webpack运行时(runtime)
const configuration = require('./webpack.config.js');

let compiler = webpack(configuration);

new webpack.ProgressPlugin().apply(compiler);

compiler.run(function (err, stats) {
  // ...
});
```

以上看到的示例和webpack运行时(runtime)本身极其类似.<br>
webpack源码中隐藏有大量使用示例,可以将其应用在自己的配置和脚本中.

## 模式(mode)

通过选择development,production或none之中的一个,来设置mode参数,<br>
可以启用webpack内置在相应环境下的优化.<br>
其默认值为production.

```js
module.exports = {
  mode: 'production',
};
```

## Module Federation

多个独立的构建可以组成一个应用程序,这些独立的构建之间不应该存在依赖关系,因此可以单独开发和部署它们.

这通常被称作微前端,但并不仅限于此.

### 底层概念

区分本地模块和远程模块.<br>
本地模块即为普通模块,是当前构建的一部分.<br>
远程模块不属于当前构建,并在运行时从所谓的容器加载.

加载远程模块被认为是异步操作.<br>
当使用远程模块时,这些异步操作将被放置在远程模块和入口之间的下一个chunk的加载操作中.<br>
如果没有chunk加载操作,就不能使用远程模块.

chunk的加载操作通常是通过调用import() 实现的,但也支持像require.ensure或require([...]) 之类的旧语法.

容器是由容器入口创建的,该入口暴露了对特定模块的异步访问.<br>
暴露的访问分为两个步骤:

0. 加载模块(异步的)
0. 执行模块(同步的)

步骤1将在chunk加载期间完成.<br>
步骤2将在与其(本地和远程)的模块交错执行期间完成.<br>
这样一来,执行顺序不受模块从本地转换为远程或从远程转为本地的影响.

容器可以嵌套使用,容器可以使用来自其容器的模块.<br>
容器之间也可以循环依赖.

### 高级概念

每个构建都充当一个容器,也可将其构建作为容器.<br>
通过这种方式,每个构建都能够通过从对应容器中加载模块来访问其容器暴露出来的模块.

共享模块是指既可重写的又可作为向嵌套容器提供重写的模块.<br>
它们通常指向每个构建中的相同模块,例如相同的库.

packageName选项允许通过设置包名来查找所需的版本.<br>
默认情况下,它会自动推断模块请求,当想禁用自动推断时,请将requiredVersion设置为false.<br>

### 构建块(Building blocks)

#### ContainerPlugin (底层API)

该插件使用指定的公开模块来创建一个额外的容器入口.

#### ContainerReferencePlugin (底层API)

该插件将特定的引用添加到作为外部资源(externals)的容器中,<br>
并允许从这些容器中导入远程模块.<br>
它还会调用这些容器的overrideAPI来为它们提供重载.<br>
本地的重载(当构建也是一个容器时,通过__webpack_override__或overrideAPI)和指定的重载被提供给所有引用的容器.

#### ModuleFederationPlugin (高级API)

ModuleFederationPlugin组合了ContainerPlugin和ContainerReferencePlugin.

### 概念目标

0. 它既可以暴露,又可以使用webpack支持的任何模块类型
0. 代码块加载应该并行加载所需的所有内容(web:到服务器的单次往返)
0. 从使用者到容器的控制<br>
   重写模块是一种单向操作<br>
   同级容器不能重写彼此的模块.
0. 概念适用于独立于环境<br>
   可用于web、Node.js等
0. 共享中的相对和绝对请求<br>
   会一直提供,即使不使用<br>
   会将相对路径解析到config.context<br>
   默认不会使用requiredVersion
0. 共享中的模块请求<br>
   只在使用时提供<br>
   会匹配构建中所有使用的相等模块请求<br>
   将提供所有匹配模块<br>
   将从图中这个位置的package.json提取requiredVersion<br>
   当有嵌套的node_modules时,可以提供和使用多个不同的版本
0. 共享中尾部带有 /  的模块请求将匹配所有具有这个前缀的模块请求

### 用例

#### 每个页面单独构建

单页应用的每个页面都是在单独的构建中从容器暴露出来的.<br>
主体应用程序(application shell)也是独立构建,会将所有页面作为远程模块来引用.<br>
通过这种方式,可以单独部署每个页面.<br>
在更新路由或添加新路由时部署主体应用程序.<br>
主体应用程序将常用库定义为共享模块,以避免在页面构建中出现重复.

#### 将组件库作为容器

许多应用程序共享一个通用的组件库,可以将其构建成暴露所有组件的容器.<br>
每个应用程序使用来自组件库容器的组件.<br>
可以单独部署对组件库的更改,而不需要重新部署所有应用程序.<br>
应用程序自动使用组件库的最新版本.

#### 动态远程容器

该容器接口支持get和init方法.<br>
init是一个兼容async的方法,调用时,只含有一个参数:共享作用域对象(shared scope object).<br>
此对象在远程容器中用作共享作用域,并由host提供的模块填充.<br>
可以利用它在运行时动态地将远程容器连接到host容器.

```js
// init.js

(async () => {
  // 初始化共享作用域(shared scope)用提供的已知此构建和所有远程的模块填充它
  await __webpack_init_sharing__('default');
  const container = window.someContainer; // 或从其地方获取容器
  // 初始化容器它可能提供共享模块
  await container.init(__webpack_share_scopes__.default);
  const module = await container.get('./module');
})();
```

容器尝试提供共享模块,但是如果共享模块已经被使用,则会发出警告,并忽略所提供的共享模块.<br>
容器仍能将其作为降级模块.

可以通过动态加载的方式,提供一个共享模块的不同版本,从而实现A/B测试.

在尝试动态连接远程容器之前,确保已加载容器.

例子:

```js
// init.js

function loadComponent(scope, module) {
  return async () => {
    // 初始化共享作用域(shared scope)用提供的已知此构建和所有远程的模块填充它
    await __webpack_init_sharing__('default');
    const container = window[scope]; // 或从其地方获取容器
    // 初始化容器它可能提供共享模块
    await container.init(__webpack_share_scopes__.default);
    const factory = await window[scope].get(module);
    const Module = factory();
    return Module;
  };
}

loadComponent('abtests', 'test123');
```

#### 基于Promise的动态Remote

一般来说,remote是使用URL配置的,示例如下:

```js
module.exports = {
  plugins: [
    new ModuleFederationPlugin({
      name: 'host',
      remotes: {
        app1: 'app1@http://localhost:3001/remoteEntry.js',
      },
    }),
  ],
};
```

但是也可以向remote传递一个promise,其会在运行时被调用.<br>
应该用任何符合上面描述的get/init接口的模块来调用这个promise.<br>
例如,如果想传递应该使用哪个版本的联邦模块,可以通过一个查询参数做以下事情:

```js
module.exports = {
  plugins: [
    new ModuleFederationPlugin({
      name: 'host',
      remotes: {
        app1: `promise new Promise(resolve => {
      const urlParams = new URLSearchParams(window.location.search)
      const version = urlParams.get('app1VersionParam')
      // This part depends on how you plan on hosting and versioning your federated modules
      const remoteUrlWithVersion = 'http://localhost:3001/' + version + '/remoteEntry.js'
      const script = document.createElement('script')
      script.src = remoteUrlWithVersion
      script.onload = () => {
        // the injected script has loaded and is available on window
        // we can now resolve this Promise
        const proxy = {
          get: (request) => window.app1.get(request),
          init: (arg) => {
try{
              return window.app1.init(arg)
            }catch(e) {
              console.log('remote container already initialized')
            }
          }
        }
        resolve(proxy)
      }
      // inject this script with the src set to the versioned remoteEntry.js
      document.head.appendChild(script);
    })
    `,
      },
      // ...
    }),
  ],
};
```

请注意当使用该API时,必须resolve一个包含get/initAPI的对象.

#### 动态Public Path

##### 提供一个host api以设置public Path{#offerahostapitosetthepublicPath}

可以允许host在运行时通过公开远程模块的方法来设置远程模块的publicPath.

当在host域的子路径上挂载独立部署的子应用程序时,这种方法特别有用.

场景:

在https://my-host.com/app/* 上有一个host应用,并且在https://foo-app.com上有一个子应用.<br>
子应用程序也挂载在host域上, 因此, https://foo-app.com is expected to be accessible via https://my-host.com/app/foo-app and https://my-host.com/app/foo/* requests are redirected to https://foo-app.com/* via a proxy.

示例:

```js
// webpack.config.js (remote)

module.exports = {
  entry: {
    remote: './public-path',
  },
  plugins: [
    new ModuleFederationPlugin({
      name: 'remote', // 该名称必须与入口名称相匹配
      exposes: ['./public-path'],
      // ...
    }),
  ],
};
```

```js
// public-path.js (remote)

export function set(value) {
  __webpack_public_path__ = value;
}
```

```js
// src/index.js (host)

const publicPath = await import('remote/public-path');
publicPath.set('/your-public-path');

//boostrap app  e.g. import('./boostrap.js')
```

#### Infer publicPath from script

One could infer the publicPath from the script tag from document.currentScript.src and set it with the __webpack_public_path__ module variable at runtime.

示例:

```js
// webpack.config.js (remote)

module.exports = {
  entry: {
    remote: './setup-public-path',
  },
  plugins: [
    new ModuleFederationPlugin({
      name: 'remote', // 该名称必须与入口名称相匹配
      // ...
    }),
  ],
};
```

```js
// setup-public-path.js (remote)

// 使用自己的逻辑派生publicPath,并使用__webpack_public_path__API设置它
__webpack_public_path__ = document.currentScript.src + '/../';
```

output.publicPath配置项也可设置为 'auto',它将为自动决定一个publicPath.

### 故障排除

#### Uncaught Error: Shared module is not available for eager consumption

应用程序正急切地执行一个作为全局主机运行的应用程序.<br>
有如下选项可供选择:

可以在模块联邦的高级API中将依赖设置为即时依赖,此API不会将模块放在异步chunk中,而是同步地提供它们.<br>
这使得在初始块中可以直接使用这些共享模块.<br>
但是要注意,由于所有提供的和降级模块是要异步下载的,因此,建议只在应用程序的某个地方提供它,例如shell.

强烈建议使用异步边界(asynchronous boundary).<br>
它将把初始化代码分割成更大的块,以避免任何额外的开销,以提高总体性能.

例如,入口看起来是这样的:

```js
// index.js

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
ReactDOM.render(<App />, document.getElementById('root'));
```

创建bootstrap.js文件,并将入口文件的内容放到里面,然后将bootstrap引入到入口文件中:

```js
// index.js

+ import('./bootstrap');
- import React from 'react';
- import ReactDOM from 'react-dom';
- import App from './App';
- ReactDOM.render(<App />, document.getElementById('root'));
```

```js
// bootstrap.js

+ import React from 'react';
+ import ReactDOM from 'react-dom';
+ import App from './App';
+ ReactDOM.render(<App />, document.getElementById('root'));
```

这种方法有效,但存在局限性或缺点.

通过ModuleFederationPlugin将依赖的eager属性设置为true

```js
webpack.config.js

// ...
new ModuleFederationPlugin({
  shared: {
    ...deps,
    react: {
      eager: true,
    },
  },
});
```

#### Uncaught Error: Module "./Button" does not exist in container.

错误提示中可能不会显示 "./Button",但是信息看起来差不多.<br>
这个问题通常会出现在将webpackbeta.16升级到webpackbeta.17中.

在ModuleFederationPlugin里,更改exposes:

```js
new ModuleFederationPlugin({
  exposes: {
-   'Button': './src/Button'
+   './Button':'./src/Button'
  }
});
```

#### Uncaught TypeError: fn is not a function

此处错误可能是丢失了远程容器,请确保在使用前添加它.<br>
如果已为试图使用远程服务器的容器加载了容器,但仍然看到此错误,则需将主机容器的远程容器文件也添加到HTML中.


## 依赖图(dependency graph)

每当一个文件依赖另一个文件时,webpack都会将文件视为直接存在依赖关系.<br>
这使得webpack可以获取非代码资源,如images或web字体等.<br>
并会把它们作为依赖提供给应用程序.<br>


当webpack处理应用程序时,它会根据命令行参数中或配置文件中定义的模块列表开始处理.<br>
从入口开始,webpack会递归的构建一个依赖关系图,<br>
这个依赖图包含着应用程序中所需的每个模块,<br>
然后将所有模块打包为少量的bundle——通常只有一个——可由浏览器加载.<br>


对于HTTP/1.1的应用程序来说,由webpack构建的bundle非常强大.<br>
当浏览器发起请求时,它能最大程度的减少应用的等待时间.<br>
而对于HTTP/2来说,还可以使用代码分割进行进一步优化.<br>


## target

由于JavaScript既可以编写服务端代码也可以编写浏览器代码,所以webpack提供了多种部署target,可以在webpack的配置选项中进行设置.<br>


webpack的target属性,不要和output.libraryTarget属性混淆.<br>
有关output属性的更多信息,请参阅output指南

### 用法
想设置target属性,只需在webpack配置中设置target字段:

```js
// webpack.config.js

module.exports = {
  target: 'node',
};
```

在上述示例中,target设置为node,webpack将在类Node.js环境编译代码.<br>
(使用Node.js的require加载chunk,而不加载任何内置模块,如fs或path).<br>


每个target都包含各种deployment(部署)/environment(环境)特定的附加项,以满足其需求.<br>
具体请参阅target可用值.<br>



### 多target

虽然webpack不支持向target属性传入多个字符串,但是可以通过设置两个独立配置,来构建对library进行同构:

```js
// webpack.config.js

const path = require('path');
const serverConfig = {
  target: 'node',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'lib.node.js',
  },
  //…
};

const clientConfig = {
  target: 'web', // <=== 默认为 'web',可省略
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'lib.js',
  },
  //…
};

module.exports = [serverConfig, clientConfig];
```

上述示例中,将会在dist文件夹下创建lib.js和lib.node.js文件.<br>


### 资源

从上面选项可以看出,可以选择部署不同的target.<br>
下面是可以参考的示例和资源:

compare-webpack-target-bundles:测试并查看webpacktarget的绝佳资源.<br>
同样包含错误上报.<br>

Boilerplate of Electron-React Application: 一个关于electron主进程和渲染进程构建过程的优秀示例.<br>


## manifest

在使用webpack构建的典型应用程序或站点中,有三种主要的代码类型:

0. 个人或团队编写的源码.
0. 源码会依赖的任何第三方的library或 "vendor" 代码.
0. webpack的runtime和manifest,管理所有模块的交互.

本文将重点介绍这三个部分中的最后部分:runtime和manifest,特别是manifest.<br>


### runtime

runtime,以及伴随的manifest数据,主要是指:在浏览器运行过程中,webpack用来连接模块化应用程序所需的所有代码.<br>
它包含:在模块交互时,连接模块所需的加载和解析逻辑.<br>
包括:已经加载到浏览器中的连接模块逻辑,以及尚未加载模块的延迟加载逻辑.<br>


### manifest

一旦应用在浏览器中以index.html文件的形式被打开,一些bundle和应用需要的各种资源都需要用某种方式被加载与链接起来.<br>
在经过打包、压缩、为延迟加载而拆分为细小的chunk这些webpack优化之后,精心安排的 /src目录的文件结构都已经不再存在.<br>
所以webpack如何管理所有所需模块之间的交互呢?这就是manifest数据用途的由来……

当compiler开始执行、解析和映射应用程序时,它会保留所有模块的详细要点.<br>
这个数据集合称为 "manifest",当完成打包并发送到浏览器时,runtime会通过manifest来解析和加载模块.<br>
无论选择哪种模块语法,那些import或require语句现在都已经转换为__webpack_require__方法,此方法指向模块标识符(module identifier).<br>
通过使用manifest中的数据,runtime将能够检索这些标识符,找出每个标识符背后对应的模块.<br>


### 问题

所以,现在应该对webpack在幕后工作有一点了解.<br>
*但是,这对有什么影响呢?”,可能会问.<br>
答案是大多数情况下没有.<br>
runtime做完成这些工作:一旦应用程序加载到浏览器中,使用manifest,然后所有内容将展现出魔幻般运行结果.<br>
然而,如果决定通过使用浏览器缓存来改善项目的性能,理解这一过程将突然变得极为重要.<br>


通过使用内容散列(content hash)作为bundle文件的名称,这样在文件内容修改时,会计算出新的hash,浏览器会使用新的名称加载文件,从而使缓存无效.<br>
一旦开始这样做,会立即注意到一些有趣的行为.<br>
即使某些内容明显没有修改,某些hash还是会改变.<br>
这是因为,注入的runtime和manifest在每次构建后都会发生变化.<br>


查看管理输出指南的manifest部分,了解如何提取manifest,并阅读下面的指南,以了解更多长效缓存错综复杂之处.<br>


## 模块热替换(hot module replacement)

模块热替换(HMR - hot module replacement)功能会在应用程序运行过程中,替换、添加或删除模块,而无需重新加载整个页面.<br>
主要是通过以下几种方式,来显著加快开发速度:

保留在完全重新加载页面期间丢失的应用程序状态.<br>

只更新变更内容,以节省宝贵的开发时间.<br>

在源代码中CSS/JS产生修改时,会立刻在浏览器中进行更新,这几乎相当于在浏览器devtools直接更改样式.<br>

### 这一切是如何运行的?

从一些不同的角度观察,以了解HMR的工作原理……

#### 在应用程序中

通过以下步骤,可以做到在应用程序中置换(swap in and out)模块:

0. 应用程序要求HMRruntime检查更新.
0. HMRruntime异步地下载更新,然后通知应用程序.
0. 应用程序要求HMRruntime应用更新.
0. HMRruntime同步地应用更新.

可以设置HMR,以使此进程自动触发更新,或者可以选择要求在用户交互时进行更新.<br>


#### 在compiler中

除了普通资源,compiler需要发出 "update",将之前的版本更新到新的版本.<br>
"update" 由两部分组成:

0. 更新后的manifest(JSON)
0. 一个或多个updated chunk (JavaScript)

manifest包括新的compilation hash和所有的updated chunk列表.<br>
每个chunk都包含着全部更新模块的最新代码(或一个flag用于表明此模块需要被移除).<br>


compiler会确保在这些构建之间的模块ID和chunkID保持一致.<br>
通常将这些ID存储在内存中(例如,使用webpack-dev-server时),但是也可能会将它们存储在一个JSON文件中.<br>


#### 在模块中

HMR是可选功能,只会影响包含HMR代码的模块.<br>
举个例子,通过style-loader为style追加补丁.<br>
为了运行追加补丁,style-loader实现了HMR接口;当它通过HMR接收到更新,它会使用新的样式替换旧的样式.<br>


类似的,当在一个模块中实现了HMR接口,可以描述出当模块被更新后发生了什么.<br>
然而在多数情况下,不需要在每个模块中强行写入HMR代码.<br>
如果一个模块没有HMR处理函数,更新就会冒泡(bubble up).<br>
这意味着某个单独处理函数能够更新整个模块树.<br>
如果在模块树的一个单独模块被更新,那么整组依赖模块都会被重新加载.<br>


有关module.hot接口的详细信息,请查看HMRAPI页面.<br>


#### 在runtime中

这件事情比较有技术性……如果对其内部不感兴趣,可以随时跳到HMRAPI页面或HMR指南.<br>


对于模块系统运行时(module system runtime),会发出额外代码,来跟踪模块parents和children关系.<br>
在管理方面,runtime支持两个方法check和apply.<br>


check方法,发送一个HTTP请求来更新manifest.<br>
如果请求失败,说明没有可用更新.<br>
如果请求成功,会将updatedchunk列表与当前的loadedchunk列表进行比较.<br>
每个loadedchunk都会下载相应的updatedchunk.<br>
当所有更新chunk完成下载,runtime就会切换到ready状态.<br>


apply方法,将所有updatedmodule标记为无效.<br>
对于每个无效module,都需要在模块中有一个updatehandler,或者在此模块的父级模块中有updatehandler.<br>
否则,会进行无效标记冒泡,并且父级也会被标记为无效.<br>
继续每个冒泡,直到到达应用程序入口起点,或者到达带有updatehandler的module(以最先到达为准,冒泡停止).<br>
如果它从入口起点开始冒泡,则此过程失败.<br>


之后,所有无效module都会被(通过disposehandler)处理和解除加载.<br>
然后更新当前hash,并且调用所有accepthandler.<br>
runtime切换回idle状态,一切照常继续.<br>


### 起步

在开发环境,可以将HMR作为LiveReload的替代.<br>
webpack-dev-server支持hot模式,在试图重新加载整个页面之前,hot模式会尝试使用HMR来更新.<br>
更多细节请查看模块热替换指南.<br>


与许多其功能一样,webpack的强大之处在于它的可定制化.<br>
取决于特定项目需求,会有许多方式来配置HMR.<br>
然而,对于多数项目的实现目的来说,webpack-dev-server都能够很好适应,可以帮助在项目中快速应用HMR.<br>

## 揭示内部原理

此章节描述webpack内部实现,对于插件开发人员可能会提供帮助.

打包,是指处理某些文件并将其输出为其文件的能力.<br>


但是,在输入和输出之间,还包括有模块, 入口起点, chunk,chunk组和许多其中间部分.<br>


### 主要部分

项目中使用的每个文件都是一个模块

```js
// ./index.js

import app from './app.js';
```
```js
// ./app.js

export default 'the app';
```

通过互相引用,这些模块会形成一个图(ModuleGraph)数据结构.<br>


在打包过程中,模块会被合并成chunk.<br>
chunk合并成chunk组,并形成一个通过模块互相连接的图(ModuleGraph).<br>
那么如何通过以上来描述一个入口起点:在其内部,会创建一个只有一个chunk的chunk组.<br>

```js
// ./webpack.config.js

module.exports = {
  entry: './index.js',
};
```

这会创建出一个名为main的chunk组(main是入口起点的默认名称).<br>
此chunk组包含 ./index.js模块.<br>
随着parser处理 ./index.js内部的import时, 新模块就会被添加到此chunk中.<br>


另外的一个示例:

```js
// ./webpack.config.js

module.exports = {
  entry: {
    home: './home.js',
    about: './about.js',
  },
};
```

这会创建出两个名为home和about的chunk组.<br>
 每个chunk组都有一个包含一个模块的chunk:./home.js对应home,./about.js对应about

一个chunk组中可能有多个chunk.<br>
例如,SplitChunksPlugin会将一个chunk拆分为一个或多个chunk.<br>


### chunk

chunk有两种形式:

initial(初始化) 是入口起点的mainchunk.<br>
此chunk包含为入口起点指定的所有模块及其依赖项.<br>

non-initial是可以延迟加载的块.<br>
可能会出现在使用动态导入(dynamic imports) 或者SplitChunksPlugin时.<br>

每个chunk都有对应的asset(资源).<br>
资源,是指输出文件(即打包结果).<br>

```js
// webpack.config.js

module.exports = {
  entry: './src/index.jsx',
};
```

```js
// ./src/index.jsx

import React from 'react';
import ReactDOM from 'react-dom';

import('./app.jsx').then((App) => {
  ReactDOM.render(<App />, root);
});
```

这会创建出一个名为main的initialchunk.<br>
其中包含:

0. ./src/index.jsx
0. react
0. react-dom

以及除 ./app.jsx外的所有依赖

然后会为 ./app.jsx创建non-initial chunk,这是因为 ./app.jsx是动态导入的.<br>


Output:

0. /dist/main.js - 一个initialchunk
0. /dist/394.js - non-initial chunk

默认情况下,这些non-initialchunk没有名称,因此会使用唯一ID来替代名称.<br>
在使用动态导入时,可以通过使用magiccomment(魔术注释) 来显式指定chunk名称:

```js
import(
  /* webpackChunkName: "app" */
  './app.jsx'
).then((App) => {
  ReactDOM.render(<App />, root);
});
```

Output:

0. /dist/main.js - 一个initialchunk
0. /dist/app.js - non-initial chunk

### output(输出)

输出文件的名称会受配置中的两个字段的影响:

0. output.filename - 用于initialchunk文件
0. output.chunkFilename - 用于non-initialchunk文件

在某些情况下,使用initial和non-initial的chunk时,可以使用output.filename.<br>

这些字段中会有一些占位符.<br>
常用的占位符如下:

0. [id] - chunk id(例如 [id].js -> 485.js)
0. [name] - chunk name(例如 [name].js -> app.js).<br>
如果chunk没有名称,则会使用其id作为名称
0. [contenthash] - 输出文件内容的md4-hash(例如 [contenthash].js -> 4ea6ff1de66c537eb9b2.js)

## 项目初始化

```bash
npm init -y
npm install webpack webpack-cli css-loader expose-loader source-map-loader style-loader --save-dev
npm install --save lodash


mkdir dist && touch dist/index.html
mkdir src && touch src/index.js
touch webpack.config.js

tee src/index.js <<-'EOF'
import _ from 'lodash';

 function component() {
   const element = document.createElement('div');

   element.innerHTML = _.join(['Hello', 'webpack'], ' ');

   return element;
 }

 document.body.appendChild(component());
EOF

tee dist/index.html <<-'EOF'
 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8" />
     <title>起步</title>
   </head>
   <body>
     <script src="main.js"></script>
   </body>
 </html>
EOF

tee webpack.config.js <<-'EOF'
const path = require('path');

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
};
EOF

sed -i -E '/.*"main":.*/d' package.json
sed -i -E '/^\{$/a \ \ "private": true,' package.json
sed -i -E '/.*"test":.*/c \ \ \ \ "test": "echo \\"Error: no test specified\\" && exit 1",' package.json
sed -i -E '/.*"test":.*/a \ \ \ \ "build": "webpack"' package.json

npm run build
```

## 制作仓库

```js
// webpack.config.js
module.exports = {
  // ...
  output: {
    // ...
    library: {
      // 外部引入名称: import Graphic from '仓库名称'
      name: "仓库名称",
      type: 'umd',
    },
    // 这意味着library 需要一个名为 lodash 的依赖,这个依赖在 consumer 环境中必须存在且可用
    externals: {
      lodash: {
        commonjs: 'lodash',
        commonjs2: 'lodash',
        amd: 'lodash',
        root: '_',
      },
    },
  },
  
};
```


# 参考资料

> [webpack | GettingStarted](https://webpack.js.org/guides/getting-started/)