<!-- TOC -->

- [说明](#说明)
  - [数据库事务](#数据库事务)
  - [IndexedDB](#indexeddb)
    - [基本概念](#基本概念)
      - [IndexedDB数据库使用key-value键值对储存数据](#indexeddb数据库使用key-value键值对储存数据)
      - [IndexedDB是事务模式的数据库](#indexeddb是事务模式的数据库)
      - [The IndexedDBAPI基本上是异步的](#the-indexeddbapi基本上是异步的)
      - [IndexedDB数据库 *请求* 无处不在](#indexeddb数据库-请求-无处不在)
      - [IndexedDB在结果准备好之后通过DOM事件通知用户](#indexeddb在结果准备好之后通过dom事件通知用户)
      - [IndexedDB是面向对象的](#indexeddb是面向对象的)
      - [indexedDB不使用结构化查询语言(SQL)](#indexeddb不使用结构化查询语言sql)
    - [事务型数据库系统](#事务型数据库系统)
    - [对象仓库(object store)](#对象仓库object-store)
    - [版本(version)](#版本version)
    - [数据库连接(database connection)](#数据库连接database-connection)
    - [事务(transaction)](#事务transaction)
    - [请求(request)](#请求request)
    - [索引(index)](#索引index)
    - [键和值](#键和值)
      - [键(key)](#键key)
      - [键生成器(key generator)](#键生成器key-generator)
      - [内键(in-line key)](#内键in-line-key)
      - [外键(out-of-line key)](#外键out-of-line-key)
      - [键路径(key path)](#键路径key-path)
      - [值(value)](#值value)
    - [范围和作用域](#范围和作用域)
      - [作用域(scope)](#作用域scope)
      - [游标(cursor)](#游标cursor)
    - [键范围(key range)](#键范围key-range)
    - [局限性](#局限性)
    - [同源策略](#同源策略)
    - [同步和异步(Synchronous、asynchronous)](#同步和异步synchronousasynchronous)
    - [储存限制和回收标准](#储存限制和回收标准)
      - [组限制](#组限制)
      - [LRU策略](#lru策略)
    - [基本模式](#基本模式)
    - [浏览器前缀](#浏览器前缀)
    - [key path](#key-path)
    - [key generator](#key-generator)
- [常用动作示例](#常用动作示例)
  - [打开数据库](#打开数据库)
  - [生成处理函数](#生成处理函数)
  - [创建和更新数据库版本号](#创建和更新数据库版本号)
  - [构建数据库](#构建数据库)
  - [使用键生成器](#使用键生成器)
  - [增加、读取和删除数据](#增加读取和删除数据)
  - [向数据库中增加数据](#向数据库中增加数据)
  - [从数据库中删除数据](#从数据库中删除数据)
  - [从数据库中获取数据](#从数据库中获取数据)
  - [更新数据库中的记录](#更新数据库中的记录)
  - [使用游标](#使用游标)
  - [使用索引](#使用索引)
  - [指定游标的范围和方向](#指定游标的范围和方向)
  - [](#)
  - [](#-1)
  - [](#-2)
  - [](#-3)
- [IDBRequest对象](#idbrequest对象)
  - [连接数据库](#连接数据库)
    - [IDBFactory](#idbfactory)
    - [IDBOpenDBRequest](#idbopendbrequest)
    - [IDBDatabase](#idbdatabase)
  - [接收和修改数据](#接收和修改数据)
    - [IDBTransaction](#idbtransaction)
    - [IDBRequest](#idbrequest)
    - [IDBObjectStore](#idbobjectstore)
    - [IDBIndex](#idbindex)
    - [IDBCursor](#idbcursor)
    - [IDBCursorWithValue](#idbcursorwithvalue)
    - [IDBKeyRange](#idbkeyrange)
    - [IDBLocaleAwareKeyRange](#idblocaleawarekeyrange)
  - [自定义事件接口](#自定义事件接口)
    - [IDBVersionChangeEvent](#idbversionchangeevent)
    - [](#-4)
    - [](#-5)
    - [](#-6)
    - [](#-7)
    - [](#-8)
    - [](#-9)
    - [](#-10)
    - [](#-11)
    - [](#-12)
    - [](#-13)
    - [](#-14)
    - [](#-15)
    - [](#-16)
    - [](#-17)
    - [](#-18)
    - [](#-19)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## 数据库事务

> 数据库事务(简称:事务)是数据库管理系统执行过程中的一个逻辑单位,由一个有限的数据库操作序列构成.

数据库事务通常包含了一个序列的对数据库的读/写操作.<br>
包含有以下两个目的:

0. 为数据库操作序列提供了一个从失败中恢复到正常状态的方法,同时提供了数据库即使在异常状态下仍能保持一致性的方法.<br>

0. 当多个应用程序在并发访问数据库时,可以在这些应用程序之间提供一个隔离方法,以防止彼此的操作互相干扰.<br>

当事务被提交给了数据库管理系统(DBMS),则DBMS需要确保该事务中的所有操作都成功完成且其结果被永久保存在数据库中,如果事务中有的操作没有成功完成,则事务中的所有操作都需要回滚,回到事务执行前的状态;同时,该事务对数据库或者其他事务的执行无影响,所有的事务都好像在独立的运行.

某人要在商店使用电子货币购买100元的东西,当中至少包括两个操作:

0. 该人账户减少100元
0. 商店账户增加100元

支持事务的数据库管理系统(transactional DBMS)就是要确保以上两个操作(整个*事务*)都能完成,或一起取消.<br>否则就会出现100元平白消失或出现的情况.

但在现实情况下,失败的风险很高.<br>
在一个数据库事务的执行过程中,有可能会遇上事务操作失败、数据库系统／操作系统出错,甚至是存储介质出错等情况.<br>
这便需要DBMS对一个执行失败的事务执行恢复操作,将其数据库状态恢复到一致状态(数据的一致性得到保证的状态).<br>
为了实现将数据库状态恢复到一致状态的功能,DBMS通常需要维护事务日志以追踪事务中所有影响数据库数据的操作.

## IndexedDB

> IndexedDB是一种在用户浏览器中持久存储数据的方法.<br>
它允许不考虑网络可用性,创建具有丰富查询能力的可离线Web应用程序.<br>
IndexedDB对于存储大量数据的应用程序(例如借阅库中的DVD目录)和不需要持久Internet连接的应用程序(例如邮件客户端、待办事项列表或记事本)很有用.

IndexedDB是一种底层API,用于在客户端存储大量的结构化数据(也包括文件/二进制大型对象(blobs)).<br>
该API使用索引实现对数据的高性能搜索.<br>
虽然WebStorage在存储较少量的数据很有用,但对于存储更大量的结构化数据来说力不从心.<br>
而IndexedDB提供了这种场景的解决方案.

IndexedDB API是强大的,但对于简单的情况可能看起来太复杂.<br>
如果喜欢一个简单的API,请尝试localForage、dexie.js、PouchDB、idb、idb-keyval、JsStore或者lovefield之类的库,<br>
这些库使IndexedDB对开发者来说更加友好.<br>

使用IndexedDB,可以使用一个key作为索引进行存储或者获取数据.<br>
可以在事务(transaction)中完成对数据的修改.<br>
和大多数web存储解决方案相同,indexedDB也遵从同源协议(same-origin policy). 所以只能访问同域中存储的数据,而不能访问其他域的.

IndexedDB是一种异步(asynchronous) API,异步API适用于大多数情况,包括Web Workers.<br>
因为在Web Workers上的使用,它过去也有一个同步(synchronous)的版本,但是因为缺少web社区的支持,它已经被从规范中移除了.

IndexedDB过去有一个竞争规范——WebSQL数据库,但是W3C组织在2010年11月18日废弃了webSql.<br>
尽管两者都是存储的解决方案,但是提供的不是同样的功能.<br>
IndexedDB和WebSQL的不同点在于WebSQL是关系型数据库访问系统,IndexedDB是索引表系统(key-value型).<br>

### 基本概念

#### IndexedDB数据库使用key-value键值对储存数据

values数据可以是结构非常复杂的对象,key可以是对象自身的属性.<br>
可以对对象的任何属性创建索引(index)以实现快速查询和列举排序.<br>
key可以是二进制对象.

#### IndexedDB是事务模式的数据库

任何操作都发生在事务(transaction)中.<br>
IndexedDB API提供了索引(indexes)、表(tables)、指针(cursors)等等,但是所有这些必须是依赖于某种事务的.<br>
因此,不能在事务外执行命令或者打开指针.<br>
事务(transaction)有生存周期,在生存周期以后使用它会报错.<br>
并且,事务(transaction)是自动提交的,不可以手动提交.

当用户在不同的标签页同时打开Web应用的两个实例时,这个事务模型就会非常有用.<br>
如果没有事务操作的支持,这两个实例就会互相影响对方的修改.

#### The IndexedDBAPI基本上是异步的

IndexedDB的API不通过return语句返回数据,而是需要提供一个回调函数来接受数据.<br>
执行API时,不以同步(synchronous)方式对数据库进行 *存储*和*读取*操作,而是向数据库发送一个操作*请求* .<br>
当操作完成时,数据库会以DOM事件的方式通知,同时事件的类型会告诉这个操作是否成功完成.<br>
这个过程听起来会有些复杂,但是里面是有明智的原因的.<br>
这个和XMLHttpRequest请求是类似的.

#### IndexedDB数据库 *请求* 无处不在

上边提到,数据库 *请求* 负责接受成功或失败的DOM事件.<br>
每一个 *请求*都包含onsuccess和onerror事件属性,同时还对*事件* 调用addEventListener()和removeEventListener().<br>
*请求 *还包括readyState,result和errorCode属性,用来表示*请求* 的状态.<br>
result属性尤其神奇,他可以根据 *请求* 生成的方式变成不同的东西,例如:IDBCursor实例、刚插入数据库的数值对应的键值(key)等.

#### IndexedDB在结果准备好之后通过DOM事件通知用户

DOM事件总是有一个类型(type)属性(在IndexedDB中,该属性通常设置为success或error).<br>
DOM事件还有一个目标(target)属性,用来告诉事件是被谁触发的.<br>
通常情况下,目标(target)属性是数据库操作生成的IDBRequest.<br>
成功(success)事件不弹出提示并且不能撤销,错误(error)事件会弹出提示且可以撤销.<br>
这一点是非常重要的,因为除非错误事件被撤销,否则会终止所在的任何事务.

#### IndexedDB是面向对象的

indexedDB不是用二维表来表示集合的关系型数据库.<br>
这一点非常重要,将影响设计和建立应用程序.

传统的关系型数据库,需要用到二维表来存储数据集合(每一行代表一个数据,每一列代表一个属性),indexedDB有所不同,它要求为一种数据创建一个对象仓库(object Store),只要这种数据是一个JavaScript对象即可.

#### indexedDB不使用结构化查询语言(SQL)

它通过索引(index)所产生的指针(cursor)来完成查询操作,从而使可以迭代遍历到结果集合.

### 事务型数据库系统

IndexedDB是一个事务型数据库系统,类似于基于SQL的RDBMS.<br>
然而,不像RDBMS使用固定列表,IndexedDB是一个基于JavaScript的面向对象数据库.<br>
IndexedDB允许存储和检索用键索引的对象;可以存储结构化克隆算法支持的任何对象.<br>
只需要指定数据库模式,打开与数据库的连接,然后检索和更新一系列事务.<br>

### 对象仓库(object store)

数据在数据库中存储的方式, 数据以键值对形式被对象仓库永久持有.<br>
对象仓库中的的数据以keys升序排列.

每一个对象仓库在同一个数据库中必须有唯一的名字.<br>
对象存储可以有一个keygenerator和一个keypath.<br>
如果对象仓库有keypath,则使用in-line keys; 否则使用out-of-line keys.

### 版本(version)

当第一次创建一个数据库,它的版本为整型1.<br>
每个数据库依次有一个版本号; 一个数据库不能同时存在多个版本号.<br>
改变版本的唯一方法是通过一个比当前版本号更高的值去打开数据库.<br>
这会开启一个VERSION_CHANGE事务并且触发upgradeneeded事件.<br>
只有在该事件的处理函数中才能更新数据库模式.

### 数据库连接(database connection)

通过打开数据库创建的操作.<br>
一个给定的数据库可以同时拥有多个连接.

### 事务(transaction)

在一个特定的数据库上,一组具备原子性和持久性的数据访问和数据修改的操作.<br>
它是与数据库交互的方式.<br>
并且,任何对于数据库中的数据读和修改的操作只能在事务中进行.<br>

一个数据库连接可以拥有多个与之关联的事务,只要进行写操作事务的作用域不相互重合.<br>
事务的作用域在事务被创建时就被确定,指定事务能够进行交互的对象仓库(object store),作用域一旦被确定就会在整个生命周期中保持不变.<br>
举个例子,如果一个数据库连接已经有了一个进行写操作的事务,其作用域覆盖flyingMonkey对象仓库,可以开启新的事务其作用于unicornCentaur和unicornPegasus对象仓库.<br>
对于读操作的事务,可以同时拥有多个,即使有重叠的作用域.

事务被期望拥有较短的生命周期,所以浏览器会终止一个消耗时间过长的事务,为了释放存储资源,运行过久的事务会被锁定.<br>
可以中断一个事务,来回滚事务中对数据库进行的操作.<br>
并且甚至不需要等待事务开始或激活就可以中断它.

事务有三种模式:读写、只读和版本变更.<br>
创建和删除对象仓库(object store)的唯一方法就是通过调用版本变更事务.<br>
了解更多关于事务类型的内容,请参考IndexedDB.

因为所有的事情都在事务中发生,所以它是IndexedDB中非常重要的一个概念.<br>
了解更多关于事务,尤其是关于它和版本控制的关联,查看IDBTransaction中的参考文档.<br>
关于同步接口的文档,查看IDBTransactionSync.

### 请求(request)

在数据库上进行读写操作完成后的操作.<br>
每一个请求代表一个读或写操作.

### 索引(index)

一个对象仓库,专门用来查找另一个对象仓库(object store)中的记录,其中被查找的对象仓库被称为引用对象仓库.<br>
索引(index)是一个稳定的键值对(key-value)存储,其记录中的值(value)是引用对象仓库记录中的键(key).<br>
当引用对象仓库中的记录新增、更新或删除时,索引中的记录会自动的进行粒子性增加.<br>
索引中的每一条记录只能指向引用对象仓库中的一条记录,但多个索引可以引用同一个对象仓库.<br>
当对象仓库发生改变时,所有引用该对象仓库的引用均会自动更新.

可选地,也可以适用键(key)再对象仓库中查找记录.

了解更多关于如何适用索引,查看使用IndexedDB.<br>
index的参考文档查看IDBKeyRange.

### 键和值

#### 键(key)

在对象仓库中阻止和检索被存储起来的值的数据值.<br>
数据仓库的键来源于以下三个方式之一:键生成器、键路径和显式指定的值.<br>
键必须是一种能够比较大小的数据类型.<br>
在同一个对象仓库中每条记录必须有一个独一无二的键,所以不能在同一个对象仓库中为多个记录设置同样的键.<br>

键可以是以下数据类型:字符串、日期、浮点和数组.<br>
对于数组,键的取值可以从空数组到无穷.<br>
并且可以使用嵌套数组.

可选地,也可以通过索引(index)来查找记录.

#### 键生成器(key generator)

一种生成有序键的机制.<br>
如果一个对象仓库并不具备一个键生成器,那么应用程序必须为被存储的记录提供键.<br>
生成器在仓库之间并不共享.<br>
它更多的是浏览器的实现细节,因为在Web开发中并不会真正的去创建或访问键生成器.

#### 内键(in-line key)

作为存储值一部分的键.<br>
内键由键路径(key path)查找.<br>
内键由生成器生成.<br>
当内键生成后,它会被键路径存储在值中,它也可以被当作键使用.


#### 外键(out-of-line key)

与值分开存储的键.

#### 键路径(key path)

指定浏览器如何从对象仓库或索引存储的值中提取键.<br>
一个合法的键路径可以是以下形式:一个空字符串,一个JavasScript标识符,或由句点分割的多个JavaScript标识符.<br>
但不能包括空格.

#### 值(value)

每条记录包含一个值,该值可以包含任何JavaScript表达式,包括:布尔、数字、字符串、日期、对象、数组、正则、未定义和null.

对于对象和数组,它们的属性和值也可以是任意合法的值.


### 范围和作用域

#### 作用域(scope)

事务所作用的一组对象仓库或索引.<br>
只读事务的作用域可以相互重叠并同时执行操作.<br>
但写操作事务的作用域不可以相互重叠.<br>
但仍然可以同时开启多个拥有相同作用域的事务,只要保证操作不会同时执行.

#### 游标(cursor)

在键的某个范围内迭代查询多条记录的机制.<br>
游标有一个指向正在被迭代的对象仓库或索引的源.<br>
它处于该范围内的一个位置,并按照键的顺序正向或逆向的移动.<br>
有关游标的参考文档,查看IDBCursor或IDBCursorSync.

### 键范围(key range)

用做键的数据类型上的连续的间隔.<br>
使用键或键的某个范围可以从对象仓库和索引中读取记录.<br>
可以通过上限和下限设置和筛选范围.<br>
比如,可以遍历x和y之间所有的键值.

### 局限性

以下情况不适合使用IndexedDB

0. 全球多种语言混合存储.国际化支持不好.需要自己处理.
0. 和服务器端数据库同步.得自己写同步代码.
0. 全文搜索.IndexedDB接口没有类似SQL语句中LIKE的功能.

注意,在以下情况下,数据库可能被清除:

0. 用户请求清除数据.
0. 浏览器处于隐私模式.最后退出浏览器的时候,数据会被清除.
0. 硬盘等存储设备的容量到限.
0. 数据损坏.
0. 进行与特性不兼容的操作.

确切的环境和浏览器特性会随着时间改变,但浏览器厂商通常会遵循尽最大努力保留数据的理念.

### 同源策略

正如大多数的web储存解决方案一样,IndexedDB也遵守同源策略.<br>
因此当在某个域名下操作储存数据的时候,不能操作其他域名下的数据.<br>

### 同步和异步(Synchronous、asynchronous)

使用IndexedDB执行的操作是异步执行的,以免阻塞应用程序.<br>
IndexedDB最初包括同步和异步API.<br>
同步API仅用于WebWorkers,且已从规范中移除,因为尚不清晰是否需要.<br>
但如果Web开发人员有足够的需求,可以重新引入同步API.<br>

### 储存限制和回收标准

有许多Web技术在客户端(即本地磁盘)存储各种数据.<br>
IndexedDB是最常见的一个.<br>
浏览器计算分配给Web数据存储的空间以及达到该限制时要删除的内容的过程并不简单,并且在浏览器之间有所不同.<br>
浏览器存储限制和回收标准尝试解释这是如何工作的,至少在火狐的情况下是如此:<br>

浏览器的最大存储空间是动态的——它取决于硬盘大小.<br>
全局限制为可用磁盘空间的50％.<br>
在Firefox中,一个名为Quota Manager的内部浏览器工具会跟踪每个源用尽的磁盘空间,并在必要时删除数据.<br>

因此,如果硬盘驱动器是500GB,那么浏览器的总存储容量为250GB.<br>
如果超过此范围,则会发起称为源回收的过程,删除整个源的数据,直到存储量再次低于限制.<br>
删除源数据没有只删一部分的说法——因为这样可能会导致不一致的问题.<br>

#### 组限制

还有另一个限制称为 **组限制** ——这被定义为全局限制的20％,但它至少有10 MB,最大为2GB.<br>
每个源都是一组(源组)的一部分.每个eTLD+1域都有一个组.例如:<br>

* mozilla.org——组1,源1
* www.mozilla.org——组1,源2
* joe.blogs.mozilla.org——组1,源3
* firefox.com——组2,源4

在这个组中,mozilla.org、www.mozilla.org和joe.blogs.mozilla.org可以聚合使用最多20％的全局限制.<br>
firefox.com单独最多使用20％.<br>

达到限制后有两种不同的反应:

0. 组限制也称为*硬限制*:它不会触发源回收.
0. 全局限制是一个 *软限制* ,因为其有可能释放一些空间并且这个操作可能持续.

注意:尽管上面提到了最小组限制,但组限制不能超过全局限制.<br>
如果内存非常低,全局限制为8 MB,则组限制也将为8 MB.

注意:如果超出组限制,或者如果原因驱逐无法释放足够的空间,浏览器将抛出QuotaExceededError错误.

#### LRU策略

当可用磁盘空间已满时,配额管理器将根据LRU策略开始清除数据——最近最少使用的源将首先被删除,然后是下一个,直到浏览器不再超过限制.

使用临时存储跟踪每个源的 *上次访问时间* .<br>
一旦达到临时存储的全局限制(之后会有更多限制),将尝试查找所有当前未使用的源(即没有打开选项卡/应用程序的那些来保持打开的数据存储).<br>
然后根据 *上次访问时间* 对它们进行排序.<br>
然后删除最近最少使用的源,直到有足够的空间来满足触发此源回收的请求.

### 基本模式

IndexedDB鼓励使用的基本模式如下所示:

0. 打开数据库.
0. 在数据库中创建一个对象仓库(object store).
0. 启动一个事务,并发送一个请求来执行一些数据库操作,像增加或提取数据等.
0. 通过监听正确类型的DOM事件以等待操作完成.
0. 在操作结果上进行一些操作(可以在request对象中找到).

### 浏览器前缀

由于IndexedDB本身的规范还在持续演进中,当前的IndexedDB的实现还是使用浏览器前缀.<br>
在规范更加稳定之前,浏览器厂商对于标准IndexedDBAPI可能都会有不同的实现.<br>
但是一旦大家对规范达成共识的话,厂商就会不带前缀标记地进行实现.<br>
实际上一些实现已经移除了浏览器前缀:IE 10,Firefox16和Chrome24.<br>
当使用前缀的时候,基于Gecko内核的浏览器使用moz前缀,基于WebKit内核的浏览器会使用webkit前缀.

### key path

### key generator

# 常用动作示例

## 打开数据库

```js
var request = window.indexedDB.open("MyTestDatabase");
```

open请求不会立即打开数据库或者开始一个事务.<br>
对open() 函数的调用会返回一个可以作为事件来处理的包含result(成功的话)或者错误值的IDBOpenDBRequest对象.<br>
在IndexedDB中的大部分异步方法做的都是同样的事情 - 返回一个包含result或错误的IDBRequest对象.<br>
open函数的结果是一个IDBDatabase对象的实例.

该open方法接受第二个参数,就是数据库的版本号.<br>
数据库的版本决定了数据库架构,即数据库的对象仓库(object store)和他的结构.<br>
如果数据库不存在,open操作会创建该数据库,然后onupgradeneeded事件被触发,需要在该事件的处理函数中创建数据库模式.<br>
如果数据库已经存在,但指定了一个更高的数据库版本,会直接触发onupgradeneeded事件,允许在处理函数中更新数据库模式.<br>
在后面的更新数据库的版本号和IDBFactory.open中会提到更多有关这方面的内容.

版本号是一个unsigned long long数字,这意味着它可以是一个特别大的数字,但 **不能使用浮点数** ,否则它将会被转变成离它最近的整数,这可能导致upgradeneeded事件不会被触发.<br>
例如,不要使用2.4作为版本号,这样版本号会被重置为2.

## 生成处理函数

几乎所有产生的请求在处理的时候首先要做的就是添加成功和失败处理函数:

```js
request.onerror = function(event) {
  // Do something with request.errorCode!
};
request.onsuccess = function(event) {
  // Do something with request.result!
};
```

如果一切顺利的话,一个success事件(即一个type属性被设置成 *success* 的DOM事件)会被触发,request会作为它的target.<br>
一旦它被触发的话,相关request的onsuccess() 处理函数就会被触发,使用success事件作为它的参数.<br>
否则,如果不是所有事情都成功的话,一个error事件(即type属性被设置成 *error* 的DOM事件) 会在request上被触发.<br>
这将会触发使用error事件作为参数的onerror() 方法.<br>

IndexedDB的API被设计来尽可能地减少对错误处理的需求,所以可能不会看到有很多的错误事件(起码,不会在已经习惯了这些API之后!).<br>
然而在打开数据库的情况下,还是有一些会产生错误事件的常见情况.<br>
最有可能出现的问题是用户决定不允许webapp访问以创建一个数据库.<br>
IndexedDB的主要设计目标之一就是允许大量数据可以被存储以供离线使用.

> 浏览器不希望允许某些广告网络或恶意网站来污染计算机,所以浏览器会在任意给定的webapp首次尝试打开一个IndexedDB存储时对用户进行提醒.<br>
用户可以选择允许访问或者拒绝访问.<br>
还有,IndexedDB在浏览器的隐私模式(Firefox的PrivateBrowsing模式和Chrome的Incognito模式)下是被完全禁止的.<br>
隐私浏览的全部要点在于不留下任何足迹,所以在这种模式下打开数据库的尝试就失败了.<br>

假设用户已经允许了要创建一个数据库的请求,同时也已经收到了一个来触发success回调的success事件.<br>
这里的request是通过调用indexedDB.open() 产生的,所以request.result是一个IDBDatabase的实例,而且肯定希望把它保存下来以供后面使用.<br>
代码看起来可能像这样:

```js
var db;
var request = indexedDB.open("MyTestDatabase");
request.onerror = function(event) {
  alert("Why didn*t you allow my web app to use IndexedDB?!");
};
request.onsuccess = function(event) {
  db = event.target.result;
};
```

## 创建和更新数据库版本号 

```js
// 该事件仅在较新的浏览器中实现了
request.onupgradeneeded = function(event) {
  // 保存IDBDataBase接口
  var db = event.target.result;

  // 为该数据库创建一个对象仓库
  var objectStore = db.createObjectStore("name",{keyPath: "myKey"});
};
```

在这种情况下,数据库会保留之前版本数据库的对象仓库(object store),因此不必再次创建这些对象仓库.<br>
需要创建新的对象仓库,或删除不再需要的上一版本中的对象仓库.<br>
如果需要修改一个已存在的对象仓库(例如要修改keyPath),必须先删除原先的对象仓库然后使用新的设置创建.<br>
(注意,这样会丢失对象仓库里的数据,如果需要保存这些信息,要在数据库版本更新前读取出来并保存在别处).

尝试创建一个与已存在的对象仓库重名(或删除一个不存在的对象仓库)会抛出错误.<br>

如果onupgradeneeded事件成功执行完成,打开数据库请求的onsuccess处理函数会被触发.

## 构建数据库

现在来构建数据库,IndexedDB使用对象存仓库而不是表,并且一个单独的数据库可以包含任意数量的对象存储空间.<br>
每当一个值被存储进一个对象存储空间时,它会被和一个键相关联.<br>
键的提供可以有几种不同的方法,这取决于对象存储空间是使用keypath还是keygenerator.

下面的表格显示了几种不同的提供键的方法:

键路径(keyPath)|键生成器(autoIncrement)|描述
-|-|-
No|No|这种对象存储空间可以持有任意类型的值,甚至是像数字和字符串这种基本数据类型的值.<br>每当想要增加一个新值的时候,必须提供一个单独的键参数.
Yes|No|这种对象存储空间只能持有JavaScript对象.<br>这些对象必须具有一个和keypath同名的属性.
No|Yes|这种对象存储空间可以持有任意类型的值.<br>键会为自动生成,或者如果想要使用一个特定键的话可以提供一个单独的键参数.
Yes|Yes|这种对象存储空间只能持有JavaScript对象.<br>通常一个键被生成的同时,生成的键的值被存储在对象中的一个和keypath同名的属性中.<br>然而,如果这样的一个属性已经存在的话,这个属性的值被用作键而不会生成一个新的键.

也可以使用对象存储空间持有的对象,不是基本数据类型,在任何对象存储空间上创建索引.<br>
索引可以让使用被存储的对象的属性的值来查找存储在对象存储空间的值,而不是用对象的键来查找.

此外,索引具有对存储的数据执行简单限制的能力.<br>
通过在创建索引时设置unique标记,索引可以确保不会有两个具有同样索引keypath值的对象被储存.<br>
因此,举例来说,如果有一个用于持有一组people的对象存储空间,并且想要确保不会有两个拥有同样email地址的people,可以使用一个带有unique标识的索引来确保这些.

例如原始数据如下:

```js
const customerData = [
  {ssn: "444-44-4444",name: "Bill",age: 35,email: "bill@company.com"},
  {ssn: "555-55-5555",name: "Donna",age: 32,email: "donna@home.org"}
];
```

创建一个IndexedDB来存储上面的数据:

```js
const dbName = "the_name";

var request = indexedDB.open(dbName, 2);

request.onerror = function(event) {
  // 错误处理
};
request.onupgradeneeded = function(event) {
  var db = event.target.result;

  // 建立一个对象仓库来存储客户的相关信息,选择ssn作为键路径(key path)
  // 因为ssn可以保证是不重复的
  var objectStore = db.createObjectStore("customers", {keyPath: "ssn" });

  // 建立一个索引来通过姓名来搜索客户,名字可能会重复,所以不能使用unique索引
  objectStore.createIndex("name", "name", {unique:false});

  // 使用邮箱建立索引,向确保客户的邮箱不会重复,所以使用unique索引
  objectStore.createIndex("email", "email", {unique:true});

  // 使用事务的oncomplete事件确保在插入数据前对象仓库已经创建完毕
  objectStore.transaction.oncomplete = function(event) {
    // 将数据保存到新创建的对象仓库
    var customerObjectStore = db.transaction("customers", "readwrite").objectStore("customers");
    customerData.forEach(function(customer) {
      customerObjectStore.add(customer);
    });
  };
};
```

正如前面提到的,onupgradeneeded是唯一可以修改数据库结构的地方.<br>
在这里面,可以创建和删除对象存储空间以及构建和删除索引.

对象仓库仅调用createObjectStore() 就可以创建.<br>
这个方法使用仓库的名称,和一个参数对象.<br>
即便这个参数对象是可选的,它还是非常重要的,因为它可以让定义重要的可选属性,并完善希望创建的对象存储空间的类型.<br>
在示例中,创建了一个名为 *customers* 的对象仓库并且定义了一个使得每个仓库中每个对象都独一无二的keyPath.<br>
在这个示例中的属性是 *ssn* ,因为社会安全号码被确保是唯一的.<br>
被存储在该仓库中的所有对象都必须存在 *ssn* .

也请求了一个名为 *name* 的着眼于存储的对象的name属性的索引.<br>
如同createObjectStore(),createIndex() 提供了一个可选地options对象,该对象细化了希望创建的索引类型.<br>
新增一个不带name属性的对象也会成功,但是这个对象不会出现在 "name" 索引中.

现在可以使用存储的用户对象的ssn直接从对象存储空间中把它们提取出来,或者通过使用索引来使用name进行提取.

## 使用键生成器

在创建对象仓库时设置autoIncrement标记会为该仓库开启键生成器.<br>
默认该设置是不开启的.

使用键生成器,当向对象仓库新增记录时键会自动生成.<br>
对象仓库生成的键往往从1开始,然后自动生成的新的键会在之前的键的基础上加1.<br>
生成的键的值从来不会减小,除非数据库操作结果被回滚,比如,数据库事务被中断.<br>
因此删除一条记录,甚至清空对象仓库里的所有记录都不会影响对象仓库的键生成器.

可以使用键生成器创建一个对象仓库:

```js
// 打开indexedDB.
var request = indexedDB.open(dbName, 3);

request.onupgradeneeded = function (event) {

    var db = event.target.result;

    // 设置autoIncrement标志为true来创建一个名为names的对象仓库
    var objStore = db.createObjectStore("names", {autoIncrement:true});

    // 因为names对象仓库拥有键生成器,所以它的键会自动生成.
    // 被插入的数据可以表示如下:
    // key : 1 => value : "Bill"
    // key : 2 => value : "Donna"
    customerData.forEach(function(customer) {
        objStore.add(customer.name);
    });
};
```

## 增加、读取和删除数据

需要开启一个事务才能对创建的数据库进行操作.<br>
事务来自于数据库对象,而且必须指定想让这个事务跨越哪些对象仓库.<br>
一旦处于一个事务中,就可以目标对象仓库发出请求.<br>
要决定是对数据库进行更改还是只需从中读取数据.<br>
事务提供了三种模式:readonly、readwrite和versionchange.

想要修改数据库模式或结构——包括新建或删除对象仓库或索引,只能在versionchange事务中才能实现.<br>
该事务由一个指定了version的IDBFactory.open方法启动.<br>
(在仍未实现最新标准的WebKit浏览器),IDBFactory.open方法只接受一个参数,即数据库的name,这样必须调用IDBVersionChangeRequest.setVersion来建立versionchange事务.

使用readonly或readwrite模式都可以从已存在的对象仓库里读取记录.<br>
但只有在readwrite事务中才能修改对象仓库.<br>
需要使用IDBDatabase.transaction启动一个事务.<br>
该方法接受两个参数:storeNames (作用域,一个想访问的对象仓库的数组),事务模式mode(readonly或readwrite).<br>
该方法返回一个包含IDBIndex.objectStore方法的事务对象,使用IDBIndex.objectStore可以访问对象仓库.<br>
未指定mode时,默认为readyonly模式.<br>

可以通过使用合适的作用域和模式来加速数据库访问,这有两个提示:

* 定义作用域时,只指定用到的对象仓库.这样,可以同时运行多个不含互相重叠作用域的事务.<br>

* 只在必要时指定readwrite事务.可以同时执行多个readnoly事务,哪怕它们的作用域有重叠;但对于在一个对象仓库上只能运行一个readwrite事务.<br>
了解更多,请查看基本概念中事务的定义.<br>

## 向数据库中增加数据

如果刚刚创建了一个数据库,可能想往里面写点东西.<br>
看起来会像下面这样:

```js
var transaction = db.transaction(["customers"], "readwrite");
```

transaction() 方法接受两个参数(一个是可选的)并返回一个事务对象.<br>
第一个参数是事务希望跨越的对象存储空间的列表.<br>
如果希望事务能够 **跨越所有的对象存储空间** 可以传入一个空数组,但请不要这样做,因为标准规定传入一个空数组会导致一个InvalidAccessError(可以使用属性db.objectStoreNames).<br>
如果没有为第二个参数指定任何内容,得到的是 **只读事务** .<br>
如果想写入数据,需要传入 "readwrite" 标识.

现在已经有了一个事务,需要理解它的生命周期.<br>
事务和事件循环的联系非常密切.<br>
如果创建了一个事务但是并没有使用它就返回给事件循环,那么事务将会失活.<br>
保持事务活跃的唯一方法就是在其上构建一个请求.<br>
当请求完成时将会得到一个DOM事件,并且,假设请求成功了,将会有另外一个机会在回调中来延长这个事务.<br>
如果没有延长事务就返回到了事件循环,那么事务将会变得不活跃,依此类推.<br>
只要还有待处理的请求事务就会保持活跃.<br>
事务生命周期真的很简单但是可能需要一点时间才能对它变得习惯.<br>
如果开始看到TRANSACTION_INACTIVE_ERR错误代码,那么已经把某些事情搞乱了.<br>

事务接收三种不同的DOM事件:error、abort和complete.<br>
已经提及error事件是冒泡机制,所以事务会接收由它产生的所有请求所产生的错误.<br>
更微妙的一点,错误会中断它所处的事务.<br>
除非在错误发生的第一时间就调用了stopPropagation并执行了其他操作来处理错误,不然整个事务将会回滚.<br>
这种机制迫使考虑和处理错误场景,如果觉得细致的错误处理太繁琐,可以在数据库上添加一个全局的错误处理.<br>
如果在事务中没有处理一个已发生的错误或者调用abort方法,那么该事务会被回滚,并触发abort事件.<br>
另外,在所有请求完成后,事务的complete事件会被触发.<br>
如果进行大量数据库操作,跟踪事务而不是具体的请求会使逻辑更加清晰.<br>

现在拥有了一个事务,需要从中取出一个对象仓库.<br>
只能在创建事务时指定的对象仓库中取出一个对象仓库.<br>
然后可以添加任何需要的数据.

```js
// 在所有数据添加完毕后的处理
transaction.oncomplete = function(event) {
  alert("All done!");
};

transaction.onerror = function(event) {
  // 不要忘记错误处理!
};

var objectStore = transaction.objectStore("customers");
customerData.forEach(function(customer) {
  var request = objectStore.add(customer);
  request.onsuccess = function(event) {
    // event.target.result === customer.ssn;
  };
});
```

调用add() 方法产生的请求的result是被添加的数据的键.<br>
所以在该例中,它应该全等于被添加对象的ssn属性,因为对象仓库使用ssn属性作为键路径(key path).<br>
注意,add() 方法的调用时,对象仓库中不能存在相同键的对象.<br>
如果想修改一个已存在的条目,或者不关心该数据是否已存在,可以使用put() 方法,就像下面Updatingan entry in thedatabase模块所展示的.

## 从数据库中删除数据

```js
var request = db.transaction(["customers"], "readwrite")
                .objectStore("customers")
                .delete("444-44-4444");
request.onsuccess = function(event) {
  // 删除成功!
};
```

## 从数据库中获取数据

现在数据库里已经有了一些信息,可以通过几种方法对它进行提取.首先是简单的get().<br>
需要提供键来提取值,像这样:

```js
var transaction = db.transaction(["customers"]);
var objectStore = transaction.objectStore("customers");
var request = objectStore.get("444-44-4444");
request.onerror = function(event) {
  // 错误处理!
};
request.onsuccess = function(event) {
  // 对request.result做些操作!
  alert("Name for SSN 444-44-4444 is " + request.result.name);
};
```

对于一个 *简单* 的提取这里的代码有点多了.<br>
下面看怎么把它再缩短一点,假设在数据库的级别上来进行的错误处理:

```js
db.transaction("customers").objectStore("customers").get("444-44-4444").onsuccess = function(event) {
  alert("Name for SSN 444-44-4444 is " + event.target.result.name);
};
```

看看这是怎么回事.因为这里只用到一个对象仓库,可以只传该对象仓库的名字作为参数,而不必传一个列表.<br>
并且,只需读取数据,所以不需要readwrite事务.<br>
不指定事务模式来调用transaction会得到一个readonly事务.<br>
另外一个微妙的地方在于并没有保存请求对象到变量中.<br>
因为DOM事件把请求作为他的目标(target),可以使用该事件来获取result属性.

注意,可以通过限制事务的作用域和模式来加速数据库访问.<br>
这里有两个提醒:

* 定义作用域时,只指定用到的对象仓库.这样,可以同时运行多个不含互相重叠作用域的事务.<br>

* 只在必要时指定readwrite事务.可以同时执行多个readnoly事务,哪怕它们的作用域有重叠.<br>
但对于在一个对象仓库上只能运行一个readwrite事务.<br>
了解更多,请查看基本概念中事务的定义.

## 更新数据库中的记录

这里创建了一个objectStore,并通过指定ssn值(444-44-4444)从中请求了一条客户记录.<br>
然后把请求的结果保存在变量data中,并更新了该对象的age属性,之后创建了第二个请求(requestUpdate)将客户数据放回objectStore来覆盖之前的值.<br>


```js
var objectStore = db.transaction(["customers"], "readwrite").objectStore("customers");
var request = objectStore.get("444-44-4444");
request.onerror = function(event) {
  // 错误处理
};
request.onsuccess = function(event) {
  // 获取想要更新的数据
  var data = event.target.result;

  // 更新想修改的数据
  data.age = 42;

  // 把更新过的对象放回数据库
  var requestUpdate = objectStore.put(data);
   requestUpdate.onerror = function(event) {
     // 错误处理
   };
   requestUpdate.onsuccess = function(event) {
     // 完成,数据已更新!
   };
};
```

## 使用游标

使用get() 要求知道想要检索哪一个键.<br>
如果想要遍历对象存储空间中的所有值,那么可以使用游标.<br>
看起来会像下面这样:

```js
var objectStore = db.transaction("customers").objectStore("customers");

objectStore.openCursor().onsuccess = function(event) {
  var cursor = event.target.result;
  if (cursor) {
    alert("Name for SSN " + cursor.key + " is " + cursor.value.name);
    cursor.continue();
  }
else{
    alert("No more entries!");
  }
};
```

openCursor() 函数需要几个参数.<br>
首先,可以使用一个keyrange对象来限制被检索的项目的范围.<br>
第二,可以指定希望进行迭代的方向.<br>
在上面的示例中,在以升序迭代所有的对象.<br>
游标成功的回调有点特别.<br>
游标对象本身是请求的result(上面使用的是简写形式,所以是event.target.result).<br>
然后实际的key和value可以根据游标对象的key和value属性被找到.<br>
如果想要保持继续前行,那么必须调用游标上的continue() .<br>
当已经到达数据的末尾时(或者没有匹配openCursor() 请求的条目)仍然会得到一个成功回调,但是result属性是undefined.<br>


使用游标的一种常见模式是提取出在一个对象存储空间中的所有对象然后把它们添加到一个数组中,像这样:

```js
var customers = [];

objectStore.openCursor().onsuccess = function(event) {
  var cursor = event.target.result;
  if (cursor) {
    customers.push(cursor.value);
    cursor.continue();
  }
else{
    alert("以获取所有客户信息: " + customers);
  }
};
```

## 使用索引

使用SSN作为键来存储客户数据是合理的,因为SSN唯一地标识了一个个体(对隐私来说这是否是一个好的想法是另外一个话题,不在本文的讨论范围内).<br>
如果想要通过姓名来查找一个客户,那么,将需要在数据库中迭代所有的SSN直到找到正确的那个.<br>
以这种方式来查找将会非常的慢,相反可以使用索引.<br>


```js
// 首先,确定已经在request.onupgradeneeded中创建了索引:
// objectStore.createIndex("name", "name");
// 否则将得到DOMException.

var index = objectStore.index("name");

index.get("Donna").onsuccess = function(event) {
  alert("Donna*s SSN is " + event.target.result.ssn);
};
```

*name* 游标不是唯一的,因此name被设成 "Donna" 的记录可能不止一条.<br>
在这种情况下,总是得到键值最小的那个.<br>


如果需要访问带有给定name的所有的记录可以使用一个游标.<br>
可以在索引上打开两个不同类型的游标.<br>
一个常规游标映射索引属性到对象存储空间中的对象.<br>
一个键索引映射索引属性到用来存储对象存储空间中的对象的键.<br>
不同之处被展示如下:

```js
index.openCursor().onsuccess = function(event) {
  var cursor = event.target.result;
  if (cursor) {
    // cursor.key是一个name, 就像 "Bill", 然后cursor.value是整个对象.
    alert("Name: " + cursor.key + ", SSN: " + cursor.value.ssn + ", email: " + cursor.value.email);
    cursor.continue();
  }
};

index.openKeyCursor().onsuccess = function(event) {
  var cursor = event.target.result;
  if (cursor) {
    // cursor.key是一个name, 就像 "Bill", 然后cursor.value是那个SSN.
    // 没有办法可以得到存储对象的其余部分.
    alert("Name: " + cursor.key + ", SSN: " + cursor.value);
    cursor.continue();
  }
};
```

## 指定游标的范围和方向

如果想要限定在游标中看到的值的范围,可以使用一个keyrange对象然后把它作为第一个参数传给openCursor() 或是openKeyCursor().<br>
可以构造一个只允许一个单一key的keyrange,或者一个具有下限或上限,或者一个既有上限也有下限.<br>
边界可以是 *闭合的*(也就是说keyrange包含给定的值)或者是* 开放的*(也就是说keyrange不包括给定的值).<br>
这里是它如何工作的:

```js
// 仅匹配 "Donna"
var singleKeyRange = IDBKeyRange.only("Donna");

// 匹配所有超过 *Bill* 的,包括*Bill*
var lowerBoundKeyRange = IDBKeyRange.lowerBound("Bill");

// 匹配所有超过 *Bill* 的,但不包括*Bill*
var lowerBoundOpenKeyRange = IDBKeyRange.lowerBound("Bill", true);

// 匹配所有不超过 *Donna* 的,但不包括*Donna*
var upperBoundOpenKeyRange = IDBKeyRange.upperBound("Donna", true);

// 匹配所有在 *Bill*和*Donna* 之间的,但不包括*Donna*
var boundKeyRange = IDBKeyRange.bound("Bill", "Donna", false, true);

// 使用其中的一个键范围,把它作为openCursor()/openKeyCursor的第一个参数
index.openCursor(boundKeyRange).onsuccess = function(event) {
  var cursor = event.target.result;
  if (cursor) {
    // 当匹配时进行一些操作
    cursor.continue();
  }
};
```

有时候可能想要以倒序而不是正序(所有游标的默认顺序)来遍历.<br>
切换方向是通过传递prev到openCursor() 方法来实现的:

```js
objectStore.openCursor(boundKeyRange, "prev").onsuccess = function(event) {
  var cursor = event.target.result;
  if (cursor) {
    // 进行一些操作
    cursor.continue();
  }
};
```

如果只是想改变遍历的方向,而不想对结果进行筛选,只需要给第一个参数传入null.<br>


```js
objectStore.openCursor(null, "prev").onsuccess = function(event) {
  var cursor = event.target.result;
  if (cursor) {
    // Do something with the entries.
    cursor.continue();
  }
};
```

因为 *name* 索引不是唯一的,那就有可能存在具有相同name的多条记录.<br>
要注意的是这种情况不可能发生在对象存储空间上,因为键必须永远是唯一的.<br>
如果想要在游标在索引迭代过程中过滤出重复的,可以传递nextunique(或prevunique如果正在向后寻找)作为方向参数.<br>
当nextunique或是prevunique被使用时,被返回的那个总是键最小的记录.

```js
index.openKeyCursor(null, IDBCursor.nextunique).onsuccess = function(event) {
  var cursor = event.target.result;
  if (cursor) {
    // Do something with the entries.
    cursor.continue();
  }
};
```

## 

```js

```

## 

```js

```

## 

```js

```

## 

```js

```


# IDBRequest对象

为了获取数据库的访问权限,需要在window对象的indexedDB属性上调用open() 方法.<br>
该方法返回一个IDBRequest对象,异步操作通过在IDBRequest对象上触发事件来和调用程序进行通信.

## 连接数据库

### IDBFactory

提供数据库访问.<br>
这是全局对象indexedDB实现的接口,因此是API的入口.

### IDBOpenDBRequest

表示一个打开数据库的请求.

### IDBDatabase

表示一个数据库连接.<br>
这是在数据库中获取事务的唯一方式.

## 接收和修改数据

### IDBTransaction

表示一个事务,在数据库上创建一个事务,指定作用域(例如要访问的存储对象),并确定所需的访问类型(只读或读写).

### IDBRequest

处理数据库请求并提供对结果访问的通用接口.

### IDBObjectStore

表示允许访问通过主键查找的IndexedDB数据库中的一组数据的对象存储区.

### IDBIndex

也是为了允许访问IndexedDB数据库中的数据子集,但使用索引来检索记录而不是主键.<br>
这有时比使用IDBObjectStore更快.

### IDBCursor

迭代对象存储和索引.

### IDBCursorWithValue

迭代对象存储和索引并返回游标的当前值.

### IDBKeyRange

定义可用于从特定范围内的数据库检索数据的键范围.

### IDBLocaleAwareKeyRange

定义一个键范围,可用于从特定范围内的数据库中检索数据,并根据为特定索引指定的语言环境的规则进行排序(详见createIndex() 的参数).<br>
这个接口不再是2.0规范的一部分.


## 自定义事件接口

### IDBVersionChangeEvent

作为IDBOpenDBRequest.onupgradeneeded事件的处理程序的结果,IDBVersionChangeEvent接口表示数据库的版本已经发生了改变.

### 



### 



### 



### 



### 



### 



### 



### 



### 



### 



### 



### 



### 



### 



### 



### 





# 参考资料

> [MDN|IndexedDB](https://developer.mozilla.org/zh-CN/docs/Web/API/IndexedDB_API)

> [MDN|使用IndexedDB](https://developer.mozilla.org/zh-CN/docs/Web/API/IndexedDB_API/Using_IndexedDB)

> [维基百科|数据库事务](https://zh.wikipedia.org/wiki/%E6%95%B0%E6%8D%AE%E5%BA%93%E4%BA%8B%E5%8A%A1)

> [MDN|基本概念](https://developer.mozilla.org/zh-CN/docs/Web/API/IndexedDB_API/Basic_Concepts_Behind_IndexedDB)