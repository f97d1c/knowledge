<!-- TOC -->

- [词法结构](#词法结构)
  - [字符集](#字符集)
  - [注释](#注释)
  - [直接量](#直接量)
  - [标识符和保留字](#标识符和保留字)
  - [可选的分号](#可选的分号)
- [类型、值和变量](#类型值和变量)
  - [数组](#数组)
    - [多对象整合](#多对象整合)
    - [稀疏数组](#稀疏数组)
    - [in 操作符](#in-操作符)
    - [length属性](#length属性)
      - [设置length属性](#设置length属性)
    - [删除数组元素](#删除数组元素)
      - [delete操作符](#delete操作符)
    - [遍历](#遍历)
      - [排除非法元素](#排除非法元素)
    - [筛选元素(filter)](#筛选元素filter)
      - [根据键/值过滤对象](#根据键值过滤对象)
    - [去重](#去重)
    - [连接字符串(join)](#连接字符串join)
    - [逆序/反转排列(reverse)](#逆序反转排列reverse)
    - [排序操作(sort)](#排序操作sort)
      - [比较函数](#比较函数)
    - [cocat()](#cocat)
    - [slice()](#slice)
      - [返回最后一个元素](#返回最后一个元素)
    - [splice()](#splice)
    - [pop()和push()](#pop和push)
    - [shift()和unshift()](#shift和unshift)
    - [toString()和toLocaleString()](#tostring和tolocalestring)
    - [ECMAScript5中新增数组方法](#ecmascript5中新增数组方法)
      - [forEach()](#foreach)
        - [关于await](#关于await)
      - [map()](#map)
      - [every()和some()](#every和some)
      - [reduce()和reduceRight()](#reduce和reduceright)
      - [indexOf()和lastIndexOf()](#indexof和lastindexof)
    - [数组类型](#数组类型)
    - [类数组对象](#类数组对象)
    - [作为数组的字符串](#作为数组的字符串)
    - [数组转对象(Object.fromEntries(arr))](#数组转对象objectfromentriesarr)
  - [数字](#数字)
    - [算术运算符](#算术运算符)
    - [日期和时间](#日期和时间)
    - [0 & false](#0--false)
  - [字符串](#字符串)
    - [匹配模式](#匹配模式)
    - [字符串转正则表达式](#字符串转正则表达式)
    - [字符串转数字(parseInt)](#字符串转数字parseint)
    - [Json字符串格式化](#json字符串格式化)
    - [哈希码](#哈希码)
    - [长度补全](#长度补全)
    - [全角/半角](#全角半角)
      - [半角](#半角)
      - [全角](#全角)
      - [二者的区别](#二者的区别)
      - [全角/半角转换](#全角半角转换)
  - [对象](#对象)
    - [默认值](#默认值)
    - [URL](#url)
      - [获取链接参数(searchParams)](#获取链接参数searchparams)
        - [特殊字符缺失](#特殊字符缺失)
  - [布尔值](#布尔值)
  - [null和undefined](#null和undefined)
  - [全局对象](#全局对象)
  - [包装对象](#包装对象)
  - [不可变的原始值和可变的对象引用](#不可变的原始值和可变的对象引用)
  - [类型转换](#类型转换)
  - [变量声明](#变量声明)
  - [变量作用域](#变量作用域)
- [表达式和运算符](#表达式和运算符)
  - [原始表达式](#原始表达式)
  - [对象和数组的初始化表达式](#对象和数组的初始化表达式)
  - [函数定义表达式](#函数定义表达式)
  - [属性访问表达式](#属性访问表达式)
  - [调用表达式](#调用表达式)
  - [对象创建表达式](#对象创建表达式)
  - [运算符概述](#运算符概述)
  - [算术表达式](#算术表达式)
  - [关系表达式](#关系表达式)
  - [逻辑表达式](#逻辑表达式)
  - [赋值表达式](#赋值表达式)
  - [表达式计算](#表达式计算)
  - [其他运算符](#其他运算符)
- [语句](#语句)
  - [表达式语句](#表达式语句)
  - [复合语句和空语句](#复合语句和空语句)
  - [声明语句](#声明语句)
  - [条件语句](#条件语句)
    - [if](#if)
    - [switch](#switch)
    - [多条件语句](#多条件语句)
  - [循环](#循环)
    - [while](#while)
    - [for](#for)
      - [for/in](#forin)
  - [跳转](#跳转)
    - [break](#break)
    - [continue](#continue)
    - [return](#return)
    - [try/catch/finally](#trycatchfinally)
  - [其他语句类型](#其他语句类型)
- [对象](#对象-1)
  - [this](#this)
    - [this is undefined](#this-is-undefined)
  - [创建对象](#创建对象)
    - [动态创建对象](#动态创建对象)
  - [属性的查询和设置](#属性的查询和设置)
    - [setAttribute](#setattribute)
  - [删除属性](#删除属性)
  - [检测属性](#检测属性)
  - [枚举属性](#枚举属性)
  - [属性getter和setter](#属性getter和setter)
  - [属性的特性](#属性的特性)
  - [对象的三个属性](#对象的三个属性)
  - [序列化对象](#序列化对象)
  - [对象方法](#对象方法)
    - [属性值替换(assign)](#属性值替换assign)
- [函数](#函数)
  - [函数定义](#函数定义)
  - [函数调用](#函数调用)
  - [函数的实参和形参](#函数的实参和形参)
  - [作为值的函数](#作为值的函数)
  - [作为命名空间的函数](#作为命名空间的函数)
  - [闭包](#闭包)
  - [函数属性、方法和构造函数](#函数属性方法和构造函数)
  - [函数式编程](#函数式编程)
- [类和模块](#类和模块)
  - [类和原型](#类和原型)
  - [类和构造函数](#类和构造函数)
  - [JavaScript中Java式的类继承](#javascript中java式的类继承)
  - [类的扩充](#类的扩充)
  - [类和类型](#类和类型)
  - [JavaScript中的面向对象技术](#javascript中的面向对象技术)
  - [子类](#子类)
  - [ECMAScript5中的类](#ecmascript5中的类)
  - [模块](#模块)
- [正则表达式的模式匹配](#正则表达式的模式匹配)
  - [正则表达式的定义](#正则表达式的定义)
  - [用于模式匹配的String方法](#用于模式匹配的string方法)
  - [RegExp对象](#regexp对象)
- [JavaScript的子集和扩展](#javascript的子集和扩展)
  - [JavaScript的子集](#javascript的子集)
  - [常量和局部变量](#常量和局部变量)
  - [解构赋值](#解构赋值)
  - [迭代](#迭代)
  - [函数简写](#函数简写)
  - [多catch从句](#多catch从句)
  - [E4X:ECMAScriptforXML](#e4xecmascriptforxml)
- [服务器端JavaScript](#服务器端javascript)
  - [用Rhino脚本化Java](#用rhino脚本化java)
  - [用Node实现异步I/O](#用node实现异步io)
- [客户端JavaScript](#客户端javascript)
  - [Web浏览器中的JavaScript](#web浏览器中的javascript)
    - [客户端JavaScript](#客户端javascript-1)
    - [在HTML里嵌入JavaScript](#在html里嵌入javascript)
    - [JavaScript程序的执行](#javascript程序的执行)
    - [兼容性和互用性](#兼容性和互用性)
    - [可访问性](#可访问性)
    - [安全性](#安全性)
    - [客户端框架](#客户端框架)
  - [Window对象](#window对象)
    - [计时器](#计时器)
    - [浏览器定位和导航](#浏览器定位和导航)
    - [浏览历史](#浏览历史)
    - [浏览器和屏幕信息](#浏览器和屏幕信息)
    - [对话框](#对话框)
    - [错误处理](#错误处理)
    - [作为Window对象属性的文档元素](#作为window对象属性的文档元素)
    - [多窗口和窗体](#多窗口和窗体)
    - [把字符串当函数执行的方法](#把字符串当函数执行的方法)
  - [脚本化文档](#脚本化文档)
    - [DOM概览](#dom概览)
    - [选取文档元素](#选取文档元素)
    - [文档结构和遍历](#文档结构和遍历)
    - [属性](#属性)
    - [元素的内容](#元素的内容)
    - [创建、插入和删除节点](#创建插入和删除节点)
      - [innerHTML](#innerhtml)
      - [插入子节点(appendChild)](#插入子节点appendchild)
        - [加载script标签](#加载script标签)
    - [例子：生成目录表](#例子生成目录表)
    - [文档和元素的几何形状和滚动](#文档和元素的几何形状和滚动)
    - [HTML表单](#html表单)
    - [其他文档特性](#其他文档特性)
  - [脚本化CSS](#脚本化css)
    - [CSS概览](#css概览)
    - [重要的CSS属性](#重要的css属性)
    - [脚本化内联样式](#脚本化内联样式)
    - [查询计算出的样式](#查询计算出的样式)
    - [脚本化CSS类](#脚本化css类)
    - [脚本化样式表](#脚本化样式表)
  - [事件处理](#事件处理)
    - [事件类型](#事件类型)
    - [注册事件处理程序](#注册事件处理程序)
    - [事件处理程序的调用](#事件处理程序的调用)
    - [文档加载事件](#文档加载事件)
    - [鼠标事件](#鼠标事件)
    - [鼠标滚轮事件](#鼠标滚轮事件)
    - [拖放事件](#拖放事件)
    - [文本事件](#文本事件)
    - [键盘事件](#键盘事件)
  - [脚本化HTTP](#脚本化http)
    - [使用XMLHttpRequest](#使用xmlhttprequest)
    - [借助发送HTTP请求：JSONP](#借助发送http请求jsonp)
    - [基于服务器端推送事件的Comet技术](#基于服务器端推送事件的comet技术)
  - [jQuery类库](#jquery类库)
    - [jQuery基础](#jquery基础)
    - [jQuery的getter和setter](#jquery的getter和setter)
    - [修改文档结构](#修改文档结构)
    - [使用jQuery处理事件](#使用jquery处理事件)
    - [动画效果](#动画效果)
    - [jQuery中的Ajax](#jquery中的ajax)
    - [工具函数](#工具函数)
    - [jQuery选择器和选取方法](#jquery选择器和选取方法)
      - [基本选择器](#基本选择器)
      - [层次选择器](#层次选择器)
      - [过滤选择器](#过滤选择器)
      - [内容过滤选择器](#内容过滤选择器)
      - [可见性过滤选择器](#可见性过滤选择器)
      - [属性过滤选择器](#属性过滤选择器)
      - [状态过滤选择器](#状态过滤选择器)
      - [表单选择器](#表单选择器)
    - [jQuery的插件扩展](#jquery的插件扩展)
    - [jQueryUI类库](#jqueryui类库)
  - [客户端存储](#客户端存储)
    - [localStorage和sessionStorage](#localstorage和sessionstorage)
    - [cookie](#cookie)
    - [利用IEuserData持久化数据](#利用ieuserdata持久化数据)
    - [应用程序存储和离线Web应用](#应用程序存储和离线web应用)
  - [多媒体和图形编程](#多媒体和图形编程)
    - [脚本化图片](#脚本化图片)
    - [脚本化音频和视频](#脚本化音频和视频)
    - [SVG：可伸缩的矢量图形](#svg可伸缩的矢量图形)
    - [中的图形](#中的图形)
  - [HTML5API](#html5api)
    - [地理位置](#地理位置)
    - [历史记录管理](#历史记录管理)
    - [跨域消息传递](#跨域消息传递)
      - [关于Fetch+Rails跨域请求实例](#关于fetchrails跨域请求实例)
    - [WebWorker](#webworker)
    - [类型化数组和ArrayBuffer](#类型化数组和arraybuffer)
    - [Blob](#blob)
      - [实际应用](#实际应用)
    - [文件系统API](#文件系统api)
    - [客户端数据库](#客户端数据库)
    - [Web套接字](#web套接字)
- [常见问题](#常见问题)
  - [$.ajax is not a function](#ajax-is-not-a-function)
- [参考资料](#参考资料)

<!-- /TOC -->

# 词法结构 
## 字符集 
## 注释 
## 直接量 
## 标识符和保留字 
## 可选的分号 

# 类型、值和变量 

> Javascript的数据类型分为两类: 原始类型和对象类型.<br>
原始类型包括数字、字符串和布尔值.<br>
<br>
Javascript中有两个特殊的原始值:null(空)和undefined(未定义),<br>他们不是数字/字符串/布尔值,他们通常代表了各自特殊类型的唯一的成员.


Javascript中除了数字,字符串,布尔值,null和undefined之外的就是对象了.<br>
对象是属性的集合,每个属性都由"名/值对"(值可以是数字,字符串,布尔值,null和undefined以及对象)构成.<br>
其中有一个特殊的对象-全局对象.

Javascipt的类型可以分为原始类型和对象类型,<br>
也可以分为拥有方法的类型和不能拥有方法的类型,<br>
同样可以分为可变类型(对象和数组)和不可变类型.

JavaScript变量是无类型的,变量可以被赋予任何类型的值,<br>同样一个变量也可以重新赋予不同类型的值.<br>使用var关键字来声明变量.
## 数组

数组元素可以是任意类型,同一数组中的不同元素也可以是不同类型.

### 多对象整合

```js
Object.assign({}, ...[{test: 'value-1'}, {name: 'value-2'}])
// {test: 'value-1', name: 'value-2'}
```

### 稀疏数组

> 从0开始的索引不连续数组.

### in 操作符

> 用于检测索引在数组中是否存在元素.

```js
var a1 = [,,,] // 数组是[undefined, undefined, undefined]
var a2 = new Array(3) // 数组没有任何元素
0 in a1 // => true
0 in a2 // => false
```

### length属性

> 对于稠密数组来说,length属性值代表数组中元素个数即数组长度.

#### 设置length属性

> 手动设置length属性为一个小于当前长度的非负整数n时,<br>当前数组中索引值 *大于或等于* n的元素将从中删除.

```js
a = [1, 2, 3, 4, 5]
a.length = 3 // a为[1, 2, 3]
```

### 删除数组元素

#### delete操作符

> delete 操作符不会修改数组的length属性,<br>也不会将高索引处移下来补充已删除属性留下的空白,<br>元素删除后数组会变为稀疏数组.

```js
a = [1, 2, 3]
delete a[1]
1 in a[1] // => false
a.length // => 3
```


### 遍历

```js
array = [1, 3, 5, '7']
Object.keys(array) // => ["0", "1", "2", "3"] 返回数组元素的索引
array[100] = 100
Object.keys(array) // => ["0", "1", "2", "3", "100"]
```

#### 排除非法元素

```js
for( i=0; i < a.length; i++) {
  // 跳过null, undefined和不存在的元素
  if (!a[i]) continue;

  // 跳过undefined和不存在元素
  if ( a[1 === undefined]) continue;

  // 跳过不存在的元素
  if (!(i in a)) continue;
}

```

### 筛选元素(filter)

> 指定函数的返回值必须为布尔类型,<br>从而由filter()判断返回结果中是否应包含该元素.

```js
data = {"名称": "2013年","数量": 185,"平均|累计净值": 1.6245945945945948,"平均|日增长率": 0.09918918918918918,"平均|单位净值": 1.6245945945945948,"最低|累计净值": 1.344,"最高|累计净值": 1.768}
Object.keys(data).filter(str => { return str.match(/平均*/)})
Object.keys(data).filter(str => str.match(/平均*/))

// => ["平均|累计净值","平均|日增长率","平均|单位净值"]
```

#### 根据键/值过滤对象

```js
Object.fromEntries(Object.entries(rep).filter(([key, value]) => !['子仓库'].includes(key)))
```

### 去重

```js
tmp = [1,2,1,1,2,1,1,2,3,4]
[...new Set(tmp)]
// => [1, 2, 3, 4]
```

### 连接字符串(join)

> 将数组中所有元素转化为字符串连接在一起.

```js
i = 3
a = [1, 2, '3', i+1]; // => [1, 2, "3", 4]
a.join();             // => "1,2,3,4"
a.join('');           // => "1234"
a.join(' ');          // => "1 2 3 4"
a.join('-');          // => "1-2-3-4"

a = [1, 2, '3', [4 ,5]]; // => [1, 2, "3", [4, 5]]
a.join('-');             // => "1-2-3-4,5"
```

### 逆序/反转排列(reverse)

> 数组元素进行颠倒顺序,返回逆序数组.

```js
a = [1, 2, '3', 4]; // => [1, 2, "3", 4]
a.reverse();        // => [4, "3", 2, 1]
a;                  // => [4, "3", 2, 1]
```

### 排序操作(sort)

> 返回数组排序后的数组,不带参数时默认以字母排序.

```js
a = ['c', 'b', 'r', 't', 'w', 'a', 'z']; // => ["c", "b", "r", "t", "w", "a", "z"]
a.sort();                                // => ["a", "b", "c", "r", "t", "w", "z"]
a;                                       // => ["a", "b", "c", "r", "t", "w", "z"]
```

#### 比较函数

> 函数返回值决定两参数在数组中的先后顺序,<br>如果第一个参数应该在前,函数返回值应小于0,<br>若第一个参数应在后,返回值应大于0.

```js
a = [72, 41, 57, 39, 32];           // => [72, 41, 57, 39, 32]
a.sort();                           // => [32, 39, 41, 57, 72]
a.sort(function(a, b){return a-b}); // => [32, 39, 41, 57, 72]
a.sort(function(a, b){return b-a}); // => [72, 57, 41, 39, 32]
```

### cocat()

> 返回包含调用方法数组以及参数在内的 *新数组*.

```js
a = [1, 2, 3, 4];         // => [1, 2, 3, 4]
a.concat(5, 6);           // => [1, 2, 3, 4, 5, 6]
a.concat([5, 6]);         // => [1, 2, 3, 4, 5, 6]
a.concat([5, 6], [7, 8]); // => [1, 2, 3, 4, 5, 6, 7, 8]
a.concat(5, [6, [7, 8]]); // => [1, 2, 3, 4, 5, 6, [7, 8]]
a;                        // => [1, 2, 3, 4]
```

### slice()

> 返回指定数组的一个片段或子数组.<br>参数1应小于参数2.

```js
a = [1, 2, 3, 4]; // => [1, 2, 3, 4]
a.slice(0, 3);    // => [1, 2, 3]
a.slice(1, -1);   // => [2, 3]
a.slice(-1, 1);   // => []
a.slice(-3, -2);  // => [2]
a;                // => [1, 2, 3, 4]
```

#### 返回最后一个元素

```js
array.slice(-1)[0]
```
### splice()

> 数组中插入或删除元素的通用方法,<br>不同与concat()和slice(),splice()会 *修改调用数组* .<br>并返回删除的元素数组.

```js
// splice() 前两个参数制定需要删除的参数

a = [1, 2, 3, 4]; // => [1, 2, 3, 4]
// 如果省略第二个参数,将删除从起始点到结尾的所有元素
a.splice(2);      // => [3, 4]
a;                // => [1, 2]

a = [1, 2, 3, 4]; // => [1, 2, 3, 4]
a.splice(1, 2);   // => [2, 3]
a;                // => [1, 4]

a = [1, 2, 3, 4]; // => [1, 2, 3, 4]
a.splice(1, 1);   // => [2]
a;                // => [1, 3, 4]

// 前两个参数后任意个数参数指定了需要插入的元素,插入位置从地一个参数指定的位置开始插入.

a = [1, 2, 3, 4];         // => [1, 2, 3, 4]
a.splice(2, 0, 'a', 'b'); // => []
a;                        // => [1, 2, "a", "b", 3, 4]

a = [1, 2, 3, 4];         // => [1, 2, 3, 4]
a.splice(2, 2, [1, 2],3); // => [3, 4]
a;                        // => [1, 2, [1, 2], 3]
```

### pop()和push() 

> 从数组 *尾部* 一次减少一个元素,并返回删除的元素.

```js
a = [1, 3, 5, 8, 9]; // => [1, 3, 5, 8, 9]
a.pop();             // => 9 返回删除元素
a;                   // => [1, 3, 5, 8]

// 与此相对的是push()方法
a.push(10) // => 5 返回数组长度, 数组为 [1, 3, 5, 8, 10]
```

### shift()和unshift()

```js
a = [1, 3, 5, 8, 9]; // => [1, 3, 5, 8, 9]
a.shift();           // => 1 返回删除元素
a;                   // => [3, 5, 8, 9]

// 与此相对的是unshift() 方法
a.unshift(10);       // => 5 返回数组长度, 
a;                   // => [10, 3, 5, 8, 9]
```

### toString()和toLocaleString()

> 不带任何参数调用toString()方法和调用join()方法的返回值一致.

```js
a = [1, 2, 3, 4]; // => [1, 2, 3, 4]
a.toString();     // => "1,2,3,4"
a;                // => [1, 2, 3, 4]
a.toString('-');  // => "1,2,3,4"
```

### ECMAScript5中新增数组方法

共同特点是这些方法的第一个参数接收一个函数并对每一个元素调用一次该函数.

#### forEach()

> 从头到尾便利数组,为每个元素执行指定函数.<br>该方法无法提前终止遍历,即break语句在循环中不好使,需抛出异常来提前终止.

```js
a = [1, 2, 3, 4];                         // => [1, 2, 3, 4]
sum = 0;
a.forEach(function(value) {sum += value}); // => undefined
sum;                                      // => 10
a;                                        // => [1, 2, 3, 4]
a.forEach(function(value, index, array) {array[index] = value+1}); // => undefined
a;                                        // => [2, 3, 4, 5]
```
##### 关于await

forEach循环中无法使用await,可以使用for-of循环代替.

#### map()

> 将每个元素传递给指定函数,并返回函数的返回值.<br>这就要求指定函数 *必须存在返回值* . 

```js
a = [1, 2, 3, 4];                              // => [1, 2, 3, 4]
a.map(function(value) {return value * value}); // => [1, 4, 9, 16]
a;                                             // => [1, 2, 3, 4]
```


#### every()和some()

> 对指定函数进行判定,返回布尔值.<br>一旦确定返回值将停止遍历,类似逻辑&&运算.

```js
a = [1, 2, 3, 4];                             // => [1, 2, 3, 4]
a.every(function(value) {return value < 10}); // => true
a.every(function(value) {return value > 10}); // => false
```

#### reduce()和reduceRight()

> 

```js


```

#### indexOf()和lastIndexOf()

> 搜索整个数组中符合给定值的元素,并返回找到的 *第一个元素* 的索引值,<br>如果没有找到则返回-1.

```js
a = [10, 32, 13, 54, 13, 58]; // => [10, 32, 13, 54, 13, 58]
a.indexOf(9);                 // => -1
a.indexOf(13);                // => 2
a.lastIndexOf(13);            // => 4
```

### 数组类型 
### 类数组对象 
### 作为数组的字符串 

### 数组转对象(Object.fromEntries(arr))

```js
const arr = [ ['0', 'a'], ['1', 'b'], ['2', 'c'] ];
const obj = Object.fromEntries(arr);
console.log(obj); // { 0: "a", 1: "b", 2: "c" }
```

## 数字

JavaScript不区分整数值和浮点数值.JavaScript中所有数值据用浮点数值表示.

### 算术运算符

除了基本运算符外,JavaScript还支持更加复杂的算数运算,<br>通过Math对象的属性定义的函数和常量来实现.

函数|结果|含义|
-|-|-
Math.pow(2, 53)|9007199254740992|2的53次幂
Math.round(.6)|1.0|四舍五入
Math.ceil(.6)|1.0|向上求整
Math.floor(.6)|0.0|向下求整
Math.abs(-5)|5|求绝对值
Math.max(1, 6, -1)|6|求最大值
Math.min(1, 6, -1)|-1|求最小值
Math.random()||生成一个大于等于0小于1.0的伪随机数.
Math.PI()|3.141592653589793|圆周率
Math.E()||自然对数的底数
Math.sqrt(3)|1.7320508075688772|3的平方根
Math.pow(3, 1/3)|1.4422495703074083|3的立方根
Math.sin(0)<br>Math.cos(0)<br>Math.atan(0)||三角函数
Math.log(10)|2.302585092994046|10的自然对数
Math.log(100)/Math.LN10||以10为底100的对数
Math.log(512)/Math.LN2||以2为底512的对数
Math.exp(3)||e的三次幂

### 日期和时间

函数|结果|含义|
-|-|-
var later = new Date(2019, 06, 07, 09, 50)|Sun Jul 07 2019 09:50:00 GMT+0800 (China Standard Time)|生成具体时间
later.getFullYear()|2019|
later.getMonth()|6|月份从0开始计算
later.getDate()|7|天数从1开始计算
later.getHours()|9|
later.getUTCHours()|1|使用UTC表示小时

### 0 & false

Js里面0和false相等:

```js
0||12 // => 12 
0 == false // => true
```

## 字符串

> 字符串可以看作是字符组成的数组,在Javascript中字符串是不可变的,<br>
可以访问字符串任意位置的文本,<br>
但JavaScript并未提供修改已知字符串的文本内容的方法.

函数|结果|含义|
-|-|-
var s = 'hello, world'|"hello, world"|定义字符串
s.charAt(0)|h|第一个字符
s.charAt(s.length - 1)|d|最后一个字符
s.substring(1, 4)<br>(不包括4)|ell|获取第2-4个字符
s.slice(1, 4)<br>(不包括4)|ell|获取第2-4个字符
s.slice(-3)|rld|最后三个字符
s.indexOf('l')|2|字符l首次出现位置的下标
s.indexOf('l', 4)|10|字符l在下标4后首次出现位置的下标
s.lastIndexOf('l')|10|字符l最后出现位置的下标
s.split(',')|["hello", " world"]|根据','分割成数组
s.replace('h', 'H')<sup>*</sup>|"Hello, world"|全文字符替换
s.toUpperCase()<sup>*</sup>|"HELLO, WORLD"|全文字符转大写

*: JavaScript中字符串是固定不变的,这些方法返回新字符串,原字符串不变.

### 匹配模式

> JavaScript定义了RegExp()函数,用来创建表示文本匹配模式的对象,<br>
这些模式成为: 正则表达式,JavaScript采用Perl中的正则表达式语法.<br>
String和PegExp对象均定义了利用正则表达式进行模式匹配和查找与替换的函数.

函数|结果|含义|
-|-|-
var text = 'testing: 1, 2, 3'|"testing: 1, 2, 3"|
text.search(/\d+/g)|9|首次匹配成功的位置下标
text.match(/\d+/g)|["1", "2", "3"]|所有匹配组成的数组
text.replace(/\d+/g, '#')|"testing: #, #, #"|
text.split(/\D+/)|["", "1", "2", "3"]|用非数字字符串截取字符串

### 字符串转正则表达式

```js
new RegExp("a|b", "g") //=> /a|b/g
```

### 字符串转数字(parseInt)

```js
parseInt('-1')
// => -1
```

### Json字符串格式化

将已解析的字符串转换成一种不错的格式:

```js
JSON.stringify(input, undefined, 2)
```

例如:

```js
$("#api_result_table").append("<tr class='auto_create'><td class='text-right'>"+key+"</td><td class='text-left'><pre><code>"+JSON.stringify(data[key], undefined, 2)+"</code></pre></td></tr>");
```

### 哈希码

```js
String.prototype.hashCode = function() {
  var hash = 0, i, chr;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

"hashstring".hashCode()
// => 1581817503
```

### 长度补全

```js
'str'.padStart(7 , '-')
// => "----str"

'str'.padEnd(7 , '-')
// => "str----"
```

### 全角/半角

#### 半角

> 是指一个字符占用一个标准的字符位置.<br>
半角占一个字节.<br>
半角就是 ASCII 方式的字符,在没有汉字输入法起作用的时候,输入的字母、数字和字符都是半角的.

#### 全角

> 是一种电脑字符,一个全角字符占用两个标准字符(或两个半角字符)的位置.全角占两个字节.<br>
汉字字符和规定了全角的英文字符及国标GB2312-80中的图形符号和特殊字符都是全角字符.<br>
在全角中,字母和数字等与汉字一样占据着等宽的位置.

#### 二者的区别

> 全角和半角主要是针对标点符号来说的,全角标点占两个字节,半角占一个字节.<br>
不管是半角还是全角,汉字都要占两个字节.<br>
在汉字输入法中,输入的字母数字默认为半角,但是标点则是默认为全角.

```
１２３,；(全角,占两个字节)
123,;  (半角,占一个字节)
```

#### 全角/半角转换

```js
// 半角转化为全角
function ToSBC(string){
  var result = "";
  for(var i=0; i<string.length; i++){
    if(string.charCodeAt(i) == 32){
      result += String.fromCharCode(12288);
    }else if(string.charCodeAt(i) > 32 && string.charCodeAt(i) < 127){
      result += String.fromCharCode(string.charCodeAt(i) + 65248);
    }else{
      result += String.fromCharCode(string.charCodeAt(i));
    }
  }
  return result;
}

// 全角转化为半角
function ToDBC(string){
  var result = "";
  for(var i=0; i<string.length; i++){
    if(string.charCodeAt(i) == 12288){
      result += String.fromCharCode(32);
    }else if(string.charCodeAt(i) > 65280 && string.charCodeAt(i) < 65375){
      result += String.fromCharCode(string.charCodeAt(i) - 65248);
    }else{
      result += String.fromCharCode(string.charCodeAt(i));
    }
  }
  return result;
}

```

## 对象

### 默认值

```js
this.elasticsearch = new Elasticsearch(esConfig)
let defaultValue = new Elasticsearch()
let handler = {
  get: function(target, name) {
    return target.hasOwnProperty(name) ? target[name] : defaultValue;
  }
}
this.elasticsearch = new Proxy(this.elasticsearch, handler)

this.elasticsearch.notDefinedKey
// => Elasticsearch {indexName: undefined, indexType: undefined, indexMappings: undefined, aggs: undefined, aggsChart: {…}, …}
```

### URL

#### 获取链接参数(searchParams)

```js
url_string = "http://www.example.com/t.html?a=1&b=3&c=m2-m3-m4-m5"
url = new URL(url_string)
c = url.searchParams.get("c")
console.log(c)
```

##### 特殊字符缺失

searchParams或者URLSearchParams在解析含有 *+* 等特殊字符时会造成内容缺失:

```js
url_string = 'https://www.example.html?order=BF7pDfjS8QJoAEX+u+h882s//4yyXvFeQqxyH6+R0V6f8Dt6LA6sPcq2Os02hImpJBVCGwvpFmoa6ruUrhw=&appId=abbf2af34d6944b&invoiceType=1'
url = new URL(url_string)
order = url.searchParams.get("order")
// +号缺失
// => "BF7pDfjS8QJoAEX u h882s//4yyXvFeQqxyH6 R0V6f8Dt6LA6sPcq2Os02hImpJBVCGwvpFmoa6ruUrhw="
```

```js
function parse_query_string(query) {
  vars = query.split("&");
  query_string = {};
  for (i = 0; i < vars.length; i++) {
    pair = vars[i].split("=");
    key = decodeURIComponent(pair[0]);
    value = decodeURIComponent(pair[1]);
    // If first entry with this name
    if (typeof query_string[key] === "undefined") {
      query_string[key] = decodeURIComponent(value);
      // If second entry with this name
    } else if (typeof query_string[key] === "string") {
      arr = [query_string[key], decodeURIComponent(value)];
      query_string[key] = arr;
      // If third or later entry with this name
    } else {
      query_string[key].push(decodeURIComponent(value));
    }
  }
  return query_string;
}

params = parse_query_string(url_string.split('?')[1])
params.order
// => "BF7pDfjS8QJoAEX+u+h882s//4yyXvFeQqxyH6+R0V6f8Dt6LA6sPcq2Os02hImpJBVCGwvpFmoa6ruUrhw"
```



## 布尔值 

## null和undefined

null是JavaScript的关键字,它表示一个特殊值,常用来描述空值.

undefined用来定义更深层次的空值.<br>
它是变量的一种取值,表明变量没有初始化,<br>
如果要查询对象属性或数组元素的值时返回undefined则说明这个属性或元素不存在.<br>
如果函数没有返回任何值也会返回undefined.<br>
引用没有提供实参的函数形参的值也只得到undefined.

undefined是预定义的全局变量,它和null不一样,它不是关键字.

可以认为undefined是系统级的,出乎意料的或类似错误的值的空缺,<br>
而null是表示程序级的,正常或在意料之中的空缺.


## 全局对象 
## 包装对象 
## 不可变的原始值和可变的对象引用 
## 类型转换 
## 变量声明 
## 变量作用域 
# 表达式和运算符 
## 原始表达式 
## 对象和数组的初始化表达式 
## 函数定义表达式 
## 属性访问表达式 
## 调用表达式 
## 对象创建表达式 
## 运算符概述 
## 算术表达式 
## 关系表达式 
## 逻辑表达式 
## 赋值表达式 
## 表达式计算 
## 其他运算符 
# 语句 
## 表达式语句 
## 复合语句和空语句 
## 声明语句 
## 条件语句

### if

花括号并非必须,当逻辑较为复杂时,应考虑适当添加以便于理解.

```js
flag = false
a = null
if (flag)
  a = 'hello'
else if (!flag)
  a = 'word'
else
  a = 'hi'
a // => "word"
```

### switch

如果没有添加 break,那么代码将会从值所匹配的 case 语句开始运行,<br>
然后持续执行下一个 case 语句而不论值是否匹配.

```js
n = 1;
switch (n){
  case 1:
    a = 1;
    break;
  case 2:
    a = 1;
    break;
  default:
    a = -1;
    break;
}
a; // => 1
```

### 多条件语句

```js
let info = {}
switch (typeof(value)){
  case 'undefined':
  case 'boolean':
  case 'number':
  case 'bigint':
  case 'string':
  case 'symbol':
  case 'function':
    info.typeof = typeof(value)
    break;
  case 'object':
    if (Array.isArray(value)) info.typeof = 'array'
    break;
  default:
    break;
}
info.typeof ||= typeof(value)
```

## 循环

### while

> 条件为真的情况下重复执行.

```js
while (life < end) {
  love++;
}
```

### for

```js
for (变量定义; 判定是否继续执行条件; 进行下次循环前要做的操作) {
  // 执行代码块
}

sum = 0;
for (i=0,j=10; i < j; i++, j--) {
  sum += i*j
}
sum; // => 70

sum = 0;
i=0,j=10;
for (; i < j; i++, j--) {
  sum += i*j
}
sum; // => 70
```

#### for/in 

```js
array = [14, 52, 63, 47, 58, 86, 97]
for (index in array) {
  console.log(index)
}
/* =>
0
1
2
3
4
5
6
*/

for (index in array) {
  console.log(array[index])
}
/* =>
14
52
63
47
58
86
97
*/
```

## 跳转

### break

> 跳出循环或其他语句的结束.

### continue

> 跳过本次循环进入下次循环同其他语言的next.

### return

> 跳出函数体的执行,并提供调用的返回值.

### try/catch/finally 

```js
try {
  // 代码块
}
catch (e) {
  // 仅当try语句中抛出异常后才执行这里的代码
}
finally {
  // 不管是否抛出异常,这里的逻辑总会执行
}
```

 
## 其他语句类型  
# 对象 

## this

### this is undefined

> 例如通过eval执行的代码中包含有this,<br>
这是this的指向是不明确的,需要手动关联(bind)对象.

```js
// object.method => "Graphic.amcharts4.verticalGraph"
let deepCall = (object = window, array, params={}) => {
  if (!!!array[0]) return object(params)
  let newObject = object[array[0]]
  return deepCall(newObject, array.slice(1, 10000), params)
}

let res_ = deepCall(window, object.method.split('.'), params)
// => ...this is undefined...

eval(object.method)(params)
// => 返回结果同上
```

可以通过bind为方法指定this.

```js
let deepCall = (object = window, array, params={}, f_object) => {
  if (!!!array[0]) return object.bind(f_object)(params)
  let newObject = object[array[0]]
  return deepCall(newObject, array.slice(1, 10000), params, object)
}

let res_ = deepCall(window, object.method.split('.'), params)
```

详细信息见[stackoverflow](https://stackoverflow.com/questions/4011793/this-is-undefined-in-javascript-class-methods).

## 创建对象 

### 动态创建对象

```html
<a onclick='change_special_info("hide");'>增值税普通发票</a>
<a onclick='change_special_info("show");'>增值税专用发票</a>
```

```js
function change_special_info(state){
  var change_display = new Function("$('.special_info')."+state+"();");
  change_display();
}
```

## 属性的查询和设置 

### setAttribute

```js
(function (){
  initCssAssets();
})();

function initCssAssets (){
  var className = "chart_js";
  // if (!document.getElementsByClassName(className).length > 0) createElement();
  if (!document.getElementsByClassName(className).length > 0){
    createElement();
  }
}

function createElement (){
  var head  = document.getElementsByTagName('head')[0];
  var obj  = document.createElement('script');
  obj.setAttribute('class', 'chart_js');
  obj.setAttribute('language', 'javascript');
  obj.setAttribute('type', 'text/javascript');    
  obj.setAttribute('src', 'http://123.57.155.17:3111/assets/dashboard/chart_js/chart_js.debug-f945f1c30287c57b175f9ae5fabd915e93e4a76b2d66351002996c4c7f1e860d.js');
  obj.setAttribute('async', '');
  head.appendChild(obj);
}
```

## 删除属性 
## 检测属性 
## 枚举属性 
## 属性getter和setter 
## 属性的特性 
## 对象的三个属性 
## 序列化对象 
## 对象方法

### 属性值替换(assign)

```js
target = { a: 1, b: 2 }
source = { b: 4, c: 5 }
source2 = { b: 9689, c: 5 }

// 参数中各对象存在相同属性时,后面的参数对象值替换前面的
Object.assign(target, source, source2)
// => {a: 1, b: 9689, c: 5}
```

```js
render_object (params) {
  defaultValue = {object: undefined, bg_color: ''}
  params = Object.assign(defaultValue, params)
}
```
# 函数 
## 函数定义 
## 函数调用 
## 函数的实参和形参 
## 作为值的函数 
## 作为命名空间的函数 
## 闭包 
## 函数属性、方法和构造函数 
## 函数式编程 
# 类和模块 
## 类和原型 
## 类和构造函数 
## JavaScript中Java式的类继承 
## 类的扩充 
## 类和类型 
## JavaScript中的面向对象技术 
## 子类 
## ECMAScript5中的类 
## 模块 
# 正则表达式的模式匹配 
## 正则表达式的定义 
## 用于模式匹配的String方法 
## RegExp对象 

```js
new RegExp('ab+c', 'i'); // 首个参数为字符串模式的构造函数
new RegExp(/ab+c/, 'i'); // 首个参数为常规字面量的构造函数
```

```js
value = "虚拟化/Docker/容器/Mysql"
reg = new RegExp('虚拟化/')
value.match(reg)
```

# JavaScript的子集和扩展 
## JavaScript的子集 
## 常量和局部变量 
## 解构赋值 
## 迭代 
## 函数简写 
## 多catch从句 
## E4X:ECMAScriptforXML 
# 服务器端JavaScript 
## 用Rhino脚本化Java 
## 用Node实现异步I/O 
 
# 客户端JavaScript 
## Web浏览器中的JavaScript 
### 客户端JavaScript 
### 在HTML里嵌入JavaScript 
### JavaScript程序的执行 
### 兼容性和互用性 
### 可访问性 
### 安全性 
### 客户端框架 
## Window对象 
### 计时器 
### 浏览器定位和导航 
### 浏览历史 
### 浏览器和屏幕信息 
### 对话框 
### 错误处理 
### 作为Window对象属性的文档元素 
### 多窗口和窗体
### 把字符串当函数执行的方法 

> 全局函数声明会变成全局对象的属性

```js
function test(str) { 
  alert(str); 
} 
window['test']('param'); //直接执行

window['test'].call(this,'param');//如果需要修改函数运行时的this
``` 
## 脚本化文档 
### DOM概览 
### 选取文档元素 
### 文档结构和遍历 
### 属性 
### 元素的内容 
### 创建、插入和删除节点

#### innerHTML

####  插入子节点(appendChild)

##### 加载script标签

HTML5不允许使用innerHTML属性动态添加脚本标签,可以通过创建节点然后插入头部标签:

```js
array = ["<link rel=\"stylesheet\" media=\"screen\" href=\"http:/…cabd26d48f43744361e79c393f7d8a98fa099ead1.css\" />", "<script src=\"http://123.57.155.17:3111/assets/dash…ac7b94e1995c3ed5f0abd10b385b139bf9c.js\"></script>", "<script src=\"http://123.57.155.17:3111/assets/dash…655251ad57e53c90e0175e213fc045ea735.js\"></script>", "<script src=\"http://123.57.155.17:3111/assets/dash…ff604b722cd0fb76eba7878f1d2eb08e27e.js\"></script>", "<script src=\"http://123.57.155.17:3111/assets/dash…6ce4f7051188a5b4d7a55a52de26613bfdc.js\"></script>", "<script src=\"http://123.57.155.17:3111/assets/dash…046ce2cc8790628740e037f4ab9f33cf44f.js\"></script>"]

for (i in array){
  item = array[i]
  attributes = item.split(/ |\>/)
  tag_name = attributes[0].split('<')[1]
  target = document.createElement(tag_name);
  
  useful_attributes = attributes.filter(function(str){return (/href|rel|media|src/.test(str))})
  for(index in useful_attributes){
    arr = useful_attributes[index].split('=')
    target.setAttribute(arr[0], arr[1].replace(/\"/g,''))
  }
  document.head.appendChild(target)
}
```


### 例子：生成目录表 
### 文档和元素的几何形状和滚动 
### HTML表单 
### 其他文档特性 
## 脚本化CSS 
### CSS概览 
### 重要的CSS属性 
### 脚本化内联样式 
### 查询计算出的样式 
### 脚本化CSS类 
### 脚本化样式表 
## 事件处理 
### 事件类型 
### 注册事件处理程序 
### 事件处理程序的调用 
### 文档加载事件 
### 鼠标事件 
### 鼠标滚轮事件 
### 拖放事件 
### 文本事件 
### 键盘事件 
## 脚本化HTTP 
### 使用XMLHttpRequest 
### 借助发送HTTP请求：JSONP 
### 基于服务器端推送事件的Comet技术 
## jQuery类库 
### jQuery基础 
### jQuery的getter和setter 
### 修改文档结构 
### 使用jQuery处理事件 
### 动画效果 
### jQuery中的Ajax 
### 工具函数 
### jQuery选择器和选取方法 

#### 基本选择器

```js
$("#id")                          // ID选择器
$("div")                          // 元素/标签选择器
$(".classname")                   // 类选择器
$("*")                            // 通用选择器
$(".classname,.classname1,#id1")  // 群组选择器
```

#### 层次选择器

```js
$("#id>.classname ")       // 子元素选择器
$("#id .classname ")       // 后代元素选择器
$("#id + .classname ")     // 紧邻下一个元素选择器
$("#id ~ .classname ")     // 兄弟元素选择器
```

#### 过滤选择器

```js
$("li:first")        // 第一个li
$("li:last")         // 最后一个li
$("li:even")         // 挑选下标为偶数的li
$("li:odd")          // 挑选下标为奇数的li
$("li:eq(4)")        // 下标等于4的li
$("li:gt(2)")        // 下标大于2的li
$("li:lt(2)")        // 下标小于2的li
$("li:not(#Joshua)") // 挑选除 id="Joshua" 以外的所有li
```

#### 内容过滤选择器

```js
$("div:contains('Joshua')")    // 包含 Joshua文本的元素
$("td:empty")                 // 不包含子元素或者文本的空元素
$("div:has(selector)")        // 含有选择器所匹配的元素
$("td:parent")                // 含有子元素或者文本的元素
```

#### 可见性过滤选择器

```js
$("li:hidden")       // 匹配所有不可见元素,或type为hidden的元素
$("li:visible")      // 匹配所有可见元素
```

#### 属性过滤选择器

```js
$("div[id]")        // 所有含有 id 属性的 div 元素
$("div[id='123']")        // id属性值为123的div 元素
$("div[id!='123']")        // id属性值不等于123的div 元素
$("div[id^='qq']")        // id属性值以qq开头的div 元素
$("div[id$='zz']")        // id属性值以zz结尾的div 元素
$("div[id*='bb']")        // id属性值包含bb的div 元素
$("input[id][name$='man']") // 多属性选过滤,同时满足两个属性的条件的元素
```

#### 状态过滤选择器

```js
$("input:enabled")    // 匹配可用的 input
$("input:disabled")   // 匹配不可用的 input
$("input:checked")    // 匹配选中的 input
$("option:selected")  // 匹配选中的 option
```

#### 表单选择器

```js
$(":input")      // 匹配所有 input, textarea, select 和 button 元素
$(":text")       // 所有的单行文本框,$(":text") 等价于$("[type=text]"),推荐使用$("input:text")效率更高,下同
$(":password")   // 所有密码框
$(":radio")      // 所有单选按钮
$(":checkbox")   // 所有复选框
$(":submit")     // 所有提交按钮
$(":reset")      // 所有重置按钮
$(":button")     // 所有button按钮
$(":file")       // 所有文件域
$(":iamge")      // 所有图像域
$(":hidden")     // 选择所有隐藏域及所有不可见元素(CSS display 属性值为 none)
```


### jQuery的插件扩展 
### jQueryUI类库 
## 客户端存储 
### localStorage和sessionStorage 
### cookie 
### 利用IEuserData持久化数据 
### 应用程序存储和离线Web应用 
## 多媒体和图形编程 
### 脚本化图片 
### 脚本化音频和视频 
### SVG：可伸缩的矢量图形 
### 中的图形 
## HTML5API 
### 地理位置 
### 历史记录管理 
### 跨域消息传递

#### 关于Fetch+Rails跨域请求实例

有文章说mode参数为必须项,但请求成功后发现去掉依然可以,主要调整在于后端.

```js
data = {
  api_code: 'publish_other_request_assets',
  rely_assets:  ["bootstrap_4", "jQuery_3_5_1"],
  response_type: 'json',
}

var requestOptions = {
  method: 'POST',
  redirect: 'follow',
  mode: 'cors',
  headers: {
    'content-type': 'application/json'
  },
  body: JSON.stringify(data),
};

fetch("http://123.57.155.17:3111/dashboard/restful/request_api", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
```

后端:

```rb
# controller
after_action :about_cors, only: [:request_api]

def about_cors
  response.set_header('Access-Control-Allow-Origin', '*')
  response.set_header('Access-Control-Allow-Methods', 'POST, PUT, DELETE, GET, OPTIONS')
  response.set_header('Access-Control-Request-Method', '*')
  response.set_header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
end

# route
match '/restful/request_api' => "restful#request_api", via: :options
```

### WebWorker 
### 类型化数组和ArrayBuffer 
### Blob 

#### 实际应用

```js
// 数据结构
{
  result: 'OK',
  download: {
    mimetype: string(mimetype in the form 'major/minor'),
    filename: string(the name of the file to download),
    data: base64(the binary data as base64 to download)
  }
}

// 执行以下操作以通过AJAX保存文件

var a = document.createElement('a');
if (window.URL && window.Blob && ('download' in a) && window.atob) {
  // Do it the HTML5 compliant way
  var blob = base64ToBlob(result.download.data, result.download.mimetype);
  var url = window.URL.createObjectURL(blob);
  a.href = url;
  a.download = result.download.filename;
  a.click();
  window.URL.revokeObjectURL(url);
}

function base64ToBlob(base64, mimetype, slicesize) {
  if (!window.atob || !window.Uint8Array) {
    // The current browser doesn't have the atob function. Cannot continue
    return null;
  }
  mimetype = mimetype || '';
  slicesize = slicesize || 512;
  var bytechars = atob(base64);
  var bytearrays = [];
  for (var offset = 0; offset < bytechars.length; offset += slicesize) {
    var slice = bytechars.slice(offset, offset + slicesize);
    var bytenums = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      bytenums[i] = slice.charCodeAt(i);
    }
    var bytearray = new Uint8Array(bytenums);
    bytearrays[bytearrays.length] = bytearray;
  }
  return new Blob(bytearrays, {type: mimetype});
};
```

### 文件系统API 
### 客户端数据库 
### Web套接字 

# 常见问题

## $.ajax is not a function

> [Error] TypeError: undefined is not a function (near '...$.ajax...')

这里的问题是Slim版本的JQuery不支持Ajax.

# 参考资料

> [京东 | JavaScript权威指南(第6版)](https://item.m.jd.com/product/10974436.html)

> [简书 | js 把字符串当函数执行的方法](https://www.jianshu.com/p/636000befcf3)

> [简书 | JQuery常用的选择器总结分类](https://www.jianshu.com/p/212f2da26985)

> [codebyamir | TypeError: $.ajax is not a function](https://www.codebyamir.com/blog/typeerror-ajax-is-not-a-function)

> [stackoverflow | JSON.stringify output to div in pretty print way](https://stackoverflow.com/questions/16862627/json-stringify-output-to-div-in-pretty-print-way)

> [stackoverflow | Download a file by jQuery.Ajax](https://stackoverflow.com/questions/4545311/download-a-file-by-jquery-ajax)

> [stackoverflow | How do you route [OPTIONS] in Rails?](https://stackoverflow.com/questions/14540065/how-do-you-route-options-in-rails)

> [stackoverflow | How do you add a custom http header?](https://stackoverflow.com/questions/16654052/how-do-you-add-a-custom-http-header)

> [stackoverflow | Can scripts be inserted with innerHTML?](https://stackoverflow.com/questions/1197575/can-scripts-be-inserted-with-innerhtml)

> [zkea | Javascript生成字符串的哈希值(Hash)](http://www.zkea.net/codesnippet/detail/javascript-hash.html)

> [简书 | JavaScript switch语句的技巧](https://www.jianshu.com/p/3efebd2c785d)

> [stackoverflow | 'this' is undefined in JavaScript class methods](https://stackoverflow.com/questions/4011793/this-is-undefined-in-javascript-class-methods)

> [stackoverflow | Set default value of Javascript object attributes](https://stackoverflow.com/questions/6600868/set-default-value-of-javascript-object-attributes)

> [stackoverflow | Find element in array using regex match without iterating over array](https://stackoverflow.com/questions/7038575/find-element-in-array-using-regex-match-without-iterating-over-array/43825436)

> [zhangxinxu | JS字符串补全方法padStart()和padEnd()简介](https://www.zhangxinxu.com/wordpress/2018/07/js-padstart-padend/)

> [最停留 | JS判断全半角字符并实现互相转化](http://zuitingliu.com/?p=202)

> [stackoverflow | How to get the value from the GET parameters?](https://stackoverflow.com/questions/979975/how-to-get-the-value-from-the-get-parameters?rq=1)

> [stackoverflow | Get the last item in an array](https://stackoverflow.com/questions/3216013/get-the-last-item-in-an-array)

> [stackoverflow | How do I convert array of Objects into one Object in JavaScript?](https://stackoverflow.com/questions/19874555/how-do-i-convert-array-of-objects-into-one-object-in-javascript)

> [stackoverflow | JavaScript: filter() for Objects](https://stackoverflow.com/questions/5072136/javascript-filter-for-objects)

> [stackoverflow | Converting user input string to regular expression](https://stackoverflow.com/questions/874709/converting-user-input-string-to-regular-expression)