import Base from './base/base.mjs'
export default class Graphic extends Base {
  #childPaths = [
    './bootstrap4/bootstrap4.mjs', 
    './amcharts4/amcharts4.mjs', 
    './chartJs2/chartJs2.mjs',
    './googleCharts/googleCharts.mjs',
    './highcharts8/highcharts8.mjs',
    './excel/excel.mjs',
    // './highcharts9/highcharts9.mjs',
    
  ]

  async loadChilds() {
    // 该方法仅类本身持有 子类不允许访问
    if (!this.__proto__.constructor.name == "Graphic") return undefined;
    if (!!this.loadedChilds) return [true, this];

    this.childs = []
    for await (let path of this.#childPaths) {
      let _this = this
      await import(path).then((module) => {
        let module_name = path.split('/')[path.split('/').length - 1].split('.')[0]
        _this[module_name] = new module.default()
        this.childs.push(module_name)
      })
    }

    for (let item of this.childs) {
      if (this[item].assets) this.loadAssets({assets: this[item].assets})
    }

    this.loadedChilds = true
    return [true, this]
  }

  constructor(params={}) {
    super();
    let defaultValue = {
      assets: undefined, // 依赖资源集合
      awaitAssets: undefined, // 需等待加载完成的资源
      cdnHost: '.',
      // cdnHost: 'https://ff4c00.gitlab.io/graphic',
    }
    params = Object.assign(defaultValue, params)
    for (let [key, value] of Object.entries(defaultValue)) {
      this[key] = value
    }

    // 类自身需要加载内容
    if (this.__proto__.constructor.name == "Graphic") {
      this.assets = [
        { element: 'link', href: './base/base.css', rel: 'stylesheet' },
        { element: 'script', src: './base/moment.js' },
      ]
      this.loadAssets({ assets: this.assets })
    }

  }

  // 将对象转换为资源链接
  // params.asset={element: 'link', href: 'https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css', rel:'stylesheet'} 
  async loadAsset(params={}) {
    let defaultValue = {
      asset: undefined
    }
    params = Object.assign(defaultValue, params)
    if (!!!params.asset) return [false, 'asset 对象不能为空']
    if (params.asset.element == 'script') {
      params.asset.type = 'text/javascript'
      params.asset.onload = "graphicLoaded();"
    }

    if (params.asset.element == 'script') {
      let src = params.asset.src
      if (src.match(/^\.\//)){
        src = src.replace(/^\./, this.cdnHost)
        params.asset.src = src
      }
      params.asset.async = false
    }else if (params.asset.element == 'link'){
      let href = params.asset.href
      if (href.match(/^\.\//)){
        href = href.replace(/^\./, this.cdnHost)
        params.asset.href = href
      }
    }

    let res = this.toElement(params.asset)
    if (!res[0]) return res

    document.head.appendChild(res[1])
    return res
  }

  async loadAssets(params={}) {
    let defaultValue = {
      assets: undefined
    }
    params = Object.assign(defaultValue, params)
    if (!!!params.assets) return [false, 'assets 对象不能为空']
    if (!!!Array.isArray(params.assets)) return [false, 'assets 对象应为数组格式']

    let errors = []
    for (let [index, item] of params.assets.entries()) {
      this.loadAsset({ asset: item }).then(res => {
        if (!res[0]) return errors.push(index + ': ' + res[1]);
      })
    }

    if (errors.length > 0) return [false, errors.join(', ')]
    return [true, '']
  }

}