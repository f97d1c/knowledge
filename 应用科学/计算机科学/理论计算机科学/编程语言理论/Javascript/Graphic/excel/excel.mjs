import Graphic from '../graphic.mjs'
export default class Excel extends Graphic {
  constructor(params = {}) {
    let assets = [
      { element: 'script', src: './excel/jsonExportExcel.min.js' },
    ]
    super({ assets: assets, awaitAssets: ['ExportJsonExcel'] })
  }

  toXlsx(params = {}) {
    let described = this.described.initialize({
      名称: '生成xlsx文件',
      描述: '根据提供对象转换成xlsx文件.',
      参数: {
        父元素: { 类型: 'string', 描述: '无效参数', 默认值: ''},
        文件名: { 类型: 'string', 描述: '', 默认值: undefined},
        时间后缀: { 类型: 'string', 描述: '文件名后添加当前时间后缀, 参数值为时间格式, 不需要后缀传空字符串', 默认值: '_YYYYMMDDHHMM'},
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    let fileName = described.文件名
    if (!!described.时间后缀){
      let now = new Date()
      let suffix = moment(now).format(described.时间后缀)
      fileName+=suffix
    }
  
    let option={
      fileName: fileName,
      datas: []
    }

    for (let [key, value] of Object.entries(described.渲染对象)){
      let sheet = {
        sheetData: value,
        sheetName: key,
        sheetFilter: Object.keys(value[0]),
        sheetHeader: Object.keys(value[0]),
      }
      option.datas.push(sheet)
    }
    
    let toExcel = new ExportJsonExcel(option)
    toExcel.saveExcel()
    return [true, '']
  }
}