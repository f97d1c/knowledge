// 页面存在同名(import.mjs)文件时通过取值graphic-cdn-host属性
let cdnHost = (
  document.querySelector("script[graphic-cdn-host]") ||
  document.createElement('script')
).getAttribute('graphic-cdn-host')
cdnHost ||= (document.querySelector("script[src*='import.mjs']") || document.createElement('script')).getAttribute('src').replace(/\/{0,}import.mjs/, '') || '.'

let objectName = (
  document.querySelector("script[graphic-object-name]") ||
  document.createElement('script')
).getAttribute('graphic-object-name') || 'Graphic'

window.graphicLoaded = async function () {
  if (!!Graphic.onloaded) return;
  let totalCount = 0
  let loadedCount = 0

  for await (let item of Graphic.childs) {
    if (!!!Graphic[item].awaitAssets) continue;
    Graphic[item].awaitAssets.forEach(item => {
      totalCount += 1
      if (!!window[item]) {
        loadedCount += 1
      }
    })
  }

  if ((totalCount != 0) && (totalCount == loadedCount)) {
    Graphic.onloaded = true
    window.graphicReady()
  }
}

import(cdnHost + '/graphic.mjs').then((module) => {
// import('./graphic.mjs').then((module) => {
  window[objectName] = new module.default({ cdnHost: cdnHost })
  window[objectName].loadChilds()
})