import Described from './described.mjs';
export default class Base {

  constructor(params = {}) {
    this.described = Described;
  }

  // 判断值原始类型
  // 基础typeof在类型表达上过于模糊例如3和3.14同属于number, 利用正则解析字符串进行相对准确判断
  _typeof(value) {
    if (!!!value) return typeof value;
    // if (typeof value == 'string') return 'string' // 字符串类型

    let valueStr = value.toString()
    if (typeof value == 'object') {
      if (Array.isArray(value)) { // 数组对象类型
        return 'array'
      } else if (!!(value.__proto__.toString().match(/^\[object .*Element\]$/))) { // html元素类型
        return 'element'
      } else {
        return 'keyValue' // 键值对类型
      }
    } else if (valueStr.match(/^http(s|)\:\/\//)) { // 链接类型
      return 'url'
    } else if (!!valueStr.match(/^(\-|)\d{1,}$/)) { // 数值类型(整数)
      return 'integer'
    } else if (!!valueStr.match(/^(\-|)\d{1,}\.{1}\d{0,2}$/)) { // 数值类型(2位小数)
      return 'decimal(2)'
    } else if (!!valueStr.match(/^(\-|)\d{1,}\.{1}\d{2,}$/)) { // 数值类型(大于2位小数)
      return 'decimal(2+)'
    } else if (!!valueStr.match(/^\d{4}\-\d{2}\-\d{2}$/)) { // 日期类型
      return 'date'
    } else if (!!valueStr.match(/^\d{4}\-\d{2}\-\d{2}T/)) { // 日期时间类型
      return 'dateTime'
    } else { // 其他按原始类型
      return typeof value
    }
  }

  // 多类型可选数据格式验证
  multiType(mtype) {
    let mapping = {
      // 名称规则 基础类型在前,其他类型在后
      'stringORfunction': (type) => { return ['string', 'function'].includes(type) },
      'stringORinteger': (type) => { return ['string', 'integer'].includes(type) },
    }
    let defaultLambda = () => { return false }
    return mapping[mtype] || defaultLambda
  }

  // 根据给定对象属性 转换为html的element元素
  toElement(params = {}) {
    let defaultValue = {
      element: undefined,
      innerHtml: undefined,
      outerText: undefined,
    }

    let object = Object.keys(params).filter(key => { return !Object.keys(defaultValue).includes(key) })
    params = Object.assign(defaultValue, params)

    let element = document.createElement(params.element)
    for (let key of object) {
      let value = params[key]
      if (typeof value == 'object') value = JSON.stringify(value)
      element.setAttribute(key, value)
    }

    if (!!params.innerHtml) {
      if (typeof params.innerHtml != 'object') {
        element.innerHTML = params.innerHtml
      } else if (!Array.isArray(params.innerHtml)) {
        let res = this.toElement(params.innerHtml)
        if (res[0]) element.innerHTML = res[1].outerHTML
      } else {
        params.innerHtml.forEach(item => {
          if (typeof item != 'object') {
            element.innerHTML = item
          } else if (!Array.isArray(item)) {
            element.appendChild(this.toElement(item)[1])
          }
        })
      }
    }
    if (!!params.outerText) element = { outerHTML: (element.outerHTML + params.outerText) }
    return [true, element]
  }

  uuid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";
    var uuid = s.join("");
    return uuid;
  }

  hashCode(string) {
    return string.split('').reduce((a, b) => { a = ((a << 5) - a) + b.charCodeAt(0); return a & a }, 0)
  }

  // 根据给出的当前节点 找到距离最近的预期类型的父节点
  // Graphic.getParentNode(this, 'TABLE')
  getParentNode(node, expectNodeName) {
    let current = node
    while (current.parentNode) {
      current = current.parentNode
      if (current.nodeName == expectNodeName) return current
    }
    return current
  }

  sudoku(params = {}) {
    let described = this.described.initialize({
      名称: '九宫格',
      描述: '根据提供的父对象,将其子元素进行等比例缩放,当点击元素时将全屏显示.',
      参数: {
        渲染对象: { 类型: 'string', 描述: '无效参数', 默认值: '无效参数' },
        每行元素个数: { 类型: 'integer', 默认值: 3 },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    if (described.父元素.childNodes.length == 0) return [false, '父对象内无子元素']

    let itemWidth = window.screen.width / described.每行元素个数
    let hwRatio = window.screen.height / window.screen.width
    let itemHeight = itemWidth * hwRatio
    described.父元素.childNodes.forEach(item => {
      item.style.width = itemWidth + 'px'
      item.style.height = itemHeight + 'px'
      item.style.float = 'left'
      item.setAttribute('onclick', 'javascript:this.requestFullscreen();')
    })

    // TODO: 后期有需要改成参数配置
    described.父元素.style.height = Math.ceil(described.父元素.childNodes.length / described.每行元素个数) * itemHeight + 'px'
    described.父元素.style.marginTop = '20px'
    described.父元素.style.marginBottom = '20px'
    return [true, '']
  }

}