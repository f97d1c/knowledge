import Graphic from '../graphic.mjs'
export default class Bootstrap4 extends Graphic {
  constructor(params = {}) {
    let assets = [
      { element: 'script', src: 'https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js' },
      { element: 'link', href: 'https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css', rel: 'stylesheet' },
      { element: 'script', src: 'https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js' },
    ]
    // TODO: deepCall $.fn , awaitAssets: ['$.fn'] 
    super({ assets: assets });

  }

  baseTable(params = {}) {
    let described = this.described.initialize({
      名称: '基础表格',
      描述: '根据提供的渲染对象转为表格',
      参数: {
        表单样式类: { 类型: 'string', 默认值: 'table table-bordered table-striped table-hover' },
        名称单元格占比: { 类型: 'integer', 默认值: 30 },
        每行元素个数: { 类型: 'integer', 默认值: 2 },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    let labelRatio = described.名称单元格占比
    described.名称单元格占比 = (100 / described.每行元素个数) * (labelRatio / 100)
    described.内容单元格占比 = (100 / described.每行元素个数) * ((100 - labelRatio) / 100)

    let groupedKeys = [];
    let keys = Object.keys(described.渲染对象)
    for (let index = 0; index < keys.length; index += described.每行元素个数) {
      groupedKeys.push(keys.slice(index, index + described.每行元素个数))
    }

    res = described.createEle(Object.assign({
      element: 'table',
      class: described.表单样式类,
    }, params))
    if (!res[0]) return res

    let table = described.element
    let tbody = document.createElement('tbody')
    table.appendChild(tbody)

    if (!!described.标题) {
      let tr = document.createElement('tr')
      let td = document.createElement('td')
      td.setAttribute('colspan', described.每行元素个数 * 2)
      td.setAttribute('class', 'text-center')
      td.innerHTML = described.标题
      tr.appendChild(td)
      tbody.appendChild(tr)
    }

    groupedKeys.forEach(array => {
      let tr = document.createElement('tr')

      for (let key of array) {
        let label = {
          element: 'td',
          class: 'text-right mr-2 td-key',
          style: 'width: ' + described.名称单元格占比 + '%;',
          innerHtml: key,
        }

        label = this.toElement(label)[1]
        tr.appendChild(label)

        let value = described.渲染对象[key]
        let content = {
          element: 'td',
          class: 'td-value',
          style: 'width: ' + described.内容单元格占比 + '%;',
        }
        let td = this.toElement(content)[1]
        if (typeof value != 'object') {
          if ((typeof value == 'number') || !!(typeof value == 'string' && value.match(/^\d{1,}\.{0,1}\d{0,}$/))) {
            td.setAttribute('class', 'text-right')
            td.innerHTML = (parseInt(value, 10)).toLocaleString(undefined, { minimumFractionDigits: 2 })
          } else {
            td.innerHTML = value
          }
        } else if (!Array.isArray(value)) {
          td.innerHTML = this.toElement(value)[1].outerHTML
        } else if (Array.isArray(value) && value.length == 1) {
          td.innerHTML = this.toElement(value[0])[1].outerHTML
        } else {
          let div = this.toElement({ element: 'div', class: 'form-inline' })[1]
          value.forEach(item => {
            if (typeof item != 'object') {
              td.innerHTML = item
            } else if (!Array.isArray(item)) {
              div.appendChild(this.toElement(item)[1])
            }
          })
          td.appendChild(div)
        }
        if (!!td) tr.appendChild(td);
      }
      tbody.appendChild(tr)
    })

    return [true, described.element]
  }

  listTable(params = {}) {
    let described = this.described.initialize({
      名称: '集合列表表格',
      描述: '根据提供的渲染对象转为表格',
      参数: {
        渲染对象: { 类型: 'array', 默认值: undefined },
        表单样式类: { 类型: 'string', 默认值: 'table table-bordered table-striped table-hover mt-3' },
        列键: { 类型: 'array', 描述: '表格中字段的渲染顺序, 如果给定数组,则按顺序渲染数组元素', 默认值: [] },
        自动换行: { 类型: 'boolean', 默认值: true },
        显示行号: { 类型: 'boolean', 默认值: true },
        总行数: { 类型: 'stringORinteger', 描述: '如果为分页数据, 这里的总行数应为全部数据条数.', 默认值: '渲染对象条数' },
        缺省默认值: { 类型: 'string', 描述: '当渲染对象属性值为空时默认渲染内容', 默认值: '无' },
        最大宽度: { 类型: 'string', 描述: 'td最大宽度(max-width)限制, 仅在自动换行为true条件下生效.', 默认值: '500px' },
        渲染顺序: { 类型: 'string', 描述: '渲染对象内部的排序方式, 参数值非"正序"均按倒序排列.', 默认值: '正序' },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    if (!!!described.自动换行) {
      described.表单样式类 += ' text-nowrap'
      if (described.渲染对象.length > 10) described.表单样式类 += ' table-fix-head'
      if (JSON.stringify(described.渲染对象[0] || '').length > 160) described.表单样式类 += ' table-responsive'
    }

    if (described.列键.length == 0) described.列键 = Object.keys(described.渲染对象[0])

    let element = {
      element: 'table',
      class: described.表单样式类,
      innerHtml: [
        {
          element: 'thead',
          innerHtml: [],
        },
        {
          element: 'tbody',
          innerHtml: [],
        }
      ]
    }
    let currentIndex = 0

    if (described.显示行号) {
      if (described.总行数 == '渲染对象条数') described.总行数 = described.渲染对象.length
      element.innerHtml[0].innerHtml.push({
        element: 'th',
        style: 'width: 5%',
        class: 'text-center text-nowrap',
        innerHtml: '序号/' + described.总行数
      })
    }
    described.列键.forEach(name => {
      element.innerHtml[0].innerHtml.push({ element: 'th', class: 'text-center text-nowrap', innerHtml: name })
    })

    described.渲染对象.forEach(object => {
      let tr = { element: 'tr', innerHtml: [] }
      element.innerHtml[1].innerHtml.push(tr)
      if (described.显示行号) tr.innerHtml.push({ element: 'td', class: 'text-center', innerHtml: (currentIndex += 1) })

      described.列键.forEach(key => {
        let td = { element: 'td' }
        if (described.自动换行) td.style = 'word-break:break-all; max-width: ' + described.最大宽度
        tr.innerHtml.push(td)
        let value = object[key] || described.缺省默认值

        switch (this._typeof(value)) {
          case 'array':
            td.innerHtml = JSON.stringify(value)
            // td.innerHTML = value.join('/')
            break;
          case 'keyValue':
            if (!!value.element) {
              td.innerHtml = value
            } else {
              td.innerHtml = JSON.stringify(value)
            }
            break;
          case 'url':
            td.innerHtml = { element: 'a', href: value, target: '_blank', innerHtml: value }
            break;
          case 'integer':
            td.class = 'text-right'
            td.innerHtml = value
            break;
          case 'decimal(2)':
            td.class = 'text-right'
            td.innerHtml = (parseFloat(value)).toLocaleString(undefined, { minimumFractionDigits: 2 })
            break;
          case 'decimal(2+)':
            td.class = 'text-right'
            td.innerHtml = (parseFloat(value)).toLocaleString(undefined, { minimumFractionDigits: 4 })
            break;
          case 'date':
          case 'dateTime':
            td.class = 'text-center'
            td.innerHtml = value
            break;
          default:
            td.innerHtml = value
            break;
        }

      })
    })

    if (!!described.标题) {
      let colSize = described.列键.length
      if (described.显示行号) colSize += 1

      let title = {
        element: 'thead',
        innerHtml: {
          element: 'th',
          colspan: colSize,
          class: 'text-center',
          innerHtml: described.标题,
        },
      }

      element.innerHtml.unshift(title)
    }

    if (described.渲染顺序 != '正序') {
      let trs = element.innerHtml[1].innerHtml
      element.innerHtml[1].innerHtml = trs.reverse()
    }

    res = described.createEle(element)
    if (!res[0]) return res

    return [true, described.element]
  }

  range_input(key, type, name) {
    name ||= key
    return [
      {
        element: 'input',
        type: type,
        name: key,
        class: 'form-control',
        placeholder: name + '大于等于',
        es_query: "{\"range\": {\"" + key + "\": {\"gte\": \"value\"}}}",
      },
      {
        element: 'input',
        type: type,
        name: key,
        class: 'form-control',
        placeholder: name + '小于等于',
        es_query: "{\"range\": {\"" + key + "\": {\"lte\": \"value\"}}}",
      }
    ]
  }

  formTable(params = {}) {
    let described = this.described.initialize({
      名称: '表单表格',
      描述: '根据提供的渲染对象转为表单表格',
      参数: {
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    for (let [key, value] of Object.entries(described.渲染对象)) {
      let innerHTML = { element: 'input', class: 'form-control', name: key, es_query: "{\"match\": {\"" + key + "\": \"value\"}}" }

      switch (value.type) {
        case 'text':
          innerHTML.type = value.type
          break;
        case 'date':
          innerHTML = this.range_input(key, value.type)
          break;
        case 'version':
          innerHTML = this.range_input(key, 'number')
          break;
        default:
          if (Array.isArray(value)) {
            innerHTML = value
          } else {
            innerHTML.type = 'text'
          }
          break;
      }
      described.渲染对象[key] = innerHTML
    }

    return this.baseTable(Object.assign(params, {
      渲染对象: described.渲染对象,
    }))
  }

  compareTable(params = {}) {
    let described = this.described.initialize({
      名称: '对比表格',
      描述: '针对渲染对象的属性值进行分类对比',
      参数: {
        表单样式类: { 类型: 'string', 默认值: 'table table-bordered table-striped table-hover mt-3' },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    let compareAttributes = Object.keys(Object.values(described.渲染对象)[0])
    let compareTargets = Object.keys(described.渲染对象)

    let innerHtml = [{
      element: 'thead',
      innerHtml: {
        element: 'tr',
        innerHtml: []
      }
    }, {
      element: 'tbody',
      innerHtml: []
    }]

    compareTargets.unshift('')
    compareTargets.forEach(item => {
      innerHtml[0].innerHtml.innerHtml.push({ element: 'td', innerHtml: item })
    })

    compareTargets.shift()
    compareAttributes.forEach(attr => {
      let tr = { element: 'tr', innerHtml: [{ element: 'td', innerHtml: attr }] }
      compareTargets.forEach(key => {
        tr.innerHtml.push({ element: 'td', innerHtml: described.渲染对象[key][attr] })
      })
      innerHtml[1].innerHtml.push(tr)
    })

    res = described.createEle(Object.assign({
      element: 'table',
      class: described.表单样式类,
      innerHtml: innerHtml
    }, params))
    if (!res[0]) return res

    return [true, described.element]
  }

  htmlContent(params = {}){
    let described = this.described.initialize({
      名称: 'HTML文本展示',
      描述: '对HTML文本内容进行展示',
      参数: {
        渲染对象: { 类型: 'string'},
        表单样式类: { 类型: 'string', 默认值: 'jumbotron' },
      }
    })

    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    let title = '<h1 class="text-center">'+described.标题+'</h1>'

    let element = {
      element: 'div',
      class: described.表单样式类,
      innerHtml: title+described.渲染对象
    }

    res = described.createEle(element)
    if (!res[0]) return res

    return [true, described.element]
  }
}