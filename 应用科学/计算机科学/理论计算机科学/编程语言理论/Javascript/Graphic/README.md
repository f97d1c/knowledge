<!-- TOC -->

- [关于](#关于)
  - [项目用途](#项目用途)
  - [说明文档](#说明文档)
    - [参数非空](#参数非空)
  - [额外参数](#额外参数)
  - [引入资源](#引入资源)
    - [自定义实例对象名称](#自定义实例对象名称)
    - [自定义CDN Host](#自定义cdn-host)

<!-- /TOC -->
# 关于

## 项目用途

> 该项目用于对JS Object对象进行渲染/转换成HTML Document对象.

## 说明文档

详见[示例页面](https://ff4c00.gitlab.io/graphic/).

### 参数非空

示例页面参数说明中,默认值为无的表示参数必传,其他参数为可选.

## 额外参数

如果传递文档中未提及的参数,那么该参数将用于渲染对象的创建过程,作为toElement的参数.

## 引入资源

页面加入JS标签:

```html
<script type="module" src="https://ff4c00.gitlab.io/graphic/import.mjs" async></script>
```

Graphic加载完毕后将调用*window.graphicReady*函数.

```js
window.graphicReady = function () {
  // 相关依赖内容应放入该方法中
}
```

### 自定义实例对象名称

默认对象名称为: Graphic,可根据 *graphic-object-name*属性进行自定义.

```js
// 最终实例对象名称为: Magic
<script type="module" src="https://ff4c00.gitlab.io/graphic/import.mjs" graphic-object-name="Magic" async></script>
```

### 自定义CDN Host

如果加载资源中存在与import.mjs同名Js标签时,可定义*graphic-cdn-host*属性.

```js
<script type="module" src="https://ff4c00.gitlab.io/graphic/import.mjs" graphic-cdn-host="https://ff4c00.gitlab.io/graphic" async></script>
```