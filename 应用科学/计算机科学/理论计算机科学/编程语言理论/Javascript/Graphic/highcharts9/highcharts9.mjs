import Graphic from '../graphic.mjs'
export default class Highcharts9 extends Graphic {
  constructor(params={}) {
    let assets = [
      { element: 'script', src: './highcharts9/highcharts9.agg.js' },
    ]
    super({ assets: assets, awaitAssets: ['Highcharts'] })
  }

}