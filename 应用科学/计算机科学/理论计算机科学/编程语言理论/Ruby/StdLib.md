> Ruby标准类库(2.7.0)介绍

<!-- TOC -->

- [说明](#说明)
  - [CGI](#cgi)
  - [代码覆盖率](#代码覆盖率)
  - [Zlib](#zlib)
  - [UUId](#uuid)
  - [包含内容](#包含内容)
- [Abbrev](#abbrev)
  - [使用](#使用)
    - [基础用法](#基础用法)
    - [匹配模式](#匹配模式)
    - [数组扩展](#数组扩展)
- [Ostruct](#ostruct)
  - [使用](#使用-1)
    - [赋值示例-1](#赋值示例-1)
    - [赋值示例-2](#赋值示例-2)
    - [特殊字符作为键](#特殊字符作为键)
    - [键名包含空格](#键名包含空格)
    - [删除属性](#删除属性)
    - [嵌套使用](#嵌套使用)
    - [转为标准哈希](#转为标准哈希)
    - [遍历](#遍历)
    - [多层嵌套转换](#多层嵌套转换)
- [SecureRandom](#securerandom)
  - [使用](#使用-2)
    - [生成随机的十六进制字符串](#生成随机的十六进制字符串)
    - [生成随机的base64字符串](#生成随机的base64字符串)
    - [适用于URL的base64字符串](#适用于url的base64字符串)
    - [生成随机二进制字符串](#生成随机二进制字符串)
    - [生成字母数字字符串](#生成字母数字字符串)
    - [生成UUID](#生成uuid)
- [FileUtils](#fileutils)
  - [新建目录(多层级)](#新建目录多层级)
    - [](#)
    - [](#-1)
    - [](#-2)
    - [](#-3)
    - [](#-4)
    - [](#-5)
    - [](#-6)
    - [](#-7)
    - [](#-8)
    - [](#-9)
    - [](#-10)
    - [](#-11)
    - [](#-12)
    - [](#-13)
    - [](#-14)
    - [](#-15)
    - [](#-16)
    - [](#-17)
    - [](#-18)
    - [](#-19)
    - [](#-20)
    - [](#-21)
    - [](#-22)
    - [](#-23)
    - [](#-24)
    - [](#-25)
    - [](#-26)
    - [](#-27)
    - [](#-28)
    - [](#-29)
    - [](#-30)
    - [](#-31)
    - [](#-32)
    - [](#-33)
    - [](#-34)
    - [](#-35)
    - [](#-36)
    - [](#-37)
    - [](#-38)
    - [](#-39)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## CGI

公共网关接口(Common Gateway Interface,CGI)是Web 服务器运行时外部程序的规范,按CGI 编写的程序可以扩展服务器功能.<br>
CGI 应用程序能与浏览器进行交互,还可通过数据API与数据库服务器等外部数据源进行通信,从数据库服务器中获取数据.<br>
格式化为HTML文档后,发送给浏览器,也可以将从浏览器获得的数据放到数据库中.

## 代码覆盖率

衡量验证进展的最简易的方式是使用代码覆盖率.<br>
这种方式衡量的是多少行代码已经被执行过.

## Zlib

Zlib逐渐成为一种便携式,免费,通用,法律上不受阻碍-即不受任何专利保护的无损数据压缩库,可用于几乎任何计算机硬件和操作系统.

## UUId

通用唯一识别码（英语：Universally Unique Identifier，缩写：UUID）是用于计算机体系中以识别信息数目的一个128位标识符。

根据标准方法生成，不依赖中央机构的注册和分配，UUID具有唯一性，这与其他大多数编号方案不同.<br>
重复UUID码概率接近零，可以忽略不计。

## 包含内容

库名|说明
-|-
[abbrev](https://ruby-doc.org/stdlib-2.7.0/libdoc/abbrev/rdoc/Abbrev.html)|计算给定字符串集的明确缩写集
[base64](https://ruby-doc.org/stdlib-2.7.0/libdoc/base64/rdoc/index.html)|提供base64编码支持
[benchmark](https://ruby-doc.org/stdlib-2.7.0/libdoc/benchmark/rdoc/index.html)|提供了测量和报告Ruby代码运行使用时间的方法
[bigdecimal](https://ruby-doc.org/stdlib-2.7.0/libdoc/bigdecimal/rdoc/index.html)|提供任意精度的浮点十进制算术
[bundler](https://ruby-doc.org/stdlib-2.7.0/libdoc/bundler/rdoc/Bundler.html)|通过跟踪和安装所需的确切gem和版本，为Ruby项目提供了一致的环境
[cgi](https://ruby-doc.org/stdlib-2.7.0/libdoc/cgi/rdoc/index.html)|cgi支持库
[cmath](https://ruby-doc.org/stdlib-2.7.0/libdoc/cmath/rdoc/index.html)|为复数提供三角函数和超越函数的函数库
[coverage](https://ruby-doc.org/stdlib-2.7.0/libdoc/coverage/rdoc/index.html)|提供覆盖率测量功能
[csv](https://ruby-doc.org/stdlib-2.7.0/libdoc/csv/rdoc/index.html)|提供csv文件格式支持
[date](https://ruby-doc.org/stdlib-2.7.0/libdoc/date/rdoc/index.html)|
[dbm](https://ruby-doc.org/stdlib-2.7.0/libdoc/dbm/rdoc/index.html)|
[debug](https://ruby-doc.org/stdlib-2.7.0/libdoc/debug/rdoc/index.html)|提供调试功能
[delegate](https://ruby-doc.org/stdlib-2.7.0/libdoc/delegate/rdoc/index.html)|提供一种代替extend的机制,可以有效地降低代码的耦合性
[digest](https://ruby-doc.org/stdlib-2.7.0/libdoc/digest/rdoc/index.html)|提供哈希/摘要算法支持
[***drb***](https://ruby-doc.org/stdlib-2.7.0/libdoc/drb/rdoc/index.html)|提供多个Ruby进程通过网络相互通信服务
[e2mmap](https://github.com/ruby/e2mmap)|提供定义带有特定消息的自定义异常服务
[erb](https://ruby-doc.org/stdlib-2.7.0/libdoc/erb/rdoc/index.html)|提供模板注入服务
[etc](https://ruby-doc.org/stdlib-2.7.0/libdoc/etc/rdoc/index.html)|提供访问通常存储在Unix系统上/etc目录中的文件中的信息服务
[expect](https://ruby-doc.org/stdlib-2.7.0/libdoc/expect/rdoc/index.html)|提供expect脚本支持
[extmk](https://ruby-doc.org/stdlib-2.7.0/libdoc/extmk/rdoc/index.html)|
[fcntl](https://ruby-doc.org/stdlib-2.7.0/libdoc/fcntl/rdoc/index.html)|
[fiddle](https://ruby-doc.org/stdlib-2.7.0/libdoc/fiddle/rdoc/index.html)|使用ruby翻译外部功能接口(FFI)的扩展
[fileutils](https://ruby-doc.org/stdlib-2.7.0/libdoc/fileutils/rdoc/index.html)|提供用于复制,移动,删除等几种文件操作支持
[find](https://ruby-doc.org/stdlib-2.7.0/libdoc/find/rdoc/index.html)|提供文件搜索服务
[forwardable](https://ruby-doc.org/stdlib-2.7.0/libdoc/forwardable/rdoc/index.html)|提供向类中定义方法委托的功能服务
[gdbm](https://ruby-doc.org/stdlib-2.7.0/libdoc/gdbm/rdoc/index.html)|GNU dbm(gdbm),一个简单的数据库引擎,用于在磁盘上存储键值对
[getoptlong](https://ruby-doc.org/stdlib-2.7.0/libdoc/getoptlong/rdoc/index.html)|提供命令行选项的解析支持
[io/console](https://ruby-doc.org/stdlib-2.7.0/libdoc/io/console/rdoc/index.html)|将控制台功能添加到IO实例
[io/nonblock](https://ruby-doc.org/stdlib-2.7.0/libdoc/io/nonblock/rdoc/index.html)|
[io/wait](https://ruby-doc.org/stdlib-2.7.0/libdoc/io/wait/rdoc/index.html)|提供负责等待IO输入支持
[ipaddr](https://ruby-doc.org/stdlib-2.7.0/libdoc/ipaddr/rdoc/IPAddr.html)|提供处理IP地址(IPv4和IPv6均受支持)服务
[irb](https://ruby-doc.org/stdlib-2.7.0/libdoc/irb/rdoc/index.html)|提供交互式执行从标准输入中读取的Ruby表达式服务
[json](https://ruby-doc.org/stdlib-2.7.0/libdoc/json/rdoc/index.html)|提供ruby中关于json格式的实现
[logger](https://ruby-doc.org/stdlib-2.7.0/libdoc/logger/rdoc/index.html)|提供日志打印服务
[<del>mathn</del>](https://ruby-doc.org/stdlib-2.7.0/libdoc/mathn/rdoc/index.html)|<del>提供更加精确的数学运算(2.2.0起弃用)</del>
[matrix](https://ruby-doc.org/stdlib-2.7.0/libdoc/matrix/rdoc/index.html)|提供算术和代数运算以及确定其数学属性等服务
[mkmf](https://ruby-doc.org/stdlib-2.7.0/libdoc/mkmf/rdoc/index.html)|提供扩展库制作支持
[***monitor***](https://ruby-doc.org/stdlib-2.7.0/libdoc/monitor/rdoc/index.html)|提供多线程监控(同步)服务
[***mutex_m***](https://ruby-doc.org/stdlib-2.7.0/libdoc/mutex_m/rdoc/index.html)|提供互斥锁支持
[net/ftp](https://ruby-doc.org/stdlib-2.7.0/libdoc/net/ftp/rdoc/index.html)|对于文件传输协议的实现
[net/http](https://ruby-doc.org/stdlib-2.7.0/libdoc/net/http/rdoc/index.html)|提供构建HTTP用户代理服务
[net/imap](https://ruby-doc.org/stdlib-2.7.0/libdoc/net/imap/rdoc/index.html)|对于消息访问协议(IMAP)的实现
[net/pop](https://github.com/ruby/net-pop)|提供通过POP3(Post Office Protocol version 3)检索电子邮件的支持
[net/smtp](https://github.com/ruby/net-smtp)|提供通过SMTP(Simple Mail Transfer Protocol)发送邮件的支持
[***net/telnet***](https://github.com/ruby/net-telnet)|提供telnet客户端功能
[nkf](https://ruby-doc.org/stdlib-2.7.0/libdoc/nkf/rdoc/index.html)|
[objspace](https://ruby-doc.org/stdlib-2.7.0/libdoc/objspace/rdoc/index.html)|
[***observer***](https://ruby-doc.org/stdlib-2.7.0/libdoc/observer/rdoc/index.html)|提供观察者模式支持
[***open-uri***](https://ruby-doc.org/stdlib-2.7.0/libdoc/open-uri/rdoc/index.html)|针对Net :: HTTP,Net :: HTTPS和Net :: FTP的封装,<br>提供通过类似打开文件方式访问链接的支持
[open3](https://github.com/ruby/open3)|提供在运行其他程序时访问stdin,stdout和stderr的支持
[***openssl***](https://ruby-doc.org/stdlib-2.7.0/libdoc/openssl/rdoc/OpenSSL.html)|提供SSL,TLS和通用加密服务
[optparse](https://ruby-doc.org/stdlib-2.7.0/libdoc/optparse/rdoc/index.html)|提供一个用于命令行选项分析的类.<br>它比GetoptLong更高级,但也更易于使用,并且是更加面向Ruby的解决方案
[***ostruct***](https://github.com/ruby/ostruct)|提供类似于Hash的数据结构,并允许定义任意属性及其伴随的值
[***pathname***](https://ruby-doc.org/stdlib-2.7.0/libdoc/pathname/rdoc/index.html)|致力于提供比Ruby标准提供的更简洁的方式处理文件路径信息
[prettyprint](https://ruby-doc.org/stdlib-2.7.0/libdoc/prettyprint/rdoc/PrettyPrint.html)|提供漂亮的打印算法服务
[prime](https://github.com/ruby/prime)|提供所有素数的集合(可枚举)服务
[profile](https://github.com/ruby/profile)|提供了对程序进行性能分析服务
[profiler](https://ruby-doc.org/stdlib-2.7.0/libdoc/profiler/rdoc/index.html)|
[pstore](https://ruby-doc.org/stdlib-2.7.0/libdoc/pstore/rdoc/index.html)|提供基于哈希实现基于文件的持久性机制
[psych](https://github.com/ruby/psych)|提供YAML解析服务
[pty](https://ruby-doc.org/stdlib-2.7.0/libdoc/pty/rdoc/index.html)|提供伪终端(Pseudo tTY)支持
[racc](https://github.com/ruby/racc)|提供对LALR(1)语法分析器的支持
[racc/parser](https://ruby-doc.org/stdlib-2.7.0/libdoc/racc/parser/rdoc/index.html)|
[***rake***](https://ruby-doc.org/stdlib-2.7.0/libdoc/rake/rdoc/index.html)|提供代码构建服务
[***rdoc***](https://ruby.github.io/rdoc/)|提供生成HTML和命令行文档服务
[readline](https://ruby-doc.org/stdlib-2.7.0/libdoc/readline/rdoc/index.html)|提供从Ruby解释器访问输入历史记录服务
[resolv](https://ruby-doc.org/stdlib-2.7.0/libdoc/resolv/rdoc/index.html)|提供通过直接使用DNS模块查找各种DNS资源服务
[resolv-replace](https://ruby-doc.org/stdlib-2.7.0/libdoc/resolv-replace/rdoc/index.html)|
[***rexml***](https://github.com/ruby/rexml)|提供XML解析服务
[***rinda***](https://ruby-doc.org/stdlib-2.7.0/libdoc/rinda/rdoc/index.html)|提供创建模块化和分布式协作服务支持
[ripper](https://ruby-doc.org/stdlib-2.7.0/libdoc/ripper/rdoc/index.html)|提供Ruby脚本解析服务
[rss](https://github.com/ruby/rss)|提供读取和创建来自特定Web服务的更新的支持
[rubygems](https://github.com/rubygems/rubygems)|提供Ruby的软件包管理服务
[scanf](https://ruby-doc.org/stdlib-2.7.0/libdoc/scanf/rdoc/index.html)|
[sdbm](https://ruby-doc.org/stdlib-2.7.0/libdoc/sdbm/rdoc/index.html)|
[***securerandom***](https://ruby-doc.org/stdlib-2.7.0/libdoc/securerandom/rdoc/index.html)|提供安全的随机数生成服务
[***set***](https://ruby-doc.org/stdlib-2.7.0/libdoc/set/rdoc/Set.html)|提供了Array直观的互操作功能和Hash快速查找相结合且无重复项的数据结构
[shell](https://github.com/ruby/shell)|提供了常见的UNIX Shell命令实现了惯用的Ruby接口服务
[shellwords](https://ruby-doc.org/stdlib-2.7.0/libdoc/shellwords/rdoc/index.html)|提供根据UNIX Bourne shell的单词分析规则处理字符串服务
[singleton](https://github.com/ruby/singleton)|提供单例模式服务
[socket](https://ruby-doc.org/stdlib-2.7.0/libdoc/socket/rdoc/index.html)|提供对基础操作系统套接字实现的访问服务
[stringio](https://ruby-doc.org/stdlib-2.7.0/libdoc/stringio/rdoc/index.html)|提供用于模拟$stdio或$stderr的支持
[strscan](https://ruby-doc.org/stdlib-2.7.0/libdoc/strscan/rdoc/index.html)|
[***sync***](https://github.com/ruby/sync)|提供带有计数器的两相锁定的服务
[syslog](https://ruby-doc.org/stdlib-2.7.0/libdoc/syslog/rdoc/index.html)|旨在提供允许安全防篡改日志记录服务
[***tempfile***](https://ruby-doc.org/stdlib-2.7.0/libdoc/tempfile/rdoc/index.html)|提供管理临时文件服务
[thwait](https://ruby-doc.org/stdlib-2.7.0/libdoc/thwait/rdoc/index.html)|提供监视多线程的终止服务
[time](https://ruby-doc.org/stdlib-2.7.0/libdoc/time/rdoc/index.html)|提供日期和时间的抽象服务
[***timeout***](https://ruby-doc.org/stdlib-2.7.0/libdoc/timeout/rdoc/index.html)|提供了一种自动终止对于可能长时间运行程序的支持
[tmpdir](https://ruby-doc.org/stdlib-2.7.0/libdoc/tmpdir/rdoc/index.html)|
[tracer](https://ruby-doc.org/stdlib-2.7.0/libdoc/tracer/rdoc/index.html)|提供输出Ruby程序的源代码级执行跟踪服务
[tsort](https://ruby-doc.org/stdlib-2.7.0/libdoc/tsort/rdoc/index.html)|使用Tarjan算法对强连接的组件实现拓扑排序
[un](https://ruby-doc.org/stdlib-2.7.0/libdoc/un/rdoc/index.html)|提供类似Unix命令的文件操作utility服务
[unicode_normalize](https://ruby-doc.org/stdlib-2.7.0/libdoc/unicode_normalize/rdoc/index.html)|
[uri](https://ruby-doc.org/stdlib-2.7.0/libdoc/uri/rdoc/index.html)|提供处理统一资源标识符服务
[weakref](https://ruby-doc.org/stdlib-2.7.0/libdoc/weakref/rdoc/index.html)|允许对引用的对象进行垃圾回收
[webrick](https://github.com/ruby/webrick)|提供代理服务器和虚拟主机服务器服务
[win32ole](https://ruby-doc.org/stdlib-2.7.0/libdoc/win32ole/rdoc/index.html)|提供对访问VBScript之类的OLE服务器的支持
[yaml](https://ruby-doc.org/stdlib-2.7.0/libdoc/yaml/rdoc/index.html)|提供对YAML格式文件的支持
[zlib](https://ruby-doc.org/stdlib-2.7.0/libdoc/zlib/rdoc/index.html)|提供对zlib库访问的支持

# Abbrev

> 计算给定字符串集的明确缩写集

## 使用

### 基础用法

```ruby
Abbrev.abbrev(['ruby'])
# =>  {"ruby"=>"ruby", "rub"=>"ruby", "ru"=>"ruby", "r"=>"ruby"}
```

### 匹配模式

> 可选的pattern参数是正则或字符串.<br>
输出哈希中仅包含与模式匹配或以字符串开头的输入字符串。

```ruby
Abbrev.abbrev(%w{car box cone crab}, /b/)
# => {"box"=>"box", "bo"=>"box", "b"=>"box", "crab" => "crab"}

Abbrev.abbrev(%w{car box cone}, 'ca')
# => {"car"=>"car", "ca"=>"car"}
```

### 数组扩展

> 它还提供了数组核心扩展Array#abbrev

```ruby
%w{ summer winter }.abbrev
# => {"summer"=>"summer",
#  "summe"=>"summer",
#  "summ"=>"summer",
#  "sum"=>"summer",
#  "su"=>"summer",
#  "s"=>"summer",
#  "winter"=>"winter",
#  "winte"=>"winter",
#  "wint"=>"winter",
#  "win"=>"winter",
#  "wi"=>"winter",
#  "w"=>"winter"}
```

# Ostruct

> OpenStruct是类似于Hash的数据结构，它允许定义任意属性及其伴随的值。

## 使用

### 赋值示例-1

```ruby
person = OpenStruct.new
person.name = "John Smith"
person.age  = 70

person.name         # => "John Smith"
person[:name]       # => "John Smith"
person['name']      # => "John Smith"
person.age          # => 70
person.address      # => nil
```

### 赋值示例-2

```ruby
australia = OpenStruct.new(:country => "Australia", :capital => "Canberra")
australia.country # => "Australia"
```

### 特殊字符作为键

```ruby
message = OpenStruct.new
message.queued? = true
# SyntaxError: unexpected '='
# message.queued? = true
#                  ^

message = OpenStruct.new(:queued? => true)
message.queued?                           # => true
message.send("queued?=", false)
message.queued?                           # => false
```

### 键名包含空格

```ruby
measurements = OpenStruct.new("length (in inches)" => 24)
measurements.send("length (in inches)")        # => 24
measurements.send("length (in inches)=", 25)   # => 25
```

### 删除属性

> 删除属性的存在执行delete_field方法，因为将属性值设置为nil不会删除该属性.

```ruby
first_pet  = OpenStruct.new(:name => "Rowdy", :owner => "John Smith")
second_pet = OpenStruct.new(:name => "Rowdy")

first_pet.owner = nil
first_pet                 # => #<OpenStruct name="Rowdy", owner=nil>
first_pet == second_pet   # => false

first_pet.delete_field(:owner)
first_pet                 # => #<OpenStruct name="Rowdy">
first_pet == second_pet   # => true
```

### 嵌套使用

> 调用dig提取由名称对象序列指定的嵌套值，如果中间步骤为nil，则返回nil。

```ruby
address = OpenStruct.new("city" => "Anytown NC", "zip" => 12345)
person  = OpenStruct.new("name" => "John Smith", "address" => address)

person.dig(:address, "zip")            # => 12345
person.dig(:business_address, "zip")   # => nil

area = OpenStruct.new("city" => "Anytown NC")
address = OpenStruct.new("area" => area, "zip" => 12345)
person  = OpenStruct.new("name" => "John Smith", "address" => address)

person.dig(:address).dig(:area, 'city') # => "Anytown NC"
person.address.area.city                # => "Anytown NC"
```

### 转为标准哈希

```ruby
data = OpenStruct.new("country" => "Australia", :capital => "Canberra")
data.to_h   # => {:country => "Australia", :capital => "Canberra" }
```

### 遍历

```ruby
data = OpenStruct.new("country" => "Australia", :capital => "Canberra")
data.each_pair.to_a   # => [[:country, "Australia"], [:capital, "Canberra"]]

data.each_pair{|k,v| p "#{k}--#{v}"} 
# "country--Australia"
# "capital--Canberra"
```

### 多层嵌套转换

```ruby
hash = {:a=>1,:b=>[{:c=>2,:d=>[{:e=>3,:f=>4},{:e=>5,:f=>6}]},{:c=>4,:d=>[{:e=>7,:f=>8},{:e=>9,:f=>10}]},{:c=>6,:d=>[{:e=>11,:f=>12},{:e=>13,:f=>14}]}]}

json = hash.to_json

object = JSON.parse(json, object_class: OpenStruct)
object.b[0].c # => 2
```
# SecureRandom

> 确保随机数生成器安全的接口，该生成器适用于在HTTP cookie等中生成会话密钥.

## 使用

### 生成随机的十六进制字符串

```ruby
SecureRandom.hex(10) #=> "52750b30ffbc7de3b362"
```

### 生成随机的base64字符串

```ruby
SecureRandom.base64(10) #=> "EcmTPZwWRAozdA=="
```

### 适用于URL的base64字符串

```ruby
SecureRandom.urlsafe_base64(10)
# => "EJW1E4IKZK5TdQ"
```

### 生成随机二进制字符串

```ruby
SecureRandom.random_bytes(10)
```

### 生成字母数字字符串

```ruby
SecureRandom.alphanumeric(10)
```

### 生成UUID

```ruby
SecureRandom.uuid
```

# FileUtils

## 新建目录(多层级)

```ruby
dir = 'config/rsa'
FileUtils.mkdir_p(dir) unless Dir.exist?(dir)
```


```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```
### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```
### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```
### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```
### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```

### 

```ruby

```
# 参考资料

> [ruby-doc | 2.7.0](https://ruby-doc.org/core-2.7.0/)

> [程序园 | Ruby 标准库 abbrev](http://www.voidcn.com/article/p-uvefojrp-a.html)

> [腾讯云 | BigDecimal](https://cloud.tencent.com/developer/section/1375549)

> [腾讯云 | CMath](https://cloud.tencent.com/developer/section/1377889)

> [博客园 | 功能覆盖率](https://www.cnblogs.com/dpc525/p/5401504.html)

> [Github | Ruby digest](https://icbd.github.io/wiki/ruby/2018/07/04/ruby-digest.html)

> [Nithin Bekal | 分布式Ruby(DRb)入门](https://nithinbekal.com/posts/distributed-ruby/)

> [腾讯云 | Etc](https://cloud.tencent.com/developer/section/1376739)

> [CSDN | Ruby标准库一览](https://blog.csdn.net/winteen/article/details/83355581)

> [ruby-doc | OpenStruct](https://ruby-doc.org/stdlib-2.7.0/libdoc/ostruct/rdoc/OpenStruct.html)

> [CSDN | Ruby on Rails 查看文件夹是否存在，不存在则新建路径（多重路径）](https://blog.csdn.net/willyang519/article/details/27112879)

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()

> []()


