> Excel表格的读取和导出

<!-- TOC -->

- [使用](#使用)
  - [设置字符编码](#设置字符编码)
  - [读取](#读取)
    - [打开文件](#打开文件)
    - [查看所有工作表](#查看所有工作表)
    - [获取某个工作表](#获取某个工作表)
  - [新建](#新建)
  - [编辑已有文件](#编辑已有文件)
- [常见问题](#常见问题)
  - [Ole::Storage::FormatError: OLE2 signature is invalid](#olestorageformaterror-ole2-signature-is-invalid)

<!-- /TOC -->

# 使用

## 设置字符编码

```ruby
Spreadsheet.client_encoding = 'UTF-8'
```

## 读取

### 打开文件

```ruby
book = Spreadsheet.open '/path/to/an/excel-file.xls'
```

### 查看所有工作表

```ruby
book.worksheets
```

### 获取某个工作表

```ruby
sheet1 = book.worksheet 下标
sheet2 = book.worksheet 表名
```

## 新建

```ruby

```

## 编辑已有文件

> 尽管从理论上讲是可行的,但不建议将生成的文档写回到读取该文档的相同文件/IO中.

```ruby
book = Spreadsheet.open '/path/to/an/excel-file.xls'
sheet = book.worksheet 0
sheet.each do |row|
  row[0] *= 2
end
book.write '/path/to/output/excel-file.xls'
```

# 常见问题

## Ole::Storage::FormatError: OLE2 signature is invalid

> excel文件保存格式问题,保存为 `97-2003(*xls)`格式即可.

> [GitHub | Getting Started with Spreadsheet](https://github.com/zdavatz/spreadsheet/blob/master/GUIDE.md)