> SOAP

SOAP是简单对象访问协议(Simple Object Access Protocol)的首字母缩写,但这种基于标准的Web服务访问协议绝非简单.

SOAP API主要使用XML,其中每个输入参数都被定义并绑定到一个类型,以发出请求和接收响应.

> WSDL

WSDL(Web服务描述语言,Web Services Description Language)是为描述Web服务发布的XML格式.

# 简介

# 创建连接

```ruby
client = Savon.client(wsdl: '服务端地址?wsdl')
```

## 查看可用方法

```ruby
client.operations
```

## 发送请求

```ruby
response = client.call(:find_user, message: { id: 42 })
```
## 获取服务端返回结果

```ruby
response.body
```

## Savon::SOAPFault: (soap:Server) The given SOAPAction * does not match an operation

> 解决方案

添加: headers: {'SOAPAction' => ''}

```ruby
client = Savon.client(wsdl: 'request_url?WSDL', headers: { 'SOAPAction' => ''})
```

# 参考资料


> [GitHub | fagiani](https://fagiani.github.io/savon/)

> [GitHub | SOAPAction http header causes:: The given SOAPAction {{HEADER}} does not match an operation.](https://github.com/claudemamo/jruby-cxf/issues/2)