<!-- TOC -->

- [准备](#准备)
- [约定](#约定)
  - [数据和记录](#数据和记录)
- [使用](#使用)
  - [常用方法](#常用方法)
    - [count](#count)
    - [action](#action)
    - [audited_changes](#audited_changes)
    - [revisions](#revisions)
    - [revision(index)](#revisionindex)
    - [revision_at](#revision_at)
  - [指定列](#指定列)
  - [指定回调](#指定回调)
  - [定制的记录信息详细内容](#定制的记录信息详细内容)
    - [comment_required](#comment_required)
  - [记录数量限制](#记录数量限制)
  - [记录所属用户](#记录所属用户)
    - [current_user](#current_user)
    - [current_user_method](#current_user_method)
  - [关联记录](#关联记录)
    - [associated_with&has_associated_audits](#associated_withhas_associated_audits)
    - [associated&associated_audits](#associatedassociated_audits)
  - [own_and_associated_audits](#own_and_associated_audits)
  - [禁用记录](#禁用记录)
- [Audited扩展](#audited扩展)
  - [Audited::Audit](#auditedaudit)
- [常见问题](#常见问题)
  - [undefined method `audited'](#undefined-method-audited)
- [参考资料](#参考资料)

<!-- /TOC -->

# 准备

> Gemfile添加

```ruby
gem "audited", "~> 版本号"
```
> 添加audits表

```bash
rails generate audited:install
rake db:migrate
```

> model添加

在需要使用audits的模型中添加 `audited`.如:

```ruby
class User < ActiveRecord::Base
  audited
end
```

# 约定

## 数据和记录

下文中提及的数据是指某张表中的一条数据.<br>
记录指该数据变动触发audited生成的关于该条数据的历史样貌.


# 使用

## 常用方法

方法名|作用
-|-
count|返回该条数据的记录总条数
action|展示触发该条记录的动作
audited_changes|展示某条记录的具体改动内容
revisions|展示该数据的所有历史版本
revision(index)|根据索引查找具体某条历史版本
revision_at|通过指定具体的日期来查看当天该条数据所有的记录


```ruby
user = User.create!(name: "Steve")
```

### count

count方法返回该条数据的记录总条数.

默认情况下,无论何时创建,更新或删除使用了audited的表中的数据都会添加一条记录.如:

```ruby
user.audits.count # => 1
```

### action

action方法将展示触发该条记录的动作.

```ruby
user.update_attributes!(name: "Ryan")
user.audits.count # => 2

audit = user.audits.last
audit.action # => "update"
```

### audited_changes

audited_changes方法将展示该条数据 *有记录的* 所有具体改动内容.

```ruby
audit.audited_changes # => {"name"=>["Steve", "Ryan"]}
```

### revisions

revisions方法将展示该条数据的 *有记录的* 所有先前版本

```ruby
user.revisions
```

### revision(index)

可以通过指定索引来查看 *有记录的* 具体某条先前版本.

```ruby
user.revision(1)
```

### revision_at

revision_at方法通过指定具体的日期来查看当天该条数据所有的记录.

```ruby
user.revision_at(Date .parse('2016-01-01'))
```

## 指定列

前面提到,默认情况下,任何属性更改都会创建一条记录.<br>
可以通过限定字段来改变创建记录的时机.

***如果指定列发生变化对于已存在记录的数据记录字段还是根据原始记录字段去记录变化.***

```ruby
class User < ActiveRecord::Base
  # 全部字段
  audited

  # 指定单个字段
  audited only: :name

  # 指定多个字段
  audited only: [:name, :address]

  # 除某些字段外的其他字段
  audited except: :password
end
```
## 指定回调

除了可以限制创建记录的字段,还可以限定具体的某些回调,而非所有回调都去创建记录.

```ruby
class User < ActiveRecord::Base
  # 只有在改动name属性或者更新和删除该数据时创建记录
  audited only: :name, on: [:update, :destroy]
end
```

## 定制的记录信息详细内容

可以使用audit_comment属性来定制记录的详细内容.

```ruby
new_name = 'Mike'
user.update_attributes!(name: new_name, audit_comment: "名字由#{self.name},更改为:#{new_name}.")
user.audits.last.comment # => "名字由Ryan,更改为:Mike."

```

### comment_required

comment_required选项用来限制是否每次记录都必须包含详细内容即:audit_comment.

```ruby
class  User <ActiveRecord :: Base 
  audited :comment_required => true 
end
```

## 记录数量限制

如果具体业务中保存每条数据的所有记录不太现实,甚至是在限制具体字段的前提下仍会产生大量记录数据.<br>
或者说没有必要保留历史记录,只关心数据近期的变化.<br>
可以限定每条数据所有记录的保存条数.<br>

通过两种方式可以实现:

> 项目初始化(initializer)时

```ruby
Audited.max_audits = 10 # 只保留最后的10条记录
```

缺点在于全局配置,如果某个模型想要增加保存记录数量,不可个性化配置.

> 模型中单独配置

```ruby
class User < ActiveRecord::Base
  audited max_audits: 2
end
```

## 记录所属用户

又或者说是谁改动这某条数据.

### current_user

如果项目中有使用current_user来保存当前登录的用户,那么这将变得很简单.<br>
因为audited默认通过current_user方法来查找当前登录的用户,来记录改动数据的操作者.

```ruby
current_user #=>#<User name:"Mike"> 
user.update_attributes!(name: "Avicii")
user.audits.last.user  #=>#<User name: "Mike"> 
```

### current_user_method

可以在项目初始化(initializer)时通过在配置current_user_method,来告诉audited寻找当前登录用户的具体方法

```ruby
Audited.current_user_method = :authenticated_user
```

## 关联记录

现有模型关系如下:

```ruby
class User < ActiveRecord::Base
  belongs_to :company
  audited
end

class Company < ActiveRecord::Base
  has_many :users
end
```

如果想要查看某公司下用户数据的变更记录该怎么查询?

### associated_with&has_associated_audits

可以通过associated_with选项来解决这一问题.

```ruby
class User < ActiveRecord::Base
  belongs_to :company
  audited associated_with: :company
end

class Company < ActiveRecord::Base
  has_many :users
  has_associated_audits
end
```

### associated&associated_audits

TODO: 实际操作确定这里的user是指操作的对象,还是执行操作的用户.

```ruby

```

## own_and_associated_audits

同上

## 禁用记录

如果某些操作符合记录条件但又不想被记录,可以使用save_without_auditing方法可以跳过记录.

```ruby
user.save_without_auditing
```

或者采用块方法

```ruby
user.without_auditing do
  user.save
end
```

# Audited扩展

## Audited::Audit

通过继承Audited::Audit类,可以扩展或重写原有方法

```ruby
class  CustomAudit < Audited :: Audit
  def  some_custom_behavior 
   'Hiya!'
  end 
end
```

并且将其加入初始化

```ruby
# config/initializers/audited.rb
Audited.config do |config|
  config.audit_class = CustomAudit
end
```

# 常见问题

## undefined method `audited'

```ruby
.rvm/gems/ruby-2.3.5/gems/activerecord-4.1.8/lib/active_record/dynamic_matchers.rb:26:in `method_missing': undefined method `audited' for #<Class:0x000055792de804f8> (NoMethodError)
```

在用到的model中require一下:
```ruby
class Quote < ActiveRecord::Base
  require "audited"
  ...
```

# 参考资料

> [Github | audited](https://github.com/collectiveidea/audited)

> [Github | When running rspec I get this error: Undefined local variable or method `audited'](https://github.com/collectiveidea/audited/issues/98)