<!-- TOC -->

- [解析HTML/XML文档](#解析htmlxml文档)
  - [解析途径](#解析途径)
    - [字符串](#字符串)
    - [文件](#文件)
    - [互联网](#互联网)
- [参考资料](#参考资料)

<!-- /TOC -->

# 解析HTML/XML文档

## 解析途径

### 字符串

```ruby
html_doc = Nokogiri::HTML("<html><body><h1>Mr. Belvedere Fan Club</h1></body></html>")
xml_doc  = Nokogiri::XML("<root><aliens><alien><name>Alf</name></alien></aliens></root>")
```

### 文件

```ruby
doc = File.open("blossom.xml") { |f| Nokogiri::XML(f) }
```

### 互联网

```ruby
require 'open-uri'
doc = Nokogiri::HTML(open("http://www.threescompany.com/"))
```



# 参考资料

> [官网](https://nokogiri.org/index.html)