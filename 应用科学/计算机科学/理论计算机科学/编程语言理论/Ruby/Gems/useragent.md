
<!-- TOC -->

- [Gemfile](#gemfile)
- [使用](#使用)
  - [获取浏览器类型](#获取浏览器类型)
  - [使用UserAgent 对user_agent进行解析](#使用useragent-对user_agent进行解析)
- [参考资料](#参考资料)

<!-- /TOC -->

# Gemfile

```ruby
gem 'useragent'
```

# 使用

## 获取浏览器类型

```ruby
user_agent = request.user_agent
# 或
user_agent = request.env['HTTP_USER_AGENT']

p user_agent 
# => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.3 Safari/537.36"
```

## 使用UserAgent 对user_agent进行解析

```ruby
user_agent_parsed = UserAgent.parse(user_agent)
p user_agent_parsed.platform #=> 'Macintosh'
p user_agent_parsed.browser # => 'Chrome'
```

# 参考资料

> [Rails根据user_agent判断请求端](http://www.alonely.com.cn/Ruby/20160826/18403.html)

> [Github | useragent](https://github.com/gshutler/useragent)