#!/usr/bin/env ruby

require 'optparse'
require 'ostruct'
require 'json'
require 'rest-client'
require 'nokogiri'
# require 'pry'
# require 'pry-rescue'
# require 'pry-byebug'

class Array
  def to_table
    column_sizes = self.reduce([]) do |lengths, row|
      row.each_with_index.map{|iterand, index| [lengths[index] || 0, iterand.to_s.length].max}
    end
    puts head = '-' * (column_sizes.inject(&:+) + (3 * column_sizes.count) + 1)
    self.each do |row|
      row = row.fill(nil, row.size..(column_sizes.size - 1))
      row = row.each_with_index.map{|v, i| v = v.to_s + ' ' * (column_sizes[i] - v.to_s.length)}
      puts '| ' + row.join(' | ') + ' |'
    end
    puts head
  end
end

class Application

  def log(content)
    print "#{Time.now} #{content}\n"
  end

  def request(method: :get, url:, headers:{})
    (@result ||= []) << url
    log("\n\n请求链接: #{url}")
    res = RestClient::Request.execute(method: method, url: url, headers: headers)
    log("返回状态码: #{res.code}")
    @result << res.code
    unless res.code == 200
      return [false, '状态码异常']
    end

    [true, res.body]
  end

  def analyze(body:)
    log("开始解析内容")
    doc = Nokogiri::HTML.parse(body)
    error_flag = false
    begin
      yield doc if block_given?
    rescue => e
      error_flag = true
      @result << e.message
    end
    log("完成解析")
    @result << 'OK' unless error_flag
    return doc unless block_given?
  end

  def save_data(name:, tags: [], type:, attributes: {}, created_at: Time.now)
    file = File.open('./outputs/standard_format_data.txt', 'a')
    item = JSON.generate({'名称': name, '标签': tags, '类型': type, '属性': attributes, '创建时间': created_at.strftime("%Y-%m-%dT%H:%M:%S%::z")})
    log("写入标准数据: #{item}")
    file.write(item+"\n")
    file.close
  end

  def run(urls: nil, url: nil, sleep: 20, file_name: , file_mode: 'w')
    urls = [urls, url].flatten.compact
    file = File.open("./outputs/#{file_name}", file_mode)
    @results = [['Start At', 'End At','Request Address', 'Response Code', 'Analyze Status']]
    begin
      urls.each_with_index do |url, index|
        @result = [Time.now]
        res = request(url: url)
        next unless res[0]
        (analyze(body:res[1]){|doc| yield doc, file}) if block_given?
        @result.insert(1, Time.now)
        @results << @result
        sleep sleep unless (index+1 == urls.size)
      end
    rescue => e
      # p {errors: {info: $!.to_s, path: $@[0..10]}}
      p e
    end
    file.close

    print "\e[2J\e[f"
    @results.to_table
  end

end