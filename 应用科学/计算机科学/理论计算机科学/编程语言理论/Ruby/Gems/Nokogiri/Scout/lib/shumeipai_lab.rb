#!/usr/bin/env ruby

class ShuMeiPaiLab < Application

  def list_articles
    urls = (1..72).to_a.map{|page| "https://shumeipai.nxez.com/page/#{page}"}

    run(urls: urls, file_name: '树莓派实验室-列表文章.md') do |doc, file|
      doc.xpath("//*[@id='main-content']")[0].children.each do |div|
        name = div.css('h3').first.children[-2].children.first.text.gsub(/\t|\n/, '')
        link = div.css('h3').first.children[1].attributes['href'].value
        file.write(">[#{name}](#{link})\n\n")
        save_data(name: name, tags: ["计算机硬件","树莓派","树莓派实验室"], attributes: {'链接': link}, type: "链接/文章")
      end
    end

  end

  def exposed_api
    {"[IT技术]列表文章": lambda{|*args| list_articles }}
  end

end