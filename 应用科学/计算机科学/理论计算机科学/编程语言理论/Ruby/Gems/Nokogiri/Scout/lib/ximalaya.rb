#!/usr/bin/env ruby

class XiMaLaYa < Application

  def analyze_list_articles(li:, doc:, file:, tags: [])
    name = li.css('a')[0].attributes['title'].value
    name = name.gsub(/^\d{1,}\./, '')
    link = li.css('a')[0].attributes['href'].value
    full_link = 'https://www.ximalaya.com'+link
    updated_at = li.children[-1].children[-1].children[-1].text
    content = ">[#{name} | #{updated_at}](#{full_link})"
    file.write("#{content}\n\n")
    save_data(name: name, tags: tags, attributes: {'链接': full_link}, type: "链接/文章")
  end

  def fun_index_fund
    urls = (1..9).to_a.map{|page| "https://www.ximalaya.com/shangye/31885744/p#{page}"}
    run(urls: urls, file_name: '喜马拉雅-玩转指数基金.md') do |doc, file|
      doc.xpath("//*[@class='sound-list _is']")[0].css('li').each do |li|
        analyze_list_articles(li: li, doc: doc, file: file, tags: ["金融", "基金", "喜马拉雅", "雪球", "玩转指数基金"])
      end
    end
  end

  def exposed_api
    {"[专栏文章]玩转指数基金": lambda{|*args| fun_index_fund}}
  end

end