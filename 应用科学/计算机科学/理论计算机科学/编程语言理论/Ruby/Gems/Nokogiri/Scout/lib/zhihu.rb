#!/usr/bin/env ruby

class ZhiHu < Application

  def daily_articles
    headers = {
      "authority": "news-at.zhihu.com",
      "cache-control": "max-age=0",
      "sec-ch-ua": '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
      "sec-ch-ua-mobile": "?0",
      "upgrade-insecure-requests": "1",
      "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36",
      "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "sec-fetch-site": "cross-site",
      "sec-fetch-mode": "navigate",
      "sec-fetch-user": "?1",
      "sec-fetch-dest": "document",
      "accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
      "cookie": '_xsrf=0SNPD8EkRmaF7QhCoTwFklIVkjjySxTY; _zap=fcc019c5-23cf-4e88-9d28-c0ad8ef1eeb3; d_c0="AIAchcjMwRKPToTo6kbof6erwI6H9ngnjpk=|1615023338"; KLBRSID=b5ffb4aa1a842930a6f64d0a8f93e9bf|1615023374|1615023317',
      "if-none-match": "88d9362112ee48db6d4b700b4acd24d4cc2987a8"
    }

    (Date.parse('2014-07-25') .. Date.parse(Time.now.strftime("%Y-%m-%d"))).each do |date|
      url = "https://news-at.zhihu.com/api/3/news/before/#{date.to_s.gsub('-','')}"
      res = request(url: url, headers: headers)
      next unless res[0]

      JSON.parse(res[1])["stories"].each do |item|
        save_data(name: item['title'], tags: ["百科","知乎","知乎日报"], attributes: {'链接': item['url']}, type: "链接/文章", created_at: date.to_time)
      end

      sleep rand(10..30)
    end  

  end

  def exposed_api
    {"[百科]知乎日报": lambda{|*args| daily_articles }}
  end

end