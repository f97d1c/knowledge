#!/usr/bin/env ruby
require './lib/application.rb'
require './lib/ximalaya.rb'
require './lib/shumeipai_lab.rb'
require './lib/zhihu.rb'

Signal.trap("INT"){ 
  print "\e[2J\e[f"
  exit
}

def chose_options(title:, options:)
  puts "\n#{title}\n"

  options_indexs = options.each_with_index.map do |key, index| 
    [index, key].join('. ')
  end.join("\n")
  
  puts options_indexs
  origin_value = gets.chomp
  origin_value = -1 if (origin_value == '')
  chose_idnex = origin_value.to_i

  unless (0..(options.size-1)).include?(chose_idnex)
    puts "请输入有效序号"
    return chose_options(title: title, options: options)
  end 
  
  return [true, options[chose_idnex]]
end

exposed_objects = {
  '喜马拉雅': XiMaLaYa,
  '知乎': ZhiHu,
  '树莓派实验室': ShuMeiPaiLab,
}

res = chose_options(title: '请输入需要获取的对象', options: exposed_objects.keys)

object = exposed_objects[res[1]]
exposed_methods = object.new().exposed_api

res = chose_options(title: '请输入具体方法', options: exposed_methods.keys)

exposed_methods[res[1]].call()