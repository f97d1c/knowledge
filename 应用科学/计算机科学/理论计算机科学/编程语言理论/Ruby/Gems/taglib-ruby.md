> 用于读写多种音频格式的元数据(标签)

<!-- TOC -->

- [说明](#说明)
  - [ID3](#id3)
- [安装](#安装)
  - [相关依赖](#相关依赖)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## ID3

ID3是最常与MP3音频文件格式结合使用的元数据容器.<br>
它允许将诸如标题,艺术家,专辑,曲目号之类的信息以及有关文件的其他信息存储在文件本身中.

ID3有两个不相关的版本:ID3v1和ID3v2.<br>
ID3v1采用128字节的形式MP3文件末尾的一段,其中包含一组固定的数据字段.<br>
ID3v1.1是一个轻微的修改,它添加了一个 *track number* 字段,但以 *comment* 字段的略微缩短为代价.<br>
ID3v2在结构上与ID3v1有很大不同,ID3v2由位于文件开头的一组可扩展的 *帧* 组成,<br>
每个帧都有一个帧标识符（三字节或四字节字符串）和一个数据.<br>
ID3v2.4规范中声明了83种类型的帧,应用程序也可以定义自己的类型.<br>
有一些标准框架,用于包含封面,BPM,版权和许可,歌词,任意文本和URL数据以及其他内容.<br>
已经记录了三个版本的ID3v2,每个版本都扩展了框架定义.

ID3是MP3文件中元数据的事实上的标准.<br>
没有标准化机构参与其创建,也没有这样的组织获得正式的批准状态,在这个领域,它与APE标签竞争.

Lyrics3v1和Lyrics3v2是ID3v2之前实施的标签标准,用于向mp3文件添加歌词.<br>
与ID3v2的区别在于,Lyrics3始终位于ID3v1标记之前的MP3文件的末尾.

# 安装

## 相关依赖

```bash
# Ubuntu
sudo apt-get install libtag1-dev
gem install taglib-ruby
```

# 参考资料

> [robinst | taglib-ruby](https://robinst.github.io/taglib-ruby/)

> [Github | taglib-ruby](https://github.com/robinst/taglib-ruby)

> [维基百科 | ID3](https://en.wikipedia.org/wiki/ID3#ID3v2_frame_specification)
