<!-- TOC -->

- [安装](#安装)
  - [uninitialized constant SimpleForm (NameError)](#uninitialized-constant-simpleform-nameerror)
- [基本用法](#基本用法)
  - [常用标签](#常用标签)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装

```bash
# Gemfile添加
gem 'simple_form'

bundle install

rails generate simple_form:install
```

## uninitialized constant SimpleForm (NameError)

config/initializers/simple_form.rb中添加:

```ruby
require'simple_form'
```

# 基本用法

## 常用标签

```erb
<%
# 如果路由有层级,如: /group/1/post/1 
# 对simple_form传入顺序不能错:
# simple_form_for [@ group,@ post] do | f |
%>

<%= simple_form_for @user do |f| %>
  <%= f.input :username, label: 'Your username please', error: 'Username is mandatory, please specify one' %>
  <%= f.input :password, hint: 'No special characters.', required: true %>
  <%= f.input :email, placeholder: 'user@domain.com' %>
  <%= f.input :age, collection: 18..60, selected: 21 %>
  <%# 这时候会制造出两个radiobox, 标签是第二每个阵列的第二个, 实际的值则是第一个第一个 %>
  <%= f.input :gender, as: :radio_buttons, collection: [['0', 'female'], ['1', 'male']], label_method: :second, value_method: :first %>
  <%= f.input :realname, disabled: true, hint: 'You cannot change your realname.' %>
  <%= f.input :remember_me, inline_label: 'Yes, remember me' %>
  <%= f.button :submit, disable_with: "Submiting..." %>
<% end %>

```
选项|作用
-|-|
label|设定标题,默认的栏位名称
error|在model设定后validates后统一出现的错误信息
hint|在输入栏位下方的提示讯息
placeholder|输入提示信息
inline_label|
input_html|自定义css,如:<br>input_html:{class:'special'}
required|validates不一定要写在模型可以直接写在HTML里面
collection|制定填写内容限制范围<br>搭配两个参数客制化选择:<br>label_method: 显示标题<br>value_method: 实际得到的值
selected|设定默认值
disabled|标记为禁用状态
disable_with|制定提交过程中按钮不可点击状态下显示内容(阻止多次点击)

# 参考资料

> [Github | simple_form](https://github.com/plataformatec/simple_form)

> [Github | 各字段类型具体包含方法](https://github.com/plataformatec/simple_form#available-input-types-and-defaults-for-each-column-type)

> [解决 | uninitialized constant SimpleForm (NameError)](https://translate.googleusercontent.com/translate_c?anno=2&depth=1&hl=zh-CN&rurl=translate.google.com&sl=en&sp=nmt4&tl=zh-CN&u=http://stackoverflow.com/questions/13633195/loading-error-for-simple-form-in-rails-3-2-8-engine&xid=17259,15700021,15700124,15700186,15700191,15700201,15700248&usg=ALkJrhgS4YS2FcF-I-8UtcYnzRwGa95Hvg)


