> 权限验证工具

<!-- TOC -->

- [安装](#安装)
- [简单的示例](#简单的示例)
  - [Pundit的假设](#pundit的假设)
  - [ApplicationPolicy](#applicationpolicy)
  - [authorize](#authorize)
  - [Headless policies](#headless-policies)
  - [Scopes](#scopes)
  - [Pundit::NotAuthorizedError](#punditnotauthorizederror)
    - [挽救被拒绝的授权](#挽救被拒绝的授权)
    - [通过I18n创建自定义错误消息](#通过i18n创建自定义错误消息)
  - [Pundit用户](#pundit用户)
  - [命名空间](#命名空间)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装

> Gemgile

```ruby
gem "pundit"
```

> ApplicationController

```ruby
class ApplicationController < ActionController::Base
  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  # 会影响回显信息(flash)展示
  # protect_from_forgery
  def user_not_authorized
    flash[:alert] = "对不起,您没有权限进行此操作！"
    redirect_to(request.referrer || root_path)
  end
  #...
end
```

> generator

```bash
rails g pundit:install
```

生成app/policies/目录,权限校验相关文件存放在该目录下.

# 简单的示例

## Pundit的假设

```ruby
# app/policies/post_policy.rb
class PostPolicy
  attr_reader :user, :post

  def initialize(user, post)
    @user = user
    @post = post
  end

  def update?
    user.admin? or not post.published?
  end
end
```

Pundit对这个类做出以下假设:

0. 该类与某种模型类具有相同的名称,后缀仅为"Policy".
0. 第一个参数是用户,在控制器中,Pundit将调用current_user方法来检索要发送到此参数的内容.
0. 第二个参数是某种模型对象,是要检查其授权的对象.<br>
这不需要是ActiveRecord甚至是ActiveModel对象,它可以是任何真正的东西.
0. 在这种情况下,该类实现了某种查询方法update?.<br>
通常,这将映射到特定控制器操作的名称.

## ApplicationPolicy

通常,会设置自定义的基类:

```ruby
class PostPolicy < ApplicationPolicy
  def update?
    user.admin? or not record.published?
  end
end
```

## authorize

假设有一个类实例Post,Pundit现在可以在控制器中执行此操作:

```ruby
def update
  @post = Post.find(params[:id])
  authorize @post
  if @post.update(post_params)
    redirect_to @post
  else
    render :edit
  end
end
```

authorize方法自动推断出Post将具有匹配的 PostPolicy类,并实例化该类,交换当前用户和给定记录.<br>
然后从动作名称推断出它应该调用 update?此策略实例.<br>
在这种情况下,authorize会做这样的事情:

```ruby
unless PostPolicy.new(current_user, @post).update?
  raise Pundit::NotAuthorizedError, "not allowed to update? this #{@post.inspect}"
end
```

如果要检查的权限的名称与操作名称不匹配,则可以传递第二个参数.例如:

```ruby
def publish
  @post = Post.find(params[:id])
  authorize @post, :update?
  @post.publish!
  redirect_to @post
end
```

如有必要,可以传递参数以覆盖策略类.例如:

```ruby
def create
  @publication = find_publication 
  # @publication.class => Post
  authorize @publication, policy_class: PublicationPolicy
  @publication.publish!
  redirect_to @publication
end
```

如果没有第一个参数的实例authorize,那么可以传递该类.<br>
例如:

```ruby
class PostPolicy < ApplicationPolicy
  def admin_list?
    user.admin?
  end
end
```

控制器:

```ruby
def admin_list
  authorize Post
  ...
end

def show
  @user = authorize User.find(params[:id])
end
```

可以通过policy 视图和控制器中的方法轻松获取策略实例.<br>
这对于在视图中有条件地显示链接或按钮特别有用:

```erb
<% if policy(@post).update? %>
  <%= link_to "Edit post", edit_post_path(@post) %>
<% end %>
```

## Headless policies

鉴于存在没有相应模型/ ruby​​类的策略,可以通过传递符号来检索它:

```ruby
# app/policies/dashboard_policy.rb
class DashboardPolicy < Struct.new(:user, :dashboard)
  # ...
end
```

该策略仍需要接受两个参数.<br>
第二个参数将只是:dashboard这种情况下的符号,它是作为记录传递到authorize下面的内容.<br>


```ruby
# controllers
authorize :dashboard, :show?
```

```erb
<% if policy(:dashboard).show? %>
  <%= link_to 'Dashboard', dashboard_path %>
<% end %>
```

## Scopes

通常,需要具有某种特定用户可以访问的视图列表记录.<br>
使用Pundit时,需要定义一个policy scope类.<br>
它看起来像这样:

```ruby
class PostPolicy < ApplicationPolicy
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      if user.admin?
        scope.all
      else
        scope.where(published: true)
      end
    end
  end

  def update?
    user.admin? or not record.published?
  end
end
```

Pundit对这个类做出以下假设:

0. 该类具有名称Scope并嵌套在策略类下.
0. 第一个参数是用户.<br>
在的控制器中,Pundit将调用该 current_user方法来检索要发送到此参数的内容.
0. 第二个参数是某种类型的范围,可以执行某种查询.<br>
它通常是一个ActiveRecord类或者ActiveRecord::Relation,但它可能完全是另一 回事.
0. 该类的实例响应该方法resolve,该方法应返回可以迭代的某种结果.<br>
对于ActiveRecord类,这通常是一个ActiveRecord::Relation.

可以创建自己的基类来继承:

```ruby
class PostPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      else
        scope.where(published: true)
      end
    end
  end

  def update?
    user.admin? or not record.published?
  end
end
```

## Pundit::NotAuthorizedError

### 挽救被拒绝的授权

```ruby
class ApplicationController < ActionController::Base
  protect_from_forgery
  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action."
    redirect_to(request.referrer || root_path)
  end
end
```

或者,您可以通过让rails将它们作为403错误处理并提供403错误页面来全局处理Pundit :: NotAuthorizedError.<br>
将以下内容添加到application.rb:

```ruby
config.action_dispatch.rescue_responses["Pundit::NotAuthorizedError"] = :forbidden
```

### 通过I18n创建自定义错误消息 

```ruby
class ApplicationController < ActionController::Base
 rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

 private

 def user_not_authorized(exception)
   policy_name = exception.policy.class.to_s.underscore

   flash[:error] = t "#{policy_name}.#{exception.query}", scope: "pundit", default: :default
   redirect_to(request.referrer || root_path)
 end
end
```

```yml
en:
 pundit:
   default: 'You cannot perform this action.'
   post_policy:
     update?: 'You cannot edit this post!'
     create?: 'You cannot create posts!'
```

## Pundit用户

当current_user不是Pundit应该调用的方法时,只需在控制器中定义一个名为的方法pundit_user即可:

```ruby
def pundit_user
  User.find_by_other_means
end
```

## 命名空间

```ruby
authorize(post)                   # => 寻找 PostPolicy
authorize([:admin, post])         # => 寻找 Admin::PostPolicy
authorize([:foo, :bar, post])     # => 寻找 Foo::Bar::PostPolicy

policy_scope(Post)                # => 寻找 PostPolicy::Scope
policy_scope([:admin, Post])      # => 寻找 Admin::PostPolicy::Scope
policy_scope([:foo, :bar, Post])  # => 寻找 Foo::Bar::PostPolicy::Scope
```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```



```ruby

```




# 参考资料

> [Github | pundit](https://github.com/varvet/pundit)