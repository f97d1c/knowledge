> 分页

<!-- TOC -->

- [安装](#安装)
  - [Gemfile](#gemfile)
- [配置](#配置)
  - [生成配置文件](#生成配置文件)
  - [常规配置选项](#常规配置选项)
  - [I18n](#i18n)
  - [分页模板](#分页模板)
- [使用](#使用)
  - [views](#views)
  - [controllers](#controllers)
  - [常用方法](#常用方法)
- [常见问题](#常见问题)
  - [Engine-undefined method 'page' for...](#engine-undefined-method-page-for)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装

## Gemfile

```ruby
gem 'kaminari'
```

# 配置

## 生成配置文件 

```bash
rails g kaminari:config
```

## 常规配置选项

```ruby
default_per_page      # 25 by default
max_per_page          # nil by default
max_pages             # nil by default
window                # 4 by default
outer_window          # 0 by default
left                  # 0 by default
right                 # 0 by default
page_method_name      # :page by default
param_name            # :page by default
params_on_first_page  # false by default
```

## I18n

```yml
zh-CN:
  views:
      pagination:
        first: "&laquo; 首页"
        last: "末页 &raquo;"
        previous: "&lsaquo; 上一页"
        next: "下一页 &rsaquo;"
        truncate: "&hellip;"
```

## 分页模板

```bash
# 可选参数--views-prefix 文件夹名称 
rails g kaminari:views default
```

# 使用

## views

```bash
<%= paginate @users %>

# ajax链接
<%= paginate @users, remote: true %>
```

## controllers

```ruby
@users = User.order(:name).page(params[:page]) 
```
## 常用方法

> 分页从第1页开始,而不是在第0页<br>第(0)页将返回与第(1)页相同的结果

方法名|返回值
-|-
User.count|#=> 1000
User.page(1).limit_value|#=> 20
User.page(1).total_pages|#=> 50
User.page(1).current_page|#=> 1
User.page(1).next_page|#=> 2
User.page(2).prev_page|#=> 1
User.page(1).first_page?|#=> true
User.page(50).last_page?|#=> true
User.page(100).out_of_range?|#=> true

# 常见问题

## Engine-undefined method 'page' for... 

```ruby 
module MyEngine
  class Engine < ::Rails::Engine
    isolate_namespace MyEngine

    require 'kaminari'
  end
end
```

# 参考资料

> [Github | kaminari](https://github.com/kaminari/kaminari)

> [Github | undefined method 'page' for...](https://github.com/kaminari/kaminari/issues/213)