> 用于限制给定用户访问的资源

<!-- TOC -->

- [前言](#%E5%89%8D%E8%A8%80)
- [定义权限](#%E5%AE%9A%E4%B9%89%E6%9D%83%E9%99%90)
  - [Ability类](#ability%E7%B1%BB)
  - [尽可能使用哈希条件](#%E5%B0%BD%E5%8F%AF%E8%83%BD%E4%BD%BF%E7%94%A8%E5%93%88%E5%B8%8C%E6%9D%A1%E4%BB%B6)
  - [哈希条件是DRYer](#%E5%93%88%E5%B8%8C%E6%9D%A1%E4%BB%B6%E6%98%AFdryer)
  - [SQL中的哈希条件是OR,提供最大的灵活性](#sql%E4%B8%AD%E7%9A%84%E5%93%88%E5%B8%8C%E6%9D%A1%E4%BB%B6%E6%98%AFor%E6%8F%90%E4%BE%9B%E6%9C%80%E5%A4%A7%E7%9A%84%E7%81%B5%E6%B4%BB%E6%80%A7)
  - [拆分ability.rb文件](#%E6%8B%86%E5%88%86abilityrb%E6%96%87%E4%BB%B6)
    - [ModelAbility文件](#modelability%E6%96%87%E4%BB%B6)
    - [覆盖控制器中的current_ability方法](#%E8%A6%86%E7%9B%96%E6%8E%A7%E5%88%B6%E5%99%A8%E4%B8%AD%E7%9A%84currentability%E6%96%B9%E6%B3%95)
- [校验权限](#%E6%A0%A1%E9%AA%8C%E6%9D%83%E9%99%90)
  - [can方法](#can%E6%96%B9%E6%B3%95)
    - [哈希的条件](#%E5%93%88%E5%B8%8C%E7%9A%84%E6%9D%A1%E4%BB%B6)
  - [类校验](#%E7%B1%BB%E6%A0%A1%E9%AA%8C)
  - [控制器权限](#%E6%8E%A7%E5%88%B6%E5%99%A8%E6%9D%83%E9%99%90)
    - [authorize!](#authorize)
    - [load_and_authorize_resource](#loadandauthorizeresource)
      - [index action](#index-action)
      - [show, edit, update and destroy actions](#show-edit-update-and-destroy-actions)
      - [new and create actions](#new-and-create-actions)
      - [明确应加载的model](#%E6%98%8E%E7%A1%AE%E5%BA%94%E5%8A%A0%E8%BD%BD%E7%9A%84model)
    - [load_resource&authorize_resource](#loadresourceauthorizeresource)
      - [自定义查找](#%E8%87%AA%E5%AE%9A%E4%B9%89%E6%9F%A5%E6%89%BE)
    - [跳过控制器校验](#%E8%B7%B3%E8%BF%87%E6%8E%A7%E5%88%B6%E5%99%A8%E6%A0%A1%E9%AA%8C)
    - [:manage规则](#manage%E8%A7%84%E5%88%99)
    - [重置当前权限](#%E9%87%8D%E7%BD%AE%E5%BD%93%E5%89%8D%E6%9D%83%E9%99%90)
    - [处理未经授权的访问](#%E5%A4%84%E7%90%86%E6%9C%AA%E7%BB%8F%E6%8E%88%E6%9D%83%E7%9A%84%E8%AE%BF%E9%97%AE)
    - [check_authorization](#checkauthorization)
    - [异常提示消息](#%E5%BC%82%E5%B8%B8%E6%8F%90%E7%A4%BA%E6%B6%88%E6%81%AF)
      - [通过国际化进行定制](#%E9%80%9A%E8%BF%87%E5%9B%BD%E9%99%85%E5%8C%96%E8%BF%9B%E8%A1%8C%E5%AE%9A%E5%88%B6)
      - [ApplicationController处理](#applicationcontroller%E5%A4%84%E7%90%86)
    - [健壮参数](#%E5%81%A5%E5%A3%AE%E5%8F%82%E6%95%B0)
- [参考资料](#%E5%8F%82%E8%80%83%E8%B5%84%E6%96%99)

<!-- /TOC -->

# 前言

CanCanCan对应用程序做出两个假设(完成安装后):

0. 有一个定义权限的Ability类.
0. 在控制器中有一个current_user方法,它返回当前用户模型.

可以通过在ApplicationController定义current_ability方法来覆盖这两者,具体步骤在[这里](https://github.com/CanCanCommunity/cancancan/wiki/Changing-Defaults)．

# 定义权限

## Ability类

Ability类是定义所有用户权限的位置

```ruby
class Ability
  include CanCan::Ability

  def initialize(user)
    can :read, :all # 权限,即使没有登录 
    return unless user.present?  # 登录用户的附加权限
    can :manage, Post, user_id: user.id 
    can :manage, :all if user.admin?  # 管理员的其他权限
  end
end
```

## 尽可能使用哈希条件

尽管范围适用于提取,但在授权离散操作时会出现问题.

```ruby
can :read, Article, Article.is_published
```

导致这个CanCan::Error

```ruby
The can? and cannot? call cannot be used with a raw sql 'can' definition.
The checking code cannot be determined for :read #<Article ..>.
```
定义相同的更好方法是:

```ruby
can :read, Article, is_published: true 
```

## 哈希条件是DRYer

```ruby
通过对所有操作使用散列而不是块,不必担心将用于成员控制器操作的块( :create, :destroy, :update )转换为集合操作的等效块( :index, :show )--which无论如何都需要哈希！
```

## SQL中的哈希条件是OR,提供最大的灵活性

尽管ActiveRecord将链式范围视为SQL AND ,但CanCanCan将给定模型/操作对的哈希条件视为SQL OR

因此,除了上面的is_published条件之外,它还允许作者查看其草稿

```ruby
 can :read, Article, author_id: @user.id, is_published: [false, nil]
```

## 拆分ability.rb文件

```ruby
# app/models/ability.rb
class Ability
  include CanCan::Ability
  def initialize(user)
    can :edit, User, id: user.id
    can :read, Book, published: true
    can :edit, Book, user_id: user.id
    can :manage, Comment, user_id: user.id
  end
end
```
下面将对上述内容为每个模型或控制器定义一个单独的Ability文件

### ModelAbility文件

```ruby
# app/abilities/user_ability.rb
class UserAbility
  include CanCan::Ability
  def initialize(user)
    can :edit, User, id: user.id
  end
end

# app/abilities/comment_ability.rb
class CommentAbility
  include CanCan::Ability
  def initialize(user)
    can :manage, Comment, user_id: user.id
  end
end

# app/abilities/book_ability.rb
class BookAbility
  include CanCan::Ability
  def initialize(user)
    can :read, Book, published: true
    can :edit, Book, user_id: user.id
  end
end
```

### 覆盖控制器中的current_ability方法

```ruby
# app/controllers/books_controller.rb
class BooksController
  def current_ability
    @current_ability ||= BookAbility.new(current_user)
  end
end
```

# 校验权限

## can方法

can方法用于定义权限并需要两个参数.<br>
第一个是正在设置权限的操作,第二个是正在设置它的对象类.

```ruby
can :update, Article
```


定义权限后可以使用can?校验当前用户的权限.

```erb
<% if can? :update, @article %>
  <%= link_to "Edit", edit_article_path(@article) %>
<% end %>
```

cannot? 方法是为了方便而执行相反的校验.

```ruby
cannot? :destroy, @project
```

可以传递 `:manage` 表示任何操作,并且:all表示任何对象.

```ruby
can :manage, Article  # 用户可以对文章执行任何操作
can :read, :all       # user可以读取任何对象
can :manage, :all     # user可以对任何对象执行任何操作
```

可以为这些参数中的任何一个传递数组以匹配任何参数.<br>
例如,这里用户将能够更新或销毁文章和评论.

```ruby
can [:update, :destroy], [Article, Comment]
```

如果只想对对象执行CRUD操作,则应创建调用的自定义操作:crud例如:crud,并使用它而不是:manage :

```ruby
def initialize(user)
  alias_action :create, :read, :update, :destroy, to: :crud
  if user.present?
    can :crud, User
    can :invite, User
  end
end
```

### 哈希的条件

```ruby
 can :read, Project, active: true, user_id: user.id 
```


## 类校验

也可以传递类而不是实例

```erb
<% if can? :create, Project %>
  <%= link_to "New Project", new_project_path %>
<% end %>
```

***如果存在条件或条件哈希值,则在检查类时将忽略它们,并且它将返回true.***
```ruby
can :read, Project, :priority => 3
can? :read, Project # returns true
```

原因在于这里的类没有要检查的priority属性.

可以把它想象为"当前用户可以阅读项目吗?".<br> 
用户可以读取项目,因此返回true .但是,*这取决于所谈论的具体项目*.<br>
如果正在进行类检查,则在实例可用时再进行一次检查非常重要,因此可以使用条件的哈希值.


## 控制器权限

### authorize!

可以使用authorize! 在控制器操作中手动处理授权的方法.<br>
如果用户无法执行给定的操作,控制器中的方法将引发异常.

```ruby
def show
  @article = Article.find(params[:id])
  authorize! :read, @article
end
```

### load_and_authorize_resource

为每个操作设置authorize!操作可能很繁琐,<br>
因此提供了load_and_authorize_resource方法以自动授权RESTful样式资源控制器中的所有操作.<br> 
它将使用before操作将资源加载到实例变量中,并为每个操作进行授权.<br>
load_and_authorize_resource为每个操作设置一个前置过滤器,以处理控制器的加载和授权.

它将添加一个before过滤器,如果它们存在,则该动作将具有此行为.<br> 
这意味着无需在控制器中放置下面的代码:

```ruby
class ProjectsController < ApplicationController
  def index
    authorize! :index, Project
    @projects = Project.accessible_by(current_ability)
  end

  def show
    @project = Project.find(params[:id])
    authorize! :show, @project
  end
end
```

```ruby
class ArticlesController < ApplicationController
  # 它将应用于控制器中的每个操作
  load_and_authorize_resource
  # 可以使用:except和:only选项指定要影响的操作,就像before_filter
  load_and_authorize_resource :only => [:index, :show]
  
  def show
    # @article is already loaded and authorized
  end
end
```

#### index action

index action 使用accessible_by加载集合资源

```ruby
def index
  # @products 自动设置为 Product.accessible_by(current_ability)
  # 如果想要自定义查找选项,例如包含或分页,可以进一步构建它
  @products = @products.includes(:category).page(params[:page])
end
```

如果Product不响应accessible_by (例如,没有使用受支持的ORM),<br>
则最初不会设置@products变量.

#### show, edit, update and destroy actions

这些动作可以直接获取记录.

```ruby
  def show
    # @product自动设置为Product.find(params [:id])
  end
```

#### new and create actions

new和create动作使用哈希条件中的属性初始化资源.<br>
例如,有这样一个can定义:

```ruby
can :manage, Product, :discontinued => false
```

在控制器的new动作中:

```ruby
@product = Product.new(:discontinued => false)
```

这样,当用户访问new操作时,它将通过授权.<br>
用户在params[:product]传递的任何内容都会覆盖这些属性.

#### 明确应加载的model

如果模型类的名称空间与控制器不同,则需要指定:class选项:

```ruby
class ProductsController < ApplicationController
  load_and_authorize_resource :class => "Store::Product"
end
```

### load_resource&authorize_resource

调用load_and_authorize_resource与调用load_resource和authorize_resource相同,<br>
但是它们是两个单独的步骤,可以选择使用其中一个.

```ruby
class ProductsController < ActionController::Base
  load_resource
  authorize_resource
end
```

#### 自定义查找

如果想通过id以外的字段获取资源,可以使用find_by选项来完成

```ruby
load_resource :find_by => :permalink # will use find_by_permalink!(params[:id])
authorize_resource
```

### 跳过控制器校验

使用skip_load_and_authorize_resource,<br>
skip_load_resource或<br>
skip_authorize_resource<br>
方法跳过任何应用的行为,并指定在过滤器之前的特定操作.

```ruby
class ProductsController < ActionController::Base
  load_and_authorize_resource
  skip_authorize_resource :only => :new
end
```

### :manage规则

```ruby
class ArticlesController < ActionController::Base
  load_and_authorize_resource
  can :manage, Article, id: 23
end
```

上面代码所规定的规则将允许渲染ArticlesController的new方法,<br>
因为这条规则的写法被认为"用户可以管理id字段设置为23的任何文章对象",如:<br>
Article.new(id: 23) .

因此,load_and_authorize_resource将在:new操作中初始化模型并将其id设置为23,并呈现页面.<br> 
但是保存不起作用.

避免new被允许的正确预期规则是:

```ruby
...
can [:read, :update, :destroy], Article, id: 23
...
```

### 重置当前权限

当前用户的操作会使该请求的当前权限失效.<br>
但can?方法仍检查将在更新之前使用用户记录.<br>
需要重置current_ability实例,以便重新加载. <br>
如果也缓存了current_user,需要执行相同的操作.

```ruby
if @user.update_attributes(params[:user])
  @current_ability = nil
  @current_user = nil
  # ...
end
```

### 处理未经授权的访问

如果用户授权失败,将引发CanCan::AccessDenied异常.<br> 
可以捕获它并在ApplicationController修改其行为.

```ruby
class ApplicationController < ActionController::Base
  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden, content_type: 'text/html' }
      format.html { redirect_to main_app.root_url, notice: exception.message }
      format.js   { head :forbidden, content_type: 'text/html' }
    end
  end
end
```

### check_authorization

如果要确保应用程序中的每个操作都发生授权,<br>
将check_authorization添加到ApplicationController.

```ruby
class ApplicationController < ActionController::Base
  check_authorization
end
```

### 异常提示消息

在控制器中,当用户无法执行给定的操作,可以选择提供消息.

```ruby
authorize! :read, Article, :message => "Unable to read this article."
```

#### 通过国际化进行定制

```ruby
# in config/locales/en.yml
en:
  unauthorized:
    manage:
      all: "Not authorized to %{action} %{subject}."
      user: "Not allowed to manage other user accounts."
    update:
      project: "Not allowed to update this project."
```

#### ApplicationController处理

可以捕获异常并在ApplicationController修改其行为.<br>
行为可能因请求格式而异. 如:<br>
将错误消息设置为flash并重定向到主页以获取HTML请求,并返回403 Forbidden for JSON请求

```ruby
class ApplicationController < ActionController::Base
  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden }
      # 可以通过异常检索操作和主题以进一步自定义行为
      exception.action # => :read
      exception.subject # => Article  
      # 也可以通过异常自定义默认错误消息.如果没有提供消息,将使用此选项
      # exception.default_message = "Default error message"
      format.html { redirect_to main_app.root_url, :alert => exception.message }
    end
  end
end
```

### 健壮参数

```ruby
def update
  if @article.update_attributes(update_params)
    # hurray
  else
    render :edit
  end
end
...

def update_params
  params.require(:article).permit(:body)
end
```

对于:create动作,CanCanCan将尝试通过查看控制器是否响应以下方法(按顺序)来初始化具有已清理输入的新实例:

1. create_params
2. <model_name>_params例如article_params (这是rails中用于命名param方法的默认约定)
3. resource_params (可以在每个控制器中指定的通用命名方法)

此外, load_and_authorize_resource可以使用param_method选项指定控制器中的自定义方法以运行以清理输入.

可以将param_method选项与将被调用的方法名称对应的符号相关联:
```ruby
class ArticlesController < ApplicationController
  load_and_authorize_resource param_method: :my_sanitizer

  def create
    if @article.save
      # hurray
    else
      render :new
    end
  end

  private

  def my_sanitizer
    params.require(:article).permit(:name)
  end
end
```

# 参考资料

> [Github | cancancan](https://github.com/CanCanCommunity/cancancan)

> [How to use CanCanCan when your applications grows](https://medium.com/@coorasse/cancancan-that-scales-d4e526fced3d)