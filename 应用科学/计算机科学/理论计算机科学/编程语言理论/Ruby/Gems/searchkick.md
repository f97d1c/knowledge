> Elasticsearch搜索Ruby客户端

<!-- TOC -->

- [安装](#安装)
  - [Gemfile](#gemfile)
  - [Model](#model)
  - [reindex](#reindex)
- [查询](#查询)
  - [类似sql查询](#类似sql查询)
  - [搜索特定字段](#搜索特定字段)
  - [where条件](#where条件)
  - [排序](#排序)
  - [Limit / offset](#limit--offset)
  - [Select](#select)
  - [Results](#results)
    - [加载方式](#加载方式)
    - [结果总数](#结果总数)
  - [查询耗时](#查询耗时)
    - [完整返回内容](#完整返回内容)
- [权重](#权重)
  - [数值权重](#数值权重)
  - [根据值赋权重](#根据值赋权重)
- [分页](#分页)
- [匹配](#匹配)
  - [部分匹配](#部分匹配)
    - [拆词](#拆词)
    - [部分匹配](#部分匹配-1)
  - [完整匹配](#完整匹配)
    - [短语匹配](#短语匹配)
- [语言](#语言)
  - [同义词](#同义词)
    - [从文件读取](#从文件读取)
- [标签和动态同义词](#标签和动态同义词)
- [拼写错误](#拼写错误)
  - [指定可以包含拼写错误字段](#指定可以包含拼写错误字段)
- [排除结果](#排除结果)
  - [通过设定系数提高结果权重](#通过设定系数提高结果权重)
- [表情符号](#表情符号)
- [索引](#索引)
  - [](#)
  - [](#-1)
- [常见问题](#常见问题)
  - [结果集过大(query_phase_execution_exception)](#结果集过大query_phase_execution_exception)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装

## Gemfile

添加:

```Ruby
gem 'searchkick'
```

## Model

如Product模型需要搜索功能,添加:

```Ruby
class Product < ApplicationRecord
  # ...
  searchkick
  # ...
end
```

## reindex

当Product模型中的数据需要初始化或者重置索引时:

```Ruby
Product.reindex
```

> 单条数据重置索引

```Ruby
Product.first.reindex
```

# 查询

```ruby
products = Product.search("apples")
products.each do |product|
  puts product.name
end
```

## 类似sql查询

```ruby
Product.search "apples", where: {in_stock: true}, limit: 10, offset: 50
```

## 搜索特定字段

```ruby
fields: [:name, :brand]
```

## where条件

```ruby
where: {
  expires_at: {gt: Time.now},   # lt, gte, lte also available
  orders_count: 1..10,          # equivalent to {gte: 1, lte: 10}
  aisle_id: [25, 30],           # in
  store_id: {not: 2},           # not
  aisle_id: {not: [25, 30]},    # not in
  user_ids: {all: [1, 3]},      # all elements in array
  category: {like: "%frozen%"}, # like [master]
  category: /frozen .+/,        # regexp
  category: {prefix: "frozen"}, # prefix
  store_id: {exists: true},     # exists [master]
  _or: [{in_stock: true}, {backordered: true}],
  _and: [{in_stock: true}, {backordered: true}],
  _not: {store_id: 1}           # negate a condition
}
```

## 排序

```ruby
order: {_score: :desc} # most relevant first - default
```

## Limit / offset

```ruby
limit: 20, offset: 40
```

## Select

```ruby
select: [:name]
```

## Results

返回一个Searchkick::Results对象.和大多数方法一样类似数组.

```ruby
results = Product.search("milk")
results.size
results.any?
results.each { |result| ... }
```

### 加载方式

默认情况下,从Elasticsearch中获取id,并从数据库中提取记录.<br>
要从Elasticsearch获取所有内容,请使用:

```ruby
Product.search("apples", load: false)
```

### 结果总数 

```ruby
results.total_count
```

## 查询耗时

```ruby
results.took
```

### 完整返回内容

```ruby
results.response
```

# 权重

提升重要字段权重:

```ruby
fields: ["title^10", "description"]
```

## 数值权重

```ruby
boost_by: [:orders_count] # give popular documents a little boost
boost_by: {orders_count: {factor: 10}} # default factor is 1
```

## 根据值赋权重

```ruby
boost_where: {user_id: 1}
boost_where: {user_id: {value: 1, factor: 100}} # default factor is 1000
boost_where: {user_id: [{value: 1, factor: 100}, {value: 2, factor: 200}]}
```

# 分页

和kaminari和will_paginate兼容很好.

```ruby
# controller
@products = Product.search "milk", page: params[:page], per_page: 20
```

页面无需改动.

# 匹配

## 部分匹配

### 拆词

```ruby
# 默认情况下,结果必须与查询中的所有单词匹配
Product.search "fresh honey"

# 匹配部分关键词
Product.search "fresh honey", operator: "or"
```

### 部分匹配

默认情况下,结果必须与整个单词back匹配,不匹配backpack.

可以使用以下方法更改此行为:
```ruby
class Product < ApplicationRecord
  searchkick word_start: [:name]
end

Product.search "back", fields: [:name], match: :word_start
```

可用选项:

选项|匹配|例子
-|-|-
:word|整个字|apple 匹配 apple
:word_start|关键词开始|app 匹配 apple
:word_middle|任何一个词的部分|ppl 匹配 apple
:word_end|关键词结束|ple 匹配 apple
:text_start|文字开头|gre匹配green apple,app不匹配
:text_middle|文本的任何部分|een app 匹配 green apple
:text_end|文字结束|ple匹配green apple,een不匹配

## 完整匹配 

```ruby
User.search query, fields: [{email: :exact}, :name]
```

### 短语匹配

```ruby
User.search "fresh honey", match: :phrase
```

# 语言

对于词干,Searchkick默认为英语.要更改此设置,请使用:

```ruby
class Product < ApplicationRecord
  searchkick language: "german"
end
```

[Github | 中文插件-analysis-ik](https://github.com/medcl/elasticsearch-analysis-ik)

## 同义词 

```ruby
class Product < ApplicationRecord
  searchkick synonyms: [["pop", "soda"], ["burger", "hamburger"]]
end
```

### 从文件读取 

```ruby
synonyms: -> { CSV.read("/some/path/synonyms.csv") }
```

# 标签和动态同义词

# 拼写错误

默认情况下,Searchkick通过返回编辑距离为1的结果来处理拼写错误的查询.

```ruby
Product.search "zucini", misspellings: {edit_distance: 2} # zucchini
```

Searchkick可以首先执行搜索而不会出现拼写错误,如果结果太少,则使用它们执行另一个搜索:

```ruby
Product.search "zuchini", misspellings: {below: 5}
```

## 指定可以包含拼写错误字段

*执行此操作时,必须指定要搜索的字段.* 

```ruby 
Product.search "zucini", fields: [:name, :color], misspellings: {fields: [:name]}
```

# 排除结果

如果用户搜索butter,可能获得结果peanut butter.<br>
为防止这种情况,请使用:

```ruby
Product.search "butter", exclude: ["peanut butter"]
```

可以映射要排除的查询和术语:

```ruby 
exclude_queries = {
  "butter" => ["peanut butter"],
  "cream" => ["ice cream", "whipped cream"]
}

Product.search query, exclude: exclude_queries[query]
```

## 通过设定系数提高结果权重

通过设定系数小于1来降级结果权重:

```ruby
Product.search("butter", boost_where: {category: {value: "pantry", factor: 0.5}}
```

# 表情符号

搜索🍨🍰得到ice cream cake

```ruby
# Gemfile
gem 'gemoji-parser'

Product.search "🍨🍰", emoji: true
```

# 索引

使用该search_data方法索引的数据

```ruby 

```

## 

```ruby

```

## 

```ruby

```

# 常见问题

## 结果集过大(query_phase_execution_exception)

> 错误提示

[500] {"error":{"root_cause":[{"type":"query_phase_execution_exception","reason":"Result window is too large, from + size must be less than or equal to: [10000] but was [37950]. See the scroll api for a more efficient way to request large data sets. This limit can be set by changing the [index.max_result_window] index level setting."}],"type":"search_phase_execution_exception","reason":"all shards failed","phase":"query","grouped":true,"failed_shards":[{"shard":0,"***index***":"`products_development_20180808150007253`","node":"stbYFJmTR-mcSA17lH0PJw","reason":{"type":"query_phase_execution_exception","reason":"Result window is too large, from + size must be less than or equal to: [10000] but was [37950]. See the scroll api for a more efficient way to request large data sets. This limit can be set by changing the [index.max_result_window] index level setting."}}]},"status":500}

> 解决措施

```
curl -XPUT "http://localhost:9200/上面标记的索引名称/_settings" -d '{ "index" : { "max_result_window" : 500000 } }'
```

# 参考资料

> [Github | searchkick](https://github.com/ankane/searchkick)




