> paranoia用于隐藏数据展示,即软删除.

<!-- TOC -->

- [安装](#安装)
  - [Gemfile](#gemfile)
  - [迁移](#迁移)
  - [model](#model)
- [使用](#使用)
  - [软删除](#软删除)
  - [硬删除](#硬删除)
  - [查找所有记录(包含软删除的在内)](#查找所有记录包含软删除的在内)
  - [排除已删除的记录](#排除已删除的记录)
  - [查找已删除的记录](#查找已删除的记录)
  - [查询是否被软删除](#查询是否被软删除)
  - [恢复记录](#恢复记录)
  - [还原记录及其依赖销毁的关联记录](#还原记录及其依赖销毁的关联记录)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装

## Gemfile

```
gem "paranoia"
```

engine使用添加:

```ruby
# lib/your_engine/engine.rb
require 'paranoia'
```

## 迁移

```ruby
add_column :table_name, :deleted_at, :datetime
add_index :table_name, :deleted_at
```

## model

添加:

```ruby
acts_as_paranoid

# 跳过默认范围
# acts_as_paranoid without_default_scope: true
```

# 使用

## 软删除

```ruby
data.destroy
```

## 硬删除

```ruby
data.really_destroy!
```

## 查找所有记录(包含软删除的在内)

```ruby
data.with_deleted
```

## 排除已删除的记录

```ruby
data.without_deleted
```

## 查找已删除的记录

```ruby
data.only_deleted
```

## 查询是否被软删除

```ruby
data.deleted?
```

## 恢复记录

```ruby
Model.restore([id,..idn])
```

## 还原记录及其依赖销毁的关联记录

```ruby
Model.restore(id,:recursive => true)
```

# 参考资料

> [Github | rubysherpas/paranoia](https://github.com/rubysherpas/paranoia)