> Puma是一个简单,快速,线程式,并且支持HTTP1.1高并发的Ruby/Rack服务器.在开发环境和生产环境中都可以使用Puma.

<!-- TOC -->

- [简介](#简介)
  - [为速度和并发而生](#为速度和并发而生)
- [安装](#安装)
  - [Gemfile](#gemfile)
  - [config/puma.rb](#configpumarb)
- [使用](#使用)
  - [常用命令](#常用命令)
- [参考资料](#参考资料)

<!-- /TOC -->


# 简介

## 为速度和并发而生

> Puma是一个应用于Ruby Web应用,<br>简单,快速,支持高并发的HTTP1.1服务器.<br>
它可以和任何支持Rack的应用配合使用,是Webrick或Mongrel的替代选择.<br>
它最初被设计为Rubinius的服务器,但是配合JRuby和MRI使用性能同样出色.<br>
在开发环境和生产环境中都可以使用Puma.<br>
Puma使用经过C语言优化的Ragel扩展(继承自Mongrel)来解析请求,<br>
用一种轻便的方式进行快速,精确的HTTP 1.1协议解析,<br>
然后使用内部线程池的线程来处理请求.<br>
这使得Puma可以为你的Web应用提供真正的并发.
Rubinius 2.0后,Puma将使用真正的线程来利用CPU的所有核,因此不用再使用多进程来提高性能.<br>
对于MRI,因为全局锁(GIL)的限制,每次只能运行一个线程.<br>
但是对于大量阻塞IO的操作(例如调用Twitter API的HTTP请求),<br>
Puma还是通过允许阻塞IO操作并行运行提高了MRI的性能(像Thin这种以EventMachine为基础的服务器关闭了这个特性,需要自己使用第三方库).<br>
为了获得最佳性能,强烈建议使用实现了真正多线程的Ruby版本,例如 Rubinius或JRuby.

# 安装

## Gemfile

```Gemfile
gem puma
```

## config/puma.rb

```ruby
#!/usr/bin/env puma

# rails的运行环境
environment 'production'
#threads - puma的线程数,第一个参数是最小的线程数,第二个参数是最大线程数
threads 2, 64
workers 4

# 项目名
app_name = "ruby_sample"
# 项目路径
application_path = "/var/www/#{app_name}"
# 这里一定要配置为项目路径下地current
directory "#{application_path}/current"

# 下面都是 puma的配置项
pidfile "#{application_path}/shared/tmp/pids/puma.pid"
state_path "#{application_path}/shared/tmp/sockets/puma.state"
stdout_redirect "#{application_path}/shared/log/puma.stdout.log", "#{application_path}/shared/log/puma.stderr.log"
# bind - 指定的是puma运行时产生的socket,后面nginx会用到
bind "unix://#{application_path}/shared/tmp/sockets/#{app_name}.sock"
activate_control_app "unix://#{application_path}/shared/tmp/sockets/pumactl.sock"

# 后台运行
daemonize true
on_restart do
  puts 'On restart...'
end
preload_app!
```

# 使用

## 常用命令

命令|作用
-|-
puma -C config/puma.rb|启动
pumactl restart|重启
kill $(ps -ef\|egrep 'shared/sockets/puma.sock'\|grep -v grep\|awk '{print $2}');pumactl start|重启

# 参考资料

> [简书 | Puma: 为并发而生的Ruby Web服务器](https://www.jianshu.com/p/1bbc6ef924ce)