> 文件上传

<!-- TOC -->

- [安装](#安装)
  - [Gemfile](#gemfile)
  - [generate](#generate)
- [使用](#使用)
  - [单个文件保存](#单个文件保存)
    - [迁移](#迁移)
    - [User](#user)
    - [保存示例](#保存示例)
  - [多个文件保存](#多个文件保存)
    - [迁移](#迁移-1)
    - [model](#model)
    - [view](#view)
    - [controller](#controller)
    - [保存示例](#保存示例-1)
  - [更改存储目录](#更改存储目录)
  - [保护上传安全](#保护上传安全)
    - [白名单](#白名单)
      - [扩展名](#扩展名)
      - [内容类型](#内容类型)
    - [黑名单](#黑名单)
      - [内容类型](#内容类型-1)
  - [添加版本](#添加版本)
    - [图像缩略图](#图像缩略图)
      - [保存示例](#保存示例-2)
    - [条件版本](#条件版本)
    - [从现有版本创建版本](#从现有版本创建版本)
  - [上传失败回显文件](#上传失败回显文件)
    - [view](#view-1)
  - [删除上传的文件](#删除上传的文件)
  - [配置CarrierWave](#配置carrierwave)
  - [国际化](#国际化)
  - [大型文件上传](#大型文件上传)
  - [ActiveRecord回调](#activerecord回调)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装

## Gemfile

```ruby
gem 'carrierwave', '~> 1.0'
```

## generate

```ruby
rails g uploader Avatar
```

执行后会生成:

```ruby
# app/uploaders/avatar_uploader.rb
class AvatarUploader < CarrierWave::Uploader::Base
  storage :file
end
```

# 使用

## 单个文件保存

如果需要永久存储文件须确保在加载ORM后加载CarrierWave:

```ruby
require 'carrierwave/orm/activerecord'
```

下面将以User模型保存文件为示例展示过程.

### 迁移

```ruby
rails g migration add_avatar_to_users avatar:string
rails db:migrate
```

### User

```ruby
class User < ActiveRecord::Base
  ...
  mount_uploader :avatar, AvatarUploader
  ...
end
```

### 保存示例

```ruby
user = User.new
user.avatar = params[:file]
user.save!
user.avatar.url # => '/url/to/file.png'
user.avatar.current_path # => 'path/to/file.png'
user.avatar_identifier # => 'file.png'
```

即使没有与之相关文件,<br>
user.avatar也永远不会返回nil.<br> 
要检查照片是否已保存到模型中,使用:<br>
user.avatar.file.nil?

## 多个文件保存

### 迁移

添加一个可以存储数组的列或JSON列.<br>
取决于数据库支持的内容.例如,创建一个这样的迁移:

```ruby
rails g migration add_avatars_to_users avatars:json
rails db:migrate
```

### model

```ruby
class User < ActiveRecord::Base
  mount_uploaders :avatars, AvatarUploader
  serialize :avatars, JSON # 如果使用SQLite,添加这一行.
end
```

### view

```erb
<%= form.file_field :avatars, multiple: true %>
```

### controller

```ruby
params.require(:user).permit(:email, :first_name, :last_name, {avatars: []})
```

### 保存示例

```ruby
u = User.new(params[:user])
u.save!
u.avatars[0].url # => '/url/to/file.png'
u.avatars[0].current_path # => 'path/to/file.png'
u.avatars[0].identifier # => 'file.png'
```

## 更改存储目录

要更改上载文件的放置位置,只需覆盖store_dir方法:

```ruby
class MyUploader < CarrierWave::Uploader::Base
  def store_dir
    # 如果想在项目根级别存储文件,将store_dir定义为nil
    'public/my/upload/directory'
  end
end
```

## 保护上传安全

### 白名单 

指定允许的扩展名或内容类型的白名单

#### 扩展名

```ruby
class MyUploader < CarrierWave::Uploader::Base
  def extension_whitelist
    %w(jpg jpeg gif png)
  end
end
```

#### 内容类型

```ruby
class MyUploader < CarrierWave::Uploader::Base
  def content_type_whitelist
    /image\//
  end
end
```

### 黑名单

#### 内容类型

```ruby
class NoJsonUploader < CarrierWave::Uploader::Base
  def content_type_blacklist
    ['application/text', 'application/json']
  end
end
```

## 添加版本

例如,需要添加同一文件的不同版本.

示例将通过图像缩略图进行介绍

### 图像缩略图

必须安装Imagemagick和MiniMagick才能进行图像大小调整.


下面代码在处理上传图片时:<br>
上传的图像将缩放为不大于800 x 800像素,将保留原始宽高比.<br>
然后创建一个名为thumb的版本,该版本缩放到200×200像素.

```ruby
class MyUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  process resize_to_fit: [800, 800]

  version :thumb do
  # resize_to_fill 将确保填充指定的宽度和高度,仅在宽高比需要时才进行裁剪
    process resize_to_fill: [200,200]
  end

end
```

#### 保存示例

```ruby
uploader = AvatarUploader.new
uploader.store!(my_file)                            # size: 1024x768

uploader.url # => '/url/to/my_file.png'             # size: 800x800
uploader.thumb.url # => '/url/to/thumb_my_file.png' # size: 200x200
```

### 条件版本

有时希望限制在模型中的某些属性上或基于图片本身创建版本:

```ruby
class MyUploader < CarrierWave::Uploader::Base

  version :human, if: :is_human?
  version :monkey, if: :is_monkey?
  version :banner, if: :is_landscape?

private
  # model变量指向上传器附加到的实例对象
  def is_human? picture
    model.can_program?(:ruby)
  end

  def is_monkey? picture
    model.favorite_food == 'banana'
  end

  def is_landscape? picture
    image = MiniMagick::Image.open(picture.path)
    image[:width] > image[:height]
  end

end
```

### 从现有版本创建版本

出于性能原因,从现有版本创建版本而不是使用原始文件通常很有用.<br> 
如果上传器生成多个版本,其中下一个版本小于上一个版本,则从较小的已处理图像生成所需的时间较短.

```ruby
class MyUploader < CarrierWave::Uploader::Base

  version :thumb do
    process resize_to_fill: [280, 280]
  end

  # :from_version选项使用缓存在:thumb版本而不是原始版本中的文件,可能会加快处理速度
  version :small_thumb, from_version: :thumb do
    process resize_to_fill: [20, 20]
  end

end
```

## 上传失败回显文件

通常验证失败时上传的文件会消失.<br>
CarrierWave可以做在这种情况下记住上传的文件.

需要添加一个名为avatar_cache的隐藏字段.

### view

```erb
<%= form_for @user, html: { multipart: true } do |f| %>
  <p>
    <label>My Avatar</label>
    <%= f.file_field :avatar %>
    <%= f.hidden_field :avatar_cache %>
  </p>
<% end %>
```

##  删除上传的文件

```ruby
# 将avatar替换为具体字段名称
@user.remove_avatar!
@user.save
#=> true
```

## 配置CarrierWave

```ruby
# config/initializers/carrierwave.rb(新建) 
CarrierWave.configure do |config|
  config.permissions = 0666
  config.directory_permissions = 0777
  config.storage = :file
  # 禁用删除旧文件功能
  config.remove_previously_stored_files_after_update = false
end
```

或者

```ruby
class AvatarUploader < CarrierWave::Uploader::Base
  permissions 0777
end
```

## 国际化

```yml
errors:
  messages:
    carrierwave_processing_error: "无法处理"
    carrierwave_integrity_error: "不是允许的文件类型"
    carrierwave_download_error: "无法下载"
    extension_whitelist_error: "您不能上传％{extension}文件,允许的类型:％{allowed_types}"
    extension_blacklist_error: "您不能上传％{extension}文件,禁止的类型:％{prohibited_types}"
    content_type_whitelist_error: "您不能上传％{content_type}个文件,允许的类型:％{allowed_types}"
    content_type_blacklist_error: "您不能上传％{content_type}个文件"
    rmagick_processing_error: "无法用rmagick进行操作,也许它不是图像? "
    mini_magick_processing_error: "无法使用MiniMagick操作,也许它不是图像?原始错误:％{e}"
    min_size_error: "文件大小应大于％{min_size}"
    max_size_error: "文件大小应小于％{max_size}" 
```

## 大型文件上传

默认情况下,CarrierWave会复制上载的文件两次,<br>
首先将文件复制到缓存中,然后将文件复制到存储中.<br>
对于大文件,这可能非常耗时.

可以通过覆盖move_to_cache和move_to_store方法中的一个或两个来更改此行为:

```ruby
class MyUploader < CarrierWave::Uploader::Base
  def move_to_cache
    true
  end

  def move_to_store
    true
  end
end
```

## ActiveRecord回调

```ruby
class User
  mount_uploader :avatar, AvatarUploader
end
```

将添加这些回调:

```ruby
after_save :store_avatar!
before_save :write_avatar_identifier
after_commit :remove_avatar!, on: :destroy
after_commit :mark_remove_avatar_false, on: :update
after_save :store_previous_changes_for_avatar
after_commit :remove_previously_stored_avatar, on: :update
```

如果想跳过任何这些回调(例如,保留现有的头像,即使在上传新的头像之后),<br>
可以使用ActiveRecord的skip_callback方法.

```ruby
class User
  mount_uploader :avatar, AvatarUploader
  skip_callback :commit, :after, :remove_previously_stored_avatar
end
```

# 参考资料

> [Github | carrierwave](https://github.com/carrierwaveuploader/carrierwave)

> [RubyChina | Gem 你需要将 CarrierWave 的删除旧文件功能禁用掉！](https://ruby-china.org/topics/22024)