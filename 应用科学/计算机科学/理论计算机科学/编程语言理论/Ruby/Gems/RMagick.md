> RMagick是Ruby编程语言和ImageMagick图像处理库之间的接口.

# 相关依赖

> ImageMagick版本6.7.7或更高版本(6.xx)

## Ubuntu

```
sudo apt-get install -fy libmagickwand-dev
```

# 参考资料

> [Github | rmagick](https://github.com/rmagick/rmagick)

