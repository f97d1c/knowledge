<!-- TOC -->

- [关于](#关于)
  - [注释](#注释)
    - [魔法注释(magic comment)](#魔法注释magic-comment)
    - [多行注释](#多行注释)
  - [代码规范](#代码规范)
  - [源码安装](#源码安装)
    - [recipe for target 'ext/openssl/all' failed](#recipe-for-target-extopensslall-failed)
- [Ruby程序的结构和运行](#ruby程序的结构和运行)
  - [词法结构](#词法结构)
  - [句法结构](#句法结构)
  - [文件结构](#文件结构)
  - [程序的编码](#程序的编码)
  - [Ruby程序的运行](#ruby程序的运行)
- [数据类型和对象](#数据类型和对象)
  - [数字](#数字)
    - [绝对值](#绝对值)
    - [奇偶](#奇偶)
    - [sprintf](#sprintf)
    - [随机数](#随机数)
    - [向上取整(ceil)](#向上取整ceil)
    - [向下取整(floor)](#向下取整floor)
    - [保留小数位数](#保留小数位数)
    - [格式化](#格式化)
  - [字符串](#字符串)
    - [表示方法](#表示方法)
    - [单复数转换](#单复数转换)
    - [编码](#编码)
      - [查看编码类型](#查看编码类型)
      - [UTF-8转GBK编码](#utf-8转gbk编码)
      - [Base64编/解码](#base64编解码)
      - [URI编/解码](#uri编解码)
      - [关于Ruby\&JavaScript两者base64编解/码中文现存问题及应对解决办法](#关于rubyjavascript两者base64编解码中文现存问题及应对解决办法)
      - [判断是否为二进制内容](#判断是否为二进制内容)
    - [拼接数字](#拼接数字)
    - [首字母大写](#首字母大写)
    - [字符串转常量](#字符串转常量)
    - [字符串解析为时间日期](#字符串解析为时间日期)
    - [补齐长度](#补齐长度)
    - [驼峰转蛇足](#驼峰转蛇足)
    - [拆分(split)](#拆分split)
      - [正则](#正则)
    - [gsub多条件替换](#gsub多条件替换)
      - [示例1](#示例1)
    - [格式化打印Json字符串](#格式化打印json字符串)
  - [数组](#数组)
    - [常用方法](#常用方法)
    - [指定默认值](#指定默认值)
    - [示例1](#示例1-1)
    - [数组转哈希](#数组转哈希)
    - [group\_by](#group_by)
    - [排序](#排序)
      - [多条件排序](#多条件排序)
    - [随机获取任意元素(sample)](#随机获取任意元素sample)
    - [查找最大值](#查找最大值)
    - [查询元素下标](#查询元素下标)
    - [将遍历结果进行一维扁平化处理(flat\_map)](#将遍历结果进行一维扁平化处理flat_map)
    - [生成指定长度的随机数数组](#生成指定长度的随机数数组)
  - [哈希](#哈希)
    - [常用方法](#常用方法-1)
    - [散列键](#散列键)
      - [符号在散列键的应用](#符号在散列键的应用)
    - [示例](#示例)
      - [默认值](#默认值)
      - [Symbol key 转 String](#symbol-key-转-string)
    - [根据散列键进行排序](#根据散列键进行排序)
      - [使用sort对散列键进行排序](#使用sort对散列键进行排序)
      - [使用sort\_by对散列键进行排序](#使用sort_by对散列键进行排序)
    - [查找最大值](#查找最大值-1)
    - [多层级哈希转为对象](#多层级哈希转为对象)
    - [格式化打印](#格式化打印)
  - [范围](#范围)
  - [符号](#符号)
    - [Symbol与String的区别](#symbol与string的区别)
    - [非法 Symbol 字符串](#非法-symbol-字符串)
    - [查看所有Symbol对象](#查看所有symbol对象)
  - [True、False和Nil](#truefalse和nil)
    - [关于nil](#关于nil)
  - [随机数](#随机数-1)
    - [rand](#rand)
    - [sample](#sample)
    - [shuffle](#shuffle)
  - [日期和时间](#日期和时间)
    - [常用函数](#常用函数)
    - [时间格式化](#时间格式化)
      - [指定日期格式](#指定日期格式)
        - [指定时区(相对于UTC的偏移)](#指定时区相对于utc的偏移)
      - [to\_s(:number)](#to_snumber)
    - [字符串转时间](#字符串转时间)
    - [随机生成日期](#随机生成日期)
  - [正则表达式](#正则表达式)
    - [字符串转换为正则表达式](#字符串转换为正则表达式)
    - [正则替换](#正则替换)
    - [匹配中文](#匹配中文)
  - [浅拷贝和深拷贝](#浅拷贝和深拷贝)
- [表达式和操作符](#表达式和操作符)
  - [命名规则](#命名规则)
  - [字面量和关键字字面量](#字面量和关键字字面量)
  - [变量](#变量)
  - [作用域](#作用域)
  - [常量](#常量)
  - [方法调用](#方法调用)
  - [赋值](#赋值)
    - [多重赋值](#多重赋值)
      - [合并执行多个赋值操作](#合并执行多个赋值操作)
      - [交换变量的值](#交换变量的值)
      - [获取数组元素](#获取数组元素)
  - [操作符](#操作符)
    - [逻辑运算符](#逻辑运算符)
      - [关于\&\&的小技巧](#关于的小技巧)
      - [关于||的小技巧](#关于的小技巧-1)
    - [一元星号操作符(splat运算符)](#一元星号操作符splat运算符)
      - [在调用方法时使用splats](#在调用方法时使用splats)
      - [放在参数列表中的任何位置](#放在参数列表中的任何位置)
      - [阵列解构](#阵列解构)
        - [从数组中获取第一项](#从数组中获取第一项)
        - [获取最后一个项目](#获取最后一个项目)
        - [获取数组的前n个项目](#获取数组的前n个项目)
          - [局限性](#局限性)
      - [构造数组](#构造数组)
        - [将对象强制转换为数组](#将对象强制转换为数组)
        - [将方法返回值强制转换为数组](#将方法返回值强制转换为数组)
- [语句和控制结构](#语句和控制结构)
  - [条件式](#条件式)
    - [if](#if)
    - [unless](#unless)
  - [循环](#循环)
    - [常用循环方法](#常用循环方法)
    - [times方法](#times方法)
    - [for 语句](#for-语句)
    - [while 语句](#while-语句)
    - [until 语句](#until-语句)
    - [each 方法](#each-方法)
      - [find\_each](#find_each)
    - [loop 方法](#loop-方法)
    - [循环控制](#循环控制)
    - [sleep](#sleep)
  - [迭代器和可枚举对象](#迭代器和可枚举对象)
  - [代码块](#代码块)
    - [关于yield](#关于yield)
      - [实例-1](#实例-1)
  - [改变控制流](#改变控制流)
    - [case](#case)
      - [对象类型匹配](#对象类型匹配)
      - [正则匹配写法](#正则匹配写法)
      - [示例3(when 条件 then 返回值)](#示例3when-条件-then-返回值)
      - [示例4](#示例4)
      - [示例5](#示例5)
      - [===与case语句](#与case语句)
  - [异常和异常处理](#异常和异常处理)
  - [BEGIN和END](#begin和end)
  - [线程、纤程和连续体](#线程纤程和连续体)
- [方法、Proc、Lambda和闭包](#方法proclambda和闭包)
  - [方法](#方法)
    - [根据方法名查询是否存在定义方法(respond\_to?)](#根据方法名查询是否存在定义方法respond_to)
    - [方法的调用](#方法的调用)
    - [运算符形式的方法调用](#运算符形式的方法调用)
    - [方法的分类](#方法的分类)
      - [实例方法](#实例方法)
      - [类方法](#类方法)
      - [函数式方法](#函数式方法)
    - [方法的定义及返回值](#方法的定义及返回值)
      - [具名参数](#具名参数)
    - [定义带块的方法](#定义带块的方法)
    - [参数不确定的方法](#参数不确定的方法)
    - [关键字参数方法](#关键字参数方法)
    - [获取方法所有参数及其参数值](#获取方法所有参数及其参数值)
    - [将数组分解为参数](#将数组分解为参数)
    - [方法可见性](#方法可见性)
      - [Public](#public)
      - [Protected](#protected)
      - [Private](#private)
  - [Proc和Lambda](#proc和lambda)
  - [闭包](#闭包)
  - [Method对象](#method对象)
  - [函数式编程](#函数式编程)
- [类和模块](#类和模块)
  - [类](#类)
    - [对象](#对象)
    - [对象与类的对应表](#对象与类的对应表)
    - [类和实例](#类和实例)
    - [对象和实例](#对象和实例)
    - [继承](#继承)
      - [基类继承关系](#基类继承关系)
    - [创建类](#创建类)
      - [class 语句](#class-语句)
    - [initialize 方法](#initialize-方法)
    - [存取器](#存取器)
    - [特殊变量 self](#特殊变量-self)
    - [类方法](#类方法-1)
    - [类变量](#类变量)
    - [限制方法的调用](#限制方法的调用)
    - [扩展类](#扩展类)
      - [在原有类的基础上添加方法](#在原有类的基础上添加方法)
      - [继承](#继承-1)
    - [alias](#alias)
    - [undef](#undef)
    - [单例类](#单例类)
  - [子类化和继承](#子类化和继承)
  - [对象创建和初始化](#对象创建和初始化)
  - [模块](#模块)
    - [利用Mix-in扩展功能](#利用mix-in扩展功能)
    - [加载(include)和请求(require)模块](#加载include和请求require模块)
    - [include和extend](#include和extend)
    - [include和prepend](#include和prepend)
  - [单键方法和Eigenclass](#单键方法和eigenclass)
  - [方法查找](#方法查找)
  - [常量查找](#常量查找)
- [反射和元编程](#反射和元编程)
  - [类型、类和模块](#类型类和模块)
  - [对字符串和块进行求值](#对字符串和块进行求值)
  - [变量和常量](#变量和常量)
  - [方法](#方法-1)
  - [钩子方法](#钩子方法)
    - [method\_missing](#method_missing)
    - [singleton\_class](#singleton_class)
    - [define\_method](#define_method)
      - [实例](#实例)
    - [instance\_methods (Module)](#instance_methods-module)
      - [为模块中的每个方法调用指定代码](#为模块中的每个方法调用指定代码)
    - [method\_defined?](#method_defined)
    - [private\_instance\_methods](#private_instance_methods)
    - [protected\_instance\_methods](#protected_instance_methods)
    - [public\_instance\_methods](#public_instance_methods)
    - [methods (Object)](#methods-object)
    - [class\_eval](#class_eval)
    - [instance\_eval](#instance_eval)
    - [instance\_eval 与 class\_eval的选择](#instance_eval-与-class_eval的选择)
    - [inherited](#inherited)
    - [append\_features](#append_features)
    - [included](#included)
    - [instance\_variable\_set](#instance_variable_set)
    - [const\_get](#const_get)
    - [Proc](#proc)
      - [\&](#)
      - [实例1](#实例1)
    - [on\_load](#on_load)
  - [跟踪](#跟踪)
  - [ObjectSpace和GC](#objectspace和gc)
  - [定制控制结构](#定制控制结构)
  - [缺失的方法和常量](#缺失的方法和常量)
  - [动态创建方法](#动态创建方法)
  - [别名链](#别名链)
  - [领域特定语言](#领域特定语言)
- [Ruby平台](#ruby平台)
  - [终端工具(IRB)](#终端工具irb)
  - [正则表达式](#正则表达式-1)
  - [文件](#文件)
    - [常用方法](#常用方法-2)
    - [文件读取流程](#文件读取流程)
  - [目录](#目录)
    - [常用方法](#常用方法-3)
  - [输入/输出](#输入输出)
    - [输出](#输出)
      - [puts和p与print方法的区别](#puts和p与print方法的区别)
      - [打印表格](#打印表格)
      - [清空屏幕](#清空屏幕)
  - [网络](#网络)
  - [线程和并发](#线程和并发)
  - [加密/编码](#加密编码)
    - [两者区别与联系](#两者区别与联系)
    - [sha加密并base64编码示例](#sha加密并base64编码示例)
    - [MD5值](#md5值)
    - [ruby生成Java hash code](#ruby生成java-hash-code)
  - [命令行脚本](#命令行脚本)
    - [交互输入(gets)](#交互输入gets)
    - [参数项描述(OptionParser)](#参数项描述optionparser)
    - [捕获信号(Signal)](#捕获信号signal)
- [Ruby环境](#ruby环境)
  - [执行Ruby解释器](#执行ruby解释器)
  - [顶层环境](#顶层环境)
  - [实用性信息抽取和产生报表的快捷方式](#实用性信息抽取和产生报表的快捷方式)
  - [调用操作系统的功能](#调用操作系统的功能)
  - [安全](#安全)
- [算法](#算法)
  - [二分查找法](#二分查找法)
    - [文档断点续读](#文档断点续读)
- [参考资料](#参考资料)

<!-- /TOC -->
# 关于

## 注释

> 以 **#** 开始的部分,到行末为止都是注释部分.

### 魔法注释(magic comment)

在程序的首行代码添加注释 **#encoding:编码方式** 来指定字符编码,又称之为魔法注释.<br>在没有指定魔法注释的前提下 **默认使用UTF-8编码方式**.

### 多行注释

```ruby
=begin
这是一个多行注释
可扩展至任意数量的行
但 =begin 和 =end 只能出现在第一行和最后一行
=end
```

## 代码规范

0. if判断里面的代码不应超过五行,如果出现这种情况,是时候想想是不是逻辑有问题,是不是应该拆分成独立的方法了.

0. 永远都不要把代码写的太死,就算是一个必须执行的校验,如果需要也要可以绕过去. 也就说一个功能除了必要的非空校验外 不做任何逻辑校验, 在调用的地方再做校验, 如果该逻辑校验属于基础性的,应避免重复编写校验代码 将逻辑校验和功能代码进行二次封装.

## 源码安装

国内有时候无法访问 rvm.io 可以通过 源码进行安装.

[源码地址(F墙)](https://cache.ruby-lang.org/pub/ruby)


```bash
cd 源码文件夹
# 现在,配置并编译源代码：
# 源码配置
./configure
# 源码编译
make
# 源码安装
sudo make install
# 安装后,通过在命令行中输入以下命令来确保一切工作正常：
ruby -v
```

### recipe for target 'ext/openssl/all' failed

```bash
Makefile:301: recipe for target 'openssl_missing.o' failed
make[2]: *** [openssl_missing.o] Error 1
make[2]: Leaving directory '/ruby-2.3.8/ext/openssl'
exts.mk:212: recipe for target 'ext/openssl/all' failed
make[1]: *** [ext/openssl/all] Error 2
make[1]: Leaving directory '/ruby-2.3.8'
uncommon.mk:203: recipe for target 'build-ext' failed
make: *** [build-ext] Error 2
```

安装旧版本OpenSSL:

```bash
sudo apt purge libssl-dev && sudo apt install libssl1.0-dev
```

# Ruby程序的结构和运行
## 词法结构
## 句法结构
## 文件结构
## 程序的编码
## Ruby程序的运行

# 数据类型和对象

## 数字

### 绝对值

```ruby
-1024.abs
# => 1024

1024.abs
# => 1024
```

### 奇偶

```ruby
5.even? # => false
6.even? # => true
```

### sprintf

常用方法|含义|示例
-|-|-
sprintf('%.2f', 数字)|保留两位小数|sprintf('%.2f', 10001) => "10001.00"
sprintf('%03d', 数字)|保持数字位数固定,位数不足用0补齐|sprintf('%03d', 12) => 012

### 随机数

```ruby
# 生成0-1之间的任意数
rand

# 生成6位随机数
rand(999999)

# 生成16进制随机数
rand(0xffffff)

# 生成15到46区间的整数
rand(15..46)
```

### 向上取整(ceil)

```ruby
(6.to_f/39).ceil
# => 1
```

### 向下取整(floor)


```ruby
(6.to_f/39).floor
# => 0
```

### 保留小数位数

```ruby
0.38887372.round(2)
# => 0.39
```

### 格式化

```ruby
"%06d" % 111 # => "000111"

"%0.2f" % 111 # => "111.00"

# 保留两位小数但不四舍五入
("%0.3f" % 111.66888)[0...-1] # 111.66
```


## 字符串

### 表示方法

```ruby
print "a \nb"
# a
# b

print 'a \nb'
# => a \nb

print %Q|a \nb|
# a
# b

print %Q{a \nb}
# a
# b

print <<BLOCK
a \\nb
BLOCK
# => a \nb

print %/a \nb /
# a
# b
```

### 单复数转换

```ruby
puts "user".pluralize # => users
puts "publish".pluralize # => publishes
puts "repositories".singularize # => repository
```

### 编码

#### 查看编码类型

```ruby
'string'.encoding
```

#### UTF-8转GBK编码

```ruby
encode('utf-8','gbk', :undef => :replace, :replace => "?", :invalid => :replace).chars.select{|i| i.valid_encoding?}.join
```

#### Base64编/解码

```ruby
# 编码
Base64.encode64(content)
# 解码
Base64.decode64(content)
```

#### URI编/解码

> URI.escape在 Ruby 1.9.2 中已弃用...

```ruby
encoded_uri = URI.escape({hello: 'world'}.to_json)
# => "%7B%22hello%22:%22world%22%7D"
URI.decode(encoded_uri)
# => "{\"hello\":\"world\"}"

# CGI.escape 输出的转义字符串和JavaScript 的 encodeURIComponent等效
CGI.escape({hello: 'world'}.to_json)
# => "%7B%22hello%22%3A%22world%22%7D"
```

#### 关于Ruby&JavaScript两者base64编解/码中文现存问题及应对解决办法

```ruby
# 在英文字符串情况下,Ruby编码,Js解码没有问题
Base64.encode64('test')
# => "dGVzdA==\n"

decodeURIComponent(window.atob("dGVzdA==\n"))
# => "test"

# 但在中文字符串情况下,Ruby编码,Js解码存在乱码问题

Base64.encode64('测试')
# => "5rWL6K+V\n"

decodeURIComponent(window.atob("5rWL6K+V\n"))
# => "æµè¯"

# 有人提到Js采用UTF-16编码 但仍无效果

Base64.encode64('测试').force_encoding('utf-16')
# => "\x35\x72\x57\x4C\x36\x4B\x2B\x56\n"

decodeURIComponent(window.atob("\x35\x72\x57\x4C\x36\x4B\x2B\x56\n"))
# => "æµè¯"

# 最后采用了GitHub上封装的一套代码才得以解决

Base64.decode('5rWL6K+V\n');
# => "测试"
```

> [GitHub | dankogai](https://github.com/dankogai/js-base64)

#### 判断是否为二进制内容

判断编码是否为 *ASCII_8BIT* 格式即可.

BINARY是ASCII_8BIT的别名.

```ruby
String.new("ABC", encoding: Encoding::BINARY).encoding
# => #<Encoding:ASCII-8BIT>

Encoding::BINARY == Encoding::ASCII_8BIT
# => true
```

### 拼接数字

```ruby
str = ''
# => ""
str << 1
# => "\u0001"
str = []
# => []
str << 1
# => [1]
str.join('')
# => "1"
```

### 首字母大写

```ruby
'capitalize'.capitalize
# => 'Capitalize'
'line_charts'.camelize # => "LineChart"
```

### 字符串转常量

尝试查找具有参数字符串中指定名称的常量.

***无论名称是否以 *::* 开头,都假定该名称是顶级常量之一.***

```ruby
'Time'.constantize.now
# => 2020-02-17 13:34:02 +0800
```

### 字符串解析为时间日期

```ruby
Date.parse("2018-08-07 17:43:42 +0800")
DateTime.parse("2018-08-07 17:43:42 +0800")
Time.parse("2018-08-07 17:43:42 +0800")
```

### 补齐长度

```ruby
22.to_s.rjust(5, '0') # => "00022"
22.to_s.ljust(5, '0') # => "22000"

Time.now.to_i.to_s.ljust(20, rand(1..100000000).to_s)
```

### 驼峰转蛇足

```ruby
"HeatMap".underscore # => "heat_map"
```

### 拆分(split)

#### 正则

```ruby
'### test'.split(/^#* /)
# => ["", "test"]

'### test'.split(/(^#*) /)
# => ["", "###", "test"]
```

### gsub多条件替换

```ruby
'hello'.gsub(/[eo]/, 'e' => 3, 'o' => '*')
```

#### 示例1

```ruby
hash = {:用户工号=>"05279102", :院系名称=>"图书馆", :院系编码=>"0527", :领导人库联系方式=>nil, :接口提供联系方式=>nil, :现存联系方式=>"15930****90"}

# 多条件默认替换模式为清空 与下面条件等价
# replace = {'{'=>'', '}'=>'',':'=>'','>'=>':',','=>',<br>',}
replace = {'>'=>':',','=>',<br>',}
# tr要求替换前后字符数相同
hash.to_s.gsub(/[\{\}\:=>,]/,  replace).tr('nil',' 暂无')
```
### 格式化打印Json字符串

```rb
JSON.pretty_generate({})
```

## 数组

### 常用方法

方法|描述
-|-
array.empty?|如果数组本身没有包含元素,则返回 true
array.length|返回 self 中元素的个数可能为零
array.size|返回 array 的长度(元素的个数)length 的别名
array.eql?(other)|如果 array 和 other 是相同的对象,或者两个数组带有相同的内容,则返回 true
array & other_array|返回一个新的数组,包含两个数组中共同的元素,没有重复
array \| other_array|通过把 other_array 加入 array 中,移除重复项,返回一个新的数组
array << obj|把给定的对象附加到数组的末尾该表达式返回数组本身,所以几个附加可以连在一起
array.concat(other_array)|追加other_array中的元素到self中
array.at(index)|返回索引为 index 的元素<br>索引值为负数将从self的末尾开始计数<br>如果索引超出范围则返回 nil
array.clear|从数组中移除所有的元素
array.compact|返回 self 的副本,移除了所有的 nil 元素
array.delete_at(index)|删除指定的 index 处的元素,并返回该元素<br>如果 index 超出范围,则返回 nil
array.delete_if { \|item\| block }|当 block 为 true 时,删除 self 的每个元素
array.flatten|返回一个新的数组,新数组是一个一维的扁平化的数组(递归)
array.include?(obj)|如果 self 中包含 obj,则返回 true,否则返回 false
array.insert(index, obj...)|在给定的 index 的元素 ***前*** 插入给定的值,index 可以是负值
array.reverse|返回一个新的数组,包含倒序排列的数组元素
array.uniq|返回一个新的数组,移除了 array 中的重复值
array.sample(size)|从数组中随机抽取size个元素
array.shuffle|数组随机排序
array.group_by{\|item\| 条件}|根据条件对数组元素进行分类
array.select{\|item\| 条件}|找出符合条件的元素
array.in_groups_of(每组元素个数, 元素个数不足补充项)|根据提供条件对数组进行分组

> insert 方法
cat_file里面有用到这个,在前一个元素插值会出现为nil的元素,需要最后调用compact方法

### 指定默认值

```ruby
Array.new(10, {content: '数据加载异常'})
# => [{:content=>"数据加载异常"},
#  {:content=>"数据加载异常"},
#  {:content=>"数据加载异常"},
#  {:content=>"数据加载异常"},
#  {:content=>"数据加载异常"},
#  {:content=>"数据加载异常"},
#  {:content=>"数据加载异常"},
#  {:content=>"数据加载异常"},
#  {:content=>"数据加载异常"},
#  {:content=>"数据加载异常"}]
```

### 示例1

OnlineConfigure是一个读取yml配置文件的模型.

```yml
defaults: &defaults

  # 竞价项目配置项
  project:

    # 项目状态
    state:
      # 可报价状态
      can_offer: ['audit_approved', 'delay_end', 'stop_wait_audited']
...

development:
  <<: *defaults

test:
  <<: *defaults

production:
  <<: *defaults
```

```ruby
# initlize文件中
PROJECT_CONFIGURE = OnlineBidding::OnlineConfigure.project

# model
# 可报价状态
CAN_OFFER_STATES = PROJECT_CONFIGURE.state.can_offer
# 竞价及供应商是否满足报价条件
def can_bid(user:)
  res = [
    # 当前项目状态是否允许报价
    CAN_OFFER_STATES.include?(self.workflow_state),
    # 当前项目时间是否允许报价
    self.start_time.to_time < Time.now && Time.now < self.end_time.to_time,
    # 当前用户是否可参与竞价
    user.can_bid?
    # 当前用户尚未对项目进行报价
    # !(self.bid_suppliers.pluck(:supplier_id).include?(user.supplier.id))
  ].uniq.include?(false)
  return [false, '当前项目或用户不满足报价条件.']if res
  [true, '']
end

```
在can_bid方法中数组中的每一项都会返回true或false.<br>
最后我对数组去重判断里面是否包含false来决定是否满足报价条件.<br>
重构前是用`if 条件1 && 条件2...`的形式来判断的.我认为这种形式有一下几点不足:<br>

0. 如果条件过多,单行代码过长
0. 针对单个条件去编写对应的注释具有一定的难度,不便于后人理解代码
0. 灵活性差,如果需要去掉某个判断条件只能删掉,不利于后期条件恢复

重构后的代码在弥补上述缺点的同时由于使用了配置文件,还具备了:<br>
全局条件统一,如果供应商可报价的状态发生变动,只需要改下配置文件里面的状态即可也就是更高的灵活性.

关于哪些内容应该放在配置文件里面,总结了一下几个条件:<br>

0. 明确指定的变量值,如:<br>
延期天数 = 3,类似这种已经明确指定而且不会根据条件不同而变化的变量值,<br>当然这个变量是因应该被声明为常量的.

### 数组转哈希

```ruby
tmp = [[1, "货到付款"], [2, "银行转账"]]

Hash[tmp]
# => {1=>"货到付款", 2=>"银行转账"}
```

### group_by

```ruby
array = [{type: '1', value: 22}, {type: '1', value: 12}, {type: '2', value: 12}]

array.group_by{|hash| hash[:type]
# => {"1"=>[{:type=>"1", :value=>22}, {:type=>"1", :value=>12}], "2"=>[{:type=>"2", :value=>12}]}
```
### 排序

```ruby
a = [ "d", "a", "e", "c", "b" ]

a.sort
# => ["a", "b", "c", "d", "e"]

a.sort { |x,y| y <=> x }
# => ["e", "d", "c", "b", "a"]

a.sort { |x,y| x <=> y }
# => ["a", "b", "c", "d", "e"]
```

#### 多条件排序

0. 按总报价的由低到高排
0. 报价相同,按信用分
0. 报价相同,信用分相同,按先后顺序

```ruby
grouped_by_supplier
# => [{:supplier_id=>149, :total=>13700.0, :visa=>0, :created_at=>1599036701},{:supplier_id=>176, :total=>13500.0, :visa=>0, :created_at=>1600248414},{:supplier_id=>192, :total=>13500.0, :visa=>3, :created_at=>1600248414},{:supplier_id=>75, :total=>13500.0, :visa=>0, :created_at=>1600313954}]

grouped_by_supplier.sort{|x,y| [x[:total], y[:visa], x[:created_at]]<=>[y[:total], x[:visa], y[:created_at]]}
# => [{:supplier_id=>192, :total=>13500.0, :visa=>3, :created_at=>1600248414},{:supplier_id=>176, :total=>13500.0, :visa=>0, :created_at=>1600248414},{:supplier_id=>75, :total=>13500.0, :visa=>0, :created_at=>1600313954},{:supplier_id=>149, :total=>13700.0, :visa=>0, :created_at=>1599036701}]
```

### 随机获取任意元素(sample)

```ruby
array = (1..100).to_a
array.sample(获取元素个数)

array.sample # => 66
array.sample(1) # => [89]
array.sample(3) # => [29, 39, 98]
```

### 查找最大值

```ruby
array = [{"CA"=>2, "MI"=>1, "NY"=>1},{"CA"=>2, "MI"=>9, "NY"=>1}]
array.max_by{|hash| hash.values[1]}
# => {"CA"=>2, "MI"=>9, "NY"=>1}
```

### 查询元素下标

```ruby
a = [ "a", "b", "c" ]
a.index("b")              # => 1
a.index("z")              # => nil
a.index { |x| x == "b" }  # => 1
```

```ruby
titles_ = table_conent.css('thead')[0].css('tr')[0].css('th').map{|children| children.text}
# => ["序号", "股票代码", "股票名称", "最新价", "涨跌幅", "相关资讯", "占净值比例", "持股数（万股）", "持仓市值（万元）"]

items = ["股票代码", "股票名称", "占净值比例", "持股数（万股）", "持仓市值（万元）"]
# => ["股票代码", "股票名称", "占净值比例", "持股数（万股）", "持仓市值（万元）"]

get_indexs = items.map{|item|titles_.index(item)}
# => [1, 2, 6, 7, 8]
```

### 将遍历结果进行一维扁平化处理(flat_map)

```ruby
[1, 2, 3, 4].flat_map { |e| [e, -e] } # => [1, -1, 2, -2, 3, -3, 4, -4]
[[1, 2], [3, 4]].flat_map { |e| e + [100] } # => [1, 2, 100, 3, 4, 100]
```

分解动作为:

```ruby
[[1, 2], [3, 4]].map{ |e| e + [100] }.flatten
# => [1, 2, 100, 3, 4, 100]
```

### 生成指定长度的随机数数组

```ruby
Array.new(4){rand(999)}
```

## 哈希

### 常用方法

方法|描述
-|-
hash.empty?|检查 hash 是否为空(不包含键值对),返回true或false
hash.length|以整数形式返回hash的大小或长度
hash.size|以整数形式返回hash的size
hash.store(key, value)|存储为hash中的一个键值对
hash[key]|使用键,从哈希引用值<br>如果未找到键,则返回默认值
hash[key]=value|把value给定的值与key给定的键进行关联
hash.keys|返回一个 ***新*** 的数组,包含hash的所有键
hash.values|返回一个 ***新*** 的数组,包含hash的所有值
hash.clear|从哈希中移除所有的键值对
hash.default = obj|为hash设置默认值
hash.delete_if { \|key,value\| block }|block 为 true 的每个块,从 hash 中删除键值对
hash.has_key?(key)<br>hash.include?(key)<br>hash.key?(key)<br>hash.member?(key)|检查给定的 key 是否存在于哈希中,返回true或false
hash.has_value?(value)<br>hash.value?(value)|检查哈希是否包含给定的 value
hash.index(value)|为给定的 value 返回哈希中的key<br>如果未找到匹配值则返回 nil
hash.invert|创建一个新的 hash,倒置hash中的keys和values<br>也就是说,在新的哈希中,hash 中的键将变成值,值将变成键
hash.merge!(other_hash)<br>hash.merge(other_hash){\|key, oldval, newval\| block}|返回一个新的哈希,包含hash和other_hash的内容<br>重写hash中与 other_hash带有重复键的键值对
hash.replace(other_hash)|把hash的内容替换为other_hash的内容
hash.to_a|从 hash 中创建一个二维数组<br>每个键值对转换为一个数组,所有这些数组都存储在一个数组中
Hash.from_xml(xml)|xml转为哈希
ActiveSupport::JSON.decode(jsonstr)|json转哈希
Hash.to_json(hash)<br>ActiveSupport::JSON.encode(hash)|哈希转json
hash.reject!|从哈希中移除指定key<br>{:a => 1, :b => 2}.reject! { \|k\| k == :a }   # => {:b => 2}
stringify_keys|Symbol key 转 String
deep_stringify_keys|所有层级的Symbol key 转 String
symbolize_keys/deep_symbolize_keys|String key 转 Symbol<br>顾名思义,两者的区别在于deep_symbolize_keys会将所有层级的key全部进行转换
hash.slice(some_keys)|返回只包含给定key的哈希<br>{ a: 1, b: 2, c: 3, d: 4 }.slice(:a, :b)<br>\# => {:a=>1, :b=>2}
array_a.any?{\|a\| array_b.include?(a)}<br>array_a.all{\|a\| array_b.include?(a)}|数组A中的元素是否全部(all?)或部分(any?)包含在数组B中
flat_map{\|a\| a << 1}|将块中执行结果返回一维扁平数组
hash.transform_keys{ \|key\| key.to_s.upcase }|将哈希键按照块内容进行格式化处理<br>类似方法还有deep_transform_keys<br>transform_values<br>deep_transform_values
hash.select{\|k,v\| 条件}|选取符合条件的键值对

### 散列键

0与0.0作为散列的键时会被判断为不同的键,这是由于散列对象内部对于键的比较使用了eql?方法

#### 符号在散列键的应用

```ruby
hash = {:foo => 'foo'}
# => {:foo=>"foo"}
hash[:foo]
# => "foo"
```

### 示例

```ruby
hash = Hash.new(nil)
hash[:test] # => nil

hash = Hash.new('')
hash[:test] # => ""
```

#### 默认值

```ruby
def index
  default_value = Hash.new(dealt_num: 0, dealt_info: '暂无待办事项')
  default_link = '/bid/background/quotes.html?utf8=✓&q%5Bworkflow_state_eq%5D='
  @dealt_array = [
    {dealt_title: '竞价申请待审核', dealt_link: '', workflow_state: 'wait_audited'},
    {dealt_title: '竞价公告待发布', dealt_link: '', workflow_state: 'wait_publish_notice'},
    {dealt_title: '竞价取消待审核', dealt_link: '', workflow_state: 'stop_wait_audited'},
    {dealt_title: '选标待审核', dealt_link: '', workflow_state: 'result_audited'},
    {dealt_title: '废标待审核', dealt_link: '', workflow_state: 'invalid_audited'},
    {dealt_title: '结果变更待审核', dealt_link: '', workflow_state: 'change_result_wait_audited'}
  ]

  @dealt_array.each do |dealt|
    count = ONLINE_QUOTE.where(workflow_state: dealt[:workflow_state]).count
    dealt[:dealt_link] = default_link + dealt[:workflow_state]
    if count == 0
      dealt.merge!(default_value[:default])
      next
    end
    dealt.merge!({dealt_num: count, dealt_info: "您有#{count}个#{dealt[:dealt_title]},请点击查看"})
  end

end
```

#### Symbol key 转 String

```ruby
tmp
# => {:minute=>46, :hour=>16, :day=>20200214, :week=>202007, :month=>202002, :quarter=>202001, :year=>2020}
tmp.stringify_keys
# => {"minute"=>46, "hour"=>16, "day"=>20200214, "week"=>202007, "month"=>202002, "quarter"=>202001, "year"=>2020}
```

### 根据散列键进行排序

tmp为一个由文件最后修改时间转整型为key,文件名为value的散列

```ruby
tmp ={
  1530695634=>"ChenguangInterface_log_p_20180704.log",
  1530761083=>"ChenguangInterface_log_p_20180705.log",
  1530701542=>"staples_log_p_20180704.log",
  1530762766=>"suning3_log_p_20180705.log",
  1530701543=>"suning3_log_p_20180704.log",
  1530761082=>"ComixInterface_log_p_20180705.log",
  1534490807=>"OfficemateInterface_log_p_20180817.log"
}
```
现在需要根据key对散列进行排序,下面时是sort和sort_by的具体实现方式

#### 使用sort对散列键进行排序

由小到大
```ruby
tmp.sort{|x,y| x[0] <=> y[0]}
# =>
[[1530695634, "ChenguangInterface_log_p_20180704.log"],
 [1530701542, "staples_log_p_20180704.log"],
 [1530701543, "suning3_log_p_20180704.log"],
 [1530761082, "ComixInterface_log_p_20180705.log"],
 [1530761083, "ChenguangInterface_log_p_20180705.log"],
 [1530762766, "suning3_log_p_20180705.log"],
 [1534490807, "OfficemateInterface_log_p_20180817.log"]]

tmp.sort{|x,y| y[0] <=> x[0]}
# =>
[[1534490807, "OfficemateInterface_log_p_20180817.log"],
 [1530762766, "suning3_log_p_20180705.log"],
 [1530761083, "ChenguangInterface_log_p_20180705.log"],
 [1530761082, "ComixInterface_log_p_20180705.log"],
 [1530701543, "suning3_log_p_20180704.log"],
 [1530701542, "staples_log_p_20180704.log"],
 [1530695634, "ChenguangInterface_log_p_20180704.log"]]
```

#### 使用sort_by对散列键进行排序
```ruby
tmp.sort_by{|x,y| x[0] <=> y[0]}
# =>
[[1530695634, "ChenguangInterface_log_p_20180704.log"],
 [1530761083, "ChenguangInterface_log_p_20180705.log"],
 [1530701542, "staples_log_p_20180704.log"],
 [1530762766, "suning3_log_p_20180705.log"],
 [1530701543, "suning3_log_p_20180704.log"],
 [1530761082, "ComixInterface_log_p_20180705.log"],
 [1534490807, "OfficemateInterface_log_p_20180817.log"]]

tmp.sort_by{|x,y| y[0] <=> x[0]}
# =>
[[1530695634, "ChenguangInterface_log_p_20180704.log"],
 [1530761083, "ChenguangInterface_log_p_20180705.log"],
 [1530701542, "staples_log_p_20180704.log"],
 [1530762766, "suning3_log_p_20180705.log"],
 [1530701543, "suning3_log_p_20180704.log"],
 [1530761082, "ComixInterface_log_p_20180705.log"],
 [1534490807, "OfficemateInterface_log_p_20180817.log"]]
```
***待整理: sort和sort_by的异同***

### 查找最大值

```ruby
hash = {"CA"=>2, "MI"=>1, "NY"=>1}
hash.max_by{|k,v| v}
# => ["CA", 2]
```

### 多层级哈希转为对象

```ruby
hash = {:a=>1,:b=>[{:c=>2,:d=>[{:e=>3,:f=>4},{:e=>5,:f=>6}]},{:c=>4,:d=>[{:e=>7,:f=>8},{:e=>9,:f=>10}]},{:c=>6,:d=>[{:e=>11,:f=>12},{:e=>13,:f=>14}]}]}

json = hash.to_json

object = JSON.parse(json, object_class: OpenStruct)
object.b[0].c # => 2
```

### 格式化打印

```ruby
print JSON.pretty_generate(@objects)
```

## 范围

## 符号

> 符号与字符串对象很相似,符号也是对象,一般作为名称标签使用,表示方法等对象的名称.<br>
符号实现的功能,大部分字符串也能实现.也可以将符号理解为轻量级的字符串.<br>
在像散列的键这样只是单纯判断是否相等的处理中,**使用符号会比字符串更加有效率**.

Ruby 内部一直在使用Symbol,比如Ruby程序中的各种名字,Symbol本质上是Ruby符号表中的东西.<br>
使用Symbol处理名字可以降低Ruby内存消耗,提高执行速度.

使用String的内存开销太大了,因为每一个String都是一个对象.<br>
一个字符串每出现一次Ruby就会创建一个String对象.<br>

通常来讲,当面临String还是Symbol的选择时,可以参考以下标准:

0. 如果使用字符串的内容,这个内容可能会变化,使用String
0. 如果使用固定的名字或者说是标识符,使用Symbol
0. 枚举值、关键字(哈希表关键字、方法的参数)等情况下只用到名字,使用Symbol即可


### Symbol与String的区别

> 在 Ruby 中字符串也是一种对象,即 String 对象, 同一字符串所属对象不同.<br>
而对于 Symbol 对象,一个名字(字符串内容)唯一确定一个 Symbol 对象.***名字相同,则Symbol相同.***

在Ruby中每一个对象都有唯一的对象标识符(Object Identifier),可以通过 object_id 方法来得到一个对象的标识符.<br>
Symbol 对象和 String 对象的差别:

```ruby
puts :foo.object_id
327458
# => nil
puts :foo.object_id
327458
# => nil
puts :"foo".object_id
327458
# => nil
puts "foo".object_id
24303850
# => nil
puts "foo".object_id
24300010
# => nil
puts "foo".object_id
24296170
# => nil
```

### 非法 Symbol 字符串

创建 Symbol 对象的字符串中不能含有'\0'字符,而 String 对象是可以的.

```ruby
:"fo\0o"
SyntaxError: compile error
(irb):1: symbol cannot contain '\0'
        from (irb):1
:"foo\0"
SyntaxError: compile error
(irb):2: symbol cannot contain '\0'
        from (irb):2
puts "foo\0".object_id
24305140
# => nil
puts "fo\0o".object_id
24301000
# => nil
irb(main):005:0>
```

### 查看所有Symbol对象

Symbol对象一旦定义将一直存在,直到程序执行退出.<br>
所有Symbol对象存放在Ruby内部的符号表中,<br>
可以通过类方法Symbol.all_symbols得到当前Ruby程序中定义的所有Symbol对象,<br>
该方法返回一个Symbol对象数组.

```ruby
Symbol.all_symbols.size
# => 4047
Symbol.all_symbols[0..9]
# => [:@level_notifier, :ppx, :msg_dn, :version, :secs, :@user, :pos, :socketpair, :TkENSURE, :HTTPAccepted]
```

## True、False和Nil

&nbsp;|&nbsp;
-|-
真|false,nil以外的所有对象均为真
假|false和nil

### 关于nil

nil是一个特殊的值,表示对象不存在.<br>
从数组或散列中获取对象以及正则表达式无法成功匹配等方法不能返回有意义的值时就会返回nil.

## 随机数

### rand

> 默认选出0到1之间的数字,<br>如果要选出0到10之间的数字,用rand(0..10),也可以用rand(11),<br>以此类推rand(101)是选出0到100之间的随机数.

### sample

> 从一个数组里抽取一个随机的值,<br>比如[1,2,3].sample,<br>是从这三个数字里随机选出其中一个.

```ruby
array = ("a".."z").to_a + ("A".."Z").to_a + ("0".."9").to_a

array

# => ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

number = ""

5.times do
  number << array.sample
end

number # => "GGZSo"
```

### shuffle

shuffle用于数组随机排序

```
[*('a'..'z'),*('A'..'Z'),*(0..9)].shuffle[0...size].join
```



## 日期和时间

### 常用函数
方法|说明|返回结果示例
-|-|-
Time.now|获取当前时间|2018-07-11 11:18:58 +0800
Time.now.beginning_of_day|获取当天起始时间|2018-07-11 00:00:00 +0800
Time.now.end_of_day|获取当天结束时间|2018-07-11 23:59:59 +0800
Time.now.end_of_week|获取本周结束时间|2018-07-15 23:59:59 +0800
Time.now.end_of_month|获取本月结束时间|2018-07-31 23:59:59 +0800
Time.now.end_of_year|获取当年结束时间|2018-12-31 23:59:59 +0800
Time.now.year|日期的年份|2020
Time.now.month|日期的月份|3
Time.now.day |一个月中的第几天|17
Time.now.wday|一周中的星期几(0 是星期日)|2
Time.now.yday|一年中的第几天|77
Time.now.hour|24小时制|11
Time.now.min |分钟|6
Time.now.sec |秒|0
Time.now.usec|微秒|258626
Time.now.zone|时区名称|"CST"
Time.new(2019,07)|生成指定时间|2019-07-01 00:00:00 +0800

### 时间格式化

#### 指定日期格式

```ruby
Time.now.strftime("%Y-%m-%d %H:%M:%S")
```

##### 指定时区(相对于UTC的偏移)

```ruby
Time.now.strftime("%Y-%m-%dT%H:%M:%S%z")
# => "2021-03-06T10:43:00+0800"

Time.now.strftime("%Y-%m-%dT%H:%M:%S%:z")
# => "2021-03-06T10:42:18+08:00"

Time.now.strftime("%Y-%m-%dT%H:%M:%S%::z")
# => "2021-03-06T10:43:29+08:00:00"
```
  
#### to_s(:number)

获取指定日期的数字部分

```ruby
Time.now.to_s
# => "2018-10-09 16:02:29"
Time.now.to_s(:number)
# => "20181009160237"
```

### 字符串转时间

```ruby
str = "2020-07-26 00:00:00"
array = str.gsub(/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/, '\1,\2,\3,\4,\5,\6').split(',')
Time.new(*array)
# => 2020-07-26 00:00:00 +0800
```

```ruby
str = "2020-07-26 00:00:00"
array = str.split(/:|-| /)
Time.new(*array)
# => 2020-07-26 00:00:00 +0800
```

### 随机生成日期

```ruby
def rand_date(days)
  rand(days).days.ago(Date.today)
end
```

## 正则表达式

关于正则表达式的基本内容基本上是各语言间通用,<br>
具体内容查看Regular.md文件.

### 字符串转换为正则表达式

Regexp用于将字符串转换为正则表达式:

```ruby
array = ['rails', 'active_storage', 'set_per_page_cookies', 'id']
reg = Regexp.new(array.join('|'))
res = _routes.routes.anchored_routes.uniq.map{|hash| [hash.defaults.values.join(','), hash.parts].join(',')}
res.delete_if{|string| reg.match(string)}
```

### 正则替换

ruby的正则替换与Bash语法中的基本一致,**()** 用于代表分组,从左往右一次排序:

```ruby
str = '2020-08-25,09:43:13,00'
str.gsub(/(\d{4}-\d{2}-\d{2}),(\d{2}:\d{2}:\d{2}),(\d{2})/, '\1 \2:\3')
# => "2020-08-25 09:43:13:00"
```

### 匹配中文

```ruby
/\p{Han}/
```

## 浅拷贝和深拷贝

浅拷贝：使用 clone 方法创建的对象与原对象共享相同的实例变量引用，修改嵌套对象会影响原对象。
深拷贝：通过递归复制或使用 Marshal 库创建完全独立的新对象，修改嵌套对象不会影响原对象。

```rb
a = []
b = a.clone

b << 1
a # => []
b # => [1]

a = {'a' => []}
b = a.clone

b['a'] << 1
a # => {"a"=>[1]}
b # => {"a"=>[1]}

a = {'a' => []}
b = Marshal.load(Marshal.dump(a))
b['a'] << 1

a # => {"a"=>[]}
b # => {"a"=>[1]}
```

# 表达式和操作符

## 命名规则

种类|方式
-|-
变量名<br>方法名| 蛇形命名(全部小写) -> just_like_this
常量名| 蛇形命名(全部大写) -> JUST_LIKE_THIS
类名<br>模块名|驼峰命名 -> LittleByLittle

## 字面量和关键字字面量
## 变量

名称|说明|作用域
-|-|-
局部变量<sup>1<sup>|以英文小写字母或_开头|局部变量的作用域从 class、module、def 或 do 到相对应的结尾或者从左大括号到右大括号 {}<br>当调用一个未初始化的局部变量时,它被解释为调用一个不带参数的方法
全局变量|以$开头
实例变量|以@开头|只要在同一个实例中,程序就可以超越方法定义,任意引用和修改实例变量的值.<br>引用未经初始化的实例变量时的返回值为nil.<br>只要实例存在,实例变量就不会消失.
类变量|以@@开头|类变量在定义它的类或模块的子类或子模块中可共享使用,也就是说类变量是该类所有实例的共享变量.<br>这一点与常量类似,但不同的是可以修改类变量的值.<br> ***类变量被共享在整个继承链中***<br>
伪变量|Ruby预先定义好的代表某特定值的特殊变量<br>即使给伪变量赋值,它的值也不会改变<br>如:nil,false,true,self等

## 作用域

局部变量,实例变量,self都是绑定在对象上的名字,简称为绑定(binding).<br>
所有绑定都有一个寄居场所,称为作用域(scope).

## 常量

名称|特性|作用域
-|-|-
变量|以英文小写字母或_开头<br>随时随地可以多次重复赋值
常量|**大写**英文字母`开头`<br>对已经赋值的常量再进行赋值时,Ruby会发出警告<sup>[1]<sup>|定义在类或模块内的常量可以从类或模块的内部访问,定义在类或模块外的常量可以被全局访问

[1]: 仅仅是发出警告,常量的值 **仍会** 改变.<br>
console中手动赋值可以,程序中会报语法错误.

## 方法调用
## 赋值

### 多重赋值
#### 合并执行多个赋值操作

```ruby
a = 1
b = 2
c = 3
```
可以简化为:

```ruby
a, b, c = 1, 2, 3
```

变量前加上 `*` 会将未分配的值封装为数组赋值给该变量

```ruby
a, b, *c = 1, 2, 3, 4, 5, 6, 7
p [a, b, c] # => [1, 2, [3, 4, 5, 6, 7]]

a, *b, c = 1, 2, 3, 4, 5, 6, 7
p [a, b, c] # => [1, [2, 3, 4, 5, 6], 7]

```
#### 交换变量的值

```ruby
a, b = 0, 1
tmp = a
a = b
b = tmp

a, b = 0, 1
a, b = b, a
```
#### 获取数组元素
用数组赋值时,如果左边有多个变量,ruby会自动获取数组的元素进行多重赋值<sup>1<sup>

```ruby
array = [1, 2, 3]
a, b = array
p [a, b] # => [1, 2]

array = [1, 2, 3]
a, *b = array
p [a, b] # => [1, [2, 3]]

```

-----

```ruby
emall_code, *sku = ['jd', '109283342', '9287361', '100973421']
```

## 操作符

### 逻辑运算符

运算符|说明
-|-
条件1 && 条件2|当条件1为真 ***并且*** 条件2也为真时,表达式整体返回真<br> **其中一个为假则整体的表达式返回假**
条件1 \|\| 条件2|条件1为真 ***或者*** 条件2为真,整体表达式返回真<br>同时为假则整体表达式返回假
!条件|对条件返回结果进行取反操作
!!条件|
\=\=\=|
=~|
<=>|

#### 关于&&的小技巧

ruby将nil视为false,利用A && B如果A为false则不再执行B的特性,<br>
在模型实例上进行应用可防止当对象为空时进行报错:

```ruby
user = nil
# => nil
user && user.real_name
# => nil

show_real_name = false
user.real_name if show_real_name
flag && user.real_name
```

```erb
<% user = ::User.find(content[:user_id]) %>
<% user&.real_name %>
<%= user && user.real_name %>

<!-- 相对于安全与(&)&&的优势在于更加简短 -->
<% user&.orders&.first&.no %>
<%= user && user.orders.first.no %>
```

#### 关于||的小技巧

```ruby
user = User.first
user.nil? '暂无' : user.real_name
user.real_name || '暂无'
```

### 一元星号操作符(splat运算符)

```ruby
def go(x, *args)
  puts args.inspect
end
go("a", "b", "c")
# => ["b", "c"]

def go(**params)
  puts params.inspect
end
go(x: 100, y: 200)
# => {:x=>100,:y=>200}

def go(test: '', **params)
  puts params.inspect
end
go(x: 100, y: 200)
# => {:x=>100,:y=>200}

def go(test: '', need:, **params)
  puts params.inspect
end
go(x: 100, y: 200)
# => {:x=>100,:y=>200}
```

#### 在调用方法时使用splats

不仅可以在定义方法时使用splats,还可以在调用方法时使用它们.<br>
可以将数组传递给需要多个参数的函数.<br>
数组中的第一项成为第一个参数,第二项成为第二个参数,依此类推.

```ruby
def go(x, y)
end
point = [12, 10]
go(*point)

def go(x:, y:)
end
point = { x: 100, y: 200 }
go(**point)
```

#### 放在参数列表中的任何位置


虽然通常将散列的参数放在参数列表的末尾,但是没有硬性要求splats这样做。

可以将splats放在参数列表中的任何位置:

```ruby
def go(x, *args, y)
  puts x # => 1
  puts y # => 5
  puts args.inspect # => [2,3,4]
end

go(1, 2, 3, 4, 5)
```

#### 阵列解构

所有这些带有参数的技巧只是数组解构的一种特殊情况。

如果不熟悉"数组解构"一词,它只是意味着将数组分解为单个项目。看起来像这样：

```ruby
a, b = [1, 2]
puts a
# 1
puts b
# 2
```

这很好用,但是必须指定一个变量来保存数组中的每个项目会很痛苦。splat运算符可以解决此问题-本质上就像通配符。

##### 从数组中获取第一项

有时,可以在不更改原始数组的情况下从数组中获取第一项很有用。这就是本示例的工作。

```ruby
first, *remainder = [1, 2, 3, 4, 5]
first
# => 1
remainder
# => [2, 3, 4, 5]
```

如果只需要第一项而不是数组的其余部分,则可以使用以下语法：

```ruby
first, * = [1, 2, 3, 4, 5]
first
# => 1
```

##### 获取最后一个项目

要将项目从数组末尾而不是开头拉出,只需将splat粘贴在开头即可,如下所示：

```ruby
*prefix, last = [1, 2, 3, 4, 5]
last
# => 5
prefix
# => [1, 2, 3, 4]
```
同样,如果我们不需要特定的变量,则不必分配它.

##### 获取数组的前n个项目

如果将splat运算符放在中间,则可以从数组的每个末端提取任意数量的项目。

```ruby
first, *, last =  [1, 2, 3, 4, 5]
first
# => 1
last
# => 5
```

###### 局限性

在数组解构中使用splat运算符时,仍然必须指定数组项相对于数组的开头和结尾的位置。因此,它不是从长数组中间提取项目的最佳工具。

#### 构造数组

splat运算符不仅可用于解构数组,而且还可用于构造它们。

在下面的示例中,使用splat连接两个数组。

```ruby
[*[1,2], *[3,4]]
# => [1, 2, 3, 4]

# 等价于:
[[1, 2], [3,4]].flatten
```

##### 将对象强制转换为数组

```ruby
x = *"hi mom"
# => ["hi mom"]

x = *nil
# => []

x = *[1,2,3]
# => [1, 2, 3]

x = *{a: 1}
# => [[:a, 1]]
```

##### 将方法返回值强制转换为数组

```ruby

config = {ignore: 'log'}

def add_ignores
  # ["scoundrels"]
  "scoundrels"
end

[*config[:ignore], *add_ignores()]
# => ["log", "scoundrels"]
```

# 语句和控制结构
## 条件式

### if

```ruby
if 条件
  处理
elsif 条件
  处理
else
  处理
end
```

### unless

```ruby
unless 条件
  处理
else
  处理
end
```

## 循环

### 常用循环方法

名称|用途
-|-
for 语句|同each
while 语句|希望自由执行循环条件时使用
until 语句|使用while条件过于难以理解时使用
times 方法|确定了循环次数时使用
each 方法|从对象集合中取出元素时使用
loop 方法|不限制循环次数时使用

> 语句和方法的区别
语句是指Ruby提供的循环控制语句.

### times方法

```ruby
循环次数.times do
  循环中的处理
end
```

### for 语句

```ruby
for 变量 in 开始时的数值..结束时的数值 do
  循环处理的内容
end
```

### while 语句

只要条件成立就会不断重复循环处理.

```ruby
while 条件 do
  条件满足时循环处理的内容
end
```

### until 语句

until语句和while语句结构完全一致,单 *条件判断相反*.<br>
也就是说until语句会一直执行循环处理,直到 *条件成立* 为止.

```ruby
until 条件 do
  条件不满足时循处理的内容
end
```

### each 方法

each方法将对象集合中的对象逐个取出.<br>
在Ruby内部,for语句使用each方法来实现的.
```ruby
对象.each do |变量|
  循环处理的内容
end
```

#### find_each

> Rilas中ActiveRecord提供方法.

默认以1000条为单位进行遍历,可通过batch_size调整每次遍历条数.

```ruby
find_each(batch_size: 100)
```

### loop 方法
没有终止的条件,只是不断的执行循环处理.

```ruby
loop do
  循环执行内容
end
```

### 循环控制
命令|用途
-|-
break|终止程序,跳出循环
next|跳到下一次循环
redo|在相同的条件下重复刚才的处理

### sleep

> sleep相当于js中的setTimeout函数,<br>
不同的是sleep后的参数单位是秒,不是微秒.<br>
另外,js中的setTimeout是另起一个线程,<br>
不停止主线程继承往下执行,和ajax类似,<br>
ruby的sleep方法却是会阻止当前线程往下执行的.

## 迭代器和可枚举对象

## 代码块

do ~ end之间的部分称为 **块**(block)也称为代码块.<br>
跨行写程序时使用 do ~ end<br>
程序写在一行时使用 { ~ }

### 关于yield

可以使用yield来允许私有方法调用return.<br>
控制器将停止执行并返回状态.

```ruby
def update
  @activity = find_activity! { return }
  @activity.update!(activity_attrs)
end

private

  def find_activity!
    activity = current_user
              .activities
              .where(id: params[:id])
              .first
    activity || (head(:not_found) && yield)
  end
```

#### 实例-1

```ruby
# model

# 查询商品上下架状态
# 如果传块结构中,代码运行正常最后返回nil,否则返回异常信息,即异常提示信息为执行代码最后一行
def query_product_shelf_state(emall_code: , skus: )
  (skus = skus.split(/,|,/)) if skus.is_a?(String)
  error_msgs = []

  res = 具体接口查询逻辑

  # res = [{:sku_id=>"000000010547796101", :status=>0}]
  res.each do |hash|
    if hash[:status].to_i == 1
      # 只有商品为上架状态才去执行商城现有上架逻辑
      res = yield(emall_code: emall_code, sku: hash[:sku_id]) if block_given?
      (error_msgs << res)if res.present?
    else
      error_msgs << "#{hash[:sku_id]}接口为下架状态"
    end
  end

  msg = "执行成功"
  (msg += ",部分产品存在异常:#{error_msgs.join(';')}") if error_msgs.present?
  [true, msg]
end
```

```ruby
# controller
def query_product_shelf_state
  emall_code = params[:operation_emall_code]
  skus = params[:product_skus]

  res = Product.query_product_shelf_state(emall_code: emall_code, skus: skus) do |hash|
    product = Product.find_by(emall_code: hash[:emall_code], sku: hash[:sku])
    if product.present?
      # 商品上架操作逻辑
      nil
    else
      "未找到符合该条件商品:#{hash}"  # 存在异常,确保异常提示信息为执行代码最后一行
    end
  end

  render json: {'success' => (res[0] ? true : false), 'msg' => res[1] }
end
```

## 改变控制流

### case

```ruby
case 比较对象
when 值1
  处理
when 值2, 值3
  # when可以一次指定多个值,当比较对象满足任何一个值时则进入其处理流程
  处理
else
  处理
end
```

#### 对象类型匹配

经常会遇到这样一种情况:方法中一个参数需要传一个数组过来<br>
但有时也会传一个字符串过来,这时就要判断下是否需要转换为数组
```ruby
...
foo = foo.split(',') if foo.is_a?(String)
```
如果使用case语句方法的可扩展性要高很多
```ruby
...
res = case foo
when String
  [true, foo.split(',')]
when Array
  [true, foo]
else
  [false, '参数类型要求为数组,请重新传参']
end
return res unless res[0]
foo = res[1]

```

#### 正则匹配写法

使用case语句匹配邮件关键信息

```ruby
...
file.each_line do |line|
  case line
  when /^From:/i
    # 针对寄信人相关操作
  when /^To:/i
    # 针对收信人相关操作
  when /^Subject:/i
    # 针对主题信息相关操作
  when /^$/i
    # 针对头部解析完毕后相关操作
  else
    next
  end
end
```

#### 示例3(when 条件 then 返回值)

```ruby
a = 1
b = true

b = case
when a == 1 then false
else
  true
end

b # => fasle
```
#### 示例4
```ruby
def chek_state(state)
  state if ['temporary','wait_audited','wait_publish_notice'].include?(state)
end

state = 'temporary'
case state
when chek_state(state)
  p 'ok'
end
```

#### 示例5

下面代码是一个竞价系统管理员审核流程的主要代码,<br>
主要包含了controller和model里面的内容.<br>
主要业务逻辑就是采购人发起竞价,管理员需要:<br>
&emsp;审核采购需求是否通过,审核终止申请是否通过,审核选标结果是否通过,<br>
&emsp;审核废标申请是否通过,审核结果变更是否通过<br>
这里面有几个出现频率比较高的词:<ins>审核</ins>, <ins>是否通过</ins><br>
是否通过也就是通过和不通过,true和false.<br>
通过对需求的抽象分析,因为审核不通过需要填写理由,<br>
所以用了一个页面,两个action来完成管理员的整个审核流程.<br>
而且随着流程状态的增加,审核过程的变更都可以轻松的来调整整个审核后项目状态的变更.<br>
灵活性很高,我觉得这种代码是活的而不是死的.<br>
也是通过这个业务逻辑的实现,我才有点明白了接到一个需求以后,<br>
应该先去抽象分析如何去实现具体功能,先去把实现过程需要用到的方法统筹规划好,<br>
这其中也应包含如果以后业务逻辑改了,如何在改动最小的前提下去调整实现的代码.<br>
也就是如何使得代码更具灵活性.<br>
如何编写优雅的代码.

```ruby
# controller
# 项目状态审核(主要针对审核通过情况)
def audit_state
  res = @quote.audit_project_state(pass: true)
  flash_msg(res[0]? :success : :error, res[0]? "保存成功." : res[1])
  redirect_to audit_demand_background_quote_path(@quote)
end

# 项目状态异步审核(主要针对审核不通过情况)
def ajax_audit_state
  res = @quote.audit_project_state(pass: false, reason: params[:reason])
  render json: {'success' => res[0], 'message' => res[1] }
end

# model
def change_state(pass:)
  state = pass ? 1 : 0
  res = case workflow_state
  when 'wait_audited'
    ['audit_rejected', 'wait_publish_notice'][state]
  when 'audit_approved'
    ['stop_wait_audited'][state]
  when 'stop_wait_audited'
    ['audit_approved', 'stoped'][state]
  when 'change_wait_audited'
    ['result_approved', 'change_wait_select'][state]
  when 'change_result_wait_audited'
    ['change_wait_select','change_result'][state]
  when 'invalid_audited'
    ['wait_purchaser', 'invalid_approved'][state]
  end
  res
end

def audit_project_state(pass:, reason: nil, state: nil)
  state = change_state(pass: pass) if state.nil?
  unless pass
    return [false, '审核理由不能为空'] if reason.nil?
    data = {}
    data['state'] = self.workflow_state
    data['reason'] = reason
    data['audit_time'] = Time.now
    self.update(:reason => self.reason << data)
  end

  self.update(workflow_state: state)
  [true, '保存成功']
end

```

#### ===与case语句

case语句在判断与when指定的值是否相等时,实际上使用的是 \=\=\= 运算符来判断的<br>
\=\=\= 还可以与=~一样判断正则表达式是否匹配<br>
\=\=\= 能表达更广义的 *相等*

## 异常和异常处理

```ruby
begin #开始

 raise.. #抛出异常

rescue [ExceptionType = StandardException] #捕获指定类型的异常默认值是 StandardException
 $! #表示异常信息
 $@ #表示异常出现的代码位置
else #其余异常
 ..
ensure #不管有没有异常,进入该代码块

end #结束
```

## BEGIN和END
## 线程、纤程和连续体

# 方法、Proc、Lambda和闭包

## 方法

方法是由对象定义的与该对象相关的操作.<br>
在Ruby中,对象的所有操作都被封装为方法.

### 根据方法名查询是否存在定义方法(respond_to?)

```ruby
object.respond_to?(:方法名)
```

### 方法的调用

对象.方法名(参数,...,参数n)<br>
上面的对象被称为接收者,在面向对象的世界中,调用方法被称为`向对象发送消息`,<br>
调用的结果就是`对象接收了消息`.

### 运算符形式的方法调用

有些方法看起来像运算符,如:<br>
四则运算等二元运算符,<br>
-(负号)等一元运算符等,<br>
指定数组,散列的元素下标的[],<br>
实际上都是方法.

### 方法的分类

根据接收者的不同,将方法分为3类:

0. 实例方法
0. 类方法
0. 函数式方法

#### 实例方法

以实例对象为接收者的方法称为实例方法.

#### 类方法

接收不是对象而是类本身的方法,称为类方法.

#### 函数式方法

***没有接收者的方法***,称为函数式方法. 如:
```ruby
print 'hello world.'
```
### 方法的定义及返回值

定义方法的一般语法:
```ruby
def 方法名(参数1,参数2 = 值,...,参数n)
  希望执行的处理
  return 值 if 判断条件
end
```

参数2的写法为定义参数的默认值.<br>
参数的默认值是指在没有指定参数的情况下调用方法时程序中参数默认使用的值.

用return语句来指定方法的返回值.<br>
如果省略return语句,此时方法的最后一个表达式的结果就会成为方法的返回值.<br>
除了返回结果,return还可以用来终止程序的执行并返回return后面的值,如果后面有值的话.<br>

方法的目的是程序处理,所以Ruby允许没有返回值的方法,也就是返回值为nil的方法.

#### 具名参数

> 当调用一个方法时,不得不按照固定的顺序传入参数,当参数很多时就会很容易出错.<br>
在ruby中可以使用键值序列来当做参数传入,这些参数会被包装成一个hash传入.

```ruby
def my_method(args)
  puts args[:a]  # => 1
end
my_method(:a => 1, :b => 2, :c => 3)
#在ruby 1.9以后也可以这样写
my_method(a:1, b:2, c:3)
```

### 定义带块的方法
```ruby
def myloop
  while true
    yield
  end
end
```
yield时是定义带块方法时的关键字,调用方法时通过块传进来的处理会在yield定义的地方执行.
```ruby
num = 1
myloop do
  puts "#{num}"
  num += 1
end
```

### 参数不确定的方法
```ruby
def foo(arg, *args, bar)
  [arg, args, bar]
end

p foo(1, 2) # => [1, [], 2]
p foo(1, 2, 3) # => [1, [2], 3]
p foo(1, 2, 3, 4) # => [1, [2, 3], 4]

def test(*args)
  p args
end

test(date: :desc, year: :asc)
# => [{:date=>:desc, :year=>:asc}]
```
`*变量名`这种形式的参数 ***只能在方法定义的参数列表中出现一次***

### 关键字参数方法

在上面介绍的方法定义中,都调用方法时需要定义参数的个数以及参数的顺序.<br>
而使用关键字参数,则可以将参数名和参数值成对地传给方法内部使用.

```ruby
def bar(a, x: 'x', y:, **args)
  [x, args, y, a]
end

p bar('a', y: 'y', k: 'k', v: 'v' ) # => ["x", {:k=>"k", :v=>"v"}, "y", "a"]
# 用散列传递参数
hash = {y: 'y', v: 'v', k: 'k'}
p bar('a', hash) # => ["x", {:v=>"v", :k=>"k"}, "y", "a"]
```
如果不需要指定默认值,则可以像参数y这样指定参数名即可,省略了默认值的参数在调用时不可以被省略.

为了避免调用方法时因指定了未定义的参数而报错.<br>
可以使用`**变量名`的形式来接收未定义的参数,如参数args所示,<br>
而且只能放在方法参数的末尾,后面定义其他参数会报语法错误.<br>

### 获取方法所有参数及其参数值

```ruby
# parameters方法获取该方法的参数列表
# 获取foo方法的参数列表
method(:foo).parameters

# 特殊变量__method__来获取当前方法的名称
method(__method__).parameters

# 获取参数对应的值
binding.local_variable_get(arg)

# 获取本方法参数及其对应值
method(__method__).parameters.map do |arg| 
  name = arg[1].to_s
  [name, binding.local_variable_get(name)]
end 
```

将方法参数及其值转化为哈希并包装为OpenStruct对象:

```ruby
def self.expose_index(index_name:, type_name:, desc:, permit: [], expose: false)
  args = method(__method__).parameters.map do |arg| 
    name = arg[1].to_s
    [name, binding.local_variable_get(name)]
  end
  args = Hash[args]
  args.merge!(index_type: (args['index_name']+'/'+args['type_name']))
  OpenStruct.new(args)
end
```

### 将数组分解为参数

在调用方法的时候,如果以`*数组`的形式指定参数,这时传递给方法的就不是数组本身,而是数组的各个元素被依次传递给方法.

```ruby
def foo(a, b, c)
  a + b + c
end

p foo(1, 2, 3) # => 6

*args = [1, 2, 3]
p foo(*args) # => 6
p foo(*[1, 2, 3]) # => 6


p foo(*[1, 2, 3, 4]) # => ArgumentError: wrong number of arguments (given 4, expected 3)

def foo(a,b,*args)
  a + b + args.sum
end

p foo(*[1, 2, 3, 4]) # => 10
```
上面谈及多重赋值的时候有举到emall_code和sku的例子.<br>
当时是这样实现的:
```ruby
emall_code, *sku = array
```
这里有了新的实现方式:
```ruby
def bar(emall_code, *sku)
...
end
bar(*array)
```

### 方法可见性
#### Public
#### Protected
#### Private

## Proc和Lambda
## 闭包
## Method对象
## 函数式编程

# 类和模块

## 类

类(class)表示的就是对象的种类.<br>
对象拥有哪些特性都是由类来决定的.<br>
**Ruby中所有的类都是Class类的对象,因此Class类的实例方法以及类对象中所添加的单一方法都是类方法.**<br>
Class是一个对象,Object是一个类.

### 对象

表现数据的基本单位称为`对象`.

名称|说明
-|-
数值对象|表示数字的对象
字符串对象|表示字符序列的对象
数组&散列对象|表示多个数据集合的对象
时间对象|表示时间的对象
文件对象|表示对文件进行读写操作的对象
符号对象|表示用于识别方法等名称的标签对象
正则表达式对象|表示匹配模式的对象

### 对象与类的对应表
对象|类
-|-
数值|Numeric
字符串|String
数组|Array
散列|Hash
时间|Time
文件|File
符号|Symbol
正则表达式|Regexp
### 类和实例
类表示对象的种类,Ruby中的对象一定都属于某个类.<br>
常说的数组对象,数组实际上都是Array类的对象(实例).<br>
相同类的对象所使用的方法也相同,类就像是对象的雏形或设计图,决定了对象的行为.

在生成新的对象时,一般会用到各类的new方法.如Array.new可以生成新的数组对象.<br>
像数组和字符串这种类也可以使用[1, 2, 3]和'abc'这种字面量来生成对象.

当判断某个对象是否属于某个类时,可以使用class和instance_of?方法.
```ruby
[1, 2, 3].class # => Array
[1, 2, 3].instance_of?(Array) # => true
```
### 对象和实例
X X类的对象,一般也会说成X X类的 **实例(instance)**.<br>
所有的Ruby对象其实都是某个类的实例,因此Ruby中的对象和实例意义几乎时一样的.
### 继承
通过扩展已定义的类来创造新类称为继承.<br>
继承后创建的新类称为子类(subclass),被继承的类称为父类(superclass).

通过继承可以实现以下操作:

0. 在不影响原有功能的前提下追加新功能.
0. 重新定义原有功能,使名称相同的方法产生不同的效果.
0. 在已有的功能的基础上追加处理,扩展已有功能.

BasicObject类是Ruby中所有类的父类,它定义了作为Ruby对象的最基本功能.<br>
BasicObject类是最基础的类,甚至连一般对象需要的功能都没有定义,因此普遍对象所需要的类一般都被定义为Object类.

#### 基类继承关系
> BasicObject
>> Object
>>> Array<br>
>>> String<br>
>>> Hash<br>
>>> Regexp<br>
>>> Dir<br>
>>> Time<br>
>>> Exception

>>> IO
>>>> File

>>> Numeric
>>>> Float<br>
>>>> Complex<br>
>>>> Rational<br>
>>>> Interger
>>>>> Fixnum<br>
>>>>> Bignum

子类和父类的关系称为`is_a关系`.<br>
根据类的继承关系追查对象是否属于某个类时,则可以使用is_a?方法.
```ruby
'string'.is_a?(String) # => true
'string'.is_a?(Object) # => true

'string'.instance_of?(String) # => true
'string'.instance_of?(Object) # => false
```
### 创建类
```ruby
class HelloWorld                  # class 语句
  def initialize(myname: 'Ruby')  # initialize 方法
    @name = myname
  end

  def hello                       # 实例方法
    puts "Hello,world.I am #{@name}."
  end
end

avicii = HelloWorld.new(myname: 'Avicii')
avicii.hello # => Hello,world.I am Avicii.

ruby = HelloWorld.new
ruby.hello # => Hello,world.I am Ruby.
```
#### class 语句
```ruby
class 类名
  类的定义
end
```
类名的首字母必须大写.
### initialize 方法
**在class语句中定义的方法为该类的实例方法.**<br>
名为initialize的方法比较特别.<br>
使用new方法生成对象的时候,initialize方法会被调用,new方法的参数也会原封不动地传递给initialize方法.

### 存取器

在Ruby中,从对象外部是不能直接访问实例变量或对实例变量赋值,需要定义以下方法:

```ruby
class HelloWorld
  ...
  def name         # 获取 @name
    @name
  end

  def name=(value) # 修改 @name
    @name = value
  end
  ...
end

# 第一个方法name只是简单的返回@name的值,可以像访问属性一样使用该方法.
p avicii.name # => Avicii

# 第二个方法名为name=,使用方法如下:
avicii.name = 'Tim Berg'
```
利用这样的方法,就可以打破Ruby原有的限制,从外部自由的访问对象内部的实例变量.

当对象有很多实例变量时,如果逐个定义存取器,就会变得难懂而且容易写错.<br>
Ruby提供了更简便的定义方法attr_reader<sup>[1]</sup>,attr_writer,attr_accessor.<br>
只要指定实例变量的符号,Ruby会自动帮定义相应的存取器.

定义|含义
-|-
attr_reader :name|只读(定义name方法)
attr_writer :name|只写(定义name=方法)
attr_accessor :name|读写(定义上面两个方法)

[1]: RUby中一般把设定实例变量的方法称为writer,读取实例变量的方法称为reader,这两个方法合称accessor.有时也把reader称为getter,writer称为setter,合称accessor(method).一般把accessor(method)译为存取器或访问器.

### 特殊变量 self

在实例方法中可以用self这个特殊的变量来引用方法的接收者.

```ruby
class HelloWorld
  attr_accessor :name, :years
  ...
  def foo
    # self.name 引用了调用foo方法时的接收者.
    puts "Hi, I am #{self.name}"
  end
  ...
end
```
***调用方法时如果省略了接收者,Ruby就会把self作为该方法的接收者.***

在调用像name=方法这样以=号结束的方法时,不能以缺省接收者的方式调用name=方法,需要显示的去调用.
```ruby
def bar
  name = 'Ruby'      # 为局部变量赋值
  self.name = 'Ruby' # 调用name=方法
end
```

### 类方法

方法的接收者就是类本身(类对象)的的方法称为类方法.<br>
**类方法的操作对象不是实例,而是类本身.**
创建类方法的几种形式:

```ruby
# class << 类名 ~ end 这种写法的类定义称为单例类定义,其中定义的方法称为单例方法.
class << HelloWorld
  def hello(name:)
    puts "#{name} said hello."
  end
end
HelloWorld.hello(name: 'Avicii') # => Avicii said hello.

class HelloWorld
  class << self
    def hello(name:)
      puts "#{self.name} said hello."
    end
  end
end

class HelloWorld
  def HelloWorld.hello(name:)
    puts "#{name} said hello."
  end
end

class HelloWorld
  def self.hello(name:)
    puts "#{name} said hello."
  end
end

```

### 类变量

> 以@@开头的变量称为类变量.<br>
类变量是该类所有实例的共享变量.这一点与常量类似,不同的是类变量可以多次进行修改.<br>
与实例变量一样,从类的外部访问类变量时也需要存取器.但不能使用attr_accessor等存取器,需要直接定义.

```ruby
class HelloWorld
  @@count = 0
  def HelloWorld.count
    @@count
  end
  ...
end
```

### 限制方法的调用

访问级别|含义
-|-
public|以实例方法的形式对外公开该方法.
private|在指定接收者的情况下不能调用该方法.<br>只能使用缺省接收者的方式调用该方法,因此无法从实例的外部访问.
protected|在同一个类(及其子类)中时可以将该方法作为实例方法调用.

没有指定访问级别的方法默认为public,但initialize方法是个例外,它通常被定义为private.

### 扩展类

#### 在原有类的基础上添加方法

Ruby允许在已经定义好的类中添加方法.

```ruby
class String
  # 为Srting类追加计算字符串单词数的实例方法
  def count_word
    ary = self.split(/\s+/) # 用空格分割self
    return ary.size         # 返回分割后的数组元素总数
  end
end
```

#### 继承

利用继承可以在不修改已有类的前提下,通过增加新功能或重新定义已有功能等来创建新的类.

```ruby
class 类名 < 父类名
  类定义
end
```

RingArray类将重定义读取数组内容时使用的[]运算符.

```ruby
class RingArray < Array  # 指定将要继承的父类
  def [](i)              # 重定义运算符[]
    idx = i % self.size  # 计算新索引值
    super(idx)           # 调用父类中的同名方法
  end
end
```

RingArray类中重新定义的[]运算符会在索引超出数组长度时,就会将溢出部分开始重新计算索引.

```ruby
6 % 7  # => 6
16 % 7 # => 2
```

利用继承可以将共同的功能定义在父类.把各自独有的功能定义在子类.<br>
定义类时如果没有指定父类,会默认该类为Object类的子类.<br>
Object类提供了很多便于实际编程的方法,如果希望使用更轻量级的类,可以使用BasicObject作为父类.<br>
BasicObject只提供了作为Ruby对象的最低限度的方法.<br>
类对象调用instance_methods方法后,会以符号的形式返回该类的实例方法列表.

### alias

有时希望给已经存在的方法设置别名.<br>
例如Array#size和Array#length

```ruby
alias 别名 原名   # 使用方法名
alias :别名 :原名 # 使用符号名
```

下面将定义HelloWorld以及其子类AliasHelloWorld,在子类中即将会对hello方法设置别名并重定义该方法.

```ruby
class HelloWorld
  def hello
    'Hello, my friend.'
  end
end

class AliasHelloWorld < HelloWorld
  alias alias_hello hello
end
alias_class = AliasHelloWorld.new
p alias_class.alias_hello # => "Hello, my friend."
p alias_class.hello       # => "Hello, my friend."

class AliasHelloWorld < HelloWorld
  alias alias_hello hello
  def hello
    "#{alias_hello} Nice to meet you."
  end
end
alias_class = AliasHelloWorld.new
p alias_class.alias_hello # => "Hello, my friend."
p alias_class.hello       # => "Hello, my friend. Nice to meet you."
```

### undef

undef用于删除已定义的方法,例如在子类中如果希望删除父类中定义的方法时,可以用undef方法.

```ruby
undef 方法名
undef :方法名
```

### 单例类

单例类中的方法,也就是单例方法是只存在于这个对象的方法,同类的其他对象则没有这个方法.<br>
起始类方法就是类的单例方法,所有的类都是Class类的实例,而类方法就是该实例的单例方法.

```ruby
str1 = 'Ruby'
str2 = 'Ruby'

class < str1
  def hello
    "Hello, #{self}"
  end
end

p str1.hello # => "Hello, Ruby."
p str2.hello # => NoMethodError: undefined method `hello'
```

## 子类化和继承

## 对象创建和初始化

## 模块

如果说类表现的是事物的实体(数据)及其行为(处理),那么模块表现的只是事物的行为部分.<br>
模块与类的不同点:

0. 模块不能有拥有实例
0. 模块不能被继承

### 利用Mix-in扩展功能

Mix-in是将模块混合到类中,在定义类时使用include,模块中的方法,常量就能被类使用.<br>
Mix-in可以灵活的解决下面的问题:

0. 虽然两个类拥有相似的功能,但是不希望把它们作为相同的种类(Class)来考虑.
0. Ruby不支持父类的多重继承,因此无法为已经继承的类添加共通的功能.


### 加载(include)和请求(require)模块

方法|特性
-|-
include|相当于把模块中定义的方法插入到类中<br>它允许使用 mixin
require|允许载入一个库并且会阻止加载多次


### include和extend

```ruby
module A
  def my_method
    puts "method in A"
  end
end

class B
  include A
end

class C
  extend A
end
```

B将新增一个**实例方法**my_method,<br>
C将新增一个**类方法**my_method,<br>
同时,B的祖先链(ancestors)中将增加A,而C的祖先链中没有A.

### include和prepend

prepend与include类似,首先都是添加实例方法的,不同的是扩展module在祖先链上的放置位置不同:

```ruby
module A
  def my_method
    puts "in A"
  end
end

class B
  include A
  def my_method
    puts "in B"
  end
end

class C
  prepend A
  def my_method
    puts "in C"
  end
end

B.new.my_method   #输出 ‘in B’
C.new.my_method   #输出 ‘in A’

puts B.ancestors
#输出
#B
#A
#Object
#Kernel
#BasicObject

puts C.ancestors
#输出
#A
#C
#Object
#Kernel
#BasicObject
```

## 单键方法和Eigenclass
## 方法查找
## 常量查找

# 反射和元编程
## 类型、类和模块
## 对字符串和块进行求值
## 变量和常量
## 方法
## 钩子方法

### method_missing

> 捕捉未定义方法,并进行处理

```ruby
class Person
  def method_missing(sym, *args)
     "#{sym} not defined on #{self}"
  end

  def name
    "My name is Person"
  end
end

p = Person.new

puts p.name     # => My name is Person
puts p.address  # => address not defined on #<Person:0x007fb2bb022fe0>
```

### singleton_class

用于操纵类方法

```ruby
singleton_class.define_method "get_base_info" do
  p 'ok'
end
```

### define_method

> 动态定义 类/实例 方法:

```ruby
Label = Class.new

class Label
  class << self
    te = 'test'
    define_method te do
      p te
    end
  end

  define_method 'he' do
    p 'hello'
  end

  define_method :he do |he: 'hello'|
    p he
  end
end

Label.test # => "test"
Label.new.he # => "hello"
```

#### 实例

```ruby
def used_count(*args, &block)

  def day(*args, &block)
    alias :used_count_day :day
    p "used_count_day: #{args}"
  end

  define_method(:used_count_day) do |*args, &block|
    day(*args, &block)
  end

  p "used_count: #{args}"
end

used_count('2020-02-13')
used_count_day('2020-02-13')
used_count.day('2020-02-13')
```

### instance_methods (Module)

> 该方法收集在类或模块中定义的实例方法的名称,并将它们作为数组返回.

#### 为模块中的每个方法调用指定代码

```ruby
module M
  def self.before(*names)
    names.each do |name|
      m = instance_method(name)
      define_method(name) do |*args, &block|
        yield
        m.bind(self).(*args, &block)
      end
    end
  end
end

module M
  def hello
    puts "yo"
  end

  def bye
    puts "bum"
  end

  before(*instance_methods) { puts "start" }
end

class C
  include M
end

C.new.bye # => "start" "bum"
C.new.hello # => "start" "yo"
```

### method_defined?



### private_instance_methods



### protected_instance_methods



### public_instance_methods



### methods (Object)



### class_eval

> Module#class_eval方法(别名: module_eval),会在一个已存在的类的上下文中执行一个块. <br>
使用该方法可以在不需要class关键字的前提下,打开类.

### instance_eval

> Module#class_eval与Object#instance_eval方法相比,后者instance_eval方法只能修改self,而class_eval方法可以同时修改self与当前类.

### instance_eval 与 class_eval的选择

> 通常使用instance_eval方法打开非类的对象,而用class_eval方法打开类定义,然后用def定义方法.

### inherited

> 当父类被其他子类继承时 inherited 类方法将会被调用.

```ruby
def self.inherited(child_class)
  child_class.class_eval do |klass|
    def klass.ejection
      puts "#{self}"
    end
  end
end
```

### append_features

```ruby
class Order < ActiveRecord::Base
  include SqlCondition
end


module SqlCondition
  def self.append_features(target)
    target.class_eval do
      include "SqlCondition::#{target.to_s}".constantize
    end
  end

  module Order
    extend self
    def test
      p 'test'
    end
  end

end

Order.new.test # => "test"

module SqlCondition
  def self.append_features(target)
    target.class_eval do
      extend "SqlCondition::#{target.to_s}".constantize
    end
  end

  module Order
    def test
      p 'test'
    end
  end

end

Order.test # => "test"
```

### included

>  included 可以当作 include 之后的的一个 callback,可以在这里 hook 一些定制代码.

```ruby
module RelationUuid
  def self.included(target)
    target.class_eval do

      has_one :uuid_, as: :uuidable, :dependent => :destroy

      after_initialize :init_columns
      before_save :validate_columns
      # ...
    end
  end
end

class Label < ApplicationRecord
  include RelationUuid
  # ...
end
```

### instance_variable_set

> 动态设置实例变量

```ruby
def find_data_by_id(object: , var: )
  return unless params[:id].present?

  tmp = object.constantize.find(params[:id])

  self.instance_variable_set("@#{var}", tmp)
end
```
### const_get

```ruby
const_get(sym, inherit=true) → objclick to toggle source
const_get(str, inherit=true) → obj
```

> 在mod中检查给定名称的常量.<br>如果设置了inherit,则查找还将搜索祖先(如果mod是模块,则搜索对象).<br>
如果找到定义,则返回常量的值,否则将引发名称错误.

```ruby
def load_resource
  class_name = controller_name.singularize.classify
  scope = Wdba.const_get(class_name) rescue nil
  if scope
    resource = if ['new', 'create'].include?(params[:action].to_s)
      scope.new
    elsif params[:id]
      scope.find(params[:id])
    else
      nil
    end
    instance_variable_set("@#{controller_name.singularize}", resource)
  end
end
```

### Proc

Proc是由块转换来的对象.<br>创建一个Proc共有四种方法,分别是:

```ruby
# 法一
inc = Proc.new { | x | x + 1}
inc.call(2)  # => 3

# 法二
inc = lambda {| x | x + 1 }
inc.call(2)  # => 3

# 法三
inc = ->(x) { x + 1}
inc.call(2) # => 3

# 法四
inc = proc {|x| x + 1 }
inc.call(2) # => 3
```

#### &

> &符号的含义是: 这是一个Proc对象,现在想把它当成代码块来使用.<br>
去掉&符号,将能再次得到一个Proc对象.

```ruby
def my_method(&the_proc)
  the_proc
end

p = my_method {|name| "Hello, #{name} !"}
p.class  # => Proc
p.call("Bill")  # => "Hello,Bill"


def my_method(greeting)
  "#{greeting}, #{yield}!"
end

my_proc = proc { "Bill" }
my_method("Hello", &my_proc)
```

#### 实例1

写一个操作方法类似attr_accessor的attr_checked的类宏,该类宏用来对属性值做检验:

```ruby
def add_checked_attribute(klass, attribute, &validation)
  klass.class_eval do
    define_method "#{attribute}=" do |value|
      raise "Invaild attribute" unless validation.call(value)
      instance_variable_set("@#{attribute}", value)
    end

    define_method attribute do
      instance_variable_get "@#{attribute}"
    end

  end
end

add_checked_attribute(String, :my_attr){|v| v >= 180 }
t = "hello,kitty"

t.my_attr = 100  #Invaild attribute (RuntimeError)
puts t.my_attr

t.my_attr = 200
puts t.my_attr  #200
```

### on_load

用于gem/engine中,可以延迟加载代码,在真正需要时才加载.

如, 当engine加载过程中向宿主应用写入方法:

```ruby
ActiveSupport.on_load :active_record do
  send :extend, NavigationLight::OnLoad
end
```

## 跟踪
## ObjectSpace和GC
## 定制控制结构
## 缺失的方法和常量
## 动态创建方法
## 别名链
## 领域特定语言

# Ruby平台

## 终端工具(IRB)

```ruby
#!/usr/bin/env ruby
require 'irb'

IRB.start
```

## 正则表达式

## 文件

### 常用方法

常用方法|说明
-|-
File::exist?(path)|如果 path 存在,则返回 true.
File::file?(path)|如果 path 是一个普通文件,则返回true.<br>如果路径不存在不会报错,而是返回false.
File::directory?(path)|判断路径是否为文件夹
File::rename(old, new)|改变文件名 old 为 new.
File::size(path)|返回 path 的文件大小.
File::split(path)|返回一个数组,包含 path 的内容,path 被分成 File::dirname(path) 和 File::basename(path)
File::writable?(path)|如果 path 是可写的,则返回 true
File::ftype(path)|返回一个字符串,表示文件类型<sup>1</sup>
File::join(item...)|返回一个字符串,由指定的项连接在一起<sup>2</sup>
File::new(path[, mode="mode"])<br>File::open(path[, mode="mode"])<br>File::open(path[, mode="mode<sup>3</sup>"]){\|f\| ...}<br>|打开文件.如果指定了块,则通过传递新文件作为参数来执行块.<br>当块退出时,**文件会自动关闭**.
File::mtime(path)|返回 path 的最后一次修改时间
File::basename(path)|路径中的基本名称
File::dirname(path)|路径的目录部分

1:

类型|含义
-|-
file|普通文件
directory|目录
characterSpecial|字符特殊文件
blockSpecial|块特殊文件
fifo|命名管道(FIFO)
link|符号链接
socket|Socket
unknown|未知的文件类型

2:

File::join("", "home", "usrs", "bin") # => "/home/usrs/bin"

3:

模式|描述
-|-
r|**只读**模式.文件指针被放置在文件的开头.这是**默认**模式.
r+|**读写**模式.文件指针被放置在文件的**开头**.
w|**只写**模式.如果文件存在,则**重写**文件.<br>如果文件不存在,则创建一个新文件用于写入.
w+|**读写**模式.如果文件存在,则**重写**已存在的文件.<br>如果文件不存在,则创建一个新文件用于读写.
a|**只写**模式.如果文件存在,则文件指针被放置在文件的**末尾**.也就是说,文件是**追加**模式.<br>如果文件不存在,则创建一个新文件用于写入.
a+|**读写**模式.如果文件存在,则文件指针被放置在文件的**末尾**.也就是说,文件是**追加**模式.<br>如果文件不存在,则创建一个新文件用于读写.
b|二进制文件模式,在Windows上禁止EOL <-> CRLF转换.<br>和除非明确将外部编码设置为ASCII-8BIT指定.<br>***无法单独使用,必须与上面选项共同使用***.<br>如: rb/wb
t|文本文件模式.<br>***无法单独使用,必须与上面选项共同使用***.<br>如: rt/wt

### 文件读取流程

文件读取的流程大致如下:<br>

0. 打开文件
0. 读取文件内容
0. 针对文件内容进行操作
0. 关闭文件

```ruby
tmp = []
File::open(file_path, mode='r') do |file|
  file.each_line do |line|
    tmp << line
  end
end
tmp
```

读取文件方法|特性
-|-
read|读取文件内的全部数据<br>读取的文件内容会暂时放在内存中,文件较大时.程序可能因此崩溃.<br>需要手动关闭: file.close
each_line|逐行读取数据<br>只需要保存一行文件数据的内存空间

只是对文件进行了比较简单的操作,把每行的内容放入一个数组里面.<br>

>遍历文件夹内文件及查看具体文件内容

根据实际情况写了一个主要用于查看log/下文件的模块,可以用配置文件灵活的进行配置,关于yml文件读取及主要配置项在[这里]()

[Github-ff4c00 cat_file.rb](https://github.com/ff4c00/markdown/blob/master/code/ruby/cat_file.rb)

## 目录

### 常用方法

常用方法|说明
-|-
glob|

## 输入/输出

### 输出

#### puts和p与print方法的区别
名称|特性
-|-
puts|输出结果的末尾一定会输出换行符
p|数值结果和字符串结果会以不同形式输出<sup>[1]<sup>

[1]:

```
p 100   # => 100
p '100' # => "100"
```

#### 打印表格

```ruby
class Array
  def to_table
    column_sizes = self.reduce([]) do |lengths, row|
      row.each_with_index.map{|iterand, index| [lengths[index] || 0, iterand.to_s.length].max}
    end
    puts head = '-' * (column_sizes.inject(&:+) + (3 * column_sizes.count) + 1)
    self.each do |row|
      row = row.fill(nil, row.size..(column_sizes.size - 1))
      row = row.each_with_index.map{|v, i| v = v.to_s + ' ' * (column_sizes[i] - v.to_s.length)}
      puts '| ' + row.join(' | ') + ' |'
    end
    puts head
  end
end

a = [[1.23, 5, :bagels], [3.14, 7, :gravel], [8.33, 11, :saturn]]
a.to_table
```

#### 清空屏幕

```ruby
print "\e[2J\e[f"
```

## 网络
## 线程和并发

## 加密/编码

### 两者区别与联系

编码绝对不是加密,加密是可以算是一种编码的操作.<br>
编码和加密的区别说通俗一点,在于编码是通常希望别人解码的.而加密是不希望的.

编码更多的是为了转换格式,加密是为了安全.

### sha加密并base64编码示例

```ruby
def to_sha(key: nil)
  # sha加密
  digest = OpenSSL::Digest.new('sha1')
  digest.update key
  # 转base64编码
  res = Base64::encode64 [digest.hexdigest].pack('H*')
  # 间隔符 \n 要去掉
  res.gsub(/\n/, '')
end
```

在实际使用过程中发现编码所产生的 `\n` 需要去掉,不然接收方会判定校验不通过.具体原因及是否应该去掉有待深究.

### MD5值

```ruby
require 'digest'
Digest::MD5.hexdigest('字符串')
```

### ruby生成Java hash code

感谢秦阳的大力帮助:

```ruby
def force_overflow_signed(i)
  force_overflow_unsigned(i + 2**31) - 2**31
end

def force_overflow_unsigned(i)
  i % 2**32   # or equivalently: i & 0xffffffff
end

str.split('').map(&:ord).inject(0) {|sum, i| force_overflow_signed(force_overflow_signed(sum * 31) + i)}
```

## 命令行脚本

>  Unix 下的命令行工具都符合一个哲学,即 *作一件事并且把它做好*.

### 交互输入(gets)



### 参数项描述(OptionParser)

```ruby
#!/usr/bin/env ruby

require 'optparse'

options = {}
option_parser = OptionParser.new do |opts|
  # 这里是这个命令行工具的帮助信息
  opts.banner = 'here is help messages of the command line tool.'

  # Option 作为switch,不带argument,用于将 switch 设置成 true 或 false
  options[:switch] = false
  # 下面第一项是 Short option(没有可以直接在引号间留空),第二项是 Long option,第三项是对 Option 的描述
  opts.on('-s', '--switch', 'Set options as switch') do
    # 这个部分就是使用这个Option后执行的代码
    options[:switch] = true
  end

  # Option 作为 flag,带argument,用于将argument作为数值解析,比如"name"信息
  # 下面的"value"就是用户使用时输入的argument
  opts.on('-n NAME', '--name Name', 'Pass-in single name') do |value|
    options[:name] = value
  end

  # Option 作为 flag,带一组用逗号分割的arguments,用于将arguments作为数组解析
  opts.on('-a A,B', '--array A,B', Array, 'List of arguments') do |value|
    options[:array] = value
  end
end.parse!

puts options.inspect
```

```bash
$ ruby my_awesome_command.rb -h
here is help messages of the command line tool.
    -s, --switch                     Set options as switch
    -n, --name Name                  Pass-in single name
    -a, --array A,B                  List of arguments

$ ruby my_awesome_command.rb -s
{:switch=>true}

$ ruby my_awesome_command.rb -n Daniel
{:switch=>false, :name=>"Daniel"}

$ ruby my_awesome_command.rb -a Foo,Bar
{:switch=>false, :array=>["Foo", "Bar"]}
```

### 捕获信号(Signal)

当执行脚本过程中执行 Ctrl-C后,清空屏幕输入并退出脚本:

```ruby
Signal.trap("INT") { 
  print "\e[2J\e[f"
  exit
}
```

# Ruby环境
## 执行Ruby解释器
## 顶层环境
## 实用性信息抽取和产生报表的快捷方式
## 调用操作系统的功能
## 安全

# 算法

## 二分查找法

### 文档断点续读

> 文件中每行内容为ES索引文档内容,逐行读取文件内容创建ES文档.<br>
当逐行读取中断后,根据ES文档是否存在判断上次中断位置.

```ruby
def 求续读文件数组下标
  左下标 = 0
  右下标 = 文件总行数 - 1
  续读下标 = while (左下标 <= 右下标) do
    中位数 = (左下标 + 右下标) / 2
    if (ES索引包含(文件数组[数组下标]) && ES索引不包含(文件数组[数组下标+1]))
    break 中位数 + 1
    elsif(ES索引包含(文件数组[数组下标]))
      左下标 = 中位数 + 1
    elsif(ES索引不包含(文件数组[数组下标]))
      右下标 = 中位数 - 1
    end
  end
  return 0 unless 续读下标
  续读下标
end
```

```ruby
def judge_doc_present(es_search_url:, doc_id:)
  request_path = es_search_url
  body = {"query": {"ids": {"type": "_doc","values": [doc_id]}}}
  headers = { 'Content-Type': "application/json"}
  # print "请求ES: #{body}\n"
  res = RestClient::Request.execute(method: 'post', headers: headers, url: request_path, payload: body.to_json)
  # print "返回结果: #{res.body}\n"
  return false unless (res.code == 200)
  return false unless JSON.parse(res.body)['hits']['hits'].present?
  true
end

# 普通字符串md5sum和hexdigest无区别 但是关于json格式数据,shell字符串不支持,所以bash和ruby的加密值也不同
# md5sum=lambda{|str| `str=#{str.to_json};md5=$(echo -n "$str" |md5sum);echo -n ${md5:0:32}`}
def md5sum(str:)
  md5 = `str=#{str};md5=$(echo -n "$str" |md5sum);echo -n $md5`
  md5[0...32]
end

def get_breakpoint_index(file_path:, es_search_url:)
  array_file = File.readlines(file_path)
  judge_doc_present_ = lambda{|md5| judge_doc_present(es_search_url: es_search_url, doc_id: md5)}
  left_index = 0
  right_index = array_file.size - 1
  next_index = 0
  search_count = 0

  while (left_index <= right_index) do
    search_count+=1
    mind = (left_index + right_index) / 2    
    md5 = md5sum(str: array_file[mind].gsub("\n", '').to_json)
    doc_flag = judge_doc_present_.call(md5)
    if (doc_flag && !judge_doc_present_.call(md5sum(str: array_file[mind+1].gsub("\n", '').to_json)))
      print "#{search_count}. 正中目标, 当前中位数: #{mind}\n"
      next_index = mind + 1
      right_index = -1
    elsif(doc_flag)
      print "#{search_count}. 向后搜索, 当前中位数: #{mind}\n"
      left_index = mind + 1
    elsif(!doc_flag)
      print "#{search_count}. 向前搜索, 当前中位数: #{mind}\n"
      right_index = mind - 1
    end
  end
  next_index
end

get_breakpoint_index(file_path: '/home/ff4c00/Space/知识体系/应用科学/计算机科学/理论计算机科学/编程语言理论/Ruby/Gems/Nokogiri/Scout/outputs/standard_format_data.txt', es_search_url: 'http://localhost:9200/link_articles/_doc/_search')
```

# 参考资料

> [豆瓣读书 | Ruby编程语言](https://book.douban.com/subject/3329887/)

> [关于Rails的149个小技巧](https://til.hashrocket.com/rails)

> [SegmentFault | Ruby 中如何生成一个随机字符串?](https://segmentfault.com/q/1010000000669833)

> [ruby-lang | [BUG] Bus Error at 0xefce7b (armv7l) (ruby 2.3.4p301)](https://bugs.ruby-lang.org/issues/13670)

> [RubyChina | Ruby ruby include extend prepend 使用方法](https://ruby-china.org/topics/21501)

> [CSDN | ruby单词单数复数相互转换](https://blog.csdn.net/kiwi_coder/article/details/8134096)

> [ITranslater | regex - 将字符串转换为正则表达式ruby](https://www.itranslater.com/qa/details/2325755851484693504)

> [博客园 | Ruby:字符集和编码学习总结](https://www.cnblogs.com/happyframework/p/3275367.html)

> [RubyChina | RUBY 2.0 UTF-8 转 GBK 错误,怎么解决？](https://ruby-china.org/topics/22408)

> [Ruby | 用 OptionParser 构建 Command Line 工具](https://ruby-china.org/wiki/building-a-command-line-tool-with-optionparser)

> [iTranslater | 如何从Hash中删除一个键并获取Ruby / Rails中的剩余哈希？](https://www.itranslater.com/qa/details/2100910401243317248)

> [简书 | 译:Ruby动态方法](https://www.jianshu.com/p/8592b667a390)

> [Rubyリファレンス | instance_methods (Module)](https://ref.xaio.jp/ruby/classes/module/instance_methods)

> [stackoverflow | Executing code for every method call in a Ruby module](https://stackoverflow.com/questions/5513558/executing-code-for-every-method-call-in-a-ruby-module)

> [lazybios |《Ruby 元编程》读书笔记(六)](http://lazybios.com/2015/08/note-of-meta-pramgraming-with-ruby-6/)

> [简书 | ruby元编程(附录)](https://www.jianshu.com/p/7826a796c446)

> [RubyChina | Ruby 中一些重要的钩子方法](https://ruby-china.org/topics/25397)

> [RubyChina | append_features 与 included 的区别？](https://ruby-china.org/topics/5835)

> [GithubGist | Append features vs included](https://gist.github.com/paneq/3273049)

> [RubyChina | Ruby Hook Methods](https://ruby-china.org/topics/28727)

> [stackoverflow | How to change hash keys from \`Symbol\`s to \`String\`s?](https://stackoverflow.com/questions/10549554/how-to-change-hash-keys-from-symbols-to-strings)

> [rubyonrails | Inflector](https://api.rubyonrails.org/classes/ActiveSupport/Inflector.html)

> [RubyChina | active-support-on-load-hooks](https://ruby-china.github.io/rails-guides/engines.html#active-support-on-load-hooks)

> [stackoverflow | Best way to convert strings to symbols in hash](https://stackoverflow.com/questions/800122/best-way-to-convert-strings-to-symbols-in-hash)

> [apidock | instance_variable_set](https://apidock.com/ruby/Object/instance_variable_set)

> [简书 | Ruby 日期 & 时间(Date & Time)](https://www.jianshu.com/p/c8df26f01f21)

> [Apidock | group_by](https://apidock.com/ruby/Enumerable/group_by)

> [Github | Ruby 时间与String相互转化](https://icbd.github.io/wiki/ruby/2018/08/07/date-time-datetime-string.html)

> [CSDN | Ruby on Rails 入门之:(3) Ruby中的多种字符串表示方法](https://blog.csdn.net/watkinsong/article/details/8014185)

> [IBM Developer | 理解 Ruby Symbol,第 1 部分](https://www.ibm.com/developerworks/cn/opensource/os-cn-rubysbl/)

> [stackoverflow | What are the Ruby File.open modes and options?](https://stackoverflow.com/questions/3682359/what-are-the-ruby-file-open-modes-and-options)

> [stackoverflow | Installing Ruby 2.3.x on Ubuntu 18.04 is causing an error by the end of the installation process](https://stackoverflow.com/questions/50085258/installing-ruby-2-3-x-on-ubuntu-18-04-is-causing-an-error-by-the-end-of-the-inst/50339492)

> [honeybadger | Using splats to build up and tear apart arrays in Ruby
](https://www.honeybadger.io/blog/ruby-splat-array-manipulation-destructuring/)

> [stackoverflow | How do you pass arguments to define_method?](https://stackoverflow.com/questions/89650/how-do-you-pass-arguments-to-define-method)

> [CSDN | ruby 生成随机数 和 随机字符串](https://blog.csdn.net/wide288/article/details/37699451)

> [RubyChina | Ruby 从数组中随机选择若干元素](https://ruby-china.org/topics/6045)

> [stackoverflow | Can I create an array in Ruby with default values?](https://stackoverflow.com/questions/5324654/can-i-create-an-array-in-ruby-with-default-values)

> [rubyguides | Ruby String Formatting](https://www.rubyguides.com/2012/01/ruby-string-formatting/)

> [thinbug | 如何用另一个键替换哈希键](https://www.thinbug.com/q/6210572)

> [geeksforgeeks | Ruby | Hash select() method](https://www.geeksforgeeks.org/ruby-hash-select-method-2/)

> [CSDN | Ruby驼峰命名转蛇足命名 - underscore](https://blog.csdn.net/lissdy/article/details/89465766)

> [itranslater | ruby - 如何找到最大值哈希的键？](https://www.itranslater.com/qa/details/2325770534971245568)

> [apidock | find_index](https://apidock.com/ruby/Array/find_index)

> [apidock | flat_map](https://apidock.com/ruby/Enumerable/flat_map)

> [coderwall | Convert a complex nested hash to an object](https://coderwall.com/p/74rajw/convert-a-complex-nested-hash-to-an-object)

> [mlog | 在Ruby中生成没有时间的随机日期](https://mlog.club/article/1299573)

> [张鑫旭 | 原来浏览器原生支持JS Base64编码解码](https://www.zhangxinxu.com/wordpress/2018/08/js-base64-atob-btoa-encode-decode/)

> [stackoverflow | Difference between Encoding::BINARY and Encoding::ASCII-8BIT?](https://stackoverflow.com/questions/48255737/difference-between-encodingbinary-and-encodingascii-8bit)

> [stackoverflow | Ruby multiple string replacement](https://stackoverflow.com/questions/8132492/ruby-multiple-string-replacement)

> [stackoverflow | Console table format in ruby](https://stackoverflow.com/questions/36156305/console-table-format-in-ruby)

> [ruby-forum | Clear screen](https://www.ruby-forum.com/t/clear-screen/92978)

> [Github | How to catch SIGINT and SIGTERM signals in Ruby](https://gist.github.com/sauloperez/6592971)

> [apidock | strftime](https://apidock.com/ruby/Time/strftime)

> [stackoverflow | How to “pretty” format JSON output in Ruby on Rails](https://stackoverflow.com/questions/86653/how-to-pretty-format-json-output-in-ruby-on-rails)

> [seancdavis | Add a Console to your Ruby Project](https://www.seancdavis.com/blog/add-console-to-ruby-project/)

> [stackoverflow | How do I populate an array with random numbers?](https://stackoverflow.com/questions/24944083/how-do-i-populate-an-array-with-random-numbers)

> [维基百科 | 二分查找算法](https://zh.wikipedia.org/wiki/%E4%BA%8C%E5%88%86%E6%90%9C%E5%B0%8B%E6%BC%94%E7%AE%97%E6%B3%95)

> [stackoverflow | Is there a way to access method arguments in Ruby?](https://stackoverflow.com/questions/9211813/is-there-a-way-to-access-method-arguments-in-ruby)

> [linuxtut | [Ruby] When you want to replace multiple characters](https://linuxtut.com/ruby-when-you-want-to-replace-multiple-characters-fac5a/)

> [stackoverflow | Ruby equivalent to JavaScript’s encodeURIComponent that produces identical output? [duplicate]](https://stackoverflow.com/questions/2858345/ruby-equivalent-to-javascript-s-encodeuricomponent-that-produces-identical-outpu)