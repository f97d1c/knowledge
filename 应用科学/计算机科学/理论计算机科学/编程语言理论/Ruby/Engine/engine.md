> 引擎可以被认为是一个可以为其宿主提供函数功能的中间件.<br>
一个Rails应用可以被看作一个"超级给力"的引擎,因为Rails::Application类是继承自 Rails::Engine的.

<!-- TOC -->

- [说明](#说明)
  - [什么是Gem](#什么是gem)
  - [引擎和插件](#引擎和插件)
  - [中划线和下划线](#中划线和下划线)
  - [README.md](#readmemd)
    - [README驱动开发](#readme驱动开发)
  - [何时发布](#何时发布)
  - [语义化版本](#语义化版本)
  - [为什么要包含一个更新日志?](#为什么要包含一个更新日志)
  - [core_ext.rb](#core_extrb)
    - [大量的方法](#大量的方法)
  - [命名空间](#命名空间)
  - [Gem加载过程](#gem加载过程)
  - [内嵌测试应用](#内嵌测试应用)
- [目录结构](#目录结构)
  - [lib](#lib)
  - [入口文件](#入口文件)
  - [LICENSE.txt](#licensetxt)
  - [Rakefile](#rakefile)
  - [Gemspec](#gemspec)
  - [Gemfile](#gemfile)
  - [lib/gem_name/version.rb](#libgem_nameversionrb)
  - [lib/gem_name.rb](#libgem_namerb)
- [初始化](#初始化)
  - [生成引擎](#生成引擎)
  - [engine_name.gemspec](#engine_namegemspec)
  - [Gemfile](#gemfile-1)
  - [bundle install](#bundle-install)
- [宿主 -> 引擎](#宿主---引擎)
  - [应用挂载引擎](#应用挂载引擎)
    - [Gemfile指定引擎](#gemfile指定引擎)
    - [添加路由](#添加路由)
    - [bundle install](#bundle-install-1)
    - [执行引擎迁移](#执行引擎迁移)
      - [引擎迁移复制到应用中](#引擎迁移复制到应用中)
    - [运行迁移](#运行迁移)
    - [执行种子数据](#执行种子数据)
  - [应用调用引擎服务](#应用调用引擎服务)
    - [覆盖引擎模型和控制器](#覆盖引擎模型和控制器)
      - [修饰器模式](#修饰器模式)
        - [引擎实现装饰器模式](#引擎实现装饰器模式)
        - [宿主应用实现装饰器模式](#宿主应用实现装饰器模式)
      - [通过ActiveSupport::Concern模块实现装饰器模式](#通过activesupportconcern模块实现装饰器模式)
    - [覆盖视图](#覆盖视图)
    - [调用路由](#调用路由)
    - [加载引擎的静态资源文件](#加载引擎的静态资源文件)
    - [独立加载静态资源文件及预编译](#独立加载静态资源文件及预编译)
    - [安装引擎的其他gem依赖](#安装引擎的其他gem依赖)
- [引擎 -> 宿主](#引擎---宿主)
  - [引擎调用宿主应用服务](#引擎调用宿主应用服务)
    - [调用宿主应用控制器](#调用宿主应用控制器)
    - [在引擎中配置所使用的宿主应用中的类](#在引擎中配置所使用的宿主应用中的类)
- [Hook](#hook)
  - [Active Support on_load 钩子](#active-support-on_load-钩子)
- [Helper](#helper)
- [其他](#其他)
  - [命令行可执行文件](#命令行可执行文件)
    - [bin/目录](#bin目录)
    - [选项解析](#选项解析)
  - [使用irb创建Ruby交互环境](#使用irb创建ruby交互环境)
      - [加入rake任务](#加入rake任务)
  - [发布](#发布)
    - [前提条件](#前提条件)
      - [注册RubyGems帐号](#注册rubygems帐号)
    - [rake build(打包)](#rake-build打包)
    - [gem push(推送)](#gem-push推送)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## 什么是Gem

Ruby gem 是一个可以复用的打包好的 Ruby 应用程序或者类库,它专注于特定的功能.

## 引擎和插件

引擎还和插件密切相关.<br>
两者具有相同的lib/目录结构,并且都使用 `rails plugin new` 生成器来生成.<br>
区别在于, **引擎被 Rails 视为完整的插件** (通过传递给生成器的 --full 选项可以看出这一点).<br>
在这里实际使用的是 --mountable 选项,这个选项包含了 --full 选项的所有特性. ***完整的插件简称为引擎*** .<br>
也就是说,引擎可以是插件,插件也可以是引擎.

## 中划线和下划线

在项目命名方面,中划线的常用意义是创建一个已有gem的扩展.

## README.md

一个 README 文件的最简形式, 需要至少能够回答下面几个问题:

0. 它是什么
0. 我如何使用它
0. 我如何作贡献

额外的部分比如系统要求,安装,作者,贡献者等,没人会责怪在REAME里写太多的东西.

### README驱动开发

先写 README 的理由:

0. 它迫使预先定义公共 API (宿主应用可以调用的方法),这会减少过程工程的倾向.
0. 它把烦人的文档部分放到了前面.<br>当完成代码并且要发布gem时,最后要做的是撰写文档.<br>说真的,这一点也不好玩.<br>所以如果可以把这件事挪到前面来做,这样处境就会好些.
0. 会忘记我的 gem 是如何工作的…… 所以如果想要其他人来使用它,应该写一个 README 来说明安装,用法和如何做贡献(如果欢迎贡献者).

## 何时发布

> 更小更频繁的发布作为主流方式不无道理,有bug就发布一个补丁.

频繁的发布小的改变.

## 语义化版本

版本格式:主版本号.次版本号.修订号<br>
版本号递增规则如下:

0. 主版本号:当做了不兼容的 API 修改,
0. 次版本号:当做了向下兼容的功能性新增,
0. 修订号:当做了向下兼容的问题修正.

假设维护一个当前版本是1.5的gem.<br>
如果打算加入一个功能而 **不影响** (这点很重要)gem的其他部分的工作方式,<br>
应该发布一个次要的版本(在原来的基础上递增),版本:1.6.0

假设在1.6版本增加了新的功能,之后意识到新的代码有一个bug.<br>
修复了这个bug并且随后发布了一个补丁版本,应该是:1.6.1

在一年的开发后,认识到gem的某个部分非常糟糕并且想要重构.<br>
在重构的过程中,已经决定永久的改变公共接口.<br>
因为这些改变不会向后兼容,将会发布一个新的主版本2.0.0

## 为什么要包含一个更新日志?

查看一个项目的提交列表不一定总是能展示所有的历史.<br>
由于这个原因,很多项目都维护一个文件CHANGELOG.md或者CHANGES.md在根目录下.<br>
这个更新日志记录了一个用户应该关心的不同版本的API和特性.<br>
就像 README.md 文件一样,这个文件通常使用markdown来书写内容,使得它易于在 Github 上被查看.

例如:

```markdown
 0.3.1 #版本号作为标题,所有的关于这个版本的更新都使用要点和示例代码(如果必要)来记录
  -----------

  - Fix location of inline testing library

  0.3
  -----------

  - Now includes a testing library that will run all jobs synchronously.
```

## core_ext.rb

Ruby让扩展标准类库变得非常容易.比如,如果想要加入#foo方法到String类中,只要这样做:

```ruby
class String 
  def foo 
    # do some thing
  end
end
```

到处加入核心扩展不是一个好主意.事实上,在大部分情况下,创建一个单独的类在gem的命名空间下来加入额外的功能是一个更好的选择.

如果只加入少数方法到少数类中,通常采用的实践是创建一个叫做 `core_ext.rb` 的文件在gem的lib/目录下.

```ruby
# lib/gem_name/core_ext.rb

class String 
  def foo 
    # do some thing
  end
end
```

### 大量的方法

需要加入不止一些的方法到 Ruby 的核心类库,比较好的做法是把它们分离到一个单独的文件,每个文件对应它们要打的猴子补丁的类.<br>
如果这样的话,那么core_ext.rb文件就做一件事,加载将会创建的独立的扩展类.

```ruby
# lib/gem_name/core_ext.rb

require "gem_name/core_ext/array"
require "gem_name/core_ext/string"
```

\#foo的实现就和以前一样,仅仅是在不同的文件中而已:

```ruby
# lib/gem_name/core_ext/core_ext.rb

class String 
  def foo 
    # do some thing
  end
end
```

注意这个文件是放在lib/gem_name/core_ext/目录中的.<br>
这是一个常用实践对于命名空间任何类库包含多个子类.也注意下虽然扩展是在 lib/gem_name/目录下,<br>
这个类不是一个GemName命名空间的派生.<br>
如果是的话,就会创建一个完全新的String类在命名空间内,而不是Ruby标准库里的String类了.

由于lib/gem_name/core_ext.rb文件已经完成了加载单独扩展的任务,<br>
主文件lib/gem_name.rb加载lib/gem_name/core_ext.rb文件,就可以加载所有它包含的库了.

##  命名空间


***引擎的所有组件,都具有独立的命名空间.***

> 在任何时候,应用的优先级都应该比引擎高.应用对其环境中发生的事情拥有最终的决定权.引擎用于增强应用的功能,而不是彻底改变应用的功能.

> 应用和引擎一旦发生冲突,应用的优先级要高于引擎.

一般来说, 一个Ruby库的文件名对应一个相同根命名空间的模块/类的名字,如:

```ruby
lib/gem_name/foo.rb #=> GemName::Foo
lib/gem_name/bar.rb #=> GemName::Bar
```

命名代码和模块是困难的.最好的建议是跟随 **单一职责原则** 并且创建只做一件事的文件/模块,并且把一件事做好.

## Gem加载过程

Bundler加载此Gem -> 解析gemspec文件 -> 加载lib/engine_name.rb文件 -> lib/engine_name/engine.rb(文件中定义了该gem的基础模块) -> 加载Gem基础模块

## 内嵌测试应用

在 test/dummy文件夹中有一个内嵌于引擎中的精简版 Rails 测试应用,可用于测试引擎.<br>
在test/dummy文件夹中运行 `rails server` 命令来启动测试应用.


# 目录结构

```
.
├── app
│   ├── assets
│   ├── controllers
│   ├── helpers
│   ├── jobs
│   ├── mailers
│   ├── models
│   └── views
├── bin
│   └── rails
├── config
│   └── routes.rb
├── db
│   └── migrate
├── Gemfile
├── Gemfile.lock
├── lib
│   ├── engine_name
│   ├── engine_name.rb
│   └── tasks
├── MIT-LICENSE
├── engine_name-0.1.0.gem
├── engine_name.gemspec
├── Rakefile
├── README.md
└── test
    ├── dummy
    ├── fixtures
    ├── integration
    ├── models
    ├── engine_name_test.rb
    └── test_helper.rb
```

## lib

打包的代码位于lib目录下.

## 入口文件

lib/gem_name.rb的工作是 **加载gem的依赖** .<br>
这些依赖可能是内部或者第三方的库.<br>
而内部的类,就像Rubygems建议的那样,应该放在 lib/gem_name/目录下并且从那里被引用.

## LICENSE.txt 

LICENSE.txt文件默认是MIT许可证.<br>
它规定代码可以被任何人用于做任何事而不用额外的许可或者认可.

## Rakefile

Rakefile 是一个用来定义和组织任务的文件,会从命令行运行它.<br>
默认情况下,它包含下面的内容:

```ruby
require "bundler/gem_tasks"
```

通过引用这个库,bundler提供了一些内置的任务来发布gem.


## Gemspec

```ruby
# 一开始的两行把的gem的lib目录加入了load path(ruby会寻找的额外库的path).
# 这会允许从的宿主应用调用gem_name并且让它正确的加载的gem.

lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "gem_name/version"

Gem::Specification.new do |spec|
  # 第一部分是一些关于(作为作者)和gem的元数据.
  # 在发布之前,要完成描述,总结和主页.
  # 之后,会看到这些信息被用在gem的Rubygem.org的页面上.
  spec.name          = "gem_name"
  spec.version       = CatFile::VERSION
  spec.authors       = ["ff4c00"]
  spec.email         = ["ff4c00@gmail.com"]

  spec.summary       = %q{TODO: Write a short summary, because RubyGems requires one.}
  spec.description   = %q{TODO: Write a longer description or delete this line.}
  spec.homepage      = "TODO: Put your gem's website or public repo URL here."
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # 中间部分使用 git 来确定 gem 的文件.
  # 有一点要注意,如果更改gem并且通过本地文件路径引用gem的应用程序进行测试时,一些文件可能是不可见的.
  # 如果它们没有被 git 提交过(比如 可执行的命令行文件).
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # 最后一部分是gem的依赖定义.
  # 当在宿主应用中被调用时,bundler将会和gem的代码一起安装这些依赖.
  
  # 有两种依赖方法:
  # add_development_dependency — 定义一个开发环境依赖.当被用于生产环境或者当开发宿主程序时,这些依赖不会被安装.
  # 只有在开发本地gem时才会被安装.

  # add_dependency — 定义在所有环境中都需要的依赖
  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end

```

## Gemfile

虽然 bundler 在项目的根目录创建了一个Gemfile,它不应该被修改除非有特殊的理由这么做.

## lib/gem_name/version.rb

```ruby
module GemName 
  VERSION = "0.0.1"
end
```

文件lib/gem_name/version.rb文件定义了一个常量(VERSION)来标志gem的版本. <br>
发布新版时,增加VERSION的值来推送新代码到Rubygems.

## lib/gem_name.rb

默认情况下,bundler创建了lib/gem_name.rb文件.

```ruby
# 注意如何在文件lib/gem_name/目录中使用相对路径来包含加载文件的.
require "gem_name/version"
# gen需要包含的代码,都要在这里显示的声明加载
require "gen_name/文件名称"

module GemName
  # 通常会把配置和初始化代码放在这个文件里.
  # 把具体实现放在/lib/gem_name目录中的其他类库中.
end
```

# 初始化

## 生成引擎

```
rails plugin new engine_name --mountable
```

使用指定Rails版本:

```
gem install rails -v 5.1.4
rails _5.1.4_ plugin new engine_name --mountable
```

通过 --mountable 选项,生成器会创建可挂载和具有独立命名空间的引擎.此选项和 --full 选项会为引擎生成相同的程序骨架.

## engine_name.gemspec

修改所有 `TODO` 内容.

## Gemfile

修改Gem源为:

```
source "https://gems.ruby-china.com"
```

## bundle install


# 宿主 -> 引擎

## 应用挂载引擎

### Gemfile指定引擎
在 Gemfile 中以普通 gem 的方式指定引擎,如果是本地引擎,需要在 Gemfile 中指定 :path 选项
```ruby
gem 'engine_name', path: '相对或绝对路径'
```
### 添加路由
```ruby
Rails.application.routes.draw do
  # 在应用中只能通过此路径访问该引擎
  mount EngineName::Engine => "/自定义根路径"
end
```
### bundle install

### 执行引擎迁移

#### 引擎迁移复制到应用中

第一次运行时,Rails 会复制引擎中所有迁移.再次运行时, *只会复制尚未复制的迁移* .

```ruby
rails engine_name:install:migrations

# 如果需要从多个引擎中复制迁移,可以使用下述命令
rails railties:install:migrations
```
### 运行迁移 
```ruby
# 应用中执行
rails db:migrate
# 通过指定 SCOPE 选项,可以只运行指定引擎的迁移
rails db:migrate SCOPE=engine_name

# 在需要还原并删除引擎的迁移时运行
rails db:migrate SCOPE=engine_name VERSION=0
```

### 执行种子数据

如果引擎有迁移，则可能还需要为db/seeds.rb文件中的数据库准备数据.<br>
可以使用load_seed方法加载该数据，例如:

```ruby
MyEngine::Engine.load_seed
```

## 应用调用引擎服务

### 覆盖引擎模型和控制器

#### 修饰器模式

##### 引擎实现装饰器模式

```ruby
# 要想扩展引擎的模型类和控制器类,比如在宿主应用中直接打开/修改它们(因为模型类和控制器类只不过是继承了特定 Rails 功能的 Ruby 类).
# 通过打开类的技术,可以根据宿主应用的需求对引擎的类进行自定义,实际操作中通常会使用装饰器模式.
# 不光是装饰器,对于添加到引擎中但没有在宿主应用中引用的任何东西,都需要进行这样的处理.
# lib/engine_name/engine.rb
...
config.to_prepare do
  Dir.glob(Rails.root + "app/decorators/**/*_decorator*.rb").each do |c|
    require_dependency(c)
  end
end
...
```

##### 宿主应用实现装饰器模式
宿主通过`Class#class_eval`实现装饰器模式.

```ruby
# EngineName/app/models/article.rb
class Article < ApplicationRecord
  def summary
    "#{title}"
  end
end

# 宿主覆盖引擎Article#summary方法:
# MyApp/app/decorators/models/EngineName/article_decorator.rb(覆盖路径取决于引擎实现装饰器中的读取路径)
EngineName::Article.class_eval do
  def summary
    "#{title} - #{truncate(text)}"
  end
end

```

#### 通过ActiveSupport::Concern模块实现装饰器模式
对类进行简单修改时,使用 Class#class_eval 方法很方便,但对于复杂的修改,就应该考虑使用 ActiveSupport::Concern 模块了.<br>
ActiveSupport::Concern模块能够管理互相关联、依赖的模块和类运行时的**加载顺序**,这样就可以放心地实现代码的模块化.<br>

添加 Article#time_since_created 方法并覆盖 Article#summary 方法:

```ruby
# MyApp/app/models/EngineName/article.rb
class EngineName::Article < ApplicationRecord
  include EngineName::Concerns::Models::Article
 
  def time_since_created
    Time.current - created_at
  end
 
  def summary
    "#{title} - #{truncate(text)}"
  end
end


# EngineName/app/models/article.rb
class Article < ApplicationRecord
  include EngineName::Concerns::Models::Article
end

# EngineName/lib/concerns/models/article.rb
module EngineName::Concerns::Models::Article
  extend ActiveSupport::Concern
 
  # `included do` 中的代码可以在代码所在位置(article.rb)的上下文中执行,
  # 而不是在模块的上下文中执行(engine_nameh/concerns/models/article).
  included do
    attr_accessor :author_name
    belongs_to :author, class_name: "User"
 
    before_validation :set_author
 
    private
      def set_author
        self.author = User.find_or_create_by(name: author_name)
      end
  end
 
  def summary
    "#{title}"
  end
 
  module ClassMethods
    def some_class_method
      'some class method string'
    end
  end
end
```

### 覆盖视图
Rails 在查找需要渲染的视图时,首先会在应用的 app/views 文件夹中查找.<br>
如果找不到,就会接着在所有引擎的 app/views 文件夹中查找.

在渲染 EngineName::ArticlesController 的 index 动作的视图时,Rails 首先在 ***应用中*** 查找 app/views/engine_nameh/articles/index.html.erb 文件.如果**找不到**,就会接着**在引擎中**查找.

只要在宿主应用中新建 app/views/engine_nameh/articles/index.html.erb 视图,<br>
就可覆盖引擎中的对应视图,这样就可以根据需要自定义视图的内容.

### 调用路由
默认情况下,引擎和应用的路由是隔离开的.<br>
这种隔离是通过在 Engine 类中调用 isolate_namespace 方法实现的.这样,应用和引擎中的同名路由就不会发生冲突.

根据渲染模板的位置不同,会调用引擎/应用的路由.<br>
当想要在应用中链接到引擎的某个位置时,就必须使用引擎的路由代理方法.<br>
```ruby
<!-- 应用下渲染模板 -->

<!-- 调用宿主应用的articles_path辅助方法 -->
<%= link_to "Blog articles", articles_path %>

<!-- 要想确保使用的是应用的 articles_path 辅助方法,可以使用main_app路由代理方法 -->
<%= link_to "Blog articles", main_app.articles_path %>

<!-- 通过路由代理方法来调用引擎的 articles_path 辅助方法 -->
<%= link_to "Blog articles", engine_name.articles_path %>

<!-- 当在引擎中渲染模板时,代码生成的链接将总是指向应用的根路径.要是不使用 main_app 路由代理方法,在不同位置渲染模板时,上述代码生成的链接就既有可能指向引擎的根路径,也有可能指向应用的根路径. -->
<%= link_to "Home", main_app.root_path %>
```
### 加载引擎的静态资源文件
引擎和应用的静态资源文件的工作原理完全相同.<br>
由于引擎类继承自 Rails::Engine 类,应用知道应该在引擎的app/assets和lib/assets 文件夹中查找静态资源文件.

引擎的静态资源文件同样也应具有独立的命名空间.<br>
也就是说,引擎的静态资源文件 style.css 的路径应该是app/assets/stylesheets/engine_name/style.css,而不是 app/assets/stylesheets/style.css.<br>
如果引擎的静态资源文件不具有独立的命名空间,那么就有可能和宿主应用中的同名静态资源文件发生冲突.<br>
而一旦发生冲突, ***宿主应用中的静态资源文件将具有更高的优先级*** ,引擎的静态资源文件将被忽略.

**若想使用 Sass 和 CoffeeScript 等语言,要把相关的 gem 添加到引擎的 .gemspec 文件中.**

```html
<!-- 假设引擎有 app/assets/stylesheets/engine_name/style.css 这么一个静态资源文件,要想在宿主应用中包含此文件,直接使用 stylesheet_link_tag 辅助方法即可 -->
<%= stylesheet_link_tag "engine_name/style.css" %>

// 同样,也可以使用Asset Pipeline的require语句加载引擎中的静态资源文件:
/*
 *= require engine_name/style
*/
```
### 独立加载静态资源文件及预编译
有时,宿主应用并不需要加载引擎的静态资源文件.<br>
例如,假设创建了一个仅适用于某个引擎的管理后台,这时宿主应用就不需要加载引擎的 admin.css 和 admin.js 文件<br>
因为只有引擎的管理后台才需要这些文件.也就是说,在宿主应用的样式表中包含 engine_name/admin.css 文件没有任何意义.<br>
对于这种情况,应该显式定义那些需要预编译的静态资源文件,这样在执行 `bin/rails assets:precompile` 命令时,Sprockets 就会预编译所指定的引擎的静态资源文件.

```ruby
# 在引擎的 engine.rb 文件中定义需要预编译的静态资源文件
initializer "engine_name.assets.precompile" do |app|
  app.config.assets.precompile += %w( admin.js admin.css )
end
```
### 安装引擎的其他gem依赖
应该在引擎根目录中的.gemspec文件中声明引擎的gem依赖<br>
因为可能会以gem的方式安装引擎.如果在引擎的Gemfile文件中声明gem依赖,在通过 `gem install` 命令安装引擎时,就无法识别并安装这些依赖,这样引擎安装后将无法正常工作.

要想让 gem install 命令能够识别引擎的 gem 依赖,只需在引擎的 .gemspec 文件的 Gem::Specification 代码块中进行声明:

```ruby
Gem::Specification do |s|
  s.add_dependency "gem_name"
  # 还可以像下面这样声明用于开发环境的依赖
  s.add_development_dependency "gem_name"
end 
```
不管是用于所有环境的依赖,还是用于开发环境的依赖,在执行 `bundle install` 命令时都会被安装,只不过用于开发环境的依赖只会在运行引擎测试时用到.

如果有些依赖在加载引擎时就必须加载,那么应该在引擎初始化之前就加载它们,例如:

```ruby
# engine_name/lib/engine_name/engine.rb
require 'other_engine/engine'
require 'yet_another_engine/engine'
 
module EngineName
  class Engine < ::Rails::Engine
  end
end
```

# 引擎 -> 宿主

## 引擎调用宿主应用服务

### 调用宿主应用控制器

```ruby
module EngineName
  # 引擎的控制器常常需要访问宿主应用的  ApplicationController 类中的方法,
  # 为此可以让引擎的 ApplicationController 类继承自宿主应用的 ApplicationController 类
  # class ApplicationController < ::ApplicationController
  class ApplicationController < ::ApplicationController
  end
end
```

### 在引擎中配置所使用的宿主应用中的类

在引擎中调用应用中的类(class)时,可能会因类名不同而产生差异.<br>
因此类名需要作为可配置项.<br>
具体操作是使用mattr_accessor方法,也就是把下面这行代码添加到引擎的 lib/engine_name.rb 文件中:
```ruby
mattr_accessor :author_class
```
mattr_accessor 方法的工作原理与attr_accessor和cattr_accessor方法类似,<br>
其作用是根据指定名称为模块提供 *设值方法* 和 *读值方法* .<br>
使用时直接调用 EngineName.author_class 方法即可

```ruby
# app/models/engine_name/article.rb
module EngineName
  class Article < ApplicationRecord
    ...
    # 当需要存取应用模型中的某个属性时,可以在引擎调用模型中为该属性注册存取器,通过回调进行存取.
    attr_accessor :author_name
    belongs_to :author, class_name: EngineName.author_class.to_s
    
    before_validation :set_author
    private
      def set_author
        # 调用应用模型
        # self.author = EngineName.author_class.constantize.find_or_create_by(name: author_name)
        self.author = EngineName.author_class.find_or_create_by(name: author_name)
      end
    ...
  end
end

# 为了避免在每次调用EngineName.author_class方法时调用constantize方法,可以在lib/engine_name.rb文件中覆盖EngineName模块的author_class读值方法,在返回author_class前调用constantize方法:

# lib/engine_name.rb
require "engine_name/engine"

module EngineName
  # mattr_accessor方法的工作原理与attr_accessor和cattr_accessor方法类似
  # 其作用是根据指定名称为模块提供设值方法和读值方法.
  # 使用时直接调用EngineName.author_class方法即可
  mattr_accessor :author_class
  def self.author_class
    @@author_class.constantize
  end
end

# 为了配置引擎所使用的应用中的类,需要使用初始化脚本.
# 只有通过初始化脚本,才能在应用启动并调用引擎模型前完成相关配置.

# 在安装 engine_name 引擎的应用中,打开 config/initializers/engine_name.rb 文件,创建新的初始化脚本并添加:

# config/initializers/engine_name.rb
EngineName.author_class = "ClassName"
```

# Hook

## Active Support on_load 钩子

ActiveSupport.on_load 可以延迟加载代码,在真正需要时才加载.

```ruby
ActiveRecord::Base.include(MyActiveRecordHelper)

# 修改后,加载 ActiveRecord::Base 时才会引入 MyActiveRecordHelper.
ActiveSupport.on_load(:active_record) { include MyActiveRecordHelper }
```

# Helper 

engine里面加载helper方法:

```ruby
# Controller
helper EngineName::Engine.helpers
```

# 其他

## 命令行可执行文件

很多Ruby gems提供了可执行的命令行作为它们的功能的一部分.想象一下离开了命令行还能使用 bunlder或者rake吗?那就不是一个gem了！

通常来说,可执行文件是独立在一个单独的类库中并且不被入口文件包含用于加载的.

如果想要创建一个命令行工具用foo命令来代理#foo方法在GemName::Foo内的作用并且返回想要的东西:

```bash
$ foo #=> the result from GemName::Foo #foo
```

### bin/目录

bin/ 目录是配套的可执行文件的标准存放位置.再一次看看gem_name.gemspec文件注意下面这行:

```ruby
spec.executables = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
```

gemspec定义了一个可执行文件的列表,通过被提交到git并且位于bin/目录下.

为了实现上面的foo命令.需要加入bin/file_name文件,由于它是一个脚本会被命令行执行,所以有两个重要的改变要做:

0. 脚本声明(在文件的顶部加入 `#!/usr/bin/env ruby`)
0. 赋予执行权限(chomd +x bin/file_name(linux或者macOS平台))

```ruby
#!/usr/bin/env ruby
# 因为这个文件是独立于gem的其他部分的,所以不得不加载必要的依赖
require_relative "../lib/gem_name/file_name"
foo = GemName::ClassName.new.foo 
puts foo
```

### 选项解析

很少有命令行不支持选项解析.如果经常使用命令行,-h可能是一个经常使用的选项.

允许选项和参数被传入命令行可以呈几何级数地增强它的功能.幸运的是,Ruby已经有了标准库内置的OptonParser类.

TODO: 一个简单的例子.

## 使用irb创建Ruby交互环境

参数|含义
-|-
r|加载 Ruby 标准库以外的库
I|允许加入一个特定的目录到Ruby的load path

```ruby
irb -r gem_name -I ./lib #=>  获得一个Ruby交互环境
```

#### 加入rake任务

```ruby
# Rakefile
...
task :console do
  exec "irb -r gem_name -I ./lib" 
end 
```

这样直接运行 `rake console` 即可调用交互终端.

## 发布

### 前提条件

> 本地仓库初始化及远程仓库配置完毕.

> 如果要将gem上传到rubygems.org(`rake release` 操作),需要一个[帐号](https://rubygems.org/sign_up, '创建帐号'). 

#### 注册RubyGems帐号

[注册页面](https://rubygems.org/sign_up)

api_key 写入本地:

```
curl -u user_name https://rubygems.org/api/v1/api_key.yaml > ~/.gem/credentials; chmod 0600 ~/.gem/credentials
```

### rake build(打包)

`rake build` 将会在gem的pkg/目录下创建一个带版本号的.gem文件.<br>
bundler创建了一个.gitignore文件来把pkg/目录排除到git版本控制之外.<br>
所以即使在运行rakebuild命令后,被打包的*.gem文件也不会被提交到源码库.

### gem push(推送)

```
gem push pkg/gem_name-version.gem
```

# 参考资料

> [Rails指南-引擎入门](https://ruby-china.github.io/rails-guides/engines.html)

> [RubyChina-基础 Ruby 中 Include, Extend, Load, Require 的使用区别](https://ruby-china.org/topics/25706)

> [rubyonrails | Creating an Engine](https://api.rubyonrails.org/classes/Rails/Engine.html)

> [简介 | 语义化](https://semver.org/lang/zh-CN/)

> [Chorder | Ruby Gem教程：如何发布一个Ruby GEM (Ruby Gems How To)](https://chorder.net/2019/01/11/RubyGemTemplate/#1-注册RubyGems帐号)