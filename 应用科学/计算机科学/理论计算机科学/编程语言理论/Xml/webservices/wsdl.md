> WSDL是基于XML的用于描述Web Services以及如何访问Web Services的语言.

<!-- TOC -->

- [简介](#简介)
  - [什么是WSDL?](#什么是wsdl)
  - [特点](#特点)
- [WSDL文档](#wsdl文档)
  - [WSDL文档结构](#wsdl文档结构)
  - [WSDL端口](#wsdl端口)
  - [WSDL消息](#wsdl消息)
  - [WSDL types](#wsdl-types)
  - [WSDL Bindings](#wsdl-bindings)
  - [WSDL实例](#wsdl实例)
- [WSDL端口](#wsdl端口-1)
  - [操作类型](#操作类型)
    - [One-Way操作](#one-way操作)
    - [Request-Response操作](#request-response操作)
- [WSDL绑定](#wsdl绑定)
  - [绑定到SOAP](#绑定到soap)
- [参考资料](#参考资料)

<!-- /TOC -->

# 简介

## 什么是WSDL?

WSDL 是一种使用 XML 编写的文档.<br>
这种文档可描述某个 Web service.<br>
它可规定服务的位置,以及此服务提供的操作(或方法).

## 特点

0. WSDL 指网络服务描述语言
0. WSDL 使用 XML 编写
0. WSDL 是一种 XML 文档
0. WSDL 用于描述网络服务
0. WSDL 也可用于定位网络服务
0. WSDL 还不是 W3C 标准
0. WSDL 可描述网络服务(Web Services)
0. WSDL 指网络服务描述语言 (Web Services Description Language).


# WSDL文档

WSDL文档仅仅是一个简单的XML文档.

它包含一系列描述某个web service的定义.

## WSDL文档结构

WSDL文档是利用这些主要的元素来描述某个web service的:

元素|定义
-|-
\<portType>|web service 执行的操作
\<message>|web service 使用的消息
\<types>|web service 使用的数据类型
\<binding>|web service 使用的通信协议

一个 WSDL 文档的主要结构是类似这样的:

```xml
<definitions>

<types>
  data type definitions........
</types>

<message>
  definition of the data being communicated....
</message>

<portType>
  set of operations......
</portType>

<binding>
  protocol and data format specification....
</binding>

</definitions>
```

WSDL文档可包含其它的元素,比如extension元素,以及一个service元素,此元素可把若干个webservices的定义组合在一个单一的WSDL文档中.

## WSDL端口

\<portType> 元素是最重要的 WSDL 元素.

它可描述一个web service、可被执行的操作,以及相关的消息.

可以把\<portType>元素比作传统编程语言中的一个函数库(或一个模块、或一个类).

## WSDL消息

\<message> 元素定义一个操作的数据元素.

每个消息均由一个或多个部件组成.<br>
可以把这些部件比作传统编程语言中一个函数调用的参数.

## WSDL types

\<types>元素定义web service使用的数据类型.

为了最大程度的平台中立性,WSDL使用XML Schema语法来定义数据类型.

## WSDL Bindings

\<binding> 元素为每个端口定义消息格式和协议细节.

## WSDL实例

这是某个 WSDL 文档的简化的片段:

```xml
<message name="getTermRequest">
  <part name="term" type="xs:string"/>
</message>

<message name="getTermResponse">
  <part name="value" type="xs:string"/>
</message>

<portType name="glossaryTerms">
  <operation name="getTerm">
    <input message="getTermRequest"/>
    <output message="getTermResponse"/>
  </operation>
</portType>
```

在这个例子中,\<portType> 元素把"glossaryTerms"定义为某个端口的名称,把"getTerm"定义为某个操作的名称.

操作"getTerm"拥有一个名为"getTermRequest"的输入消息,以及一个名为"getTermResponse"的输出消息.

\<message>元素可定义每个消息的部件,以及相关联的数据类型.

对比传统的编程,glossaryTerms是一个函数库,而"getTerm"是带有输入参数"getTermRequest"和返回参数getTermResponse的一个函数.

# WSDL端口

\<portType>元素是最重要的WSDL元素.


它可描述一个webservice、可被执行的操作,以及相关的消息.

可以把\<portType>元素比作传统编程语言中的一个函数库(或一个模块、或一个类).

## 操作类型

请求-响应是最普通的操作类型,不过WSDL定义了四种类型:

类型|定义
-|-
One-way|此操作可接受消息,但不会返回响应.
Request-response|此操作可接受一个请求并会返回一个响应
Solicit-response|此操作可发送一个请求,并会等待一个响应.
Notification|此操作可发送一条消息,但不会等待响应.

### One-Way操作

```xml
<message name="newTermValues">
  <part name="term" type="xs:string"/>
  <part name="value" type="xs:string"/>
</message>

<portType name="glossaryTerms">
  <operation name="setTerm">
    <input name="newTerm" message="newTermValues"/>
  </operation>
</portType >
```

在这个例子中,端口"glossaryTerms"定义了一个名为"setTerm"的one-way操作.

这个"setTerm"操作可接受新术语表项目消息的输入,这些消息使用一条名为"newTermValues"的消息,此消息带有输入参数"term"和"value".<br>
不过,没有为这个操作定义任何输出.

### Request-Response操作

```xml
<message name="getTermRequest">
  <part name="term" type="xs:string"/>
</message>

<message name="getTermResponse">
  <part name="value" type="xs:string"/>
</message>

<portType name="glossaryTerms">
  <operation name="getTerm">
    <input message="getTermRequest"/>
    <output message="getTermResponse"/>
  </operation>
</portType>
```

在这个例子中,端口"glossaryTerms"定义了一个名为"getTerm"的request-response操作.

"getTerm"操作会请求一个名为"getTermRequest"的输入消息,此消息带有一个名为"term"的参数,并将返回一个名为"getTermResponse"的输出消息,此消息带有一个名为"value"的参数.

# WSDL绑定

WSDL绑定可为web service定义消息格式和协议细节.

## 绑定到SOAP

一个 请求 - 响应 操作的例子:

```xml
<message name="getTermRequest">
  <part name="term" type="xs:string"/>
</message>

<message name="getTermResponse">
  <part name="value" type="xs:string"/>
</message>

<portType name="glossaryTerms">
  <operation name="getTerm">
    <input message="getTermRequest"/>
    <output message="getTermResponse"/>
  </operation>
</portType>

<binding type="glossaryTerms" name="b1">
   <soap:binding style="document"
   transport="http://schemas.xmlsoap.org/soap/http" />
   <operation>
     <soap:operation soapAction="http://example.com/getTerm"/>
     <input><soap:body use="literal"/></input>
     <output><soap:body use="literal"/></output>
  </operation>
</binding>
```

binding元素有两个属性: 

0. name 属性
0. type 属性

name 属性,定义binding的名称.<br>
type属性指向用于binding的端口,在这个例子中是"glossaryTerms"端口.

soap:binding元素有两个属性:

0. style属性
0. transport属性

style属性可取值"rpc"或"document".<br>
在这个例子中使用document.

transport属性定义了要使用的SOAP协议.<br>
在这个例子中我们使用HTTP.

operation元素定义了每个端口提供的操作符.

对于每个操作,相应的SOAP行为都需要被定义.<br>
同时必须如何对输入和输出进行编码.<br>
在这个例子中使用了"literal".

# 参考资料

> [菜鸟教程 | WSDL 教程](https://www.runoob.com/wsdl/wsdl-tutorial.html)