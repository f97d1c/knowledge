> SOAP是一种简单的基于XML的协议,它使应用程序通过HTTP来交换信息.
<!-- TOC -->

- [简介](#简介)
  - [什么是 SOAP?](#什么是-soap)
  - [为什么使用 SOAP?](#为什么使用-soap)
- [SOAP语法](#soap语法)
  - [SOAP构建模块](#soap构建模块)
  - [语法规则](#语法规则)
  - [SOAP消息的基本结构](#soap消息的基本结构)
- [SOAP Envelope元素](#soap-envelope元素)
  - [xmlns:soap命名空间](#xmlnssoap命名空间)
  - [encodingStyle 属性](#encodingstyle-属性)
- [SOAP Header元素](#soap-header元素)
  - [mustUnderstand属性](#mustunderstand属性)
  - [actor属性](#actor属性)
  - [encodingStyle属性](#encodingstyle属性)
- [SOAP Body元素](#soap-body元素)
- [SOAP Fault元素](#soap-fault元素)
  - [SOAP Fault代码](#soap-fault代码)
- [SOAP HTTP协议](#soap-http协议)
  - [Content-Type](#content-type)
  - [规定请求或响应主体的字节数](#规定请求或响应主体的字节数)
- [参考资料](#参考资料)

<!-- /TOC -->

# 简介

## 什么是 SOAP?

0. SOAP 指简易对象访问协议
0. SOAP 是一种通信协议
0. SOAP 用于应用程序之间的通信
0. SOAP 是一种用于发送消息的格式
0. SOAP 被设计用来通过因特网进行通信
0. SOAP 独立于平台
0. SOAP 独立于语言
0. SOAP 基于 XML
0. SOAP 很简单并可扩展
0. SOAP 允许您绕过防火墙
0. SOAP 将被作为 W3C 标准来发展

## 为什么使用 SOAP?

对于应用程序开发来说,使程序之间进行因特网通信是很重要的.

目前的应用程序通过使用远程过程调用(RPC)在诸如DCOM与CORBA等对象之间进行通信,但是HTTP不是为此设计的.<br>
RPC会产生兼容性以及安全问题;防火墙和代理服务器通常会阻止此类流量.

通过HTTP在应用程序间通信是更好的方法,因为HTTP得到了所有的因特网浏览器及服务器的支持.<br>
SOAP就是被创造出来完成这个任务的.

SOAP提供了一种标准的方法,使得运行在不同的操作系统并使用不同的技术和编程语言的应用程序可以互相进行通信.

# SOAP语法

## SOAP构建模块

一条SOAP消息就是一个普通的XML文档,包含下列元素:

0. 必需的Envelope元素,可把此XML文档标识为一条SOAP消息
0. 可选的Header元素,包含头部信息
0. 必需的Body元素,包含所有的调用和响应信息
0. 可选的Fault元素,提供有关在处理此消息所发生错误的信息

所有以上的元素均被声明于针对SOAP封装的默认命名空间中:

```
http://www.w3.org/2001/12/soap-envelope
```

以及针对SOAP编码和数据类型的默认命名空间:

```
http://www.w3.org/2001/12/soap-encoding
```

## 语法规则

这里是一些重要的语法规则:

0. SOAP 消息必须用XML来编码
0. SOAP 消息必须使用SOAP Envelope命名空间
0. SOAP 消息必须使用SOAP Encoding命名空间
0. SOAP 消息不能包含DTD引用
0. SOAP 消息不能包含XML处理指令

## SOAP消息的基本结构


```xml
<?xml version="1.0"?>
<soap:Envelope
xmlns:soap="http://www.w3.org/2001/12/soap-envelope"
soap:encodingStyle="http://www.w3.org/2001/12/soap-encoding">

<soap:Header>
...
</soap:Header>

<soap:Body>
...
  <soap:Fault>
  ...
  </soap:Fault>
</soap:Body>

</soap:Envelope>
```

# SOAP Envelope元素

强制使用的SOAP的Envelope元素是SOAP消息的根元素.<br>
必需的SOAP的Envelope元素是SOAP消息的根元素.<br>
它可把XML文档定义为SOAP消息.

```xml
<?xml version="1.0"?>
<soap:Envelope
xmlns:soap="http://www.w3.org/2001/12/soap-envelope"
soap:encodingStyle="http://www.w3.org/2001/12/soap-encoding">
  ...
  Message information goes here
  ...
</soap:Envelope>
```

## xmlns:soap命名空间

SOAP消息必须拥有与命名空间"http://www.w3.org/2001/12/soap-envelope"相关联的一个Envelope元素.

如果使用了不同的命名空间,应用程序会发生错误,并抛弃此消息.

## encodingStyle 属性

SOAP的encodingStyle属性用于定义在文档中使用的数据类型.<br>
此属性可出现在任何SOAP元素中,并会被应用到元素的内容及元素的所有子元素上.

SOAP消息没有默认的编码方式.

语法:

```xml
soap:encodingStyle="URI"
```

实例:

```xml
<?xml version="1.0"?>
<soap:Envelope
xmlns:soap="http://www.w3.org/2001/12/soap-envelope"
soap:encodingStyle="http://www.w3.org/2001/12/soap-encoding">
  ...
  Message information goes here
  ...
</soap:Envelope>
```

# SOAP Header元素

可选的SOAP Header元素包含头部信息.

可选的SOAP Header元素可包含有关SOAP消息的应用程序专用信息(比如认证、支付等).

如果Header元素被提供,则它 **必须是Envelope元素的第一个子元素**.

所有Header元素的直接子元素必须是合格的命名空间.

```xml
<?xml version="1.0"?>
<soap:Envelope
xmlns:soap="http://www.w3.org/2001/12/soap-envelope"
soap:encodingStyle="http://www.w3.org/2001/12/soap-encoding">

<soap:Header>
  <m:Trans xmlns:m="http://www.w3schools.com/transaction/"
  soap:mustUnderstand="1">234
  </m:Trans>
</soap:Header>
...
...
</soap:Envelope>
```

上面的例子包含了一个带有一个 "Trans" 元素的头部,它的值是 234,此元素的 "mustUnderstand" 属性的值是 "1".

SOAP在默认的命名空间中("http://www.w3.org/2001/12/soap-envelope")定义了三个属性.

这三个属性是:actor、 mustUnderstand 以及 encodingStyle.<br>
这些被定义在 SOAP 头部的属性可定义容器如何对 SOAP 消息进行处理.

## mustUnderstand属性

SOAP的mustUnderstand属性可用于标识标题项对于要对其进行处理的接收者来说是强制的还是可选的.

假如向Header元素的某个子元素添加了"mustUnderstand="1",则它可指示处理此头部的接收者必须认可此元素.<br>
假如此接收者无法认可此元素,则在处理此头部时必须失效.

实例:

```xml
<?xml version="1.0"?>
<soap:Envelope
xmlns:soap="http://www.w3.org/2001/12/soap-envelope"
soap:encodingStyle="http://www.w3.org/2001/12/soap-encoding">

<soap:Header>
  <m:Trans xmlns:m="http://www.w3schools.com/transaction/"
  soap:mustUnderstand="1">234
  </m:Trans>
</soap:Header>
...
...
</soap:Envelope>
```

## actor属性

通过沿着消息路径经过不同的端点,SOAP消息可从某个发送者传播到某个接收者.<br>
并非SOAP消息的所有部分均打算传送到SOAP消息的最终端点,不过,另一个方面,也许打算传送给消息路径上的一个或多个端点.

SOAP的actor属性可被用于将Header元素寻址到一个特定的端点.

实例:

```xml
<?xml version="1.0"?>
<soap:Envelope
xmlns:soap="http://www.w3.org/2001/12/soap-envelope"
soap:encodingStyle="http://www.w3.org/2001/12/soap-encoding">

<soap:Header>
  <m:Trans xmlns:m="http://www.w3schools.com/transaction/"
  soap:actor="http://www.w3schools.com/appml/">234
  </m:Trans>
</soap:Header>
...
...
</soap:Envelope>
```

## encodingStyle属性

SOAP的encodingStyle属性用于定义在文档中使用的数据类型.<br>
此属性可出现在任何SOAP元素中,并会被应用到元素的内容及元素的所有子元素上.

SOAP消息没有默认的编码方式.

```xml
soap:encodingStyle="URI"
```

# SOAP Body元素

强制使用的SOAP Body元素包含实际的SOAP消息.

必需的SOAP Body元素可包含打算传送到消息最终端点的实际SOAP消息.

SOAP Body元素的直接子元素可以是合格的命名空间.

实例:

```xml
<?xml version="1.0"?>
<soap:Envelope
xmlns:soap="http://www.w3.org/2001/12/soap-envelope"
soap:encodingStyle="http://www.w3.org/2001/12/soap-encoding">

<soap:Body>
  <m:GetPrice xmlns:m="http://www.w3schools.com/prices">
    <m:Item>Apples</m:Item>
  </m:GetPrice>
</soap:Body>

</soap:Envelope>
```

上面的例子请求苹果的价格.<br>
请注意,上面的m:GetPrice和Item元素是应用程序专用的元素.<br>
它们并不是SOAP标准的一部分.

而一个SOAP响应应该类似这样:

```xml
<?xml version="1.0"?>
<soap:Envelope
xmlns:soap="http://www.w3.org/2001/12/soap-envelope"
soap:encodingStyle="http://www.w3.org/2001/12/soap-encoding">

<soap:Body>
  <m:GetPriceResponse xmlns:m="http://www.w3schools.com/prices">
    <m:Price>1.90</m:Price>
  </m:GetPriceResponse>
</soap:Body>

</soap:Envelope>
```

# SOAP Fault元素

SOAP Fault元素用于存留SOAP消息的错误和状态信息,指示错误消息.

如果已提供了Fault元素,则它必须是Body元素的子元素.<br>
在一条SOAP消息中,Fault元素只能出现一次.

SOAP的Fault元素拥有下列子元素:

子元素|描述
-|-
\<faultcode>|供识别故障的代码
\<faultstring>|可供人阅读的有关故障的说明
\<faultactor>|有关是谁引发故障的信息
\<detail>|存留涉及Body元素的应用程序专用错误信息

## SOAP Fault代码

在下面定义的 faultcode 值必须用于描述错误时的 faultcode 元素中:

错误|描述
-|-
VersionMismatch|SOAP Envelope 元素的无效命名空间被发现
MustUnderstand|Header 元素的一个直接子元素(带有设置为 "1" 的 mustUnderstand 属性)无法被理解.
Client|消息被不正确地构成,或包含了不正确的信息.
Server|服务器有问题,因此无法处理进行下去.

# SOAP HTTP协议

HTTP 在 TCP/IP 之上进行通信.<br>
HTTP 客户机使用 TCP 连接到 HTTP 服务器.<br>
在建立连接之后,客户机可向服务器发送 HTTP 请求消息:

```http
POST /item HTTP/1.1
Host: 189.123.255.239
Content-Type: text/plain
Content-Length: 200
```

随后服务器会处理此请求,然后向客户机发送一个 HTTP 响应.<br>
此响应包含了可指示请求状态的状态代码:

```http
200 OK
Content-Type: text/plain
Content-Length: 200
```

在上面的例子中,服务器返回了一个 200 的状态代码.<br>
这是HTTP的标准成功代码.

假如服务器无法对请求进行解码,它可能会返回类似这样的信息:

```http
400 Bad Request
Content-Length: 0
SOAP HTTP Binding
```

SOAP方法指的是遵守SOAP编码规则的HTTP请求/响应.

```
HTTP + XML = SOAP
```

SOAP请求可能是HTTP POST或HTTP GET请求.

HTTP POST请求规定至少两个HTTP头:Content-Type和Content-Length.

## Content-Type

SOAP的请求和响应的Content-Type头可定义消息的MIME类型,以及用于请求或响应的XML主体的字符编码(可选).

```http
POST /item HTTP/1.1
Content-Type: application/soap+xml; charset=utf-8
Content-Length
```

## 规定请求或响应主体的字节数

SOAP的请求和响应的Content-Length头规定请求或响应主体的字节数.

```http
POST /item HTTP/1.1
Content-Type: application/soap+xml; charset=utf-8
Content-Length: 250
```

# 参考资料

> [菜鸟教程 | SOAP教程](https://www.runoob.com/soap/soap-tutorial.html)