
<!-- TOC -->

- [简介](#简介)
  - [什么是 XML?](#什么是-xml)
  - [关于XML和HTML](#关于xml和html)
  - [XML不会做任何事情](#xml不会做任何事情)
  - [XML用途](#xml用途)
    - [XML把数据从HTML分离](#xml把数据从html分离)
    - [XML简化数据共享](#xml简化数据共享)
    - [XML简化数据传输](#xml简化数据传输)
    - [XML简化平台变更](#xml简化平台变更)
    - [XML使的数据更有用](#xml使的数据更有用)
    - [假如开发人员都是理性的](#假如开发人员都是理性的)
- [树结构](#树结构)
- [XML 语法规则](#xml-语法规则)
  - [XML文档必须有根元素](#xml文档必须有根元素)
  - [XML 声明](#xml-声明)
  - [XML标签对大小写敏感](#xml标签对大小写敏感)
  - [XML 必须正确嵌套](#xml-必须正确嵌套)
  - [XML 属性值必须加引号](#xml-属性值必须加引号)
  - [实体引用](#实体引用)
  - [XML中的注释](#xml中的注释)
  - [在XML中,空格会被保留](#在xml中空格会被保留)
  - [XML以LF存储换行](#xml以lf存储换行)
- [XML 元素](#xml-元素)
  - [什么是 XML 元素?](#什么是-xml-元素)
  - [XML命名规则](#xml命名规则)
  - [最佳命名习惯](#最佳命名习惯)
  - [XML元素是可扩展的](#xml元素是可扩展的)
- [XML属性](#xml属性)
  - [XML属性必须加引号](#xml属性必须加引号)
  - [XML 元素 vs. 属性](#xml-元素-vs-属性)
    - [推荐方式](#推荐方式)
  - [避免 XML 属性?](#避免-xml-属性)
  - [针对元数据的 XML 属性](#针对元数据的-xml-属性)
- [XML验证](#xml验证)
  - [形式良好的XML文档](#形式良好的xml文档)
  - [验证XML文档](#验证xml文档)
  - [XML DTD](#xml-dtd)
  - [XML Schema](#xml-schema)
- [使用CSS显示XML](#使用css显示xml)
- [使用XSLT显示XML](#使用xslt显示xml)
- [XMLHttpRequest 对象](#xmlhttprequest-对象)
  - [创建一个 XMLHttpRequest 对象](#创建一个-xmlhttprequest-对象)
- [XML解析器](#xml解析器)
  - [解析 XML 文档](#解析-xml-文档)
  - [解析 XML 字符串](#解析-xml-字符串)
  - [跨域访问](#跨域访问)
- [XML DOM](#xml-dom)
  - [HTML DOM](#html-dom)
  - [HTML 页面显示 XML 数据](#html-页面显示-xml-数据)
- [XML 应用程序](#xml-应用程序)
  - [在 HTML div 元素中显示第一个 CD](#在-html-div-元素中显示第一个-cd)
  - [添加导航脚本](#添加导航脚本)
- [XML命名空间](#xml命名空间)
  - [命名冲突](#命名冲突)
  - [使用前缀来避免命名冲突](#使用前缀来避免命名冲突)
  - [xmlns 属性](#xmlns-属性)
  - [默认的命名空间](#默认的命名空间)
  - [实际使用中的命名空间](#实际使用中的命名空间)
- [XML CDATA](#xml-cdata)
  - [PCDATA-被解析的字符数据](#pcdata-被解析的字符数据)
  - [CDATA - (未解析)字符数据](#cdata---未解析字符数据)
- [XML编码](#xml编码)
- [XML DOM高级](#xml-dom高级)
  - [获取元素的值](#获取元素的值)
  - [获取属性的值](#获取属性的值)
  - [改变元素的值](#改变元素的值)
  - [创建新的属性](#创建新的属性)
  - [创建元素](#创建元素)
  - [删除元素](#删除元素)
- [XML相关技术](#xml相关技术)
- [参考资料](#参考资料)

<!-- /TOC -->

# 简介

## 什么是 XML?

0. XML 指可扩展标记语言(EXtensible Markup Language).
0. XML 是一种很像HTML的标记语言.
0. XML 的设计宗旨是传输数据,而不是显示数据.
0. XML 标签没有被预定义.需要自行定义标签.
0. XML 被设计为具有自我描述性.
0. XML 是 W3C 的推荐标准.

## 关于XML和HTML

XML 不是 HTML 的替代.

XML 和 HTML 为不同的目的而设计:

0. XML 被设计用来传输和存储数据,其焦点是数据的内容.
0. HTML 被设计用来显示数据,其焦点是数据的外观.

HTML 旨在显示信息,而 XML 旨在传输信息.

XML 是对 HTML 的补充.<br>
XML 不会替代 HTML,理解这一点很重要.<br>
在大多数 Web 应用程序中,XML 用于传输数据,而 HTML 用于格式化并显示数据.

对 XML 最好的描述是:<br>
XML 是独立于软件和硬件的信息传输工具.

## XML不会做任何事情

XML 被设计用来结构化、存储以及传输信息.

下面实例是 Jani 写给 Tove 的便签,存储为 XML:

```xml
<note>
<to>Tove</to>
<from>Jani</from>
<heading>Reminder</heading>
<body>Don't forget me this weekend!</body>
</note>
```

上面的这条便签具有自我描述性.<br>
它包含了发送者和接受者的信息,同时拥有标题以及消息主体.

但是,这个 XML 文档仍然没有做任何事情.<br>
它仅仅是包装在 XML 标签中的纯粹的信息.<br>
需要编写软件或者程序,才能传送、接收和显示出这个文档.

## XML用途

XML应用于Web开发的许多方面,常用于简化数据的存储和共享.

### XML把数据从HTML分离

如果需要在HTML文档中显示动态数据,那么每当数据改变时将花费大量的时间来编辑HTML.

通过XML,数据能够存储在独立的XML文件中.<br>
这样就可以专注于使用HTML/CSS进行显示和布局,并确保修改底层数据不再需要对HTML进行任何的改变.

通过使用几行JavaScript代码,就可以读取一个外部XML文件,并更新的网页的数据内容.

### XML简化数据共享

在真实的世界中,计算机系统和数据使用不兼容的格式来存储数据.

XML数据以纯文本格式进行存储,因此提供了一种独立于软件和硬件的数据存储方法.

这让创建不同应用程序可以共享的数据变得更加容易.

### XML简化数据传输

对开发人员来说,其中一项最费时的挑战一直是在互联网上的不兼容系统之间交换数据.

由于可以通过各种不兼容的应用程序来读取数据,以XML交换数据降低了这种复杂性.

### XML简化平台变更

升级到新的系统(硬件或软件平台),总是非常费时的.<br>
必须转换大量的数据,不兼容的数据经常会丢失.

XML数据以文本格式存储.<br>
这使得XML在不损失数据的情况下,更容易扩展或升级到新的操作系统、新的应用程序或新的浏览器.

### XML使的数据更有用

不同的应用程序都能够访问的数据,不仅仅在HTML页中,也可以从XML数据源中进行访问.

通过XML,数据可供各种阅读设备使用(掌上计算机、语音设备、新闻阅读器等),还可以供盲人或其他残障人士使用.

XML用于创建新的互联网语言<br>
很多新的互联网语言是通过XML创建的.

这里有一些实例:

0. XHTML
0. 用于描述可用的Web服务的WSDL
0. 作为手持设备的标记语言的WAP和WML
0. 用于新闻feed的RSS语言
0. 描述资本和本体的RDF和OWL
0. 用于描述针针对Web的多媒体的SMIL

### 假如开发人员都是理性的

假如他们都是理性的,就让未来的应用程序使用XML来交换数据吧.

未来也许会出现某种字处理软件、电子表格程序以及数据库,它们可以使用XML格式读取彼此的数据,而不需要使用任何的转换程序.

# 树结构

XML 文档形成了一种树结构,它从"根部"开始,然后扩展到"枝叶".

XML 文档必须包含根元素.<br>
该元素是所有其他元素的父元素.

XML 文档中的元素形成了一棵文档树.<br>
这棵树从根部开始,并扩展到树的最底端.

所有的元素都可以有子元素:

```xml
<root>
<child>
<subchild>.....</subchild>
</child>
</root>
```

父、子以及同胞等术语用于描述元素之间的关系.<br>
父元素拥有子元素.<br>
相同层级上的子元素成为同胞(兄弟或姐妹).

所有的元素都可以有文本内容和属性(类似 HTML 中).

下面实例在展示XML的树结构的同时也体现了其简单的具有自我描述性的语法:

```xml
<!-- XML 声明,它定义 XML 的版本(1.0)和所使用的编码(UTF-8 : 万国码, 可显示各种语言) -->
<?xml version="1.0" encoding="UTF-8"?>
<!-- 描述文档的根元素(像在说:"本文档是一个便签") -->
<note>
<!-- 描述根的 4 个子元素(to, from, heading 以及 body) -->
<to>Tove</to>
<from>Jani</from>
<heading>Reminder</heading>
<body>Don't forget me this weekend!</body>
<!-- 定义根元素的结尾 -->
</note>
```

这个实例中,XML 文档包含了一张 Jani 写给 Tove 的便签.<br>
所以说XML具有出色的自我描述性.

# XML 语法规则

XML的语法规则很简单,且很有逻辑.<br>
这些规则很容易学习,也很容易使用.

## XML文档必须有根元素

XML必须包含根元素,它是所有其他元素的父元素,比如以下实例中 root 就是根元素:

```xml
<root>
  <child>
    <subchild>.....</subchild>
  </child>
</root>
```

以下实例中 note 是根元素:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<note>
  <to>Tove</to>
  <from>Jani</from>
  <heading>Reminder</heading>
  <body>Don't forget me this weekend!</body>
</note>
```

## XML 声明

XML 声明文件的可选部分,如果存在需要放在文档的第一行,如下所示:

```xml
<?xml version="1.0" encoding="utf-8"?>
```

以上实例包含 XML 版本(UTF-8 也是 HTML5, CSS, JavaScript, PHP, 和 SQL 的默认编码).

所有的 XML 元素都必须有一个关闭标签<br>
在 HTML 中,某些元素不必有一个关闭标签:

```html
<p>This is a paragraph.
<br>
```

在 XML 中,**省略关闭标签是非法的**.<br>
所有元素都必须有关闭标签:

```html
<p>This is a paragraph.</p>
<br/>
```

从上面的实例中,也许已经注意到XML声明没有关闭标签.<br>
这不是错误.<br>
声明不是XML文档本身的一部分,它没有关闭标签.

## XML标签对大小写敏感

XML 标签对大小写敏感.<br>
标签 <Letter> 与标签 <letter> 是不同的.

必须使用相同的大小写来编写打开标签和关闭标签:

```xml
<Message>这是错误的</message>
<message>这是正确的</message>
```

打开标签和关闭标签通常被称为开始标签和结束标签.<br>
不论喜欢哪种术语,它们的概念都是相同的.

## XML 必须正确嵌套

在 HTML 中,常会看到没有正确嵌套的元素:

```html
<b><i>This text is bold and italic</b></i>
```

在 XML 中,所有元素都必须彼此正确地嵌套:

```xml
<b><i>This text is bold and italic</i></b>
```

在上面的实例中,正确嵌套的意思是:由于 \<i> 元素是在 \<b> 元素内打开的,那么它必须在 \<b> 元素内关闭.

## XML 属性值必须加引号

与 HTML 类似,XML 元素也可拥有属性(名称/值的对).

在 XML 中,XML 的属性值必须加引号.

下面的两个 XML 文档.<br>
 第一个是错误的,第二个是正确的:

```xml
<note date=12/11/2007>
<to>Tove</to>
<from>Jani</from>
</note>
```

```xml
<note date="12/11/2007">
<to>Tove</to>
<from>Jani</from>
</note>
```

在第一个文档中的错误是,note 元素中的 date 属性没有加引号.

## 实体引用

在 XML 中,一些字符拥有特殊的意义.

如果把字符 "<" 放在 XML 元素中,会发生错误,这是因为解析器会把它当作新元素的开始.

这样会产生 XML 错误:

```xml
<message>if salary < 1000 then</message>
```

为了避免这个错误,请用实体引用来代替 "<" 字符:

```xml
<message>if salary &lt; 1000 then</message>
```

在XML中,有5个预定义的实体引用:

符号|实体引用
-|-
<|\&lt;
>|\&gt;
&|\&amp;
'|\&apos;
"|\&quot;

在XML中,只有字符 "<" 和 "&" 确实是非法的.<br>
大于号是合法的,但是用实体引用来代替它是一个好习惯.

## XML中的注释

在 XML 中编写注释的语法与 HTML 的语法很相似.

```xml
<!-- This is a comment -->
```

## 在XML中,空格会被保留

HTML 会把多个连续的空格字符裁减(合并)为一个,在 XML 中,文档中的空格不会被删减.

## XML以LF存储换行

在Windows应用程序中,换行通常以一对字符来存储:回车符(CR)和换行符(LF).

在Unix和MacOSX中,使用LF来存储新行.

在旧的Mac系统中,使用CR来存储新行.

XML以LF存储换行.

# XML 元素

XML 文档包含 XML 元素.

## 什么是 XML 元素?

XML 元素指的是从(且包括)开始标签直到(且包括)结束标签的部分.

一个元素可以包含:

0. 其他元素
0. 文本
0. 属性
0. 或混合以上所有...

```xml
<bookstore>
    <book category="CHILDREN">
        <title>Harry Potter</title>
        <author>J K. Rowling</author>
        <year>2005</year>
        <price>29.99</price>
    </book>
    <book category="WEB">
        <title>Learning XML</title>
        <author>Erik T. Ray</author>
        <year>2003</year>
        <price>39.95</price>
    </book>
</bookstore>
```

在上面的实例中,\<bookstore> 和 \<book> 都有元素内容,因为他们包含其他元素.<br>
\<book> 元素也有属性(category="CHILDREN").<br>
\<title>、\<author>、\<year> 和 \<price> 有文本内容,因为他们包含文本.

## XML命名规则

XML 元素必须遵循以下命名规则:

0. 名称可以包含字母、数字以及其他的字符
0. 名称不能以数字或者标点符号开始
0. 名称不能以字母 xml(或者 XML、Xml 等等)开始
0. 名称不能包含空格
0. 可使用任何名称,没有保留的字词

## 最佳命名习惯

使名称具有描述性.<br>
使用下划线的名称也很不错:\<first_name>、\<last_name>.

名称应简短和简单,比如:\<book_title>,而不是:\<the_title_of_the_book>.

避免 "-" 字符.<br>
如果按照这样的方式进行命名:"first-name",一些软件会认为您想要从 first 里边减去 name.

避免 "." 字符.<br>
如果您按照这样的方式进行命名:"first.name",一些软件会认为 "name" 是对象 "first" 的属性.

避免 ":" 字符.<br>
冒号会被转换为命名空间来使用(稍后介绍).

XML文档经常有一个对应的数据库,其中的字段会对应 XML 文档中的元素.<br>
有一个实用的经验,即 使用数据库的命名规则来命名XML文档中的元素.

在XML中,éòá 等非英语字母是完全合法的,不过需要留意,所使用的软件供应商不支持这些字符时可能出现的问题.

## XML元素是可扩展的

XML元素是可扩展,以携带更多的信息.

请看下面的 XML 实例:

```xml
<note>
    <to>Tove</to>
    <from>Jani</from>
    <body>Don't forget me this weekend!</body>
</note>
```

设想一下,创建了一个应用程序,可将 \<to>、\<from> 以及 \<body> 元素从XML文档中提取出来,并产生以下的输出:

```
MESSAGE
To: Tove
From: Jani

Don't forget me this weekend!
```

想象一下,XML 文档的作者添加的一些额外信息:

```xml
<note>
    <date>2008-01-10</date>
    <to>Tove</to>
    <from>Jani</from>
    <heading>Reminder</heading>
    <body>Don't forget me this weekend!</body>
</note>
```

那么这个应用程序会中断或崩溃吗?

不会.<br>
这个应用程序仍然可以找到XML文档中的 \<to>、\<from> 以及 \<body> 元素,并产生同样的输出.

XML的优势之一,就是可以在不中断应用程序的情况下进行扩展.

# XML属性

XML元素具有属性,类似 HTML.

属性(Attribute)提供有关元素的额外信息.

在 HTML 中,属性提供有关元素的额外信息:

```html
<img src="computer.gif">
<a href="demo.html">
```

属性通常提供不属于数据组成部分的信息.<br>
在中,文件类型与数据无关,但是对需要处理这个元素的软件来说却很重要:

```html
<file type="gif">computer.gif</file>
```

## XML属性必须加引号

属性值必须被引号包围,不过**单引号和双引号均可使用**.<br>
比如一个人的性别,person 元素可以这样写:

```xml
<person sex="female">
<!-- 或者这样也可以 -->
<person sex='female'>
```

如果属性值本身包含双引号,可以使用单引号,就像这个实例:

```xml
<gangster name='George "Shotgun" Ziegler'>
```

或者可以使用字符实体:

```xml
<gangster name="George &quot;Shotgun&quot; Ziegler">
```

## XML 元素 vs. 属性

请看这些实例:

```xml
<person sex="female">
<firstname>Anna</firstname>
<lastname>Smith</lastname>
</person>
```

```xml
<person>
<sex>female</sex>
<firstname>Anna</firstname>
<lastname>Smith</lastname>
</person>
```xml

在第一个实例中,sex 是一个属性.<br>
在第二个实例中,sex 是一个元素.<br>
这两个实例都提供相同的信息.

没有什么规矩约束什么时候该使用属性,而什么时候该使用元素.<br>
经验是在 HTML 中,属性用起来很便利,但是在 XML 中,应该尽量避免使用属性.<br>
如果信息感觉起来很像数据,那么使用元素.

### 推荐方式

下面的三个 XML 文档包含完全相同的信息:

第一个实例中使用了 date 属性:

```xml
<note date="10/01/2008">
<to>Tove</to>
<from>Jani</from>
<heading>Reminder</heading>
<body>Don't forget me this weekend!</body>
</note>
```

第二个实例中使用了 date 元素:

```xml
<note>
<date>10/01/2008</date>
<to>Tove</to>
<from>Jani</from>
<heading>Reminder</heading>
<body>Don't forget me this weekend!</body>
</note>
```

第三个实例中使用了扩展的 date 元素(推荐方式):

```xml
<note>
<date>
<day>10</day>
<month>01</month>
<year>2008</year>
</date>
<to>Tove</to>
<from>Jani</from>
<heading>Reminder</heading>
<body>Don't forget me this weekend!</body>
</note>
```

## 避免 XML 属性?

因使用属性而引起的一些问题:

0. 属性不能包含多个值(元素可以)
0. 属性不能包含树结构(元素可以)
0. 属性不容易扩展(为未来的变化)
0. 属性难以阅读和维护.<br>
请尽量使用元素来描述数据.<br>
而仅仅使用属性来提供与数据无关的信息.

不要做这样的蠢事(这不是 XML 应该被使用的方式):

```xml
<note day="10" month="01" year="2008"
to="Tove" from="Jani" heading="Reminder"
body="Don't forget me this weekend!">
</note>
```

## 针对元数据的 XML 属性

有时候会向元素分配 ID 引用.<br>
这些 ID 索引可用于标识 XML 元素,它起作用的方式与 HTML 中 id 属性是一样的.<br>
这个实例演示了这种情况:

```xml
<messages>
<note id="501">
<to>Tove</to>
<from>Jani</from>
<heading>Reminder</heading>
<body>Don't forget me this weekend!</body>
</note>
<note id="502">
<to>Jani</to>
<from>Tove</from>
<heading>Re: Reminder</heading>
<body>I will not</body>
</note>
</messages>
```

上面的 id 属性仅仅是一个标识符,用于标识不同的便签.<br>
它并不是便签数据的组成部分.

在此极力传递的理念是: ***元数据(有关数据的数据)应当存储为属性,而数据本身应当存储为元素***.

# XML验证

拥有正确语法的 XML 被称为"形式良好"的 XML.

通过 DTD 验证的XML是"合法"的 XML.

## 形式良好的XML文档

"形式良好"的 XML 文档拥有正确的语法.

在前面的章节描述的语法规则:

0. XML 文档必须有一个根元素
0. XML元素都必须有一个关闭标签
0. XML 标签对大小写敏感
0. XML 元素必须被正确的嵌套
0. XML 属性值必须加引号

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>
<note>
<to>Tove</to>
<from>Jani</from>
<heading>Reminder</heading>
<body>Don't forget me this weekend!</body>
</note>
```

## 验证XML文档

合法的 XML 文档是"形式良好"的 XML 文档,这也符合文档类型定义(DTD)的规则:

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE note SYSTEM "Note.dtd">
<note>
<to>Tove</to>
<from>Jani</from>
<heading>Reminder</heading>
<body>Don't forget me this weekend!</body>
</note>
```

在上面的实例中,DOCTYPE 声明是对外部 DTD 文件的引用.<br>
下面的段落展示了这个文件的内容.

## XML DTD

DTD 的目的是定义 XML 文档的结构.<br>
它使用一系列合法的元素来定义文档结构:

```xml
<!DOCTYPE note
[
<!ELEMENT note (to,from,heading,body)>
<!ELEMENT to (#PCDATA)>
<!ELEMENT from (#PCDATA)>
<!ELEMENT heading (#PCDATA)>
<!ELEMENT body (#PCDATA)>
]>
```

## XML Schema

W3C 支持一种基于 XML 的 DTD 代替者,它名为 XML Schema:

```xml
<xs:element name="note">

<xs:complexType>
<xs:sequence>
<xs:element name="to" type="xs:string"/>
<xs:element name="from" type="xs:string"/>
<xs:element name="heading" type="xs:string"/>
<xs:element name="body" type="xs:string"/>
</xs:sequence>
</xs:complexType>

</xs:element>
```

# 使用CSS显示XML

通过使用 CSS(Cascading Style Sheets 层叠样式表),可以添加显示信息到XML文档中.

使用 CSS 来格式化 XML 文档是有可能的.

就是关于如何使用 CSS 样式表来格式化 XML 文档:

请看这个 XML 文件:[CD 目录](http://www.runoob.com/try/xml/cd_catalog.xml)

然后看这个样式表:[CSS 文件](http://www.runoob.com/try/xml/cd_catalog.css)

最后,请查看:[使用 CSS 文件格式化的 CD 目录](http://www.runoob.com/try/xml/cd_catalog_with_css.xml)

下面是 XML 文件的一小部分.<br>
第二行把 XML 文件链接到 CSS 文件:

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/css" href="cd_catalog.css"?>
<CATALOG>
<CD>
<TITLE>Empire Burlesque</TITLE>
<ARTIST>Bob Dylan</ARTIST>
<COUNTRY>USA</COUNTRY>
<COMPANY>Columbia</COMPANY>
<PRICE>10.90</PRICE>
<YEAR>1985</YEAR>
</CD>
<CD>
<TITLE>Hide your heart</TITLE>
<ARTIST>Bonnie Tyler</ARTIST>
<COUNTRY>UK</COUNTRY>
<COMPANY>CBS Records</COMPANY>
<PRICE>9.90</PRICE>
<YEAR>1988</YEAR>
</CD>
.
.
.
</CATALOG>
```

```css
CATALOG {
  background-color: #ffffff;
  width: 100%;
}
CD {
  display: block;
  margin-bottom: 30pt;
  margin-left: 0;
}
TITLE {
  color: #FF0000;
  font-size: 20pt;
}
ARTIST {
  color: #0000FF;
  font-size: 20pt;
}
COUNTRY,PRICE,YEAR,COMPANY {
  display: block;
  color: #000000;
  margin-left: 20pt;
}
```

使用 CSS 格式化 XML 不是常用的方法,W3C 推荐使用 XSLT.

# 使用XSLT显示XML

通过使用XSLT,可以把 XML 文档转换成 HTML 格式.

XSLT是首选的 XML 样式表语言.

XSLT(eXtensible Stylesheet Language Transformations)远比 CSS 更加完善.

# XMLHttpRequest 对象

> XMLHttpRequest 对象用于在后台与服务器交换数据.

XMLHttpRequest 对象可以做到:

0. 在不重新加载页面的情况下更新网页
0. 在页面已加载后从服务器请求数据
0. 在页面已加载后从服务器接收数据
0. 在后台向服务器发送数据

## 创建一个 XMLHttpRequest 对象

所有现代浏览器(IE7+、Firefox、Chrome、Safari 和 Opera)都有内建的 XMLHttpRequest 对象.

创建 XMLHttpRequest 对象的语法:

```xml
xmlhttp=new XMLHttpRequest();
<!-- 旧版本的Internet Explorer(IE5和IE6)中使用 ActiveX 对象 -->
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
```

# XML解析器

> 所有现代浏览器都有内建的 XML 解析器.

XML解析器把XML文档转换为XML DOM对象-可通过JavaScript操作的对象.

## 解析 XML 文档

下面的代码片段把XML文档解析到XML DOM对象中:

```js
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.open("GET","books.xml",false);
xmlhttp.send();
xmlDoc=xmlhttp.responseXML;
```

## 解析 XML 字符串

下面的代码片段把XML字符串解析到XML DOM对象中:

```js
txt="<bookstore><book>";
txt=txt+"<title>Everyday Italian</title>";
txt=txt+"<author>Giada De Laurentiis</author>";
txt=txt+"<year>2005</year>";
txt=txt+"</book></bookstore>";

if (window.DOMParser)
{
parser=new DOMParser();
xmlDoc=parser.parseFromString(txt,"text/xml");
}
else // Internet Explorer
{
xmlDoc=new ActiveXObject("Microsoft.XML DOM");
xmlDoc.async=false;
xmlDoc.loadXML(txt); 
}
```

Internet Explorer 使用 loadXML() 方法来解析 XML 字符串,而其他浏览器使用 DOMParser 对象.

## 跨域访问

出于安全方面的原因,现代的浏览器不允许跨域的访问.

这意味着,网页以及它试图加载的XML文件,都必须位于相同的服务器上.

# XML DOM

DOM(Document Object Model 文档对象模型)定义了访问和操作文档的标准方法.

XML DOM(XML Document Object Model)定义了访问和操作 XML 文档的标准方法.

XML DOM 把XML文档作为树结构来查看.

所有元素可以通过 DOM 树来访问.<br>
可以修改或删除它们的内容,并创建新的元素.<br>
元素,它们的文本,以及它们的属性,都被认为是节点.

## HTML DOM

HTML DOM 定义了访问和操作HTML文档的标准方法.

所有 HTML 元素可以通过HTML DOM来访问.

## HTML 页面显示 XML 数据

打开一个[XML文件](http://www.runoob.com/try/xml/cd_catalog.xml),然后遍历每个 CD 元素,并显示HTML 表格中的 ARTIST 元素和 TITLE 元素的值:

```html
<html>
<body>

<script>
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.open("GET","cd_catalog.xml",false);
xmlhttp.send();
xmlDoc=xmlhttp.responseXML; 

document.write("<table border='1'>");
var x=xmlDoc.getElementsByTagName("CD");
for (i=0;i<x.length;i++)
{ 
document.write("<tr><td>");
document.write(x[i].getElementsByTagName("ARTIST")[0].childNodes[0].nodeValue);
document.write("</td><td>");
document.write(x[i].getElementsByTagName("TITLE")[0].childNodes[0].nodeValue);
document.write("</td></tr>");
}
document.write("</table>");
</script>

</body>
</html>
```

# XML 应用程序

演示一些基于 XML, HTML, XML DOM 和 JavaScript 构建的小型 XML 应用程序.

继续使用上面的[XML文件](http://www.runoob.com/try/xml/cd_catalog.xml).

## 在 HTML div 元素中显示第一个 CD

从第一个 CD 元素中获取 XML 数据,然后在 id="showCD" 的 HTML 元素中显示数据.<br>
displayCD() 函数在页面加载时调用:

```js
x=xmlDoc.getElementsByTagName("CD");
i=0;

function displayCD()
{
artist=(x[i].getElementsByTagName("ARTIST")[0].childNodes[0].nodeValue);
title=(x[i].getElementsByTagName("TITLE")[0].childNodes[0].nodeValue);
year=(x[i].getElementsByTagName("YEAR")[0].childNodes[0].nodeValue);
txt="Artist: " + artist + "<br />Title: " + title + "<br />Year: "+ year;
document.getElementById("showCD").innerHTML=txt;
}
```

## 添加导航脚本

为了向上面的实例添加导航(功能),需要创建 next() 和 previous() 两个函数:

```js
function next(){
  if (i<x.length-1){
    i++;
    displayCD();
  }
}

function previous(){ 
  if (i>0){
    i--;
    displayCD();
  }
}
```

# XML命名空间

XML命名空间提供避免元素命名冲突的方法.

## 命名冲突

在XML中,元素名称是由开发者定义的,当两个不同的文档使用相同的元素名时,就会发生命名冲突.

这个XML携带HTML表格的信息:

```xml
<table>
<tr>
<td>Apples</td>
<td>Bananas</td>
</tr>
</table>
```

这个 XML 文档携带有关桌子的信息(一件家具):

```xml
<table>
<name>African Coffee Table</name>
<width>80</width>
<length>120</length>
</table>
```

假如这两个XML文档被一起使用,由于两个文档都包含带有不同内容和定义的 \<table> 元素,就会发生命名冲突.

XML 解析器无法确定如何处理这类冲突.

## 使用前缀来避免命名冲突

在XML中的命名冲突可以通过使用名称前缀从而容易地避免.

该XML携带某个HTML表格和某件家具的信息:

```xml
<h:table>
<h:tr>
<h:td>Apples</h:td>
<h:td>Bananas</h:td>
</h:tr>
</h:table>
```

```xml
<f:table>
<f:name>African Coffee Table</f:name>
<f:width>80</f:width>
<f:length>120</f:length>
</f:table>
```

在上面的实例中,不会有冲突,因为两个 \<table> 元素有不同的名称.

## xmlns 属性

当在XML中使用前缀时,一个所谓的用于前缀的命名空间必须被定义.

命名空间是在元素的开始标签的 xmlns 属性中定义的.

命名空间声明的语法如下.<br>
xmlns:前缀="URI".

```xml
<root>

<h:table xmlns:h="http://www.w3.org/TR/html4/">
<h:tr>
<h:td>Apples</h:td>
<h:td>Bananas</h:td>
</h:tr>
</h:table>

<f:table xmlns:f="http://www.w3cschool.cc/furniture">
<f:name>African Coffee Table</f:name>
<f:width>80</f:width>
<f:length>120</f:length>
</f:table>

</root>
```

在上面的实例中,\<table> 标签的 xmlns 属性定义了 h: 和 f: 前缀的合格命名空间.

当命名空间被定义在元素的开始标签中时,所有带有相同前缀的子元素都会与同一个命名空间相关联.

命名空间,可以在他们被使用的元素中或者在 XML 根元素中声明:

```xml
<root xmlns:h="http://www.w3.org/TR/html4/"
xmlns:f="http://www.w3cschool.cc/furniture">

<h:table>
<h:tr>
<h:td>Apples</h:td>
<h:td>Bananas</h:td>
</h:tr>
</h:table>

<f:table>
<f:name>African Coffee Table</f:name>
<f:width>80</f:width>
<f:length>120</f:length>
</f:table>

</root>
```

命名空间URI不会被解析器用于查找信息.

其目的是赋予命名空间一个惟一的名称.<br>
不过,很多公司常常会作为指针来使用命名空间指向实际存在的网页,这个网页包含关于命名空间的信息.

统一资源标识符(URI,全称 Uniform Resource Identifier),<br>
统一资源标识符(URI)是一串可以标识因特网资源的字符.

最常用的URI是用来标识因特网域名地址的统一资源定位器(URL).<br>
另一个不那么常用的URI是统一资源命名(URN).

在这个实例中,仅使用URL.

## 默认的命名空间

为元素定义默认的命名空间可以省去在所有的子元素中使用前缀的工作.<br>
它的语法如下:

```
xmlns="namespaceURI"
```

这个XML携带HTML表格的信息:

```xml
<table xmlns="http://www.w3.org/TR/html4/">
<tr>
<td>Apples</td>
<td>Bananas</td>
</tr>
</table>
```

这个XML携带有关一件家具的信息:

```xml
<table xmlns="http://www.w3schools.com/furniture">
<name>African Coffee Table</name>
<width>80</width>
<length>120</length>
</table>
```

## 实际使用中的命名空间

XSLT是一种用于把XML文档转换为其他格式的XML语言,比如HTML.

在下面的XSLT文档中,可以看到,大多数的标签是HTML标签.

非HTML的标签都有前缀xsl,并由此命名空间标识:xmlns:xsl="http://www.w3.org/1999/XSL/Transform"

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<html>
<body>
<h2>My CD Collection</h2>
<table border="1">
<tr>
<th align="left">Title</th>
<th align="left">Artist</th>
</tr>
<xsl:for-each select="catalog/cd">
<tr>
<td><xsl:value-of select="title"/></td>
<td><xsl:value-of select="artist"/></td>
</tr>
</xsl:for-each>
</table>
</body>
</html>
</xsl:template>

</xsl:stylesheet>
```

# XML CDATA

XML文档中的所有文本均会被解析器解析.

只有CDATA区段中的文本会被解析器忽略.

## PCDATA-被解析的字符数据

XML解析器通常会解析XML文档中所有的文本.

当某个XML元素被解析时,其标签之间的文本也会被解析:

```xml
<message>This text is also parsed</message>
```

解析器之所以这么做是因为XML元素可包含其他元素,就像这个实例中,其中的 \<name> 元素包含着另外的两个元素(first 和 last):

```xml
<name><first>Bill</first><last>Gates</last></name>
```

而解析器会把它分解为像这样的子元素:

```xml
<name>
<first>Bill</first>
<last>Gates</last>
</name>
```

解析字符数据(PCDATA)是XML解析器解析的文本数据使用的一个术语.

## CDATA - (未解析)字符数据

术语 CDATA 是不应该由XML解析器解析的文本数据.

像"<"和"&"字符在XML元素中都是非法的.

"<"会产生错误,因为解析器会把该字符解释为新元素的开始.

"&"会产生错误,因为解析器会把该字符解释为字符实体的开始.

某些文本,比如 JavaScript 代码,包含大量 "<" 或 "&" 字符.<br>
为了避免错误,可以将脚本代码定义为 CDATA.

CDATA 部分中的所有内容都会被解析器忽略.

CDATA 部分由 "<![CDATA[" 开始,由 "]]>" 结束:

```js
<script>
<![CDATA[
function matchwo(a,b)
{
if (a < b && a < 0) then
{
return 1;
}
else
{
return 0;
}
}
]]>
</script>
```

在上面的实例中,解析器会忽略 CDATA 部分中的所有内容.

关于 CDATA 部分的注释:

CDATA 部分不能包含字符串 "]]>".<br>
也不允许嵌套的 CDATA 部分.

标记 CDATA 部分结尾的 "]]>" 不能包含空格或换行.

# XML编码

XML文档可以包含非ASCII字符,比如挪威语æøå,或者法语êèé.

为了避免错误,需要规定XML编码,或者将XML文件存为Unicode.

# XML DOM高级

中使用的XML文件:

```xml
<!--  Edited by XMLSpy®  -->
<bookstore>
<book category="COOKING">
<title lang="en">Everyday Italian</title>
<author>Giada De Laurentiis</author>
<year>2005</year>
<price>30.00</price>
</book>
<book category="CHILDREN">
<title lang="en">Harry Potter</title>
<author>J K. Rowling</author>
<year>2005</year>
<price>29.99</price>
</book>
<book category="WEB">
<title lang="en">XQuery Kick Start</title>
<author>James McGovern</author>
<author>Per Bothner</author>
<author>Kurt Cagle</author>
<author>James Linn</author>
<author>Vaidyanathan Nagarajan</author>
<year>2003</year>
<price>49.99</price>
</book>
<book category="WEB">
<title lang="en">Learning XML</title>
<author>Erik T. Ray</author>
<year>2003</year>
<price>39.95</price>
</book>
</bookstore>
```

## 获取元素的值


检索第一个\<title>元素的文本值:

```xml
txt=xmlDoc.getElementsByTagName("title")[0].childNodes[0].nodeValue;
```

## 获取属性的值

检索第一个\<title>元素的"lang"属性的文本值:

```xml
txt=xmlDoc.getElementsByTagName("title")[0].getAttribute("lang");
```

## 改变元素的值

改变第一个\<title>元素的文本值:

```xml
x=xmlDoc.getElementsByTagName("title")[0].childNodes[0];
x.nodeValue="EasyCooking";
```

## 创建新的属性

XML DOM的setAttribute()方法可用于改变现有的属性值,或创建一个新的属性.

创建了一个新的属性(edition="first"),然后把它添加到每一个\<book>元素中:

```xml
x=xmlDoc.getElementsByTagName("book");

for(i=0;i<x.length;i++)
{
x[i].setAttribute("edition","first");
}
```

## 创建元素

XML DOM的createElement()方法创建一个新的元素节点.

XML DOM的createTextNode()方法创建一个新的文本节点.

XML DOM的appendChild()方法向节点添加子节点(在最后一个子节点之后).

如需创建带有文本内容的新元素,需要同时创建元一个新的元素节点和一个新的文本节点,然后把他追加到现有的节点.

创建了一个新的元素(<edition>),带有如下文本:First,然后把它添加到第一个<book>元素:

```xml
<!-- 创建一个<edition>元素 -->
newel=xmlDoc.createElement("edition");
<!-- 创建值为"First"的文本节点 -->
newtext=xmlDoc.createTextNode("First");
<!-- 把这个文本节点追加到新的<edition>元素 -->
newel.appendChild(newtext);

<!-- 把<edition>元素追加到第一个<book>元素 -->
x=xmlDoc.getElementsByTagName("book");
x[0].appendChild(newel);
```

## 删除元素

删除第一个\<book>元素的第一个节点:

```xml
x=xmlDoc.getElementsByTagName("book")[0];
x.removeChild(x.childNodes[0]);
```

上面实例的结果可能会根据所用的浏览器而不同.<br>
Firefox把新行字符当作空的文本节点,而InternetExplorer不是这样.

# XML相关技术

名称|说明|作用
-|-|-
XHTML|可扩展HTML|更严格更纯净的基于XML的HTML版本
XMLDOM|XML文档对象模型|访问和操作XML的标准文档模型
XSL|可扩展样式表语言|XSL包含三个部分:<br>XSLT &nbsp;XSL转换 &nbsp;把XML转换为其他格式,比如HTML<br>XSL-FO &nbsp;XSL格式化对象 &nbsp;用于格式化XML文档的语言<br>XPath&nbsp;用于导航XML文档的语言
XQuery|XML查询语言|基于XML的用于查询XML数据的语言
DTD|文档类型定义|用于定义XML文档中的合法元素的标准
XSD|XML架构|基于XML的DTD替代物
XLink|XML链接语言|在XML文档中创建超级链接的语言
XPointer|XML指针语言|允许XLink超级链接指向XML文档中更多具体的部分
SOAP|简单对象访问协议|允许应用程序在HTTP之上交换信息的基于XML的协议
WSDL|Web服务描述语言|用于描述网络服务的基于XML的语言
RDF|资源描述框架|用于描述网络资源的基于XML的语言
RSS|真正简易聚合|聚合新闻以及类新闻站点内容的格式
SVG|可伸缩矢量图形|定义XML格式的图形

# 参考资料

> [菜鸟教程 | XML 教程](http://www.runoob.com/xml/xml-tutorial.html)

> [菜鸟教程 | XML Schema 教程](http://www.runoob.com/schema/schema-tutorial.html)

> [菜鸟教程 | XSLT 教程](http://www.runoob.com/xsl/xsl-tutorial.html)