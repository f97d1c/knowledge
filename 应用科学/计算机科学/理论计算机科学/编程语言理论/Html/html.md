<!-- TOC -->

- [常用标签](#常用标签)
  - [表格(table)](#表格table)
  - [输入框(input)](#输入框input)
  - [font-字体](#font-字体)
  - [iframe](#iframe)
  - [hr-水平分割线](#hr-水平分割线)
  - [ins-插入字](#ins-插入字)
  - [del-删除字](#del-删除字)
  - [en-强调](#en-强调)
  - [sup-上标](#sup-上标)
  - [sub-下标](#sub-下标)
  - [键盘文本 <kbd>Ctrl</kbd>](#键盘文本-kbdctrlkbd)
  - [video-视频流播放](#video-视频流播放)
- [input](#input)
  - [disabled和readonly](#disabled和readonly)
- [参考资料](#参考资料)

<!-- /TOC -->
&nbsp;|&nbsp;
-|-
常用网站|[W3C](http://www.w3school.com.cn)&emsp;

# 常用标签
## 表格(table)

标签|属性
-|-
td|colspan 跨列<br>rowspan 跨行

## 输入框(input)

属性|值|描述
-|-|-
accept|mime_type|规定通过文件上传来提交的文件的类型.
align|left<br>right<br>top<br>middle<br>bottom|规定图像输入的对齐方式(*不赞成使用*)
alt|text|定义图像输入的替代文本.
autocomplete|on<br>off|规定是否使用输入字段的自动完成功能.
autofocus|autofocus|规定输入字段在页面加载时是否获得焦点(不适用于 type="hidden")
checked|checked|规定此 input 元素首次加载时应当被选中.
disabled|disabled|当 input 元素加载时禁用此元素.
form|formname|规定输入字段所属的一个或多个表单.
formaction|URL|覆盖表单的 action 属性.<br>(适用于 type="submit" 和 type="image")
formenctype|见注释|覆盖表单的 enctype 属性.<br>(适用于 type="submit" 和 type="image")
formmethod|get<br>post|覆盖表单的 method 属性.<br>(适用于 type="submit" 和 type="image")
formnovalidate|formnovalidate|覆盖表单的 novalidate 属性.<br>如果使用该属性，则提交表单时不进行验证.
formtarget|_blank<br>_self<br>_parent<br>_top<br>framename|覆盖表单的 target 属性.<br>(适用于 type="submit" 和 type="image")
height|pixels<br>%|定义 input 字段的高度.<br>(适用于 type="image")
list|datalist-id|引用包含输入字段的预定义选项的 datalist .
max|number<br>date|规定输入字段的最大值.<br>与 "min" 属性配合使用，来创建合法值的范围.
maxlength|number|规定输入字段中的字符的最大长度.
min|number<br>date|规定输入字段的最小值.<br>与 "max" 属性配合使用，来创建合法值的范围.
multiple|multiple|如果使用该属性，则允许一个以上的值.
name|field_name|定义 input 元素的名称.
pattern|regexp_pattern|规定输入字段的值的模式或格式.<br>例如 pattern="[0-9]" 表示输入值必须是 0 与 9 之间的数字.
placeholder|text|规定帮助用户填写输入字段的提示.
readonly|readonly|规定输入字段为只读.
required|required|指示输入字段的值是必需的.
size|number_of_char|定义输入字段的宽度.
src|URL|定义以提交按钮形式显示的图像的 URL.
step|number|规定输入字的的合法数字间隔.
type|button<br>checkbox<br>file<br>hidden<br>image<br>password<br>radio<br>reset<br>submit<br>text|规定 input 元素的类型.
value|value|规定 input 元素的值.
width|pixels<br>%|定义 input 字段的宽度.<br>(适用于 type="image")

## font-字体
设置字体的大小及颜色
```html
<font size="3" color="red">红色</font>
```
<font size="3" color="red">红色</font>

## iframe
在当前页面的特定区域展示其他网址页面内容.<br>
可以把需要的文本放置在 `<iframe>` 和 `</iframe>` 之间,这样就可以应对不支持该的浏览器.如:<br>

```html
<iframe src="http://www.runoob.com">
  <p>您的浏览器不支持  iframe 标签.</p>
</iframe>
```

>[菜鸟教程](http://www.runoob.com/tags/tag-iframe.html)

## hr-水平分割线
```html
<hr>
```
## ins-插入字
```html
<ins>文字下方会带有横线</ins>
```
<ins>文字下方会带有横线</ins>

## del-删除字
```html
<del>原价:998</del>
```
<del>原价:998</del>

## en-强调

```html
<en>强调</en>
```
<en>强调</en>

## sup-上标

```html
上标<sup>TM</sup>
```
上标<sup>TM</sup>

## sub-下标

```html
下标<sub>TM</sub>
```
下标<sub>TM</sub>

## 键盘文本 <kbd>Ctrl</kbd>

```html
键盘文本 <kbd>Ctrl</kbd>
```
键盘文本 <kbd>Ctrl</kbd>


## video-视频流播放

**IE 8 或更早版本的 IE 浏览器不支持video标签**<br>
video标签是 HTML5 的新标签
```html
<video>
    <source src="" type="MIME-type">
    <!--可以在 <video> 和 </video> 标签之间放置文本内容,这样不支持 <video> 元素的浏览器就可以显示出该标签的信息 -->
    您的浏览器不支持 video 标签.
</video>
```
音频格式|说明|MIME-type
-|-|-
MP4|MPEG 4文件使用 H264 视频编解码器和AAC音频编解码器|video/mp4
WebM|WebM 文件使用 VP8 视频编解码器和 Vorbis 音频编解码器|video/webm
Ogg|Ogg 文件使用 Theora 视频编解码器和 Vorbis音频编解码器|video/ogg

# input

## disabled和readonly

名称|作用
-|-
disabled|被禁用的input元素既**不可用**,也不可点击.<br>***参数值不会传向后台***
readonly|参数只读<br>***参数值不会传向后台***

# 参考资料

> [菜鸟教程 | video 标签](http://www.runoob.com/tags/tag-video.html)

> [w3school | HTML \<input> 标签](https://www.w3school.com.cn/tags/tag_input.asp)