module BlackBox
  module Agent
    module Restful
      # 所有对外提供服务功能应该都由该module负责
      module Public
        extend self

        def exposed_api
          tmp = {}
          BlackBox::Agent::Public::exposed_api.each do |key, value|
            extra_params = [
              {key: 'api_code', name: '接口标识', explain: '访问该接口的对应唯一标识', regular: /^[^\s]+$/, default: nil, example: key, necessary: true, value_type: :string},
              {key: 'encrypt_pub', name: '加密公钥', explain: '使用提供的公钥对返回数据进行RSA加密(需进行Base64及UrlEncode编码)(TODO: 尚未实现).', default: nil, necessary: false, value_type: :string},
            ]

            value[:attributes] = (value[:attributes] + extra_params )
            tmp.merge!({key => value})
          end
          tmp
        end 

      end
    end
  end
end