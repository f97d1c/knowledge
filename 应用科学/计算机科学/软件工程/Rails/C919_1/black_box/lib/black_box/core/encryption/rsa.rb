module BlackBox
  module Core
    module Encryption
      class Rsa

        def initialize(public_key: Rsa.public_key)
          @rsa_pub  = OpenSSL::PKey::RSA.new(public_key)
          @rsa_pri = Rsa.private_object
        end

        # 公钥加密
        def public_encrypt(content:, format: :base_64)
          encrypt_content = @rsa_pub.public_encrypt(content)
          case format
          when :base_64
            Base64.encode64(encrypt_content)
          else
            encrypt_content
          end 
        end

        # 公钥解密
        def public_decrypt(encrypt_content:)
          @rsa_pub.public_decrypt(encrypt_content)
        end

        # 私钥解密
        def private_decrypt(encrypt_content:, format: :base_64)
          encrypt_content = case format
          when :base_64
            Base64.decode64(encrypt_content)
          else
            encrypt_content
          end
          @rsa_pri.private_decrypt(encrypt_content)
        end

        # 私钥加密
        def private_encrypt(content:)
          @rsa_pri.private_encrypt(content)
        end

        class << self

          def public_key
            reload_keys unless ::File.exist?(store_path.public)
            ::File.read(store_path.public)
          end

          def private_object
            OpenSSL::PKey::RSA.new(private_key)
          end

          private
            def store_path
              dir = 'config/black_box/rsa'
              OpenStruct.new({
                dir: dir,
                private: dir+'/private.pem',
                public: dir+'/public.pem'
              })
            end

            def reload_keys
              rsa = OpenSSL::PKey::RSA.new(10240)
              FileUtils.mkdir_p(store_path.dir) unless Dir.exist?(store_path.dir)
              ::File.open(store_path.private, 'w'){|file| file.write(rsa.to_pem)}
              ::File.open(store_path.public, 'w'){|file| file.write(rsa.public_key.to_pem)}
              [true, '']
            end

            def private_key
              reload_keys unless ::File.exist?(store_path.private)
              ::File.read(store_path.private)
            end 
        end

      end
    end
  end
end