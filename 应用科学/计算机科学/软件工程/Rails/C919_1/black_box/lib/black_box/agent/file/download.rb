module BlackBox
  module Agent
    module File
      module Download
        extend self

        def exposed_api
          {
            send_file: send_file(show_how: true),
          }
        end

        def send_file(show_how: false, **args)
          method_name = '下载文档'
          explain = '根据生成文档时返回的rsa_pub参数下载对应文档(该接口目前不适用于示例页面,页面由ajax请求原因,正在兼容中.)'

          described = Described.new(bind: binding)
          described.set_attribute(key: 'rsa_pub', name: 'RsaPub', explain: '生成文档时返回的rsa_pub', example: '', value_type: :string)
          self_described = described.get_described
          return self_described if show_how

          res = Wireway::Standard::inspect_all(ideal: self_described[:attributes], reality: args)
          return res unless res[0]

          return [false, 'rsa_pub 参数内容不能为空'] unless args[:rsa_pub]
          encrypt_content = URI.decode(args[:rsa_pub])
          rsa = Core::Encryption::Rsa.new
          content = rsa.private_decrypt(encrypt_content: encrypt_content) rescue (return [false, 'rsa_pub 参数内容格式无效'])
          decrypt_params = JSON.parse(content).deep_symbolize_keys rescue (return [false, 'rsa_pub 传参内容类型错误'])
          return [false, 'rsa_pub 传参内容缺失'] unless decrypt_params[:file_path].present?
          res = Core::File::Path::legal?(file_path: decrypt_params[:file_path])
          return [false, res[1]] unless res[0]

          [true, {type: :send_file, file_path: decrypt_params[:file_path]}]
        end
        
      end
    end
  end
end