require 'pry-byebug'
require 'rails/all'

module BlackBox
  class Engine < ::Rails::Engine
    isolate_namespace BlackBox
    
    config.eager_load_paths += [File.expand_path("../../lib/", __dir__)]
    config.eager_load_paths += [File.expand_path("lib/", __dir__)]
    require 'wireway'
  end
end
