BlackBox::Engine.routes.draw do
  resources :restful, only:[] do 
    collection do
      get :exposed_api 
      post :request_api
      get :request_api_params
      get :download
    end 
  end
end
