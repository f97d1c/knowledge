module BlackBox
  class RestfulController < ApplicationController
    
    def example
      @exposed_api = BlackBox::Agent::Restful::Public::exposed_api
    end

    def exposed_api
      exposed_api = {}
      BlackBox::Agent::Restful::Public::exposed_api.each do |key, info|
        exposed_api.merge!({key => {name: info[:method_name], explain: info[:explain]}})
      end

      render json:{
        black_box: {
          name: '文件处理接口',
          explain: '提供提供Excel生成,归档在内的文件存储服务.',
          exposed_api: exposed_api
        }
      }
    end


    def request_api_params
      api_code = params[:api_code]
      return (render json: {success: false, result: '接口标识(api_code)不能为空'}) unless api_code
      exposed_api = BlackBox::Agent::Restful::Public::exposed_api
      current_api = exposed_api[api_code.to_sym]      
      render json: {success: true, request_params: current_api[:attributes], explain: current_api[:explain]}
    end

    def request_api
      exposed_api = BlackBox::Agent::Restful::Public::exposed_api
      current_api = exposed_api[params[:api_code].to_sym]

      # TODO 初步了解为rails5的安全机制 去除存在风险 暂时先这样
      args = params.permit!.to_hash.reject!{|key| ["response_type","api_code", "action", "controller"].include?(key)}
      
      if current_api
        res = begin
          current_api[:method_path].call(args.deep_symbolize_keys)
        rescue
          info = {errors: {message: $!.to_s, path: $@}}
          print info
          return [false, 'BlackBox运行时异常', info]
        end
      end

      unless current_api
        res = [false, "根据api_code未找到相关授权接口, 允许调用接口及相关介绍参考:#{BlackBox::Agent::Public::explain_api.to_a.map{|array| array.join(':')}.join(',')}"]
      end

      return (render json: {success: false, result: res[1]}) unless res[0]

      case (res[1][:type] || '').to_sym
      when :send_file
        send_file(
          res[1][:file_path],
          :type => 'text/excel;charset=utf-8;header=present',
          :type => 'application/octet-stream',
          :filename => "#{Time.now.strftime("%Y-%m-%d_%H%M")}_#{res[1][:file_path].split('/').last}",
          :content_type => MIME::Types.of(::File.basename(res[1][:file_path]))[0].content_type,
          :disposition => 'attachment',
          :x_sendfile => true,
        )
      else
        return render json: ({success: true}.merge!(res[1]))
      end
    end

  end
end