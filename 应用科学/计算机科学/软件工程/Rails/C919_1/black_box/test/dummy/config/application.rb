require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)
require "black_box"

module Dummy
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    # config.action_dispatch.x_sendfile_header="X-Sendfile"
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end

