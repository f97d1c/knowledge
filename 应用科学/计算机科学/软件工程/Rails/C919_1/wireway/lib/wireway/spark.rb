module Wireway
  module Spark
    def self.method_missing(method_name, local: true, **args)
      begin
        target = Wireway::Core::Request
        unless [true, 'true', 1, '1', 'yes'].include?(local)
          return [false, "未找到名称为:#{method_name}的方法定义."] unless target.respond_to?(method_name)
        end
        res = target.send(method_name, args)
      rescue
        error_msg = [false, '[Wireway]请求异常', {errors: {message: $!.to_s, path: $@}}]
        return error_msg unless [true, 'true', 1, '1', 'yes'].include?(local)
        # 当以Gem形式的组件无法处理请求时,尝试请求线上版组件是否可以正常处理请求
        # 即当gem出现问题时会请求线上服务 有利于断点排查
        args.merge!(platform: method_name)
        res = target.wireway(args)
      end
    end

  end
end