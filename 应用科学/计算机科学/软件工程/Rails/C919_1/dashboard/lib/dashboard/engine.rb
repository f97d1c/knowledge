module Dashboard
  class Engine < ::Rails::Engine
    isolate_namespace Dashboard
    require 'commonmarker'
    require 'rest-client'
    require 'colorize'
    require 'wireway'
  end
end
