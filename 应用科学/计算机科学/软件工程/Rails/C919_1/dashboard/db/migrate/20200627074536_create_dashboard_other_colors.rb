class CreateDashboardOtherColors < ActiveRecord::Migration[5.2]
  def change
    create_table :dashboard_other_colors do |t|

      t.string :name, null: false, comment: '色号'
      t.index :name

      t.string :hex, null: false, comment: '16进制表示'
      t.index :hex

      t.string :rgb, null: false, comment: 'RGB表示'
      t.index :rgb

      t.string :lab, null: false, comment: 'LAB表示'
      t.index :lab

      t.string :lib, null: false, comment: '色库'
      t.index :lib

      t.string :form, null: false, comment: '所属纯色'
      t.index :form

      t.datetime :deleted_at
      t.index :deleted_at
      
      t.timestamps
    end
  end
end
