class Core {
  constructor(params){

    Object.prototype.renderElement = function () {
      let defaultValue = {
        element: undefined,
        innerHtml: undefined,
      }

      let attributes = Object.keys(this).filter(key => { return !Object.keys(defaultValue).includes(key)})
      let params = Object.assign(defaultValue, this)

      let element = document.createElement(params.element)
      for(let key of attributes){
        element.setAttribute(key, params[key])
      }

      if (!!params.innerHtml) element.innerHTML = params.innerHtml
      return [true, element]
    }
    
  }
}

class SearchForm extends Core {
  // class SearchForm {
  space = {}

  constructor(params) {
    super()

    let defaultValue = {
      items: undefined,
      class: 'table table-bordered table-striped table-hover',
      splitSize: 2, // 每行元素个数
      labelRatio: 30, // 名称单元格占比
      onchange: undefined,
      id: 'searchForm',
      buttonOptions: [], // 选项按钮
    }
    params = Object.assign(defaultValue, params)

    for (let [key, value] of Object.entries(params)) {
      if(!value) continue;
      this.space[key] = value
    }
    this.space.columnKeys ||= this.space.columnNames

    
    let form = {
      element: 'form',
      id: this.space.id,
    }
    this.space.form = form.renderElement()[1]

    this.space.table = document.createElement('table')
    this.space.table.setAttribute('class', this.space.class)
    this.space.form.appendChild(this.space.table)

    this.space.tbody = document.createElement('tbody')
    this.space.table.appendChild(this.space.tbody)
  }

  renderItems(params){
    let defaultValue = {
      items: this.space.items,
      splitSize: this.space.splitSize,
      labelRatio: this.space.labelRatio,
      onchange: this.space.onchange,
    }
    params = Object.assign(defaultValue, params)
    params.splitSize = parseInt(params.splitSize)
    
    this.space.tbody.innerHTML = ''

    let labelRatio =  (100/params.splitSize)*(params.labelRatio/100)
    let contentRatio =  (100/params.splitSize)*((100-params.labelRatio)/100)

    // [items1, item2, item3, item4] =splitSize=> [[items1, item2], [item3, item4]]
    let formatedItems = [];
    for (let index = 0; index < params.items.length; index += params.splitSize) {
      formatedItems.push(params.items.slice(index, index + params.splitSize))
    }

    formatedItems.forEach(array => {
      let tr = document.createElement('tr')
      array.forEach(item => {

        let label = {
          element: 'td', 
          class: 'text-right mr-2', 
          style: 'width: '+labelRatio+'%;',
          innerHtml: item.name,
        }
        label = label.renderElement()[1]

        tr.appendChild(label)

        let content = document.createElement('td')
        content.setAttribute('style', 'width: '+contentRatio+'%;')

        // <select class="" name="ransack[<%= column_name%>]">
        let select = document.createElement('select')
        select.setAttribute('class', 'form-control w-25 float-left')
        select.setAttribute('name', 'ransack['+item.name+']')
        select.setAttribute('onchange', params.onchange)
        
        let condictions = {包含: 'cont', 不包含: 'not_cont', 等于: 'eq', 不等于: 'not_eq', 大于: 'gt', 小于: 'lt'}

        for(let [key, value] of Object.entries(condictions)){
          let option = {
            element: 'option',
            value: value,
            innerHtml: key
          }
          
          select.appendChild(option.renderElement()[1])
        }
        content.appendChild(select)

        let input = {
          element: 'input', 
          class: 'w-75 float-left form-control', 
          name: 'search['+item.name+']',
          innerHtml: item.value,
          onchange: params.onchange
        }
        content.appendChild(input.renderElement()[1])

        tr.appendChild(content)
      })
      this.space.tbody.appendChild(tr)
    })

  }

  renderOptions(){
    let tr = document.createElement('tr')
    let td = document.createElement('td')
    td.setAttribute('colspan', this.space.splitSize*2)
    td.setAttribute('class', 'text-center')

    let defaultButtons = [
      {
        element: 'button',
        class: 'btn btn-primary mr-3',
        onclick:"document.getElementById('"+this.space.id+"').reset(); "+this.space.onchange,
        innerHtml: '清空条件',
        type: 'button',
      }
    ]
    Array.prototype.push.apply(this.space.buttonOptions, defaultButtons);

    this.space.buttonOptions.forEach(button => {
      button = button.renderElement()[1]
      td.appendChild(button)
    })

    tr.appendChild(td)
    this.space.tbody.appendChild(tr)

  }

  init(params){
    this.renderItems(params)
    this.renderOptions(params)
    return [true, this.space.form.outerHTML]
  }

}