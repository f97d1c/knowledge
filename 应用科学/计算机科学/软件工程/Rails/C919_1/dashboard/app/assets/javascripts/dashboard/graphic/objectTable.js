class ObjectTable {
  table = {}

  constructor(params) {
    let defaultValue = {
      object: undefined,
      class: 'table table-bordered table-striped table-hover',
      splitSize: 2, // 每行元素个数
      labelRatio: 30, // 名称单元格占比
      onchange: undefined,
      id: 'table',
    }
    params = Object.assign(defaultValue, params)

    let labelRatio =  params.labelRatio
    params.labelRatio =  (100/params.splitSize)*(labelRatio/100)
    params.contentRatio =  (100/params.splitSize)*((100-labelRatio)/100)

    params.groupedKeys = [];
    let keys = Object.keys(params.object)
    for (let index = 0; index < keys.length; index += params.splitSize) {
      params.groupedKeys.push(keys.slice(index, index + params.splitSize))
    }

    for (let [key, value] of Object.entries(params)) {
      this.table[key] = value
    }

    let table = {
      element: 'table',
      class: this.table.class,
      id: this.table.id,
    }    
    this.table.self = this.renderElement(table)[1]

    this.table.tbody = document.createElement('tbody')
    this.table.self.appendChild(this.table.tbody)
  }

  isJson(item) {
    item = typeof item !== "string" ? JSON.stringify(item) : item;
  
    try {
      item = JSON.parse(item);
    } catch (e) {
      return false;
    }
  
    if (typeof item === "object" && item !== null) {
      return true;
    }
  
    return false;
  }

  renderElement (params) {
    let defaultValue = {
      element: undefined,
      innerHtml: undefined,
      outerText: undefined,
    }
  
    let object = Object.keys(params).filter(key => { return !Object.keys(defaultValue).includes(key)})
    params = Object.assign(defaultValue, params)
  
    let element = document.createElement(params.element)
    for(let key of object){
      element.setAttribute(key, params[key])
    }

    if (!!params.innerHtml){
      if (this.isJson(params.innerHtml)){
        // TODO: 1200 代表屏幕宽度 这里应该动态获取实际
        element.setAttribute('style', 'max-width: '+1600*(this.table.contentRatio/100)+'px;')
        element.innerHTML = "<pre><code>"+JSON.stringify(JSON.parse(params.innerHtml), undefined, 2)+"</code></pre>"
      }else if(typeof params.innerHtml != 'object'){
        element.innerHTML = params.innerHtml
      }else{
        debugger
        let res = this.renderElement(params.innerHtml)
        if (res[0]) element.innerHTML = res[1].outerHTML
      }
    } 
    if (!!params.outerText) element = {outerHTML: (element.outerHTML + params.outerText)}
    return [true, element]
  }

  renderTextContent(value){
    let content = {
      element: 'td',
      style: 'width: '+this.table.contentRatio+'%;',
      innerHtml: value,
    }
    // if (value == '完整参数') debugger
    content = this.renderElement(content)[1]

    return [true, content]
  }

  renderArrayContent(array){
    let content = {
      element: 'td',
      style: 'width: '+this.table.contentRatio+'%;',
      innerHtml: '',
    }

    array.forEach(item => {
      let res
      if(typeof item != 'object'){
        res = this.renderTextContent(item)
        content.innerHtml += res[1]
      }
      
      res = this.renderElement(item)
      if (res[0]) content.innerHtml+= res[1].outerHTML
    })

    content = this.renderElement(content)[1]
    return [true, content]
  }

  render(){
    this.table.groupedKeys.forEach(array => {
      let tr = document.createElement('tr')

      for (let key of array) {
        let label = {
          element: 'td', 
          class: 'text-right mr-2', 
          style: 'width: '+this.table.labelRatio+'%;',
          innerHtml: key,
        }
        
        label = this.renderElement(label)[1]
        tr.appendChild(label)

        let value = this.table.object[key]
        let res 
        if(typeof value != 'object'){
          res = this.renderTextContent(value)
        }else {
          value = [value].flat()
          res = this.renderArrayContent(value)
        }

        tr.appendChild(res[1])  
      }
      this.table.tbody.appendChild(tr)
    })
    
  }

  outerHtml(){
    this.render()
    return [true, this.table.self.outerHTML]
  }


}