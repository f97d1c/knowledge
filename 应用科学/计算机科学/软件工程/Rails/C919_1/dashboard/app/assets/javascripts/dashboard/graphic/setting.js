graphic = (typeof graphic === 'undefined') ? new Object() : graphic

class Setting {
  createControlPanel(object, params){
    var defaultValue = {
      rowCount: 1,
      title: "相关设置",
      divBgClass: 'bg-dark',
      rowCount: 1,
      pureTextContent: false
    }

    params = Object.assign(defaultValue, params)
    if (!object) return [false, '对象(object)不能为空']

    var handledObject = {}
    for (let [key, value] of Object.entries(object)) {
      let res
      switch (value.type){
        case 'input':
          res = graphic.input.renderToInput(value, {id: key})
          if (res[0]) handledObject[value.name] = res[1].outerHTML
          break;
        case 'range':
          res = graphic.input.renderToRange(value, {id: key})
          if (res[0]) handledObject[value.name] = res[1].outerHTML
          break;
        case 'radioList':
          for (let [k, v] of Object.entries(value.value)){
            res = graphic.input.renderToRadio(v, {id: key})
            handledObject[value.name]||=''
            if (res[0]) handledObject[value.name] = handledObject[value.name]+res[1].outerHTML
          }
          break;
          case 'textarea':
            let result = graphic.textarea.renderToTextarea(value, {id: key})
            if (result[0]) handledObject[value.name] = result[1].outerHTML
            break;
          case 'timeRange':
            let re = graphic.input.renderToTimeRange(value, {id: key})
            if (re[0]) handledObject[value.name] = re[1].outerHTML
            break;
          case "buttonList":
            for (let [k, v] of Object.entries(value.buttons)){
              res = graphic.button.renderToButton(v, {id: k})
              handledObject[value.name]||=''
              if (res[0]) handledObject[value.name] = handledObject[value.name]+res[1].outerHTML
            }
            break;
        default:
          break;
      }
    }

    let res = graphic.cardTable.renderCardTable(handledObject, params)
    return res
  }
}

graphic.setting = new Setting();