class Input {
 
  renderToRange (object, params) {
    let defaultValue = {
      id: undefined,
      class: 'custom-range w-100',
    }

    params = Object.assign(defaultValue, params)
    if (!object) return [false, '渲染对象(object)不能为空']

    let element = document.createElement('input')
    element.setAttribute('type', 'range')
    element.setAttribute('class', params.class)

    element.setAttribute('id', params.id)
    element.setAttribute('name', 'setting['+params.id+']')
    element.setAttribute('value', object.value)

    if (!object.options) return [true, element];
    for (const [key_, value_] of Object.entries(object.options)) {
      element.setAttribute(key_, value_)
    }

    return [true, element]
  }

  renderToRadio(object, params){
    let defaultValue = {
      id: undefined,
      labelClass: 'radio-inline mr-3',
    }
    params = Object.assign(defaultValue, params)
    if (!object) return [false, '渲染对象(object)不能为空']

    let element = document.createElement('label')
    element.setAttribute('class', params.labelClass)
    let input = document.createElement('input')
    input.setAttribute('name', 'setting['+params.id+']')
    input.setAttribute('type', 'radio')
    input.setAttribute('class', 'mr-1')
    input.setAttribute('value', object.value)
    if(object.checked) input.setAttribute('checked', 'checked')
    
    if (object.options){
      for (const [key_, value_] of Object.entries(object.options)) {
        input.setAttribute(key_, value_)
      }
    }

    element.innerHTML = input.outerHTML+object.name
    return [true, element]
  }

  renderToTimeRange(object, params){
    let defaultValue = {
      id: undefined,
      divClass: 'form-inline',
    }
    params = Object.assign(defaultValue, params)
    if (!object) return [false, '渲染对象(object)不能为空']

    let element = document.createElement('div')
    element.setAttribute('class', params.divClass)

    let inputStart = document.createElement('input')
    inputStart.setAttribute('type', 'datetime-local')
    inputStart.setAttribute('style', 'width: 29%')
    inputStart.setAttribute('value', (object.value.startAt||{}).value)
    inputStart.setAttribute('placeholder', '开始时间')
    inputStart.setAttribute('name', 'setting['+params.id+'][startAt]')
    if (object.options){
      for (const [key_, value_] of Object.entries(object.options)) {
        inputStart.setAttribute(key_, value_)
      }
    }

    let inputEnd = document.createElement('input')
    inputEnd.setAttribute('type', 'datetime-local')
    inputEnd.setAttribute('style', 'width: 29%')
    inputEnd.setAttribute('value', (object.value.endAt||{}).value)
    inputEnd.setAttribute('placeholder', '结束时间')
    inputEnd.setAttribute('name', 'setting['+params.id+'][endAt]')
    if (object.options){
      for (const [key_, value_] of Object.entries(object.options)) {
        inputEnd.setAttribute(key_, value_)
      }
    }

    element.innerHTML = inputStart.outerHTML+'&nbsp;至&nbsp;'+inputEnd.outerHTML
    return [true, element]
  }

  renderToButton(object, params){
    let defaultValue = {
      id: undefined,
      class: 'btn btn-primary ml-2',
    }
    params = Object.assign(defaultValue, params)
    if (!object) return [false, '渲染对象(object)不能为空']

    // <button type="button" class="btn btn-primary">主要按钮</button>
    let element = document.createElement('button')
    element.setAttribute('class', params.class)
    element.setAttribute('type', 'button')
    element.setAttribute('value', object.value)
    
    if (object.options){
      for (const [key_, value_] of Object.entries(object.options)) {
        element.setAttribute(key_, value_)
      }
    }

    element.innerHTML = object.name
    return [true, element]
  }

  renderToInput(object, params){
    let defaultValue = {
      id: undefined,
      class: 'form-control',
    }
    params = Object.assign(defaultValue, params)
    if (!object) return [false, '渲染对象(object)不能为空']

    // <button type="button" class="btn btn-primary">主要按钮</button>
    let element = document.createElement('input')
    element.setAttribute('class', params.class)
    element.setAttribute('type', 'input')
    element.setAttribute('value', object.value)
    
    if (object.options){
      for (const [key_, value_] of Object.entries(object.options)) {
        element.setAttribute(key_, value_)
      }
    }

    element.innerHTML = object.name
    return [true, element]
  }
}

graphic = (typeof graphic === 'undefined') ? new Object() : graphic
graphic.input = new Input()