module Dashboard
  module Other
    module RelyAsset
      
      def self.exposed_asset
        {
          bootstrap_4: {
            stylesheet: [
              "dashboard/bootstrap_4/bootstrap.min.css",
            ],
            javascript: ["dashboard/bootstrap_4/popper.js", "dashboard/bootstrap_4/bootstrap.min.js"]
          },
          chart_js: {
            javascript: ["dashboard/chart_js/chart_js.js"]
          },
          jQuery_3_5_1: {
            javascript: [
              "dashboard/jQuery/3_5_1/min.js",
              "dashboard/jQuery/serializejson.js"
            ]
          },
          amcharts_4: {
            javascript: [
              "dashboard/amcharts/v4/core.js",
              "dashboard/amcharts/v4/charts.js",
              "dashboard/amcharts/v4/animated.js",
              "dashboard/amcharts/v4/zh_Hans.js",
              "dashboard/amcharts/v4/word_cloud.js",
            ]
          },
          google_charts:{
            javascript: [
              "dashboard/google_chart/loader.js"
            ]
          },
          highcharts:{
            javascript: [
              "dashboard/highcharts/highcharts.js",
              "dashboard/highcharts/timeline.js",
              # "dashboard/highcharts/exporting.js", # 该js用于在图表右上方显示保存图片功能
              "dashboard/highcharts/accessibility.js",
              "dashboard/highcharts/data.js",
              "dashboard/highcharts/heatmap.js",
              "dashboard/highcharts/boost-canvas.js",
              "dashboard/highcharts/boost.js",
              "dashboard/highcharts/zh_CN.js",
            ]
          },
          space:{
            javascript: [
              "dashboard/space/core.js",
              "dashboard/space/string.js",
              "dashboard/space/md5.js",
            ]
          },
          graphic:{
            javascript: [
              "dashboard/graphic/cardTable.js",
              "dashboard/graphic/objectTable.js",
              "dashboard/graphic/listTable.js",
              "dashboard/graphic/searchForm.js",
              "dashboard/graphic/input.js",
              "dashboard/graphic/textarea.js",
              "dashboard/graphic/button.js",
              "dashboard/graphic/setting.js",
            ]
          },
          ff4c00:{
            stylesheet: [
              "dashboard/ff4c00/main_title.css",
            ],
            javascript: [
              "dashboard/ff4c00/header.js",
              "dashboard/ff4c00/footer.js",
            ]
          },
          wireway: {
            javascript: [
              "dashboard/wireway/main.js",
            ]
          },
        }
      end 

    end
  end
end