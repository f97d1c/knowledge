module Dashboard
  module BusinessLogic
    module Publish

      module Other
        extend self

        # 所有可调用接口集合
        def exposed_api
          {
            request_assets: request_assets(show_how: true),
            api_used_assets: api_used_assets(show_how: true),
          }
        end
        
        def request_assets(show_how: false, **args)
          method_name = '请求依赖资源'
          explain = '根据提供参数仅提供所请求资源,无其他任何额外内容'
          rely_assets = Dashboard::Other::RelyAsset::exposed_asset.keys.map(&:to_s)
          described = SelfDescribed.new(bind: binding)

          self_described = described.get_described
          return self_described if show_how
          
          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          
          [true, {generated_data: args}]
        end

        def api_used_assets(show_how: false, **args)
          method_name = '接口依赖资源列表'
          explain = '返回所有允许调用接口对应的依赖资源,可用于调用接口前预判是否需要请求对应依赖资源'
          rely_assets = []
          described = SelfDescribed.new(bind: binding)

          self_described = described.get_described
          return self_described if show_how
          
          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          
          api_used_assets = {}
          Public::exposed_api.each do |key, value| 
            assets = value[:attributes].select{|hash| hash[:key] == 'rely_assets'}[0][:example]
            api_used_assets.merge!({key => assets})
          end 
          args.merge!({api_used_assets: api_used_assets})

          [true, {generated_data: args}]
        end 

      end

    end
  end
end