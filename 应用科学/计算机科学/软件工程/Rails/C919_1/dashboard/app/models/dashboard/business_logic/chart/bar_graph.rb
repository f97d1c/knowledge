module Dashboard
  module BusinessLogic
    module Chart
      
      module BarGraph
        extend self
      
        # 所有可调用接口集合
        def exposed_api
          {
            vertical_type: vertical_type(show_how: true),
          }
        end

        def vertical_type(show_how: false, **args)
          method_name = '纵向条形图'
          explain = '主要用于横轴内容较多时使用'
          rely_assets = ["amcharts_4"]

          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'base_datas', name: '基础数据', explain: '', example: :public_file, value_type: :array)
          self_described = described.get_described
          return self_described if show_how

          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args
          
          [true, {generated_data: generated_data}]
        end

      end
    end
  end
end