module Dashboard
  module BusinessLogic
    module Chart
      
      module Other
        extend self
      
        # 所有可调用接口集合
        def exposed_api
          {
            doughnut: doughnut(show_how: true),
            tag_cloud: tag_cloud(show_how: true),
            pyramid: pyramid(show_how: true),
          }
        end

        def doughnut(show_how: false, **args)
          method_name = '甜甜圈'
          explain = '根据提供的Json数据返回对应的甜甜圈图表'
          rely_assets = ["chart_js"]
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'chart_type', name: '图表类型', explain: '默认甜甜圈(doughnut)形状,可选择饼图(pie)', example: nil, value_type: :string, default: 'doughnut', necessary: false)
          described.set_attribute(key: 'parts', name: '组成部分', explain: 'Json键值对中,键代表组成部分的名称,值为组成部分的数值.', example: :public_file, value_type: :key_value)
          described.set_attribute(key: 'part_colors', name: '组成部分颜色', explain: '为每部分赋予不同的颜色区分,和上面的键值对(parts)顺序保持一致.', example: ["#007bff","#6610f2","#6f42c1","#e83e8c","#dc3545","#fd7e14","#ffc107","#28a745","#20c997","#17a2b8"].to_json, value_type: :array)
          described.set_attribute(key: 'title', name: '表格标题', explain: '', example: :public_file, value_type: :string, necessary: false)
          described.set_attribute(key: 'auto_sort', name: '自动排序', explain: '自动根据parts中参数进行排序', example: nil, value_type: :boolean, default: true, necessary: false)
          self_described = described.get_described
          return self_described if show_how

          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args
          
          sort_datas = generated_data[:parts]
          sort_datas = sort_datas.sort{|x,y| y[1].to_f <=> x[1].to_f}.to_h if generated_data[:auto_sort]

          labels = sort_datas.keys
          datas = sort_datas.values

          generated_data.merge!({labels: labels, datas: datas})
          
          [true, {generated_data: generated_data}]
        end

        def tag_cloud(show_how: false, **args)
          method_name = '标签云'
          explain = '根据提供的Json数据转换成词云'
          rely_assets = ["amcharts_4"]
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'tag_datas', name: '标签数据', explain: 'Json键值对中,键代表名称,key_word值为链接变量,weight为所占权重(权重越大,显示越大, 中英文标签混合情况,中文标签最小权重大于英文标签最大权重,这样不会出现加载卡顿情况.).', example: :public_file, value_type: :key_value)
          described.set_attribute(key: 'tag_url', name: '标签链接', explain: '点击标签后的跳转链接, 链接参数(tag_datas中key_word的值)用{key_word}代替', example: 'http://fund.eastmoney.com/{key_word}.html', value_type: :string)
          described.set_attribute(key: 'url_target', name: '链接跳转方式', explain: '参考<a>标签的target属性', example: nil, value_type: :string, default: '_blank', necessary: false)
          described.set_attribute(key: 'chart_style', name: '图表样式', explain: '为图表添加style属性', example: nil, value_type: :string, necessary: false)

          self_described = described.get_described
          return self_described if show_how

          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args

          word_datas = []
          generated_data[:tag_datas].deep_symbolize_keys.each do |key, value|
            word_datas << {"tag": key, "key_word": value[:key_word], "weight": value[:weight]}
          end 

          generated_data.merge!({word_datas: word_datas})
          
          [true, {generated_data: generated_data}]
        end

        def pyramid(show_how: false, **args)
          method_name = '金字塔'
          explain = '将数据占比转换为金字塔表示'
          rely_assets = ["amcharts_4"]
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'base_datas', name: '基础数据', explain: '键值对中键表示名称,值表示占比.', example: :public_file, value_type: :key_value)
          
          self_described = described.get_described
          return self_described if show_how

          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args

          tmp = []
          generated_data[:base_datas].each do |key, value|
            tmp << { name: key, value: value }
          end 

          generated_data[:base_datas] = tmp
          
          [true, {generated_data: generated_data}]
        end

      end
    end
  end
end