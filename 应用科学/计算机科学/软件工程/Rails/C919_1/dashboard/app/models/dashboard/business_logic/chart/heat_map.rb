module Dashboard
  module BusinessLogic
    module Chart
      
      module HeatMap
        extend self
      
        # 所有可调用接口集合
        def exposed_api
          {
            canvas: canvas(show_how: true),
          }
        end

        def canvas(show_how: false, **args)
          method_name = '热力范围'
          explain = '使用颜色表示表中的数据值,主要用于绘制大型和复杂数据.该图表像表格一样可视化,具有有限数量的行和列.'
          rely_assets = ["highcharts"]
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'common_datas', name: '行数据', explain: '', regular: nil, default: nil, example: :public_file, value_type: :array)

          self_described = described.get_described

          return self_described if show_how          
          
          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args
          
          [true, {generated_data: generated_data}]
        end

      end
    end
  end
end