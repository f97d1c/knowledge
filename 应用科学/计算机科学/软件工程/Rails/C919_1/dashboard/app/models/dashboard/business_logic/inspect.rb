module Dashboard
  module BusinessLogic
    # 对标准格式内容进行检查
    module Inspect
      extend self

        # 全自检项检查
        def inspect_all(self_described:, reality:)
          ideal_reality = {ideal: self_described[:attributes], reality: reality}

          res = default_value(ideal_reality)
          return res unless res[0]

          res = value_type(ideal_reality)
          return res unless res[0]
          
          res = attribute_necessary(ideal_reality)
          return res unless res[0]

          res = rely_assets(ideal_reality)
          return res unless res[0]

          [true, '']
        end 

        # 参数项检查
        def attribute_necessary(ideal:, reality:)
          # 检查必须参数是否缺失
          necessary_standards = ideal.select{|standard| standard[:necessary] == true}
          necessary_standards.each do |standard|
            key = standard[:key]
            unless (reality[key.to_sym] || reality[key]).present?
              return [false, "#{standard[:name]}(#{key})为必须项,不能为空"]
            end 
          end 
          
          # 检查参数值是否符合正则式要求
          reality.each do |key, value|
            standard = ideal.select{|standard| [key, key.to_sym, key.to_s].uniq.include?(standard[:key])}[0]
            # return [false, "未找到关于参数:#{key} 的定义"] unless standard
            next unless standard
            
            if standard[:regular] && (value.to_s =~ standard[:regular]).nil?
              tmp = "参数:#{standard[:name]}(#{key})的参数值(#{value})不满足要求"
              tmp+= ", 请参考: #{standard[:explain]}" if standard[:explain].present?
              tmp+= ", 示例值: #{standard[:example]}" if standard[:example].present?
              tmp+= ", 默认值: #{standard[:default]}" if standard[:default].present?
              tmp+='.'
              return [false, tmp] 
            end 
          end 

          [true, '']
        end 

        # 参数默认值检查
        def default_value(ideal:, reality:)
          ideal.select{|standard| !standard[:default].nil?}.each do |standard|
            unless reality[standard[:key].to_sym].present?
              reality.merge!({standard[:key].to_sym => standard[:default]})
            end
          end 

          [true, '']
        end

        # 参数值格式检查
        def value_type(ideal:, reality:)
          ideal.each do |standard|
            unless standard[:value_type].present?
              return [false, "自描述参数标准检查异常: #{standard[:name]}(#{standard[:key]})未设置对应参数值类型(value_type)."]
            end 
          end

          ideal.each do |standard|
            old_value = reality[standard[:key].to_sym]

            case standard[:value_type].to_sym
            when :array
              error_msg = [false, "参数:#{standard[:key]},参数类型不符合Json数组要求."]
              next if old_value.is_a?(Array)

              new_value = (JSON.parse(old_value) rescue (return error_msg))
              return error_msg unless new_value.is_a?(Array)
            when :key_value
              error_msg = [false, "参数:#{standard[:key]},参数类型不符合Json键值对要求."]
              next if old_value.is_a?(Hash)

              new_value = (JSON.parse(old_value) rescue (return error_msg))
              return error_msg unless new_value.is_a?(Hash)
            when :boolean
              new_value = [true, 'true', '1', 1].include?(old_value)
            when :integer
              next if old_value.is_a?(Integer)
              if old_value == '0'
                new_value = 0
              else
                return [false, "参数:#{standard[:key]},参数类型不符合数字要求."] if ((old_value.to_i rescue 0) == 0)
                new_value = old_value.to_i
              end 
            else
              next
            end 

            unless (old_value == new_value)
              reality.merge!({standard[:key].to_sym => new_value})
            end 
          end

          [true, '']
        end 

        # 依赖资源检查
        def rely_assets(ideal:, reality:)
          standard = ideal.select{|standard| [:rely_assets, 'rely_assets'].include?(standard[:key])}[0]
          return [false, "接口未定义依赖资源项"] unless standard
          return [true, ''] unless reality[:rely_assets].present?
          
          surplus_items = reality[:rely_assets] - standard[:example]
          return [false, "存在请求接口中未定义资源项: #{surplus_items.join(', ')}"] if surplus_items.present?

          [true, '']
        end 

    end
  end
end