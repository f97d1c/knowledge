module Dashboard
  module Print
    # 所有对外提供服务功能应该都由该module负责
    module Public
      extend self

      # Dashboard::Print::Public::exposed_api
      def exposed_api
        tmp = {}
        BusinessLogic::Public::exposed_api.each do |key, value|
          extra_params = [
            {key: 'api_code', name: '接口标识', explain: '访问该接口的对应唯一标识', regular: /^[^\s]+$/, default: nil, example: key, necessary: true, value_type: 'string'},
          ]

          value[:attributes] = (value[:attributes] + extra_params)
          tmp.merge!({key => value})
        end
        tmp
      end

      def output(api_code:, **args)
        current_api = exposed_api[api_code.to_sym]
        unless current_api
          return [false, '根据api_code未找到相关授权接口, 允许调用接口及相关介绍参考explain_api内容.', {explain_api: Dashboard::BusinessLogic::Public::explain_api}]
        end 

        res = current_api[:method_path].call(args.deep_symbolize_keys)
        return res unless res[0]

        html_content = ApplicationController.new.render_to_string(:partial => current_api[:template], :locals => res[1][:generated_data])
        [true, html_content]
      end 

    end
  end
end