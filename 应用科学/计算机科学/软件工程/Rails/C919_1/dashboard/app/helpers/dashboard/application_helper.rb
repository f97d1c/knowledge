module Dashboard
  module ApplicationHelper
    
    def markdown(content:, parse_options: [:UNSAFE, :HARDBREAKS], render_options: [:table, :autolink])
      CommonMarker.render_html(content, parse_options, render_options)
    end

    def load_rely_assets(assets:, method: '')
      (assets = assets.split(',')) if assets.is_a?(String)
      (assets = [assets]) if assets.is_a?(Symbol)
      assets.map do |asset|
        exposed_asset = Dashboard::Other::RelyAsset::exposed_asset
        target_object = exposed_asset[asset.to_sym]
        next unless target_object
        
        case method.to_sym
        when :link_label, :original_content
          send("write_#{method}", {stylesheet: target_object[:stylesheet], javascript: target_object[:javascript]})
        end 
      end.join("\n \n").html_safe 
    end 

    def write_link_label(stylesheet: [], javascript: [])
      tmp = []
      (stylesheet || []).each do |file_path|
        tmp << (stylesheet_link_tag file_path)
      end
      
      (javascript || []).each do |file_path|
        tmp << (javascript_include_tag file_path)
      end
      tmp.compact.join("\n \n").html_safe
    end 

    # 将页面依赖的样式/js文件写入到页面中,有利于页面单独保存及打印
    def write_original_content(stylesheet: [], javascript: [])
      tmp = []
      (stylesheet || []).each do |stylesheet|
        tmp << (stylesheet_write_tag stylesheet)
      end

      (javascript || []).each do |javascript|
        tmp << (javascript_write_tag javascript)
      end
      tmp.compact.join("\n \n").html_safe
    end 

    def javascript_write_tag(*files_path)
      tmp = []
      files_path.each do |file_path|
        at_var_name = file_path.gsub(/\/|\./, '_')
        next if self.instance_variable_get("@#{at_var_name}")
        self.instance_variable_set("@#{at_var_name}", true)

        file_full_path = Rails.root.join("../../app/assets/javascripts/#{file_path}")
        tmp << (render file: file_full_path)
      end 

      return unless tmp.present?

      %Q|
        <script type="text/javascript">
          #{tmp.join("\n\n")}
        </script>
      |.html_safe
    end 

    def stylesheet_write_tag(*files_path)
      tmp = []
      files_path.each do |file_path|
        at_var_name = file_path.gsub(/\/|\./, '_')
        next if self.instance_variable_get("@#{at_var_name}")
        self.instance_variable_set("@#{at_var_name}", true)

        file_full_path = Rails.root.join("../../app/assets/stylesheets/#{file_path}")
        # file = File.open(file_full_path)
        # tmp << file.read
        # file.close
        tmp << (render file: file_full_path)
      end 

      return unless tmp.present?

      %Q|
        <style type="text/css">
          #{tmp.join("\n\n")}
        </style>
      |.html_safe
    end 

    
  end
end
