> C919系列成员, Dashboard致力于为前台页面展示相关内容提供统解决方法, <br>
提供包括resful在内多种页面代码生成方式,实现单一维护,全局应用.<br>
目前分为图表展示,列表展示, 段落排版以及其他等四大方面.

# 说明

## 参数制定基本原则

0. 尽量保持单个参数项的数据结构简介
0. 参数结构应按照接口特点进行制定,而不是同一个接口类型的不同js插件需要传递不同规格的参数.

## BusinessLogic

> 基础业务逻辑,该命名空间下均为通用逻辑实现,<br>
为restful,mount engine等具体继承方式提供最原始,最基本的数据校验及数据加工,<br>
各具体集成方式应根据业务逻辑返回数据进行个性化处理加工.

# TODO

0. 自描述params中example改为配置读取文件
0. 自描述部分参数示例值过大
0. 依赖资源也应具有自描述特性
0. 依赖资源应具有根据不同方法加载特定文件功能
0. app/assets/config/dashboard_manifest.js应该自动写入

# 其他

## 颜色

### 抓取ColorTell网站颜色

```ruby
info = {
  '赤' => ['https://www.colortell.com/colorinfos?colorid=21559&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=21586&colorbook=q'],
  '橙' => ['https://www.colortell.com/colorinfos?colorid=21669&colorbook=q'],
  '黄' => ['https://www.colortell.com/colorinfos?colorid=21510&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=21517&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=21565&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=21594&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=22077&colorbook=q'],
  '绿' => ['https://www.colortell.com/colorinfos?colorid=21521&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=21542&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=21569&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=21598&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=21890&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=21921&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=22106&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=22127&colorbook=q'],
  '青' =>[],
  '蓝' => ['https://www.colortell.com/colorinfos?colorid=21526&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=21609&colorbook=q'],
  '紫' => ['https://www.colortell.com/colorinfos?colorid=21580&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=21964&colorbook=q'],
  '粉' => ['https://www.colortell.com/colorinfos?colorid=21512&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=21535&colorbook=q', 'https://www.colortell.com/colorinfos?colorid=21770&colorbook=q'],
}

datas = []
total = info.values.flatten.size
count = 0
info.each do |form, array|
  array.each do |info_url|
    print "#{Time.now} 当前进度: #{count+=1}/#{total} \n"
    result = RestClient.get info_url rescue(puts info_url)
    doc = Nokogiri::HTML.parse(result)
    contents = doc.search("tr").map{|content| content.children.text.gsub('  ', '').gsub(/^\n|\n$/, '')}

    datas << {
      name: contents[0].gsub("色号\n", ''),
      hex: contents[1].gsub("HEX\n", ''),
      rgb: contents[2].gsub("RGB\n", '').gsub("\n", ', '),
      lab: contents[3].gsub("L*a*b*\n", '').gsub("\n", ', '),
      lib: contents[5].gsub("色库\n", ''),
      material: info_url,
      form:form,
    }
  end
  # 网站有频率限制
  sleep 10 
end 

puts datas.map(&:to_json)
```

### 生成Markdown

```ruby


file_path = Rails.root.join("tmp/colors_#{Time.now.to_i}.md")
file = File::open(file_path, mode='w')

file.write %Q|
<style>
  p {
    /* color: #25282A */
  }
  .tl{
    text-align: left;
  }
  .tr{
    text-align: right;
  }
  .color_space{
    width: 100%;
    height: 80px;
    border-bottom: 0.01rem solid #ffffff;
  }
</style>
|

file.write("\n# #{datas.first[:lib]}\n")

datas.group_by{|hash| hash[:form]}.each do |form, colors|
  file.write("\n## #{form}\n")
  colors.each do |color|
    file.write %Q|
<div style="background:#{color[:hex]}">
  <div class='color_space'></div>
  <table>
    <tr>
      <td>色号</td>
      <td>#{color[:name]}</td>
      <td>HEX</td>
      <td>#{color[:hex]}</td>
    </tr>
    <tr>
      <td>RGB</td>
      <td>#{color[:rgb]}</td>
      <td>L*a*b*</td>
      <td>65.43	35.02	4.15</td>
    </tr>
    <tr>
      <td>色库</td>
      <td>#{color[:lib]}</td>
      <td>参考资料</td>
      <td><a href="#{color[:material]}">ColorTell</a></td>
    </tr>
  </table>
</div>
    |
  end
end

file.write("\n")
file.write("# 参考资料\n\n")
file.write("> [ColorTell 颜色工具 | 色册与色库查询工具](https://www.colortell.com/colorbook/)")
file.close
```