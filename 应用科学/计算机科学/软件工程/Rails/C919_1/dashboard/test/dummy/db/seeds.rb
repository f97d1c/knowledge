def read_file(path:, object:)

  File::open(path, 'r') do |file|
    print "\n开始读取文件: #{path}\n\n".yellow.underline

    hash_datas = JSON.parse(file.read)
    hash_datas.each_with_index do |hash_data, index|
      msg = ''
      msg += "开始处理数据: "
      msg += "#{hash_data.to_s[0..135]+'...'}".ljust(140, ' ')

      column_names = object.column_names
      hash_data.delete_if{|k, v| !column_names.include?(k) }
      record = object.find_or_initialize_by(hash_data)
      # hash_data.each{|column, value|record.send("#{column}=",value)}
      
      if record.changed?
        flag = record.new_record?
        record.save!
        msg += flag ? ('数据已保存') : ('数据存在修改'.red)
      else 
        msg += '数据未改变'
      end 
      
      print msg.colorize(background: (index.even? ? :blue : :black))
      print "\n"
    end 
  end 
end 

def each_dir(dir_path:, object:)
  Dir.foreach(dir_path) do |name|
    next if ['.', '..'].include?(name)

    path = "#{dir_path}/#{name}"
    name_ = name.gsub('.json', '')
    class_name = name_.singularize.classify
    object_ = (object.const_get(class_name) rescue object)

    if File::directory?(path)
      each_dir(dir_path: path, object: object_)
    elsif File::file?(path)
      read_file(path: path, object: object_)
    end 

  end 
end 

def init_model_records(seeds_path: Rails.root.join('db/seeds/dashboard'))
  print "开始执行种子数据\n"
  return (print '提供的种子路径非文件夹'.red) unless File::directory?(seeds_path)
  
  Dir.foreach(seeds_path) do |name|
    next if ['.', '..'].include?(name)

    path = "#{seeds_path}/#{name}"
    name_ = name.gsub('.json', '')
    class_name = name_.singularize.classify
    object = Dashboard.const_get(class_name) rescue ( print "#{class_name}执行失败:#{$!}\n"; next)

    if File::directory?(path)
      each_dir(dir_path: path, object: object)
    elsif File::file?(path)
      read_file(path: path, object: object)
    end 

  end

  print "种子数据执行完毕\n"

end 

init_model_records