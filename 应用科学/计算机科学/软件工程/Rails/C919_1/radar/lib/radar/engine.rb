module Radar
  class Engine < ::Rails::Engine
    isolate_namespace Radar
    require 'wireway'
    require 'ransack'
  end
end
