# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_12_04_065639) do

  create_table "contact_records", force: :cascade do |t|
    t.string "name", null: false
    t.string "content", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contact_relations", force: :cascade do |t|
    t.integer "contactabel_id"
    t.string "contactabel_type"
    t.integer "contact_record_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_record_id"], name: "index_contact_relations_on_contact_record_id"
    t.index ["contactabel_id"], name: "index_contact_relations_on_contactabel_id"
    t.index ["contactabel_type"], name: "index_contact_relations_on_contactabel_type"
  end

  create_table "radar_table_conditions", force: :cascade do |t|
    t.string "table_name", null: false
    t.string "name", null: false
    t.string "show_columns"
    t.string "search", null: false
    t.string "ransack", null: false
    t.integer "is_default", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["is_default"], name: "index_radar_table_conditions_on_is_default"
    t.index ["name"], name: "index_radar_table_conditions_on_name"
    t.index ["ransack"], name: "index_radar_table_conditions_on_ransack"
    t.index ["search"], name: "index_radar_table_conditions_on_search"
    t.index ["show_columns"], name: "index_radar_table_conditions_on_show_columns"
    t.index ["table_name"], name: "index_radar_table_conditions_on_table_name"
  end

  create_table "users", force: :cascade do |t|
    t.string "name", null: false
    t.string "gender", null: false
    t.date "birthday", null: false
    t.integer "age", null: false
    t.string "address", null: false
    t.string "id_number", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
