module Radar
  module BusinessLogic
    class Unite

      attr_reader :klass
      attr_reader :object
      attr_reader :zh_cn
      attr_reader :unite_id
      
      def initialize(object:)
        @klass = object.class.to_s
        @object = object
        @unite_id = "#{@klass}_#{object.id}"
        @zh_cn = nil
        load_attributes_zh_cn
        
        object.attributes.each do |column, value|
          define_singleton_method column.to_sym do
            value
          end
        end 
      end

      def new_record?
        !(id.present? rescue false)
      end 

      def klass_columns
        Unite.columns_info(klass: @klass)
      end 

      def self.columns_info(klass:)
        klass = klass.constantize if klass.is_a?(String)

        tmp = {}
        klass.columns.each do |column|
          human_read_name = klass.human_attribute_name(column.name)
          tmp.merge!({column.name => {locale_key: human_read_name, type: column.type, limit: column.limit, null: column.null, default: column.default}})
        end

        JSON.parse(tmp.to_json, object_class: OpenStruct)
      end

      private
        def load_attributes_zh_cn
          @zh_cn = Translation::human_attributes(object: @object)
        end 

        def load_attribute_infos
          @attributes_info = Translation::attributes_info(object: @object)
        end 
    end
  end
end
