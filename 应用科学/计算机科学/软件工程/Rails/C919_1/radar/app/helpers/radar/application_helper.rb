module Radar
  module ApplicationHelper

    def auto_load?
      auto_load = params[:auto_load]
      return false unless auto_load
      auto_load.downcase == 'true'
    end
    
    def auto_load(**args)
      render "shared/radar/auto_load", args
    end 

    def init_load?
      init_load = params[:init_load]
      return true unless init_load
      init_load.downcase == 'true'
    end

    def ransack_condictions
      {
        cont: '包含',
        eq: '等于',
        eq_any: '等于任意值',
        eq_all: '等于所有值',
        not_eq: '不等于',
        not_eq_any: '不等于任意值',
        not_eq_all: '不等于所有值',
        matches: '符合',
        matches_any: '符合任意条件',
        matches_all: '符合所有条件',
        does_not_match: '不符合',
        does_not_match_any: '符合任意条件',
        does_not_match_all: '不符合所有条件',
        lt: '小于',
        lt_any: '小于任意一个值',
        lt_all: '小于所有值',
        lteq: '小于等于',
        lteq_any: '小于等于任意一个值',
        lteq_all: '小于等于所有值',
        gt: '大于',
        gt_any: '大于任意一个值',
        gt_all: '大于所有值',
        gteq: '大于等于',
        gteq_any: '大于等于任意一个值',
        gteq_all: '大于等于所有值',
        in: '被包含',
        in_any: '被任意值包含',
        in_all: '被所有值包含',
        not_in: '不被包含',
        not_in_any: '不被任意值包含',
        not_in_all: '不被所有值包含',
        cont_any: '包含任意一个值',
        cont_all: '包含所有值',
        not_cont: '不包含',
        not_cont_any: '不包含任意一个值',
        not_cont_all: '不包含所有值',
        start: '以该值开始',
        start_any: '以任意一个值开始',
        start_all: '以所有值开始',
        not_start: '不以该值开始',
        not_start_any: '不以任意一个值开始',
        not_start_all: '不以所有值开始',
        end: '以该值结尾',
        end_any: '以任意一个值结尾',
        end_all: '以所有值结尾',
        not_end: '不以该值结尾',
        not_end_any: '不以任意一个值结尾',
        not_end_all: '不以所有值结尾',
        true: '等于true',
        false: '等于false',
        present: '有值',
        blank: '为空',
        null: '是null值',
        not_null: '不是null'
      }
    end
  end
end
