Radar::Engine.routes.draw do
  resources :manages, only:[:index] do 
    collection do
      get :search_form
      get :data_list
      get :object
      get :object_info
      get :object_edit
      get :object_delete
      post :object_update
      post :save_condition
      get :download
    end
  end
end
