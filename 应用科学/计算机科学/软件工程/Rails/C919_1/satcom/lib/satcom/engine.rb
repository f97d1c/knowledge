module Satcom
  class Engine < ::Rails::Engine
    isolate_namespace Satcom
    require 'rest-client'
    require 'wireway'
  end
end
