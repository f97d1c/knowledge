# Satcom
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'satcom'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install satcom
```

## Contributing
Contribution directions go here.

# 说明

## 构造结构

### BusinessLogic

> 由基础业务逻辑构成,文件夹按照大类和细分类及具体服务接口构成三级分类,例如:<br>

BusinessLogic::Finance::Fund::DailyMarket(app/models/satcom/business_logic/finance/fund/daily_market.rb),<br>
翻译过来即为基础业务逻辑中,金融大类,基金细分类中有关每日基金行情的数据接口.

#### master&slave

是的, 每个文件仅提供一个完整的数据接口,在每个文件中可以分为两类方法,<br>
一个主(master)方法和多个从(slave)方法,master中并不实现具体业务逻辑,而是定义该接口的数据结构标准,请求参数标准,<br>
以及调度作用(根据默认从方法返回结果决定是否满足接口要求,是否需要调用其他具体实现).<br>

也只有master方法是注册在Public类中可以被外部发现并调用.

##### 数据结构标准的制定

主(master)方法中定义返回数据的key以及对应的处理变量名称(lambda类型).<br>
各从(slave)方法针对标准一一进行实现,具体执行前将有统一校验去检查从方法是否提供了对应的变量.

## 中央集权

执行流程如下:

```
公共方法 -相关参数信息-> 主方法 -> 调用从方法
                       返回处理结果<-|                     
```

## 执照

根据以下条款,该Gem可作为开源软件使用 [MIT License](https://opensource.org/licenses/MIT).
