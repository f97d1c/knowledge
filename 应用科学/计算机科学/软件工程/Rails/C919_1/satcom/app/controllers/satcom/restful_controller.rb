module Satcom
  class RestfulController < ApplicationController
    after_action :about_cors, only: [:request_api]
    
    def example
      @exposed_api = Satcom::Execute::Public::exposed_api
    end

    def exposed_api
      exposed_api = {}
      Satcom::Execute::Public::exposed_api.each do |key, info|
        exposed_api.merge!({key => {name: info[:method_name], explain: info[:explain]}})
      end

      render json:{
        satcom: {
          name: '数据接口',
          explain: '提供数据支持,针对不同接口服务提供标准格式数据结构.',
          exposed_api: exposed_api
        }
      }
    end

    def request_api_params
      api_code = params[:api_code]
      return (render json: {success: false, result: '接口标识(api_code)不能为空'}) unless api_code
      exposed_api = Satcom::Execute::Public::exposed_api
      current_api = exposed_api[api_code.to_sym]
      render json: {success: true, request_params: current_api[:attributes], explain: current_api[:explain]}
    end 

    def request_api
      # TODO 初步了解为rails5的安全机制 去除存在风险 暂时先这样
      args = params.permit!.to_hash.reject!{|key| ["action", "controller"].include?(key)}

      res = Satcom::Execute::Entrance::run(args.deep_symbolize_keys)
      tmp = {success: res[0], result_info: {type: (res[1].is_a?(Array) ? 'array' : 'key_value')}}
      tmp[:result_info].merge!(result_count: res[1].size) if res[1].is_a?(Array)
      tmp.merge!({result: res[1]})

      if res[0]
        exposed_api = Satcom::Execute::Public::exposed_api
        current_api = exposed_api[params[:api_code].to_sym]
        tmp.merge!({data_structure: current_api[:data_structure]})
        tmp.merge!({thanks: res[2][:handle_by]}) if ((res[2] || {})[:handle_by] rescue false)
      end

      render json: tmp
    end 

    def test

    end 

    private
      def about_cors
        response.set_header('Access-Control-Allow-Origin', '*')
        response.set_header('Access-Control-Allow-Methods', 'POST, PUT, DELETE, GET, OPTIONS')
        response.set_header('Access-Control-Request-Method', '*')
        response.set_header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
      end

  end
end