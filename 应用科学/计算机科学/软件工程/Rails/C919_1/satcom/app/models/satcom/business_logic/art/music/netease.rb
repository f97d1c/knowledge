module Satcom
  module BusinessLogic
    module Art
      module Music
        module Netease
          extend self

          def blueprint
            method_name = '网易云音乐详情'
            explain = '根据提供的音乐id或链接提供音乐文件链接等详细信息.'
            realizations = [:realization_liuzhijin]

            # 规定标准返回数据格式
            data_structure = {
              title: '名称',
              artist: '艺术家',
              lrc: '歌词',
              plantform_code: '平台标识',
              plantform_id: '音乐标识',
              plantform_info_url: '详情链接',
              plantform_file_url: '文件地址',
              plantform_cover_urls: '封面链接',
            }

            described = BlueprintDescribed.new(bind: binding)
            described.set_attribute(key: 'music_ids', name: '音乐id', explain: '网易云音乐id', example: '63612', value_type: :string_or_array, necessary: false)
            described.set_attribute(key: 'music_urls', name: '音乐链接', explain: '网易云音乐链接', example: 'https://music.163.com/song?id=63612', value_type: :string_or_array, necessary: false)

            described.get_described
          end 

          private
            def realization_liuzhijin(show_how: false, **args)
              supplier_name = '刘志进实验室'
              source_url = 'https://music.liuzhijin.cn'
              source_data_handel_method = 'restful请求'
              
              # 根据主方法定义数据格式加工数据
              data_structure = {
                title: lambda{|hash| hash[:title]},
                artist: lambda{|hash| hash[:author]},
                lrc: lambda{|hash| hash[:lrc]},
                plantform_code: lambda{|hash| 'Netease'},
                plantform_id: lambda{|hash| hash[:songid]},
                plantform_info_url: lambda{|hash| hash[:link]},
                plantform_file_url: lambda{|hash| hash[:url]},
                plantform_cover_urls: lambda{|hash| [hash[:pic]]},
              }

              described = RealizationDescribed.new(bind: binding)
              self_described = described.get_described
              return self_described if show_how

              res = handle_music_id(music_ids: args[:music_ids], music_urls: args[:music_urls])
              return res unless res[0]

              headers = {
                'authority': 'music.liuzhijin.cn',
                'method': 'POST',
                'path': '/',
                'scheme': 'https',
                'accept': 'application/json, text/javascript, */*; q=0.01',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'zh-CN,zh;q=0.9,en;q=0.8',
                'content-length': '44',
                'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'cookie': 'Hm_lvt_50027a9c88cdde04a70f5272a88a10fa=1607508642,1607913045,1607954820,1608169920; Hm_lpvt_50027a9c88cdde04a70f5272a88a10fa=1608200006',
                'origin': 'https://music.liuzhijin.cn',
                'sec-fetch-dest': 'empty',
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'same-origin',
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
                'x-requested-with': 'XMLHttpRequest',
              }
              url = 'https://music.liuzhijin.cn'
              results = []

              res[1].each do |music_id|
                params = {input: music_id, filter: 'id', type: 'netease', page: '1'}
                res = Satcom::Signal::Http.post(url: url, headers: headers, params: params)
                return res unless res
                results << JSON.parse(res[1][:body])['data'][0].deep_symbolize_keys
              end

              [true, results]
            end

          def handle_music_id(music_ids: nil, music_urls: nil)
            music_ids = (music_ids.map(&:to_s) | music_urls.map{|url| url.gsub(/(.*\?id=)(\d{1,})(.*)/, '\2')})
            return [false, "music_ids和music_urls不能同时为空"] if music_ids.blank?
            [true, music_ids]
          end

        end 
      end
    end
  end
end