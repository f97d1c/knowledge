module Satcom
  module BusinessLogic  
    module Art
      module Color
        module ColorList
          extend self

          def blueprint
            method_name = '颜色列表'
            explain = '返回所有可用颜色'
            realizations = [:gather]

            data_structure = {
              name: '颜色名称',
              hex: '16进制编码',
              rgb: 'RGB表达方式',
              lab: 'Lab表达方式',
              lib: '所属色库',
              basic_color: '基础色',
            }
            
            described = BlueprintDescribed.new(bind: binding)
            described.get_described
          end 

          def gather(show_how: false, code: 'code', **args)
            supplier_name = '个人整理收集'
            source_url = "https://www.colortell.com/colortool"
            source_data_handel_method='Html解析'

            data_structure = {
              name: lambda{|hash| hash["name"]},
              hex: lambda{|hash| hash["hex"]},
              rgb: lambda{|hash| hash["rgb"]},
              lab: lambda{|hash| hash["lab"]},
              lib: lambda{|hash| hash["lib"]},
              basic_color: lambda{|hash| hash["basic_color"]},
            }

            described = RealizationDescribed.new(bind: binding)
            self_described = described.get_described
            return self_described if show_how
            
            file_path = "public/satcom/art/color/color_list.json"
            array = YAML.load(File.open(Rails.root.join(file_path)))

            [true, array]
          end 

        end
      end
    end
  end
end
