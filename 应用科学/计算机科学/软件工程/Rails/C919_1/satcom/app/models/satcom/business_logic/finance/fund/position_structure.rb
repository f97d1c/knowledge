module Satcom
  module BusinessLogic
    module Finance
      module Fund
        module PositionStructure
          extend self

          def blueprint
            method_name = '持仓结构'
            explain = '返回基金所持仓结构'
            realizations = [:realization_eastmoney]

            # 规定标准返回数据格式
            data_structure = {
              stock_code: '股票代码',
              stock_name: '股票名称',
              proportion_in_net_worth: '占净值比例',
              number_of_shares_held: '持股数',
              market_value_of_position: '持仓市值',
            }

            described = BlueprintDescribed.new(bind: binding)
            described.set_attribute(key: 'code', name: '基金代码', explain: '', example: '377240', value_type: :string)

            described.get_described
          end 

          private
            def realization_eastmoney(show_how: false, **args)
              supplier_name = '天天基金'
              source_url = 'http://fundf10.eastmoney.com/FundArchivesDatas.aspx'
              source_data_handel_method = 'Js变量解析'
              
              # 根据主方法定义数据格式加工数据
              data_structure = {
                stock_code: lambda{|hash| hash['股票代码']},
                stock_name: lambda{|hash| hash['股票名称']},
                proportion_in_net_worth: lambda{|hash| hash['占净值比例'].gsub('%', '').to_f/100},
                number_of_shares_held: lambda{|hash| hash['持股数（万股）'].to_f*10000},
                market_value_of_position: lambda{|hash| hash['持仓市值（万元）'].to_f*10000},
              }

              described = RealizationDescribed.new(bind: binding)
              self_described = described.get_described
              return self_described if show_how

              params = {
                type:'jjcc',
                code: args[:code],
                topline:'10',
                year:'2020',
                month:'6,3,9',
                rt:'0.3260991934481672',
              }
              
              res = Satcom::Signal::Http.get(url: source_url, params: params)
              html_content = res[1][:body].gsub(/var apidata={ content:\"(.*)(\",arryear.*$)/, '\1')
              html_object = Nokogiri::HTML.parse(html_content)

              datas = []
              html_object.css('table').each_with_index do |table, index|
                h4 = html_object.css('h4')[index]
                key = h4.css('label')[-1].text.gsub(/.*(\d{4}\-\d{2}\-\d{2})/, '\1')
                res = handle_table_conent(table_conent: table)
                # TODO: 数据外层嵌套Hash各组成元素符合获取问题
                # return res unless res[0]
                return res
                # datas << {key => res[1]}
                datas << res[1]
              end

              [true, datas]
            end

            def handle_table_conent(table_conent:)
              titles_ = table_conent.css('thead')[0].css('tr')[0].css('th').map{|children| children.text}
              items = ["股票代码", "股票名称", "占净值比例", "持股数（万股）", "持仓市值（万元）"]
              get_indexs = items.map{|item|titles_.index(item)}
              titles = get_indexs.map{|index| titles_.at(index)}
            
              tmp = []
              table_conent.css('tbody')[0].css('tr').each do |tr_content|
                
                array = tr_content.css('td').map{|children| children.text}
                contents = get_indexs.map{|index| array.at(index).gsub(',', '')}
            
                tmp_ = {}
                titles.each_with_index do |title, index|
                  tmp_.merge!(title => contents[index])
                end 
                tmp << tmp_
              end 
            
              [true, tmp]
            end 

        end 
      end
    end
  end
end