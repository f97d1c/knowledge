module Satcom
  module BusinessLogic
    # 自描述类(蓝图) 用于对接口进行定义和描述                                                 
    class BlueprintDescribed
      
      def initialize(bind: )
        @bind = bind
        @self_ = eval("self", @bind)
        @method = eval("__method__", @bind)
        @attributes = []
        @args = (eval("args", @bind) rescue {})
        @realizations = []
      end

      def columns
        {
          method_name: '用于定义方法的中文名称, 起到简单解释的作用',
          explain: '关于方法的进一步详细阐述',
          method_path: '方法调用路径',
          data_structure: '标准数据结构',
          attributes: '方法所用的属性,即所需参数',
          realizations: '先关具体实现方法',
        }
      end 

      def get_described
        # raise "获取自描述内容前先定义属性值(set_attribute)" unless @attributes.present?
        tmp = {}
        columns.keys.each do |column|
          method_name = "get_#{column}".to_sym
          res = if respond_to?(method_name)
            send(method_name)
          else
            get_variable.call(column)
          end

          return res unless res[0]
          tmp.merge!(res[1])
        end 
        tmp
      end 

      def get_variable
        lambda do |variable|
          variable_ = (eval(variable.to_s, @bind) rescue nil)
          return [false, "宿主方法内未定义:#{variable}变量(#{columns[variable.to_sym] || '暂无任何描述信息'})"] unless variable_
          [true, {variable => variable_}.symbolize_keys]
        end 
      end 

      def get_method_path
        method_path = (eval("method_path", @bind) rescue nil)
        return [true, {method_path: method_path}] if method_path
        method_path = lambda{|**args| @self_.send(@method, args)}
        [true, {method_path: method_path}]
      end 

      def get_attributes
        # 加载通用属性
        currency_attributes
        [true, {attributes: @attributes}]
      end 

      def get_realizations
        realizations = (eval("realizations", @bind) rescue [])

        realizations.each do |slave|
          @realizations << @self_.send(slave, @args.merge!({show_how: true}).deep_symbolize_keys)
        end 
        
       [true, {realizations: @realizations}]
      end 

      def set_attribute(key:, name:, explain:, regular: nil, default: '', example:, necessary: true, value_type:, max: nil)
        attribute = {
          key: key,
          name: name,
          explain: explain,
          regular: regular,
          default: default,
          example: example,
          necessary: necessary,
          value_type: value_type
        }
        @attributes << attribute
      end 

      private

        # 通用属性
        def currency_attributes
          @attributes += [
            {key: 'data_format', name: '数据格式', explain: '可根据标准数据结构选择部分数据项,默认按照标准定义格式返回', regular: nil, default: 'original', example: nil, necessary: false, value_type: 'string_or_array'},
            {key: 'key_type', name: '哈希键类型', explain: '根据不同传值返回不同类型键: zh: 中文键,其他任意值返回英文键,该选项仅在使用标准数据格式时可用,自定义数据格式将以传递参数为准.', regular: nil, default: 'zh', example: nil, necessary: false, value_type: 'string'},
          ]
        end

    end

  end 
end