module Satcom
  module BusinessLogic
    module Finance
      module Fund
        module WheelSignal28
          extend self

          def blueprint
            method_name = '二八轮动'
            explain = '策略'
            realizations = [:realization_eastmoney]

            data_structure = {
              signal_name: '信号名称',
              created_at: '创建时间',
              content: '内容',
            }
            
            described = BlueprintDescribed.new(bind: binding)
            described.set_attribute(key: 'count', name: '总条数', explain: '无论接口条数限制为多少,返回条数与请求条数保持一致(20的倍数,若请求21条则返回40条),原始接口包含部分无关内容,为保证目标数量,建议多请求20条.', example: nil, default: 20, value_type: :string, necessary: false)
            described.get_described
          end 

          def realization_eastmoney(show_how: false, **args)
            supplier_name = '雪球'

            source_url = 'https://xueqiu.com/query/v1/symbol/search/status'

            params = {
              u: '561598886632098',
              uuid: '1300451009692315648',
              count: 20,
              comment: '0',
              symbol: 'CSI010',
              hl: '0',
              source: 'all',
              sort:'',
              page: '1',
              q:'',
              type: '27',
              session_token: nil,
              access_token: '4db837b914fc72624d814986f5b37e2a3d9e9944',
            }
            
            source_data_handel_method='Json键值对读取'

            data_structure = {
              signal_name: lambda{|hash| hash[:signal_name]},
              created_at: lambda{|hash| hash[:created_at]},
              content: lambda{|hash| hash[:content]},
            }

            described = RealizationDescribed.new(bind: binding)
            self_described = described.get_described
            return self_described if show_how
            
            datas = []
            (args[:count].to_f/20).ceil.times do |index|
              params[:page] = (index+1)
              flag = true
              count = 0
              while flag do
                res = Satcom::Signal::Http.get(url: source_url, params: params)
                count += 1
                list_size = (JSON.parse(res[1][:body])['list'].size rescue 0)
                # 在count小于30的情况下 body为空 或 list size 不等于 20则继续循环
                flag = (count < 30) && ((res[1][:body].blank?) || (list_size != 20))
              end
              
              return res unless res[0]
              datas += (JSON.parse(res[1][:body])['list'])
            end 

            tmp = []
            datas.each do |info|
              next unless (info['text'] =~ /今日收盘轮动信号/)
              content = info['text'].gsub(/(.*)(<a.*>\$)(.*)(\$<\/a>)/, '\1\3').gsub(/(.*)(<a.*>\$)(.*)(\$<\/a>)/, '\1\3')
              content = content.gsub(/.*今日收盘轮动信号:(.*)/, '\1')
              content = content.gsub(/（本指数仅用.*$/, '')
              content = content.gsub(/^\s{1,}|/, '')
              content = content.gsub(/，\s{1,}/, '。')

              signal_name = info['user']['screen_name']
              created_at = info['created_at']
              tmp << {signal_name: signal_name, created_at: created_at, content: content}
            end

            [true, tmp]
          end 

        end 
      end
    end
  end
end