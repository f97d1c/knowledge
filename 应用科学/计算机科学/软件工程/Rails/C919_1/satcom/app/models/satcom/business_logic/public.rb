module Satcom
  module BusinessLogic
    module Public
      extend self

      # Satcom::BusinessLogic::Public::exposed_api
      def exposed_api
        exposed_api_flat_
      end 

      private
        def exposed_api_
          {
            finance: {
              fund: {
                daily_market: Finance::Fund::DailyMarket::blueprint,
                base_info: Finance::Fund::BaseInfo::blueprint,
                key_word_search: Finance::Fund::KeywordSearch::blueprint,
                companys_list: Finance::Fund::CompanysList::blueprint,
                market_overview_management_scale: Finance::Fund::MarketOverviewManagementScale::blueprint,
                market_overview_quantity: Finance::Fund::MarketOverviewQuantity::blueprint,
                classification: Finance::Fund::Classification::blueprint,
                wheel_signal_28: Finance::Fund::WheelSignal28::blueprint,
                position_structure: Finance::Fund::PositionStructure::blueprint,
                article_xueqiu: Finance::Fund::ArticleXueqiu::blueprint,
              }
            },
            art: {
              color: {
                color_list: Art::Color::ColorList::blueprint,
              },
              music: {
                netease: Art::Music::Netease::blueprint,
              }
            },
            other:{
              c919:{
                node_addresses: Other::C919::NodeAddresses::blueprint,
              },
              test_data:{
                people: Other::TestData::People::blueprint,
              }
            }
          }
        end

        # 将多层级Hash结构变为浅层
        def exposed_api_flat_
          tmp = {}
          exposed_api_.each do |key, value|
            value.each do |ke, val|
              val.each do |k,v|
                tmp.merge!({"#{key}_#{ke}_#{k}".to_sym => v})
              end
            end 
          end
          tmp
        end

    end 
  end
end