module Satcom
  module BusinessLogic
    module Finance
      module Fund
        module Classification
          extend self

          def blueprint
            method_name = '主题分类'
            explain = '返回在基金分类中根据不同主题进行分类的结果'
            realizations = [:realization_eastmoney]

            data_structure = {
              composite_index: '综合指数',
              industry: '行业',
              concept: '概念',
              region: '地区',
              topics_with_qdii: '含QDII的主题',
            }
            
            described = BlueprintDescribed.new(bind: binding)
            described.get_described
          end 

          def realization_eastmoney(show_how: false, **args)
            supplier_name = '天天基金'
            source_url = "http://fund.eastmoney.com/api/fundtopicinterface.ashx?dt=13"
            source_data_handel_method='Json键值对读取'

            data_structure = {
              composite_index: lambda{|hash| hash['zhzs']},
              industry: lambda{|hash| hash['hy']},
              concept: lambda{|hash| hash['gn']},
              region: lambda{|hash| hash['dq']},
              topics_with_qdii: lambda{|hash| hash['qdii']},
            }

            described = RealizationDescribed.new(bind: binding)
            self_described = described.get_described
            return self_described if show_how

            res = Satcom::Signal::Http.get(url: source_url)
            return res unless res[0]

            datas = JSON.parse(res[1][:body])['data']

            handle_data = lambda do |array|
              tmp = []
              array.each do |hash|
                tmp << hash["TTypeName"]
              end 
              tmp
            end 

            data = {}
            datas.each do |key, array|
              res = handle_data.call(array)
              data.merge!({key => res})
            end 
            
            [true, data]
          end 

        end 
      end
    end
  end
end