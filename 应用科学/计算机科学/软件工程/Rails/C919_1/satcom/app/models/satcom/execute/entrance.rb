module Satcom
  module Execute
    module Entrance
      extend self

      # Satcom::Execute::Entrance::run
      def run(**args)
        begin
          # 检查接口服务
          exposed_api = Public::exposed_api
          @blueprint = exposed_api[args[:api_code].to_sym]
          return [false, "根据api_code(#{args[:api_code]})未找到相关授权方法"] unless @blueprint
          @realizations = @blueprint[:realizations]
          return [false, "该服务(#{args[:api_code]})尚不存在任何具体实现内容"] unless @realizations.present?
          # 检查传参
          res = BusinessLogic::Inspect::inspect_all(self_described: @blueprint, reality: args)
          return res unless res[0]
          @args = args.deep_symbolize_keys

          # 运行具体实现
          res = exec_realizations
          return res unless res[0]

          # 根据传参加工结果
          res_ = handle_data_format(result: res[1])
          res[1] = res_[1]

          res
        rescue
          info = {errors: {message: $!.to_s, path: $@}}
          print info
          return [false, '请求异常', info]
        end
      end

      private
        def exec_realizations
          @realizations.each_with_index do |realization, index|
            @realization = realization
            res = realization[:method_path].call(@args.deep_symbolize_keys)
            if res[0]
              handle_by = {
                "原始数据来源": @realization[:supplier_name],
                "原始数据地址": @realization[:source_url],
                "原始数据处理方式": @realization[:source_data_handel_method],
                "优先级": (index+1),
              }
              hash = (res[2] || {})
              hash.merge!({handle_by: handle_by})
              res[2] = hash
              return res
            end
          end
          [false, '接口所有具体实现均无法正常处理请求']
        end 

        def handle_data_format(result:)
          data_format = lambda do |item|
            tmp = {}
            @args[:data_format].each do |key, value|
              data = @realization[:data_structure][value.to_sym].call(item)
              tmp.merge!({key => data})
            end
            tmp
          end 
          
          if result.is_a?(Hash)
            return [true, data_format.call(result)]
          end 

          array = []
          [result].flatten.each do |item|
            array << data_format.call(item)
          end 

          [true, array]
        end 

    end 
  end
end