module Satcom
  module Execute
    module Public
      extend self

      # Satcom::Execute::Public::exposed_api
      def exposed_api
        tmp = {}
        Satcom::BusinessLogic::Public::exposed_api.each do |key, value|
          extra_params = [
            {key: 'api_code', name: '接口标识', explain: '访问该接口的对应唯一标识', regular: nil, default: nil, example: key, necessary: true, value_type: 'string'},
          ]

          value[:attributes] = (value[:attributes] + extra_params)
          tmp.merge!({key => value})
        end
        tmp
      end 

    end 
  end
end