
<!-- TOC -->

- [ctiveRecord](#ctiverecord)
  - [[MODULE] Aggregations](#module-aggregations)
    - [ClassMethods](#classmethods)
  - [[MODULE] Associations](#module-associations)
  - [[MODULE] AttributeAssignment](#module-attributeassignment)
  - [[MODULE] AttributeMethods](#module-attributemethods)
  - [[MODULE] Attributes](#module-attributes)
  - [[MODULE] AutosaveAssociation](#module-autosaveassociation)
  - [[MODULE] Batches](#module-batches)
    - [find_each](#find_each)
    - [find_in_batches](#find_in_batches)
    - [in_batches](#in_batches)
    - [BatchEnumerator](#batchenumerator)
      - [each](#each)
      - [each_record](#each_record)
  - [[MODULE] Calculations](#module-calculations)
    - [average](#average)
    - [count](#count)
    - [sum](#sum)
    - [average](#average-1)
    - [minimum](#minimum)
    - [maximum](#maximum)
    - [ids](#ids)
    - [pick](#pick)
    - [pluck](#pluck)
  - [[MODULE] Callbacks](#module-callbacks)
    - [save](#save)
    - [valid](#valid)
    - [before_validation](#before_validation)
    - [validate](#validate)
    - [after_validation](#after_validation)
    - [before_save](#before_save)
    - [before_create](#before_create)
    - [create](#create)
    - [after_create](#after_create)
    - [after_save](#after_save)
    - [after_commit](#after_commit)
    - [after_rollback](#after_rollback)
    - [after_find](#after_find)
    - [after_initialize](#after_initialize)
    - [after_touch](#after_touch)
  - [[MODULE] ConnectionAdapters](#module-connectionadapters)
  - [[MODULE] ConnectionHandling](#module-connectionhandling)
    - [clear_query_caches_for_current_thread](#clear_query_caches_for_current_thread)
    - [connected?](#connected)
    - [connected_to](#connected_to)
    - [connected_to?](#connected_to)
    - [connection_config](#connection_config)
    - [connection_pool](#connection_pool)
    - [connection_specification_name](#connection_specification_name)
    - [connect_to](#connect_to)
    - [current_role](#current_role)
    - [establish_connection](#establish_connection)
    - [remove_connection](#remove_connection)
    - [resolve_connection](#resolve_connection)
  - [[MODULE] Core](#module-core)
    - [configurations](#configurations)
    - [configurations=](#configurations)
    - [connection_handler](#connection_handler)
    - [connection_handler=](#connection_handler)
    - [new](#new)
    - [实例公共方法](#实例公共方法)
      - [<=>](#)
      - [==](#)
      - [clone](#clone)
      - [connection_handler](#connection_handler-1)
      - [dup](#dup)
      - [encode_with(coder)](#encode_withcoder)
      - [eql?](#eql)
      - [freeze](#freeze)
      - [frozen?](#frozen)
      - [init_with(coder, &block)](#init_withcoder-block)
      - [inspect](#inspect)
      - [pretty_print](#pretty_print)
      - [readonly!](#readonly)
      - [readonly?](#readonly)
      - [slice(*methods)](#slicemethods)
  - [[MODULE] CounterCache](#module-countercache)
    - [ClassMethods](#classmethods-1)
      - [decrement_counter](#decrement_counter)
      - [increment_counter](#increment_counter)
      - [reset_counters](#reset_counters)
      - [update_counters](#update_counters)
  - [[MODULE] DefineCallbacks](#module-definecallbacks)
  - [[MODULE] DynamicMatchers](#module-dynamicmatchers)
  - [[MODULE] Enum](#module-enum)
  - [[MODULE] Explain](#module-explain)
  - [[MODULE] FinderMethods](#module-findermethods)
    - [exists?](#exists)
    - [lock](#lock)
    - [find_by](#find_by)
    - [find_or_initialize_by](#find_or_initialize_by)
    - [find_or_create_by](#find_or_create_by)
    - [select](#select)
    - [first](#first)
    - [last](#last)
    - [take](#take)
  - [[MODULE] Inheritance](#module-inheritance)
  - [[MODULE] Integration](#module-integration)
  - [[MODULE] LegacyYamlAdapter](#module-legacyyamladapter)
  - [[MODULE] Locking](#module-locking)
  - [[MODULE] Middleware](#module-middleware)
  - [[MODULE] ModelSchema](#module-modelschema)
  - [[MODULE] NestedAttributes](#module-nestedattributes)
    - [ClassMethods](#classmethods-2)
      - [accepts_nested_attributes_for](#accepts_nested_attributes_for)
        - [一对一](#一对一)
          - [创建](#创建)
          - [更新](#更新)
          - [删除](#删除)
        - [一对多](#一对多)
          - [创建](#创建-1)
          - [更新](#更新-1)
          - [删除](#删除-1)
        - [reject_if](#reject_if)
        - [验证父模型的存在](#验证父模型的存在)
  - [[MODULE] NoTouching](#module-notouching)
  - [[MODULE] Persistence](#module-persistence)
  - [[MODULE] QueryMethods](#module-querymethods)
  - [[MODULE] Querying](#module-querying)
  - [[MODULE] ReadonlyAttributes](#module-readonlyattributes)
  - [[MODULE] Reflection](#module-reflection)
  - [[MODULE] Sanitization](#module-sanitization)
  - [[MODULE] Scoping](#module-scoping)
  - [[MODULE] SecureToken](#module-securetoken)
  - [[MODULE] Serialization](#module-serialization)
  - [[MODULE] SpawnMethods](#module-spawnmethods)
  - [[MODULE] Store](#module-store)
  - [[MODULE] Suppressor](#module-suppressor)
  - [[MODULE] Tasks](#module-tasks)
  - [[MODULE] TestFixtures](#module-testfixtures)
  - [[MODULE] Timestamp](#module-timestamp)
  - [[MODULE] Transactions](#module-transactions)
  - [[MODULE] Translation](#module-translation)
  - [[MODUL] Type](#modul-type)
  - [[MODULE] VERSION](#module-version)
  - [[MODULE] Validations](#module-validations)
  - [[CLASS] ActiveRecordError](#class-activerecorderror)
  - [[CLASS] AdapterNotFound](#class-adapternotfound)
  - [[CLASS] AdapterNotSpecified](#class-adapternotspecified)
  - [[CLASS] AssociationRelation](#class-associationrelation)
  - [[CLASS] AssociationTypeMismatch](#class-associationtypemismatch)
  - [[CLASS] AttributeAssignmentError](#class-attributeassignmenterror)
  - [[CLASS] Base](#class-base)
  - [[CLASS] ConfigurationError](#class-configurationerror)
  - [[CLASS] ConnectionNotEstablished](#class-connectionnotestablished)
  - [[CLASS] ConnectionTimeoutError](#class-connectiontimeouterror)
  - [[CLASS] DangerousAttributeError](#class-dangerousattributeerror)
  - [[CLASS] DatabaseConfigurations](#class-databaseconfigurations)
  - [[CLASS] Deadlocked](#class-deadlocked)
  - [[CLASS] EagerLoadPolymorphicError](#class-eagerloadpolymorphicerror)
  - [[CLASS] EnvironmentMismatchError](#class-environmentmismatcherror)
  - [[CLASS] ExclusiveConnectionTimeoutError](#class-exclusiveconnectiontimeouterror)
  - [[CLASS] FixtureSet](#class-fixtureset)
  - [[CLASS] ImmutableRelation](#class-immutablerelation)
  - [[CLASS] InvalidForeignKey](#class-invalidforeignkey)
  - [[CLASS] IrreversibleMigration](#class-irreversiblemigration)
  - [[CLASS] IrreversibleOrderError](#class-irreversibleordererror)
  - [[CLASS] LockWaitTimeout](#class-lockwaittimeout)
  - [[CLASS] LogSubscriber](#class-logsubscriber)
  - [[CLASS] Migration](#class-migration)
  - [[CLASS] MismatchedForeignKey](#class-mismatchedforeignkey)
  - [[CLASS] MultiparameterAssignmentErrors](#class-multiparameterassignmenterrors)
  - [[CLASS] NoDatabaseError](#class-nodatabaseerror)
  - [[CLASS] NotNullViolation](#class-notnullviolation)
  - [[CLASS] PreparedStatementCacheExpired](#class-preparedstatementcacheexpired)
  - [[CLASS] PreparedStatementInvalid](#class-preparedstatementinvalid)
  - [[CLASS] QueryCache](#class-querycache)
  - [[CLASS] QueryCanceled](#class-querycanceled)
  - [[CLASS] RangeError](#class-rangeerror)
  - [[CLASS] ReadOnlyError](#class-readonlyerror)
  - [[CLASS] ReadOnlyRecord](#class-readonlyrecord)
  - [[CLASS] RecordInvalid](#class-recordinvalid)
  - [[CLASS] RecordNotDestroyed](#class-recordnotdestroyed)
  - [[CLASS] RecordNotFound](#class-recordnotfound)
  - [[CLASS] RecordNotSaved](#class-recordnotsaved)
  - [[CLASS] RecordNotUnique](#class-recordnotunique)
  - [[CLASS] Relation](#class-relation)
  - [[CLASS] Result](#class-result)
  - [[CLASS] Rollback](#class-rollback)
  - [[CLASS] Schema](#class-schema)
  - [[CLASS] SerializationFailure](#class-serializationfailure)
  - [[CLASS] SerializationTypeMismatch](#class-serializationtypemismatch)
  - [[CLASS] StaleObjectError](#class-staleobjecterror)
  - [[CLASS] StatementCache](#class-statementcache)
  - [[CLASS] StatementInvalid](#class-statementinvalid)
  - [[CLASS] StatementTimeout](#class-statementtimeout)
  - [[CLASS] SubclassNotFound](#class-subclassnotfound)
  - [[CLASS] TransactionIsolationError](#class-transactionisolationerror)
  - [[CLASS] TransactionRollbackError](#class-transactionrollbackerror)
  - [[CLASS] TypeConflictError](#class-typeconflicterror)
  - [[CLASS] UnknownAttributeError](#class-unknownattributeerror)
  - [[CLASS] UnknownAttributeReference](#class-unknownattributereference)
  - [[CLASS] UnknownPrimaryKey](#class-unknownprimarykey)
  - [[CLASS] ValueTooLong](#class-valuetoolong)
  - [[CLASS] WrappedDatabaseException](#class-wrappeddatabaseexception)
- [参考资料](#参考资料)

<!-- /TOC -->

# ctiveRecord

## [MODULE] Aggregations

> [RubyOnRails | Aggregations](https://api.rubyonrails.org/classes/ActiveRecord/Aggregations.html)

### ClassMethods



## [MODULE] Associations

> [RubyOnRails | Associations](https://api.rubyonrails.org/classes/ActiveRecord/Associations.html)


## [MODULE] AttributeAssignment

> [RubyOnRails | AttributeAssignment](https://api.rubyonrails.org/classes/ActiveRecord/AttributeAssignment.html)


## [MODULE] AttributeMethods

> [RubyOnRails | AttributeMethods](https://api.rubyonrails.org/classes/ActiveRecord/AttributeMethods.html)


## [MODULE] Attributes

> [RubyOnRails | Attributes](https://api.rubyonrails.org/classes/ActiveRecord/Attributes.html)


## [MODULE] AutosaveAssociation

> [RubyOnRails | AutosaveAssociation](https://api.rubyonrails.org/classes/ActiveRecord/AutosaveAssociation.html)

> 负责在保存关联记录时自动保存关联记录。除保存外，它还会破坏所有标记为要破坏的关联记录.

## [MODULE] Batches

> [RubyOnRails | Batches](https://api.rubyonrails.org/classes/ActiveRecord/Batches.html)

### find_each

### find_in_batches

> 将查找选项找到的每一批记录生成为一个数组.

### in_batches

> 产生ActiveRecord::Relation对象以使用一批记录

### BatchEnumerator

#### each

#### each_record

## [MODULE] Calculations

> [RubyOnRails | Calculations](https://api.rubyonrails.org/classes/ActiveRecord/Calculations.html)

### average

### count

### sum

### average

### minimum

### maximum

### ids

### pick

> 从当前关系的命名列中选择值.<br>
像一样pluck，pick只会加载实际值，而不是整个记录对象，因此效率更高.

### pluck 

> 无需装载了一系列的记录只是加载想要的属性.

## [MODULE] Callbacks

> [RubyOnRails | Callbacks](https://api.rubyonrails.org/classes/ActiveRecord/Callbacks.html)

### save

### valid

### before_validation

### validate

### after_validation

### before_save

### before_create

### create

### after_create

### after_save

### after_commit

### after_rollback

### after_find

### after_initialize

### after_touch

## [MODULE] ConnectionAdapters

> [RubyOnRails | ConnectionAdapters](https://api.rubyonrails.org/classes/ActiveRecord/ConnectionAdapters.html)


## [MODULE] ConnectionHandling

> [RubyOnRails | ConnectionHandling](https://api.rubyonrails.org/classes/ActiveRecord/ConnectionHandling.html)

### clear_query_caches_for_current_thread

> 清除与当前线程关联的所有连接的查询缓存.

### connected?

> 返回是否连接Active Record.

### connected_to

> 在块的持续时间内连接到数据库或角色

```ruby
ActiveRecord::Base.connected_to(role: :writing) do
  Dog.create! # creates dog using dog writing connection
end

ActiveRecord::Base.connected_to(role: :reading) do
  Dog.create! # throws exception because we're on a replica
end

ActiveRecord::Base.connected_to(role: :unknown_role) do
  # raises exception due to non-existent role
end
```

### connected_to? 

```ruby
ActiveRecord::Base.connected_to(database: :test, role: :writing) do
  ActiveRecord::Base.connected_to?(role: :writing) #=> true
  ActiveRecord::Base.connected_to?(role: :reading) #=> false
  ActiveRecord::Base.connected_to?(database: :animals_slow_replica) #=> false
end
```

### connection_config

> 以哈希形式返回关联连接的配置.<br>
仅用于查看.

```ruby
ActiveRecord::Base.connection_config
# => {pool: 5, timeout: 5000, database: "db/development.sqlite3", adapter: "sqlite3"}
```

### connection_pool



### connection_specification_name

> 从当前类或其父级返回规范名称.

### connect_to

> 将模型连接到指定的数据库.

```ruby
class AnimalsModel < ApplicationRecord
  self.abstract_class = true

  connects_to database: { writing: :primary, reading: :primary_replica }
end
```

### current_role 

> 返回代表当前连接角色的符号.


### establish_connection

> 建立与数据库的连接.<br>
接受哈希作为输入，其中:adapter必须使用常规数据库(MySQL，PostgreSQL等)的数据库适配器名称(小写).

```ruby
ActiveRecord::Base.establish_connection(
  adapter:  "mysql2",
  host:     "localhost",
  username: "myuser",
  password: "mypass",
  database: "somedatabase"
)
```

### remove_connection



### resolve_connection



## [MODULE] Core

> [RubyOnRails | Core](https://api.rubyonrails.org/classes/ActiveRecord/Core.html)


### configurations

> 返回完整的 ActiveRecord::DatabaseConfigurations对象.

### configurations=



### connection_handler



### connection_handler=



### new

> 可以将新对象实例化为空(不传递任何构造参数)，也可以将其实例化为具有属性的预设，但尚未保存(传递具有与关联的表列名称匹配的键名称的散列)。在这两种情况下，有效的属性键均由关联表的列名确定–因此，不能拥有不属于表列的属性.

### 实例公共方法

#### <=>

> 对对象进行排序.

#### ==



#### clone

> 修改clone的属性将修改原始属性，因为它们都指向相同的属性哈希。如果需要属性哈希的副本，请使用dup方法.

```ruby
user = User.first
new_user = user.clone
user.name               # => "Bob"
new_user.name = "Joe"
user.name               # => "Joe"

user.object_id == new_user.object_id            # => false
user.name.object_id == new_user.name.object_id  # => true

user.name.object_id == user.dup.name.object_id  # => false
```

#### connection_handler



#### dup

> 复制对象没有分配ID，将被视为新记录.<br>
请注意，这是"浅"副本，因为它仅复制对象的属性，而不复制其关联.<br>
"深层"副本的范围是特定于应用程序的，因此留给应用程序根据其需要实施.<br>
dup方法不保留(创建|更新)_(at | on)的时间戳.

#### encode_with(coder)

> 填充coder有关该记录的应序列化的属性.<br>
coder确保此方法中定义的结构与coder传递给该init_with方法的结构相匹配.

```ruby
class Post < ActiveRecord::Base
end
coder = {}
Post.new.encode_with(coder)
coder # => {"attributes" => {"id" => nil, ... }}
```

#### eql?

> 别名:==

#### freeze



#### frozen?


#### init_with(coder, &block)

```ruby
class Post < ActiveRecord::Base
end

old_post = Post.new(title: "hello world")
coder = {}
old_post.encode_with(coder)

post = Post.allocate
post.init_with(coder)
post.title # => 'hello world'
```

#### inspect

> 以格式正确的字符串返回记录的内容.

#### pretty_print



#### readonly!

> 将此记录标记为只读.

#### readonly?



#### slice(*methods)

> 返回给定*methods的哈希，其名称为键，返回值作为值.


## [MODULE] CounterCache

> [RubyOnRails | CounterCache](https://api.rubyonrails.org/classes/ActiveRecord/CounterCache.html)

### ClassMethods

#### decrement_counter

> 通过直接SQL更新将数字字段减1.

:touch-更新时触摸时间戳列.<br>
通过true触摸updated_at和/或updated_on.<br>
传递一个符号以触摸该列，或者传递一个符号阵列以仅触摸那些符号

```ruby
# 减少ID为5的记录的posts_count列
DiscussionBoard.decrement_counter(:posts_count, 5)

# 减少ID为5的记录的posts_count列
# 并更新 updated_at 的值.
DiscussionBoard.decrement_counter(:posts_count, 5, touch: true)
```

#### increment_counter

> 通过直接SQL更新，将数字字段加1.

```ruby

```

#### reset_counters

> 使用SQL计数查询将一个或多个计数器缓存重置为其正确值。当添加新的计数器缓存，或者计数器已被SQL直接破坏或修改时，此功能很有用.

```ruby

```

#### update_counters

```ruby

```

## [MODULE] DefineCallbacks

> [RubyOnRails | DefineCallbacks](https://api.rubyonrails.org/classes/ActiveRecord/DefineCallbacks.html)


## [MODULE] DynamicMatchers

> [RubyOnRails | DynamicMatchers](https://api.rubyonrails.org/classes/ActiveRecord/DynamicMatchers.html)


## [MODULE] Enum

> [RubyOnRails | Enum](https://api.rubyonrails.org/classes/ActiveRecord/Enum.html)

> 声明一个枚举属性，该值将值映射到数据库中的整数，但可以按名称查询.

```ruby
class Conversation < ActiveRecord::Base
  enum status: [ :active, :archived ]
end

# conversation.update! status: 0
conversation.active!
conversation.active? # => true
conversation.status  # => "active"

# conversation.update! status: 1
conversation.archived!
conversation.archived? # => true
conversation.status    # => "archived"

# conversation.status = 1
conversation.status = "archived"

conversation.status = nil
conversation.status.nil? # => true
conversation.status      # => nil
```

还将提供基于枚举字段的允许值的范围:

```ruby
Conversation.active
Conversation.not_active
Conversation.archived
Conversation.not_archived
```

还可以使用哈希显式映射属性和数据库整数之间的关系:

```ruby
class Conversation < ActiveRecord::Base
  enum status: { active: 0, archived: 1 }
end
```

直接访问映射。映射是通过具有复数属性名称的类方法公开的，该方法以形式返回映射HashWithIndifferentAccess: 

```ruby
Conversation.statuses[:active]    # => 0
Conversation.statuses["archived"] # => 1
```

当需要了解枚举的序数值时，请使用该类方法。例如，可以在手动构建SQL字符串时使用它:

```ruby
Conversation.where("status <> ?", Conversation.statuses[:archived])
```

需要定义具有相同值的多个枚举时，可以使用:_prefix或:_suffix选项。如果传递的值为true，则方法将以枚举的名称作为前缀/后缀。也可以提供自定义值:

```ruby
class Conversation < ActiveRecord::Base
  enum status: [:active, :archived], _suffix: true
  enum comments_status: [:active, :inactive], _prefix: :comments
end
```

在上面的示例中，bang和predicate方法以及关联的作用域现在被相应地添加了前缀和/或后缀:

```ruby
conversation.active_status!
conversation.archived_status? # => false

conversation.comments_inactive!
conversation.comments_active? # => false
```


## [MODULE] Explain

> [RubyOnRails | Explain](https://api.rubyonrails.org/classes/ActiveRecord/Explain.html)


## [MODULE] FinderMethods

> [RubyOnRails | FinderMethods](https://api.rubyonrails.org/classes/ActiveRecord/FinderMethods.html)

### exists?

参数类型|作用
-|-
Integer|使用此主键查找记录.
String|查找具有与此字符串相对应的主键的记录(例如'5').
Array|查找符合这些find-style条件(例如['name LIKE ?', "%#{query}%"])的记录.
Hash|查找符合这些find-style条件(例如{name: 'David'})的记录.

```ruby
Person.exists?(5)
Person.exists?('5')
Person.exists?(['name LIKE ?', "%#{query}%"])
Person.exists?(id: [1, 4, 8])
Person.exists?(name: 'David')
Person.exists?(false)
Person.exists?
Person.where(name: 'Spartacus', rating: 4).exists?
```

### lock

> 想象两个并发的事务:每个事务都会读取person.visits == 2，并向其中添加1并保存，从而导致两次保存person.visits = 3。通过锁定该行，第二个事务必须等到第一个事务完成为止。得到了期望person.visits == 4.

```ruby
Person.transaction do
  person = Person.lock(true).find(1)
  person.visits += 1
  person.save!
end
```

### find_by 

### find_or_initialize_by

### find_or_create_by

### select

### first

### last

### take

## [MODULE] Inheritance

> [RubyOnRails | Inheritance](https://api.rubyonrails.org/classes/ActiveRecord/Inheritance.html)

> Active Record通过将类的名称存储在默认情况下名为"type"的列中(可以通过覆盖进行更改Base.inheritance_column)来允许继承.<br>
这意味着继承如下所示:

```ruby
class Company < ActiveRecord::Base; end
class Firm < Company; end
class Client < Company; end
class PriorityClient < Client; end
```

完成后Firm.create(name: "37signals")，该记录将以 type=" Firm" 保存在company表中.<br>
然后，可以使用再次获取该行Company.where(name: '37signals').first，它将返回Firm对象.

请注意，由于type列是记录中的一个属性，因此每个新的子类都会立即被标记为改变，并且type列将包含在记录中已更改的属性列表中.<br>
这与非单一表继承(STI)类不同:

```ruby
Company.new.changed? # => false
Firm.new.changed?    # => true
Firm.new.changes     # => {"type"=>["","Firm"]}
```

## [MODULE] Integration

> [RubyOnRails | Integration](https://api.rubyonrails.org/classes/ActiveRecord/Integration.html)


## [MODULE] LegacyYamlAdapter

> [RubyOnRails | LegacyYamlAdapter](https://api.rubyonrails.org/classes/ActiveRecord/LegacyYamlAdapter.html)


## [MODULE] Locking

> [RubyOnRails | Locking](https://api.rubyonrails.org/classes/ActiveRecord/Locking.html)


## [MODULE] Middleware

> [RubyOnRails | Middleware](https://api.rubyonrails.org/classes/ActiveRecord/Middleware.html)


## [MODULE] ModelSchema

> [RubyOnRails | ModelSchema](https://api.rubyonrails.org/classes/ActiveRecord/ModelSchema.html)


## [MODULE] NestedAttributes

> [RubyOnRails | NestedAttributes](https://api.rubyonrails.org/classes/ActiveRecord/NestedAttributes.html)

### ClassMethods

#### accepts_nested_attributes_for

> 嵌套属性更新,通过父级将属性保存在关联记录上.

默认情况下嵌套属性更新处于关闭状态,需手动启用.

保存父模型后，对模型的所有更改(包括那些标记为销毁的销毁)都会自动保存并自动销毁。这发生在由父级的save方法启动的事务内

```ruby
class Book < ActiveRecord::Base
  has_one :author
  has_many :pages

  accepts_nested_attributes_for :author, :pages
end
```

模型中将自动生成两个方法:<br>
author_attributes=(attributes)和pages_attributes=(attributes).

##### 一对一

```ruby
class Member < ActiveRecord::Base
  has_one :avatar
  accepts_nested_attributes_for :avatar
end

```

###### 创建

```ruby
params = { member: { name: 'Jack', avatar_attributes: { icon: 'smiling' } } }
member = Member.create(params[:member])
member.avatar.id # => 2
member.avatar.icon # => 'smiling'
```

###### 更新

```ruby
params = { member: { avatar_attributes: { id: '2', icon: 'sad' } } }
member.update params[:member]
member.avatar.icon # => 'sad'
```

如果要在不提供ID的情况下更新，则必须添加:update_only选项.

```ruby
class Member < ActiveRecord::Base
  has_one :avatar
  accepts_nested_attributes_for :avatar, update_only: true
end

params = { member: { avatar_attributes: { icon: 'sad' } } }
member.update params[:member]
member.avatar.id # => 2
member.avatar.icon # => 'sad'
```

###### 删除

如果要通过属性哈希破坏关联的模型，则必须首先使用:allow_destroy选项启用它.

```ruby
class Member < ActiveRecord::Base
  has_one :avatar
  accepts_nested_attributes_for :avatar, allow_destroy: true
end
```

0. 在保存父模型之前，不会破坏模型.
0. 删除必须指定id.

```ruby
member.avatar_attributes = { id: '2', _destroy: '1' }
member.avatar.marked_for_destruction? # => true
member.save
member.reload.avatar # => nil
```

##### 一对多

```ruby
class Member < ActiveRecord::Base
  has_many :posts
  accepts_nested_attributes_for :posts
end
```

###### 创建

对于每个没有id键的散列，将实例化一个新记录，除非该散列还包含一个结果为true的_destroy键.

```ruby
params = { member: {
  name: 'joe', posts_attributes: [
    { title: 'Kari, the awesome Ruby documentation browser!' },
    { title: 'The egalitarian assumption of the modern citizen' },
    { title: '', _destroy: '1' } # this will be ignored
  ]
}}

member = Member.create(params[:member])
member.posts.length # => 2
member.posts.first.title # => 'Kari, the awesome Ruby documentation browser!'
member.posts.second.title # => 'The egalitarian assumption of the modern citizen'
```

###### 更新

如果哈希包含id与已经关联的记录匹配的键，则将修改匹配的记录:

```ruby
member.attributes = {
  name: 'Joe',
  posts_attributes: [
    { id: 1, title: '[UPDATED] An, as of yet, undisclosed awesome Ruby documentation browser!' },
    { id: 2, title: '[UPDATED] other post' }
  ]
}

member.posts.first.title # => '[UPDATED] An, as of yet, undisclosed awesome Ruby documentation browser!'
member.posts.second.title # => '[UPDATED] other post'
```

该方法只适用于member已存在情况下的更新.

如果想创建一个member命名的joe并想同时更新它posts，那将会ActiveRecord::RecordNotFound出错.

###### 删除

```ruby
class Member < ActiveRecord::Base
  has_many :posts
  accepts_nested_attributes_for :posts, allow_destroy: true
end
```

```ruby
params = { member: {
  posts_attributes: [{ id: '2', _destroy: '1' }]
}}

member.attributes = params[:member]
member.posts.detect { |p| p.id == 2 }.marked_for_destruction? # => true
member.posts.length # => 2
member.save
member.reload.posts.length # => 1
```

##### reject_if

> 设置更新条件, 使其在没有通过条件的情况下静默忽略任何新的记录哈希.

```ruby
class Member < ActiveRecord::Base
  has_many :posts
  accepts_nested_attributes_for :posts, reject_if: proc { |attributes| attributes['title'].blank? }
end

params = { member: {
  name: 'joe', posts_attributes: [
    { title: 'Kari, the awesome Ruby documentation browser!' },
    { title: 'The egalitarian assumption of the modern citizen' },
    { title: '' } # this will be ignored because of the :reject_if proc
  ]
}}

member = Member.create(params[:member])
member.posts.length # => 2
member.posts.first.title # => 'Kari, the awesome Ruby documentation browser!'
member.posts.second.title # => 'The egalitarian assumption of the modern citizen'
```

:reject_if也接受使用方法的符号

```ruby
class Member < ActiveRecord::Base
  has_many :posts
  accepts_nested_attributes_for :posts, reject_if: :new_record?
end

class Member < ActiveRecord::Base
  has_many :posts
  accepts_nested_attributes_for :posts, reject_if: :reject_posts

  def reject_posts(attributes)
    attributes['title'].blank?
  end
end
```

##### 验证父模型的存在

如果要验证子记录是否与父记录相关联，可以使用此示例说明的validates_presence_of方法和:inverse_of键:

```ruby
class Member < ActiveRecord::Base
  has_many :posts, inverse_of: :member
  accepts_nested_attributes_for :posts
end

class Post < ActiveRecord::Base
  belongs_to :member, inverse_of: :posts
  validates_presence_of :member
end
```

请注意，如果未指定该:inverse_of选项，则Active Record将尝试根据启发式方法自动猜测反向关联.

对于一对一的嵌套关联，如果在分配之前自己构建新的(内存中的)子对象，则此模块不会覆盖它，例如:

```ruby
class Member < ActiveRecord::Base
  has_one :avatar
  accepts_nested_attributes_for :avatar

  def avatar
    super || build_avatar(width: 200)
  end
end

member = Member.new
member.avatar_attributes = {icon: 'sad'}
member.avatar.width # => 200
```

## [MODULE] NoTouching

> [RubyOnRails | NoTouching](https://api.rubyonrails.org/classes/ActiveRecord/NoTouching.html)


## [MODULE] Persistence

> [RubyOnRails | Persistence](https://api.rubyonrails.org/classes/ActiveRecord/Persistence.html)


## [MODULE] QueryMethods

> [RubyOnRails | QueryMethods](https://api.rubyonrails.org/classes/ActiveRecord/QueryMethods.html)


## [MODULE] Querying

> [RubyOnRails | Querying](https://api.rubyonrails.org/classes/ActiveRecord/Querying.html)


## [MODULE] ReadonlyAttributes

> [RubyOnRails | ReadonlyAttributes](https://api.rubyonrails.org/classes/ActiveRecord/ReadonlyAttributes.html)


## [MODULE] Reflection

> [RubyOnRails | Reflection](https://api.rubyonrails.org/classes/ActiveRecord/Reflection.html)


## [MODULE] Sanitization

> [RubyOnRails | Sanitization](https://api.rubyonrails.org/classes/ActiveRecord/Sanitization.html)


## [MODULE] Scoping

> [RubyOnRails | Scoping](https://api.rubyonrails.org/classes/ActiveRecord/Scoping.html)


## [MODULE] SecureToken

> [RubyOnRails | SecureToken](https://api.rubyonrails.org/classes/ActiveRecord/SecureToken.html)


## [MODULE] Serialization

> [RubyOnRails | Serialization](https://api.rubyonrails.org/classes/ActiveRecord/Serialization.html)


## [MODULE] SpawnMethods

> [RubyOnRails | SpawnMethods](https://api.rubyonrails.org/classes/ActiveRecord/SpawnMethods.html)


## [MODULE] Store

> [RubyOnRails | Store](https://api.rubyonrails.org/classes/ActiveRecord/Store.html)


## [MODULE] Suppressor

> [RubyOnRails | Suppressor](https://api.rubyonrails.org/classes/ActiveRecord/Suppressor.html)


## [MODULE] Tasks

> [RubyOnRails | Tasks](https://api.rubyonrails.org/classes/ActiveRecord/Tasks.html)


## [MODULE] TestFixtures

> [RubyOnRails | TestFixtures](https://api.rubyonrails.org/classes/ActiveRecord/TestFixtures.html)


## [MODULE] Timestamp

> [RubyOnRails | Timestamp](https://api.rubyonrails.org/classes/ActiveRecord/Timestamp.html)


## [MODULE] Transactions

> [RubyOnRails | Transactions](https://api.rubyonrails.org/classes/ActiveRecord/Transactions.html)


## [MODULE] Translation

> [RubyOnRails | Translation](https://api.rubyonrails.org/classes/ActiveRecord/Translation.html)


## [MODUL] Type

> [RubyOnRails | Type](https://api.rubyonrails.org/classes/ActiveRecord/Type.html)


## [MODULE] VERSION

> [RubyOnRails | VERSION](https://api.rubyonrails.org/classes/ActiveRecord/VERSION.html)


## [MODULE] Validations

> [RubyOnRails | Validations](https://api.rubyonrails.org/classes/ActiveRecord/Validations.html)


## [CLASS] ActiveRecordError

> [RubyOnRails | ActiveRecordError](https://api.rubyonrails.org/classes/ActiveRecord/ActiveRecordError.html)


## [CLASS] AdapterNotFound

> [RubyOnRails | AdapterNotFound](https://api.rubyonrails.org/classes/ActiveRecord/AdapterNotFound.html)


## [CLASS] AdapterNotSpecified

> [RubyOnRails | AdapterNotSpecified](https://api.rubyonrails.org/classes/ActiveRecord/AdapterNotSpecified.html)


## [CLASS] AssociationRelation

> [RubyOnRails | AssociationRelation](https://api.rubyonrails.org/classes/ActiveRecord/AssociationRelation.html)


## [CLASS] AssociationTypeMismatch

> [RubyOnRails | AssociationTypeMismatch](https://api.rubyonrails.org/classes/ActiveRecord/AssociationTypeMismatch.html)


## [CLASS] AttributeAssignmentError

> [RubyOnRails | AttributeAssignmentError](https://api.rubyonrails.org/classes/ActiveRecord/AttributeAssignmentError.html)


## [CLASS] Base

> [RubyOnRails | Base](https://api.rubyonrails.org/classes/ActiveRecord/Base.html)


## [CLASS] ConfigurationError

> [RubyOnRails | ConfigurationError](https://api.rubyonrails.org/classes/ActiveRecord/ConfigurationError.html)


## [CLASS] ConnectionNotEstablished

> [RubyOnRails | ConnectionNotEstablished](https://api.rubyonrails.org/classes/ActiveRecord/ConnectionNotEstablished.html)


## [CLASS] ConnectionTimeoutError

> [RubyOnRails | ConnectionTimeoutError](https://api.rubyonrails.org/classes/ActiveRecord/ConnectionTimeoutError.html)


## [CLASS] DangerousAttributeError

> [RubyOnRails | DangerousAttributeError](https://api.rubyonrails.org/classes/ActiveRecord/DangerousAttributeError.html)


## [CLASS] DatabaseConfigurations

> [RubyOnRails | DatabaseConfigurations](https://api.rubyonrails.org/classes/ActiveRecord/DatabaseConfigurations.html)


## [CLASS] Deadlocked

> [RubyOnRails | Deadlocked](https://api.rubyonrails.org/classes/ActiveRecord/Deadlocked.html)


## [CLASS] EagerLoadPolymorphicError

> [RubyOnRails | EagerLoadPolymorphicError](https://api.rubyonrails.org/classes/ActiveRecord/EagerLoadPolymorphicError.html)


## [CLASS] EnvironmentMismatchError

> [RubyOnRails | EnvironmentMismatchError](https://api.rubyonrails.org/classes/ActiveRecord/EnvironmentMismatchError.html)


## [CLASS] ExclusiveConnectionTimeoutError

> [RubyOnRails | ExclusiveConnectionTimeoutError](https://api.rubyonrails.org/classes/ActiveRecord/ExclusiveConnectionTimeoutError.html)


## [CLASS] FixtureSet

> [RubyOnRails | FixtureSet](https://api.rubyonrails.org/classes/ActiveRecord/FixtureSet.html)


## [CLASS] ImmutableRelation

> [RubyOnRails | ImmutableRelation](https://api.rubyonrails.org/classes/ActiveRecord/ImmutableRelation.html)


## [CLASS] InvalidForeignKey

> [RubyOnRails | InvalidForeignKey](https://api.rubyonrails.org/classes/ActiveRecord/InvalidForeignKey.html)


## [CLASS] IrreversibleMigration

> [RubyOnRails | IrreversibleMigration](https://api.rubyonrails.org/classes/ActiveRecord/IrreversibleMigration.html)


## [CLASS] IrreversibleOrderError

> [RubyOnRails | IrreversibleOrderError](https://api.rubyonrails.org/classes/ActiveRecord/IrreversibleOrderError.html)


## [CLASS] LockWaitTimeout

> [RubyOnRails | LockWaitTimeout](https://api.rubyonrails.org/classes/ActiveRecord/LockWaitTimeout.html)


## [CLASS] LogSubscriber

> [RubyOnRails | LogSubscriber](https://api.rubyonrails.org/classes/ActiveRecord/LogSubscriber.html)


## [CLASS] Migration

> [RubyOnRails | Migration](https://api.rubyonrails.org/classes/ActiveRecord/Migration.html)


## [CLASS] MismatchedForeignKey

> [RubyOnRails | MismatchedForeignKey](https://api.rubyonrails.org/classes/ActiveRecord/MismatchedForeignKey.html)


## [CLASS] MultiparameterAssignmentErrors

> [RubyOnRails | MultiparameterAssignmentErrors](https://api.rubyonrails.org/classes/ActiveRecord/MultiparameterAssignmentErrors.html)


## [CLASS] NoDatabaseError

> [RubyOnRails | NoDatabaseError](https://api.rubyonrails.org/classes/ActiveRecord/NoDatabaseError.html)


## [CLASS] NotNullViolation

> [RubyOnRails | NotNullViolation](https://api.rubyonrails.org/classes/ActiveRecord/NotNullViolation.html)


## [CLASS] PreparedStatementCacheExpired

> [RubyOnRails | PreparedStatementCacheExpired](https://api.rubyonrails.org/classes/ActiveRecord/PreparedStatementCacheExpired.html)


## [CLASS] PreparedStatementInvalid

> [RubyOnRails | PreparedStatementInvalid](https://api.rubyonrails.org/classes/ActiveRecord/PreparedStatementInvalid.html)


## [CLASS] QueryCache

> [RubyOnRails | QueryCache](https://api.rubyonrails.org/classes/ActiveRecord/QueryCache.html)


## [CLASS] QueryCanceled

> [RubyOnRails | QueryCanceled](https://api.rubyonrails.org/classes/ActiveRecord/QueryCanceled.html)


## [CLASS] RangeError

> [RubyOnRails | RangeError](https://api.rubyonrails.org/classes/ActiveRecord/RangeError.html)


## [CLASS] ReadOnlyError

> [RubyOnRails | ReadOnlyError](https://api.rubyonrails.org/classes/ActiveRecord/ReadOnlyError.html)


## [CLASS] ReadOnlyRecord

> [RubyOnRails | ReadOnlyRecord](https://api.rubyonrails.org/classes/ActiveRecord/ReadOnlyRecord.html)


## [CLASS] RecordInvalid

> [RubyOnRails | RecordInvalid](https://api.rubyonrails.org/classes/ActiveRecord/RecordInvalid.html)


## [CLASS] RecordNotDestroyed

> [RubyOnRails | RecordNotDestroyed](https://api.rubyonrails.org/classes/ActiveRecord/RecordNotDestroyed.html)


## [CLASS] RecordNotFound

> [RubyOnRails | RecordNotFound](https://api.rubyonrails.org/classes/ActiveRecord/RecordNotFound.html)


## [CLASS] RecordNotSaved

> [RubyOnRails | RecordNotSaved](https://api.rubyonrails.org/classes/ActiveRecord/RecordNotSaved.html)


## [CLASS] RecordNotUnique

> [RubyOnRails | RecordNotUnique](https://api.rubyonrails.org/classes/ActiveRecord/RecordNotUnique.html)


## [CLASS] Relation

> [RubyOnRails | Relation](https://api.rubyonrails.org/classes/ActiveRecord/Relation.html)


## [CLASS] Result

> [RubyOnRails | Result](https://api.rubyonrails.org/classes/ActiveRecord/Result.html)


## [CLASS] Rollback

> [RubyOnRails | Rollback](https://api.rubyonrails.org/classes/ActiveRecord/Rollback.html)


## [CLASS] Schema

> [RubyOnRails | Schema](https://api.rubyonrails.org/classes/ActiveRecord/Schema.html)


## [CLASS] SerializationFailure

> [RubyOnRails | SerializationFailure](https://api.rubyonrails.org/classes/ActiveRecord/SerializationFailure.html)


## [CLASS] SerializationTypeMismatch

> [RubyOnRails | SerializationTypeMismatch](https://api.rubyonrails.org/classes/ActiveRecord/SerializationTypeMismatch.html)


## [CLASS] StaleObjectError

> [RubyOnRails | StaleObjectError](https://api.rubyonrails.org/classes/ActiveRecord/StaleObjectError.html)


## [CLASS] StatementCache

> [RubyOnRails | StatementCache](https://api.rubyonrails.org/classes/ActiveRecord/StatementCache.html)


## [CLASS] StatementInvalid

> [RubyOnRails | StatementInvalid](https://api.rubyonrails.org/classes/ActiveRecord/StatementInvalid.html)


## [CLASS] StatementTimeout

> [RubyOnRails | StatementTimeout](https://api.rubyonrails.org/classes/ActiveRecord/StatementTimeout.html)


## [CLASS] SubclassNotFound

> [RubyOnRails | SubclassNotFound](https://api.rubyonrails.org/classes/ActiveRecord/SubclassNotFound.html)


## [CLASS] TransactionIsolationError

> [RubyOnRails | TransactionIsolationError](https://api.rubyonrails.org/classes/ActiveRecord/TransactionIsolationError.html)


## [CLASS] TransactionRollbackError

> [RubyOnRails | TransactionRollbackError](https://api.rubyonrails.org/classes/ActiveRecord/TransactionRollbackError.html)


## [CLASS] TypeConflictError

> [RubyOnRails | TypeConflictError](https://api.rubyonrails.org/classes/ActiveRecord/TypeConflictError.html)


## [CLASS] UnknownAttributeError

> [RubyOnRails | UnknownAttributeError](https://api.rubyonrails.org/classes/ActiveModel/UnknownAttributeError.html)


## [CLASS] UnknownAttributeReference

> [RubyOnRails | UnknownAttributeReference](https://api.rubyonrails.org/classes/ActiveRecord/UnknownAttributeReference.html)


## [CLASS] UnknownPrimaryKey

> [RubyOnRails | UnknownPrimaryKey](https://api.rubyonrails.org/classes/ActiveRecord/UnknownPrimaryKey.html)


## [CLASS] ValueTooLong

> [RubyOnRails | ValueTooLong](https://api.rubyonrails.org/classes/ActiveRecord/ValueTooLong.html)


## [CLASS] WrappedDatabaseException

> [RubyOnRails | WrappedDatabaseException](https://api.rubyonrails.org/classes/ActiveRecord/WrappedDatabaseException.html)


# 参考资料

> [Ruby on Rails | Active Record Nested Attributes](https://api.rubyonrails.org/classes/ActiveRecord/NestedAttributes/ClassMethods.html)