<!-- TOC -->

- [说明](#说明)
  - [WIP](#wip)
  - [特殊符号 `.`](#特殊符号-)
  - [传输协议](#传输协议)
- [基本动作](#基本动作)
  - [克隆(clone)](#克隆clone)
    - [稀疏检出](#稀疏检出)
      - [检出流程](#检出流程)
      - [初始化项目](#初始化项目)
      - [添加远程分支](#添加远程分支)
      - [开启稀疏检出](#开启稀疏检出)
      - [配置稀疏检出](#配置稀疏检出)
      - [checkout分支](#checkout分支)
      - [拉取远程分支](#拉取远程分支)
    - [指定分支](#指定分支)
  - [拉(pull)](#拉pull)
  - [推(push)](#推push)
    - [添加到暂存区](#添加到暂存区)
    - [撤销暂存](#撤销暂存)
    - [代码提交](#代码提交)
    - [提交空目录](#提交空目录)
      - [目录是空的](#目录是空的)
        - [批量创建 .gitkeep 文件](#批量创建-gitkeep-文件)
      - [目录中已经存在文件](#目录中已经存在文件)
  - [其他](#其他)
    - [查看状态](#查看状态)
    - [版本回退](#版本回退)
    - [查看分支](#查看分支)
    - [头指针分离](#头指针分离)
    - [查看提交内容](#查看提交内容)
- [其他操作](#其他操作)
  - [拆分子项目(subtree)](#拆分子项目subtree)
- [相关设置](#相关设置)
  - [设置文件/目录大小写敏感](#设置文件目录大小写敏感)
  - [帐号缓存](#帐号缓存)
  - [重置缓存账号信息](#重置缓存账号信息)
- [高阶](#高阶)
  - [git log](#git-log)
    - [format(格式化显示历史提交日志)](#format格式化显示历史提交日志)
- [常见问题](#常见问题)
  - [git status显示中文文件名乱码问题](#git-status显示中文文件名乱码问题)
  - [git diff 显示中文乱码问题](#git-diff-显示中文乱码问题)
  - [PROTOCOL_ERROR](#protocol_error)
  - [insufficient permission for adding an object](#insufficient-permission-for-adding-an-object)
  - [MERGE_HEAD exists](#merge_head-exists)
  - [fatal: 不是一个 git 仓库(或者任何父目录)：.git](#fatal-不是一个-git-仓库或者任何父目录git)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## WIP

> WIP全称：Work in progress,正在工作过程中,引申含义为*目前工作区中的代码正在编写中,这部分代码不能独立运行,是半成品*.


WIP其实代表的就是WIP版本里面的代码是 *正在工作并编写的代码*,意思便是 *代码工作只开发了一半,不能独立的运行*.<br>
直接把这种半成品代码提交commit是极其不合适的,但是我们可以把这部分代码git stash储藏起来,并用WIP给他们做标记.<br>

在GitLab中如果申请合并的title以WIP:开头的合并请求会被认为是暂存:

> Start the title with WIP: to prevent a Work In Progress merge request from being merged before it's ready.

```
WIP: Test
```

## 特殊符号 `.`

符号 `.` 的作用是将命令的作用域仅限于当前文件夹及其子文件夹,如:

```bash
git status . # 查看当前目录下的修改文件
git diff . # 查看当前目录下的修改内容
git add . # 将当前目录下的修改全部提交
```

## 传输协议

git可以使用四种主要的协议来传输资料:

0. 本地协议(Local)
0. HTTP协议
0. SSH(Secure Shell)协议
0. git协议.

其中,本地协议由于目前大都是进行远程开发和共享代码所以一般不常用,<br>
而git协议由于缺乏授权机制且较难架设所以也不常用.

最常用的便是SSH和HTTP(S)协议.<br>
git关联远程仓库可以使用http协议或者ssh协议.

配置ssh协议时,公钥和私钥路径和名称必须和下面的保持一致:

```
~/.ssh/id_rsa.pub(公钥)
~/.ssh/id_rsa(私钥)
```

# 基本动作

## 克隆(clone)

### 稀疏检出

> 检出相匹配的部分内容

#### 检出流程

对于空目录,流程如下

#### 初始化项目
#### 添加远程分支
#### 开启稀疏检出

```bash
git config core.sparsecheckout true
```

#### 配置稀疏检出

.git/info/sparse-checkout文件用于配置检出内容规则,常见配置方法:

**注意:每行开头/结尾不能存在空格**

```bash
# 子目录的匹配
/ruby/rails # 匹配根目录下的ruby/rails文件夹
ruby/rails # 匹配任意位置下的ruby/rails文件夹

# 通配符匹配
*.rb # 检出所有后缀为.rb的文件

# 排除项过滤
# 如果有排除项,必须写 /*,同时排除项要写在通配符后面
/*
!/docs/ # 排除根目录docs下的所有内容

```

需要注意的是该规则只能每行配置一项,不能用逗号分隔配置多项,例如:

```bash
tee .git/info/sparse-checkout <<-'EOF'
/*
!*.wav
!*.flac
!*.ape
!*.wavpack
!*.mp3
!*.aac
!*.vorbis
!*.opus
EOF
```

如果要关闭sparsecheckout功能,全取整个项目库,可以写一个”*“号.

#### checkout分支

.git/info/sparse-checkout 增加或删除部分目录,都需要checkout下该分支.

#### 拉取远程分支

### 指定分支

```bash
git clone -b 分支名 仓库地址
```

## 拉(pull)

```
git pull 别名 分支
```

## 推(push)

### 添加到暂存区

```bash
git add options
```
options|含义
-|-
file1 file2 ...|添加指定文件到暂存区
dir1 dir2...|添加指定目录到暂存区,包括子目录
.|添加当前目录及其子目录的所有文件到暂存区

### 撤销暂存

操作|说明
-|-
git checkout .|恢复暂存区的所有文件到工作区


### 代码提交

```bash
git commit options
```

options|含义
-|-
-m message<sup>[1]</sup>|提交暂存区内容到仓库区
-a message<sup>[2]</sup>|提交工作区自上次commit之后的变化,直接到仓库区
-v|提交时显示所有diff信息

[1] :message指对本次提交的一个简要概述,如:<br>
git commit -m "订单完成条件修改"

[2] 通常会使用 git -am message 来提交代码.

### 提交空目录

#### 目录是空的

> .gitkeep 是一个`约定俗成的文件名`并不会带有特殊规则.

在目录下创建.gitkeep文件,然后在项目的.gitignore中设置不忽略.gitkeep:

```
!.gitkeep
```

该文件应放在空目录下,而不是在根目录下存放一个就可以实现全局空文件夹上传.

##### 批量创建 .gitkeep 文件

在每一个空目录的末级下创建.gitkeep文件:

```bash
find . -type d -empty -execdir touch {}/.gitkeep \;
```

#### 目录中已经存在文件

首先在根目录中设置!.gitignore,然后在目标目录也创建一个.gitignore文件,并在文件中设置:

```bash
# 忽略说有文件
*
# .gitignore除外
!.gitignore
```

## 其他

### 查看状态

操作|含义
-|-
git status|显示有变更的文件
git log|显示当前分支的版本历史
git diff|显示暂存区和工作区的代码差异

### 版本回退

```
git reset commit_id
```

### 查看分支

操作|含义
-|-
git branch|列出所有本地分支
git branch -r|列出所有远程分支
git branch -a|列出所有本地分支和远程分支
git branch branch_name|新建一个分支,但依然停留在当前分支
git checkout -b branch_name|新建一个分支,并切换到该分支
git branch -d branch_name|删除分支
git checkout branch_name|切换到指定分支

### 头指针分离

```bash
# 强制将 master 分支指向当前头指针的位置
git branch -f master HEAD
# 检出 master 分支
git checkout master
```

### 查看提交内容

根据 commit id查看该提交修改内容

```
git show commit_id
```

# 其他操作

## 拆分子项目(subtree)

```sh
# 进入当前项目根目录下
root_path=$PWD
# 被拆分对象的当前相对路径
current_path='知识体系/应用科学/计算机科学/计算机系统/数据库/Elasticsearch/graphic-object-es'
# 拆分后存储路径
split_path='./graphic-object-es'
# 拆分后分支
split_branch='graphic-object-es'
# 拆分后项目仓库路径
split_remote='git@gitlab.com:f97d1c/graphic-object-es.git'

git subtree split -P $current_path -b $split_branch
mkdir $split_path
cd $split_path
git init
git remote add origin $split_remote
git pull $root_path $split_branch
git push origin master
cd $root_path
```

# 相关设置

## 设置文件/目录大小写敏感

> Windows, macOS 默认是不区分大小写的,而Linux区分大小.

开启大小写敏感:

```
git config core.ignorecase false
```

## 帐号缓存

```bash
git config --global credential.helper 'cache --timeout 36000000000'
```

36000000000为缓存秒数.

## 重置缓存账号信息

> fatal: repository 'https://*.git/' not found

有一种可能是由于缓存账号的问题导致,可以进行重置.

```bash
git config --global --unset credential.helper
```

# 高阶

## git log

### format(格式化显示历史提交日志)

```sh
git log -1 --format="%s" 0725397d15282cd12b9a6f79b1e42fd829cc4723
git show -1 --format="%s" 0725397d15282cd12b9a6f79b1e42fd829cc4723
# 脚本拆分

git log -1 --format="The author of %h was %an, %ar%nThe title was >>%s<<%n" 0725397d15282cd12b9a6f79b1e42fd829cc4723
# The author of 0725397 was ff4c00, 3 个月前
# The title was >>脚本拆分<<
```

# 常见问题

## git status显示中文文件名乱码问题

> core.quotepath 选项为 true 时,会对中文字符进行转义再显示,看起来就像是乱码.设置该选项为false,就会正常显示中文.

```
git config --global core.quotepath false
```

## git diff 显示中文乱码问题

```bash
git config --global i18n.commitencoding utf-8
git config --global i18n.logoutputencoding utf-8
export LESSCHARSET=utf-8
```

## PROTOCOL_ERROR


> error: RPC failed; curl 92 HTTP/2 stream 0 was not closed cleanly: PROTOCOL_ERROR (err 1)

提交/拉取 较大文件时易出现该错误.

通过增加git缓冲区的大小得以解决:

```
git config --global http.postBuffer 524288000
```

## insufficient permission for adding an object

> error: insufficient permission for adding an object to repository database .git/objects

原因在于项目的 .git 目录有些文件夹的权限是root用户.

将.git文件夹改为当前用户所有即可.

## MERGE_HEAD exists

> error: You have not concluded your merge (MERGE_HEAD exists)

解决办法:保留本地的更改,中止合并->重新合并->重新拉取

```bash
git merge --abort
git reset --merge
git pull
```

pull的时候提示已存在临时文件:<br>
..."~/Documents/Space/.git/.MERGE_MSG.swp"...

尚不知其具体影响,直接删除了.

## fatal: 不是一个 git 仓库(或者任何父目录)：.git

情景1:

.git/HEAD 缺失,但 .git/ORIG_HEAD 文件是好的(它有提交id).<br>
把 .git/ORIG_HEAD 内容为 .git/HEAD 文件

# 参考资料

> [博客园 | 天才卧龙](https://www.cnblogs.com/chenwolong/p/GIT.html)

> [简书 | 危险!分离头指针](https://www.jianshu.com/p/91a0f8feb45d)

> [CSDN | Git中对于"git stash"中的"WIP"缩写的正确理解](https://blog.csdn.net/wq6ylg08/article/details/88965520)

> [廖雪峰的官方网站 | 版本回退](https://www.liaoxuefeng.com/wiki/896043488029600/897013573512192)

> [segmentfault | 解决git status显示中文文件名乱码问题](https://segmentfault.com/a/1190000020807726)

> [知乎 | 为什么git默认不区分文件夹大小写？](https://www.zhihu.com/question/57779034)

> [IT工具网 | git - 如何获得单个子文件夹的git-status？](https://www.coder.work/article/185223)

> [简书 | git log,git diff命令在powershell, cmd, cmder乱码问题](https://www.jianshu.com/p/86f9c0358c4a)

> [Github | Git只获取部分目录的内容(稀疏检出)](https://zhgcao.github.io/2016/05/11/git-sparse-checkout/)

> [stackoverflow | Git Sparse Checkout Leaves No Entry on Working Directory](https://stackoverflow.com/questions/24691300/git-sparse-checkout-leaves-no-entry-on-working-directory)

> [segmentfault | git中如何提交空目录](https://segmentfault.com/a/1190000006866280)

> [bbsmax | git 提交空文件夹](https://www.bbsmax.com/A/1O5EO83z7a/)

> [stackoverflow | error: RPC failed; curl 92 HTTP/2 stream 0 was not closed cleanly: PROTOCOL_ERROR (err 1)](https://stackoverflow.com/questions/59282476/error-rpc-failed-curl-92-http-2-stream-0-was-not-closed-cleanly-protocol-erro)

> [CSDN | git clone 指定分支 拉代码](https://blog.csdn.net/weixin_39800144/article/details/78205617)

> [博客园 | 解决Git报错:error: You have not concluded your merge (MERGE_HEAD exists).](https://www.cnblogs.com/whowhere/p/9334793.html)

> [CSDN | git 重置用户名 密码信息](https://blog.csdn.net/qq_27263265/article/details/83241364)

> [小空笔记 | git fatal 不是一个git仓库(或任何一个父目录).git](https://www.xknote.com/ask/61101d9f8a731.html)


> [stackoverflow | How to find a commit by its hash?](https://stackoverflow.com/questions/14167335/how-to-find-a-commit-by-its-hash)

> [kernel | git-show(1) Manual Page](https://mirrors.edge.kernel.org/pub/software/scm/git/docs/git-show.html#_pretty_formats)

