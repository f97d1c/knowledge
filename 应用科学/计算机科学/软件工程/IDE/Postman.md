
<!-- TOC -->

- [官方教程](#官方教程)
  - [入门](#入门)
    - [介绍](#介绍)
    - [安装和更新](#安装和更新)
    - [导航Postman](#导航postman)
    - [发送第一个请求](#发送第一个请求)
    - [管理账户](#管理账户)
    - [同步工作](#同步工作)
    - [发现模板](#发现模板)
    - [创建第一个收藏](#创建第一个收藏)
    - [创建工作区](#创建工作区)
    - [设置Postman应用](#设置postman应用)
    - [导入和导出数据](#导入和导出数据)
    - [解决应用问题](#解决应用问题)
  - [发送请求](#发送请求)
    - [建立要求](#建立要求)
    - [授权请求](#授权请求)
    - [收到回应](#收到回应)
    - [将集合中的请求分组](#将集合中的请求分组)
    - [使用变量](#使用变量)
    - [管理环境](#管理环境)
    - [可视化响应](#可视化响应)
    - [指定示例](#指定示例)
    - [使用cookie](#使用cookie)
    - [使用证书](#使用证书)
    - [生成客户端代码](#生成客户端代码)
    - [故障排除请求](#故障排除请求)
  - [捕获请求数据](#捕获请求数据)
    - [捕获HTTP请求](#捕获http请求)
    - [使用代理](#使用代理)
    - [使用Postman拦截器](#使用postman拦截器)
  - [支持的API框架](#支持的api框架)
    - [用GraphQL查询](#用graphql查询)
    - [发出SOAP请求](#发出soap请求)
  - [写剧本](#写剧本)
    - [Postman脚本](#postman脚本)
    - [编写请求前脚本](#编写请求前脚本)
    - [编写测试](#编写测试)
  - [脚本参考](#脚本参考)
    - [测试脚本示例](#测试脚本示例)
    - [动态变量](#动态变量)
    - [Postman沙箱](#postman沙箱)
  - [运行收藏](#运行收藏)
    - [使用收集运行器](#使用收集运行器)
    - [通过监视器安排运行](#通过监视器安排运行)
    - [构建请求工作流程](#构建请求工作流程)
    - [导入数据文件](#导入数据文件)
  - [使用newman CLI](#使用newman-cli)
    - [使用Newman在命令行上运行集合](#使用newman在命令行上运行集合)
    - [newman 与Docker](#newman-与docker)
    - [带有Postman API的CI](#带有postman-api的ci)
    - [与Travis CI集成](#与travis-ci集成)
    - [与Jenkins集成](#与jenkins集成)
  - [与Postman合作](#与postman合作)
    - [与团队合作](#与团队合作)
    - [定义角色](#定义角色)
    - [请求访问](#请求访问)
    - [分享工作](#分享工作)
    - [专用API网络](#专用api网络)
    - [评论收藏](#评论收藏)
    - [版本控制API](#版本控制api)
    - [使用版本控制](#使用版本控制)
  - [使用工作区](#使用工作区)
    - [创建工作区](#创建工作区-1)
    - [使用和管理工作区](#使用和管理工作区)
    - [查看工作空间活动](#查看工作空间活动)
    - [解决团队冲突](#解决团队冲突)
  - [设计和开发API](#设计和开发api)
    - [使用API Builder](#使用api-builder)
    - [管理和共享API](#管理和共享api)
    - [验证API](#验证api)
  - [模拟数据](#模拟数据)
    - [设置模拟服务器](#设置模拟服务器)
    - [模拟例子](#模拟例子)
    - [通过API模拟](#通过api模拟)
    - [了解示例匹配](#了解示例匹配)
  - [监控API](#监控api)
    - [监控API](#监控api-1)
    - [设置显示器](#设置显示器)
    - [查看监控结果](#查看监控结果)
    - [监控API和网站](#监控api和网站)
    - [设置集成以接收警报](#设置集成以接收警报)
    - [使用静态IP运行Postman监视器](#使用静态ip运行postman监视器)
    - [排除显示器故障](#排除显示器故障)
    - [监控常见问题](#监控常见问题)
    - [分析报告](#分析报告)
  - [发布API](#发布api)
    - [记录API](#记录api)
    - [撰写文件](#撰写文件)
    - [发布文档](#发布文档)
    - [查看文件](#查看文件)
    - [使用自定义域](#使用自定义域)
    - [发布模板](#发布模板)
    - [发布到API网络](#发布到api网络)
    - [提交准则](#提交准则)
  - [在Postman中运行](#在postman中运行)
    - [使用在Postman中运行按钮](#使用在postman中运行按钮)
    - [创建在Postman中运行按钮](#创建在postman中运行按钮)
    - [在Postman中运行编码](#在postman中运行编码)
  - [行政](#行政)
    - [管理团队](#管理团队)
    - [采购Postman](#采购postman)
    - [开票](#开票)
    - [配置团队设置](#配置团队设置)
    - [利用审核日志](#利用审核日志)
    - [入职清单](#入职清单)
  - [单点登录（SSO）](#单点登录sso)
    - [SSO简介](#sso简介)
    - [为团队配置SSO](#为团队配置sso)
    - [登录到SSO团队](#登录到sso团队)
    - [Microsoft AD FS](#microsoft-ad-fs)
    - [Azure AD中的自定义SAML](#azure-ad中的自定义saml)
    - [Duo中的自定义SAML](#duo中的自定义saml)
    - [GSuite中的自定义SAML](#gsuite中的自定义saml)
    - [Okta中的自定义SAML](#okta中的自定义saml)
    - [Onelogin中的自定义SAML](#onelogin中的自定义saml)
    - [Ping身份中的自定义SAML](#ping身份中的自定义saml)
    - [迁移到最新版本的Postman](#迁移到最新版本的postman)
  - [开发人员资源](#开发人员资源)
    - [使用Postman实用程序进行开发](#使用postman实用程序进行开发)
    - [PostmanAPI](#postmanapi)
    - [回声API](#回声api)
    - [集合SDK](#集合sdk)
    - [Postman运行时库](#postman运行时库)
    - [代码生成器库](#代码生成器库)
    - [Postman收藏转换](#postman收藏转换)
  - [整合](#整合)
    - [与Postman整合](#与postman整合)
    - [自定义Webhook](#自定义webhook)
  - [可用的集成](#可用的集成)
    - [APIMatic](#apimatic)
    - [AWS API网关](#aws-api网关)
    - [大熊猫](#大熊猫)
    - [比特桶](#比特桶)
    - [Coralogix](#coralogix)
    - [数据狗](#数据狗)
    - [投寄箱](#投寄箱)
    - [的GitHub](#的github)
    - [亚搏体育app](#亚搏体育app)
    - [嘻哈](#嘻哈)
    - [敏锐](#敏锐)
    - [微软流程](#微软流程)
    - [微软团队](#微软团队)
    - [OpenAPI的](#openapi的)
    - [PagerDuty](#pagerduty)
    - [松弛](#松弛)
    - [维克多](#维克多)
- [参考资料](#参考资料)

<!-- /TOC -->

# 官方教程

## 入门

### 介绍

>[官网文档 | 介绍](urrent="page" class="" href="https://learning.postman.com/docs/getting-started/introduction)

### 安装和更新

>[官网文档 | 安装和更新](https://learning.postman.com/docs/getting-started/installation-and-updates)

### 导航Postman

>[官网文档 | 导航Postman](https://learning.postman.com/docs/getting-started/navigating-postman)

### 发送第一个请求

>[官网文档 | 发送第一个请求](https://learning.postman.com/docs/getting-started/sending-the-first-request)

### 管理账户

>[官网文档 | 管理账户](https://learning.postman.com/docs/getting-started/postman-account)

### 同步工作

>[官网文档 | 同步工作](https://learning.postman.com/docs/getting-started/syncing)

### 发现模板

>[官网文档 | 发现模板](https://learning.postman.com/docs/getting-started/importing-templates)

### 创建第一个收藏

>[官网文档 | 创建第一个收藏](https://learning.postman.com/docs/getting-started/creating-the-first-collection)

### 创建工作区

>[官网文档 | 创建工作区](https://learning.postman.com/docs/getting-started/creating-your-first-workspace)

### 设置Postman应用

>[官网文档 | 设置Postman应用](https://learning.postman.com/docs/getting-started/settings)

### 导入和导出数据

>[官网文档 | 导入和导出数据](https://learning.postman.com/docs/getting-started/importing-and-exporting-data)

### 解决应用问题

>[官网文档 | 解决应用问题](https://learning.postman.com/docs/getting-started/troubleshooting-inapp)

## 发送请求

### 建立要求

>[官网文档 | 建立要求](https://learning.postman.com/docs/sending-requests/requests)

### 授权请求

>[官网文档 | 授权请求](https://learning.postman.com/docs/sending-requests/authorization)

### 收到回应

>[官网文档 | 收到回应](https://learning.postman.com/docs/sending-requests/responses)

### 将集合中的请求分组

>[官网文档 | 将集合中的请求分组](https://learning.postman.com/docs/sending-requests/intro-to-collections)

### 使用变量

>[官网文档 | 使用变量](https://learning.postman.com/docs/sending-requests/variables)

### 管理环境

>[官网文档 | 管理环境](https://learning.postman.com/docs/sending-requests/managing-environments)

### 可视化响应

>[官网文档 | 可视化响应](https://learning.postman.com/docs/sending-requests/visualizer)

### 指定示例

>[官网文档 | 指定示例](https://learning.postman.com/docs/sending-requests/examples)

### 使用cookie

>[官网文档 | 使用cookie](https://learning.postman.com/docs/sending-requests/cookies)

### 使用证书

>[官网文档 | 使用证书](https://learning.postman.com/docs/sending-requests/certificates)

### 生成客户端代码

>[官网文档 | 生成客户端代码](https://learning.postman.com/docs/sending-requests/generate-code-snippets)

### 故障排除请求

>[官网文档 | 故障排除请求](https://learning.postman.com/docs/sending-requests/troubleshooting-api-requests)

## 捕获请求数据
### 捕获HTTP请求

>[官网文档 | 捕获HTTP请求](https://learning.postman.com/docs/sending-requests/capturing-request-data/capturing-http-requests)

### 使用代理

>[官网文档 | 使用代理](https://learning.postman.com/docs/sending-requests/capturing-request-data/proxy)

### 使用Postman拦截器 

>[官网文档 | 使用Postman拦截器 ](https://learning.postman.com/docs/sending-requests/capturing-request-data/interceptor)

## 支持的API框架

### 用GraphQL查询

>[官网文档 | 用GraphQL查询](https://learning.postman.com/docs/sending-requests/supported-api-frameworks/graphql)

### 发出SOAP请求

>[官网文档 | 发出SOAP请求](https://learning.postman.com/docs/sending-requests/supported-api-frameworks/making-soap-requests)

## 写剧本

### Postman脚本

>[官网文档 | Postman脚本](https://learning.postman.com/docs/writing-scripts/intro-to-scripts)

### 编写请求前脚本

>[官网文档 | 编写请求前脚本](https://learning.postman.com/docs/writing-scripts/pre-request-scripts)

### 编写测试

>[官网文档 | 编写测试](https://learning.postman.com/docs/writing-scripts/test-scripts)

## 脚本参考

### 测试脚本示例

>[官网文档 | 测试脚本示例](https://learning.postman.com/docs/writing-scripts/script-references/test-examples)

### 动态变量

>[官网文档 | 动态变量](https://learning.postman.com/docs/writing-scripts/script-references/variables-list)

### Postman沙箱

>[官网文档 | Postman沙箱](https://learning.postman.com/docs/writing-scripts/script-references/postman-sandbox-api-reference)

## 运行收藏

### 使用收集运行器

>[官网文档 | 使用收集运行器](https://learning.postman.com/docs/running-collections/intro-to-collection-runs)

### 通过监视器安排运行

>[官网文档 | 通过监视器安排运行](https://learning.postman.com/docs/running-collections/scheduling-collection-runs)

### 构建请求工作流程

>[官网文档 | 构建请求工作流程](https://learning.postman.com/docs/running-collections/building-workflows)

### 导入数据文件

>[官网文档 | 导入数据文件](https://learning.postman.com/docs/running-collections/working-with-data-files)

## 使用newman CLI
### 使用Newman在命令行上运行集合

>[官网文档 | 使用Newman在命令行上运行集合](https://learning.postman.com/docs/running-collections/using-newman-cli/command-line-integration-with-newman)

### newman 与Docker

>[官网文档 | newman 与Docker](https://learning.postman.com/docs/running-collections/using-newman-cli/newman-with-docker)

### 带有Postman API的CI

>[官网文档 | 带有Postman API的CI](https://learning.postman.com/docs/running-collections/using-newman-cli/continuous-integration)

### 与Travis CI集成

>[官网文档 | 与Travis CI集成](https://learning.postman.com/docs/running-collections/using-newman-cli/integration-with-travis)

### 与Jenkins集成

>[官网文档 | 与Jenkins集成](https://learning.postman.com/docs/running-collections/using-newman-cli/integration-with-jenkins)

## 与Postman合作

### 与团队合作

>[官网文档 | 与团队合作](https://learning.postman.com/docs/collaborating-in-postman/collaboration-intro)

### 定义角色

>[官网文档 | 定义角色](https://learning.postman.com/docs/collaborating-in-postman/roles-and-permissions)

### 请求访问

>[官网文档 | 请求访问](https://learning.postman.com/docs/collaborating-in-postman/requesting-access-to-collections)

### 分享工作

>[官网文档 | 分享工作](https://learning.postman.com/docs/collaborating-in-postman/sharing)

### 专用API网络

>[官网文档 | 专用API网络](https://learning.postman.com/docs/collaborating-in-postman/adding-private-network)

### 评论收藏

>[官网文档 | 评论收藏](https://learning.postman.com/docs/collaborating-in-postman/commenting-on-collections)

### 版本控制API

>[官网文档 | 版本控制API](https://learning.postman.com/docs/collaborating-in-postman/versioning-an-api)

### 使用版本控制

>[官网文档 | 使用版本控制](https://learning.postman.com/docs/collaborating-in-postman/version-control-for-collections)

## 使用工作区

### 创建工作区

>[官网文档 | 创建工作区](https://learning.postman.com/docs/collaborating-in-postman/using-workspaces/creating-workspaces)

### 使用和管理工作区

>[官网文档 | 使用和管理工作区](https://learning.postman.com/docs/collaborating-in-postman/using-workspaces/managing-workspaces)

### 查看工作空间活动

>[官网文档 | 查看工作空间活动](https://learning.postman.com/docs/collaborating-in-postman/using-workspaces/changelog-and-restoring-collections)

### 解决团队冲突

>[官网文档 | 解决团队冲突](https://learning.postman.com/docs/collaborating-in-postman/using-workspaces/conflicts)

## 设计和开发API
### 使用API Builder

>[官网文档 | 使用API Builder](https://learning.postman.com/docs/designing-and-developing-your-api/the-api-workflow)

### 管理和共享API

>[官网文档 | 管理和共享API](https://learning.postman.com/docs/designing-and-developing-your-api/managing-apis)

### 验证API

>[官网文档 | 验证API](https://learning.postman.com/docs/designing-and-developing-your-api/validating-elements-against-schema)

## 模拟数据
### 设置模拟服务器

>[官网文档 | 设置模拟服务器](https://learning.postman.com/docs/designing-and-developing-your-api/mocking-data/setting-up-mock)

### 模拟例子

>[官网文档 | 模拟例子](https://learning.postman.com/docs/designing-and-developing-your-api/mocking-data/mocking-with-examples)

### 通过API模拟

>[官网文档 | 通过API模拟](https://learning.postman.com/docs/designing-and-developing-your-api/mocking-data/mock-with-api)

### 了解示例匹配 

>[官网文档 | 了解示例匹配 ](https://learning.postman.com/docs/designing-and-developing-your-api/mocking-data/matching-algorithm)

## 监控API
### 监控API

>[官网文档 | 监控API](https://learning.postman.com/docs/designing-and-developing-your-api/monitoring-your-api/intro-monitors)

### 设置显示器

>[官网文档 | 设置显示器](https://learning.postman.com/docs/designing-and-developing-your-api/monitoring-your-api/setting-up-monitor)

### 查看监控结果

>[官网文档 | 查看监控结果](https://learning.postman.com/docs/designing-and-developing-your-api/monitoring-your-api/viewing-monitor-results)

### 监控API和网站

>[官网文档 | 监控API和网站](https://learning.postman.com/docs/designing-and-developing-your-api/monitoring-your-api/monitoring-apis-websites)

### 设置集成以接收警报

>[官网文档 | 设置集成以接收警报](https://learning.postman.com/docs/designing-and-developing-your-api/monitoring-your-api/integrations-for-alerts)

### 使用静态IP运行Postman监视器

>[官网文档 | 使用静态IP运行Postman监视器](https://learning.postman.com/docs/designing-and-developing-your-api/monitoring-your-api/using-static-IPs-to-monitor)

### 排除显示器故障

>[官网文档 | 排除显示器故障](https://learning.postman.com/docs/designing-and-developing-your-api/monitoring-your-api/troubleshooting-monitors)

### 监控常见问题 

>[官网文档 | 监控常见问题 ](https://learning.postman.com/docs/designing-and-developing-your-api/monitoring-your-api/faqs-monitors)

### 分析报告

>[官网文档 | 分析报告](https://learning.postman.com/docs/designing-and-developing-your-api/view-and-analyze-api-reports)

## 发布API

### 记录API

>[官网文档 | 记录API](https://learning.postman.com/docs/publishing-your-api/documenting-your-api)

### 撰写文件

>[官网文档 | 撰写文件](https://learning.postman.com/docs/publishing-your-api/authoring-your-documentation)

### 发布文档

>[官网文档 | 发布文档](https://learning.postman.com/docs/publishing-your-api/publishing-your-docs)

### 查看文件

>[官网文档 | 查看文件](https://learning.postman.com/docs/publishing-your-api/viewing-documentation)

### 使用自定义域

>[官网文档 | 使用自定义域](https://learning.postman.com/docs/publishing-your-api/custom-doc-domains)

### 发布模板

>[官网文档 | 发布模板](https://learning.postman.com/docs/publishing-your-api/add-templates)

### 发布到API网络

>[官网文档 | 发布到API网络](https://learning.postman.com/docs/publishing-your-api/add-api-network)

### 提交准则

>[官网文档 | 提交准则](https://learning.postman.com/docs/publishing-your-api/api-submission-guidelines)

## 在Postman中运行
### 使用在Postman中运行按钮

>[官网文档 | 使用在Postman中运行按钮](https://learning.postman.com/docs/publishing-your-api/run-in-postman/introduction-run-button)

### 创建在Postman中运行按钮

>[官网文档 | 创建在Postman中运行按钮](https://learning.postman.com/docs/publishing-your-api/run-in-postman/creating-run-button)

### 在Postman中运行编码

>[官网文档 | 在Postman中运行编码](https://learning.postman.com/docs/publishing-your-api/run-in-postman/run-button-API)

## 行政

### 管理团队

>[官网文档 | 管理团队](https://learning.postman.com/docs/administration/managing-your-team)

### 采购Postman

>[官网文档 | 采购Postman](https://learning.postman.com/docs/administration/buying)

### 开票

>[官网文档 | 开票](https://learning.postman.com/docs/administration/billing)

### 配置团队设置

>[官网文档 | 配置团队设置](https://learning.postman.com/docs/administration/team-settings)

### 利用审核日志

>[官网文档 | 利用审核日志](https://learning.postman.com/docs/administration/audit-logs)

### 入职清单

>[官网文档 | 入职清单](https://learning.postman.com/docs/administration/onboarding-checklist)

## 单点登录（SSO）

### SSO简介

>[官网文档 | SSO简介](https://learning.postman.com/docs/administration/sso/intro-sso)

### 为团队配置SSO

>[官网文档 | 为团队配置SSO](https://learning.postman.com/docs/administration/sso/admin-sso)

### 登录到SSO团队

>[官网文档 | 登录到SSO团队](https://learning.postman.com/docs/administration/sso/user-sso)

### Microsoft AD FS

>[官网文档 | Microsoft AD FS](https://learning.postman.com/docs/administration/sso/microsoft-adfs)

### Azure AD中的自定义SAML

>[官网文档 | Azure AD中的自定义SAML](https://learning.postman.com/docs/administration/sso/saml-in-azure-ad)

### Duo中的自定义SAML

>[官网文档 | Duo中的自定义SAML](https://learning.postman.com/docs/administration/sso/saml-duo)

### GSuite中的自定义SAML

>[官网文档 | GSuite中的自定义SAML](https://learning.postman.com/docs/administration/sso/saml-gsuite)

### Okta中的自定义SAML

>[官网文档 | Okta中的自定义SAML](https://learning.postman.com/docs/administration/sso/saml-okta)

### Onelogin中的自定义SAML

>[官网文档 | Onelogin中的自定义SAML](https://learning.postman.com/docs/administration/sso/saml-onelogin)

### Ping身份中的自定义SAML 

>[官网文档 | Ping身份中的自定义SAML ](https://learning.postman.com/docs/administration/sso/saml-ping)

### 迁移到最新版本的Postman 

>[官网文档 | 迁移到最新版本的Postman ](https://learning.postman.com/docs/administration/migrating-to-v7)

## 开发人员资源

### 使用Postman实用程序进行开发

>[官网文档 | 使用Postman实用程序进行开发](https://learning.postman.com/docs/developer/resources-intro)

### PostmanAPI

>[官网文档 | PostmanAPI](https://learning.postman.com/docs/developer/intro-api)

### 回声API

>[官网文档 | 回声API](https://learning.postman.com/docs/developer/echo-api)

### 集合SDK

>[官网文档 | 集合SDK](https://learning.postman.com/docs/developer/collection-sdk)

### Postman运行时库

>[官网文档 | Postman运行时库](https://learning.postman.com/docs/developer/runtime-library)

### 代码生成器库

>[官网文档 | 代码生成器库](https://learning.postman.com/docs/developer/code-generators)

### Postman收藏转换

>[官网文档 | Postman收藏转换](https://learning.postman.com/docs/developer/collection-conversion)

## 整合

### 与Postman整合

>[官网文档 | 与Postman整合](https://learning.postman.com/docs/integrations/intro-integrations)

### 自定义Webhook

>[官网文档 | 自定义Webhook](https://learning.postman.com/docs/integrations/webhooks)

## 可用的集成

### APIMatic

>[官网文档 | APIMatic](https://learning.postman.com/docs/integrations/available-integrations/apimatic)

### AWS API网关

>[官网文档 | AWS API网关](https://learning.postman.com/docs/integrations/available-integrations/aws-api-gateway)

### 大熊猫

>[官网文档 | 大熊猫](https://learning.postman.com/docs/integrations/available-integrations/bigpanda)

### 比特桶

>[官网文档 | 比特桶](https://learning.postman.com/docs/integrations/available-integrations/bitbucket)

### Coralogix

>[官网文档 | Coralogix](https://learning.postman.com/docs/integrations/available-integrations/coralogix)

### 数据狗

>[官网文档 | 数据狗](https://learning.postman.com/docs/integrations/available-integrations/datadog)

### 投寄箱

>[官网文档 | 投寄箱](https://learning.postman.com/docs/integrations/available-integrations/dropbox)

### 的GitHub

>[官网文档 | 的GitHub](https://learning.postman.com/docs/integrations/available-integrations/github)

### 亚搏体育app

>[官网文档 | 亚搏体育app](https://learning.postman.com/docs/integrations/available-integrations/gitlab)

### 嘻哈

>[官网文档 | 嘻哈](https://learning.postman.com/docs/integrations/available-integrations/hipchat)

### 敏锐

>[官网文档 | 敏锐](https://learning.postman.com/docs/integrations/available-integrations/keen)

### 微软流程

>[官网文档 | 微软流程](https://learning.postman.com/docs/integrations/available-integrations/microsoft-flow)

### 微软团队

>[官网文档 | 微软团队](https://learning.postman.com/docs/integrations/available-integrations/microsoft-teams)

### OpenAPI的

>[官网文档 | OpenAPI的](https://learning.postman.com/docs/integrations/available-integrations/working-with-openAPI)

### PagerDuty

>[官网文档 | PagerDuty](https://learning.postman.com/docs/integrations/available-integrations/pagerduty)

### 松弛

>[官网文档 | 松弛](https://learning.postman.com/docs/integrations/available-integrations/slack)

### 维克多

>[官网文档 | 维克多](https://learning.postman.com/docs/integrations/available-integrations/victorops)



# 参考资料

> [Postman | getting-started](https://learning.postman.comhttps://learning.postman.com/docs/getting-started/introduction/)