<!-- TOC -->

- [常用快捷键](#常用快捷键)
  - [光标移动](#光标移动)
  - [编辑](#编辑)
    - [批量替换](#批量替换)
  - [保存](#保存)
  - [复制](#复制)
  - [撤销](#撤销)
  - [其他](#其他)
  - [常用组合操作](#常用组合操作)
- [常用命令](#常用命令)
- [常用插件](#常用插件)
  - [YouCompleteMe](#youcompleteme)
    - [安装依赖项](#安装依赖项)
    - [编译](#编译)
    - [The ycmd server SHUT DOWN...](#the-ycmd-server-shut-down)
    - [NERDTree](#nerdtree)
      - [快捷键](#快捷键)
- [插件管理](#插件管理)
  - [Vim-plug](#vim-plug)
    - [安装Vim-plug](#安装vim-plug)
    - [声明插件](#声明插件)
    - [重新加载配置文件](#重新加载配置文件)
    - [检查插件状态](#检查插件状态)
    - [安装插件](#安装插件)
- [参考资料](#参考资料)

<!-- /TOC -->
# 常用快捷键

## 光标移动

快捷键|作用
-|-
0|数字零,到行头
$|到本行行尾
^|到本行第一个不是空格的位置
g_|到本行最后一个不是blank字符的位置
nG|移动到第n行
gg|移动到首行
GG|移动到末行

## 编辑

快捷键|作用
-|-
a|在光标后插入
I|在行首插入
o|在当前行后插入一个新行
O|在当前行前插入一个新行
\<C-n>|插入模式下代码补全
dt"|删除内容直到遇见"
yt,|同理复制内容直到遇见,
x|删除光标所在的字符(单字符删除)
dw|删除光标位置到单词结尾
vf%|光标处选中直到遇见%

### 批量替换

> addrs/原字符串/目的字符串/option

参数|含义
:-:|:-
addr|表示检索范围,省略时表示当前行<br>"1,20" :表示从第1行到20行;<br>"%" :表示整个文件,同"1,$"<br>". ,$" :从当前行到文件尾
s|表示替换操作
option|表示操作类型<br>g 表示全局替换 <br>c 表示进行确认<br>p 表示替代结果逐行显示(Ctrl + L恢复屏幕)<br>省略option时仅对每行第一个匹配串进行替换<br>如果在原字符串和目的字符串中出现特殊字符,需要用"\"转义

`%s/'\/user\/home'/'/home/mike'/g |在文件全局将/user/home替换为home/mike

## 保存

快捷键|作用
-|-
w|保存文件
w path/fine_name|将文件另存为指定路径下的指定文件名
wq|保存并退出
q!|不保存当前修改,强制关闭文件
ZZ|根据文件状态判断是否需要保存并关闭文件

## 复制

快捷键|作用
-|-
ve|选取当前单词
"+y|复制到系统剪贴板
v (光标移动)|选中文本
yy|复制当前行
np|将复制内容粘贴n次(一次n可以省略)
d|将选中内容进行剪切
dd|将当前光标所在行进行剪切

## 撤销

快捷键|作用
-|-
u|撤销上一步操作
\<C-r>|恢复上一步被撤销的操作

## 其他

快捷键|作用
-|-
zo|展开折叠,只展开最外层的折叠.
zO|对所在范围内所有嵌套的折叠点展开,包括嵌套折叠
tabnew 路径|在新table打开路径指向的文件
s|替换光标下字符
tabc|关闭当前table
gt|多窗口间切换/usr/bin/apt-add-repository
vsplit|纵向分割窗口
split|横向分割窗口
g/^s*$/d|删除文本中的空白行
tabo|关闭除当前tab外其他所有tab
shift tab|在多个tab中快速切换
\<C-p>|模糊搜索文件


## 常用组合操作

0 \<C-v> (光标移动) I(要插入的内容) [ESC]`  → 对多行执行同一操作,常用于多行注释

# 常用命令

命令|作用
-|-
set number|设置行号

# 常用插件

## YouCompleteMe

> YouCompleteMe是一款自动补全插件.



### 安装依赖项

```bash
sudo apt-get install -y build-essential cmake python-dev python3-dev
```

### 编译

使用vim-plug下载的YouCompleteMe源码保存在目录~/.vim/plugged/YouCompleteMe,在该目录下执行: 

```bash
~/.vim/plugged/YouCompleteMe/./install.py --clang-completer
```

### The ycmd server SHUT DOWN...

> The ycmd server SHUT DOWN (restart with ‘:YcmRestartServer’). YCM core library not detected; you need to compile YCM before using it. Follow the instructions in the documentation.

这个问题多数是由于插件没有安装或更新后没有编译引起,重新编译即可.

### NERDTree

> 文件夹树形结构展示.

#### 快捷键

快捷键|作用
-|-
ctrl + w + h|光标 focus 左侧树形目录
ctrl + w + l|光标 focus 右侧文件显示窗口
ctrl + w + w|光标自动在左右侧窗口切换
ctrl + w + r|移动当前窗口的布局位置
o|在已有窗口中打开文件、目录或书签,并跳到该窗口
go|在已有窗口中打开文件、目录或书签,但不跳到该窗口
t|在新 Tab 中打开选中文件/书签,并跳到新 Tab
T|在新 Tab 中打开选中文件/书签,但不跳到新 Tab
i|split 一个新窗口打开选中文件,并跳到该窗口
gi|split 一个新窗口打开选中文件,但不跳到该窗口
s|vsplit 一个新窗口打开选中文件,并跳到该窗口
gs|vsplit 一个新 窗口打开选中文件,但不跳到该窗口
gT|前一个 tab
gt|后一个 tab
r|递归刷新选中目录
R|递归刷新根结点
C|将选中目录或选中文件的父目录设为根结点
u|将当前根结点的父目录设为根目录,并变成合拢原根结点
U|将当前根结点的父目录设为根目录,但保持展开原根结点
B|切换是否显示书签
D|删除当前书签
F5|打开/关闭NerdTree 窗口<br>vimrc里面配置:<br>map \<F5> :NERDTreeMirror\<CR>

# 插件管理

## Vim-plug

### 安装Vim-plug

```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

### 声明插件

要安装插件,必须在Vim配置文件中声明它们(~/.vimrc)

```vim
"列表以下面语句开始 
call plug#begin(PLUGIN_DIRECTORY) 

" 声明具体插件
Plug 'junegunn/vim-easy-align'

" 以下面结束 
plug#end()
```

### 重新加载配置文件

在 vim 配置文件中添加内容后,通过输入以下命令重新加载:

```vim
:source ~/.vimrc
```

或者,只需重新加载 Vim 编辑器.

### 检查插件状态

使用以下命令检查状态:

```vim
:PlugStatus
```

### 安装插件

然输入下面的命令,然后按回车键安装之前在配置文件中声明的插件:

```vim 
:PlugInstall
```

# 参考资料

> [酷壳 | VIM入门](https://coolshell.cn/articles/5426.html)

> [简书 | Markdown的基本语法](https://www.jianshu.com/p/250e36bb5690)

> [简书 | Vim-plug:极简 Vim 插件管理器](https://www.jianshu.com/p/0c83e6aed270)

> [vimjc | Vim自动补齐插件YouCompleteMe安装指南(2019年最新)-Vim插件(15)](https://vimjc.com/vim-youcompleteme-install.html)

> [邱明成 | vim 常用 NERDTree 快捷键](https://www.cnblogs.com/qiumingcheng/p/6275510.html)