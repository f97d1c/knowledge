
<!-- TOC -->

- [XXX/python3.X/site-packages/django](#xxxpython3xsite-packagesdjango)
  - [db](#db)
    - [models](#models)
      - [信号(signals)](#信号signals)
        - [模型信号](#模型信号)
          - [pre_init](#pre_init)
          - [post_init](#post_init)
          - [pre_save](#pre_save)
          - [post_save](#post_save)
          - [pre_delete](#pre_delete)
          - [post_delete](#post_delete)
          - [m2m_changed](#m2m_changed)
          - [class_prepared](#class_prepared)
        - [管理信号](#管理信号)
          - [pre_migrate](#pre_migrate)
          - [post_migrate](#post_migrate)
        - [请求／响应信号](#请求／响应信号)
          - [request_started](#request_started)
          - [request_finished](#request_finished)
          - [got_request_exception](#got_request_exception)
        - [测试信号](#测试信号)
          - [setting_changed](#setting_changed)
          - [template_rendered](#template_rendered)
        - [数据库包装器](#数据库包装器)
          - [connection_created](#connection_created)
  - [contrib](#contrib)
    - [auth](#auth)
      - [User模型](#user模型)
        - [字段](#字段)
        - [属性](#属性)
        - [方法](#方法)
        - [管理器方法](#管理器方法)
      - [AnonymousUser对象](#anonymoususer对象)
      - [Permission模型](#permission模型)
        - [字段](#字段-1)
        - [方法](#方法-1)
      - [Group模型](#group模型)
        - [字段](#字段-2)
      - [验证器](#验证器)
      - [signals](#signals)
        - [成功登录时](#成功登录时)
        - [注销](#注销)
        - [未能成功登录](#未能成功登录)
      - [认证后端](#认证后端)
      - [可用的认证后端](#可用的认证后端)
      - [实用工具函数](#实用工具函数)
        - [获取当前登录用户(get_user(request))](#获取当前登录用户get_userrequest)
    - [admin](#admin)
      - [初始化项目](#初始化项目)
      - [创建超级用户](#创建超级用户)
      - [后台管理](#后台管理)
      - [用户活动监控(LogEntries)](#用户活动监控logentries)
        - [管理权限](#管理权限)
        - [添加编辑对象的链接](#添加编辑对象的链接)
        - [日志记录](#日志记录)
- [YYY/项目根目录](#yyy项目根目录)
  - [项目主目录](#项目主目录)
    - [__init__.py](#__init__py)
    - [asgi.py](#asgipy)
    - [settings.py](#settingspy)
    - [urls.py](#urlspy)
      - [path()函数](#path函数)
      - [re_path](#re_path)
      - [正则路径中的分组](#正则路径中的分组)
        - [正则路径中的无名分组](#正则路径中的无名分组)
        - [正则路径中的有名分组](#正则路径中的有名分组)
      - [路由分发(include)](#路由分发include)
      - [反向解析](#反向解析)
        - [普通路径](#普通路径)
        - [正则路径(无名分组)](#正则路径无名分组)
        - [正则路径(有名分组)](#正则路径有名分组)
      - [命名空间](#命名空间)
        - [普通路径](#普通路径-1)
    - [wsgi.py](#wsgipy)
  - [templates](#templates)
    - [模板标签](#模板标签)
      - [注释标签](#注释标签)
      - [列表](#列表)
      - [字典](#字典)
      - [过滤器](#过滤器)
        - [大小写转换(upper/lower)](#大小写转换upperlower)
        - [长度截取(truncatewords)](#长度截取truncatewords)
        - [转义(addslashes)](#转义addslashes)
        - [时间格式化(date)](#时间格式化date)
        - [变量长度(length)](#变量长度length)
        - [默认值(default)](#默认值default)
        - [文件大小格式化(filesizeformat)](#文件大小格式化filesizeformat)
        - [省略号(truncatechars)](#省略号truncatechars)
        - [安全标记(safe)](#安全标记safe)
      - [if/else](#ifelse)
      - [for](#for)
        - [reversed](#reversed)
        - [遍历字典](#遍历字典)
        - [{{forloop}}](#forloop)
        - [empty](#empty)
      - [ifequal/ifnotequal](#ifequalifnotequal)
      - [include](#include)
      - [csrf_token](#csrf_token)
    - [模板继承](#模板继承)
      - [父模板(block...endblock)](#父模板blockendblock)
      - [子模板(extends)](#子模板extends)
  - [templatetags](#templatetags)
    - [my_tags.py](#my_tagspy)
      - [自定义过滤器(@register.filter)](#自定义过滤器registerfilter)
        - [is_sage](#is_sage)
        - [needs_autoescape](#needs_autoescape)
        - [expects_localtime](#expects_localtime)
        - [模板过滤器期望字符串](#模板过滤器期望字符串)
        - [过滤器和自动转义](#过滤器和自动转义)
      - [自定义标签(@register.simple_tag)](#自定义标签registersimple_tag)
      - [语义化标签](#语义化标签)
    - [load my_tags](#load-my_tags)
  - [statics](#statics)
    - [css](#css)
    - [js](#js)
    - [images](#images)
      - [从静态目录中引入图片(load static)](#从静态目录中引入图片load-static)
    - [plugins](#plugins)
      - [插件引用](#插件引用)
  - [manage.py](#managepy)
  - [项目模型](#项目模型)
    - [__init__.py](#__init__py-1)
    - [admin.py](#adminpy)
    - [models.py](#modelspy)
      - [字段选项](#字段选项)
        - [允许为空-数据存储(null)](#允许为空-数据存储null)
        - [允许为空-表单验证(blank)](#允许为空-表单验证blank)
        - [特定选择(choices)](#特定选择choices)
        - [数据库列名(db_column)](#数据库列名db_column)
        - [数据库索引(db_index)](#数据库索引db_index)
        - [数据库表空间(db_tablespace)](#数据库表空间db_tablespace)
        - [默认值(default)](#默认值default-1)
        - [(editable)](#editable)
        - [错误信息(error_messages)](#错误信息error_messages)
        - [*帮助*文本(help_text)](#帮助文本help_text)
        - [模型主键(primary_key)](#模型主键primary_key)
        - [是否唯一(unique)](#是否唯一unique)
        - [日期字段值唯一(unique_for_date)](#日期字段值唯一unique_for_date)
        - [月份唯一(unique_for_month)](#月份唯一unique_for_month)
        - [年度唯一(unique_for_year)](#年度唯一unique_for_year)
        - [详细字段名(verbose_name)](#详细字段名verbose_name)
        - [验证器列表(validators)](#验证器列表validators)
      - [字段类型](#字段类型)
        - [自动递增(AutoField)](#自动递增autofield)
        - [布尔值(BooleanField)](#布尔值booleanfield)
        - [字符串(CharField)](#字符串charfield)
        - [日期(DateField)](#日期datefield)
        - [日期和时间(DateTimeField)](#日期和时间datetimefield)
        - [时间(TimeField)](#时间timefield)
        - [时间段(DurationField)](#时间段durationfield)
        - [大文本(TextField)](#大文本textfield)
        - [十进制固定精度(DecimalField)](#十进制固定精度decimalfield)
        - [浮点数(FloatField)](#浮点数floatfield)
        - [唯一标识符(UUIDField)](#唯一标识符uuidfield)
        - [64位整数自动递增(BigAutoField)](#64位整数自动递增bigautofield)
        - [64位整数(BigIntegerField)](#64位整数bigintegerfield)
        - [二进制数据(BinaryField)](#二进制数据binaryfield)
        - [电子邮件地址(EmailField)](#电子邮件地址emailfield)
        - [文件存储(FileField)](#文件存储filefield)
        - [文件路径(FilePathField)](#文件路径filepathfield)
        - [图像(ImageField)](#图像imagefield)
        - [整数(IntegerField)](#整数integerfield)
        - [IPv4/IPv6地址(GenericIPAddressField)](#ipv4ipv6地址genericipaddressfield)
        - [JSON编码数据(JSONField)](#json编码数据jsonfield)
        - [(PositiveBigIntegerField)](#positivebigintegerfield)
        - [(PositiveIntegerField)](#positiveintegerfield)
        - [(PositiveSmallIntegerField)](#positivesmallintegerfield)
        - [(SlugField)](#slugfield)
        - [(SmallAutoField)](#smallautofield)
        - [(SmallIntegerField)](#smallintegerfield)
        - [链接(URLField)](#链接urlfield)
      - [关系字段](#关系字段)
        - [多对一关联(ForeignKey)](#多对一关联foreignkey)
        - [多对多关联(ManyToManyField)](#多对多关联manytomanyfield)
        - [(OneToOneField)](#onetoonefield)
      - [Field类](#field类)
        - [查找](#查找)
      - [description](#description)
      - [descriptor_class](#descriptor_class)
        - [get_internal_type()](#get_internal_type)
        - [db_type(connection)](#db_typeconnection)
        - [rel_db_type(connection)](#rel_db_typeconnection)
        - [get_prep_value(value)](#get_prep_valuevalue)
        - [get_db_prep_value(value, connection, prepared=False)](#get_db_prep_valuevalue-connection-preparedfalse)
        - [from_db_value(value, expression, connection)](#from_db_valuevalue-expression-connection)
        - [get_db_prep_save(value, connection)](#get_db_prep_savevalue-connection)
        - [pre_save(model_instance, add)](#pre_savemodel_instance-add)
        - [to_python(value)](#to_pythonvalue)
        - [value_from_object(obj)](#value_from_objectobj)
        - [value_to_string(obj)](#value_to_stringobj)
        - [formfield(form_class=None, choices_form_class=None, **kwargs)](#formfieldform_classnone-choices_form_classnone-kwargs)
        - [deconstruct()](#deconstruct)
      - [字段属性参考](#字段属性参考)
        - [字段的属性](#字段的属性)
          - [Field.auto_created](#fieldauto_created)
          - [Field.concrete](#fieldconcrete)
          - [Field.hidden](#fieldhidden)
          - [Field.is_relation](#fieldis_relation)
          - [Field.model](#fieldmodel)
        - [有关系的字段的属性](#有关系的字段的属性)
          - [Field.many_to_many](#fieldmany_to_many)
          - [Field.many_to_one](#fieldmany_to_one)
          - [Field.one_to_many](#fieldone_to_many)
          - [Field.one_to_one](#fieldone_to_one)
          - [Field.related_model](#fieldrelated_model)
      - [定义模型](#定义模型)
      - [执行查询](#执行查询)
        - [创建对象](#创建对象)
        - [将修改保存至对象](#将修改保存至对象)
          - [保存ForeignKey和ManyToManyField字段](#保存foreignkey和manytomanyfield字段)
        - [检索对象](#检索对象)
        - [检索全部对象](#检索全部对象)
        - [通过过滤器检索指定对象](#通过过滤器检索指定对象)
          - [链式过滤器](#链式过滤器)
          - [每个QuerySet都是唯一的](#每个queryset都是唯一的)
          - [QuerySet是惰性的](#queryset是惰性的)
        - [用get()检索单个对象](#用get检索单个对象)
        - [其它QuerySet方法](#其它queryset方法)
        - [限制QuerySet条目数](#限制queryset条目数)
        - [字段查询](#字段查询)
        - [跨关系查询](#跨关系查询)
          - [跨多值关联](#跨多值关联)
        - [过滤器可以为模型指定字段](#过滤器可以为模型指定字段)
        - [Expressionscanreferencetransforms](#expressionscanreferencetransforms)
        - [主键(pk)查询快捷方式](#主键pk查询快捷方式)
        - [在LIKE语句中转义百分号和下划线](#在like语句中转义百分号和下划线)
        - [缓存和QuerySet](#缓存和queryset)
          - [当QuerySet未被缓存时](#当queryset未被缓存时)
        - [查询JSONField](#查询jsonfield)
        - [保存和查询None值](#保存和查询none值)
        - [Key,index,和路径转换](#keyindex和路径转换)
        - [包含与键查找](#包含与键查找)
          - [contains](#contains)
          - [contained_by](#contained_by)
          - [has_key](#has_key)
          - [has_keys](#has_keys)
          - [has_any_keys](#has_any_keys)
        - [通过Q对象完成复杂查询](#通过q对象完成复杂查询)
        - [比较对象](#比较对象)
        - [删除对象](#删除对象)
        - [复制模型实例](#复制模型实例)
        - [一次修改多个对象](#一次修改多个对象)
        - [关联对象](#关联对象)
          - [一对多关联](#一对多关联)
          - [正向访问](#正向访问)
          - [*反向*关联](#反向关联)
          - [使用自定义反向管理器](#使用自定义反向管理器)
          - [管理关联对象的额外方法](#管理关联对象的额外方法)
          - [多对多关联](#多对多关联)
          - [一对一关联](#一对一关联)
          - [反向关联是如何实现的?](#反向关联是如何实现的)
          - [查询关联对象](#查询关联对象)
        - [回归原生SQL](#回归原生sql)
      - [获取模型字段](#获取模型字段)
      - [密码管理](#密码管理)
        - [使用Argon2](#使用argon2)
        - [使用bcrypt](#使用bcrypt)
        - [增加盐的熵值](#增加盐的熵值)
        - [增加工作因子](#增加工作因子)
          - [PBKDF2和bcrypt](#pbkdf2和bcrypt)
          - [Argon2](#argon2)
        - [密码升级](#密码升级)
          - [无需登录的密码升级](#无需登录的密码升级)
        - [手动管理用户的密码](#手动管理用户的密码)
          - [验证密码(check_password(password, encoded))](#验证密码check_passwordpassword-encoded)
          - [创建密码(make_password(password, salt=None, hasher='default'))](#创建密码make_passwordpassword-saltnone-hasherdefault)
          - [is_password_usable(encoded_password)](#is_password_usableencoded_password)
        - [密码验证](#密码验证)
          - [启用密码验证](#启用密码验证)
          - [已包含的验证器](#已包含的验证器)
          - [集成检查](#集成检查)
          - [编写自定义的验证器](#编写自定义的验证器)
    - [tests.py](#testspy)
    - [views.py](#viewspy)
      - [QueryDict对象](#querydict对象)
        - [__getitem__](#__getitem__)
        - [__setitem__](#__setitem__)
        - [get()](#get)
        - [update()](#update)
        - [items()](#items)
        - [values()](#values)
        - [copy()](#copy)
        - [getlist(key)](#getlistkey)
        - [setlist(key,list_)](#setlistkeylist_)
        - [appendlist(key,item)](#appendlistkeyitem)
        - [setlistdefault(key,list)](#setlistdefaultkeylist)
        - [lists()](#lists)
        - [urlencode()](#urlencode)
      - [Request对象](#request对象)
        - [path](#path)
        - [method](#method)
        - [GET](#get)
        - [POST](#post)
        - [REQUEST](#request)
        - [COOKIES](#cookies)
        - [FILES](#files)
        - [META](#meta)
        - [user](#user)
        - [session](#session)
        - [raw_post_data](#raw_post_data)
        - [__getitem__(key)](#__getitem__key)
        - [has_key()](#has_key)
        - [get_full_path()](#get_full_path)
        - [is_secure()](#is_secure)
      - [HttpResponse对象](#httpresponse对象)
        - [render()](#render)
        - [redirect()](#redirect)
      - [通用显示视图-列表(ListView)](#通用显示视图-列表listview)
      - [通用显示视图-详情(DetailView)](#通用显示视图-详情detailview)
    - [forms.py](#formspy)
      - [核心字段参数](#核心字段参数)
        - [required](#required)
        - [label](#label)
        - [label_suffix](#label_suffix)
        - [initial](#initial)
        - [widget](#widget)
        - [help_text](#help_text)
        - [error_messages](#error_messages)
        - [validators](#validators)
        - [localize](#localize)
        - [disabled](#disabled)
      - [检查字段数据是否有变化](#检查字段数据是否有变化)
        - [has_changed()](#has_changed)
      - [内置Field类](#内置field类)
        - [BooleanField](#booleanfield)
        - [CharField](#charfield)
        - [ChoiceField](#choicefield)
        - [TypedChoiceField](#typedchoicefield)
        - [DateField](#datefield)
        - [DateTimeField](#datetimefield)
        - [DecimalField](#decimalfield)
        - [DurationField](#durationfield)
        - [EmailField](#emailfield)
        - [FileField](#filefield)
        - [FilePathField](#filepathfield)
        - [FloatField](#floatfield)
        - [ImageField](#imagefield)
        - [IntegerField](#integerfield)
        - [JSONField](#jsonfield)
        - [GenericIPAddressField](#genericipaddressfield)
        - [MultipleChoiceField](#multiplechoicefield)
        - [TypedMultipleChoiceField](#typedmultiplechoicefield)
        - [NullBooleanField](#nullbooleanfield)
        - [RegexField](#regexfield)
        - [SlugField](#slugfield)
        - [TimeField](#timefield)
        - [URLField](#urlfield)
        - [UUIDField](#uuidfield)
      - [稍复杂的内置Field类](#稍复杂的内置field类)
        - [ComboField](#combofield)
        - [MultiValueField](#multivaluefield)
        - [SplitDateTimeField](#splitdatetimefield)
      - [处理关系的字段](#处理关系的字段)
        - [ModelChoiceField](#modelchoicefield)
        - [ModelMultipleChoiceField](#modelmultiplechoicefield)
        - [迭代关系选择](#迭代关系选择)
          - [ModelChoiceIterator](#modelchoiceiterator)
          - [ModelChoiceIteratorValue](#modelchoiceiteratorvalue)
        - [创建自定义字段](#创建自定义字段)
      - [ModelForm](#modelform)
        - [字段类型](#字段类型-1)
        - [验证ModelForm](#验证modelform)
          - [覆盖clean()方法](#覆盖clean方法)
          - [与模型验证交互](#与模型验证交互)
          - [有关模型的error_messages的注意事项](#有关模型的error_messages的注意事项)
        - [save()方法](#save方法)
        - [选择要使用的字段](#选择要使用的字段)
        - [覆盖默认字段](#覆盖默认字段)
        - [启用对字段的本地化](#启用对字段的本地化)
        - [表单继承](#表单继承)
        - [提供初始值](#提供初始值)
        - [ModelForm的工厂函数](#modelform的工厂函数)
      - [模型表单集](#模型表单集)
        - [更改查询集](#更改查询集)
        - [更改表单](#更改表单)
        - [在表单中使用widgets指定部件.](#在表单中使用widgets指定部件)
        - [使用localized_fields来启用字段本地化](#使用localized_fields来启用字段本地化)
        - [提供初始值](#提供初始值-1)
        - [在表单集中保存对象](#在表单集中保存对象)
        - [限制可编辑对象的数量](#限制可编辑对象的数量)
        - [在视图中使用模型表单集](#在视图中使用模型表单集)
        - [覆盖ModelFormSet上的clean()</span></code>](#覆盖modelformset上的cleanspancode)
        - [使用自定义查询结果集](#使用自定义查询结果集)
        - [在模板中使用表单集](#在模板中使用表单集)
      - [内联表单集](#内联表单集)
        - [覆盖InlineFormSet上的方法](#覆盖inlineformset上的方法)
        - [同一个模型有多个外键](#同一个模型有多个外键)
        - [在视图中使用内联表单集](#在视图中使用内联表单集)
        - [指定要在内联表单中使用的widgets](#指定要在内联表单中使用的widgets)
  - [locale](#locale)
    - [翻译](#翻译)
      - [概况](#概况)
      - [在Python代码中进行国际化](#在python代码中进行国际化)
        - [标准翻译](#标准翻译)
        - [为翻译者提供注释](#为翻译者提供注释)
        - [标记不用翻译的字符](#标记不用翻译的字符)
        - [多元化](#多元化)
        - [上下文标记](#上下文标记)
        - [惰性翻译](#惰性翻译)
          - [模型字段和相关的verbose_name与help_text选项值](#模型字段和相关的verbose_name与help_text选项值)
          - [模型详细名称的值](#模型详细名称的值)
          - [Modelmethodsdescriptionargumenttothe@displaydecorator](#modelmethodsdescriptionargumenttothedisplaydecorator)
      - [使用惰性翻译对象](#使用惰性翻译对象)
        - [惰性翻译与复数](#惰性翻译与复数)
        - [格式化字符串:format_lazy()](#格式化字符串format_lazy)
        - [延迟翻译中惰性(lazy)的其用法](#延迟翻译中惰性lazy的其用法)
      - [语言的本地化名称](#语言的本地化名称)
      - [在模板代码中国际化](#在模板代码中国际化)
        - [translate模板标签](#translate模板标签)
        - [blocktranslate模板标签](#blocktranslate模板标签)
        - [传递字符串给标签和过滤器](#传递字符串给标签和过滤器)
        - [模板内对翻译的注释](#模板内对翻译的注释)
        - [在模板中选择语言](#在模板中选择语言)
        - [其标签](#其标签)
          - [get_available_languages](#get_available_languages)
          - [get_current_language](#get_current_language)
          - [get_current_language_bidi](#get_current_language_bidi)
          - [i18n上下文处理器](#i18n上下文处理器)
          - [get_language_info](#get_language_info)
          - [get_language_info_list](#get_language_info_list)
          - [模板过滤器](#模板过滤器)
      - [国际化:在JavaScript代码里](#国际化在javascript代码里)
        - [JavaScriptCatalog视图](#javascriptcatalog视图)
        - [使用JavaScript翻译目录](#使用javascript翻译目录)
          - [gettext](#gettext)
          - [ngettext](#ngettext)
          - [interpolate](#interpolate)
          - [get_format](#get_format)
          - [gettext_noop](#gettext_noop)
          - [pgettext](#pgettext)
          - [npgettext](#npgettext)
          - [pluralidx](#pluralidx)
        - [JSONCatalog视图](#jsoncatalog视图)
        - [性能说明](#性能说明)
      - [国际化:在URL模式中](#国际化在url模式中)
        - [URL模式中的语言前缀](#url模式中的语言前缀)
        - [翻译URL模式](#翻译url模式)
        - [在模板中反向解析URL](#在模板中反向解析url)
      - [本地化:如何创建语言文件](#本地化如何创建语言文件)
        - [消息文件](#消息文件)
        - [编译消息文件](#编译消息文件)
        - [疑难解答:gettext()在带有百分号的字符串中错误地检测python-format](#疑难解答gettext在带有百分号的字符串中错误地检测python-format)
        - [从JavaScript源码中创建消息文件](#从javascript源码中创建消息文件)
        - [Windows上的gettext](#windows上的gettext)
        - [自定义makemessages命令](#自定义makemessages命令)
      - [杂项](#杂项)
        - [set_language重定向试图](#set_language重定向试图)
        - [显式设置语言](#显式设置语言)
        - [使用视图和模板外的翻译](#使用视图和模板外的翻译)
        - [Languagecookie](#languagecookie)
      - [实施说明](#实施说明)
        - [Django翻译的特性](#django翻译的特性)
        - [Django如何发现语言偏好](#django如何发现语言偏好)
        - [Django如何发现翻译](#django如何发现翻译)
        - [使用非英语基础语言](#使用非英语基础语言)
- [其他](#其他)
  - [断点](#断点)
- [参考资料](#参考资料)

<!-- /TOC -->

# XXX/python3.X/site-packages/django

## db
### models
#### 信号(signals)

##### 模型信号

###### pre_init



###### post_init



###### pre_save



###### post_save



###### pre_delete



###### post_delete



###### m2m_changed



###### class_prepared



##### 管理信号

###### pre_migrate



###### post_migrate



##### 请求／响应信号

###### request_started



###### request_finished



###### got_request_exception



##### 测试信号

###### setting_changed



###### template_rendered



##### 数据库包装器

###### connection_created


## contrib

django.contrib是一套庞大的功能集,它是Django基本代码的组成部分.

### auth



```py

```

#### User模型



```py

```

##### 字段



```py

```

##### 属性



```py

```

##### 方法



```py

```

##### 管理器方法



```py

```

#### AnonymousUser对象



```py

```

#### Permission模型



```py

```

##### 字段



```py

```

##### 方法



```py

```

#### Group模型



```py

```

##### 字段



```py

```

#### 验证器



```py

```

#### signals

##### 成功登录时

```py
user_logged_in()
```

用此信号发送的参数:

0. sender: 刚刚登录的用户的类.
0. request: 当前的HttpRequest实例.
0. user: 刚刚登录的用户的实例.

```py
from django.contrib.auth.signals import user_logged_in

user_logged_in.send(sender=user.__class__, request=request, user=user)
```

该模型必须实现last_login字段:

```py
last_login = models.DateTimeField(default=datetime.now())
```


##### 注销

```py
user_logged_out()
```

0. sender: 刚刚注销的用户的类,如果用户没有经过认证,则为None.
0. request: 当前的HttpRequest实例.
0. user: 刚刚注销的用户实例,如果用户没有经过认证,则为None.

该方法本地无效,下面可以:

```py
from django.contrib.auth import logout
logout(request)
```

##### 未能成功登录

```py
user_login_failed()
```

0. sender: 用于认证的模块名称.
0. credentials: 一个包含关键字参数的字典,其中包含传递给authenticate() 或自己的自定义认证后端的用户凭证.<br>
匹配一组 *敏感的* 模式的凭证(包括密码)将不会作为信号的一部分被发送.
0. request:HttpRequest对象,如果有提供给authenticate() 对象的话.

#### 认证后端



```py

```

#### 可用的认证后端



```py

```

#### 实用工具函数

##### 获取当前登录用户(get_user(request))

返回与给定request的会话相关联的用户模型实例.

它检查会话中存储的认证后端是否存在于AUTHENTICATION_BACKENDS中.<br>
如果存在,它使用后端的get_user() 方法检索用户模型实例,然后通过调用用户模型的get_session_auth_hash() 方法来认证会话.

如果存储在会话中的认证后端不再在AUTHENTICATION_BACKENDS中,如果后端的get_user() 方法没有返回用户,或者会话认证的哈希没有认证,则返回一个AnonymousUser的实例.

```py

```


### admin

#### 初始化项目

```sh
pip install Django==3.2.9
django-adminstartproject项目名称
```

#### 创建超级用户

```py
python manage.py createsuperuser
Username (leave blank to use 'root'): admin
Email address: admin@runoob.com
Password:
Password (again):
Superuser created successfully.
```

#### 后台管理

Django提供了基于web的管理工具.

Django自动管理工具是django.contrib的一部分.<br>
可以在项目的settings.py中的INSTALLED_APPS看到它:

```py
# /项目根目录/项目主目录/settings.py
# ...
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)
```

启动开发服务器,然后在浏览器中访问http://127.0.0.1:8000/admin/ 进行登录.

#### 用户活动监控(LogEntries)

每当用户在Djangoadmin中添加、删除甚至更改对象时,该操作都会使用名为LogEntry的模型记录在名为django_admin_log的数据库中的表中.<br>
LogEntry的模型非常简单,它包括以下字段:action_time、user、object_id、action_flag和change_message,其中包括已更改字段的名称.

现在模型已经由标准管理Django应用程序提供给,当使用django-admin startprojet命令启动Django项目时,它会自动初始化.<br>
所以不需要对模型做任何改变,要做的只是在任何admin.py文件中注册LogEntery模型,就像注册任何其模型一样.

现在模型已经由标准管理Django应用程序提供给,当使用django-admin startprojet命令启动Django项目时,它会自动初始化.<br>
所以不需要对模型做任何改变,要做的只是在任何admin.py文件中注册LogEntery模型,就像注册任何其模型一样.

```py
from django.contrib.admin.models import LogEntry

@admin.register(LogEntry)
class LogEntryAdmin(admin.ModelAdmin):
    # to have a date-based drilldown navigation in the admin page
    date_hierarchy = 'action_time'

    # to filter the resultes by users, content types and action flags
    list_filter = [
        'user',
        'content_type',
        'action_flag'
    ]

    # when searching the user will be able to search in both object_repr and change_message
    search_fields = [
        'object_repr',
        'change_message'
    ]

    list_display = [
        'action_time',
        'user',
        'content_type',
        'action_flag',
    ]
```

##### 管理权限

日志条目设置某种权限系统非常重要.<br>
在现在任何员工用户都可以查看、更改甚至删除任何日志条目的状态下,这是危险的.<br>
所以应该为LogEntryAdmin类设置4个权限:has_add_permission、has_change_permission、has_delete_permission和has_view_permission.
对于前三个,认为应用程序中的任何人都不应该拥有这样的权限,因为添加、删除或更改日志条目可能对数据库非常有害,因此应该将其设置为始终返回False.
至于第四个,查看权限,看个人喜好了.<br>
喜欢只为超级用户保留查看日志条目的权限.<br>
但是可以自由更改它以返回想要的任何内容.<br>
可以通过覆盖LogEntryAdmin类中的以下方法来设置权限:

```py
def has_add_permission(self, request):
    return False
def has_change_permission(self, request, obj=None):
    return False
def has_delete_permission(self, request, obj=None):
    return False
def has_view_permission(self, request, obj=None):
    return request.user.is_superuser
```

##### 添加编辑对象的链接

可以添加到此功能的另一个巧妙之处是能够通过单击其链接来打开编辑的对象.<br>
这可以通过使用Django的urls.reverse函数来完成,方法是将它添加为LogEntryAdmin类的方法:

```py
from django.contrib.admin.models import DELETION
from django.utils.html import escape
from django.urls import reverse
from django.utils.safestring import mark_safe

def object_link(self, obj):
  if obj.action_flag == DELETION:
    link = escape(obj.object_repr)
  else:
    ct = obj.content_type
    link = '<a href="%s">%s</a>' % (
                reverse('admin:%s_%s_change' % (ct.app_label, ct.model), args=[obj.object_id]),
                escape(obj.object_repr),
            )
  return mark_safe(link)
```

##### 日志记录

```py
LogEntry.objects.log_action(
    user_id         = request.user.pk,
    content_type_id = ContentType.objects.get_for_model(object).pk,
    object_id       = object.pk,
    object_repr     = object,
    action_flag     = ADDITION
)
```

参数名|含义
-|-
user_id|当前用户id.
content_type_id|要保存内容的类型,上面的代码中使用的是get_.content_type_for_model方法拿到对应Model的类型id.<br>这可以简单理解为ContentType为每个Model定义了一个类型id.
object_id|记录变更实例的id,比如PostAdmin中它就是post. id.
object_repr|实例的展示名称,可以简单理解为定义的__str__所返回的内容.
action_flag|操作标记.<br>admin的Model里面定义了几种基础的标记: ADDITION、CHANGE和DELETION.<br>它用来标记当前参数是数据变更、新增,还是删除.
change_message|这是记录的消息,可以自行定义.<br>可以把新添加的内容放进去(必要时可以通过这里来恢复),也可以把新旧内容的区别放进去.


# YYY/项目根目录

## 项目主目录

### __init__.py

一个空文件,告诉Python该目录是一个Python包.

### asgi.py

一个ASGI兼容的Web服务器的入口,以便运行项目.

### settings.py

该Django项目的设置/配置.

### urls.py

该Django项目的URL声明; 一份由Django驱动的网站 *目录* .

路由简单的来说就是根据用户请求的URL链接来判断对应的处理程序,并返回处理结果,也就是URL与Django的视图建立映射关系.

Django路由在urls.py配置,urls.py中的每一条配置对应相应的处理方法.


#### path()函数

用于普通路径,不需要自己手动添加正则首位限制符号,底层已经添加.

path() 可以接收四个参数,分别是两个必选参数:route、view和两个可选参数:kwargs、name.

语法格式:

```py
path(route, view, kwargs=None, name=None)
```

0. route: 字符串,表示URL规则,与之匹配的URL会执行对应的第二个参数view.
0. view: 用于执行与正则表达式匹配的URL请求.
0. kwargs: 视图使用的字典类型的参数.
0. name: 用来反向获取URL.

#### re_path

用于正则路径,需要自己手动添加正则首位限制符号.

```py
from django.urls import re_path # 用re_path需要引入
urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/', views.index), # 普通路径
    re_path(r'^articles/([0-9]{4})/$', views.articles), # 正则路径
]
```

#### 正则路径中的分组

##### 正则路径中的无名分组

无名分组按位置传参,一一对应.

views中除了request,其形参的数量要与urls中的分组数量一致.

```py
urlpatterns = [
    re_path("^index/([0-9]{4})/$", views.index),
]
```

```py
def index(request,year):
    print(year) # 一个形参代表路径中一个分组的内容,按顺序匹配
    return HttpResponse('菜鸟教程')
```

##### 正则路径中的有名分组

```reg
(?P<组名>正则表达式)
```

有名分组按关键字传参,与位置顺序无关.

views中除了request,其形参的数量要与urls中的分组数量一致, 并且views中的形参名称要与urls中的组名对应.

```py
urlpatterns = [
    re_path("^index/(?P[0-9]{4})/(?P[0-9]{2})/$", views.index),
]
```

```py
def index(request, year, month):
    print(year,month) # 一个形参代表路径中一个分组的内容,按关键字对应匹配
    return HttpResponse('菜鸟教程')
```

#### 路由分发(include)

Django项目里多个app目录共用一个urls容易造成混淆,后期维护也不方便.<br>
使用路由分发(include),让每个app目录都单独拥有自己的urls.

0. 在每个app目录里都创建一个urls.py文件.
0. 在项目名称目录下的urls文件里,统一将路径分发给各个app目录.

```py
from django.contrib import admin
from django.urls import path,include # 从django.urls引入include
urlpatterns = [
    path('admin/', admin.site.urls),
    path("app01/", include("app01.urls")),
    path("app02/", include("app02.urls")),
]
```

```py
# app01/urls.py
from django.urls import path,re_path
from app01 import views # 从自己的app目录引入views
urlpatterns = [
    re_path(r'^login/(?P<m>[0-9]{2})/$', views.index, ),
]
```

#### 反向解析

随着功能的增加,路由层的url发生变化,就需要去更改对应的视图层和模板层的url,非常麻烦,不便维护.

这时可以利用反向解析,当路由层url发生改变,在视图层和模板层动态反向解析出更改后的url,免去修改的操作.

反向解析一般用在模板中的超链接及视图中的重定向.

##### 普通路径

在urls.py中给路由起别名,name="路由别名".

```py
path("login1/", views.login, name="login")
```

在views.py中,从django.urls中引入reverse,利用reverse("路由别名") 反向解析:

```py
return redirect(reverse("login"))
```

在模板templates中的HTML文件中,利用{% url "路由别名" %}反向解析.

```django
<form action="{% url 'login' %}" method="post">
```

##### 正则路径(无名分组)

```py
re_path(r"^login/([0-9]{2})/$", views.login, name="login")
```

在views.py中,从django.urls中引入reverse,利用reverse("路由别名",args=(符合正则匹配的参数,)) 反向解析.

```py
return redirect(reverse("login",args=(10,)))
```

在模板templates中的HTML文件中利用{% url "路由别名" 符合正则匹配的参数 %}反向解析.

```django
<form action="{% url 'login' 10 %}" method="post">
```

##### 正则路径(有名分组)

```py
re_path(r"^login/(?P<year>[0-9]{4})/$", views.login, name="login")
```

```py
return redirect(reverse("login",kwargs={"year":3333}))
```

```django
<form action="{% url 'login' year=3333 %}" method="post">
```

#### 命名空间

命名空间(英语:Namespace)是表示标识符的可见范围.

一个标识符可在多个命名空间中定义,它在不同命名空间中的含义是互不相干的.

一个新的命名空间中可定义任何标识符,它们不会与任何重复的标识符发生冲突,因为重复的定义都处于其它命名空间中.

解决问题:路由别名name没有作用域,Django在反向解析URL时,会在项目全局顺序搜索,当查找到第一个路由别名name指定URL时,立即返回.<br>
当在不同的app目录下的urls中定义相同的路由别名name时,可能会导致URL反向解析错误.



##### 普通路径

定义命名空间(include里面是一个元组)格式如下:

```py
include(("app名称:urls","app名称"))
```

```py
path("app01/", include(("app01.urls","app01")))
path("app02/", include(("app02.urls","app02")))
```

```py
# app01/urls.py

path("login/", views.login, name="login")
```

在views.py中使用名称空间,语法格式如下:

```py
reverse("app名称:路由别名")
```

```py
return redirect(reverse("app01:login")
```

在templates模板的HTML文件中使用名称空间,语法格式如下:

```django
{% url "app名称:路由别名" %}
```

```django
<form action="{% url 'app01:login' %}" method="post">
```

### wsgi.py

一个WSGI兼容的Web服务器的入口,以便运行项目.

## templates

模板是一个文本,用于分离文档的表现形式和内容.

需要向Django说明模板文件的路径:

```py
# 项目根目录/项目主目录/settings.py
# ...
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],       # 修改位置
        'APP_DIRS': True
    },
]
# ...
```

### 模板标签

模板语法:

```py
views:｛"HTML变量名": "views变量名"｝
HTML:｛｛变量名｝｝
```

#### 注释标签

```django
{# 这是一个注释 #}
```

#### 列表

```django
<!-- 取出整个列表 -->
<p>{{views_list}}</p>
<!-- 取出列表的第一个元素 -->
<p>{{views_list.0}}</p>
```

#### 字典

```django
<p>{{views_dict.name}}</p>
```

#### 过滤器

模板语法:

```django
{{变量名|过滤器:可选参数}}
```

##### 大小写转换(upper/lower)

{{name}}变量被过滤器lower处理后,文档大写转换文本为小写.

```django
{{name|lower}}
```

过滤管道可以被 *套接* ,既是说,一个过滤器管道的输出又可以作为下一个管道的输入,

将第一个元素并将其转化为大写:

```django
{{my_list|first|upper}}
```

##### 长度截取(truncatewords)

有些过滤器有参数.<br>
过滤器的参数跟随冒号之后并且总是以双引号包含.

显示变量bio的前30个词:

```django
{{bio|truncatewords:"30"}}
```

##### 转义(addslashes)

添加反斜杠到任何反斜杠、单引号或者双引号前面.

##### 时间格式化(date)

按指定的格式字符串参数格式化date或者datetime对象.

格式Y-m-d H:i:s返回年-月-日小时:分钟:秒的格式时间.

```django
{{pub_date|date:"F j, Y"}}
```

##### 变量长度(length)


##### 默认值(default)

default为变量提供一个默认值.

如果views传的变量的布尔值是false,则使用指定的默认值.

以下值为false:

```py
00.0  False  0j  ""  [] None ()  set()  {}
```

```django
{{name|default:"菜鸟教程"}}
```

##### 文件大小格式化(filesizeformat)

以更易读的方式显示文件的大小(即'13 KB', '4.1 MB', '102 bytes'等).

字典返回的是键值对的数量,集合返回的是去重后的长度.

```py
{{num|filesizeformat}}
```


##### 省略号(truncatechars)

如果字符串包含的字符总个数多于指定的字符数量,那么会被截断掉后面的部分.

截断的字符串将以 ... 结尾.

```django
{{views_str|truncatechars:2}}
```

##### 安全标记(safe)

将字符串标记为安全,不需要转义.

要保证views.py传过来的数据绝对安全,才能用safe.

和后端views.py的mark_safe效果相同.

Django会自动对views.py传到HTML文件中的标签语法进行转义,令其语义失效.<br>
加safe过滤器是告诉Django该数据是安全的,不必对其进行转义,可以让该数据语义生效.

```django
{{views_str|safe}}
```

#### if/else


```django
{% if condition1 %}
   ... display 1
{% elif condition2 %}
   ... display 2
{% else %}
   ... display 3
{% endif %}
```

{% if %}标签接受and,or或者not关键字来对多个变量做判断 ,或者对变量取反( not ),例如:

```django
{% if athlete_list and coach_list %}
    athletes和coaches变量都是可用的.
{% endif %}
```

#### for

```django
<ul>
{% for athlete in athlete_list %}
    <li>{{athlete.name}}</li>
{% endfor %}
</ul>
```

##### reversed

给标签增加一个reversed使得该列表被反向迭代:

```django
{% for athlete in athlete_list reversed %}
...
{% endfor %}
```

##### 遍历字典

可以直接用字典 .items方法,用变量的解包分别获取键和值.

```django
{% for i,j in views_dict.items %}
{{i}}---{{j}}
{% endfor %}
```

##### {{forloop}}

在{% for %}标签里可以通过{{forloop}}变量获取循环序号.

0. forloop.counter: 顺序获取循环序号,从1开始计算
0. forloop.counter0: 顺序获取循环序号,从0开始计算
0. forloop.revcounter: 倒叙获取循环序号,结尾序号为1
0. forloop.revcounter0: 倒叙获取循环序号,结尾序号为0
0. forloop.first(一般配合if标签使用): 第一条数据返回True,其数据返回False
0. forloop.last(一般配合if标签使用): 最后一条数据返回True,其数据返回False

##### empty

可选的{% empty %}从句:在循环为空的时候执行(即in后面的参数布尔值为False).

```django
{% for i in listvar %}
    {{forloop.counter0}}
{% empty %}
    空空如也～
{% endfor %}
```

#### ifequal/ifnotequal

{% ifequal %}标签比较两个值,当相等时,显示在{% ifequal %}和{% endifequal %}之中所有的值.

```django
{% ifequal section 'sitenews' %}
    <h1>Site News</h1>
{% else %}
    <h1>No News Here</h1>
{% endifequal %}
```

#### include

{% include %}标签允许在模板中包含其它的模板的内容.

```django
{% include "nav.html" %}
```

#### csrf_token

srf_token用于form表单中,作用是跨站请求伪造保护.

如果不用｛% csrf_token %｝标签,在用form表单时,要再次跳转页面会报403权限错误.

用了｛% csrf_token %｝标签,在form表单提交数据时,才会成功.

### 模板继承

模板可以用继承的方式来实现复用,减少冗余内容.

网页的头部和尾部内容一般都是一致的,就可以通过模板继承来实现复用.

父模板用于放置可重复利用的内容,子模板继承父模板的内容,并放置自己的内容.

父模板:

```django
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>菜鸟教程(runoob.com)</title>
</head>
<body>
    <h1>Hello World!</h1>
    <p>菜鸟教程Django测试.<br>
</p>
    {% block mainbody %}
       <p>original</p>
    {% endblock %}
</body>
</html>
```

子模板:

```py
{%extends "base.html" %}

{% block mainbody %}
<p>继承了base.html文件</p>
{% endblock %}
```

#### 父模板(block...endblock)

标签block...endblock: 父模板中的预留区域,该区域留给子模板填充差异性的内容,不同预留区域名字不能相同.

```django
{%block名称%}
{# 预留给子模板的区域,可以设置设置默认内容 #}
{%endblock名称%}
```

#### 子模板(extends)

子模板使用标签extends继承父模板:

```django
{% extends "父模板路径"%}
```

子模板如果没有设置父模板预留区域的内容,<br>
则使用在父模板设置的默认内容,当然也可以都不设置,就为空.

子模板设置父模板预留区域的内容:

```django
{%block名称%}
{# 内容 #}
{%endblock名称%}
```

## templatetags

在应用目录下创建templatetags目录(与templates目录同级,目录名只能是templatetags).

在templatetags目录下创建任意py文件,如:my_tags.py.

### my_tags.py

```py
from django import template

register = template.Library()   # register的名字是固定的,不可改变
```

修改settings.py文件的TEMPLATES选项配置,添加libraries配置:

```py
# ...
TEMPLATES = [
  {
    'OPTIONS': {
      # ...
      # 添加libraries配置
      "libraries":{
        'my_tags':'templatetags.my_tags'
      }
    },
  },
]
# ...
```

#### 自定义过滤器(@register.filter)

装饰器的参数最多只能有2个.

```py
@register.filter
def my_filter(v1, v2):
    return v1 * v2
```

##### is_sage



##### needs_autoescape



##### expects_localtime


##### 模板过滤器期望字符串

如果编写只接收一个字符串作为第一个参数的模板过滤器,需要使用stringfilter的装饰器.<br>
它会将参数前转为字符串后传递给函数:

```py
from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def lower(value):
    return value.lower()
```

这样,就可以将一个整数传递给这个过滤器,而不会导致AttributeError(因为整数没有lower() 方法).


##### 过滤器和自动转义


编写自定义过滤器时,考虑一下过滤器将如何与Django的自动转义行为交互.

0. 原始字符串指原生Python字符串.<br>
在输出时,如果自动转义生效,则对它们进行转义,否则将保持不变.
0. 安全字符串是在输出时被标记为安全的字符串,不会进一步转义.<br>
必要的转义已在之前完成.<br>
它们通常用于原样输出HTML,HTML会在客户端被解释.

```py
from django.utils.safestring import SafeString

if isinstance(value, SafeString):
    # Do something with the "safe" string.
    ...
```

#### 自定义标签(@register.simple_tag)

标签比过滤器更复杂,因为标签啥都能做.<br>
Django提供了很多快捷方式,简化了编写绝大多数类型的标签过程.

许多模板标签接受多个参数——字符串或模板变量——并仅根据输入参数和一些额外信息进行某种处理,并返回结果.<br>
例如,current_time标签可能接受一个格式字符串,并将时间按照字符串要求的格式返回.

为了简化创建标签类型的流程,Django提供了一个助手函数, simple_tag.<br>
该函数实际是django.template.Library的一个方法,该函数接受任意个数的参数,将其封装在一个render函数以及上述其它必要的位置,并用模板系统注册它.

current_time函数因此能这样写:

```py
import datetime
from django import template

register = template.Library()

@register.simple_tag
def current_time(format_string):
    return datetime.datetime.now().strftime(format_string)
```

关于simple_tag助手函数,有几点要注意:

0. 检测要求参数的个数等在调用函数时就已完成,所以无需再做.
0. 包裹参数(如果有的话)的引号已被删除,所以收到一个普通字符串.
0. 如果参数是一个模板变量,函数将传递变量值,而不是变量本身.

若模板上下文处于自动转义模式,不像其它标签实体,simple_tag通过conditional_escape() 传递输出,为了确保输出正确的HTML,避免XSS漏洞的威胁.

如果不需要额外转义,可能需要在万分确定代码不会引入任何XSS漏洞的情况下使用mark_safe().<br>
如果只是构建小的HTML片段,强烈建议使用format_html(),而不是mark_safe().

若模板标签需要访问当前上下文,可以在注册标签时传入takes_context参数:

```py
@register.simple_tag(takes_context=True)
def current_time(context, format_string):
    timezone = context['timezone']
    return your_get_current_time_method(timezone, format_string)
```

若需要重命名标签,可以为其提供一个自定义名称:

```py
register.simple_tag(lambda x: x - 1, name='minusone')

@register.simple_tag(name='minustwo')
def some_function(value):
    return value - 2
```

simple_tag函数可以接受任意数量的位置或关键字参数.<br>
例如:

```py
@register.simple_tag
def my_tag(a, b, *args, **kwargs):
    warning = kwargs['warning']
    profile = kwargs['profile']
    ...
    return ...
```

随后在模板中,任意数量的,以空格分隔的参数会被传递给模板标签.<br>
与Python中类似,关键字参数的赋值使用等号("="),且必须在位置参数后提供.<br>
例子:

```django
{% my_tag 123 "abcd" book.title warning=message|lower profile=user.profile %}
```

将标签结果存入一个模板变量而不是直接将其输出是可能的.<br>
这能通过使用as参数,后跟变量名实现.<br>
这样做能在期望的位置输出内容:

```django
{% load current_time %}
{% current_time "%Y-%m-%d %I:%M %p" as the_time %}
<p>The timeis{{the_time}}.</p>
```

#### 语义化标签

文件中导入mark_safe.

```py
from django import template
from django.utils.safestring import mark_safe

register = template.Library()

# ...
```

定义标签时,用上mark_safe方法,令标签语义化,相当于jQuery中的html() 方法.

和前端HTML文件中的过滤器safe效果一样.


```py
@register.simple_tag
def my_html(v1, v2):
    temp_html = "<input type='text' id='%s' class='%s' />" %(v1, v2)
    return mark_safe(temp_html)
```

在HTML中使用该自定义标签,在页面中动态创建标签.

```django
{% my_html "zzz" "xxx" %}
```

### load my_tags

在使用自定义标签和过滤器前,要在html文件body的最上方中导入该py文件.

```django
{% load my_tags %}
```

## statics

在settings文件的最下方配置添加以下配置:

```py
STATIC_URL = '/static/' # 别名
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "statics"),
]
```


### css



### js



### images

#### 从静态目录中引入图片(load static)

```django
{% load static %}
{{name}}<img src="{% static "images/runoob-logo.png" %}" alt="runoob-logo">
```

### plugins


#### 插件引用

在HTML文件的head标签中引入bootstrap.

此时引用路径中的要 *** 用配置文件中的别名*** static,而不是目录statics.

```py
<link rel="stylesheet" href="/static/plugins/bootstrap-3.3.7/dist/css/bootstrap.css">
```

## manage.py

一个实用的命令行工具,可以各种方式与该Django项目进行交互.

## 项目模型

Django规定,如果要使用模型,必须要创建一个app.

```bash
django-admin.py startapp APP名称
```

接下来在settings.py中找到INSTALLED_APPS这一项,如下:

```py
INSTALLED_APPS = (
    # ...
    'APP名称',               # 添加此项
)
```

### __init__.py



### admin.py

为了让admin界面管理某个数据模型,需要先注册该数据模型到admin.

```py
from django.contrib import admin
from APP名称.modelsimport表名

admin.site.register(表名)
```

### models.py

#### 字段选项


##### 允许为空-数据存储(null)



##### 允许为空-表单验证(blank)



##### 特定选择(choices)



##### 数据库列名(db_column)



##### 数据库索引(db_index)



##### 数据库表空间(db_tablespace)



##### 默认值(default)



##### (editable)



##### 错误信息(error_messages)



##### *帮助*文本(help_text)



##### 模型主键(primary_key)



##### 是否唯一(unique)



##### 日期字段值唯一(unique_for_date)



##### 月份唯一(unique_for_month)



##### 年度唯一(unique_for_year)



##### 详细字段名(verbose_name)



##### 验证器列表(validators)



#### 字段类型

##### 自动递增(AutoField)



##### 布尔值(BooleanField)



##### 字符串(CharField)



##### 日期(DateField)



##### 日期和时间(DateTimeField)




##### 时间(TimeField)



##### 时间段(DurationField)



##### 大文本(TextField)


##### 十进制固定精度(DecimalField)

有两个必要的参数:

0. DecimalField.max_digits: 数字中允许的最大位数.<br>
请注意,这个数字必须大于或等于decimal_places.

0. DecimalField.decimal_places: 与数字一起存储的小数位数.

##### 浮点数(FloatField)




##### 唯一标识符(UUIDField)

一个用于存储通用唯一标识符的字段.使用Python的UUID类.<br>
当在PostgreSQL上使用时,<br>
它存储在一个uuid的数据类型中,否则存储在一个char(32) 中.

通用唯一标识符是primary_key的AutoField的一个很好的替代方案.<br>
数据库不会为生成UUID,<br>
所以建议使用default:

import uuid
from django.db import models

```py
class MyUUIDModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # other fields
```

请注意,一个可调用对象(省略括号)被传递给default,<br>
而不是UUID的实例.

##### 64位整数自动递增(BigAutoField)



##### 64位整数(BigIntegerField)



##### 二进制数据(BinaryField)



##### 电子邮件地址(EmailField)



##### 文件存储(FileField)



##### 文件路径(FilePathField)




##### 图像(ImageField)



##### 整数(IntegerField)



##### IPv4/IPv6地址(GenericIPAddressField)



##### JSON编码数据(JSONField)



##### (PositiveBigIntegerField)



##### (PositiveIntegerField)



##### (PositiveSmallIntegerField)



##### (SlugField)



##### (SmallAutoField)



##### (SmallIntegerField)



##### 链接(URLField)


#### 关系字段

##### 多对一关联(ForeignKey)

```py
from django.db import models

class Reporter(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField()

    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)

class Article(models.Model):
    headline = models.CharField(max_length=100)
    pub_date = models.DateField()
    reporter = models.ForeignKey(Reporter, on_delete=models.CASCADE)

    def __str__(self):
        return self.headline

    class Meta:
        ordering = ['headline']
```


##### 多对多关联(ManyToManyField)


在这个例子中,一篇 *Article(报刊上的文章)”可能在多个*公开发行物(对象objects)”中发布,并且一个* 公开发行物(对象objects)”也有多个具体发行的对象(Article):

```py
from django.db import models

class Publication(models.Model):
    title = models.CharField(max_length=30)

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title

class Article(models.Model):
    headline = models.CharField(max_length=100)
    publications = models.ManyToManyField(Publication)

    class Meta:
        ordering = ['headline']

    def __str__(self):
        return self.headline
```

##### (OneToOneField)



#### Field类

Field是一个抽象的类,表示一个数据库表的列.<br>
Django使用字段来创建数据库表( db_type() ),<br>
将Python类型映射到数据库( get_prep_value() ),反之亦然( from_db_value() ).

因此,一个字段在不同的DjangoAPI中是一个基本的部分,特别是models和querysets.

在模型中中,字段被实例化为一个类属性,并代表一个特定的表列,见模型.<br>
它的属性有null和unique,以及Django用来将字段值映射到数据库特定值的方法.

Field是RegisterLookupMixin的子类,因此Transform和Lookup都可以在它上面注册,以便在QuerySet中使用(例如: field_name__exact="foo" ).<br>
所有内置查找都是默认注册的.

所有Django的内置字段,如CharField,都是Field的特殊实现.<br>
如果需要一个自定义的字段,可以对任何一个内置字段进行子类化,或者从头开始写一个Field.<br>
无论哪种情况,请参见[编写自定义模型字段(model fields)](https://docs.djangoproject.com/zh-hans/3.2/howto/custom-model-fields/).

##### 查找

后缀|含义
-|-
__exact|完全匹配
__iexact|不区分大小写的完全匹配
__contains|区分大小写的包含测试
__icontains|不区分大小写的包含测试
__in|在一个给定的可迭代对象中
__gt|大于
__gte|大于等于
__lt|小于
__lte|小于等于
__startswith|区分大小写的开头为
__istartswith|不区分大小写的开头为
__endswith|区分大小写的结尾为
__iendswith|不区分大小写的结尾为
__range|范围测试(含)
__date|对于日期时间字段,将值投射为日期
__year|对于日期和日期时间字段,精确匹配年份
__iso_year|对于日期和日期时间字段,精确的 ISO 8601 周号年份匹配
__month|对于日期和日期时间字段,精确的月份匹配
__day|对于日期和日期时间字段,精确匹配日期
__week|对于日期和日期时间字段,根据 ISO-8601 ,返回星期号
__week_day|对于日期和日期时间字段,*星期几*匹配
__iso_week_day|对于日期和日期时间字段,精确匹配 ISO 8601 星期几
__quarter|对于日期和日期时间字段,*一年的四分之一*匹配
__time|对于日期时间字段,将其值强制转换为时间
__hour|对于日期时间和时间字段,精确的小时匹配
__minute|对于日期时间和时间字段,精确的分钟匹配
__second|对于日期时间和时间字段,完全秒配
__regex|区分大小写的正则表达式匹配
__iregex|不区分大小写的正则表达式匹配

#### description

字段的详细描述,例如: [django.contrib.admindocs](https://docs.djangoproject.com/zh-hans/3.2/ref/contrib/admin/admindocs/#module-django.contrib.admindocs)应用程序.

描述的形式可以是:

```py
description = _("String (up to %(max_length)s)")
```

其中的参数是从字段的__dict__中插入的.

#### descriptor_class

一个实现[描述符协议](https://docs.python.org/3/reference/datamodel.html#descriptors)的类,它被实例化并分配给模型实例属性.<br>
构造函数必须接受一个参数,即Field实例.<br>
覆盖该类属性可以自定义获取和设置行为.

为了将一个Field映射到数据库的特定类型,Django提供了一些方法:

##### get_internal_type()

返回一个字符串,用于命名这个字段,以满足后台的特定目的.<br>
默认情况下,它返回的是类名.

参见仿造内置字段类型在自定义字段中的用法.

##### db_type(connection)

返回Field的数据库列数据类型,并考虑connection.

参见自定义数据库类型在自定义字段中的用法.

##### rel_db_type(connection)

返回指向Field的ForeignKey和OneToOneField等字段的数据库列数据类型,并考虑connection.

参见自定义数据库类型在自定义字段中的用法.

Django主要有三种情况需要与数据库后台和字段进行交互.

当它查询数据库时(Python值 -> 数据库后台值)<br>
当它从数据库中加载数据时(数据库后台值 ->Python值)<br>
当它保存到数据库时(Python值 -> 数据库后端值)<br>
查询时,使用get_db_prep_value() 和get_prep_value().

##### get_prep_value(value)

value是模型属性的当前值,该方法应以准备作为查询参数的格式返回数据.

见将Python转为查询值的用法.

##### get_db_prep_value(value, connection, prepared=False)

将value转换为后台特定的值,默认情况下,如果prepared=True,则返回value.<br>
默认情况下,如果prepared=True,它将返回value,而如果是False,它将返回get_prep_value() .

使用方法参见将查询值转为数据库值.

加载数据时,使用from_db_value().

##### from_db_value(value, expression, connection)

将数据库返回的值转换为Python对象.<br>
与get_prep_value() 相反.

这个方法不用于大多数内置字段,因为数据库后端已经返回了正确的Python类型,或者后端自己进行了转换.

expression与self相同.

使用方法参见将值转为Python对象.

注解

由于性能原因,from_db_value并没有在不需要它的字段上实现no-op(所有Django字段).<br>
因此,不能在定义中调用super.

保存时,使用pre_save() 和get_db_prep_save() .

##### get_db_prep_save(value, connection)

与get_db_prep_value() 相同,但当字段值必须保存到数据库中时,会被调用.<br>
默认情况下返回get_db_prep_value().

##### pre_save(model_instance, add)

在get_db_prep_save() 之前调用的方法,在保存前准备好值(例如DateField.auto_now ).

model_instance是该字段所属的实例,add是该实例是否第一次被保存到数据库中.

它应该从model_instance中返回这个字段的适当属性的值.<br>
属性名在self.attname中(这是由Field设置的).

使用方法参见在保存前预处理数值.

字段经常以不同的类型接收它们的值,要么来自序列化,要么来自表单.

##### to_python(value)

将值转换为正确的Python对象.<br>
它的作用与value_to_string() 相反,并且在clean() 中也被调用.

使用方法参见将值转为Python对象.

除了保存到数据库,字段还需要知道如何将其值序列化.

##### value_from_object(obj)

返回给定模型实例的字段值.

这个方法经常被value_to_string() 使用.

##### value_to_string(obj)

将obj转换为字符串.<br>
用于序列化字段的值.

使用方法参见为序列化转换字段数据.

当使用modelforms时,Field需要知道它应该由哪个表单字段来表示.

##### formfield(form_class=None, choices_form_class=None, **kwargs)

返回该字段默认的django.forms.Field给ModelForm.

默认情况下,如果form_class和choices_form_class都是None,则使用CharField.<br>
如果字段有chips,且choices_form_class没有指定,则使用TypedChoiceField.

使用方法参见为模型字段指定表单字段.

##### deconstruct()

返回一个包含足够信息的四元元组来重新创建字段.

0. 模型上的字段名称.
0. 字段的导入路径(例如 "django.db.models.IntegerField" ).这应该是最可移植版本,所以不那么具体可能更好.
0. 一个位置参数的列表.
0. 一个关键字参数的字典.

这个方法必须添加到1.7之前的字段中,才能使用迁移迁移其数据.

#### 字段属性参考

每个Field实例都包含几个属性,允许对其行为进行内省.<br>
当需要编写依赖于字段功能的代码时,可以使用这些属性来代替isinstance检查.<br>
这些属性可以与Model._metaAPI一起使用,以缩小对特定字段类型的搜索范围.<br>
自定义模型字段应该实现这些标志.

##### 字段的属性
###### Field.auto_created

表示是否自动创建字段的布尔标志,如模型继承使用的OneToOneField.

###### Field.concrete

布尔值标志,表示该字段是否有与之相关的数据库列.

###### Field.hidden

布尔值标志,表示一个字段是否用于支持另一个非隐藏字段的功能(例如,组成GenericForeignKey的content_type和object_id字段).<br>
hidden标志用于区分构成模型上公共字段子集的内容和模型上所有字段.

Options.get_fields() 默认情况下不包括隐藏字段.<br>
传入include_hidden=True在结果中返回隐藏字段.

###### Field.is_relation

布尔值标志,表示一个字段是否包含对一个或多个其模型的功能引用(如ForeignKey、ManyToManyField、OneToOneField等).

###### Field.model

返回定义字段的模型.<br>
如果一个字段定义在一个模型的超类上,model将指的是超类,而不是实例的类.

##### 有关系的字段的属性

这些属性用于查询关系的基数和其细节.<br>
这些属性在所有字段上都存在;但是,如果字段是关系类型( Field.is_relation=True ),它们只有布尔值(而不是None).

###### Field.many_to_many

如果字段有多对多关系,则为True,否则为False.<br>
Django中唯一一个是True的字段是ManyToManyField.

###### Field.many_to_one

如果字段有多对一关系,如ForeignKey,则为True;否则为False.

###### Field.one_to_many

如果该字段有一对多关系,如GenericRelation或ForeignKey的反向关系,则为True;否则为False.

###### Field.one_to_one

如果字段有一对一的关系,如OneToOneField,则为True;否则为False.

###### Field.related_model

指向该字段所涉及的模型.<br>
例如,ForeignKey(Author, on_delete=models.CASCADE) 中的Author.<br>
GenericForeignKey的related_model总是None.

#### 定义模型

```py
from django.db import models

# 类名代表了数据库表名,且继承了models.Model
class Test(models.Model):
    # 字段代表数据表中的字段(name)
    # 数据类型则由CharField(相当于varchar)、DateField(相当于datetime)
    # max_length参数限定长度
    name = models.CharField(max_length=20)
```

模型定义好后在命令行中运行:

```sh
python3 manage.py migrate   # 项目初始化

python3 manage.py makemigrations APP名称  # 让Django知道在模型有一些变更
python3 manage.py migrate APP名称   # 创建表结构
```

```sh
Creating tables ...
# ...
Creating table APP名称_test  #自定义的表(表名组成结构为:应用名_类名)
# ...
```

#### 执行查询


##### 创建对象



```py
from blog.models import Blog
b = Blog(name='Beatles Blog', tagline='All the latest Beatles news.')
b.save()
```

##### 将修改保存至对象



```py
b5.name = 'New name'
b5.save()
```

###### 保存ForeignKey和ManyToManyField字段



```py

```

##### 检索对象



```py

```

##### 检索全部对象

方法all() 返回了一个包含数据库中所有对象的QuerySet对象.

```py
all_entries = Model.objects.all()
```

##### 通过过滤器检索指定对象



```py

```

###### 链式过滤器



```py

```

###### 每个QuerySet都是唯一的



```py

```

###### QuerySet是惰性的



```py

```

##### 用get()检索单个对象



```py

```

##### 其它QuerySet方法



```py

```

##### 限制QuerySet条目数



```py

```

##### 字段查询



```py

```

##### 跨关系查询



```py

```

###### 跨多值关联



```py

```

##### 过滤器可以为模型指定字段



```py

```

##### Expressionscanreferencetransforms



```py

```

##### 主键(pk)查询快捷方式



```py

```

##### 在LIKE语句中转义百分号和下划线



```py

```

##### 缓存和QuerySet



```py

```

###### 当QuerySet未被缓存时



```py

```

##### 查询JSONField



```py

```

##### 保存和查询None值



```py

```

##### Key,index,和路径转换



```py

```

##### 包含与键查找



```py

```

###### contains



```py

```

###### contained_by



```py

```

###### has_key



```py

```

###### has_keys



```py

```

###### has_any_keys



```py

```

##### 通过Q对象完成复杂查询



```py

```

##### 比较对象



```py

```

##### 删除对象



```py

```

##### 复制模型实例



```py

```

##### 一次修改多个对象



```py

```

##### 关联对象



```py

```

###### 一对多关联



```py

```

###### 正向访问



```py

```

###### *反向*关联



```py

```

###### 使用自定义反向管理器



```py

```

###### 管理关联对象的额外方法



```py

```

###### 多对多关联



```py

```

###### 一对一关联



```py

```

###### 反向关联是如何实现的?



```py

```

###### 查询关联对象



```py

```

##### 回归原生SQL



```py

```

#### 获取模型字段

```py
Model._meta.fields
```

#### 密码管理

Django提供灵活的密码存储系统,默认使用PBKDF2.

User对象的password属性是如下这种格式:

```
<algorithm>$<iterations>$<salt>$<hash>
```

这些是用来存储用户密码的插件,以美元符号分隔,包括:哈希算法,算法迭代次数(工作因子),随机Salt和最终的密码哈希值.<br>
该算法是Django可以使用的单向哈希或密码存储算法中的一种.<br>
迭代描述了算法在哈希上运行的次数.<br>
Salt是所使用的随机种子,哈希是单向函数的结果.

默认情况下,Django使用带有SHA256哈希的PBKDF2算法,它是NIST推荐的密码延展机制.<br>
它足够安全,需要大量的运算时间才能破解,这对大部分用户来说足够了.

但是,根据需求,可以选择不同的算法,甚至使用自定义的算法来匹配特定的安全场景.<br>
再次强调,大部分用户没必要这么做,如果不确定的话,很可能并不需要.<br>
如果坚持要做,请继续阅读:

Django通过查阅PASSWORD_HASHERS的设置来选择算法.<br>
这是一个Django支持的哈希算法类列表,第一个条目( settings.PASSWORD_HASHERS[0] )将被用来存储密码,其条目都是有效的哈希函数,可用来检测已存密码.<br>
这意味着如果想使用不同算法,需要修改PASSWORD_HASHERS,在列表中首选列出算法.

PASSWORD_HASHERS的默认值是:

```py
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
]
```

这意味着Django除了使用PBKDF2来存储所有密码,也支持使用PBKDF2SHA1、argon2和bcrypt来检测已存储的密码.

##### 使用Argon2

Argon2是2015年哈希密码竞赛的获胜者,这是一个社区为选择下一代哈希算法而主办的公开竞赛.<br>
它被设计成在定制硬件上计算不比在普通CPU上计算更容易.

Argon2并不是Django的默认首选,因为它依赖第三方库.<br>
尽管哈希密码竞赛主办方建议立即使用Argon2,而不是Django提供的其算法.

使用Argon2作为默认存储算法,需要以下步骤:

0. 安装argon2-cffilibrary库,可通过python-m pip install django[argon2] 安装,相当于python-m pip install argon2-cffi (以及Django的setup.cfg的任何版本要求).

0. 修改PASSWORD_HASHERS配置,把Argon2PasswordHasher放在首位.<br>
如下:

```py
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
]
```

如果需要Django升级密码( upgrade passwords ),请保留或添加这个列表中的任何条目.

##### 使用bcrypt

Bcrypt是一个非常流行的密码存储算法,尤其是为长期密码存储设计.<br>
Django默认不使用它,因为它需要使用第三方库,但由于很多人想使用它,Django只需要很少的努力就能支持bcrypt.

使用Bcrypt作为默认存储算法,需要以下步骤:

0. 安装bcrypt library库.<br>
通过python-m pip install django[bcrypt] 安装,相当于python-m pip install bcrypt (以及Django的setup.cfg的任何版本要求).

0. 修改PASSWORD_HASHERS配置,把BCryptSHA256PasswordHasher放在首位.<br>
如下:

```py
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.Argon2PasswordHasher',
]
```

##### 增加盐的熵值

大多数密码哈希值包括一个盐,与密码哈希值一起,以防止彩虹表攻击.<br>
盐本身是一个随机值,它增加了彩虹表的大小和成本,目前在BasePasswordHasher中的salt_entropy值设置为128比特.<br>
随着计算和存储成本的降低,这个值应该被提高.<br>
当实现自己的密码散列器时,可以自由地覆盖这个值,以便为密码散列器使用一个理想的熵值.<br>
salt_entropy是以比特为单位.

##### 增加工作因子

###### PBKDF2和bcrypt

PBKDF2和bcrypt算法使用一些迭代或几轮哈希.<br>
这样有意降低了攻击者的速度,使得破解密码变得更困难.<br>
然而,随着算力的增加,迭代的次数也需要增加.<br>
已经选择了合理的默认配置(也会对Django的每个版本加入),但可能希望根据安全需求和可支配的能力来调高或调低它.<br>
为此,将子类化合适的算法并覆盖iterations参数.<br>
比如,增加默认PBKDF2算法使用的一些迭代:

1. 创建django.contrib.auth.hashers.PBKDF2PasswordHasher的子类:

```py
# myproject/hashers.py
from django.contrib.auth.hashers import PBKDF2PasswordHasher

class MyPBKDF2PasswordHasher(PBKDF2PasswordHasher):
    """
    A subclass of PBKDF2PasswordHasher that uses 100 times more iterations.
    """
    iterations = PBKDF2PasswordHasher.iterations * 100
```

2. 在PASSWORD_HASHERS中把新哈希放在首位:

```py
PASSWORD_HASHERS = [
    'myproject.hashers.MyPBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
]
```

现在Django使用PBKDF2存储密码时将会多次迭代.


###### Argon2

Argon2有三个可以自定义的属性:

0.time_cost控制哈希的次数.
0.memory_cost控制被用来计算哈希时的内存大小.
0.parallelism控制并行计算哈希的CPU数量.

这三个属性的默认值足够适合.<br>
如果确定密码哈希过快或过慢,可以按如下方式调整它:

0. 选择parallelism可以节省计算哈希的线程数.
0. 选择memory_cost可以节省内存的KiB.
0. 调整time_cost和估计哈希一个密码所需的时间.挑选出可以接受的time_cost.<br>
如果设置为1的time_cost慢的无法接受,则调低memory_cost.<br>
argon2命令行工具和一些其库解释了memory_cost参数不同于Django使用的值.<br>
换算公式是\``memory_cost == 2 ** memory_cost_commandline\`` .

##### 密码升级

###### 无需登录的密码升级

##### 手动管理用户的密码

django.contrib.auth.hasher模块提供了一组函数来创建和验证哈希密码.<br>
可以独立于用户模型使用它们.

###### 验证密码(check_password(password, encoded))

```py
from django.contrib.auth.hashers import check_password
```

如果想通过对比纯文本密码和数据库中的哈希密码来验证用户,可以使用check_password() 快捷函数.<br>
它需要2个参数:要检查的纯文本密码和要检查的数据库中用户密码字段的值.<br>
如果匹配成功,返回True,否则返回False.

###### 创建密码(make_password(password, salt=None, hasher='default'))

通过此应用的格式创建一个哈希密码.<br>
它需要一个必需的参数:纯文本密码(字符串或字节).<br>
或者,如果不想使用默认配置(PASSWORD_HASHERS配置的首个条目 ),那么可以提供salt和使用的哈希算法.<br>
有关每个哈希的算法名,可查看已包含的哈希 .<br>
如果密码参数是None,将返回一个不可用的密码(永远不会被check_password() 通过的密码).

###### is_password_usable(encoded_password)

如果密码是User.set_unusable_password() 的结果,则返回False.

##### 密码验证

用户经常会选择弱密码.<br>
为了缓解这个问题,Django提供可插拔的密码验证.<br>
可以同时配置多个密码验证.<br>
Django已经包含了一些验证,但也可以编写自己的验证.

每个密码验证器必须提供给用户提供帮助文案以向用户解释要求,<br>
验证密码并在不符合要求时返回错误信息,并且可选择接受已经设置过的密码.<br>
验证器也可以使用可选设置来微调它们的行为.

验证由AUTH_PASSWORD_VALIDATORS控制.<br>
默认的设置是一个空列表,这意味着默认是不验证的.<br>
在使用默认的start project创建的新项目中,默认启用了验证器集合.

默认情况下,验证器在重置或修改密码的表单中使用,也可以在createsuperuser和changepassword命令中使用.<br>
验证器不能应用在模型层,比如User.objects.create_user() 和create_superuser() ,因为假设开发者(非用户)会在模型层与Django进行交互,也因为模型验证不会在创建模型时自动运行.

密码验证器可以防止使用很多类型的弱密码.<br>
但是,密码通过所有的验证器并不能保证它就是强密码.<br>
这里有很多因素削弱即便最先进的密码验证程序也检测不到的密码.

密码验证器可以防止使用很多类型的弱密码.<br>
但是,密码通过所有的验证器并不能保证它就是强密码.<br>
这里有很多因素削弱即便最先进的密码验证程序也检测不到的密码.

###### 启用密码验证

在AUTH_PASSWORD_VALIDATORS中设置密码验证:

```py
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 9,
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
```

这个例子启用了所有包含的验证器:

0. UserAttributeSimilarityValidator检查密码和一组用户属性集合之间的相似性.
0. MinimumLengthValidator用来检查密码是否符合最小长度.<br>
这个验证器可以自定义设置:它现在需要最短9位字符,而不是默认的8个字符.
0. CommonPasswordValidator检查密码是否在常用密码列表中.<br>
默认情况下,它会与列表中的2000个常用密码作比较.
0. NumericPasswordValidator检查密码是否是完全是数字的.

对于UserAttributeSimilarityValidator和CommonPasswordValidator,在这个例子里使用默认配置.<br>
NumericPasswordValidator不需要设置.

帮助文本和来自密码验证器的任何错误信息始终按照AUTH_PASSWORD_VALIDATORS列出的顺序返回.


###### 已包含的验证器

Django包含了四种验证器:


```py
class MinimumLengthValidator(min_length=8)
```

验证密码是否符合最小长度.<br>
最小长度可以在min_length参数中自定义.


```py
class UserAttributeSimilarityValidator(user_attributes=DEFAULT_USER_ATTRIBUTES, max_similarity=0.7)
```

验证密码是否与用户的某些属性有很大的区别.

user_attributes参数应该是可比较的用户属性名的可迭代参数.<br>
如果没有提供这个参数,默认使用:'username', 'first_name', 'last_name', 'email' .<br>
不存在的属性会被忽略.

不合格密码的最小相似度被设置为0到1这个区间.<br>
设置为0会拒绝所有密码,而设置为1只会拒绝与属性值相同的密码.


```py
class CommonPasswordValidator(password_list_path=DEFAULT_PASSWORD_LIST_PATH)
```

验证密码是否是常用密码.<br>
先转换密码为小写字母(做一个不区分大小写的比较),然后根据RoyceWilliams创建的2000个常用密码的列表进行检查.

password_list_path用来设置自定义的常用密码列表文件的路径.<br>
这个文件应该每行包含一个小写密码,并且文件是纯文本或gzip压缩过的.


```py
class NumericPasswordValidator
```

检查密码是否完全是数字.

###### 集成检查

###### 编写自定义的验证器



### tests.py



### views.py

#### QueryDict对象

在HttpRequest对象中, GET和POST属性是django.http.QueryDict类的实例.

QueryDict类似字典的自定义类,用来处理单键对应多值的情况.

QueryDict实现所有标准的词典方法.<br>
还包括一些特有的方法:

##### __getitem__

和标准字典的处理有一点不同,就是,如果Key对应多个Value,__getitem__()返回最后一个value.

##### __setitem__

设置参数指定key的value列表(一个Python list).<br>
注意:它只能在一个mutableQueryDict对象上被调用(就是通过copy()产生的一个QueryDict对象的拷贝).

##### get()

如果key对应多个value,get()返回最后一个value.

##### update()

参数可以是QueryDict,也可以是标准字典.<br>
和标准字典的update方法不同,该方法添加字典items,而不是替换它们:

```py
q = QueryDict('a=1')

q = q.copy() # to make it mutable

q.update({'a': '2'})

q.getlist('a')
# => ['1', '2']

q['a'] # returns the last
# => ['2']
```

##### items()

和标准字典的items()方法有一点不同,该方法使用单值逻辑的__getitem__():

```py
q = QueryDict('a=1&a=2&a=3')

q.items()
# => [('a', '3')]
```

##### values()

和标准字典的values()方法有一点不同,该方法使用单值逻辑的__getitem__()

##### copy()

返回对象的拷贝,内部实现是用Python标准库的copy.deepcopy().<br>
该拷贝是mutable(可更改的) —就是说,可以更改该拷贝的值.

##### getlist(key)

返回和参数key对应的所有值,作为一个Python list返回.<br>
如果key不存在,则返回空list.

##### setlist(key,list_)

设置key的值为list_ (unlike __setitem__()).

##### appendlist(key,item)

添加item到和key关联的内部list.

##### setlistdefault(key,list)

和setdefault有一点不同,它接受list而不是单个value作为参数.

##### lists()

和items()有一点不同, 它会返回key的所有值,作为一个list, 例如:

```py
q = QueryDict('a=1&a=2&a=3')

q.lists()
# => [('a', ['1', '2', '3'])]
```

##### urlencode()

返回一个以查询字符串格式进行格式化后的字符串(例如:"a=2&b=3&b=5").

#### Request对象

每个视图函数的第一个参数是一个HttpRequest对象.

```py
from django.http import HttpResponse

def runoob(request):
    return HttpResponse("Hello world")
```

HttpRequest对象包含当前请求URL的一些信息:

##### path

请求页面的全路径,不包括域名—例如, "/hello/".

##### method

请求中使用的HTTP方法的字符串表示.<br>
全大写表示.例如:

```py
if request.method == 'GET':
    do_something()
elif request.method == 'POST':
    do_something_else()
```

##### GET

包含所有HTTP GET参数的类字典对象.<br>
参见QueryDict文档.

##### POST

包含所有HTTP POST参数的类字典对象.<br>
参见QueryDict文档.

服务器收到空的POST请求的情况也是有可能发生的.<br>
也就是说,表单form通过HTTP POST方法提交请求,但是表单中可以没有数据.<br>
因此,不能使用语句if request.POST来判断是否使用HTTP POST方法.<br>
应该使用if request.method == "POST" (参见本表的method属性).

注意: POST不包括file-upload信息.<br>
参见FILES属性.

##### REQUEST

为了方便,该属性是POST和GET属性的集合体,但是有特殊性,先查找POST属性,然后再查找GET属性.<br>
借鉴PHP's $_REQUEST.

例如,如果GET = {"name": "john"}和POST = {"age": '34'},<br>
则REQUEST["name"] 的值是"john", REQUEST["age"]的值是"34".

强烈建议使用GET and POST,因为这两个属性更加显式化,写出的代码也更易理解.

##### COOKIES

包含所有cookies的标准Python字典对象.<br>
Keys和values都是字符串.

##### FILES

包含所有上传文件的类字典对象.<br>
FILES中的每个Key都是\<input type="file" name="" />标签中name属性的值.<br>
FILES中的每个value同时也是一个标准Python字典对象,包含下面三个Keys:

0. filename: 上传文件名,用Python字符串表示
0. content-type: 上传文件的Content type
0. content: 上传文件的原始内容

注意:只有在请求方法是POST,并且请求页面中\<form>有enctype="multipart/form-data"属性时FILES才拥有数据.<br>
否则,FILES是一个空字典.

##### META

包含所有可用HTTP头部信息的字典.例如:

0. CONTENT_LENGTH
0. CONTENT_TYPE
0. QUERY_STRING: 未解析的原始查询字符串
0. REMOTE_ADDR: 客户端IP地址
0. REMOTE_HOST: 客户端主机名
0. SERVER_NAME: 服务器主机名
0. SERVER_PORT: 服务器端口

META中这些头加上前缀HTTP_为Key, 冒号(:)后面的为Value, 例如:

0. HTTP_ACCEPT_ENCODING
0. HTTP_ACCEPT_LANGUAGE
0. HTTP_HOST: 客户发送的HTTP主机头信息
0. HTTP_REFERER: referring页
0. HTTP_USER_AGENT: 客户端的user-agent字符串
0. HTTP_X_BENDER: X-Bender头信息

##### user

是一个django.contrib.auth.models.User对象,代表当前登录的用户.

如果访问用户当前没有登录,user将被初始化为django.contrib.auth.models.AnonymousUser的实例.

可以通过user的is_authenticated()方法来辨别用户是否登录:

```py
if request.user.is_authenticated():
    # Do something for logged-in users.
else:
    # Do something for anonymous users.
```

只有激活Django中的AuthenticationMiddleware时该属性才可用.

##### session

**唯一可读写的属性**,代表当前会话的字典对象.<br>
只有激活Django中的session支持时该属性才可用.

##### raw_post_data

原始HTTP POST数据,未解析过.<br>
高级处理时会有用处.

##### __getitem__(key)

返回GET/POST的键值,先取POST,后取GET.<br>
如果键不存在抛出KeyError.
这是可以使用字典语法访问HttpRequest对象.
例如,request["foo"]等同于先request.POST["foo"] 然后request.GET["foo"]的操作.

##### has_key()

检查request.GET or request.POST中是否包含参数指定的Key.

##### get_full_path()

返回包含查询字符串的请求路径.<br>
例如, "/music/bands/the_beatles/?print=true"

##### is_secure()

如果请求是安全的,返回True,就是说,发出的是HTTPS请求.

#### HttpResponse对象

返回文本,参数为字符串,字符串中写文本内容.<br>
如果参数为字符串里含有html标签,也可以渲染.

```py
def runoob(request):
    # return HttpResponse("菜鸟教程")
    return HttpResponse("<a href='https://www.runoob.com/'>菜鸟教程</a>")
```

响应对象主要有三种形式:HttpResponse()、render()、redirect().

render和redirect是在HttpResponse的基础上进行了封装:

0. render:底层返回的也是HttpResponse对象
0. redirect:底层继承的是HttpResponse对象

##### render()

返回文本,第一个参数为request,第二个参数为字符串(页面名称),第三个参数为字典(可选参数,向页面传递的参数:键为页面参数名,值为views参数名).

```py
def runoob(request):
    name ="菜鸟教程"
    return render(request,"runoob.html",{"name":name})
```

##### redirect()

重定向,跳转新页面.<br>
参数为字符串,字符串中填写页面路径.<br>
一般用于form表单提交后,跳转到新页面.

```py
def runoob(request):
    return redirect("/index/")
```

#### 通用显示视图-列表(ListView)

当该视图执行时,self.object_list将包含该视图正在操作的对象列表(通常,但不一定是查询集).

该视图从以下视图继承方法和属性.

0. django.views.generic.list.MultipleObjectTemplateResponseMixin
0. django.views.generic.base.TemplateResponseMixin
0. django.views.generic.list.BaseListView
0. django.views.generic.list.MultipleObjectMixin
0. django.views.generic.base.View

方法流程图

0. setup()
0. dispatch()
0. http_method_not_allowed()
0. get_template_names()
0. get_queryset()
0. get_context_object_name()
0. get_context_data()
0. get()
0. render_to_response()

```py
# app_name/views.py
from django.views.generic import ListView
from people.models import Role as ModelRole

class RoleListView(ListView):
    model = ModelRole # 模型类
    paginate_by = 3 # 分页条数
    template_name='people/roles/index.html' # 模板名称
```

```django
{%extends "layout/fore.html" %}

{% if is_paginated %}
  <ul>
    {% if page_obj.has_previous %}
      {% if page_obj.number|add:'-4' > 1 %}
        <li><a href="?page=1">首页</a></li>
      {% endif %}
      <li><a href="?page={{page_obj.previous_page_number}}">上一页</a></li>
    {% endif %}

    {% if page_obj.number|add:'-4' > 1 %}
      <li><a href="?page={{page_obj.number|add:'-5' }}">&hellip;</a></li>
    {% endif %}

    {% for num in page_obj.paginator.page_range %}
      {% if num == page_obj.number %}
      <li class="active">
        <a href="?page={{num}}">{{num}}</a>
      {% elif num > page_obj.number|add:'-5' and num < page_obj.number|add:'5' %}
        <li><a href="?page={{num}}">{{num}}</a></li>
      {% endif %}
      </li>
    {% endfor %}

    {% if page_obj.paginator.num_pages > page_obj.number|add:'4' %}
      <li><a href="?page={{page_obj.number|add:'5' }}">&hellip;</a></li>
    {% endif %}

    {% if page_obj.has_next %}
      <li><a href="?page={{page_obj.next_page_number}}">下一页</a></li>
    {% endif %}
  </ul>
{% endif %}
```

#### 通用显示视图-详情(DetailView)

当该视图执行时,self.object将包含该视图正在操作的对象.

该视图从以下视图继承方法和属性.

0. django.views.generic.detail.SingleObjectTemplateResponseMixin
0. django.views.generic.base.TemplateResponseMixin
0. django.views.generic.detail.BaseDetailView
0. django.views.generic.detail.SingleObjectMixin
0. django.views.generic.base.View

方法流程图

0. setup()
0. dispatch()
0. http_method_not_allowed()
0. get_template_names()
0. get_slug_field()
0. get_queryset()
0. get_object()
0. get_context_object_name()
0. get_context_data()
0. get()
0. render_to_response()

```py
# app_name/views.py
from django.utils import timezone
from django.views.generic.detail import DetailView

from articles.models import Article

class ArticleDetailView(DetailView):

    model = Article

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context
```

```py
# app_name/urls.py:

from django.urls import path

from article.views import ArticleDetailView

urlpatterns = [
    path('<slug:slug>/', ArticleDetailView.as_view(), name='article-detail'),
]
```

### forms.py

DjangoForm组件用于对页面进行初始化,生成HTML标签,此外还可以对用户提交对数据进行校验(显示错误信息).

报错信息显示顺序:

0. 先显示字段属性中的错误信息,然后再显示局部钩子的错误信息.
0. 若显示了字段属性的错误信息,就不会显示局部钩子的错误信息.
0. 若有全局钩子,则全局钩子是等所有的数据都校验完,才开始进行校验,并且全局钩子的错误信息一定会显示.

使用Form组件,需要先导入forms:

```py
from django import forms
```

```py
from django import forms
from django.core.exceptions import ValidationError

class类名(forms.Form):
    name = forms.CharField(min_length=4, label="姓名", error_messages={"min_length": "太短了", "required": "该字段不能为空!"})
    age = forms.IntegerField(label="年龄")
    salary = forms.DecimalField(label="工资")
```

```py
# views.py

from django.shortcuts import render, HttpResponse
from app_name.form_name import ClassName
from django.core.exceptions import ValidationError


def add_emp(request):
    if request.method == "GET":
        form = EmpForm()
        return render(request, "add_emp.html", {"form": form})
    else:
        form = EmpForm(request.POST)
        if form.is_valid():  # 进行数据校验
            # 校验成功
            data = form.cleaned_data  # 校验成功的值,会放在cleaned_data里.
            data.pop('r_salary')
            print(data)

            models.Emp.objects.create(**data)
            return HttpResponse(
                'ok'
            )
            # return render(request, "add_emp.html", {"form": form})
        else:
            print(form.errors)    # 打印错误信息
            clean_errors = form.errors.get("__all__")
            print(222, clean_errors)
        return render(request, "add_emp.html", {"form": form, "clean_errors": clean_errors})
```


```django
{# 通过form对象的as_p方法实现#}
<form action="" method="post" novalidate>
    {% csrf_token %}
    {{form.as_p}}
    <input type="submit">
</form>

```

#### 核心字段参数



##### required



##### label



##### label_suffix



##### initial



##### widget



##### help_text



##### error_messages



##### validators



##### localize



##### disabled



#### 检查字段数据是否有变化



##### has_changed()



#### 内置Field类



##### BooleanField



##### CharField



##### ChoiceField



##### TypedChoiceField



##### DateField



##### DateTimeField



##### DecimalField



##### DurationField



##### EmailField



##### FileField



##### FilePathField



##### FloatField



##### ImageField



##### IntegerField



##### JSONField



##### GenericIPAddressField



##### MultipleChoiceField



##### TypedMultipleChoiceField



##### NullBooleanField



##### RegexField



##### SlugField



##### TimeField



##### URLField



##### UUIDField



#### 稍复杂的内置Field类



##### ComboField



##### MultiValueField



##### SplitDateTimeField



#### 处理关系的字段



##### ModelChoiceField



##### ModelMultipleChoiceField



##### 迭代关系选择



###### ModelChoiceIterator



###### ModelChoiceIteratorValue



##### 创建自定义字段



#### ModelForm

如果正在构建一个数据库驱动的应用程序,那么很有可能会用到与Django模型密切相关的表单.<br>
例如,可能有一个BlogComment模型,并且想创建一个让用户提交评论的表单.<br>
在这种情况下,在表单中定义字段类型是多余的,因为已经在模型中定义了字段.

因此,Django提供了一个辅助类可以从一个Django模型创建一个Form类.

例如:

```py
from django.forms import ModelForm
from myapp.models import Article

# Create the form class.
class ArticleForm(ModelForm):
    class Meta:
        model = Article
        # 将fields属性设置为特殊值 '__all__' 以表明需要使用模型中的所有字段
        fields = ['pub_date', 'headline', 'content', 'reporter']
        # exclude属性设置为表单中需要排除的字段列表
        exclude = ['title']

# Creating a form to add an article.
form = ArticleForm()

# Creating a form to change an existing article.
article = Article.objects.get(pk=1)
form = ArticleForm(instance=article)
```

##### 字段类型



```py

```

##### 验证ModelForm



```py

```

###### 覆盖clean()方法



```py

```

###### 与模型验证交互



```py

```

###### 有关模型的error_messages的注意事项



```py

```


##### save()方法



```py

```

##### 选择要使用的字段



```py

```

##### 覆盖默认字段



```py

```

##### 启用对字段的本地化



```py

```

##### 表单继承



```py

```

##### 提供初始值



```py

```

##### ModelForm的工厂函数



```py

```


#### 模型表单集
##### 更改查询集



```py

```

##### 更改表单



```py

```

##### 在表单中使用widgets指定部件.



```py

```

##### 使用localized_fields来启用字段本地化



```py

```

##### 提供初始值



```py

```

##### 在表单集中保存对象



```py

```

##### 限制可编辑对象的数量



```py

```

##### 在视图中使用模型表单集



```py

```

##### 覆盖ModelFormSet上的clean()</span></code>



```py

```

##### 使用自定义查询结果集



```py

```

##### 在模板中使用表单集



```py

```


#### 内联表单集
##### 覆盖InlineFormSet上的方法



```py

```

##### 同一个模型有多个外键



```py

```

##### 在视图中使用内联表单集



```py

```

##### 指定要在内联表单中使用的widgets



```py

```

## locale

### 翻译

#### 概况

为了使Django项目可以翻译,需要在Python代码和模板中添加少量钩子.<br>
这些钩子被成为translation strings .<br>
它们告知Django:如果在终端用户语言里,这个文本有对应的翻译,那么应该使用翻译.<br>
系统只会翻译它知道的字符串.

然后Django提供工具将翻译字符串提取到message file中.<br>
这个文件让翻译者方便地提供翻译字符串.<br>
一旦翻译者填写了messag efile ,就必须编译它.<br>
这个过程依赖GNUgettext工具集.

完成后,Django会根据用户的语言偏好,使用每种可用语言对网页进行即时翻译.

Django的国际化钩子默认是开启的,这意味着在框架的某些位置存在一些i18n相关的开销.<br>
如果不使用国际化,应该在配置文件里设置USE_I18N = False .<br>
然后Django将进行优化,以免加载国际化机制.

#### 在Python代码中进行国际化



##### 标准翻译

使用函数gettext() 来指定翻译字符串.<br>
按照惯例,将其作为下划线( _ )导入,以保存输入.

Python标准库gettext模块在全局命名空间里安装了_() .<br>
在Django里,没有遵循这个做法,出于下面两个原因:

0. 有时,对于特定文件,应该使用gettext_lazy() 作为默认翻译方法.<br>
如果全局命名空间里没有_() ,开发者必须考虑哪个是最合适的翻译函数.
0. 下划线("_") 用于表示在Python的交互式终端和doctest测试中 "上一个结果" .<br>
安装全局_() 函数会引发冲突.<br>
导入gettext() 替换_() 可以避免这个问题.

由于xgettext的工作方式,只有带有单一字符串参数的函数才能当做_引入:

* gettext()
* gettext_lazy()

在这个例子里,文本 "Welcome to my site."  被标记为翻译字符串:

```py
from django.http import HttpResponse
from django.utils.translation import gettext as _

def my_view(request):
    output = _("Welcome to my site.")
    return HttpResponse(output)
```

代码里可以不使用别名.<br>
这个例子与上一个例子等同:

```py
from django.http import HttpResponse
from django.utils.translation import gettext

def my_view(request):
    output = gettext("Welcome to my site.")
    return HttpResponse(output)
```

翻译适用于计算值.<br>
这个例子与前面两个例子等同:

```py
def my_view(request):
    words = ['Welcome', 'to', 'my', 'site.']
    output = _(' '.join(words))
    return HttpResponse(output)
```

##### 为翻译者提供注释



##### 标记不用翻译的字符



##### 多元化



##### 上下文标记



##### 惰性翻译



###### 模型字段和相关的verbose_name与help_text选项值



###### 模型详细名称的值



###### Modelmethodsdescriptionargumenttothe@displaydecorator



#### 使用惰性翻译对象



##### 惰性翻译与复数



##### 格式化字符串:format_lazy()



##### 延迟翻译中惰性(lazy)的其用法



#### 语言的本地化名称



#### 在模板代码中国际化



##### translate模板标签



##### blocktranslate模板标签



##### 传递字符串给标签和过滤器



##### 模板内对翻译的注释



##### 在模板中选择语言



##### 其标签



###### get_available_languages



###### get_current_language



###### get_current_language_bidi



###### i18n上下文处理器



###### get_language_info



###### get_language_info_list



###### 模板过滤器



#### 国际化:在JavaScript代码里



##### JavaScriptCatalog视图



##### 使用JavaScript翻译目录



###### gettext



###### ngettext



###### interpolate



###### get_format



###### gettext_noop



###### pgettext



###### npgettext



###### pluralidx



##### JSONCatalog视图



##### 性能说明



#### 国际化:在URL模式中



##### URL模式中的语言前缀



##### 翻译URL模式



##### 在模板中反向解析URL



#### 本地化:如何创建语言文件



##### 消息文件



##### 编译消息文件



##### 疑难解答:gettext()在带有百分号的字符串中错误地检测python-format



##### 从JavaScript源码中创建消息文件



##### Windows上的gettext



##### 自定义makemessages命令



#### 杂项



##### set_language重定向试图



##### 显式设置语言



##### 使用视图和模板外的翻译



##### Languagecookie



#### 实施说明



##### Django翻译的特性



##### Django如何发现语言偏好



##### Django如何发现翻译



##### 使用非英语基础语言



# 其他

## 断点

```py
import pdb; pdb.set_trace()
```

# 参考资料

> [菜鸟教程 | Django教程](https://www.runoob.com/django/django-tutorial.html)
