
<!-- TOC -->

- [构建博客应用程序](#构建博客应用程序)
  - [安装Django](#安装django)
    - [创建隔离的Python环境](#创建隔离的python环境)
    - [利用pip安装Django](#利用pip安装django)
  - [创建第一个项目](#创建第一个项目)
    - [运行开发服务器](#运行开发服务器)
    - [项目设置](#项目设置)
    - [项目和应用程序](#项目和应用程序)
    - [创建应用程序](#创建应用程序)
  - [设计博客数据方案](#设计博客数据方案)
    - [激活应用程序](#激活应用程序)
    - [设置并使用迁移方案](#设置并使用迁移方案)
  - [针对模型创建管理站点](#针对模型创建管理站点)
    - [创建超级用户](#创建超级用户)
    - [Django管理站点](#django管理站点)
    - [向管理站点中添加模型](#向管理站点中添加模型)
    - [定制模型的显示方式](#定制模型的显示方式)
  - [与QuerySet和管理器协同工作](#与queryset和管理器协同工作)
    - [创建对象](#创建对象)
    - [更新对象](#更新对象)
    - [检索对象](#检索对象)
    - [删除对象](#删除对象)
    - [评估QuerySet](#评估queryset)
    - [创建模型管理器](#创建模型管理器)
  - [构建列表和详细视图](#构建列表和详细视图)
    - [生成列表和视图](#生成列表和视图)
    - [向视图添加URL路径](#向视图添加url路径)
    - [模型的标准URL](#模型的标准url)
  - [创建视图模板](#创建视图模板)
  - [添加分页机制](#添加分页机制)
  - [使用基于类的视图](#使用基于类的视图)
  - [本章小结](#本章小结)
- [利用高级特性完善博客程序](#利用高级特性完善博客程序)
  - [通过电子邮件共享帖子](#通过电子邮件共享帖子)
    - [使用Django创建表单](#使用django创建表单)
    - [处理视图中的表单](#处理视图中的表单)
    - [利用Django发送邮件](#利用django发送邮件)
    - [渲染模板中的表单](#渲染模板中的表单)
  - [构建评论系统](#构建评论系统)
    - [构建模型](#构建模型)
    - [创建模型中的表单](#创建模型中的表单)
    - [处理视图中的ModelForms](#处理视图中的modelforms)
    - [向帖子详细模板中添加评论](#向帖子详细模板中添加评论)
  - [添加标签功能](#添加标签功能)
  - [根据相似性检索帖子](#根据相似性检索帖子)
  - [本章小结](#本章小结-1)
- [扩展博客应用程序](#扩展博客应用程序)
  - [创建自定义模板标签和过滤器](#创建自定义模板标签和过滤器)
    - [自定义模板标签](#自定义模板标签)
    - [自定义模板过滤器](#自定义模板过滤器)
  - [向站点添加网站地图](#向站点添加网站地图)
  - [创建帖子提要](#创建帖子提要)
  - [向博客中添加全文本搜索功能](#向博客中添加全文本搜索功能)
    - [安装PostgreSQL](#安装postgresql)
    - [简单的查询操作](#简单的查询操作)
    - [多字段搜索](#多字段搜索)
    - [构建搜索视图](#构建搜索视图)
    - [词干提取和排名](#词干提取和排名)
    - [加权查询](#加权查询)
    - [利用三元相似性进行搜索](#利用三元相似性进行搜索)
    - [其他全文本搜索引擎](#其他全文本搜索引擎)
  - [本章小结](#本章小结-2)
- [构建社交型网站](#构建社交型网站)
  - [创建社交型网站](#创建社交型网站)
  - [使用Django验证框架](#使用django验证框架)
    - [构建登录视图](#构建登录视图)
    - [使用Django验证视图](#使用django验证视图)
    - [登录和注销视图](#登录和注销视图)
    - [修改密码视图](#修改密码视图)
    - [重置密码视图](#重置密码视图)
  - [用户注册和用户配置](#用户注册和用户配置)
    - [用户注册](#用户注册)
    - [扩展用户模型](#扩展用户模型)
    - [使用自定义用户模型](#使用自定义用户模型)
    - [使用消息框架](#使用消息框架)
  - [构建自定义验证后端](#构建自定义验证后端)
  - [向站点中添加社交网站验证](#向站点中添加社交网站验证)
    - [通过HTTPS运行开发服务器](#通过https运行开发服务器)
    - [基于Facebook的验证](#基于facebook的验证)
    - [基于Twitter的验证](#基于twitter的验证)
    - [基于Google的验证](#基于google的验证)
  - [本章小结](#本章小结-3)
- [共享网站中的内容](#共享网站中的内容)
  - [构建图像书签网站](#构建图像书签网站)
    - [构建图像模型](#构建图像模型)
    - [生成多对多关系](#生成多对多关系)
    - [在管理站点中注册图像模型](#在管理站点中注册图像模型)
  - [发布其他站点中的内容](#发布其他站点中的内容)
    - [清空表单字段](#清空表单字段)
    - [覆写ModelForm的save()方法](#覆写modelform的save方法)
    - [利用jQuery构建书签工具](#利用jquery构建书签工具)
  - [创建图像的细节视图](#创建图像的细节视图)
  - [利用easy-thumbnails生成图像缩略图](#利用easy-thumbnails生成图像缩略图)
  - [利用jQuery添加AJAX操作](#利用jquery添加ajax操作)
    - [加载jQuery](#加载jquery)
    - [AJAX请求中的跨站点请求伪造](#ajax请求中的跨站点请求伪造)
    - [利用jQuery执行AJAX请求](#利用jquery执行ajax请求)
  - [针对视图创建自定义装饰器](#针对视图创建自定义装饰器)
  - [向列表视图中添加AJAX分页机制](#向列表视图中添加ajax分页机制)
  - [本章小结](#本章小结-4)
- [跟踪用户活动](#跟踪用户活动)
  - [构建关注系统](#构建关注系统)
    - [利用中间模型创建多对多关系](#利用中间模型创建多对多关系)
    - [针对用户配置创建列表和详细视图](#针对用户配置创建列表和详细视图)
    - [构建AJAX视图以关注用户](#构建ajax视图以关注用户)
  - [构建通用活动流应用程序](#构建通用活动流应用程序)
    - [使用contenttypes框架](#使用contenttypes框架)
    - [向模型中添加通用关系](#向模型中添加通用关系)
    - [避免活动流中的重复内容](#避免活动流中的重复内容)
    - [向活动流中添加用户活动](#向活动流中添加用户活动)
    - [显示活动流](#显示活动流)
    - [优化涉及关系对象的QuerySet](#优化涉及关系对象的queryset)
    - [针对操作活动创建模板](#针对操作活动创建模板)
  - [利用信号实现反规范化计数](#利用信号实现反规范化计数)
    - [与信号协同工作](#与信号协同工作)
    - [应用程序配置类](#应用程序配置类)
  - [利用Redis存储数据项视图](#利用redis存储数据项视图)
    - [安装Redis](#安装redis)
    - [结合Python使用Redis](#结合python使用redis)
    - [将数据视图存储于Redis中](#将数据视图存储于redis中)
    - [将排名结果存储于数据库中](#将排名结果存储于数据库中)
    - [Redis特性](#redis特性)
  - [本章小结](#本章小结-5)
- [构建在线商店](#构建在线商店)
  - [创建在线商店项目](#创建在线商店项目)
    - [创建商品目录模型](#创建商品目录模型)
    - [注册管理站点上的目录模型](#注册管理站点上的目录模型)
    - [构建目录视图](#构建目录视图)
    - [创建目录模板](#创建目录模板)
  - [创建购物车](#创建购物车)
    - [使用Django会话](#使用django会话)
    - [会话设置](#会话设置)
    - [会话过期](#会话过期)
    - [将购物车存储于会话中](#将购物车存储于会话中)
    - [创建购物车视图](#创建购物车视图)
    - [针对购物车创建上下文处理器](#针对购物车创建上下文处理器)
  - [注册客户订单](#注册客户订单)
    - [创建订单模型](#创建订单模型)
    - [在管理站点中包含订单模型](#在管理站点中包含订单模型)
    - [创建客户订单](#创建客户订单)
  - [利用Celery启动异步任务](#利用celery启动异步任务)
    - [安装Celery](#安装celery)
    - [安装RabbitMQ](#安装rabbitmq)
    - [向项目中添加Celery](#向项目中添加celery)
    - [向应用程序中添加异步任务](#向应用程序中添加异步任务)
    - [监视Celery](#监视celery)
  - [本章小结](#本章小结-6)
- [管理支付操作和订单](#管理支付操作和订单)
  - [整合支付网关](#整合支付网关)
    - [创建Braintree沙箱账号](#创建braintree沙箱账号)
    - [安装Braintree Python模块](#安装braintree-python模块)
    - [集成支付网关](#集成支付网关)
    - [使用托管字段集成Braintree](#使用托管字段集成braintree)
    - [支付的测试操作](#支付的测试操作)
    - [上线](#上线)
  - [将订单导出为CSV文件](#将订单导出为csv文件)
  - [利用自定义视图扩展管理站点](#利用自定义视图扩展管理站点)
  - [动态生成PDF发票](#动态生成pdf发票)
    - [安装WeasyPrint](#安装weasyprint)
    - [创建PDF模板](#创建pdf模板)
    - [显示PDF文件](#显示pdf文件)
    - [通过电子邮件发送PDF文件](#通过电子邮件发送pdf文件)
  - [本章小结](#本章小结-7)
- [扩展在线商店应用程序](#扩展在线商店应用程序)
  - [创建优惠券系统](#创建优惠券系统)
    - [构建优惠券模型](#构建优惠券模型)
    - [在购物车中使用优惠券](#在购物车中使用优惠券)
    - [在订单中使用优惠券](#在订单中使用优惠券)
  - [添加国际化和本地化机制](#添加国际化和本地化机制)
    - [Django的国际化处理](#django的国际化处理)
    - [项目的国际化](#项目的国际化)
    - [翻译Python代码](#翻译python代码)
    - [翻译模板](#翻译模板)
    - [使用Rosetta翻译接口](#使用rosetta翻译接口)
    - [模糊翻译](#模糊翻译)
    - [国际化的URL路径](#国际化的url路径)
    - [切换语言](#切换语言)
    - [利用django-parler翻译模块](#利用django-parler翻译模块)
    - [本地化格式](#本地化格式)
    - [使用django-localflavor验证表单字段](#使用django-localflavor验证表单字段)
  - [构建推荐引擎](#构建推荐引擎)
  - [本章小结](#本章小结-8)
- [打造网络教学平台](#打造网络教学平台)
  - [设置网络教学项目](#设置网络教学项目)
  - [构建课程模型](#构建课程模型)
    - [在管理站点中注册模型](#在管理站点中注册模型)
    - [使用固定文件提供模型的初始数据](#使用固定文件提供模型的初始数据)
  - [创建包含多样化内容的模型](#创建包含多样化内容的模型)
    - [使用模型继承机制](#使用模型继承机制)
    - [创建内容模型](#创建内容模型)
    - [创建自定义模型字段](#创建自定义模型字段)
    - [向模块和内容对象中添加顺序机制](#向模块和内容对象中添加顺序机制)
  - [创建CMS](#创建cms)
    - [添加认证系统](#添加认证系统)
    - [创建认证模板](#创建认证模板)
    - [设置基于类的视图](#设置基于类的视图)
    - [针对基于类的视图使用混合类](#针对基于类的视图使用混合类)
    - [分组和权限](#分组和权限)
    - [限制访问基于类的视图](#限制访问基于类的视图)
  - [管理课程模块和内容](#管理课程模块和内容)
    - [针对课程模块使用表单集](#针对课程模块使用表单集)
    - [向课程模块中添加内容](#向课程模块中添加内容)
    - [管理模块和内容](#管理模块和内容)
    - [对模块和内容重排序](#对模块和内容重排序)
  - [本章小结](#本章小结-9)
- [渲染和缓存内容](#渲染和缓存内容)
  - [显示课程](#显示课程)
  - [添加学生注册机制](#添加学生注册机制)
    - [创建学生注册视图](#创建学生注册视图)
    - [注册课程](#注册课程)
  - [访问课程内容](#访问课程内容)
  - [渲染不同内容的类型](#渲染不同内容的类型)
  - [使用缓存框架](#使用缓存框架)
    - [有效的缓存后端](#有效的缓存后端)
    - [安装Memcached](#安装memcached)
    - [缓存设置](#缓存设置)
    - [向项目中添加Memcached](#向项目中添加memcached)
    - [监控Memcached](#监控memcached)
    - [缓存级别](#缓存级别)
    - [使用底层缓存API](#使用底层缓存api)
    - [缓存动态数据](#缓存动态数据)
    - [缓存模板片段](#缓存模板片段)
    - [缓存视图](#缓存视图)
    - [使用每个站点缓存](#使用每个站点缓存)
  - [本章小结](#本章小结-10)
- [构建API](#构建api)
  - [构建RESTful API](#构建restful-api)
    - [安装Django REST框架](#安装django-rest框架)
    - [定义序列化器](#定义序列化器)
    - [理解解析器和渲染器](#理解解析器和渲染器)
    - [构建列表和详细视图](#构建列表和详细视图-1)
    - [创建嵌套序列化器](#创建嵌套序列化器)
    - [构建自定义视图](#构建自定义视图)
    - [处理身份验证](#处理身份验证)
    - [向视图中添加权限](#向视图中添加权限)
    - [创建视图集和路由器](#创建视图集和路由器)
    - [向视图集添加附加操作](#向视图集添加附加操作)
    - [创建自定义权限](#创建自定义权限)
    - [序列化课程内容](#序列化课程内容)
    - [使用RESTful API](#使用restful-api)
  - [本章小结](#本章小结-11)
- [搭建聊天服务器](#搭建聊天服务器)
  - [创建聊天应用程序](#创建聊天应用程序)
    - [实现聊天室视图](#实现聊天室视图)
    - [禁用站点缓存](#禁用站点缓存)
  - [基于Channels的实时Django](#基于channels的实时django)
    - [基于ASGI的异步应用程序](#基于asgi的异步应用程序)
    - [基于Channels的请求/响应周期](#基于channels的请求响应周期)
  - [安装Channels](#安装channels)
  - [编写使用者](#编写使用者)
  - [路由机制](#路由机制)
  - [实现WebSocket客户端](#实现websocket客户端)
  - [启用通道层](#启用通道层)
    - [通道和分组](#通道和分组)
    - [利用Redis设置通道层](#利用redis设置通道层)
    - [更新使用者以广播消息](#更新使用者以广播消息)
    - [将上下文添加至消息中](#将上下文添加至消息中)
  - [调整使用者使其处于完全异步状态](#调整使用者使其处于完全异步状态)
  - [集成聊天应用程序和视图](#集成聊天应用程序和视图)
  - [本章小结](#本章小结-12)
- [部署项目](#部署项目)
  - [创建产品环境](#创建产品环境)
    - [针对多种环境管理设置内容](#针对多种环境管理设置内容)
    - [使用PostgreSQL](#使用postgresql)
    - [项目检查](#项目检查)
    - [通过WSGI为Django提供服务](#通过wsgi为django提供服务)
    - [安装uWSGI](#安装uwsgi)
    - [配置uWSGI](#配置uwsgi)
    - [安装NGINX](#安装nginx)
    - [产品环境](#产品环境)
    - [配置NGINX](#配置nginx)
    - [向静态和媒体数据集提供服务](#向静态和媒体数据集提供服务)
    - [基于SSL/TLS的安全连接](#基于ssltls的安全连接)
    - [针对Django Channels使用Daphne](#针对django-channels使用daphne)
    - [使用安全的WebSocket连接](#使用安全的websocket连接)
    - [将Daphne包含于NGINX配置中](#将daphne包含于nginx配置中)
  - [创建自定义中间件](#创建自定义中间件)
    - [创建子域名中间件](#创建子域名中间件)
    - [利用NGINX向多个子域名提供服务](#利用nginx向多个子域名提供服务)
  - [实现自定义管理命令](#实现自定义管理命令)
  - [本章小结](#本章小结-13)
- [参考链接](#参考链接)

<!-- /TOC -->

# 构建博客应用程序



## 安装Django



### 创建隔离的Python环境



### 利用pip安装Django



## 创建第一个项目



### 运行开发服务器



### 项目设置



### 项目和应用程序



### 创建应用程序



## 设计博客数据方案



### 激活应用程序



### 设置并使用迁移方案



## 针对模型创建管理站点



### 创建超级用户



### Django管理站点



### 向管理站点中添加模型



### 定制模型的显示方式



## 与QuerySet和管理器协同工作



### 创建对象



### 更新对象



### 检索对象



### 删除对象



### 评估QuerySet



### 创建模型管理器



## 构建列表和详细视图



### 生成列表和视图



### 向视图添加URL路径



### 模型的标准URL



## 创建视图模板



## 添加分页机制



## 使用基于类的视图



## 本章小结



# 利用高级特性完善博客程序



## 通过电子邮件共享帖子



### 使用Django创建表单



### 处理视图中的表单



### 利用Django发送邮件



### 渲染模板中的表单



## 构建评论系统



### 构建模型



### 创建模型中的表单



### 处理视图中的ModelForms



### 向帖子详细模板中添加评论



## 添加标签功能



## 根据相似性检索帖子



## 本章小结



# 扩展博客应用程序



## 创建自定义模板标签和过滤器



### 自定义模板标签



### 自定义模板过滤器



## 向站点添加网站地图



## 创建帖子提要



## 向博客中添加全文本搜索功能



### 安装PostgreSQL



### 简单的查询操作



### 多字段搜索



### 构建搜索视图



### 词干提取和排名



### 加权查询



### 利用三元相似性进行搜索



### 其他全文本搜索引擎



## 本章小结



# 构建社交型网站



## 创建社交型网站



## 使用Django验证框架



### 构建登录视图



### 使用Django验证视图



### 登录和注销视图



### 修改密码视图



### 重置密码视图



## 用户注册和用户配置



### 用户注册



### 扩展用户模型



### 使用自定义用户模型



### 使用消息框架



## 构建自定义验证后端



## 向站点中添加社交网站验证



### 通过HTTPS运行开发服务器



### 基于Facebook的验证



### 基于Twitter的验证



### 基于Google的验证



## 本章小结



# 共享网站中的内容



## 构建图像书签网站



### 构建图像模型



### 生成多对多关系



### 在管理站点中注册图像模型



## 发布其他站点中的内容



### 清空表单字段



### 覆写ModelForm的save()方法



### 利用jQuery构建书签工具



## 创建图像的细节视图



## 利用easy-thumbnails生成图像缩略图



## 利用jQuery添加AJAX操作



### 加载jQuery



### AJAX请求中的跨站点请求伪造



### 利用jQuery执行AJAX请求



## 针对视图创建自定义装饰器



## 向列表视图中添加AJAX分页机制



## 本章小结



# 跟踪用户活动



## 构建关注系统



### 利用中间模型创建多对多关系



### 针对用户配置创建列表和详细视图



### 构建AJAX视图以关注用户



## 构建通用活动流应用程序



### 使用contenttypes框架



### 向模型中添加通用关系



### 避免活动流中的重复内容



### 向活动流中添加用户活动



### 显示活动流



### 优化涉及关系对象的QuerySet



### 针对操作活动创建模板



## 利用信号实现反规范化计数



### 与信号协同工作



### 应用程序配置类



## 利用Redis存储数据项视图



### 安装Redis



### 结合Python使用Redis



### 将数据视图存储于Redis中



### 将排名结果存储于数据库中



### Redis特性



## 本章小结



# 构建在线商店



## 创建在线商店项目



### 创建商品目录模型



### 注册管理站点上的目录模型



### 构建目录视图



### 创建目录模板



## 创建购物车



### 使用Django会话



### 会话设置



### 会话过期



### 将购物车存储于会话中



### 创建购物车视图



### 针对购物车创建上下文处理器



## 注册客户订单



### 创建订单模型



### 在管理站点中包含订单模型



### 创建客户订单



## 利用Celery启动异步任务



### 安装Celery



### 安装RabbitMQ



### 向项目中添加Celery



### 向应用程序中添加异步任务



### 监视Celery



## 本章小结



# 管理支付操作和订单



## 整合支付网关



### 创建Braintree沙箱账号



### 安装Braintree Python模块



### 集成支付网关



### 使用托管字段集成Braintree



### 支付的测试操作



### 上线



## 将订单导出为CSV文件



## 利用自定义视图扩展管理站点



## 动态生成PDF发票



### 安装WeasyPrint



### 创建PDF模板



### 显示PDF文件



### 通过电子邮件发送PDF文件



## 本章小结



# 扩展在线商店应用程序



## 创建优惠券系统



### 构建优惠券模型



### 在购物车中使用优惠券



### 在订单中使用优惠券



## 添加国际化和本地化机制



### Django的国际化处理



### 项目的国际化



### 翻译Python代码



### 翻译模板



### 使用Rosetta翻译接口



### 模糊翻译



### 国际化的URL路径



### 切换语言



### 利用django-parler翻译模块



### 本地化格式



### 使用django-localflavor验证表单字段



## 构建推荐引擎



## 本章小结



# 打造网络教学平台



## 设置网络教学项目



## 构建课程模型



### 在管理站点中注册模型



### 使用固定文件提供模型的初始数据



## 创建包含多样化内容的模型



### 使用模型继承机制



### 创建内容模型



### 创建自定义模型字段



### 向模块和内容对象中添加顺序机制



## 创建CMS



### 添加认证系统



### 创建认证模板



### 设置基于类的视图



### 针对基于类的视图使用混合类



### 分组和权限



### 限制访问基于类的视图



## 管理课程模块和内容



### 针对课程模块使用表单集



### 向课程模块中添加内容



### 管理模块和内容



### 对模块和内容重排序



## 本章小结



# 渲染和缓存内容



## 显示课程



## 添加学生注册机制



### 创建学生注册视图



### 注册课程



## 访问课程内容



## 渲染不同内容的类型



## 使用缓存框架



### 有效的缓存后端



### 安装Memcached



### 缓存设置



### 向项目中添加Memcached



### 监控Memcached



### 缓存级别



### 使用底层缓存API



### 缓存动态数据



### 缓存模板片段



### 缓存视图



### 使用每个站点缓存



## 本章小结



# 构建API



## 构建RESTful API



### 安装Django REST框架



### 定义序列化器



### 理解解析器和渲染器



### 构建列表和详细视图



### 创建嵌套序列化器



### 构建自定义视图



### 处理身份验证



### 向视图中添加权限



### 创建视图集和路由器



### 向视图集添加附加操作



### 创建自定义权限



### 序列化课程内容



### 使用RESTful API



## 本章小结



# 搭建聊天服务器



## 创建聊天应用程序



### 实现聊天室视图



### 禁用站点缓存



## 基于Channels的实时Django



### 基于ASGI的异步应用程序



### 基于Channels的请求/响应周期



## 安装Channels



## 编写使用者



## 路由机制



## 实现WebSocket客户端



## 启用通道层



### 通道和分组



### 利用Redis设置通道层



### 更新使用者以广播消息



### 将上下文添加至消息中



## 调整使用者使其处于完全异步状态



## 集成聊天应用程序和视图



## 本章小结



# 部署项目



## 创建产品环境



### 针对多种环境管理设置内容



### 使用PostgreSQL



### 项目检查



### 通过WSGI为Django提供服务



### 安装uWSGI



### 配置uWSGI



### 安装NGINX



### 产品环境



### 配置NGINX



### 向静态和媒体数据集提供服务



### 基于SSL/TLS的安全连接



### 针对Django Channels使用Daphne



### 使用安全的WebSocket连接



### 将Daphne包含于NGINX配置中



## 创建自定义中间件



### 创建子域名中间件



### 利用NGINX向多个子域名提供服务



## 实现自定义管理命令



## 本章小结

# 参考链接

> [京东 | Django 3项目实例精解](https://item.jd.com/12878657.html)