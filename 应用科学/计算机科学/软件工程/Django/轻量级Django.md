
<!-- TOC -->

- [前言](#前言)
- [绪论](#绪论)
- [世界上最小的Django 项目](#世界上最小的django-项目)
  - [你好Django](#你好django)
  - [改进](#改进)
- [无状态的Web 应用](#无状态的web-应用)
  - [什么是无状态？](#什么是无状态)
  - [可复用应用与可组合服务](#可复用应用与可组合服务)
  - [占位图片服务器](#占位图片服务器)
  - [占位视图](#占位视图)
  - [建主页面视图](#建主页面视图)
- [创建静态站点生成器](#创建静态站点生成器)
  - [使用Django 创建静态站点](#使用django-创建静态站点)
  - [什么是快速原型？](#什么是快速原型)
  - [最初的项目结构](#最初的项目结构)
  - [修饰页面](#修饰页面)
  - [生成静态内容](#生成静态内容)
  - [处理和压缩静态文件](#处理和压缩静态文件)
  - [生成动态内容](#生成动态内容)
- [构建REST API](#构建rest-api)
  - [Django 和REST](#django-和rest)
  - [Scrum 板数据图](#scrum-板数据图)
  - [设计API](#设计api)
  - [测试API](#测试api)
  - [下一步](#下一步)
- [使用Backbone.js 的客户端Django](#使用backbonejs-的客户端django)
  - [Backbone 简述](#backbone-简述)
  - [设置项目文件](#设置项目文件)
  - [连接Backbone 到Django](#连接backbone-到django)
  - [客户端Backbone 路由](#客户端backbone-路由)
  - [构建用户认证](#构建用户认证)
- [单页面Web 应用](#单页面web-应用)
  - [什么是单页面Web 应用？](#什么是单页面web-应用)
  - [发现API](#发现api)
  - [构建主页](#构建主页)
  - [sprint 详情页面](#sprint-详情页面)
  - [CRUD 任务](#crud-任务)
- [实时Django](#实时django)
  - [HTML 实时API](#html-实时api)
  - [在Tornado 下使用websocket](#在tornado-下使用websocket)
  - [客户端通信](#客户端通信)
- [Django 与Tornado 通信](#django-与tornado-通信)
  - [从Tornado 接收更新](#从tornado-接收更新)
  - [改善服务器](#改善服务器)
  - [最终的websocket 服务器](#最终的websocket-服务器)
- [参考资料](#参考资料)

<!-- /TOC -->

# 前言



# 绪论



# 世界上最小的Django 项目 



## 你好Django



## 改进



# 无状态的Web 应用



## 什么是无状态？



## 可复用应用与可组合服务



## 占位图片服务器



## 占位视图



## 建主页面视图



# 创建静态站点生成器



## 使用Django 创建静态站点



## 什么是快速原型？



## 最初的项目结构



## 修饰页面



## 生成静态内容



## 处理和压缩静态文件



## 生成动态内容



# 构建REST API



## Django 和REST



## Scrum 板数据图



## 设计API



## 测试API



## 下一步



# 使用Backbone.js 的客户端Django



## Backbone 简述



## 设置项目文件



## 连接Backbone 到Django



## 客户端Backbone 路由



## 构建用户认证



# 单页面Web 应用



## 什么是单页面Web 应用？



## 发现API



## 构建主页



## sprint 详情页面



## CRUD 任务



# 实时Django



## HTML 实时API 



## 在Tornado 下使用websocket



## 客户端通信



# Django 与Tornado 通信



## 从Tornado 接收更新



## 改善服务器



## 最终的websocket 服务器

# 参考资料

> [京东 | 轻量级Django](https://item.jd.com/12059360.html)