@集群.可用节点.each do |目标, 详情|
  详情['任务目录'] = [@集群.任务目录, 目标].join('/')

  FileUtils.rm_r(详情['任务目录']) if File.directory?(详情['任务目录'])
  FileUtils.mkdir_p(详情['任务目录'])

  详情["节点任务目录"].each_slice(1000) do |批次任务目录|
    `ln -s #{批次任务目录.join(' ')} #{详情['任务目录']}`
    断点调试(binding) unless $?.exitstatus == 0
  end
end