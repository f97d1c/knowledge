性能测试 = lambda do |目标:, 对象:|
  # 待分配任务 = Dir["#{对象.目标目录}/*"]
  待分配任务 = Dir[对象.目标目录]
  节点 = 对象.可用节点[目标]
  断点调试(binding) if 待分配任务.size == 0
  设备分配目录 = 待分配任务.slice!(0, (节点['并行使用线程数'] || (节点['线程数']- 1)))

  节点任务目录 = [对象.任务目录, 目标].join('/')

  FileUtils.rm_r(节点任务目录) if File.directory?(节点任务目录)
  FileUtils.mkdir_p(节点任务目录)

  设备分配目录.each_slice(1000) do |批次任务目录|
    `ln -s #{批次任务目录.join(' ')} #{节点任务目录}`
    断点调试(binding) unless $?.exitstatus == 0
  end

  对象.参数 = JSON.parse(对象.参数.to_json.gsub('{节点任务目录}', 节点任务目录))
  对象.参数['参数']['禁用缓存随机参数'] = rand(0xffffff) if 对象.参数['参数']
  
  参数文件 = [节点任务目录, ".#{Digest::MD5.hexdigest(对象.参数.to_json)}.json"].join('/')
  File.open(参数文件, 'w'){|f| f.write(对象.参数.to_json)}

  # fork do
    开始时间 = Time.now.to_f
    result = 对象.执行容器命令(说明: '执行节点具体任务', 执行文件: 对象.执行文件, 参数: 参数文件, 操作目标: "#{节点['系统用户名'] || 'ubuntu'}@#{节点['IPV4地址']}")
    if result[0]
      结束时间 = Time.now.to_f
      对象.日志记录(日志类型: '任务时耗', 目标: 目标, 写入: {
        '开始时间': 开始时间,
        '结束时间': 结束时间,
        '任务数量': 设备分配目录.size,
        '总时耗': (结束时间 - 开始时间),
        '平均时耗': (结束时间 - 开始时间) / 设备分配目录.size,
      })
    else
      File.open([对象.任务目录, "#{目标}.log"].join('/'), 'w'){|文件对象| 文件对象.write(result.to_json)}
    end

  # end
end

@集群.可用节点.each do |目标, 详情|
  while !(状态||=false) do
    状态, 日志 = @集群.日志读取(目标: 目标, 日志类型: '任务时耗')
    性能测试.call(目标: 目标, 对象: @集群) if 状态 == false && 日志 == 404
  end

  # 单任务时耗 = 日志.map{|info| (info['结束时间'] - info['开始时间']).to_f / info['任务数量']}
  # 详情['平均时耗'] = 单任务时耗.sum.to_f/单任务时耗.size
  样本 = 日志.select{|info| info['平均时耗'] != nil}
  详情['平均时耗'] = 样本.map{|info| info['平均时耗']}.sum.to_f/样本.size
  断点调试(binding) if 详情['平均时耗'] == 0
end