requires = {
  '断点调试(binding)' => 'pry',
  'Http请求'         => 'rest-client',
  'JSON处理'         => 'json',
  '文字转拼音'       => 'ruby-pinyin',
  'Html文本解析'     => 'nokogiri',
  '文件系统工具'     => 'fileutils',
  'uri'             => 'uri',
  'base64'          => 'base64',
  'CGI接口实现'      => 'cgi', # 主要用于链接内容转义
  'open3'           => 'open3', # Ruby标准库中的一个模块,它提供了一个方便的接口来执行外部命令,并且可以捕获命令的输出、错误和状态
}

requires.each do |desc, gem_name|
  begin
    require gem_name
  rescue LoadError => e
    puts "缺少Gem: #{gem_name}(用于#{desc}), 正在尝试安装该Gem."
    `gem install #{gem_name}`
    断点调试(binding, 备注: "#{gem_name}安装失败") unless $?.exitstatus == 0
  end
end

@参数 = {}
if File::file?(ARGV[0])
  @ARGV = JSON.parse(File.read(ARGV[0]))
else
  @ARGV = JSON.parse(ARGV[0])
end

def 断点调试(binding, 备注 = "无")
  return binding.pry unless ARGV[0]["运行模式"]
  # 目前并行模式下无法进行断点调试(binding) 只能提供相关信息后退出程序
  time = Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")
  print "[#{time}] 运行异常: 运行过程中遇到无法处理的异常,将退出程序等待后续排查.\n"
  message = [false, "运行异常", { 时间: time, 地点: caller, 原因: "运行过程中遇到无法处理", 备注: 备注, 参数: @运行参数 }]
  print JSON.pretty_generate(message)
  print "\n"
  print message.to_json
  exit 255
end

遍历执行脚本 = lambda do |路径:, 层级:-1|
  if File::directory?(路径)
    流程判断文件 = Dir["#{路径}/流程判断.rb"][0]
    if 流程判断文件
      ARGV[1] = [false, '']
      require 流程判断文件
      return unless ARGV[1][0]
    end

    Dir["#{路径}/**"].sort.each do |d|
      遍历执行脚本.call(路径: d, 层级: 层级+1)
    end
  elsif 层级 != 0 && File::file?(路径)
    require 路径 if /.*\.rb$/ =~ 路径
  end
end

遍历执行脚本.call(路径: Dir.pwd)

puts [true, @output].to_json if @output