return if @参数['可用节点']
@参数['可用节点'] = {}

@参数['可控节点'].each do |标识, 详情|
  ipv4 = 详情['IPV4地址']

  打印输出(内容: "可用检查: #{ipv4}")
  res = 执行SSH命令(用户名: (详情['系统用户名'] || 'ubuntu'), 地址: ipv4, 命令: "docker ps")
  raise [false, "#{ipv4} 节点可用容器检查失败"].to_json unless res[1].include?('self.container.system')

  打印输出(内容: "节点可用")
  @参数['可用节点'][标识] = 详情
end

打印输出(内容: "可用节点: #{@参数['可用节点'].keys.join('/')}")