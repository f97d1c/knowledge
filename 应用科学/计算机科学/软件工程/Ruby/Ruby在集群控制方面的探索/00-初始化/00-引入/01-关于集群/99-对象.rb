class J集群
  include ClusterLog
  # include ClusterWeight

  def initialize(**params)
    任务标识 = Digest::MD5.hexdigest(params.to_json)
    挂载目录 = '/home/ubuntu/nfs'

    params.merge!({
      任务标识: 任务标识,
      归档时间: Time.now.strftime("%Y-%m-%dT%H:%M:%S.%L"),
      日志目录地址: "#{挂载目录}/log",
      任务目录: [挂载目录, 任务标识].join('/'),
      挂载目录: 挂载目录,
    })

    params.each do |属性名, 属性值|
      self.class.attr_accessor "#{属性名}"
      self.instance_variable_set("@#{属性名}", 属性值)
    end
  end
  
  def 执行容器命令(说明:, 执行文件:, 参数:, 操作目标:, 容器名称: 'self.container.system')
    执行目录 = 执行文件.split('/')[0...-1].join('/')
    执行文件名 = 执行文件.split('/')[-1]
    print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 开始执行: #{说明}\n"

    shell = %Q|ssh -o StrictHostKeyChecking=no -T #{操作目标} "docker exec --workdir '#{执行目录}' #{容器名称} bash -cl 'bash #{执行文件名} \"#{参数}\"'"|
    print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 具体命令: #{shell}\n"

    stdout_str, stderr_str, status = Open3.capture3(shell)
    断点调试(binding) unless status.success?
    result = stdout_str.split("\n")[-1]
    result = (JSON.parse(result) rescue 断点调试(binding))
    print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S.%L")}] 返回结果: \n#{result}"
    result
  end

end