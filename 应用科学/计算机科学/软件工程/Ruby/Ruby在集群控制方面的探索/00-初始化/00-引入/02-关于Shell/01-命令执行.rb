def 执行Shell命令(命令:)
  stdout_str, stderr_str, status = Open3.capture3(命令)  
  断点调试(binding) unless status.success?
  return [false, ''] unless status.success?

  return [stdout_str.split("\n").select{|i| i != ''}, stdout_str]
end

def 执行SSH命令(用户名:, 地址:, 命令:)
  return 执行Shell命令(命令: %Q|ssh -n -o PasswordAuthentication=no -o StrictHostKeyChecking=no #{用户名}@#{地址} "#{命令}"|)
end
