<!-- TOC -->

- [基础知识](#基础知识)
  - [Oracle概述](#oracle概述)
    - [简述Oracle的发展史](#简述oracle的发展史)
    - [关系型数据库的基本理论](#关系型数据库的基本理论)
      - [关系型数据库与数据库管理系统](#关系型数据库与数据库管理系统)
      - [关系型数据库的E-R模型](#关系型数据库的e-r模型)
      - [关系型数据库的设计范式](#关系型数据库的设计范式)
    - [Oracle的新功能](#oracle的新功能)
    - [Oracle的安装与卸载](#oracle的安装与卸载)
      - [Oracle的安装](#oracle的安装)
      - [Oracle的卸载](#oracle的卸载)
    - [Oracle的管理工具](#oracle的管理工具)
      - [SQL*Plus工具](#sqlplus工具)
      - [Oracle企业管理器](#oracle企业管理器)
      - [数据库配置助手](#数据库配置助手)
    - [启动与关闭数据库实例](#启动与关闭数据库实例)
      - [启动数据库实例](#启动数据库实例)
      - [关闭数据库实例](#关闭数据库实例)
  - [Oracle体系结构](#oracle体系结构)
    - [Oracle体系结构概述](#oracle体系结构概述)
    - [逻辑存储结构](#逻辑存储结构)
      - [数据块(Data Blocks)](#数据块data-blocks)
      - [数据区(Extent)](#数据区extent)
      - [段(Segment)](#段segment)
      - [表空间(TableSpace)](#表空间tablespace)
    - [物理存储结构](#物理存储结构)
      - [数据文件](#数据文件)
      - [控制文件](#控制文件)
      - [日志文件](#日志文件)
      - [服务器参数文件](#服务器参数文件)
      - [密码文件、警告文件和跟踪文件](#密码文件警告文件和跟踪文件)
    - [Oracle服务器结构](#oracle服务器结构)
      - [系统全局区(SGA)](#系统全局区sga)
      - [程序全局区(PGA)](#程序全局区pga)
      - [前台进程](#前台进程)
      - [后台进程](#后台进程)
    - [数据字典](#数据字典)
      - [Oracle数据字典简介](#oracle数据字典简介)
      - [Oracle常用数据字典](#oracle常用数据字典)
  - [SQL*Plus命令](#sqlplus命令)
    - [SQL*Plus与数据库的交互](#sqlplus与数据库的交互)
    - [设置SQL*Plus的运行环境](#设置sqlplus的运行环境)
      - [简介SET命令](#简介set命令)
      - [使用SET命令设置运行环境](#使用set命令设置运行环境)
    - [常用SQL*Plus命令](#常用sqlplus命令)
      - [HELP命令](#help命令)
      - [DESCRIBE命令](#describe命令)
      - [SPOOL命令](#spool命令)
      - [其他常用命令](#其他常用命令)
    - [格式化查询结果](#格式化查询结果)
      - [COLUMN命令](#column命令)
      - [TTITLE和BTITLE命令](#ttitle和btitle命令)
  - [SQL语言基础](#sql语言基础)
    - [SQL语言简介](#sql语言简介)
      - [SQL语言的特点](#sql语言的特点)
      - [SQL语言的分类](#sql语言的分类)
      - [SQL语言的编写规则](#sql语言的编写规则)
    - [用户模式](#用户模式)
      - [模式与模式对象](#模式与模式对象)
      - [示例模式SCOTT](#示例模式scott)
      - [创建只读用户](#创建只读用户)
        - [创建用户](#创建用户)
        - [赋连接权限](#赋连接权限)
        - [赋表权限](#赋表权限)
        - [创建同义词](#创建同义词)
    - [检索数据](#检索数据)
      - [简单查询](#简单查询)
      - [筛选查询(WHERE)](#筛选查询where)
        - [正则匹配字段内容(REGEXP_LIKE)](#正则匹配字段内容regexp_like)
      - [分组查询](#分组查询)
      - [排序查询](#排序查询)
      - [多表关联查询](#多表关联查询)
    - [Oracle常用系统函数](#oracle常用系统函数)
      - [字符类函数](#字符类函数)
      - [数字类函数](#数字类函数)
      - [日期和时间类函数](#日期和时间类函数)
      - [转换类函数](#转换类函数)
      - [聚合类函数](#聚合类函数)
    - [子查询的用法](#子查询的用法)
      - [什么是子查询](#什么是子查询)
      - [单行子查询](#单行子查询)
      - [多行子查询](#多行子查询)
      - [关联子查询](#关联子查询)
    - [操作数据库](#操作数据库)
      - [插入数据(INSERT语句)](#插入数据insert语句)
      - [更新数据(UPDATE语句)](#更新数据update语句)
      - [删除数据(DELETE语句和TRUNCATE语句)](#删除数据delete语句和truncate语句)
  - [PL/SQL编程](#plsql编程)
    - [PL/SQL简介](#plsql简介)
      - [PL/SQL块结构](#plsql块结构)
      - [代码注释和标示符](#代码注释和标示符)
      - [文本](#文本)
    - [数据类型、变量和常量](#数据类型变量和常量)
      - [基本数据类型](#基本数据类型)
      - [特殊数据类型](#特殊数据类型)
      - [定义变量和常量](#定义变量和常量)
      - [PL/SQL表达式](#plsql表达式)
    - [流程控制语句](#流程控制语句)
      - [选择语句](#选择语句)
      - [循环语句](#循环语句)
    - [PL/SQL游标](#plsql游标)
      - [基本原理](#基本原理)
      - [显式游标](#显式游标)
      - [隐式游标](#隐式游标)
      - [游标的属性](#游标的属性)
      - [游标变量](#游标变量)
      - [通过for语句循环游标](#通过for语句循环游标)
    - [PL/SQL异常处理](#plsql异常处理)
      - [异常处理方法](#异常处理方法)
      - [异常处理语法](#异常处理语法)
      - [预定义异常](#预定义异常)
      - [自定义异常](#自定义异常)
  - [过程、函数、触发器和包](#过程函数触发器和包)
    - [存储过程](#存储过程)
      - [创建存储过程](#创建存储过程)
      - [存储过程的参数](#存储过程的参数)
      - [IN参数的默认值](#in参数的默认值)
      - [删除存储过程](#删除存储过程)
    - [函数](#函数)
      - [创建函数](#创建函数)
      - [调用函数](#调用函数)
      - [删除函数](#删除函数)
    - [触发器](#触发器)
      - [触发器简介](#触发器简介)
      - [语句级触发器](#语句级触发器)
      - [行级触发器](#行级触发器)
      - [替换触发器](#替换触发器)
      - [用户事件触发器](#用户事件触发器)
      - [删除触发器](#删除触发器)
      - [查询触发器具体内容](#查询触发器具体内容)
    - [程序包](#程序包)
      - [程序包的规范](#程序包的规范)
      - [程序包的主体](#程序包的主体)
      - [删除包](#删除包)
- [核心技术](#核心技术)
  - [管理控制文件和日志文件](#管理控制文件和日志文件)
    - [管理控制文件](#管理控制文件)
      - [控制文件简介](#控制文件简介)
      - [控制文件的多路复用](#控制文件的多路复用)
      - [创建控制文件](#创建控制文件)
      - [备份和恢复控制文件](#备份和恢复控制文件)
      - [删除控制文件](#删除控制文件)
      - [查询控制文件的信息](#查询控制文件的信息)
    - [管理重做日志文件](#管理重做日志文件)
      - [重做日志文件概述](#重做日志文件概述)
      - [增加日志组及其成员](#增加日志组及其成员)
      - [删除重做日志文件](#删除重做日志文件)
      - [更改重做日志文件的位置或名称](#更改重做日志文件的位置或名称)
      - [查看重做日志信息](#查看重做日志信息)
    - [管理归档日志文件](#管理归档日志文件)
      - [日志模式分类](#日志模式分类)
      - [管理归档操作](#管理归档操作)
      - [设置归档文件位置](#设置归档文件位置)
      - [查看归档日志信息](#查看归档日志信息)
  - [管理表空间和数据文件](#管理表空间和数据文件)
    - [表空间与数据文件的关系](#表空间与数据文件的关系)
    - [Oracle的默认表空间](#oracle的默认表空间)
      - [SYSTEM表空间](#system表空间)
      - [SYSAUX表空间](#sysaux表空间)
    - [创建表空间](#创建表空间)
      - [创建表空间的语法](#创建表空间的语法)
      - [通过本地化管理方式创建表空间](#通过本地化管理方式创建表空间)
      - [通过段空间管理方式创建表空间](#通过段空间管理方式创建表空间)
      - [创建非标准块表空间](#创建非标准块表空间)
      - [建立大文件表空间](#建立大文件表空间)
    - [维护表空间与数据文件](#维护表空间与数据文件)
      - [设置默认表空间](#设置默认表空间)
      - [更改表空间的状态](#更改表空间的状态)
      - [重命名表空间](#重命名表空间)
      - [删除表空间](#删除表空间)
      - [维护表空间中的数据文件](#维护表空间中的数据文件)
    - [管理撤销表空间](#管理撤销表空间)
      - [撤销表空间的作用](#撤销表空间的作用)
      - [撤销表空间的初始化参数](#撤销表空间的初始化参数)
      - [撤销表空间的基本操作](#撤销表空间的基本操作)
    - [管理临时表空间](#管理临时表空间)
      - [临时表空间简介](#临时表空间简介)
      - [创建临时表空间](#创建临时表空间)
      - [查询临时表空间的信息](#查询临时表空间的信息)
      - [关于临时表空间组](#关于临时表空间组)
  - [数据表对象](#数据表对象)
    - [数据表概述](#数据表概述)
    - [创建数据表](#创建数据表)
      - [数据表的逻辑结构](#数据表的逻辑结构)
      - [创建一个数据表](#创建一个数据表)
      - [数据表的特性](#数据表的特性)
    - [维护数据表](#维护数据表)
      - [增加和删除字段](#增加和删除字段)
      - [修改字段](#修改字段)
      - [重命名表](#重命名表)
      - [改变表空间和存储参数](#改变表空间和存储参数)
      - [删除表](#删除表)
        - [关于purge](#关于purge)
      - [修改表的状态](#修改表的状态)
    - [数据完整性和约束性](#数据完整性和约束性)
      - [非空约束](#非空约束)
      - [主键约束](#主键约束)
      - [唯一性约束](#唯一性约束)
      - [外键约束](#外键约束)
      - [禁用和激活约束](#禁用和激活约束)
      - [删除约束](#删除约束)
  - [其他数据对象](#其他数据对象)
    - [索引对象](#索引对象)
      - [索引概述](#索引概述)
      - [创建索引](#创建索引)
      - [修改索引](#修改索引)
      - [删除索引](#删除索引)
      - [显示索引信息](#显示索引信息)
    - [视图对象](#视图对象)
      - [创建视图](#创建视图)
      - [管理视图](#管理视图)
    - [同义词对象](#同义词对象)
    - [序列对象](#序列对象)
      - [创建序列](#创建序列)
      - [管理序列](#管理序列)
        - [查看序列的last_number](#查看序列的last_number)
        - [删除序列](#删除序列)
        - [修改序列的last_number](#修改序列的last_number)
          - [increment](#increment)
          - [restart](#restart)
  - [表分区与索引分区](#表分区与索引分区)
    - [分区技术简介](#分区技术简介)
    - [创建表分区](#创建表分区)
      - [范围分区](#范围分区)
      - [散列分区](#散列分区)
      - [列表分区](#列表分区)
- [异常编码](#异常编码)
  - [ORA-28001](#ora-28001)
  - [ORA-01653](#ora-01653)
    - [表空间预览](#表空间预览)
    - [查询是否开启表空间的自动扩展功能](#查询是否开启表空间的自动扩展功能)
      - [开启表空间自动扩展功能](#开启表空间自动扩展功能)
      - [关闭表空间自动扩展功能](#关闭表空间自动扩展功能)
    - [查询表空间的目前使用大小](#查询表空间的目前使用大小)
    - [查询表空间的分配大小](#查询表空间的分配大小)
    - [查询表空间最大扩展大小](#查询表空间最大扩展大小)
    - [查看表空间和物理文件路径](#查看表空间和物理文件路径)
    - [扩展表空间大小](#扩展表空间大小)
    - [增加数据文件](#增加数据文件)
      - [验证已经增加的数据文件](#验证已经增加的数据文件)
- [参考资料](#参考资料)

<!-- /TOC -->

# 基础知识

## Oracle概述 

### 简述Oracle的发展史 



### 关系型数据库的基本理论 



#### 关系型数据库与数据库管理系统 



#### 关系型数据库的E-R模型 



#### 关系型数据库的设计范式 



### Oracle的新功能 



### Oracle的安装与卸载 



#### Oracle的安装 



#### Oracle的卸载 



### Oracle的管理工具 



#### SQL*Plus工具 



#### Oracle企业管理器 



#### 数据库配置助手 



### 启动与关闭数据库实例 



#### 启动数据库实例 



#### 关闭数据库实例



## Oracle体系结构 

### Oracle体系结构概述 



### 逻辑存储结构 



#### 数据块(Data Blocks) 



#### 数据区(Extent) 



#### 段(Segment) 



#### 表空间(TableSpace) 



### 物理存储结构 



#### 数据文件 



#### 控制文件 



#### 日志文件 



#### 服务器参数文件 



#### 密码文件、警告文件和跟踪文件 



### Oracle服务器结构 



#### 系统全局区(SGA) 



#### 程序全局区(PGA) 



#### 前台进程 



#### 后台进程 



### 数据字典 



#### Oracle数据字典简介 



#### Oracle常用数据字典




## SQL*Plus命令 

### SQL*Plus与数据库的交互 



### 设置SQL*Plus的运行环境 



#### 简介SET命令 



#### 使用SET命令设置运行环境 



### 常用SQL*Plus命令 



#### HELP命令 



#### DESCRIBE命令 



#### SPOOL命令 



#### 其他常用命令 



### 格式化查询结果 



#### COLUMN命令 



#### TTITLE和BTITLE命令 




## SQL语言基础 

### SQL语言简介 



#### SQL语言的特点 



#### SQL语言的分类 



#### SQL语言的编写规则 



### 用户模式 

> Schemas and users help database administrators manage database security.<br>
用户是用来连接数据库对象.而模式用是用创建管理对象的.模式跟用户在oracle 是一对一的关系.

#### 模式与模式对象 



#### 示例模式SCOTT 


#### 创建只读用户

##### 创建用户

```sql
create user 用户名 identified by 密码 default tablespace 表空间;
```

##### 赋连接权限 

```sql
grant connect to 用户名;
```

##### 赋表权限

```sql
grant select on owner.表名 to 用户名;

--- 如果有多表,可以用selece转换批量执行语句:
select 'grant select on '||owner||'.'||object_name||' to 用户名;'
from dba_objects
where owner in ('owner')
and object_type='TABLE';
```

##### 创建同义词

```sql
create or replace SYNONYM 用户名.表名 FOR owner.表名;
 
--- 如果有多表,可以用selece转换批量执行语句:
SELECT 'create or replace SYNONYM  用户名.'||object_name||' FOR '||owner||'.'||object_name||';'  from dba_objects 
where owner in ('owner')
and object_type='TABLE';
```

 



### 检索数据 



#### 简单查询 



#### 筛选查询(WHERE) 

##### 正则匹配字段内容(REGEXP_LIKE)

```sql
WHERE REGEXP_LIKE(first_name, '^Ste(v|ph)en$')
```

#### 分组查询 



#### 排序查询 



#### 多表关联查询 



### Oracle常用系统函数 



#### 字符类函数 



#### 数字类函数 



#### 日期和时间类函数 



#### 转换类函数 



#### 聚合类函数 



### 子查询的用法 



#### 什么是子查询 



#### 单行子查询 



#### 多行子查询 



#### 关联子查询 



### 操作数据库 



#### 插入数据(INSERT语句) 



#### 更新数据(UPDATE语句) 



#### 删除数据(DELETE语句和TRUNCATE语句)




## PL/SQL编程 

### PL/SQL简介 



#### PL/SQL块结构 



#### 代码注释和标示符 



#### 文本 



### 数据类型、变量和常量 



#### 基本数据类型 



#### 特殊数据类型 



#### 定义变量和常量 



#### PL/SQL表达式 



### 流程控制语句 



#### 选择语句 



#### 循环语句 



### PL/SQL游标 



#### 基本原理 



#### 显式游标 



#### 隐式游标 



#### 游标的属性 



#### 游标变量 



#### 通过for语句循环游标 



### PL/SQL异常处理 



#### 异常处理方法 



#### 异常处理语法 



#### 预定义异常 



#### 自定义异常 




## 过程、函数、触发器和包 

### 存储过程 



#### 创建存储过程 



#### 存储过程的参数 



#### IN参数的默认值 



#### 删除存储过程 



### 函数 



#### 创建函数 



#### 调用函数 



#### 删除函数 



### 触发器 



#### 触发器简介 



#### 语句级触发器 

> 语句级触发器对每个DML语句执行一次,如果一条INSERT语句在TABLE表中插入500行,那么这个表上的语句级触发器只执行一次,而行级的触发器就要执行500次了.

#### 行级触发器 

> 行级触发器对DML语句影响的每个行执行一次.<br>
:NEW 和:OLD使用方法和意义,new 只出现在insert和update时,old只出现在update和delete时.<br>
在insert时new表示新插入的行数据,update时new表示要替换的新数据、old表示要被更改的原来的数据行,delete时old表示要被删除的数据.

```sql
-- 删除原有序列
DROP SEQUENCE table_name_ID_SEQ;

-- 创建序列
CREATE SEQUENCE table_name_ID_SEQ START WITH last_number;

-- 创建触发器
CREATE OR REPLACE TRIGGER table_name_TRG
BEFORE INSERT ON table_name
FOR EACH ROW
BEGIN
-- 判断应自增序列是否null,如果为null再触发
-- 不然会出现保存前id已存在,但在触发器的作用下id又发生了变化
  IF :new.id IS NULL THEN
    :new.id := table_name_ID_SEQ.NEXTVAL;
  END IF;
END;
/

```
`/` 为必须项


#### 替换触发器 



#### 用户事件触发器 



#### 删除触发器 

```sql
DROP TRIGGER trigger_name;
```


#### 查询触发器具体内容

```sql
select dbms_metadata.get_ddl('TRIGGER','大写触发器名称','用户名') from dual;
```


### 程序包 



#### 程序包的规范 



#### 程序包的主体 



#### 删除包  



 
# 核心技术 

## 管理控制文件和日志文件 

### 管理控制文件 



#### 控制文件简介 



#### 控制文件的多路复用 



#### 创建控制文件 



#### 备份和恢复控制文件 



#### 删除控制文件 



#### 查询控制文件的信息 



### 管理重做日志文件 



#### 重做日志文件概述 



#### 增加日志组及其成员 



#### 删除重做日志文件 



#### 更改重做日志文件的位置或名称 



#### 查看重做日志信息 



### 管理归档日志文件 



#### 日志模式分类 



#### 管理归档操作 



#### 设置归档文件位置 



#### 查看归档日志信息




## 管理表空间和数据文件 

### 表空间与数据文件的关系 



### Oracle的默认表空间 



#### SYSTEM表空间 



#### SYSAUX表空间 



### 创建表空间 



#### 创建表空间的语法 



#### 通过本地化管理方式创建表空间 



#### 通过段空间管理方式创建表空间 



#### 创建非标准块表空间 



#### 建立大文件表空间 



### 维护表空间与数据文件 



#### 设置默认表空间 



#### 更改表空间的状态 



#### 重命名表空间 



#### 删除表空间 



#### 维护表空间中的数据文件 



### 管理撤销表空间 



#### 撤销表空间的作用 



#### 撤销表空间的初始化参数 



#### 撤销表空间的基本操作 



### 管理临时表空间 



#### 临时表空间简介 



#### 创建临时表空间 



#### 查询临时表空间的信息 



#### 关于临时表空间组 




## 数据表对象 

### 数据表概述 



### 创建数据表 



#### 数据表的逻辑结构 



#### 创建一个数据表 



#### 数据表的特性 



### 维护数据表 



#### 增加和删除字段 



#### 修改字段 



#### 重命名表 



#### 改变表空间和存储参数 



#### 删除表 

##### 关于purge

purge用于清除oracle 回收站(recyclebin)中的表和索引并释放与其相关的空间,还可清空回收站,或者清除表空间中记录的已删除的部分表空间.<br>
注意:purge后不能回滚和恢复.

如果不使用purge参数,重新建表提示表名已存在,通过下面语句查询确实存在记录:

```sql 
select count(*) from user_tables where table_name =upper('表名')
```
      
```sql 
-- 删除表
drop table 表名 purge;

-- 删除序列
DROP SEQUENCE 表名_SEQ; 
```

#### 修改表的状态 



### 数据完整性和约束性 



#### 非空约束 



#### 主键约束 



#### 唯一性约束 



#### 外键约束 



#### 禁用和激活约束 



#### 删除约束 




## 其他数据对象 

### 索引对象 



#### 索引概述 



#### 创建索引 



#### 修改索引 



#### 删除索引 



#### 显示索引信息 



### 视图对象 



#### 创建视图 



#### 管理视图 



### 同义词对象 



### 序列对象 



#### 创建序列 



#### 管理序列 

##### 查看序列的last_number

```sql
select last_number from user_sequences where sequence_name = 'sequence_name';
```

##### 删除序列

```sql
DROP SEQUENCE 序列名;
```

##### 修改序列的last_number

###### increment

> 通过修改步进长度来实现修改last_number.

```sql
-- 修改序列步进
-- 这里的want_number = 期待last_number - 当前last_number
alter sequence sequence_name increment by want_number;

-- 查询序列下一个number从而实现last_number的增长
select sequence_name.nextval from dual;

-- 检查序列当前的last_number
select last_number from user_sequences where sequence_name = 'sequence_name';

-- 将步进改为1
alter sequence sequence_name increment by 1;
```

###### restart

> 此功能已在18c中正式添加,但在12.1中才正式提供.

```sql
alter sequence sequence_name restart start with last_number;
```

检查序列last_number是否变化.

## 表分区与索引分区 

### 分区技术简介 



### 创建表分区 



#### 范围分区 



#### 散列分区 



#### 列表分区 

# 异常编码

## ORA-28001

> Oracle11G创建用户时缺省密码过期限制是180天(即6个月),<br> 
如果超过180天用户密码未做修改则该用户无法登录.

> 解决措施

1. 将密码有效期由默认的180天修改成"无限制":

```sql
ALTER PROFILE DEFAULT LIMIT PASSWORD_LIFE_TIME UNLIMITED;
```

修改之后不需要重启动数据库,会立即生效.

2. 修改后,再改一次密码:

```bash
sqlplus / as sysdba
sql>alter user 用户名 identified by <原来的密码> account unlock; ----不用换新密码
```

## ORA-01653

> OCIError: ORA-01653: unable to extend table HZKJ_ORA.SESSIONS by 8192 in tablespace.

解决办法:

0. 扩展表空间大小
0. 增加新文件

以下为排查思路.

***以下内容无误,但是没有解决所遇到的问题.***

看了些解决方法,基本上就上面两种方案,但是没有解决.<br>
数据库的具体表现也是使用比达到90%,但是按照扩展表空间大小的方法实践,空间有进行相应增大,但是是通过清空sessions表(错误信息中的 HZKJ_ORA.SESSIONS)数据(60W+)来解决的.

```sql
truncate table sessions;
```

由于这张表只用于存储会话相关信息,数据删除掉没有较大影响,如果是其他表遇到该问题就比较棘手了.


### 表空间预览

```sql
SELECT a.tablespace_name "表空间名",
a.bytes / 1024 / 1024 "表空间大小(M)",
(a.bytes - b.bytes) / 1024 / 1024 "已使用空间(M)",
b.bytes / 1024 / 1024 "空闲空间(M)",
round(((a.bytes - b.bytes) / a.bytes) * 100, 2) "使用比"
FROM (SELECT tablespace_name, sum(bytes) bytes
FROM dba_data_files
GROUP BY tablespace_name) a,
(SELECT tablespace_name, sum(bytes) bytes, max(bytes) largest
FROM dba_free_space
GROUP BY tablespace_name) b
WHERE a.tablespace_name = b.tablespace_name
ORDER BY ((a.bytes - b.bytes) / a.bytes) DESC;
```

### 查询是否开启表空间的自动扩展功能

```
SELECT file_id, file_name, tablespace_name, autoextensible, increment_by
FROM dba_data_files
WHERE tablespace_name = '表空间名'
ORDER BY file_id desc;
```
查看"autoextensible"对应的值是YES还是NO,若是NO,说明MSMS表空间的自动扩展功能没有开,改成YES就可以了.

#### 开启表空间自动扩展功能

```sql
alter database datafile '/u01/app/oracle/oradata/sktest/skuser.dbf' autoextend on;
```

#### 关闭表空间自动扩展功能

```sql
alter database datafile '/u01/app/oracle/oradata/sktest/skuser.dbf' autoextend off;
```

### 查询表空间的目前使用大小

```sql
select tablespace_name,sum(bytes/1024/1024) mb from dba_segments where tablespace_name='表空间名' group by tablespace_name;
```

### 查询表空间的分配大小
```sql
select tablespace_name,bytes/1024/1024 mb from dba_data_files;
```

### 查询表空间最大扩展大小

> 当表空间开启了自动扩展特性,表空间会一直扩展到操作系统支持的最大大小.

```sql
select tablespace_name,maxblocks*8 from dba_data_files; 
```

### 查看表空间和物理文件路径

```sql
select tablespace_name, file_id, file_name from dba_data_files order by tablespace_name;
```

### 扩展表空间大小

```sql
alter database datafile '/oracle/oradata/dbaxj/users01.dbf' resize 30G;
```

### 增加数据文件

```sql
SELECT dbms_metadata.get_ddl('TABLESPACE', '表空间名称') FROM dual;

--新增一个数据文件,全路径的数据文件名称为该新增数据文件的全路径文件名称.<br>
大小为***M,自动扩展功能打开,且该数据文件的最大扩展值为20G.<br>

alter tablespace 表空间名称 add datafile '全路径的数据文件名称' size ***M autoextend on maxsize 20G;
```

#### 验证已经增加的数据文件

```sql
SELECT file_name, file_id, tablespace_name
FROM dba_data_files
WHERE tablespace_name = '表空间名称'
```


# 参考资料

> [京东 | Oracle 11g从入门到精通(第2版)](https://item.m.jd.com/product/12156981.html)

> [51CTO | Oracle用户和模式的区别](https://database.51cto.com/art/201010/231679.htm)

> [博客园 | Oracle报错,ORA-28001: 口令已经失效](https://www.cnblogs.com/luckly-hf/p/3828573.html)

> [CSDN | oracle创建只读权限的用户](https://blog.csdn.net/antma/article/details/53435704)

> [CSDN | oracle数据库判断某表是否存在](https://blog.csdn.net/wohaqiyi/article/details/79358338)

> [CSDN | Oracle purge 用法介绍](https://blog.csdn.net/indexman/article/details/27379597)

> [CSDN | Oracle 修改表空间为自动扩展](https://blog.csdn.net/levy_cui/article/details/51140937)

> [博客园 | ORACLE ORA-01653: unable to extend table 的错误](https://www.cnblogs.com/vinsonLu/p/10648137.html)

> [CSDN | 行级触发器和语句级触发器](https://blog.csdn.net/zy18755122285/article/details/51286356)

> [stackexchange | Change LAST_NUMBER value in a sequence (NEXTVAL)](https://dba.stackexchange.com/questions/239966/change-last-number-value-in-a-sequence-nextval)

> [oracle | 4 Using Regular Expressions in Oracle Database](https://docs.oracle.com/cd/B19306_01/B14251_01/adfns_regexp.htm)