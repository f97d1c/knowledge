
<!-- TOC -->

- [安装](#安装)
  - [相关gem](#相关gem)
  - [相关配置](#相关配置)
    - [config/application.rb](#configapplicationrb)
    - [config/initializers/redis.rb](#configinitializersredisrb)
- [应用](#应用)
  - [赋值/取值](#赋值取值)
- [redis-rails](#redis-rails)
  - [存储Session](#存储session)
    - [config/initializers/session_store.rb](#configinitializerssession_storerb)
  - [HTTP缓存](#http缓存)
    - [Gemfile](#gemfile)
    - [config/environments/production.rb](#configenvironmentsproductionrb)
    - [使用Redis Sentinel](#使用redis-sentinel)
- [redis-namespace](#redis-namespace)
  - [使用](#使用)
- [redis-rack-cache](#redis-rack-cache)
  - [配置](#配置)
    - [config/environments/production.rb](#configenvironmentsproductionrb-1)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装

## 相关gem

```Gemfile
gem 'redis'
gem 'redis-namespace'
gem 'redis-rails'
gem 'redis-rack-cache'
```

## 相关配置

### config/application.rb

```rb
config.cache_store = :redis_store, 'redis://localhost:6379/0/cache', { expires_in: 90.minutes }
```

### config/initializers/redis.rb 

```ruby
$redis = Redis::Namespace.new("site_point", :redis => Redis.new)
```

# 应用

## 赋值/取值

现在所有的Redis功能都可以通过$redis进行全局使用.

访问在redis服务器上的值(rails console 中):

```ruby
$redis.set("test_key", "Hello World!")
# 这个命令创建了一个key:"test_key"和value:"Hello World"保存在Redis中

# 取值
$redis.get("test_key")
```

# redis-rails

## 存储Session

### config/initializers/session_store.rb

```ruby
MyApplication::Application.config.session_store :redis_store,
  # servers是将尝试从中查找数据的Redis服务器阵列.这使用与语法相同的语法:redis_store.
  servers: ["redis://localhost:6379/0/session"],
  # 设置会话存储生成的任何cookie的到期时间
  expire_after: 90.minutes,
  # 客户端cookie的名称
  key: "_#{Rails.application.class.parent_name.downcase}_session",
  # hreadsafe适用于在多个实例上运行的应用程序.如果要禁用会话数据上的全局互斥锁,请将此项设置为:false.默认情况下为true
  threadsafe: false
```

## HTTP缓存

### Gemfile

```Gemfile
group :production do
  gem 'redis-rack-cache'
end
```

### config/environments/production.rb

```ruby
config.action_dispatch.rack_cache = {
  metastore: "redis://localhost:6379/1/metastore",
  entitystore: "redis://localhost:6379/1/entitystore"
}
```

### 使用Redis Sentinel

还可以使用Redis Sentinel管理Redis服务器群集,以实现高可用性数据访问:

```ruby
# config/environments/production.rb
sentinel_config = {
  url: "redis://mymaster/0",
  role: "master",
  sentinels: [{
    host: "127.0.0.1",
    port: 26379
  },{
    host: "127.0.0.1",
    port: 26380
  },{
    host: "127.0.0.1",
    port: 26381
  }]
}
```

```ruby
# config/environments/production.rb
config.cache_store = :redis_store, sentinel_config.merge(
  namespace: "cache",
  expires_in: 1.days
)
config.session_store :redis_store, {
  servers: [
    sentinel_config.merge(
      namespace: "sessions"
    )
  ],
  expire_after: 2.days
}
```

# redis-namespace

## 使用

```ruby
require 'redis-namespace'
# => true

redis_connection = Redis.new
# => #<Redis client v3.1.0 for redis://127.0.0.1:6379/0>
namespaced_redis = Redis::Namespace.new(:ns, :redis => redis_connection)
# => #<Redis::Namespace v1.5.0 with client v3.1.0 for redis://127.0.0.1:6379/0/ns>

namespaced_redis.set('foo', 'bar') # redis_connection.set('ns:foo', 'bar')
# => "OK"

# Redis::Namespace automatically prepended our namespace to the key
# before sending it to our redis client.

namespaced_redis.get('foo')
# => "bar"
redis_connection.get('ns:foo')
# => "bar"

namespaced_redis.del('foo')
# => 1
namespaced_redis.get('foo')
# => nil
redis_connection.get('ns:foo')
# => nil
```

# redis-rack-cache

> 如果使用Redis存储Rails缓存则除了redis-rails之外,此gem也是必需的.

## 配置

### config/environments/production.rb

```ruby
Rails.application.configure do
  config.action_dispatch.rack_cache = {
    metastore: "#{Rails.credentials.redis_url}/1/rack_cache_metastore",
    entitystore: "#{Rails.credentials.redis_url}/1/rack_cache_entitystore"
    # NOTE: `:meta_store` and `:entity_store` are also supported.
  }
end
```

使用自定义选项时,也可以使用以下语法:

```ruby
Rails.application.configure do
  config.action_dispatch.rack_cache = {
    meta_store: ::Rack::Cache::MetaStore::Redis.new("#{Rails.credentials.redis_url}/1/rack_cache_metastore", default_ttl: 10.days.to_i),
    entity_store: ::Rack::Cache::EntityStore::Redis.new("#{Rails.credentials.redis_url}/1/rack_cache_entitystore", default_ttl: 120.days.to_i)
    # NOTE: `:metastore` and `:entitystore` are also supported.
  }
end
```

# 参考资料

> [RubyChina | 使用 Redis 处理 Rails Model 缓存](https://ruby-china.org/topics/26711)

> [Github | redis-rails](https://github.com/redis-store/redis-rails)

> [Github | redis-namespace](https://github.com/resque/redis-namespace)

> [Github |redis-rack-cache ](https://github.com/redis-store/redis-rack-cache)