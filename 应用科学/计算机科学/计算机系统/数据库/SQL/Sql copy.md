
<!-- TOC -->

- [增](#增)
  - [插入数据](#插入数据)
  - [创建索引](#创建索引)
  - [存储过程](#存储过程)
  - [添加字段](#添加字段)
- [删](#删)
  - [DELETE语句](#delete语句)
- [改](#改)
  - [UPDATE语句](#update语句)
- [查](#查)
  - [常用符号](#常用符号)
  - [简单查询](#简单查询)
  - [计算列查询](#计算列查询)
  - [条件查询](#条件查询)
  - [范围查询](#范围查询)
  - [模糊查询](#模糊查询)
    - [正则表达式](#正则表达式)
      - [REGEXP_LIKE(Oracle)](#regexp_likeoracle)
      - [REGEXP(Mysql)](#regexpmysql)
  - [简单子查询](#简单子查询)
  - [多行子查询](#多行子查询)
  - [数据过滤](#数据过滤)
    - [逻辑运算符过滤](#逻辑运算符过滤)
    - [IN操作符过滤](#in操作符过滤)
    - [行数据过滤](#行数据过滤)
  - [格式化结果集](#格式化结果集)
  - [数据排序](#数据排序)
    - [升序/降序](#升序降序)
    - [多列排序](#多列排序)
- [统计分析](#统计分析)
  - [常用函数](#常用函数)
    - [取余](#取余)
    - [MOD()函数](#mod函数)
  - [数据统计分析](#数据统计分析)
  - [分组统计](#分组统计)
  - [多表连接](#多表连接)
  - [游标](#游标)
- [事务处理](#事务处理)
- [数据库管理](#数据库管理)
- [数据表管理](#数据表管理)
- [参考资料](#参考资料)

<!-- /TOC -->

# 增



## 插入数据



## 创建索引

```sql
CREATE INDEX 索引名称 ON 表名称 (列名称);
```

## 存储过程


## 添加字段

```sql
alter table 表名 add 字段名 字段属性 [default 缺省值] [not null]
```


# 删

## DELETE语句

# 改

## UPDATE语句

# 查

## 常用符号

符号|含义|示例
-|-|-
<>|不等于|查询拥有竞价供应商身份用户所属电商中trade_type不等于1的电商:<br>Emall.joins(users: :roles).where("roles.name = ? and emalls.trade_type <> ?", '竞价供应商', 1)

## 简单查询



## 计算列查询



## 条件查询



## 范围查询



## 模糊查询

### 正则表达式 

#### REGEXP_LIKE(Oracle)

> 指定字段匹配多个可能包含的关键字

```sql
REGEXP_LIKE(字段名, '(匹配串1|匹配串2|...)'); --全模糊匹配
REGEXP_LIKE(字段名, '^(匹配串1|匹配串2|...)') "; --右模糊匹配
REGEXP_LIKE(字段名, '(匹配串1|匹配串2|...)$') "; --左模糊匹配
```

```ruby
Product.where("REGEXP_LIKE(name, '(桌|椅)')").count
Product.where("name like ? or name like ?", '%桌%', '%椅%').count
```

#### REGEXP(Mysql)

模式|描述
-|-
^|匹配输入字符串的开始位置.<br>如果设置了 RegExp 对象的 Multiline 属性,^ 也匹配 '\n' 或 '\r' 之后的位置.
$|匹配输入字符串的结束位置.<br>如果设置了RegExp 对象的 Multiline 属性,$ 也匹配 '\n' 或 '\r' 之前的位置.
.|匹配除 "\n" 之外的任何单个字符.<br>要匹配包括 '\n' 在内的任何字符,请使用象 '[.\n]' 的模式.
[...]|字符集合.<br>匹配所包含的任意一个字符.<br>例如, '[abc]' 可以匹配 "plain" 中的 'a'.
[^...]|负值字符集合.<br>匹配未包含的任意字符.<br>例如, '[^abc]' 可以匹配 "plain" 中的'p'.
p1|p2|p3|匹配 p1 或 p2 或 p3.<br>例如,'z|food' 能匹配 "z" 或 "food".<br>'(z|f)ood' 则匹配 "zood" 或 "food".
*|匹配前面的子表达式零次或多次.<br>例如,zo* 能匹配 "z" 以及 "zoo".<br>* 等价于{0,}.
+|匹配前面的子表达式一次或多次.<br>例如,'zo+' 能匹配 "zo" 以及 "zoo",但不能匹配 "z".<br>+ 等价于 {1,}.
{n}|n 是一个非负整数.<br>匹配确定的 n 次.<br>例如,'o{2}' 不能匹配 "Bob" 中的 'o',但是能匹配 "food" 中的两个 o.
{n,m}|m 和 n 均为非负整数,其中n <= m.<br>最少匹配 n 次且最多匹配 m 次.

实例:

```sql
-- 查找name字段中以'st'为开头的所有数据:
SELECT name FROM person_tbl WHERE name REGEXP '^st';

-- 查找name字段中以'ok'为结尾的所有数据:
SELECT name FROM person_tbl WHERE name REGEXP 'ok$';

-- 查找name字段中包含'mar'字符串的所有数据:
SELECT name FROM person_tbl WHERE name REGEXP 'mar';

-- 查找name字段中以元音字符开头或以'ok'字符串结尾的所有数据:
SELECT name FROM person_tbl WHERE name REGEXP '^[aeiou]|ok$';

```


## 简单子查询



## 多行子查询



## 数据过滤



### 逻辑运算符过滤



### IN操作符过滤



### 行数据过滤



## 格式化结果集



## 数据排序

> 理论上来讲,SELECT语句所返回的结果集都是无序的,<br>
结果集中记录之间的顺序主要取决于物理位置.<br>
对结果集进行排序的唯一方法就是在SELECT查询中嵌入ORDER BY子句.<br>
ORDER BY子句用来指定最后结果集中的行顺序.

### 升序/降序

```sql
-- ORDER BY关键字后面跟一个用逗号分隔的排序列表
ORDER BY {列名/别名/表达式 [ASC(升序, 默认) | DESC(降序)]}[, ...N];
```

```sql
-- 根据消息的更新时间倒序排列
SELECT * FROM messages ORDER BY updated_at DESC;

-- 根据别名对消息的更新时间倒序排列
SELECT id 消息ID, content 消息内容, created_at 创建时间, updated_at 更新时间 FROM messages ORDER BY 更新时间 DESC;

-- 根据消息内容最左面的一个字符进行升序排列
SELECT id,content,created_at FROM messages ORDER BY LEFT(content,1);
```

### 多列排序

> 在对多列排序时,只有在主排列出现重复值时,才进行次排列.

```sql
ORDER BY 主排列, 次排列 [,...N];
```

```sql
-- 根据消息更新时间倒序排列, 如果更新时间相同,则对相同值记录按创建时间倒序排列
SELECT * FROM messages ORDER BY updated_at DESC, created_at DESC;

-- 在SELECT语句的ORDER BY子句中可以引用查询中没有使用的字段
-- 但如果SELECT查询语句中使用了DISTINCT或者UNIQUE关键字则不被允许
SELECT id,content,created_at FROM messages ORDER BY updated_at DESC;

-- ORDER BY clause is not in SELECT list
SELECT DISTINCT id,content,created_at FROM messages ORDER BY updated_at DESC;
```


# 统计分析

## 常用函数

### 取余

### MOD()函数

> MOD(N,M)<br>
返回N除以M余值

```ruby
# 返回id为奇数的订单
Order.where("mod(id, 2) = ?", 1)
```

## 数据统计分析



## 分组统计



## 多表连接



## 游标



# 事务处理



# 数据库管理 



# 数据表管理

# 参考资料

> [SQL即查即用]()

> [CSDN | oracle 字段like多个条件(or关系)](https://blog.csdn.net/kenry121212/article/details/79270100)

> [菜鸟教程 | MySQL 正则表达式](http://www.runoob.com/mysql/mysql-regexp.html)