
<!-- TOC -->

- [说明](#说明)
  - [SELECT 语句执行顺序](#select-语句执行顺序)
- [DDL](#ddl)
  - [CREATE TABLE](#create-table)
  - [ALERT TABLE](#alert-table)
  - [DROP TABLE](#drop-table)
  - [CREATE INDEX](#create-index)
  - [ALTER INDEX](#alter-index)
  - [DROP INDEX](#drop-index)
  - [CREATE VIEW](#create-view)
  - [DROP VIEW](#drop-view)
  - [表约束](#表约束)
    - [主键](#主键)
    - [外键](#外键)
    - [NOT NULL](#not-null)
    - [DEFAULT](#default)
    - [CHECK](#check)
  - [设计原则](#设计原则)
- [DML](#dml)
  - [INSERT](#insert)
  - [DELETE](#delete)
  - [UPDATE](#update)
- [DQL](#dql)
  - [SELECT](#select)
    - [常用符号](#常用符号)
    - [正则匹配(REGEXP_SUBSTR)](#正则匹配regexp_substr)
  - [FORM](#form)
  - [WHERE](#where)
    - [某一时间范围内(between)](#某一时间范围内between)
  - [排序(ORDER BY)](#排序order-by)
    - [升序/降序(ASC/DESC)](#升序降序ascdesc)
    - [多列排序](#多列排序)
  - [分组(GROUP BY)](#分组group-by)
  - [分页(LIMIT)](#分页limit)
  - [联表(JOIN)](#联表join)
  - [子查询](#子查询)
  - [函数](#函数)
    - [算术](#算术)
    - [字符串](#字符串)
    - [日期](#日期)
    - [转换](#转换)
  - [视图](#视图)
    - [创建](#创建)
    - [修改](#修改)
    - [删除](#删除)
    - [嵌套](#嵌套)
    - [与临时表的相关对比](#与临时表的相关对比)
  - [计算列查询](#计算列查询)
  - [条件查询](#条件查询)
  - [范围查询](#范围查询)
  - [简单子查询](#简单子查询)
  - [多行子查询](#多行子查询)
  - [数据过滤](#数据过滤)
    - [逻辑运算符过滤](#逻辑运算符过滤)
    - [IN操作符过滤](#in操作符过滤)
    - [行数据过滤](#行数据过滤)
  - [格式化结果集](#格式化结果集)
  - [模糊查询](#模糊查询)
    - [正则表达式](#正则表达式)
      - [REGEXP_LIKE(Oracle)](#regexp_likeoracle)
      - [REGEXP(Mysql)](#regexpmysql)
- [优化](#优化)
  - [索引优化分析](#索引优化分析)
  - [查询截取分析](#查询截取分析)
  - [常见优化方法](#常见优化方法)
    - [小表驱动大表](#小表驱动大表)
    - [ORDER BY 优化](#order-by-优化)
    - [GROUP BY 优化](#group-by-优化)
    - [WHERE 尽量避免重复](#where-尽量避免重复)
    - [慎用IN和NOT IN](#慎用in和not-in)
      - [NOT IN 替代方法](#not-in-替代方法)
        - [外连接+判断为空](#外连接判断为空)
    - [VARCHAR代替CHAR](#varchar代替char)
    - [避免*查询](#避免查询)
- [锁机制](#锁机制)
  - [排它锁](#排它锁)
  - [共享锁](#共享锁)
  - [更新锁](#更新锁)
  - [乐观锁](#乐观锁)
  - [悲观锁](#悲观锁)
  - [粒度](#粒度)
    - [行锁](#行锁)
    - [表锁](#表锁)
    - [页锁](#页锁)
- [SQL安全](#sql安全)
  - [用户权限管理](#用户权限管理)
  - [数据恢复](#数据恢复)
  - [防注入](#防注入)
- [其他](#其他)
  - [DMS架构](#dms架构)
  - [ORACLE](#oracle)
    - [SQL执行流程](#sql执行流程)
      - [共享池](#共享池)
      - [软解析](#软解析)
      - [硬解析](#硬解析)
  - [MYSQL](#mysql)
    - [主要层次](#主要层次)
      - [连接层](#连接层)
      - [SQL层](#sql层)
        - [原理解析](#原理解析)
      - [存储引擎层](#存储引擎层)
    - [存储引擎](#存储引擎)
  - [影响性能因素](#影响性能因素)
- [DPL](#dpl)
- [CCL](#ccl)
  - [声明](#声明)
  - [使用](#使用)
  - [判断](#判断)
- [DCL](#dcl)
  - [ALTER PASSWORD](#alter-password)
  - [GRANT](#grant)
  - [REVOKE](#revoke)
  - [CREATE SYNONYM](#create-synonym)
- [数据管理命令](#数据管理命令)
  - [START AUDIT](#start-audit)
  - [STOP AUDIT](#stop-audit)
- [事务控制命令](#事务控制命令)
  - [COMMIT](#commit)
  - [ROLLBACK](#rollback)
  - [SAVEPOINT](#savepoint)
  - [SET TRANSACTION](#set-transaction)
- [统计分析](#统计分析)
  - [常用函数](#常用函数)
    - [取余](#取余)
    - [MOD()函数](#mod函数)
  - [数据统计分析](#数据统计分析)
  - [分组统计](#分组统计)
  - [多表连接](#多表连接)
- [游标](#游标)
- [](#)
- [](#-1)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## SELECT 语句执行顺序

> SQL语言不同于其他编程语言的最明显特征是处理代码的顺序.<br>
在大多数据库语言中,代码按编码顺序被处理.<br>
但在SQL语句中,第一个被处理的子句式FROM,而不是第一出现的SELECT.

Select语句完整的执行顺序: 

0. from子句组装来自不同数据源的数据.
0. where子句基于指定的条件对记录行进行筛选.
0. group by子句将数据划分为多个分组.
0. 使用聚集函数进行计算.
0. 使用having子句筛选分组.
0. 计算所有的表达式.
0. select 的字段.
0. 使用order by对结果集进行排序.

# DDL

> 数据定义语言(DDL),用于创建和重构数据库对象.

## CREATE TABLE



## ALERT TABLE

```sql
alter table 表名 add 字段名 字段属性 [default 缺省值] [not null]
```

## DROP TABLE



## CREATE INDEX



## ALTER INDEX



## DROP INDEX



## CREATE VIEW



## DROP VIEW


## 表约束



### 主键



### 外键



### NOT NULL



### DEFAULT



### CHECK



## 设计原则

三多一少的原则就是简单可复用

0. 数据表越少越好
0. 表中字段越少越好
0. 表中联合主键越少越好
0. 主键和外键越多越好


# DML

> 数据操作语言(DML),用于操作关系型数据库对象内部的数据.

## INSERT



## DELETE



## UPDATE



# DQL

> 数据查询语言(DQL)

## SELECT

### 常用符号

符号|含义|示例
-|-|-
<>|不等于|查询拥有竞价供应商身份用户所属电商中trade_type不等于1的电商:<br>Emall.joins(users: :roles).where("roles.name = ? and emalls.trade_type <> ?", '竞价供应商', 1)

### 正则匹配(REGEXP_SUBSTR)

> REGEXP_SUBSTR用于对字段值进行正则切割.

```sql
-- 切割后内容如果用于排序需要转下整数
SELECT TO_NUMBER(REGEXP_SUBSTR(ASSETS_ACCEPTANCE_DATA.DATA_ID, '[^-]+', 1, 3)) sequence,
       REGEXP_SUBSTR(ASSETS_ACCEPTANCE_DATA.DATA_ID, '[^-]+', 1, 2) emall_id, ASSETS_ACCEPTANCE_DATA.DATA_ID
FROM "ASSETS_ACCEPTANCE_DATA"  WHERE (DATA_ID like '266-%')  ORDER BY sequence desc;

-- SEQUENCE EMALL_ID DATA_ID 
-- 6 106 266-106-6
-- 5 106 266-106-5
-- 4 106 266-106-4
-- 3 106 266-106-3
-- 2 106 266-106-2
-- 1 106 266-106-1
```

## FORM



## WHERE

### 某一时间范围内(between)

```ruby
time_range = (Time.new(2019,01)..Time.new(2019,12).end_of_month)

base_records = BidProjectItemSupplier.joins(:bid_project).where("bid_project_item_suppliers.supplier_id = ? and (bid_project_item_suppliers.created_at between ? and ?)", supplier_id, time_range.first, time_range.last)
```

## 排序(ORDER BY)

> 理论上来讲,SELECT语句所返回的结果集都是无序的,<br>
结果集中记录之间的顺序主要取决于物理位置.<br>
对结果集进行排序的唯一方法就是在SELECT查询中嵌入ORDER BY子句.<br>
ORDER BY子句用来指定最后结果集中的行顺序.

### 升序/降序(ASC/DESC)

```sql
-- ORDER BY关键字后面跟一个用逗号分隔的排序列表
ORDER BY {列名/别名/表达式 [ASC(升序, 默认) | DESC(降序)]}[, ...N];
```

```sql
-- 根据消息的更新时间倒序排列
SELECT * FROM messages ORDER BY updated_at DESC;

-- 根据别名对消息的更新时间倒序排列
SELECT id 消息ID, content 消息内容, created_at 创建时间, updated_at 更新时间 FROM messages ORDER BY 更新时间 DESC;

-- 根据消息内容最左面的一个字符进行升序排列
SELECT id,content,created_at FROM messages ORDER BY LEFT(content,1);
```

### 多列排序

> 在对多列排序时,只有在主排列出现重复值时,才进行次排列.

```sql
ORDER BY 主排列, 次排列 [,...N];
```

```sql
-- 根据消息更新时间倒序排列, 如果更新时间相同,则对相同值记录按创建时间倒序排列
SELECT * FROM messages ORDER BY updated_at DESC, created_at DESC;

-- 在SELECT语句的ORDER BY子句中可以引用查询中没有使用的字段
-- 但如果SELECT查询语句中使用了DISTINCT或者UNIQUE关键字则不被允许
SELECT id,content,created_at FROM messages ORDER BY updated_at DESC;

-- ORDER BY clause is not in SELECT list
SELECT DISTINCT id,content,created_at FROM messages ORDER BY updated_at DESC;
```

## 分组(GROUP BY)

 group by 一般和聚合函数一起使用才有意义,比如 count sum avg等.
 
使用group by的两个要素:

0. 出现在select后面的字段 要么是是聚合函数中的,要么就是group by 中的.
0. 要筛选结果 可以先使用where 再用group by 或者先用group by 再用having.

```sql
-- oracle 中根据数据的创建时间(年度)进行分组
select to_char(p.created_at,'yyyy') time_unit, count(*) num from products p group by to_char(p.created_at,'yyyy')
```

## 分页(LIMIT)



## 联表(JOIN)



## 子查询



## 函数


### 算术




### 字符串




### 日期




### 转换




## 视图



### 创建


### 修改



### 删除



### 嵌套



### 与临时表的相关对比

## 计算列查询



## 条件查询



## 范围查询




## 简单子查询



## 多行子查询



## 数据过滤



### 逻辑运算符过滤



### IN操作符过滤



### 行数据过滤



## 格式化结果集


## 模糊查询

### 正则表达式

#### REGEXP_LIKE(Oracle)

> 指定字段匹配多个可能包含的关键字

```sql
REGEXP_LIKE(字段名, '(匹配串1|匹配串2|...)'); --全模糊匹配
REGEXP_LIKE(字段名, '^(匹配串1|匹配串2|...)') "; --右模糊匹配
REGEXP_LIKE(字段名, '(匹配串1|匹配串2|...)$') "; --左模糊匹配
```

```ruby
Product.where("REGEXP_LIKE(name, '(桌|椅)')").count
Product.where("name like ? or name like ?", '%桌%', '%椅%').count
```

#### REGEXP(Mysql)

模式|描述
-|-
^|匹配输入字符串的开始位置.<br>如果设置了 RegExp 对象的 Multiline 属性,^ 也匹配 '\n' 或 '\r' 之后的位置.
$|匹配输入字符串的结束位置.<br>如果设置了RegExp 对象的 Multiline 属性,$ 也匹配 '\n' 或 '\r' 之前的位置.
.|匹配除 "\n" 之外的任何单个字符.<br>要匹配包括 '\n' 在内的任何字符,请使用象 '[.\n]' 的模式.
[...]|字符集合.<br>匹配所包含的任意一个字符.<br>例如, '[abc]' 可以匹配 "plain" 中的 'a'.
[^...]|负值字符集合.<br>匹配未包含的任意字符.<br>例如, '[^abc]' 可以匹配 "plain" 中的'p'.
p1|p2|p3|匹配 p1 或 p2 或 p3.<br>例如,'z|food' 能匹配 "z" 或 "food".<br>'(z|f)ood' 则匹配 "zood" 或 "food".
*|匹配前面的子表达式零次或多次.<br>例如,zo* 能匹配 "z" 以及 "zoo".<br>* 等价于{0,}.
+|匹配前面的子表达式一次或多次.<br>例如,'zo+' 能匹配 "zo" 以及 "zoo",但不能匹配 "z".<br>+ 等价于 {1,}.
{n}|n 是一个非负整数.<br>匹配确定的 n 次.<br>例如,'o{2}' 不能匹配 "Bob" 中的 'o',但是能匹配 "food" 中的两个 o.
{n,m}|m 和 n 均为非负整数,其中n <= m.<br>最少匹配 n 次且最多匹配 m 次.

实例:

```sql
-- 查找name字段中以'st'为开头的所有数据:
SELECT name FROM person_tbl WHERE name REGEXP '^st';

-- 查找name字段中以'ok'为结尾的所有数据:
SELECT name FROM person_tbl WHERE name REGEXP 'ok$';

-- 查找name字段中包含'mar'字符串的所有数据:
SELECT name FROM person_tbl WHERE name REGEXP 'mar';

-- 查找name字段中以元音字符开头或以'ok'字符串结尾的所有数据:
SELECT name FROM person_tbl WHERE name REGEXP '^[aeiou]|ok$';

```

# 优化



## 索引优化分析



## 查询截取分析



## 常见优化方法



### 小表驱动大表



### ORDER BY 优化



### GROUP BY 优化



### WHERE 尽量避免重复



### 慎用IN和NOT IN


#### NOT IN 替代方法

##### 外连接+判断为空

查询还未评价的订单商品:

```ruby
OrderItem.joins("left join cgr_reviews on cgr_reviews.order_item_id = order_items.id").where("cgr_reviews.order_item_id is null")
```


### VARCHAR代替CHAR



### 避免*查询



# 锁机制



## 排它锁



## 共享锁



## 更新锁



## 乐观锁



## 悲观锁



## 粒度



### 行锁

> 偏向写操作

### 表锁

> 偏向读操作

### 页锁


# SQL安全



## 用户权限管理



## 数据恢复



## 防注入


# 其他



## DMS架构



## ORACLE



### SQL执行流程



#### 共享池



#### 软解析



#### 硬解析



## MYSQL



### 主要层次



#### 连接层



#### SQL层


##### 原理解析


#### 存储引擎层



### 存储引擎



## 影响性能因素



# DPL



# CCL



## 声明



## 使用


## 判断


# DCL

> 数据控制语言(DCL),用于控制对数据库里的数据的访问.

## ALTER PASSWORD

## GRANT



## REVOKE


## CREATE SYNONYM



# 数据管理命令

> 数据管理命令用于对数据库里的操作进行审计和分析.

## START AUDIT



## STOP AUDIT



# 事务控制命令



## COMMIT

> 保存数据库事务.

## ROLLBACK

> 撤销数据库事务.

## SAVEPOINT

> 在一组事务里创建标记点以用于回退(ROLLBACK).

## SET TRANSACTION

> 设置事务的名称.

# 统计分析

## 常用函数

### 取余

### MOD()函数

> MOD(N,M)<br>
返回N除以M余值

```ruby
# 返回id为奇数的订单
Order.where("mod(id, 2) = ?", 1)
```

## 数据统计分析



## 分组统计



## 多表连接



# 游标

#

##



##

# 参考资料

> [SQL即查即用]()

> [CSDN | oracle 字段like多个条件(or关系)](https://blog.csdn.net/kenry121212/article/details/79270100)

> [菜鸟教程 | MySQL 正则表达式](http://www.runoob.com/mysql/mysql-regexp.html)

> [脚本之家 | SQLServer 优化SQL语句 in 和not in的替代方案](https://www.jb51.net/article/23293.htm)

> [CSDN | MySQL中between和大于小于的比较](https://blog.csdn.net/A_Runner/article/details/81663627)

> [CSDN | Oracle按年、月、日、周等统计数据](https://blog.csdn.net/wqh8522/article/details/79646934)

> [CSDN | 8、Oracle:group by用法](https://blog.csdn.net/xushaozhang/article/details/74275873)

> [博客园 | 【SQL】SQL 中Select语句完整的执行顺序](https://www.cnblogs.com/HDK2016/p/6884191.html)

> [CSDN | 【Oracle】regexp_substr()函数详解](https://blog.csdn.net/imliuqun123/article/details/79895578)