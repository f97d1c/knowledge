<!-- TOC -->

- [环境搭建](#环境搭建)
  - [ubuntu](#ubuntu)
    - [测试安装结果](#测试安装结果)
    - [默认密码](#默认密码)
    - [关闭开机自启](#关闭开机自启)
- [常用命令](#常用命令)
  - [开关数据库](#开关数据库)
  - [连接数据库](#连接数据库)
  - [导出表数据](#导出表数据)
    - [导出提示Couldn't execute SELECT COLUMN_NAME...](#导出提示couldnt-execute-select-column_name)
  - [开启远程访问权限](#开启远程访问权限)
- [sql语句](#sql语句)
  - [数据类型](#数据类型)
    - [数值类型](#数值类型)
    - [日期和时间类型](#日期和时间类型)
    - [字符串类型](#字符串类型)
  - [增](#增)
    - [新增字段](#新增字段)
  - [删](#删)
    - [表](#表)
    - [字段](#字段)
  - [改](#改)
    - [修改主键id为自增](#修改主键id为自增)
- [常见问题](#常见问题)
  - [ERROR 1045 (28000): Access denied for user](#error-1045-28000-access-denied-for-user)
    - [跳过密码认证过程](#跳过密码认证过程)
  - [Unknown column ‘NaN’ in ‘field list’](#unknown-column-nan-in-field-list)
    - [重启MySQL](#重启mysql)
    - [修改root的密码](#修改root的密码)
  - [cannot create directory ‘//.cache’](#cannot-create-directory-cache)
- [参考资料](#参考资料)

<!-- /TOC -->

# 环境搭建

## ubuntu

```bash
sudo apt-get update;sudo apt-get install -y mysql-server mysql-client libmysqlclient-dev

```

### 测试安装结果

```bash
sudo netstat -tap | grep mysql

# =>
tcp        0      0 localhost:mysql         0.0.0.0:*               LISTEN      9701/mysqld
```

### 默认密码

5.7之后会设置默认密码.

```bash
# 查看默认密码
sudo grep 'password' /etc/mysql/debian.cnf

# 查看mysql用户名
sudo grep 'user' /etc/mysql/debian.cnf
```

### 关闭开机自启

```bash
# /etc/init/mysql.conf

description     "MySQL 5.7 Server"
author          "Mario Limonciello <superm1@ubuntu.com>"

# 只需把该行修改成start on runlevel [345]
start on runlevel [2345]
stop on starting rc RUNLEVEL=[016]

respawn
respawn limit 2 5

env HOME=/etc/mysql
...
```


# 常用命令

## 开关数据库

```bash
sudo service mysql start
sudo service mysql restart
sudo service mysql stop
```

## 连接数据库

```bash
mysql -u 用户名 [-h IP地址] [-P 端口] -p
```

## 导出表数据
-d参数用于仅导出表结构(无数据)

```sql
mysqldump -h主机名 -P端口号 -u用户名 -p密码 [-d] 数据库名 [表名1 表名2 表名3] > 存储路径/文件名.sql;
```

### 导出提示Couldn't execute SELECT COLUMN_NAME...

因为新版的mysqldump默认启用了一个新标志,通过 --column-statistics=0来禁用

解决方法:

```sh
mysqldump --column-statistics=0 ...;
```

## 开启远程访问权限

```sql
USE mysql;
-- 修改权限
update user set host = '%' where user = 'root';
-- 清理缓存
FLUSH PRIVILEGES;
SELECT host,user,authentication_string FROM user;
```

# sql语句

## 数据类型

### 数值类型

> MySQL支持所有标准SQL数值数据类型.

类型|大小|范围(有符号)|范围(无符号)|用途
-|-|-|-|-
TINYINT|1 字节|(-128,127)|(0,255)|小整数值
SMALLINT|2 字节|(-32 768,32 767)|(0,65 535)|大整数值
MEDIUMINT|3 字节|(-8 388 608,8 388 607)|(0,16 777 215)|大整数值
INT或INTEGER|4 字节|(-2 147 483 648,2 147 483 647)|(0,4 294 967 295)|大整数值
BIGINT|8 字节|(-9,223,372,036,854,775,808,9 223 372 036 854 775 807)|(0,18 446 744 073 709 551 615)|极大整数值
FLOAT|4 字节|(-3.402 823 466 E+38,-1.175 494 351 E-38),0,(1.175 494 351 E-38,3.402 823 466 351 E+38)|0,(1.175 494 351 E-38,3.402 823 466 E+38)|单精度
浮点数值DOUBLE|8 字节|(-1.797 693 134 862 315 7 E+308,-2.225 073 858 507 201 4 E-308),0,(2.225 073 858 507 201 4 E-308,1.797 693 134 862 315 7 E+308)|0,(2.225 073 858 507 201 4 E-308,1.797 693 134 862 315 7 E+308)|双精度
浮点数值DECIMAL|对DECIMAL(M,D) ,如果M>D,为M+2否则为D+2|依赖于M和D的值|依赖于M和D的值|小数值


### 日期和时间类型

每个时间类型有一个有效值范围和一个"零"值,当指定不合法的MySQL不能表示的值时使用"零"值.

TIMESTAMP类型有专有的自动更新特性.


类型|大小(字节)|范围|格式|用途
-|-|-|-|-
DATE|3|1000-01-01/9999-12-31|YYYY-MM-DD|日期值
TIME|3|'-838:59:59'/'838:59:59'|HH:MM:SS|时间值或持续时间
YEAR|1|1901/2155|YYYY|年份值
DATETIME|8|1000-01-01 00:00:00/9999-12-31 23:59:59|YYYY-MM-DD HH:MM:SS|混合日期和时间值
TIMESTAMP|4|1970-01-01 00:00:00/2038 结束时间是第 2147483647 秒,北京时间 2038-1-19 11:14:07,格林尼治时间 2038年1月19日 凌晨 03:14:07 YYYYMMDD HHMMSS|混合日期和时间值,时间戳

### 字符串类型

类型|大小|用途
-|-|-
CHAR|0-255字节|定长字符串
VARCHAR|0-65535 字节|变长字符串
TINYBLOB|0-255字节|不超过 255 个字符的二进制字符串
TINYTEXT|0-255字节|短文本字符串
BLOB|0-65 535字节|二进制形式的长文本数据
TEXT|0-65 535字节|长文本数据
MEDIUMBLOB|0-16 777 215字节|二进制形式的中等长度文本数据
MEDIUMTEXT|0-16 777 215字节|中等长度文本数据
LONGBLOB|0-4 294 967 295字节|二进制形式的极大文本数据
LONGTEXT|0-4 294 967 295字节|极大文本数据

CHAR 和 VARCHAR 类型类似,但它们保存和检索的方式不同.<br>
它们的最大长度和是否尾部空格被保留等方面也不同.<br>
在存储或检索过程中不进行大小写转换.

BINARY 和 VARBINARY 类似于 CHAR 和 VARCHAR,不同的是它们包含二进制字符串而不要非二进制字符串.<br>
也就是说,它们包含字节字符串而不是字符字符串.<br>
这说明它们没有字符集,并且排序和比较基于列值字节的数值值.

BLOB 是一个二进制大对象,可以容纳可变数量的数据.<br>
有 4 种 BLOB 类型:TINYBLOB、BLOB、MEDIUMBLOB 和 LONGBLOB.<br>
它们区别在于可容纳存储范围不同.

有 4 种 TEXT 类型:TINYTEXT、TEXT、MEDIUMTEXT 和 LONGTEXT.<br>
对应的这 4 种 BLOB 类型,可存储的最大长度不同,可根据实际情况选择.


## 增

### 新增字段

```sql
ALTER TABLE wdba_single_sources add wdba_quote_id int;
```

## 删

### 表

```sql
DROP TABLE 表名;
```
### 字段

```sql
ALTER TABLE 表名 DROP 字段名;
```

## 改

### 修改主键id为自增

```sql
ALTER TABLE 表名 change id id int AUTO_INCREMENT;
```

# 常见问题

## ERROR 1045 (28000): Access denied for user

> 一般这个错误是由密码错误引起,需重置密码.

### 跳过密码认证过程

my.cnf中在\[mysqld]后面任意一行添加:

```cnf
[mysqld]
# ...
skip-grant-tables
# ...
```

## Unknown column ‘NaN’ in ‘field list’

> 当被除数为0,结果就是NaN,导致mysql插入数据时, 本来因该是一个浮点类型,结果插入了NaN.

### 重启MySQL

重启后无需密码即可登录.

### 修改root的密码

```sql
USE mysql;
-- 修改用户登录密码
-- 5.7后password字段更名为:authentication_string
UPDATE user SET authentication_string=password("密码") where user="用户名";
-- 清理缓存
FLUSH PRIVILEGES;
EXIT
```

## cannot create directory ‘//.cache’

> No directory, logging in with HOME=/
mkdir: cannot create directory ‘//.cache’: Permission denied
-su: 19: /etc/profile.d/wsl-integration.sh: cannot create //.cache/wslu/integration: Directory nonexistent

mysql安装后端口非监听状态,

```
sudo service mysql start
```

手动尝试启动报上面错误, 解决措施:

/etc/profile.d/wsl-integration.sh文件中最上面加入:

```bash
# Check if we have HOME folder
if [[ "${HOME}" == "/" ]]; then
  exit 0
fi
```

# 参考资料

> [linux | mysql | 安装指南](https://www.linuxidc.com/Linux/2017-06/144805.htm)

> [菜鸟教程 | MySQL 数据类型](https://www.runoob.com/mysql/mysql-data-types.html)

> [CNBLOGS | 重置密码解决MySQL for Linux错误 ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)](https://www.cnblogs.com/gumuzi/p/5711495.html)

> [CNBLOGS | Mysql5.7.18.1修改用户密码报错ERROR 1054 (42S22): Unknown Column 'Password' In 'Field List'解决办法](https://www.cnblogs.com/wangbaobao/p/7087032.html)

> [单客欧朋 | Unknown column ‘NaN’ in ‘field list’](http://www.thxopen.com/database/2018/09/19/unknown-column-nan-in-field-list.html)

> [Github | Cannot create directory "//.cache": Permission denied #101](https://github.com/wslutilities/wslu/issues/101)

> [博客园 | mysqldump 导出提示Couldn't execute SELECT COLUMN_NAME...](https://www.cnblogs.com/commissar-Xia/p/10302336.html)