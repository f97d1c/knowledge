require 'pry'
require 'digest'
require 'fileutils'

输入文件 = '双部门.sql'

标题等级 = 4
文件名 = 输入文件.split('/').last.split('.')[0]
输出文件 = "#{文件名}.md"

# ---
# 逻辑处理部分
内容 = File.read(输入文件)
拆分部分 = 内容.split(/;\s*$/).reject(&:empty?)

def 匹配数据字典(语句:)
  备注 = '[\p{Han}\d\w、]+'

  备注字典格式 = [
    /(.*)（(.*)）/,
    /([^:]*):[\s]*(.*)/,
  ]

  纯名称, 内容 = nil
  备注字典格式.each do |正则|
    next unless 语句 =~ 正则
    纯名称 = Regexp.last_match(1)
    内容 = Regexp.last_match(2)
    break unless [纯名称, 内容].include?(nil)
  end
  return [false, ''] if [纯名称, 内容].include?(nil)

  return [false, ''] unless 内容 =~ /\w/

  结果 = {}

  键值间间隔符 = ['：', ':', '=', '-']
  字典间间隔符 = ['\s', '，', ',', '；', ';', '/', '、']
  # 键值之间无区分
  if 内容 =~ /^[\w#{备注}[\s;\/、]{1}]+\w#{备注}$/
    内容.split(Regexp.new(字典间间隔符.join('|'))).each do |字典|
      代码名称 = 字典.scan(/[\p{ASCII}]+|[\p{Han}]+/)
      binding.pry if 代码名称.size != 2
      代码, 名称 = 代码名称
      结果[代码] = 名称
    end
  # 键值之间有区分
  elsif 内容 =~ /^[\w{1,}[#{键值间间隔符.join}]+#{备注}[#{字典间间隔符.join}]{1}]+\w{1,}[#{键值间间隔符.join}]+#{备注}$/
    内容.split(Regexp.new(字典间间隔符.join('|'))).each do |字典|
      代码名称 = 字典.split(Regexp.new(键值间间隔符.join('|')))
      binding.pry if 代码名称.size != 2
      代码, 名称 = 代码名称
      结果[代码] = 名称
    end
  else
    binding.pry
  end

  [true, 纯名称, 结果]
end

def 匹配特定格式(语句:)
  清除关键字 = [
    'unsigned',
    'collate [^\s]+',
    'charset [^\s]+',
    'character set [^\s]+ collate [^\s]+',
  ]
  语句 = 语句.gsub(Regexp.new(清除关键字.join('|'), 'i'), '')

  # 字段相关
  字段名 = '[\w`]+'
  字段类型 = '[\w\d\(\)]+|\w+\(\d{1,}, \d{1,}\)'
  默认值 = 'default \'([^\']*)\'|default ([\w\d\.]+)'
  非空 = '[not\s]*null'
  备注 = 'comment\s\'(.+)\''
  主键 = 'primary key'

  构建正则 = lambda do |数组|
    哈希 = {}
    映射下标 = 0
    变量值 = 数组.map{|i| binding.local_variable_get(i) rescue i}
    数组.zip(变量值).to_h.each do |键,值|
      # 当片段中包括非转义()时,正则下标应增加
      映射下标 +=(值.split(/(?<!\\)\(/).size - 1) + 1
      哈希[键] = {
        '片段' => 值,
        '映射下标' => 映射下标,
      }
    end

    {
      '正则' => Regexp.new('^\s*'+哈希.values.map{|i| i['片段']}.map{|i| '('+i+')'}.join('\s+'), 'i'),
      '映射' => 哈希.map{|键,值| [键, 值['映射下标']]}.to_h,
    }
  end

  映射 = lambda{|x| x.map{|k, v| [k, v.is_a?(Integer) ? Regexp.last_match(v) : v]}.to_h}

  [
    ['.*', '备注', '主键'],
    ['.*', '主键'],
  ].each do |组合|
    主键语句 = 构建正则.call(['字段名', '字段类型', 组合].flatten)
    if 语句 =~ 主键语句['正则']
      res = 映射.call(主键语句['映射'])
      res['非空'] = true
      return res
    end
  end

  复合主键语句 = 构建正则.call(['主键', '\(.*\)'])
  if 语句 =~ 复合主键语句['正则']
    res = 映射.call(复合主键语句['映射'])
    res['字段名'] = res.values[-1]
    res['非空'] = true
    res['字段类型'] = ''
    res['备注'] = '复合主键'
    return res
  end

  [
    ['默认值', '非空', '备注'],
    ['默认值', '非空'],
    ['默认值', '备注'],
    ['非空', '备注'],
    ['默认值'],
    ['非空'],
    ['备注'],
  ].each do |组合|
    普通字段语句 = 构建正则.call(['字段名', '字段类型', 组合].flatten)
    if 语句 =~ 普通字段语句['正则']
      res = 映射.call(普通字段语句['映射'])
      res['非空'] ||= 'null'
      res['非空'] = (res['非空'] =~ /not null/i)

      res['默认值'] ||= ''
      res['备注'] ||= '无'
      return res 
    end
  end

  binding.pry
end

def 建表语句处理(语句:)
  语句 = 语句.gsub("\n", '')
  表名,字段,表名备注 = nil

  表名 = Regexp.last_match(1) if 语句 =~ /.*create table ([\w\._`-]+)/i
  字段 = Regexp.last_match(1) if 语句 =~ /[^\(]*\((.*)\).*/

  if 语句 =~ /.*\)[^\)]*comment[\s=]+'([^']*)'[^\)]*$/i
    表名备注 = Regexp.last_match(1)
  elsif 语句 =~ /^-- ([\u4e00-\u9fff\u3400-\u4dbf\uf900-\ufaff\d\w、]+).*create table.*/
    表名备注 = Regexp.last_match(1)
  else
    表名备注 = ''
    # binding.pry
  end

  binding.pry if [表名,字段,表名备注].include?(nil)

  结果 = []
  字段.split(',  ').each_with_index do |行, 下标|
    next if 行 =~ /\s+constraint.*/
    next if 行 =~ /^\s+[unique ]*key.*/i
    结果 << 匹配特定格式(语句: 行)
  end

  {'表名_英' => 表名, '表名_中' => 表名备注, '字段' => 结果}
end

# 文档整理部分
文档字段内容 = []
文档字典内容 = {}

拆分部分.each do |部分|
  res = 建表语句处理(语句: 部分)

  规范表名 = res['表名_英'].split('.')[-1].gsub('`', '')
  文档单元 = {
    '标题' => "#{规范表名} [#{res['表名_中']}]",
    '表格行' => [],
  }

  res['字段'].each_with_index do |字段, 下标|
    字段名_中 = 字段['备注'] || ''
    result = 匹配数据字典(语句: 字段名_中)

    if result[0]
      字段名_中 = [result[1]]
      表格行 = []

      result[2].each_with_index do |字典, index|
        字段名_中 << 字典.join(': ')
        表格行 << {
          "#" => index+1,
          "代码" => 字典[0],
          "名称" => 字典[1],
          "备注说明" => '',
        }
      end
      
      文档字典内容[Digest::MD5.hexdigest(result.to_s)] = {
        '标题' => "#{规范表名} [#{result[1]}]",
        '表格行' => 表格行,
      }
    end
    
    文档单元['表格行'] << {
      '序号' => 下标+1,
      '字段' => 字段['字段名'].gsub('`', ''),
      # '名称' => 字段名_中.is_a?(String) ? 字段名_中 : 字段名_中.map{|i| "<p>#{i}</p>"}.join,
      '名称' => 字段名_中.is_a?(String) ? 字段名_中 : 字段名_中.join("↩<br>"), # pandoc 无法将<br>转为word中的换行 需要手动在wps中将↩替换为^p
      '数据类型' => 字段['字段类型'],
      '是否主键' => (字段['主键'] ? '是' : ''),
      '是否为空' => (!字段['非空'] ? '是' : '否')
    }
  end
  
  文档字段内容 << 文档单元
end

表格行 = []
文档字段内容.map{|i|i['标题']}.each_with_index do |标题,下标|
  数据表, 名称 = 标题.split(' ')

  表格行 << {
    "#" => 下标+1,
    "数据表" => 数据表,
    "名称" => 名称.gsub(/\[|\]/, ''),
    "备注说明" => '',
  }
end
文档字段内容.insert(0, {
  '标题' => '表清单',
  '表格行' => 表格行,
})

# 文档输出部分
输出对象 = File.open(输出文件, 'w')

[文档字段内容, 文档字典内容.values()].flatten.each do |单元|
  输出对象.write("#{Array.new(标题等级, '#').join()} #{单元['标题']}\n\n")

  列名 = 单元['表格行'].map(&:keys).flatten.uniq
  输出对象.write(列名.map{|i| "|#{i}|"}.join('').gsub('||', '|')+"\n")
  输出对象.write("#{Array.new(列名.size, '|---|').join('').gsub('||', '|')}\n")

  单元['表格行'].each do |行|
    字段 = 列名.map{|键| 行[键]||'无'}
    输出对象.write(字段.map{|i| "|#{i}|"}.join('').gsub('||', '|')+"\n")
  end
  输出对象.write("\n\n")
end

输出对象.close()

# 文档转换部分
`pandoc -f markdown -t docx --self-contained \
  --metadata title="数据库说明文档" \
  -o #{文件名}.docx #{输出文件}`