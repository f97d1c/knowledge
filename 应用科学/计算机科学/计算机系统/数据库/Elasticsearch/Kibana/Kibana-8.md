
<!-- TOC -->

- [.](#)
  - [Kibana 概念](#kibana-概念)
    - [创建数据视图](#创建数据视图)
    - [设置时间范围](#设置时间范围)
    - [Kibana 查询语言](#kibana-查询语言)
    - [Lucene 查询语法](#lucene-查询语法)
    - [保存查询](#保存查询)
- [快速开始](#快速开始)
- [设置](#设置)
  - [安装 Kibana](#安装-kibana)
    - [在 Linux 或 macOS 上从存档安装](#在-linux-或-macos-上从存档安装)
    - [在 Windows 上安装](#在-windows-上安装)
    - [使用 Debian 软件包安装](#使用-debian-软件包安装)
    - [使用 RPM 安装](#使用-rpm-安装)
    - [使用 Docker 安装](#使用-docker-安装)
  - [配置 Kibana](#配置-kibana)
    - [警报和操作设置](#警报和操作设置)
    - [APM 设置](#apm-设置)
    - [横幅设置](#横幅设置)
    - [企业搜索设置](#企业搜索设置)
    - [车队设置](#车队设置)
    - [i18n 设置](#i18n-设置)
    - [记录设置](#记录设置)
    - [日志设置](#日志设置)
    - [指标设置](#指标设置)
    - [监控设置](#监控设置)
    - [报告设置](#报告设置)
    - [搜索会话设置](#搜索会话设置)
    - [安全设置](#安全设置)
    - [安全设定](#安全设定)
    - [空间设置](#空间设置)
    - [任务管理器设置](#任务管理器设置)
    - [遥测设置](#遥测设置)
    - [URL 向下钻取设置](#url-向下钻取设置)
  - [启动和停止 Kibana](#启动和停止-kibana)
  - [访问 Kibana](#访问-kibana)
  - [保护对 Kibana 的访问](#保护对-kibana-的访问)
  - [添加数据](#添加数据)
  - [升级 Kibana](#升级-kibana)
    - [迁移保存的对象](#迁移保存的对象)
    - [解决迁移失败](#解决迁移失败)
    - [回滚到以前版本的 Kibana](#回滚到以前版本的-kibana)
  - [配置安全性](#配置安全性)
    - [验证](#验证)
    - [使用 Elasticsearch 的双向 TLS](#使用-elasticsearch-的双向-tls)
    - [审核日志](#审核日志)
    - [访问协议](#访问协议)
    - [会话管理](#会话管理)
    - [保护保存的对象](#保护保存的对象)
  - [配置报告](#配置报告)
  - [配置日志记录](#配置日志记录)
    - [例子](#例子)
    - [命令行配置](#命令行配置)
  - [配置监控](#配置监控)
    - [使用 Metricbeat 收集监控数据](#使用-metricbeat-收集监控数据)
    - [查看监控数据](#查看监控数据)
    - [遗留收集方法](#遗留收集方法)
  - [命令行工具](#命令行工具)
    - [kibana 加密密钥](#kibana-加密密钥)
    - [kibana-验证码](#kibana-验证码)
- [生产注意事项](#生产注意事项)
  - [安全](#安全)
  - [警报](#警报)
  - [报告](#报告)
  - [任务管理器](#任务管理器)
    - [健康监测](#健康监测)
    - [故障排除](#故障排除)
- [发现](#发现)
  - [浏览您的文档](#浏览您的文档)
  - [搜索相关性](#搜索相关性)
  - [保存搜索以供重复使用](#保存搜索以供重复使用)
  - [在后台运行搜索会话](#在后台运行搜索会话)
  - [查看字段统计信息](#查看字段统计信息)
- [仪表板和可视化](#仪表板和可视化)
  - [创建您的第一个仪表板](#创建您的第一个仪表板)
  - [分析时间序列数据](#分析时间序列数据)
  - [使用编辑器创建面板](#使用编辑器创建面板)
    - [镜片](#镜片)
    - [TSVB](#tsvb)
    - [织女星](#织女星)
    - [基于聚合](#基于聚合)
    - [天狮](#天狮)
  - [使仪表板具有交互性](#使仪表板具有交互性)
    - [使用控件过滤仪表板数据](#使用控件过滤仪表板数据)
    - [在 Discover 中打开面板数据](#在-discover-中打开面板数据)
    - [自定义与向下钻取的交互](#自定义与向下钻取的交互)
  - [改善仪表板加载时间](#改善仪表板加载时间)
- [帆布](#帆布)
  - [编辑工作台](#编辑工作台)
  - [展示您的工作台](#展示您的工作台)
  - [教程：创建用于监控销售的工作台](#教程创建用于监控销售的工作台)
  - [画布表达式生命周期](#画布表达式生命周期)
    - [表达式总是以函数开头](#表达式总是以函数开头)
    - [函数参数](#函数参数)
    - [别名和未命名参数](#别名和未命名参数)
    - [改变你的表达，改变你的输出](#改变你的表达改变你的输出)
    - [获取和操作数据](#获取和操作数据)
    - [用子表达式组合函数](#用子表达式组合函数)
    - [处理上下文和参数类型](#处理上下文和参数类型)
  - [画布功能参考](#画布功能参考)
    - [TinyMath 函数](#tinymath-函数)
- [地图](#地图)
  - [构建地图以按国家或地区比较指标](#构建地图以按国家或地区比较指标)
  - [实时跟踪、可视化和提醒资产](#实时跟踪可视化和提醒资产)
  - [使用反向地理编码映射自定义区域](#使用反向地理编码映射自定义区域)
  - [热图图层](#热图图层)
  - [平铺层](#平铺层)
  - [矢量图层](#矢量图层)
    - [矢量造型](#矢量造型)
    - [矢量样式属性](#矢量样式属性)
    - [矢量工具提示](#矢量工具提示)
  - [绘制大数据](#绘制大数据)
    - [集群](#集群)
    - [显示每个实体最相关的文档](#显示每个实体最相关的文档)
    - [点对点](#点对点)
    - [长期加入](#长期加入)
  - [搜索地理数据](#搜索地理数据)
    - [从地图创建过滤器](#从地图创建过滤器)
    - [过滤单层](#过滤单层)
    - [跨多个索引搜索](#跨多个索引搜索)
  - [配置地图设置](#配置地图设置)
  - [连接到弹性地图服务](#连接到弹性地图服务)
  - [导入地理空间数据](#导入地理空间数据)
    - [教程：索引 GeoJSON 数据](#教程索引-geojson-数据)
  - [疑难解答](#疑难解答)
- [报告和分享](#报告和分享)
  - [自动生成报告](#自动生成报告)
  - [故障排除](#故障排除-1)
- [机器学习](#机器学习)
  - [异常检测](#异常检测)
  - [数据框分析](#数据框分析)
- [图形](#图形)
  - [配置图表](#配置图表)
  - [故障排除和限制](#故障排除和限制)
- [警报](#警报-1)
  - [设置](#设置-1)
  - [创建和管理规则](#创建和管理规则)
  - [规则类型](#规则类型)
    - [指标阈值](#指标阈值)
    - [弹性搜索查询](#弹性搜索查询)
    - [追踪遏制](#追踪遏制)
  - [故障排除和限制](#故障排除和限制-1)
    - [常见问题](#常见问题)
    - [事件日志索引](#事件日志索引)
    - [测试连接器](#测试连接器)
- [可观察性](#可观察性)
- [APM](#apm)
  - [设置](#设置-2)
  - [开始使用](#开始使用)
    - [服务](#服务)
    - [痕迹](#痕迹)
    - [依赖项](#依赖项)
    - [服务地图](#服务地图)
    - [服务概览](#服务概览)
    - [交易](#交易)
    - [跟踪样本时间线](#跟踪样本时间线)
    - [错误](#错误)
    - [指标](#指标)
  - [操作指南](#操作指南)
    - [使用中央配置配置 APM 代理](#使用中央配置配置-apm-代理)
    - [控制对 APM 数据的访问](#控制对-apm-数据的访问)
    - [创建警报](#创建警报)
    - [创建自定义链接](#创建自定义链接)
    - [过滤数据](#过滤数据)
    - [查找事务延迟和故障相关性](#查找事务延迟和故障相关性)
    - [与机器学习集成](#与机器学习集成)
    - [查询您的数据](#查询您的数据)
    - [使用注释跟踪部署](#使用注释跟踪部署)
  - [用户和权限](#用户和权限)
    - [创建 APM 阅读器用户](#创建-apm-阅读器用户)
    - [创建注释用户](#创建注释用户)
    - [创建中央配置用户](#创建中央配置用户)
    - [创建 API 用户](#创建-api-用户)
  - [设置](#设置-3)
  - [REST API](#rest-api)
    - [代理配置 API](#代理配置-api)
    - [注释 API](#注释-api)
    - [RUM 源地图 API](#rum-源地图-api)
    - [APM 代理密钥 API](#apm-代理密钥-api)
  - [故障排除](#故障排除-2)
- [安全](#安全-1)
- [开发工具](#开发工具)
  - [运行 API 请求](#运行-api-请求)
  - [配置文件查询和聚合](#配置文件查询和聚合)
  - [调试 grok 表达式](#调试-grok-表达式)
  - [调试无痛脚本](#调试无痛脚本)
- [舰队](#舰队)
- [奥斯查询](#奥斯查询)
  - [管理集成](#管理集成)
  - [导出字段参考](#导出字段参考)
  - [预建包参考](#预建包参考)
  - [Osquery 常见问题](#osquery-常见问题)
- [堆栈监控](#堆栈监控)
  - [节拍指标](#节拍指标)
  - [弹性搜索指标](#弹性搜索指标)
  - [Kibana 警报](#kibana-警报)
  - [Kibana 指标](#kibana-指标)
  - [Logstash 指标](#logstash-指标)
  - [故障排除](#故障排除-3)
- [堆栈管理](#堆栈管理)
  - [高级设置](#高级设置)
  - [案例](#案例)
    - [配置对案例的访问](#配置对案例的访问)
    - [打开和管理案例](#打开和管理案例)
    - [添加连接器](#添加连接器)
  - [连接器](#连接器)
    - [电子邮件](#电子邮件)
    - [IBM 弹性](#ibm-弹性)
    - [指数](#指数)
    - [吉拉](#吉拉)
    - [微软团队](#微软团队)
    - [寻呼机](#寻呼机)
    - [服务器日志](#服务器日志)
    - [ServiceNow ITSM](#servicenow-itsm)
    - [ServiceNow SecOps](#servicenow-secops)
    - [ServiceNow ITOM](#servicenow-itom)
    - [泳道](#泳道)
    - [松弛](#松弛)
    - [网络钩子](#网络钩子)
    - [xMatters](#xmatters)
    - [预配置的连接器](#预配置的连接器)
  - [许可证管理](#许可证管理)
  - [管理数据视图](#管理数据视图)
  - [数字格式](#数字格式)
  - [汇总作业](#汇总作业)
  - [保存的对象](#保存的对象)
    - [保存的对象 ID](#保存的对象-id)
  - [安全](#安全-2)
    - [授予对 Kibana 的访问权限](#授予对-kibana-的访问权限)
    - [Kibana 角色管理](#kibana-角色管理)
    - [Kibana 特权](#kibana-特权)
    - [API 密钥](#api-密钥)
    - [角色映射](#角色映射)
  - [空间](#空间)
  - [标签](#标签)
  - [观察者](#观察者)
- [REST API](#rest-api-1)
  - [获取功能 API](#获取功能-api)
  - [Kibana 空间 API](#kibana-空间-api)
    - [创造空间](#创造空间)
    - [更新空间](#更新空间)
    - [获得空间](#获得空间)
    - [获取所有空间](#获取所有空间)
    - [删除空间](#删除空间)
    - [将保存的对象复制到空间](#将保存的对象复制到空间)
    - [解决复制到空间冲突](#解决复制到空间冲突)
    - [禁用旧版 URL 别名](#禁用旧版-url-别名)
  - [Kibana 角色管理 API](#kibana-角色管理-api)
    - [创建或更新角色](#创建或更新角色)
    - [获取特定角色](#获取特定角色)
    - [获取所有角色](#获取所有角色)
    - [删除角色](#删除角色)
  - [用户会话管理 API](#用户会话管理-api)
    - [使用户会话无效](#使用户会话无效)
  - [保存的对象 API](#保存的对象-api)
    - [获取对象](#获取对象)
    - [批量获取对象](#批量获取对象)
    - [查找对象](#查找对象)
    - [创建保存的对象](#创建保存的对象)
    - [批量创建保存的对象](#批量创建保存的对象)
    - [更新对象](#更新对象)
    - [批量更新对象](#批量更新对象)
    - [删除对象](#删除对象)
    - [导出对象](#导出对象)
    - [导入对象](#导入对象)
    - [解决导入错误](#解决导入错误)
    - [解决对象](#解决对象)
    - [批量解析对象](#批量解析对象)
    - [轮换加密密钥](#轮换加密密钥)
  - [数据视图 API](#数据视图-api)
    - [获取数据视图](#获取数据视图)
    - [创建数据视图](#创建数据视图-1)
    - [更新数据视图](#更新数据视图)
    - [删除数据视图](#删除数据视图)
    - [获取默认数据视图](#获取默认数据视图)
    - [设置默认数据视图](#设置默认数据视图)
    - [更新数据视图字段元数据](#更新数据视图字段元数据)
    - [获取运行时字段](#获取运行时字段)
    - [创建运行时字段](#创建运行时字段)
    - [Upsert 运行时字段](#upsert-运行时字段)
    - [更新运行时字段](#更新运行时字段)
    - [删除运行时字段](#删除运行时字段)
  - [索引模式 API](#索引模式-api)
    - [获取索引模式](#获取索引模式)
    - [创建索引模式](#创建索引模式)
    - [更新索引模式](#更新索引模式)
    - [删除索引模式](#删除索引模式)
    - [获取默认索引模式](#获取默认索引模式)
    - [设置默认索引模式](#设置默认索引模式)
    - [更新索引模式字段元数据](#更新索引模式字段元数据)
    - [获取运行时字段](#获取运行时字段-1)
    - [创建运行时字段](#创建运行时字段-1)
    - [Upsert 运行时字段](#upsert-运行时字段-1)
    - [更新运行时字段](#更新运行时字段-1)
    - [删除运行时字段](#删除运行时字段-1)
  - [警报 API](#警报-api)
    - [创建规则](#创建规则)
    - [删除规则](#删除规则)
    - [禁用规则](#禁用规则)
    - [启用规则](#启用规则)
    - [查找规则](#查找规则)
    - [获取警报框架运行状况](#获取警报框架运行状况)
    - [获取规则](#获取规则)
    - [获取规则类型](#获取规则类型)
    - [更新规则](#更新规则)
    - [静音所有警报](#静音所有警报)
    - [静音警报](#静音警报)
    - [取消静音所有警报](#取消静音所有警报)
    - [取消静音警报](#取消静音警报)
    - [已弃用的 7.x API](#已弃用的-7x-api)
  - [操作和连接器 API](#操作和连接器-api)
    - [创建连接器](#创建连接器)
    - [删除连接器](#删除连接器)
    - [获取连接器](#获取连接器)
    - [获取所有连接器](#获取所有连接器)
    - [列出所有连接器类型](#列出所有连接器类型)
    - [运行连接器](#运行连接器)
    - [更新连接器](#更新连接器)
    - [已弃用的 7.x API](#已弃用的-7x-api-1)
  - [案例 API](#案例-api)
    - [添加评论](#添加评论)
    - [创建案例](#创建案例)
    - [删除案例](#删除案例)
    - [删除评论](#删除评论)
    - [查找案例](#查找案例)
    - [查找连接器](#查找连接器)
    - [获取警报](#获取警报)
    - [获取案例活动](#获取案例活动)
    - [获取案例](#获取案例)
    - [获取案例状态](#获取案例状态)
    - [通过警报获取案例](#通过警报获取案例)
    - [获取评论](#获取评论)
    - [获取配置](#获取配置)
    - [找记者](#找记者)
    - [获取标签](#获取标签)
    - [推壳](#推壳)
    - [设置配置](#设置配置)
    - [更新案例](#更新案例)
    - [更新评论](#更新评论)
    - [更新配置](#更新配置)
  - [导入和导出仪表板 API](#导入和导出仪表板-api)
    - [导入仪表板](#导入仪表板)
    - [导出仪表板](#导出仪表板)
  - [Logstash 配置管理 API](#logstash-配置管理-api)
    - [删除管道](#删除管道)
    - [列出管道](#列出管道)
    - [创建 Logstash 管道](#创建-logstash-管道)
    - [检索管道](#检索管道)
  - [机器学习 API](#机器学习-api)
    - [同步机器学习保存的对象](#同步机器学习保存的对象)
  - [短网址 API](#短网址-api)
    - [创建短网址](#创建短网址)
    - [获取短网址](#获取短网址)
    - [删除短网址](#删除短网址)
    - [解析短网址](#解析短网址)
  - [获取任务管理器运行状况](#获取任务管理器运行状况)
  - [升级助手 API](#升级助手-api)
    - [升级准备状态](#升级准备状态)
    - [开始或恢复重新索引](#开始或恢复重新索引)
    - [批量启动或恢复重新索引](#批量启动或恢复重新索引)
    - [批量重新索引队列](#批量重新索引队列)
    - [添加默认字段](#添加默认字段)
    - [检查重新索引状态](#检查重新索引状态)
    - [取消重新索引](#取消重新索引)
- [Kibana 插件](#kibana-插件)
  - [故障排除](#故障排除-4)
    - [使用 Kibana 服务器日志](#使用-kibana-服务器日志)
    - [在 Kibana 中跟踪 Elasticsearch 查询到源](#在-kibana-中跟踪-elasticsearch-查询到源)
- [可访问性](#可访问性)
- [发行说明](#发行说明)
  - [Kibana 8.3.3](#kibana-833)
  - [Kibana 8.3.2](#kibana-832)
  - [Kibana 8.3.1](#kibana-831)
  - [Kibana 8.3.0](#kibana-830)
    - [增强功能和错误修复](#增强功能和错误修复)
- [开发者指南](#开发者指南)
  - [入门](#入门)
    - [运行 Kibana](#运行-kibana)
    - [安装示例数据](#安装示例数据)
    - [调试 Kibana](#调试-kibana)
    - [构建 Kibana 可分发](#构建-kibana-可分发)
    - [插件资源](#插件资源)
    - [Kibana Monorepo 包](#kibana-monorepo-包)
  - [最佳实践](#最佳实践)
    - [保持 Kibana 快速](#保持-kibana-快速)
    - [路由、导航和 URL](#路由导航和-url)
    - [稳定](#稳定)
    - [安全最佳实践](#安全最佳实践)
    - [打字稿](#打字稿)
  - [建筑学](#建筑学)
    - [Kibana 插件 API](#kibana-插件-api)
    - [Kibana 核心 API](#kibana-核心-api)
    - [申请服务](#申请服务)
    - [配置服务](#配置服务)
    - [弹性搜索服务](#弹性搜索服务)
    - [HTTP 服务](#http-服务)
    - [日志服务](#日志服务)
    - [记录配置更改](#记录配置更改)
    - [保存对象服务](#保存对象服务)
    - [界面设置服务](#界面设置服务)
    - [模式](#模式)
    - [安全](#安全-3)
    - [添加数据教程](#添加数据教程)
    - [开发可视化](#开发可视化)
    - [报告集成](#报告集成)
  - [贡献](#贡献)
    - [我们如何使用 Git 和 GitHub](#我们如何使用-git-和-github)
    - [测试](#测试)
    - [解释 CI 失败](#解释-ci-失败)
    - [CI 指标](#ci-指标)
    - [开发过程中的文档](#开发过程中的文档)
    - [提交拉取请求](#提交拉取请求)
    - [Kibana 中的有效问题报告](#kibana-中的有效问题报告)
    - [拉取请求审查指南](#拉取请求审查指南)
    - [掉毛](#掉毛)
  - [外部插件开发](#外部插件开发)
    - [插件工具](#插件工具)
    - [将旧插件迁移到 Kibana 平台](#将旧插件迁移到-kibana-平台)
    - [迁移示例](#迁移示例)
    - [Kibana 存储库之外的插件功能测试](#kibana-存储库之外的插件功能测试)
    - [Kibana 存储库之外的插件本地化](#kibana-存储库之外的插件本地化)
    - [测试 Kibana 插件](#测试-kibana-插件)
  - [先进的](#先进的)
    - [每日 Elasticsearch 快照](#每日-elasticsearch-快照)
    - [在开发期间运行 Elasticsearch](#在开发期间运行-elasticsearch)
    - [基本路径的注意事项](#基本路径的注意事项)
    - [升级 Node.js](#升级-nodejs)
    - [共享保存的对象](#共享保存的对象)
    - [旧版 URL 别名](#旧版-url-别名)
  - [Kibana 插件列表](#kibana-插件列表)
    - [仪表板插件](#仪表板插件)
    - [可嵌入插件](#可嵌入插件)
    - [<code class="literal">expressions</code>插入](#code-classliteralexpressionscode插入)
    - [用户界面操作](#用户界面操作)
    - [仪表板应用程序增强插件](#仪表板应用程序增强插件)
    - [增强的可嵌入插件](#增强的可嵌入插件)
    - [翻译插件](#翻译插件)
  - [开发遥测](#开发遥测)
- [参考资料](#参考资料)

<!-- /TOC -->

# .

## Kibana 概念

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-concepts-analysts.html)

### 创建数据视图

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views.html)

### 设置时间范围

* [详情](https://www.elastic.co/guide/en/kibana/8.3/set-time-filter.html)

### Kibana 查询语言

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kuery-query.html)

### Lucene 查询语法

* [详情](https://www.elastic.co/guide/en/kibana/8.3/lucene-query.html)

### 保存查询

* [详情](https://www.elastic.co/guide/en/kibana/8.3/save-load-delete-query.html)

# 快速开始

* [详情](https://www.elastic.co/guide/en/kibana/8.3/get-started.html)

# 设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/setup.html)

## 安装 Kibana

* [详情](https://www.elastic.co/guide/en/kibana/8.3/install.html)

### 在 Linux 或 macOS 上从存档安装

* [详情](https://www.elastic.co/guide/en/kibana/8.3/targz.html)

### 在 Windows 上安装

* [详情](https://www.elastic.co/guide/en/kibana/8.3/windows.html)

### 使用 Debian 软件包安装

* [详情](https://www.elastic.co/guide/en/kibana/8.3/deb.html)

### 使用 RPM 安装

* [详情](https://www.elastic.co/guide/en/kibana/8.3/rpm.html)

### 使用 Docker 安装

* [详情](https://www.elastic.co/guide/en/kibana/8.3/docker.html)

## 配置 Kibana

* [详情](https://www.elastic.co/guide/en/kibana/8.3/settings.html)

### 警报和操作设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/alert-action-settings-kb.html)

### APM 设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-settings-kb.html)

### 横幅设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/banners-settings-kb.html)

### 企业搜索设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/enterprise-search-settings-kb.html)

### 车队设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/fleet-settings-kb.html)

### i18n 设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/i18n-settings-kb.html)

### 记录设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/logging-settings.html)

### 日志设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/logs-ui-settings-kb.html)

### 指标设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/infrastructure-ui-settings-kb.html)

### 监控设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/monitoring-settings-kb.html)

### 报告设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/reporting-settings-kb.html)

### 搜索会话设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/search-session-settings-kb.html)

### 安全设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/secure-settings.html)

### 安全设定

* [详情](https://www.elastic.co/guide/en/kibana/8.3/security-settings-kb.html)

### 空间设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/spaces-settings-kb.html)

### 任务管理器设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/task-manager-settings-kb.html)

### 遥测设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/telemetry-settings-kbn.html)

### URL 向下钻取设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/url-drilldown-settings-kb.html)

## 启动和停止 Kibana

* [详情](https://www.elastic.co/guide/en/kibana/8.3/start-stop.html)

## 访问 Kibana

* [详情](https://www.elastic.co/guide/en/kibana/8.3/access.html)

## 保护对 Kibana 的访问

* [详情](https://www.elastic.co/guide/en/kibana/8.3/tutorial-secure-access-to-kibana.html)

## 添加数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/connect-to-elasticsearch.html)

## 升级 Kibana

* [详情](https://www.elastic.co/guide/en/kibana/8.3/upgrade.html)

### 迁移保存的对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-object-migrations.html)

### 解决迁移失败

* [详情](https://www.elastic.co/guide/en/kibana/8.3/resolve-migrations-failures.html)

### 回滚到以前版本的 Kibana

* [详情](https://www.elastic.co/guide/en/kibana/8.3/upgrade-migrations-rolling-back.html)

## 配置安全性

* [详情](https://www.elastic.co/guide/en/kibana/8.3/using-kibana-with-security.html)

### 验证

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-authentication.html)

### 使用 Elasticsearch 的双向 TLS

* [详情](https://www.elastic.co/guide/en/kibana/8.3/elasticsearch-mutual-tls.html)

### 审核日志

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-security-audit-logging.html)

### 访问协议

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-security-access-agreement.html)

### 会话管理

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-security-session-management.html)

### 保护保存的对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-security-secure-saved-objects.html)

## 配置报告

* [详情](https://www.elastic.co/guide/en/kibana/8.3/secure-reporting.html)

## 配置日志记录

* [详情](https://www.elastic.co/guide/en/kibana/8.3/logging-configuration.html)

### 例子

* [详情](https://www.elastic.co/guide/en/kibana/8.3/log-settings-examples.html)

### 命令行配置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/_cli_configuration.html)

## 配置监控

* [详情](https://www.elastic.co/guide/en/kibana/8.3/configuring-monitoring.html)

### 使用 Metricbeat 收集监控数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/monitoring-metricbeat.html)

### 查看监控数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/monitoring-data.html)

### 遗留收集方法

* [详情](https://www.elastic.co/guide/en/kibana/8.3/monitoring-kibana.html)

## 命令行工具

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cli-commands.html)

### kibana 加密密钥

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-encryption-keys.html)

### kibana-验证码

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-verification-code.html)

# 生产注意事项

* [详情](https://www.elastic.co/guide/en/kibana/8.3/production.html)

## 安全

* [详情](https://www.elastic.co/guide/en/kibana/8.3/Security-production-considerations.html)

## 警报

* [详情](https://www.elastic.co/guide/en/kibana/8.3/alerting-production-considerations.html)

## 报告

* [详情](https://www.elastic.co/guide/en/kibana/8.3/reporting-production-considerations.html)

## 任务管理器

* [详情](https://www.elastic.co/guide/en/kibana/8.3/task-manager-production-considerations.html)

### 健康监测

* [详情](https://www.elastic.co/guide/en/kibana/8.3/task-manager-health-monitoring.html)

### 故障排除

* [详情](https://www.elastic.co/guide/en/kibana/8.3/task-manager-troubleshooting.html)

# 发现

* [详情](https://www.elastic.co/guide/en/kibana/8.3/discover.html)

## 浏览您的文档

* [详情](https://www.elastic.co/guide/en/kibana/8.3/document-explorer.html)

## 搜索相关性

* [详情](https://www.elastic.co/guide/en/kibana/8.3/discover-search-for-relevance.html)

## 保存搜索以供重复使用

* [详情](https://www.elastic.co/guide/en/kibana/8.3/save-open-search.html)

## 在后台运行搜索会话

* [详情](https://www.elastic.co/guide/en/kibana/8.3/search-sessions.html)

## 查看字段统计信息

* [详情](https://www.elastic.co/guide/en/kibana/8.3/show-field-statistics.html)

# 仪表板和可视化

* [详情](https://www.elastic.co/guide/en/kibana/8.3/dashboard.html" class="current_page)

## 创建您的第一个仪表板

* [详情](https://www.elastic.co/guide/en/kibana/8.3/create-a-dashboard-of-panels-with-web-server-data.html)

## 分析时间序列数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/create-a-dashboard-of-panels-with-ecommerce-data.html)

## 使用编辑器创建面板

* [详情](https://www.elastic.co/guide/en/kibana/8.3/aggregation-reference.html)

### 镜片

* [详情](https://www.elastic.co/guide/en/kibana/8.3/lens.html)

### TSVB

* [详情](https://www.elastic.co/guide/en/kibana/8.3/tsvb.html)

### 织女星

* [详情](https://www.elastic.co/guide/en/kibana/8.3/vega.html)

### 基于聚合

* [详情](https://www.elastic.co/guide/en/kibana/8.3/add-aggregation-based-visualization-panels.html)

### 天狮

* [详情](https://www.elastic.co/guide/en/kibana/8.3/timelion.html)

## 使仪表板具有交互性

* [详情](https://www.elastic.co/guide/en/kibana/8.3/drilldowns.html)

### 使用控件过滤仪表板数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/add-controls.html)

### 在 Discover 中打开面板数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/explore-the-underlying-documents.html)

### 自定义与向下钻取的交互

* [详情](https://www.elastic.co/guide/en/kibana/8.3/create-drilldowns.html)

## 改善仪表板加载时间

* [详情](https://www.elastic.co/guide/en/kibana/8.3/dashboard-troubleshooting.html)

# 帆布

* [详情](https://www.elastic.co/guide/en/kibana/8.3/canvas.html)

## 编辑工作台

* [详情](https://www.elastic.co/guide/en/kibana/8.3/edit-workpads.html)

## 展示您的工作台

* [详情](https://www.elastic.co/guide/en/kibana/8.3/canvas-present-workpad.html)

## 教程：创建用于监控销售的工作台

* [详情](https://www.elastic.co/guide/en/kibana/8.3/canvas-tutorial.html)

## 画布表达式生命周期

* [详情](https://www.elastic.co/guide/en/kibana/8.3/canvas-expression-lifecycle.html)

### 表达式总是以函数开头

* [详情](https://www.elastic.co/guide/en/kibana/8.3/canvas-expressions-always-start-with-a-function.html)

### 函数参数

* [详情](https://www.elastic.co/guide/en/kibana/8.3/canvas-function-arguments.html)

### 别名和未命名参数

* [详情](https://www.elastic.co/guide/en/kibana/8.3/canvas-aliases-and-unnamed-arguments.html)

### 改变你的表达，改变你的输出

* [详情](https://www.elastic.co/guide/en/kibana/8.3/canvas-change-your-expression-change-your-output.html)

### 获取和操作数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/canvas-fetch-and-manipulate-data.html)

### 用子表达式组合函数

* [详情](https://www.elastic.co/guide/en/kibana/8.3/canvas-expressions-compose-functions-with-subexpressions.html)

### 处理上下文和参数类型

* [详情](https://www.elastic.co/guide/en/kibana/8.3/canvas-handling-context-and-argument-types.html)

## 画布功能参考

* [详情](https://www.elastic.co/guide/en/kibana/8.3/canvas-function-reference.html)

### TinyMath 函数

* [详情](https://www.elastic.co/guide/en/kibana/8.3/canvas-tinymath-functions.html)

# 地图

* [详情](https://www.elastic.co/guide/en/kibana/8.3/maps.html)

## 构建地图以按国家或地区比较指标

* [详情](https://www.elastic.co/guide/en/kibana/8.3/maps-getting-started.html)

## 实时跟踪、可视化和提醒资产

* [详情](https://www.elastic.co/guide/en/kibana/8.3/asset-tracking-tutorial.html)

## 使用反向地理编码映射自定义区域

* [详情](https://www.elastic.co/guide/en/kibana/8.3/reverse-geocoding-tutorial.html)

## 热图图层

* [详情](https://www.elastic.co/guide/en/kibana/8.3/heatmap-layer.html)

## 平铺层

* [详情](https://www.elastic.co/guide/en/kibana/8.3/tile-layer.html)

## 矢量图层

* [详情](https://www.elastic.co/guide/en/kibana/8.3/vector-layer.html)

### 矢量造型

* [详情](https://www.elastic.co/guide/en/kibana/8.3/vector-style.html)

### 矢量样式属性

* [详情](https://www.elastic.co/guide/en/kibana/8.3/maps-vector-style-properties.html)

### 矢量工具提示

* [详情](https://www.elastic.co/guide/en/kibana/8.3/vector-tooltip.html)

## 绘制大数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/maps-aggregations.html)

### 集群

* [详情](https://www.elastic.co/guide/en/kibana/8.3/maps-grid-aggregation.html)

### 显示每个实体最相关的文档

* [详情](https://www.elastic.co/guide/en/kibana/8.3/maps-top-hits-aggregation.html)

### 点对点

* [详情](https://www.elastic.co/guide/en/kibana/8.3/point-to-point.html)

### 长期加入

* [详情](https://www.elastic.co/guide/en/kibana/8.3/terms-join.html)

## 搜索地理数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/maps-search.html)

### 从地图创建过滤器

* [详情](https://www.elastic.co/guide/en/kibana/8.3/maps-create-filter-from-map.html)

### 过滤单层

* [详情](https://www.elastic.co/guide/en/kibana/8.3/maps-layer-based-filtering.html)

### 跨多个索引搜索

* [详情](https://www.elastic.co/guide/en/kibana/8.3/maps-search-across-multiple-indices.html)

## 配置地图设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/maps-settings.html)

## 连接到弹性地图服务

* [详情](https://www.elastic.co/guide/en/kibana/8.3/maps-connect-to-ems.html)

## 导入地理空间数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/import-geospatial-data.html)

### 教程：索引 GeoJSON 数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/indexing-geojson-data-tutorial.html)

## 疑难解答

* [详情](https://www.elastic.co/guide/en/kibana/8.3/maps-troubleshooting.html)

# 报告和分享

* [详情](https://www.elastic.co/guide/en/kibana/8.3/reporting-getting-started.html)

## 自动生成报告

* [详情](https://www.elastic.co/guide/en/kibana/8.3/automating-report-generation.html)

## 故障排除

* [详情](https://www.elastic.co/guide/en/kibana/8.3/reporting-troubleshooting.html)

# 机器学习

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-ml.html)

## 异常检测

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-ml-anomalies.html)

## 数据框分析

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-ml-dfanalytics.html)

# 图形

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-graph.html)

## 配置图表

* [详情](https://www.elastic.co/guide/en/kibana/8.3/graph-configuration.html)

## 故障排除和限制

* [详情](https://www.elastic.co/guide/en/kibana/8.3/graph-troubleshooting.html)

# 警报

* [详情](https://www.elastic.co/guide/en/kibana/8.3/alerting-getting-started.html)

## 设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/alerting-setup.html)

## 创建和管理规则

* [详情](https://www.elastic.co/guide/en/kibana/8.3/create-and-manage-rules.html)

## 规则类型

* [详情](https://www.elastic.co/guide/en/kibana/8.3/rule-types.html)

### 指标阈值

* [详情](https://www.elastic.co/guide/en/kibana/8.3/rule-type-index-threshold.html)

### 弹性搜索查询

* [详情](https://www.elastic.co/guide/en/kibana/8.3/rule-type-es-query.html)

### 追踪遏制

* [详情](https://www.elastic.co/guide/en/kibana/8.3/geo-alerting.html)

## 故障排除和限制

* [详情](https://www.elastic.co/guide/en/kibana/8.3/alerting-troubleshooting.html)

### 常见问题

* [详情](https://www.elastic.co/guide/en/kibana/8.3/alerting-common-issues.html)

### 事件日志索引

* [详情](https://www.elastic.co/guide/en/kibana/8.3/event-log-index.html)

### 测试连接器

* [详情](https://www.elastic.co/guide/en/kibana/8.3/testing-connectors.html)

# 可观察性

* [详情](https://www.elastic.co/guide/en/kibana/8.3/observability.html)

# APM

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-apm.html)

## 设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-ui.html)

## 开始使用

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-getting-started.html)

### 服务

* [详情](https://www.elastic.co/guide/en/kibana/8.3/services.html)

### 痕迹

* [详情](https://www.elastic.co/guide/en/kibana/8.3/traces.html)

### 依赖项

* [详情](https://www.elastic.co/guide/en/kibana/8.3/dependencies.html)

### 服务地图

* [详情](https://www.elastic.co/guide/en/kibana/8.3/service-maps.html)

### 服务概览

* [详情](https://www.elastic.co/guide/en/kibana/8.3/service-overview.html)

### 交易

* [详情](https://www.elastic.co/guide/en/kibana/8.3/transactions.html)

### 跟踪样本时间线

* [详情](https://www.elastic.co/guide/en/kibana/8.3/spans.html)

### 错误

* [详情](https://www.elastic.co/guide/en/kibana/8.3/errors.html)

### 指标

* [详情](https://www.elastic.co/guide/en/kibana/8.3/metrics.html)

## 操作指南

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-how-to.html)

### 使用中央配置配置 APM 代理

* [详情](https://www.elastic.co/guide/en/kibana/8.3/agent-configuration.html)

### 控制对 APM 数据的访问

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-spaces.html)

### 创建警报

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-alerts.html)

### 创建自定义链接

* [详情](https://www.elastic.co/guide/en/kibana/8.3/custom-links.html)

### 过滤数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/filters.html)

### 查找事务延迟和故障相关性

* [详情](https://www.elastic.co/guide/en/kibana/8.3/correlations.html)

### 与机器学习集成

* [详情](https://www.elastic.co/guide/en/kibana/8.3/machine-learning-integration.html)

### 查询您的数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/advanced-queries.html)

### 使用注释跟踪部署

* [详情](https://www.elastic.co/guide/en/kibana/8.3/transactions-annotations.html)

## 用户和权限

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-app-users.html)

### 创建 APM 阅读器用户

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-app-reader.html)

### 创建注释用户

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-app-annotation-user-create.html)

### 创建中央配置用户

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-app-central-config-user.html)

### 创建 API 用户

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-app-api-user.html)

## 设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-settings-in-kibana.html)

## REST API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-api.html)

### 代理配置 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/agent-config-api.html)

### 注释 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/apm-annotation-api.html)

### RUM 源地图 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/rum-sourcemap-api.html)

### APM 代理密钥 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/agent-key-api.html)

## 故障排除

* [详情](https://www.elastic.co/guide/en/kibana/8.3/troubleshooting.html)

# 安全

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-siem.html)

# 开发工具

* [详情](https://www.elastic.co/guide/en/kibana/8.3/devtools-kibana.html)

## 运行 API 请求

* [详情](https://www.elastic.co/guide/en/kibana/8.3/console-kibana.html)

## 配置文件查询和聚合

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-profiler.html)

## 调试 grok 表达式

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-grokdebugger.html)

## 调试无痛脚本

* [详情](https://www.elastic.co/guide/en/kibana/8.3/painlesslab.html)

# 舰队

* [详情](https://www.elastic.co/guide/en/kibana/8.3/fleet.html)

# 奥斯查询

* [详情](https://www.elastic.co/guide/en/kibana/8.3/osquery.html)

## 管理集成

* [详情](https://www.elastic.co/guide/en/kibana/8.3/manage-osquery-integration.html)

## 导出字段参考

* [详情](https://www.elastic.co/guide/en/kibana/8.3/exported-fields-osquery.html)

## 预建包参考

* [详情](https://www.elastic.co/guide/en/kibana/8.3/prebuilt-packs.html)

## Osquery 常见问题

* [详情](https://www.elastic.co/guide/en/kibana/8.3/osquery-faq.html)

# 堆栈监控

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-monitoring.html)

## 节拍指标

* [详情](https://www.elastic.co/guide/en/kibana/8.3/beats-page.html)

## 弹性搜索指标

* [详情](https://www.elastic.co/guide/en/kibana/8.3/elasticsearch-metrics.html)

## Kibana 警报

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-alerts.html)

## Kibana 指标

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-page.html)

## Logstash 指标

* [详情](https://www.elastic.co/guide/en/kibana/8.3/logstash-page.html)

## 故障排除

* [详情](https://www.elastic.co/guide/en/kibana/8.3/monitor-troubleshooting.html)

# 堆栈管理

* [详情](https://www.elastic.co/guide/en/kibana/8.3/management.html)

## 高级设置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/advanced-options.html)

## 案例

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases.html)

### 配置对案例的访问

* [详情](https://www.elastic.co/guide/en/kibana/8.3/setup-cases.html)

### 打开和管理案例

* [详情](https://www.elastic.co/guide/en/kibana/8.3/manage-cases.html)

### 添加连接器

* [详情](https://www.elastic.co/guide/en/kibana/8.3/add-case-connectors.html)

## 连接器

* [详情](https://www.elastic.co/guide/en/kibana/8.3/action-types.html)

### 电子邮件

* [详情](https://www.elastic.co/guide/en/kibana/8.3/email-action-type.html)

### IBM 弹性

* [详情](https://www.elastic.co/guide/en/kibana/8.3/resilient-action-type.html)

### 指数

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-action-type.html)

### 吉拉

* [详情](https://www.elastic.co/guide/en/kibana/8.3/jira-action-type.html)

### 微软团队

* [详情](https://www.elastic.co/guide/en/kibana/8.3/teams-action-type.html)

### 寻呼机

* [详情](https://www.elastic.co/guide/en/kibana/8.3/pagerduty-action-type.html)

### 服务器日志

* [详情](https://www.elastic.co/guide/en/kibana/8.3/server-log-action-type.html)

### ServiceNow ITSM

* [详情](https://www.elastic.co/guide/en/kibana/8.3/servicenow-action-type.html)

### ServiceNow SecOps

* [详情](https://www.elastic.co/guide/en/kibana/8.3/servicenow-sir-action-type.html)

### ServiceNow ITOM

* [详情](https://www.elastic.co/guide/en/kibana/8.3/servicenow-itom-action-type.html)

### 泳道

* [详情](https://www.elastic.co/guide/en/kibana/8.3/swimlane-action-type.html)

### 松弛

* [详情](https://www.elastic.co/guide/en/kibana/8.3/slack-action-type.html)

### 网络钩子

* [详情](https://www.elastic.co/guide/en/kibana/8.3/webhook-action-type.html)

### xMatters

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xmatters-action-type.html)

### 预配置的连接器

* [详情](https://www.elastic.co/guide/en/kibana/8.3/pre-configured-connectors.html)

## 许可证管理

* [详情](https://www.elastic.co/guide/en/kibana/8.3/managing-licenses.html)

## 管理数据视图

* [详情](https://www.elastic.co/guide/en/kibana/8.3/managing-data-views.html)

## 数字格式

* [详情](https://www.elastic.co/guide/en/kibana/8.3/numeral.html)

## 汇总作业

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-rollups.html)

## 保存的对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/managing-saved-objects.html)

### 保存的对象 ID

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-object-ids.html)

## 安全

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-security.html)

### 授予对 Kibana 的访问权限

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-security-authorization.html)

### Kibana 角色管理

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-role-management.html)

### Kibana 特权

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-privileges.html)

### API 密钥

* [详情](https://www.elastic.co/guide/en/kibana/8.3/api-keys.html)

### 角色映射

* [详情](https://www.elastic.co/guide/en/kibana/8.3/role-mappings.html)

## 空间

* [详情](https://www.elastic.co/guide/en/kibana/8.3/xpack-spaces.html)

## 标签

* [详情](https://www.elastic.co/guide/en/kibana/8.3/managing-tags.html)

## 观察者

* [详情](https://www.elastic.co/guide/en/kibana/8.3/watcher-ui.html)

# REST API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/api.html)

## 获取功能 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/features-api-get.html)

## Kibana 空间 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/spaces-api.html)

### 创造空间

* [详情](https://www.elastic.co/guide/en/kibana/8.3/spaces-api-post.html)

### 更新空间

* [详情](https://www.elastic.co/guide/en/kibana/8.3/spaces-api-put.html)

### 获得空间

* [详情](https://www.elastic.co/guide/en/kibana/8.3/spaces-api-get.html)

### 获取所有空间

* [详情](https://www.elastic.co/guide/en/kibana/8.3/spaces-api-get-all.html)

### 删除空间

* [详情](https://www.elastic.co/guide/en/kibana/8.3/spaces-api-delete.html)

### 将保存的对象复制到空间

* [详情](https://www.elastic.co/guide/en/kibana/8.3/spaces-api-copy-saved-objects.html)

### 解决复制到空间冲突

* [详情](https://www.elastic.co/guide/en/kibana/8.3/spaces-api-resolve-copy-saved-objects-conflicts.html)

### 禁用旧版 URL 别名

* [详情](https://www.elastic.co/guide/en/kibana/8.3/spaces-api-disable-legacy-url-aliases.html)

## Kibana 角色管理 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/role-management-api.html)

### 创建或更新角色

* [详情](https://www.elastic.co/guide/en/kibana/8.3/role-management-api-put.html)

### 获取特定角色

* [详情](https://www.elastic.co/guide/en/kibana/8.3/role-management-specific-api-get.html)

### 获取所有角色

* [详情](https://www.elastic.co/guide/en/kibana/8.3/role-management-api-get.html)

### 删除角色

* [详情](https://www.elastic.co/guide/en/kibana/8.3/role-management-api-delete.html)

## 用户会话管理 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/session-management-api.html)

### 使用户会话无效

* [详情](https://www.elastic.co/guide/en/kibana/8.3/session-management-api-invalidate.html)

## 保存的对象 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api.html)

### 获取对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-get.html)

### 批量获取对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-bulk-get.html)

### 查找对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-find.html)

### 创建保存的对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-create.html)

### 批量创建保存的对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-bulk-create.html)

### 更新对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-update.html)

### 批量更新对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-bulk-update.html)

### 删除对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-delete.html)

### 导出对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-export.html)

### 导入对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-import.html)

### 解决导入错误

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-resolve-import-errors.html)

### 解决对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-resolve.html)

### 批量解析对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-bulk-resolve.html)

### 轮换加密密钥

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-api-rotate-encryption-key.html)

## 数据视图 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views-api.html)

### 获取数据视图

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views-api-get.html)

### 创建数据视图

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views-api-create.html)

### 更新数据视图

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views-api-update.html)

### 删除数据视图

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views-api-delete.html)

### 获取默认数据视图

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views-api-default-get.html)

### 设置默认数据视图

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views-api-default-set.html)

### 更新数据视图字段元数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views-fields-api-update.html)

### 获取运行时字段

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views-runtime-field-api-get.html)

### 创建运行时字段

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views-runtime-field-api-create.html)

### Upsert 运行时字段

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views-runtime-field-api-upsert.html)

### 更新运行时字段

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views-runtime-field-api-update.html)

### 删除运行时字段

* [详情](https://www.elastic.co/guide/en/kibana/8.3/data-views-runtime-field-api-delete.html)

## 索引模式 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-patterns-api.html)

### 获取索引模式

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-patterns-api-get.html)

### 创建索引模式

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-patterns-api-create.html)

### 更新索引模式

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-patterns-api-update.html)

### 删除索引模式

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-patterns-api-delete.html)

### 获取默认索引模式

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-patterns-api-default-get.html)

### 设置默认索引模式

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-patterns-api-default-set.html)

### 更新索引模式字段元数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-patterns-fields-api-update.html)

### 获取运行时字段

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-patterns-runtime-field-api-get.html)

### 创建运行时字段

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-patterns-runtime-field-api-create.html)

### Upsert 运行时字段

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-patterns-runtime-field-api-upsert.html)

### 更新运行时字段

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-patterns-runtime-field-api-update.html)

### 删除运行时字段

* [详情](https://www.elastic.co/guide/en/kibana/8.3/index-patterns-runtime-field-api-delete.html)

## 警报 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/alerting-apis.html)

### 创建规则

* [详情](https://www.elastic.co/guide/en/kibana/8.3/create-rule-api.html)

### 删除规则

* [详情](https://www.elastic.co/guide/en/kibana/8.3/delete-rule-api.html)

### 禁用规则

* [详情](https://www.elastic.co/guide/en/kibana/8.3/disable-rule-api.html)

### 启用规则

* [详情](https://www.elastic.co/guide/en/kibana/8.3/enable-rule-api.html)

### 查找规则

* [详情](https://www.elastic.co/guide/en/kibana/8.3/find-rules-api.html)

### 获取警报框架运行状况

* [详情](https://www.elastic.co/guide/en/kibana/8.3/get-alerting-framework-health-api.html)

### 获取规则

* [详情](https://www.elastic.co/guide/en/kibana/8.3/get-rule-api.html)

### 获取规则类型

* [详情](https://www.elastic.co/guide/en/kibana/8.3/list-rule-types-api.html)

### 更新规则

* [详情](https://www.elastic.co/guide/en/kibana/8.3/update-rule-api.html)

### 静音所有警报

* [详情](https://www.elastic.co/guide/en/kibana/8.3/mute-all-alerts-api.html)

### 静音警报

* [详情](https://www.elastic.co/guide/en/kibana/8.3/mute-alert-api.html)

### 取消静音所有警报

* [详情](https://www.elastic.co/guide/en/kibana/8.3/unmute-all-alerts-api.html)

### 取消静音警报

* [详情](https://www.elastic.co/guide/en/kibana/8.3/unmute-alert-api.html)

### 已弃用的 7.x API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/alerts-api.html)

## 操作和连接器 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/actions-and-connectors-api.html)

### 创建连接器

* [详情](https://www.elastic.co/guide/en/kibana/8.3/create-connector-api.html)

### 删除连接器

* [详情](https://www.elastic.co/guide/en/kibana/8.3/delete-connector-api.html)

### 获取连接器

* [详情](https://www.elastic.co/guide/en/kibana/8.3/get-connector-api.html)

### 获取所有连接器

* [详情](https://www.elastic.co/guide/en/kibana/8.3/get-all-connectors-api.html)

### 列出所有连接器类型

* [详情](https://www.elastic.co/guide/en/kibana/8.3/list-connector-types-api.html)

### 运行连接器

* [详情](https://www.elastic.co/guide/en/kibana/8.3/execute-connector-api.html)

### 更新连接器

* [详情](https://www.elastic.co/guide/en/kibana/8.3/update-connector-api.html)

### 已弃用的 7.x API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/actions-and-connectors-legacy-apis.html)

## 案例 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api.html)

### 添加评论

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-add-comment.html)

### 创建案例

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-create.html)

### 删除案例

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-delete-cases.html)

### 删除评论

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-delete-comments.html)

### 查找案例

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-find-cases.html)

### 查找连接器

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-find-connectors.html)

### 获取警报

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-get-alerts.html)

### 获取案例活动

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-get-case-activity.html)

### 获取案例

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-get-case.html)

### 获取案例状态

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-get-status.html)

### 通过警报获取案例

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-get-cases-by-alert.html)

### 获取评论

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-get-comments.html)

### 获取配置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-get-configuration.html)

### 找记者

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-get-reporters.html)

### 获取标签

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-get-tag.html)

### 推壳

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-push.html)

### 设置配置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-set-configuration.html)

### 更新案例

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-update.html)

### 更新评论

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-update-comment.html)

### 更新配置

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cases-api-update-configuration.html)

## 导入和导出仪表板 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/dashboard-api.html)

### 导入仪表板

* [详情](https://www.elastic.co/guide/en/kibana/8.3/dashboard-import-api.html)

### 导出仪表板

* [详情](https://www.elastic.co/guide/en/kibana/8.3/dashboard-api-export.html)

## Logstash 配置管理 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/logstash-configuration-management-api.html)

### 删除管道

* [详情](https://www.elastic.co/guide/en/kibana/8.3/logstash-configuration-management-api-delete.html)

### 列出管道

* [详情](https://www.elastic.co/guide/en/kibana/8.3/logstash-configuration-management-api-list.html)

### 创建 Logstash 管道

* [详情](https://www.elastic.co/guide/en/kibana/8.3/logstash-configuration-management-api-create.html)

### 检索管道

* [详情](https://www.elastic.co/guide/en/kibana/8.3/logstash-configuration-management-api-retrieve.html)

## 机器学习 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/machine-learning-api.html)

### 同步机器学习保存的对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/machine-learning-api-sync.html)

## 短网址 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/short-urls-api.html)

### 创建短网址

* [详情](https://www.elastic.co/guide/en/kibana/8.3/short-urls-api-create.html)

### 获取短网址

* [详情](https://www.elastic.co/guide/en/kibana/8.3/short-urls-api-get.html)

### 删除短网址

* [详情](https://www.elastic.co/guide/en/kibana/8.3/short-urls-api-delete.html)

### 解析短网址

* [详情](https://www.elastic.co/guide/en/kibana/8.3/short-urls-api-resolve.html)

## 获取任务管理器运行状况

* [详情](https://www.elastic.co/guide/en/kibana/8.3/task-manager-api-health.html)

## 升级助手 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/upgrade-assistant-api.html)

### 升级准备状态

* [详情](https://www.elastic.co/guide/en/kibana/8.3/upgrade-assistant-api-status.html)

### 开始或恢复重新索引

* [详情](https://www.elastic.co/guide/en/kibana/8.3/start-resume-reindex.html)

### 批量启动或恢复重新索引

* [详情](https://www.elastic.co/guide/en/kibana/8.3/batch-start-resume-reindex.html)

### 批量重新索引队列

* [详情](https://www.elastic.co/guide/en/kibana/8.3/batch-reindex-queue.html)

### 添加默认字段

* [详情](https://www.elastic.co/guide/en/kibana/8.3/upgrade-assistant-api-default-field.html)

### 检查重新索引状态

* [详情](https://www.elastic.co/guide/en/kibana/8.3/check-reindex-status.html)

### 取消重新索引

* [详情](https://www.elastic.co/guide/en/kibana/8.3/cancel-reindex.html)

# Kibana 插件

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-plugins.html)

## 故障排除

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-troubleshooting.html)

### 使用 Kibana 服务器日志

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-troubleshooting-kibana-server-logs.html)

### 在 Kibana 中跟踪 Elasticsearch 查询到源

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-troubleshooting-trace-query.html)

# 可访问性

* [详情](https://www.elastic.co/guide/en/kibana/8.3/accessibility.html)

# 发行说明

* [详情](https://www.elastic.co/guide/en/kibana/8.3/release-notes.html)

## Kibana 8.3.3

* [详情](https://www.elastic.co/guide/en/kibana/8.3/release-notes-8.3.3.html)

## Kibana 8.3.2

* [详情](https://www.elastic.co/guide/en/kibana/8.3/release-notes-8.3.2.html)

## Kibana 8.3.1

* [详情](https://www.elastic.co/guide/en/kibana/8.3/release-notes-8.3.1.html)

## Kibana 8.3.0

* [详情](https://www.elastic.co/guide/en/kibana/8.3/release-notes-8.3.0.html)

### 增强功能和错误修复

* [详情](https://www.elastic.co/guide/en/kibana/8.3/enhancements-and-bug-fixes-v8.3.0.html)

# 开发者指南

* [详情](https://www.elastic.co/guide/en/kibana/8.3/development.html)

## 入门

* [详情](https://www.elastic.co/guide/en/kibana/8.3/development-getting-started.html)

### 运行 Kibana

* [详情](https://www.elastic.co/guide/en/kibana/8.3/running-kibana-advanced.html)

### 安装示例数据

* [详情](https://www.elastic.co/guide/en/kibana/8.3/sample-data.html)

### 调试 Kibana

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-debugging.html)

### 构建 Kibana 可分发

* [详情](https://www.elastic.co/guide/en/kibana/8.3/building-kibana.html)

### 插件资源

* [详情](https://www.elastic.co/guide/en/kibana/8.3/development-plugin-resources.html)

### Kibana Monorepo 包

* [详情](https://www.elastic.co/guide/en/kibana/8.3/monorepo-packages.html)

## 最佳实践

* [详情](https://www.elastic.co/guide/en/kibana/8.3/development-best-practices.html)

### 保持 Kibana 快速

* [详情](https://www.elastic.co/guide/en/kibana/8.3/plugin-performance.html)

### 路由、导航和 URL

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-navigation.html)

### 稳定

* [详情](https://www.elastic.co/guide/en/kibana/8.3/stability.html)

### 安全最佳实践

* [详情](https://www.elastic.co/guide/en/kibana/8.3/security-best-practices.html)

### 打字稿

* [详情](https://www.elastic.co/guide/en/kibana/8.3/typescript.html)

## 建筑学

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-architecture.html)

### Kibana 插件 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-platform-plugin-api.html)

### Kibana 核心 API

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-platform-api.html)

### 申请服务

* [详情](https://www.elastic.co/guide/en/kibana/8.3/application-service.html)

### 配置服务

* [详情](https://www.elastic.co/guide/en/kibana/8.3/configuration-service.html)

### 弹性搜索服务

* [详情](https://www.elastic.co/guide/en/kibana/8.3/elasticsearch-service.html)

### HTTP 服务

* [详情](https://www.elastic.co/guide/en/kibana/8.3/http-service.html)

### 日志服务

* [详情](https://www.elastic.co/guide/en/kibana/8.3/logging-service.html)

### 记录配置更改

* [详情](https://www.elastic.co/guide/en/kibana/8.3/logging-config-changes.html)

### 保存对象服务

* [详情](https://www.elastic.co/guide/en/kibana/8.3/saved-objects-service.html)

### 界面设置服务

* [详情](https://www.elastic.co/guide/en/kibana/8.3/ui-settings-service.html)

### 模式

* [详情](https://www.elastic.co/guide/en/kibana/8.3/patterns.html)

### 安全

* [详情](https://www.elastic.co/guide/en/kibana/8.3/development-security.html)

### 添加数据教程

* [详情](https://www.elastic.co/guide/en/kibana/8.3/add-data-tutorials.html)

### 开发可视化

* [详情](https://www.elastic.co/guide/en/kibana/8.3/development-visualize-index.html)

### 报告集成

* [详情](https://www.elastic.co/guide/en/kibana/8.3/reporting-integration.html)

## 贡献

* [详情](https://www.elastic.co/guide/en/kibana/8.3/contributing.html)

### 我们如何使用 Git 和 GitHub

* [详情](https://www.elastic.co/guide/en/kibana/8.3/development-github.html)

### 测试

* [详情](https://www.elastic.co/guide/en/kibana/8.3/development-tests.html)

### 解释 CI 失败

* [详情](https://www.elastic.co/guide/en/kibana/8.3/interpreting-ci-failures.html)

### CI 指标

* [详情](https://www.elastic.co/guide/en/kibana/8.3/ci-metrics.html)

### 开发过程中的文档

* [详情](https://www.elastic.co/guide/en/kibana/8.3/development-documentation.html)

### 提交拉取请求

* [详情](https://www.elastic.co/guide/en/kibana/8.3/development-pull-request.html)

### Kibana 中的有效问题报告

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-issue-reporting.html)

### 拉取请求审查指南

* [详情](https://www.elastic.co/guide/en/kibana/8.3/pr-review.html)

### 掉毛

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-linting.html)

## 外部插件开发

* [详情](https://www.elastic.co/guide/en/kibana/8.3/external-plugin-development.html)

### 插件工具

* [详情](https://www.elastic.co/guide/en/kibana/8.3/plugin-tooling.html)

### 将旧插件迁移到 Kibana 平台

* [详情](https://www.elastic.co/guide/en/kibana/8.3/migrating-legacy-plugins.html)

### 迁移示例

* [详情](https://www.elastic.co/guide/en/kibana/8.3/migrating-legacy-plugins-examples.html)

### Kibana 存储库之外的插件功能测试

* [详情](https://www.elastic.co/guide/en/kibana/8.3/external-plugin-functional-tests.html)

### Kibana 存储库之外的插件本地化

* [详情](https://www.elastic.co/guide/en/kibana/8.3/external-plugin-localization.html)

### 测试 Kibana 插件

* [详情](https://www.elastic.co/guide/en/kibana/8.3/testing-kibana-plugin.html)

## 先进的

* [详情](https://www.elastic.co/guide/en/kibana/8.3/advanced.html)

### 每日 Elasticsearch 快照

* [详情](https://www.elastic.co/guide/en/kibana/8.3/development-es-snapshots.html)

### 在开发期间运行 Elasticsearch

* [详情](https://www.elastic.co/guide/en/kibana/8.3/running-elasticsearch.html)

### 基本路径的注意事项

* [详情](https://www.elastic.co/guide/en/kibana/8.3/development-basepath.html)

### 升级 Node.js

* [详情](https://www.elastic.co/guide/en/kibana/8.3/upgrading-nodejs.html)

### 共享保存的对象

* [详情](https://www.elastic.co/guide/en/kibana/8.3/sharing-saved-objects.html)

### 旧版 URL 别名

* [详情](https://www.elastic.co/guide/en/kibana/8.3/legacy-url-aliases.html)

## Kibana 插件列表

* [详情](https://www.elastic.co/guide/en/kibana/8.3/plugin-list.html)

### 仪表板插件

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-dashboard-plugin.html)

### 可嵌入插件

* [详情](https://www.elastic.co/guide/en/kibana/8.3/embeddable-plugin.html)

### <code class="literal">expressions</code>插入

* [详情](https://www.elastic.co/guide/en/kibana/8.3/kibana-expressions-plugin.html)

### 用户界面操作

* [详情](https://www.elastic.co/guide/en/kibana/8.3/uiactions-plugin.html)

### 仪表板应用程序增强插件

* [详情](https://www.elastic.co/guide/en/kibana/8.3/dashboard-enhanced-plugin.html)

### 增强的可嵌入插件

* [详情](https://www.elastic.co/guide/en/kibana/8.3/enhanced-embeddables-plugin.html)

### 翻译插件

* [详情](https://www.elastic.co/guide/en/kibana/8.3/translations-plugin.html)

## 开发遥测

* [详情](https://www.elastic.co/guide/en/kibana/8.3/development-telemetry.html)

# 参考资料

> [Kibana 指南](https://www.elastic.co/guide/en/kibana/8.3/index.html)