<!-- TOC -->

- [说明](#说明)
  - [关于Elasticsearch](#关于elasticsearch)
    - [关于 Elasticsearch 的三件事](#关于-elasticsearch-的三件事)
  - [面向版本](#面向版本)
  - [多租户](#多租户)
  - [Node与Cluster](#node与cluster)
  - [Index](#index)
  - [Document](#document)
  - [Type](#type)
  - [数据类型](#数据类型)
    - [二进制(binary)](#二进制binary)
    - [布尔(boolean)](#布尔boolean)
    - [关键字族(Keywords)](#关键字族keywords)
      - [常量关键字字段的参数](#常量关键字字段的参数)
      - [通配符字段类型](#通配符字段类型)
    - [数值族(Numbers)](#数值族numbers)
    - [日期族(Dates)](#日期族dates)
      - [毫秒型(date)](#毫秒型date)
      - [纳秒型(date_nanos)](#纳秒型date_nanos)
    - [别名(alias)](#别名alias)
    - [对象和关系类型](#对象和关系类型)
    - [结构化数据类型](#结构化数据类型)
    - [汇总数据类型](#汇总数据类型)
    - [文字搜寻类型](#文字搜寻类型)
    - [文件等级类型](#文件等级类型)
    - [空间数据类型](#空间数据类型)
    - [其他种类](#其他种类)
    - [数组](#数组)
    - [多领域](#多领域)
- [常用功能](#常用功能)
  - [新建和删除索引(Index)](#新建和删除索引index)
  - [中文分词设置流程](#中文分词设置流程)
    - [安装分词插件](#安装分词插件)
    - [新建索引(Index)](#新建索引index)
  - [新增记录](#新增记录)
  - [根据id查看记录](#根据id查看记录)
  - [删除记录](#删除记录)
  - [更新记录](#更新记录)
  - [返回所有记录](#返回所有记录)
  - [全文搜索](#全文搜索)
  - [逻辑运算](#逻辑运算)
- [运行维护](#运行维护)
  - [启动](#启动)
  - [检查是否运行](#检查是否运行)
  - [查询进程](#查询进程)
  - [使用容器(单实例)](#使用容器单实例)
    - [启动](#启动-1)
    - [进入容器](#进入容器)
  - [项目根目录](#项目根目录)
    - [config](#config)
      - [elasticsearch.yml](#elasticsearchyml)
        - [配置跨域](#配置跨域)
- [常见问题](#常见问题)
  - [Could not register mbeans](#could-not-register-mbeans)
  - [vm.max_map_count [65530] is too low](#vmmax_map_count-65530-is-too-low)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## 关于Elasticsearch

Elasticsearch可用于搜索各种文档.<br>
它提供可扩展的搜索,近实时搜索,并支持多租户.

### 关于 Elasticsearch 的三件事

无需了解有关Elasticsearch的所有知识即可使用Kibana,但最重要的概念如下:

0. Elasticsearch使JSON文档可搜索和可聚合.<br>
文档存储在索引或数据流中,它们代表一种数据.

0. 可搜索意味着可以根据条件过滤文档.<br>
例如,可以过滤 *过去7天内* 的数据或 *包含Kibana单词* 的数据.

0. 可聚合意味着可以从匹配的文档中提取摘要.<br>
最简单的聚合是count,它经常与日期直方图结合使用,以查看随时间变化的计数.术语聚合显示最常见的值.


## 面向版本

该文档面向7.X版本.

## 多租户

*软件多租户* 指的是软件架构,<br>
其中一个实例的软件在服务器上运行,并提供多租户.<br>
租户是一组用户,共享具有软件实例特定权限的公共访问权限.<br>
通过多租户架构,软件应用程序旨在为每个租户提供实例的专用共享,<br>
包括其数据,配置,用户管理,租户个人功能和非功能属性.<br>
多租户与多实例架构形成对比,其中 **单独的软件实例代表不同的租户运行** .

## Node与Cluster

Elastic本质上是一个分布式数据库,允许多台服务器协同工作,每台服务器可以运行多个Elastic实例.

单个Elastic实例称为一个节点(node).一组节点构成一个集群(cluster).

## Index

Elastic会索引所有字段,经过处理后写入一个反向索引(Inverted Index).<br>
查找数据的时候,直接查找该索引.

所以,Elastic数据管理的顶层单位就叫做Index(索引).<br>
它是单个数据库的同义词.<br>
每个Index(即数据库)的名字必须是小写.

## Document

Index里面单条的记录称为Document(文档).<br>
许多条Document构成了一个Index.

Document使用JSON格式表示,下面是一个例子.

```json
{
  "user": "张三",
  "title": "工程师",
  "desc": "数据库管理"
}
```

同一个Index里面的Document,不要求有相同的结构(scheme),但是最好保持相同,这样有利于提高搜索效率.

## Type

Document可以分组,比如weather这个Index里面,可以按城市分组(北京和上海),也可以按气候分组(晴天和雨天).<br>
这种分组就叫做Type,它是虚拟的逻辑分组,用来过滤Document.

不同的Type应该有相似的结构(schema),举例来说,id字段不能在这个组是字符串,在另一个组是数值.<br>
这是与关系型数据库的表的一个区别.<br>
性质完全不同的数据(比如products和logs)应该存成两个Index,而不是一个Index里面的两个Type(虽然可以做到).

根据规划,Elastic 6.x版只允许每个Index包含一个Type,7.x版将会彻底移除Type.

## 数据类型

### 二进制(binary)

binary类型接受二进制值作为 Base64编码的字符串.<br>
该字段默认情况下不存储,并且不可搜索.

Base64编码的二进制值不能包含嵌入式换行符\n.

```json
// PUT my-index-000001
{
  "mappings": {
    "properties": {
      "name": {
        "type": "text"
      },
      "blob": {
        "type": "binary"
      }
    }
  }
}

// PUT my-index-000001/_doc/1
{
  "name": "Some binary blob",
  "blob": "U29tZSBiaW5hcnkgYmxvYg==" 
}
```

字段参数:

0. doc_values
0. store

### 布尔(boolean)

布尔字段接受JSONtrue和false值,但也可以接受解释为true或false的字符串:

0. 虚值: false,"false",""(空字符串)
0. 真值: true, "true"

```json
// PUT my-index-000001
{
  "mappings": {
    "properties": {
      "is_published": {
        "type": "boolean"
      }
    }
  }
}

// POST my-index-000001/_doc/1
{
  "is_published": "true" 
}

// GET my-index-000001/_search
{
  "query": {
    "term": {
      "is_published": true 
    }
  }
}
```

字段参数:

0. boost
0. doc_values
0. index
0. null_value
0. store
0. meta


### 关键字族(Keywords)

关键字族包括以下字段类型:

0. keyword,用于结构化内容,例如ID,电子邮件地址,主机名,状态代码,邮政编码或标签.
0. constant_keyword 对于始终包含相同值的关键字字段.
0. wildcard,可针对类似grep的通配符查询优化日志行和类似的关键字值.

关键字字段通常用于排序,汇总和术语级查询,例如term.

避免将关键字字段用于全文搜索.<br>
改用text 字段类型.

```json
// PUT my-index-000001
{
  "mappings": {
    "properties": {
      "tags": {
        "type":  "keyword"
      }
    }
  }
}
```

#### 常量关键字字段的参数


#### 通配符字段类型


### 数值族(Numbers)

支持以下数字类型:

数字类型|说明
-|-
long|有符号的64位整数,最小值为,最大值为: -2<sup>63</sup>2<sup>63<sup>-1
integer|带符号的32位整数,最小值为,最大值为: -2<sup>31</sup>2<sup>31</sup>-1
short|有符号的16位整数,最小值为-32768,最大值为32767
byte|带符号的8位整数,最小值为-128,最大值为127
double|双精度64位IEEE 754浮点数,限制为有限值
float|单精度32位IEEE 754浮点数,限制为有限值
half_float|半精度16位IEEE 754浮点数,限制为有限值
scaled_float|以a为后缀的浮点数long,以固定double比例因子缩放
unsigned_long|一个无符号的64位整数,最小值为0,最大值为: 2<sup>64</sup>-1

```json
// PUT my-index-000001
{
  "mappings": {
    "properties": {
      "number_of_bytes": {
        "type": "integer"
      },
      "time_in_seconds": {
        "type": "float"
      },
      "price": {
        "type": "scaled_float",
        "scaling_factor": 100
      }
    }
  }
}
```

### 日期族(Dates)

#### 毫秒型(date)

JSON没有日期数据类型,因此Elasticsearch中的日期可以是:

0. 包含格式化日期的字符串,例如"2015-01-01"或"2015/01/01 12:10:30"
0. 从纪元以来 代表毫秒的长整数
0. 表示从秒开始的秒数的整数

在内部,日期会转换为UTC(如果指定了时区),并存储为一个整数,表示从纪元以来的毫秒数.

日期查询在内部转换为这种长表示形式的范围查询,并且聚合和存储字段的结果将转换为字符串,具体取决于与该字段关联的日期格式.

日期格式可以自定义,但是如果未format指定,则使用默认格式:

```
strict_date_optional_time || epoch_millis
```

这意味着它将接受带有可选时间戳记的日期,该时间戳记与时间戳记strict_date_optional_time 或毫秒级以来所支持的格式一致.

```json
// PUT my-index-000001
{
  "mappings": {
    "properties": {
      "date": {
        "type": "date" 
      }
    }
  }
}

// PUT my-index-000001/_doc/1
{ "date": "2015-01-01" } 

// PUT my-index-000001/_doc/2
{ "date": "2015-01-01T12:10:30Z" } 

// PUT my-index-000001/_doc/3
{ "date": 1420070400001 } 

// GET my-index-000001/_search
{
  "sort": { "date": "asc"} 
}

// ===

// PUT my-index-000001
{
  "mappings": {
    "properties": {
      "date": {
        "type":   "date",
        "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
      }
    }
  }
}
```

#### 纳秒型(date_nanos)

该数据类型是数据类型的补充date.<br>
但是,两者之间有重要区别.<br>
现有date数据类型以毫秒为单位存储日期.<br>
该date_nanos数据类型存储在纳秒的分辨率,这限制了它的范围内的日期从大约1970至2262年,为日期仍然作为从epoch长表示纳秒存储日期.

纳秒级的查询在内部转换为对此长表示形式的范围查询,并且聚合和存储的字段的结果将转换为字符串,具体取决于与该字段关联的日期格式.

日期格式可以自定义,但是如果未format指定,则使用默认格式:

```
strict_date_optional_time || epoch_millis
```

这意味着它将接受带有可选时间戳记的日期,这些时间戳记通过自strict_date_optional_time包含时间戳记以来最多包含9秒分数或毫秒数(从而使纳秒部分失去精度)来支持所支持的格式 .<br>
使用strict_date_optional_time最多可格式化结果三分之一小数.<br>
要打印和解析最多九位数的分辨率,请使用strict_date_optional_time_nanos.

```json
// PUT my-index-000001?include_type_name=true
{
  "mappings": {
    "_doc": {
      "properties": {
        "date": {
          "type": "date_nanos" 
        }
      }
    }
  }
}

// PUT my-index-000001/_doc/1
{ "date": "2015-01-01" } 

// PUT my-index-000001/_doc/2
{ "date": "2015-01-01T12:10:30.123456789Z" } 

// PUT my-index-000001/_doc/3
{ "date": 1420070400 } 

// GET my-index-000001/_search
{
  "sort": { "date": "asc"} 
}

// GET my-index-000001/_search
{
  "script_fields" : {
    "my_field" : {
      "script" : {
        "lang" : "painless",
        "source" : "doc['date'].value.nano" 
      }
    }
  }
}

// GET my-index-000001/_search
{
  "docvalue_fields" : [
    {
      "field" : "date",
      "format": "strict_date_time" 
    }
  ]
}
```

### 别名(alias)

一个alias映射为索引中的一个字段定义的替代名称.<br>
别名可以代替搜索请求中的目标字段,也可以使用其他其他API(例如field features)使用.

```json
// PUT trips
{
  "mappings": {
    "properties": {
      "distance": {
        "type": "long"
      },
      "route_length_miles": {
        "type": "alias",
        "path": "distance" 
      },
      "transit_mode": {
        "type": "keyword"
      }
    }
  }
}

// GET _search
{
  "query": {
    "range" : {
      "route_length_miles" : {
        "gte" : 39
      }
    }
  }
}
```

### 对象和关系类型

### 结构化数据类型

### 汇总数据类型

### 文字搜寻类型

### 文件等级类型

### 空间数据类型

### 其他种类

### 数组

### 多领域

# 常用功能

## 新建和删除索引(Index)

新建Index,可以直接向Elastic服务器发出PUT请求.<br>
下面的例子是新建一个名叫weather的Index.

```bash
curl -H "Content-Type: application/json" -X PUT 'localhost:9200/weather'
```

服务器返回一个JSON对象,里面的acknowledged字段表示操作成功.

```json
{
  "acknowledged":true,
  "shards_acknowledged":true
}
```

然后,发出DELETE请求,删除这个Index.

```bash
curl -X DELETE 'localhost:9200/weather'
```

## 中文分词设置流程

### 安装分词插件

首先,安装中文分词插件.<br>
这里使用的是ik(7.10.1版本),也可以考虑其他插件(比如smartcn).

```bash
# elasticsearch根目录下
./bin/elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.10.1/elasticsearch-analysis-ik-7.10.1.zip
```

然后重启服务/容器.

### 新建索引(Index)

新建一个Index,指定需要分词的字段.<br>
这一步根据数据结构而异,下面的命令只针对本文.<br>
基本上,凡是需要搜索的中文字段,都要单独设置一下.

数据结构如下:

```json
{
  "user": "张三",
  "title": "工程师",
  "desc": "数据库管理"
}
```

```bash
# 创建索引
curl -X PUT 'localhost:9200/accounts'

curl -H "Content-Type: application/json" -X PUT 'localhost:9200/accounts/_mapping' -d '
{
  "properties": {
    "user": {
      "type": "text",
      "analyzer": "ik_max_word",
      "search_analyzer": "ik_max_word"
    },
    "title": {
      "type": "text",
      "analyzer": "ik_max_word",
      "search_analyzer": "ik_max_word"
    },
    "desc": {
      "type": "text",
      "analyzer": "ik_max_word",
      "search_analyzer": "ik_max_word"
    }
  } 
}'
```

properties有三个字段:

0. user
0. title
0. desc

这三个字段都是中文,而且类型都是文本(text),所以需要指定中文分词器,不能使用默认的英文分词器.

Elastic的分词器称为analyzer.<br>
对每个字段指定分词器.

```json
// ik_max_word分词器是插件ik提供的,可以对文本进行最大数量的分词
"user": {
  "type": "text",
  "analyzer": "ik_max_word", // 字段文本的分词器
  "search_analyzer": "ik_max_word" // 搜索词的分词器
}
```


## 新增记录

向指定的 /Index/_create 发送POST请求,就可以在Index里面新增一条记录.

```bash
curl -H "Content-Type: application/json" -X POST 'localhost:9200/accounts/_create/1' -d '
{
  "user": "张三",
  "title": "工程师",
  "desc": "数据库管理"
}'
```

服务器返回的JSON对象,会给出Index、Type、Id、Version等信息.

```json
{
  "_index":"accounts",
  "_type":"_doc",
  "_id":"1",
  "_version":1,
  "result":"created",
  "_shards":{"total":2,"successful":1,"failed":0},
  "_seq_no":0,
  "_primary_term":1
}
```

如果仔细看,会发现请求路径是/accounts/_create/1,最后的1是该条记录的Id.<br>
它不一定是数字,任意字符串(比如abc)都可以.

新增记录的时候,也可以不指定Id,这时要改成POST请求.

```bash
curl -H "Content-Type: application/json" -X POST 'localhost:9200/accounts/_doc' -d '
{
  "user": "李四",
  "title": "工程师",
  "desc": "系统管理"
}'
```

上面代码中,向/accounts/_doc发出一个POST请求,添加一个记录.<br>
这时,服务器返回的JSON对象里面,_id字段就是一个随机字符串.

```json
{
  "_index":"accounts",
  "_type":"_doc",
  "_id":"Y5l__HcBbWyJOVwbSYv_",
  "_version":1,
  "result":"created",
  "_shards":{"total":2,"successful":1,"failed":0},
  "_seq_no":1,
  "_primary_term":1
}
```

## 根据id查看记录

向/Index/Type/Id发出GET请求,就可以查看这条记录.

```bash
curl 'localhost:9200/accounts/_doc/1?pretty=true'
```

上面代码请求查看/accounts/_doc/1这条记录,URL的参数pretty=true表示以易读的格式返回.

返回的数据中,found字段表示查询成功,_source字段返回原始记录.

```json
{
  "_index" : "accounts",
  "_type" : "_doc",
  "_id" : "1",
  "_version" : 1,
  "_seq_no" : 0,
  "_primary_term" : 1,
  "found" : true,
  "_source" : {
    "user" : "张三",
    "title" : "工程师",
    "desc" : "数据库管理"
  }
}
```

如果Id不正确,就查不到数据,found字段就是false.

```bash
curl 'localhost:9200/accounts/_doc/12?pretty=true'
```

```json
{
  "_index" : "accounts",
  "_type" : "_doc",
  "_id" : "12",
  "found" : false
}
```

## 删除记录

删除记录就是发出DELETE请求.

```bash
curl -X DELETE 'localhost:9200/accounts/_doc/1'
```

## 更新记录

更新记录就是使用PUT请求,重新发送一次数据.

```bash
curl -H "Content-Type: application/json" -X PUT 'localhost:9200/accounts/_doc/1' -d '
{
  "user" : "张三",
  "title" : "工程师",
  "desc" : "数据库管理,软件开发"
}' 
```

```json
{
  "_index":"accounts",
  "_type":"_doc",
  "_id":"1",
  "_version":2,
  "result":"updated",
  "_shards":{"total":2,"successful":1,"failed":0},
  "_seq_no":4,
  "_primary_term":1
}
```

上面代码中,将原始数据从"数据库管理"改成"数据库管理,软件开发".<br>
返回结果里面,有几个字段发生了变化.

```json
"_version" : 2,
"result" : "updated"
```

可以看到,记录的Id没变,但是版本(version)从1变成2,<br>
操作类型(result)从created变成updated.

## 返回所有记录

使用GET方法,直接请求/Index/Type/_search,就会返回所有记录.

```bash
curl 'localhost:9200/accounts/_doc/_search?pretty=true'
```

```json
{
  "took" : 2, // 该操作的耗时(单位为毫秒)
  "timed_out" : false, // 是否超时
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : { // 命中的记录
    "total" : {
      "value" : 2, // 返回记录数
      "relation" : "eq"
    },
    "max_score" : 1.0, // 最高的匹配程度
    "hits" : [ // 返回的记录组成的数组
      {
        "_index" : "accounts",
        "_type" : "_doc",
        "_id" : "Y5l__HcBbWyJOVwbSYv_",
        "_score" : 1.0,
        "_source" : {
          "user" : "李四",
          "title" : "工程师",
          "desc" : "系统管理"
        }
      },
      {
        "_index" : "accounts",
        "_type" : "_doc",
        "_id" : "1",
        "_score" : 1.0,
        "_source" : {
          "user" : "张三",
          "title" : "工程师",
          "desc" : "数据库管理,软件开发"
        }
      }
    ]
  }
}
```

## 全文搜索

```bash
curl -H "Content-Type: application/json" 'localhost:9200/accounts/_doc/_search?pretty=true'  -d '
{
  "query" : { "match" : { "desc" : "软件" }}
}'
```

上面代码使用Match查询,指定的匹配条件是desc字段里面包含"软件"这个词.<br>
返回结果如下.

```json
{
  "took" : 4,
  "timed_out" : false,
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 1,
      "relation" : "eq"
    },
    "max_score" : 0.6235748,
    "hits" : [
      {
        "_index" : "accounts",
        "_type" : "_doc",
        "_id" : "1",
        "_score" : 0.6235748,
        "_source" : {
          "user" : "张三",
          "title" : "工程师",
          "desc" : "数据库管理,软件开发"
        }
      }
    ]
  }
}
```

Elastic默认一次返回10条结果,可以通过size字段改变这个设置.

```bash
curl 'localhost:9200/accounts/_doc/_search'  -d '
{
  "query" : { "match" : { "desc" : "管理" }},
  "size": 1
}'
```

上面代码指定,每次只返回一条结果.

还可以通过from字段,指定位移.

```bash
curl -H "Content-Type: application/json" 'localhost:9200/accounts/_doc/_search?pretty=true'  -d '
{
  "query" : { "match" : { "desc" : "管理" }},
  "from": 1,
  "size": 1
}'
```

上面代码指定,从位置1开始(默认是从位置0开始),只返回一条结果.

## 逻辑运算

如果有多个搜索关键字,Elastic认为它们是or关系.

```bash
curl -H "Content-Type: application/json" 'localhost:9200/accounts/_doc/_search?pretty=true'  -d '
{
  "query" : { "match" : { "desc" : "软件 系统" }}
}'
```

上面代码搜索的是软件or系统.

如果要执行多个关键词的and搜索,必须使用布尔查询.

```bash
curl -H "Content-Type: application/json" 'localhost:9200/accounts/_doc/_search?pretty=true'  -d '
{
  "query": {
    "bool": {
      "must": [
        { "match": { "desc": "软件" }},
        { "match": { "desc": "系统" }}
      ]
    }
  }
}'
```

# 运行维护

## 启动

```
bash /data/elasticsearch-5.6.3/bin/elasticsearch -d
```

## 检查是否运行

```
curl 'http://localhost:9200/?pretty'
```

## 查询进程

```
psaux | grep elasticsearch
```

## 使用容器(单实例)

[官方镜像地址](https://hub.docker.com/_/elasticsearch)

### 启动

```bash
docker run -itd \
--name es_space \
-p 9200:9200 \
-p 9300:9300 \
-e "discovery.type=single-node" \
elasticsearch:7.10.1
```

### 进入容器

```bash
docker exec -i -t es_space /bin/bash
```

## 项目根目录

### config
#### elasticsearch.yml

##### 配置跨域

```yml
# config/elasticsearch.yml
http.cors.enabled: true
http.cors.allow-origin: "*"
```

然后重启服务即可.

# 常见问题

## Could not register mbeans

> main ERROR Could not register mbeans java.security.AccessControlException: access denied ("javax.management.MBeanTrustPermission" "register")

检查文件夹是否为当前为root用户所用.

切换命令:

```bash
chown [-R] 账号名称:用户组名称文件或目录
```

## vm.max_map_count [65530] is too low

> max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]

**如果是容器需要在启动时指定为特权容器(可修改环境变量):--privileged=true**<sup>1</sup>

```bash
echo '修改文件描述符数量'
sudo tee -a /etc/sysctl.conf <<-'EOF'
vm.max_map_count=262144
EOF

sudo sysctl -p
```

1: 关于容器内修改

# 参考资料 

> [Elasticsearch|权威指南](https://www.elastic.co/guide/cn/elasticsearch/guide/cn/index.html)

> [Github|polyfractal](https://github.com/polyfractal)

> [CSDN|elasticsearch安装过程中可遇到的问题](https://blog.csdn.net/zhang89xiao/article/details/68925294)

> [阮一峰的网络日志|全文搜索引擎Elasticsearch入门教程](https://www.ruanyifeng.com/blog/2017/08/elasticsearch.html)

> [elastic | Field data type](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-types.html)