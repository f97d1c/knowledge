#!/usr/bin/env ruby

requires = {
  # '终端工具' => 'irb',
  "断点调试" => "pry",
  "Http请求" => "rest-client",
  "JSON数据处理" => "json",
  "命令行选项分析" => "optparse",
# 'JSON转Ruby对象' => 'ostruct',
# '格式化打印JSON数据' => 'awesome_print',
}

requires.each do |desc, gem_name|
  begin
    require gem_name
  rescue LoadError => e
    puts "缺少Gem: #{gem_name}(用于#{desc}), 请先手动安装该Gem."
  end
end

Signal.trap("INT") {
  print "\e[2J\e[f"
  exit
}

def judge_doc_present(es_search_url:, doc_id:)
  request_path = es_search_url
  body = { "query": { "ids": { "type": "_doc", "values": [doc_id] } } }
  headers = { 'Content-Type': "application/json" }
  # print "请求ES: #{body}\n"
  res = RestClient::Request.execute(method: "post", headers: headers, url: request_path, payload: body.to_json)
  # print "返回结果: #{res.body}\n"
  return false unless (res.code == 200)
  return false unless JSON.parse(res.body)["hits"]["hits"].length > 0
  true
end

# 普通字符串md5sum和hexdigest无区别 但是关于json格式数据,shell字符串不支持,所以bash和ruby的加密值也不同
def md5sum(str:)
  md5 = `str=#{str};md5=$(echo -n "$str" |md5sum);echo -n $md5`
  md5[0...32]
end

def get_breakpoint_index(array_file:, es_search_url:)
  judge_doc_present_ = lambda { |md5| judge_doc_present(es_search_url: es_search_url, doc_id: md5) }
  left_index = 0
  right_index = array_file.size - 1
  next_index = 0
  search_count = 0

  while (left_index <= right_index)
    search_count += 1
    mind = (left_index + right_index) / 2
    md5 = md5sum(str: array_file[mind].gsub("\n", ""))
    doc_flag = judge_doc_present_.call(md5)
    next_index = 0

    if (doc_flag && !judge_doc_present_.call(md5sum(str: array_file[mind + 1].gsub("\n", ""))))
      print "#{search_count}. 正中目标, 当前中位数: #{mind}\n"
      next_index = mind + 1
      right_index = -1
    elsif (doc_flag)
      print "#{search_count}. 向后搜索, 当前中位数: #{mind}\n"
      left_index = mind + 1
    elsif (!doc_flag)
      print "#{search_count}. 向前搜索, 当前中位数: #{mind}\n"
      right_index = mind - 1
    end
  end
  next_index
end

options = {
  file_path: {
    desc: "所读取文件(绝对)路径",
    type: "字符串",
    default: nil,
    lam: lambda { |value| return [false, "文件:#{value}不存在"] unless File::file?(value); return [true, value] },
  },
  breakpoint: {
    desc: "是否启用断点续读, 只针对ES文档不存在部分进行创建, 已存在内容不会更新",
    type: "布尔值",
    default: true,
    lam: lambda { |value| [true, ["true", "yes", "Yes", "Y", "y"].include?(value)] },
  },
  es_host: { desc: "ES索引地址", type: "字符串", default: "http://localhost:9200" },
  es_index_name: { desc: "ES索引名称", type: "字符串", default: nil },
}

OptionParser.new do |opts|
  opts.banner = "该脚本用于将文件内容逐行创建为ES文档."
  options.each do |key, _value|
    desc = "#{_value[:desc]}, #{_value[:default] ? "默认值为: #{_value[:default]}" : "必填."}"
    opts.on("--#{key} #{_value[:type]}", desc) do |value|
      if (_value[:lam])
        res = _value[:lam].call(value)
        if res[0]
          options[key][:current] = res[1]
        else
          print "#{res[1]}.\n"
          exit 401
        end
      else
        options[key][:current] = value
      end
    end
  end
end.parse!

options.map { |key, value| value[:current] ||= value[:default] }

options.each do |key, value|
  unless value[:current]
    print "--#{key} #{value[:desc]}, 不能为空.\n"
    exit 401
  end
end

start_index = 0
@array_file = File.readlines(options[:file_path][:current])
@es_host = options[:es_host][:current]
@es_index_name = options[:es_index_name][:current]
@es_search_url = "#{@es_host}/#{@es_index_name}/_search"
@es_create_url = "#{@es_host}/#{@es_index_name}"
if (options[:breakpoint][:current])
  start_index = get_breakpoint_index(array_file: @array_file, es_search_url: @es_search_url)
end

print "从#{start_index}开始创建索引.\n"
@current_index = start_index-1
@array_file[start_index..-1].each do |content|
  @current_index+=1
  next unless content
  json_str = content.gsub("\n", "")
  md5 = md5sum(str: json_str)
  headers = { 'Content-Type': "application/json" }
  res = RestClient::Request.execute(method: "post", headers: headers, url: (@es_create_url + "/#{md5}"), payload: json_str)
  res = JSON.parse(res.body)
  print "#{@current_index}: #{res['result']}, #{json_str}\n"
end

# ruby 创建文档.rb --file_path '/home/ff4c00/Space/知识体系/应用科学/计算机科学/理论计算机科学/编程语言理论/Ruby/Gems/Nokogiri/Scout/outputs/standard_format_data.txt' --es_index_name 'link_articles/_doc'

# ruby 创建文档.rb --file_path '/home/ff4c00/Space/知识体系/人文科学/经济学/金融/微观金融学/金融体系/金融工具/基金/数据研究/基础数据/377240.txt' --es_index_name 'daily_market_funds/_doc'