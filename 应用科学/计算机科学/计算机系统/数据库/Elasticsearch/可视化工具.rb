#!/usr/bin/env ruby

requires = {
  '终端工具' => 'irb',
  '断点调试' => 'pry',
  'Http请求' => 'rest-client',
  'JSON数据处理' => 'json',
  # '命令行选项分析' => 'optparse',
  'JSON转Ruby对象' => 'ostruct',
  '格式化打印JSON数据' => 'awesome_print',
}

requires.each do |desc, gem_name|
  begin 
    require gem_name
  rescue LoadError => e
    puts "缺少Gem: #{gem_name}(用于#{desc}), 请先手动安装该Gem."
  end
end

class Array
  def to_table
    column_sizes = self.reduce([]) do |lengths, row|
      row.each_with_index.map{|iterand, index| [lengths[index] || 0, iterand.to_s.length].max}
    end
    puts head = '-' * (column_sizes.inject(&:+) + (3 * column_sizes.count) + 1)
    self.each do |row|
      row = row.fill(nil, row.size..(column_sizes.size - 1))
      row = row.each_with_index.map{|v, i| v = v.to_s + ' ' * (column_sizes[i] - v.to_s.length)}
      puts '| ' + row.join(' | ') + ' |'
    end
    puts head
  end
end

def print_info(array)
  printf "\e[2J\e[H\e[m\n"
  array.to_table 
end

def handle_response(response:)
  # result = JSON.parse(response, object_class: OpenStruct) # 多层转换
  response = JSON.parse(response)
  # result = OpenStruct.new(response)

  details = []
  objects = []
  details << ['响应时长', "#{response['took']} (ms)"]
  details << ['是否超时', response['timed_out']]

  response['hits']['hits'].each do |item|
    objects << {'id' => item['_id']}.merge!(item['_source'])
  end

  [true, {details: details, objects: objects}]
end

def requestES(host: 'localhost', port: 9200, headers: {'Content-Type': 'application/json'}, method: 'GET', route: 'unite_structure/_doc/_search', params: nil)
  # request_path="#{host}:#{port}/#{route}?pretty=true"
  request_path="#{host}:#{port}/#{route}"
  details = [
    ['开始时间', Time.now],
    ['请求方式', method],
    ['完整路径', request_path],
    ['请求参数', params],
  ]

  begin
    res = RestClient::Request.execute(method: method, url: request_path, headers: headers, payload: params)
    details << ['响应状态', res.code]
    res = handle_response(response: res.body)
    unless res[0]
      details << ['解析异常', res[1]]
      print_info(details)
      return
    end

    details += res[1][:details]
    @objects = res[1][:objects]
    details << ['返回结果', '@objects'] if @objects
    
   rescue => e
    details << ['异常说明', $!]
    $@[0..10].each do|string|
      error_info = (string.split('/')[-3..-1] || [string]).join('/')
      details << ['异常详情', error_info]
    end
   ensure
    details.insert(1, ['结束时间', Time.now])
    print_info(details)
   end

   ap(@objects) if @objects
end

IRB.start