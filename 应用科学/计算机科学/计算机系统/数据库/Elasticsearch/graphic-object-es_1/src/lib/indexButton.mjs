export default class IndexButton {
  constructor(params = {}) {
    let res = Graphic.toElement({ element: 'div', id: 'space-index-button' })
    if (!res[0]) return res
    params.父节点.appendChild(res[1])
    this.node = res[1]
    return [true, this]
  }

  init(params = {}) {
    let buttons = { 选择索引: [] }

    for (let [key, value] of Object.entries(params.elasticsearch)) {
      let button = {
        element: 'button',
        type: 'button',
        id: 'space-index-button-' + key,
        value: key,
        class: 'btn btn-primary ms-2 me-2 mt-1',
        innerHtml: value.其他.索引描述,
        onclick: 'GraphicES.index.change({unineCode: this.value});',
      }
      // if (params.unineCode == key) button.selected = true
      buttons.选择索引.push(button)
    }

    return Graphic.bootstrap5.baseTable({ 渲染对象: buttons, 父元素: this.node, 元素ID: 'space-index-button-table', 名称单元格占比: 10 })
  }

  beforeLoad(params){
    this.unineCode ||= params.unineCode
    return this.changeButton(params, 'before')
  }
  afterLoad(params) {
    this.unineCode = params.unineCode
    return this.changeButton(params, 'after')
  }

  changeButton(params, mode){
    if (!!!this.unineCode) return [false, '当前索引为空']
    let button = this.getButton(this.unineCode)
    if (!!!button) return [false, '当前索引按钮为空']

    // let classes = ['btn-primary', 'btn-danger']
    let classes = []
    switch (mode){
      case 'before':
        classes.reverse()
        button.classList.remove('disabled')
        if (params.unineCode) this.getButton(params.unineCode).classList.add('disabled')
        break
      case 'after':
        break
    }

    button.classList.remove(classes[0])
    button.classList.add(classes[1])
    
    return [true, '']
  }

  getButton(unineCode){
    return document.getElementById('space-index-button-' + unineCode) || document.createElement('button')
  }
}