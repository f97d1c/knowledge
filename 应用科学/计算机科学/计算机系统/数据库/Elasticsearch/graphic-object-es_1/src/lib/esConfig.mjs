export default class EsConfig {
  constructor(params = {}) {
    let res = Graphic.toElement({ element: 'div', id: 'space-es-config' })
    if (!res[0]) return res
    params.父节点.appendChild(res[1])
    this.node = res[1]
    return [true, this]
  }

  init(params = {}){
    if (!!!params.configRequest) return [false, 'ES请求参数为空']
    if (Object.keys(params.elasticsearch || {}).length == 0) return [false, 'ES可用索引为空']

    let configRequest = {}
    for (let [key, value] of Object.entries(params.configRequest)) {
      configRequest[key] = { element: 'input', type: 'text', class: 'form-control', value: value, name: key }
    }

    let options = [{ element: 'option', value: '', innerHtml: '选择当前索引' }]
    for (let [key, value] of Object.entries(params.elasticsearch)) {
      let option = {
        element: 'option',
        value: key,
        innerHtml: value.其他.索引描述 + ': ' + value.索引全称,
      }
      if (params.unineCode == key) option.selected = true
      options.push(option)
    }

    configRequest['当前索引'] = {
      element: 'select',
      class: 'form-control',
      name: '当前索引',
      innerHtml: options,
      onchange: 'GraphicES.index.change({unineCode: this.value});',
    }

    configRequest['其他'] = [
      {
        element: 'button',
        type: 'button',
        class: 'btn btn-primary ms-2 me-2 mt-2',
        innerHtml: '导出结果',
        onclick: 'GraphicES.index.assemblyLine.exportExcel();',
      },
      {
        element: 'button',
        type: 'button',
        class: 'btn btn-primary ms-2 me-2 mt-2',
        innerHtml: '查询结果',
        onclick: 'GraphicES.index.change({reload: true});',
      }
    ]

    let res = Graphic.bootstrap5.baseTable({ 渲染对象: configRequest, 父元素: this.node, 元素ID: 'space-es-config-request' })
    if (!res[0]) return res
    
    // TODO: 后期优化 统一调用方法
    if(params.生产模式 && (localStorage.getItem('开发模式') != '1')){
      this.node.style.display = 'none'
      return [false, "当前为生产模式, 参数表单不做展示, 进入开发模式: localStorage.setItem('开发模式', 1)"]
    }
    return [true, this]
  }

  beforeLoad(params){
    this.init(params)
    this.requestParams(params)
    return [true, '']
  }

  afterLoad(params){
    params.configRequest = params.requestParams
    return this.init(params)
  }

  // 请求参数
  requestParams(params) {
    let requestParams = {}
    this.node.querySelectorAll('input, select').forEach(item => {
      if (!!!item.value || !!!item.name) return;
      requestParams[item.name] = item.value
    })
    params.requestParams = requestParams
    return [true, params.requestParams]
  }

}