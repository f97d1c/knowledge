export default class Aggs {
  constructor(params = {}) {
    let res = Graphic.toElement({ element: 'div', id: 'space-aggs' })
    if (!res[0]) return res
    params.父节点.appendChild(res[1])
    this.element = res[1]
    return [true, this]
  }

  currentResponse(params) {
    return params.dataStore.indexResponse[params.unineCode]
  }

  afterLoad(params) {
    if (!!!this.currentResponse(params)) return [false, '当前索引的原始返回数据为空']
    let aggs = this.currentResponse(params).result.aggs
    if (!!!aggs) return [false, '当前索引的数据聚合信息为空']
    for (let [key, value] of Object.entries(aggs)) {
      let res = Graphic.bootstrap5.listTable({ 元素ID: this.element.id + '-' + params.unineCode + '-' + key, 渲染对象: value, 父元素: this.element, 标题: key })
      if (!res[0]) {
        res[1] += '  value: '+JSON.stringify(value)
        return res
      }
    }
    return [true, '']
  }

  beforeExportExcel(params) {
    let currentResponse = params.dataStore.indexResponse[params.unineCode]
    if (!!!currentResponse) return [false, '当前索引返回结果为空']
    if (!!!currentResponse.result.aggs) return [false, '当前索引的数据聚合信息为空']
    Object.assign(params.dataStore['exportExcel'], currentResponse.result.aggs)
  }

}