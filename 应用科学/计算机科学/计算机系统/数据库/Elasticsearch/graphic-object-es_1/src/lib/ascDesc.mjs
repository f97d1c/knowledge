export default class AscDesc {
  constructor(params = {}) {
    let res = Graphic.toElement({ element: 'div', id: 'space-asc_desc' })
    if (!res[0]) return res
    params.父节点.appendChild(res[1])
    this.element = res[1]
    return [true, this]
  }

  currentResponse(params) {
    return params.dataStore.indexResponse[params.unineCode]
  }

  currentId(params) {
    return ('space-asc_desc-table-' + params.unineCode)
  }

  currentNode(params) {
    return document.getElementById(this.currentId(params))
  }

  beforeLoad(params) {
    if (!!this.currentNode(params)) {
      let orderColumn = document.getElementById(this.currentId(params) + '-orderColumn').value
      if (!!!orderColumn) return [false, '排序字段为空']

      let orderType = document.getElementById(this.currentId(params) + '-orderType').value
      if (!!!orderType) return [false, '排序方式为空']
      params.currentIndex.排序方式 = {}
      params.currentIndex.排序方式[orderColumn] = { "order": orderType }
    } else {
      params.排序方式 = params.currentIndex.排序方式
    }
    return [true, '']
  }

  afterLoad(params) {
    if (!!!params.currentIndex.索引映射) return [false, '当前索引的字段映射为空']
    let orderColumn = Object.keys(params.currentIndex.排序方式 || {})[0]
    let orderType = ((params.currentIndex.排序方式 || {})[orderColumn] || {}).order

    if (!!this.currentNode(params)) {
      if (!!orderColumn) {
        document.getElementById(this.currentId(params) + '-orderColumn').value = orderColumn
      }
      if (!!orderType) {
        document.getElementById(this.currentId(params) + '-orderType').value = orderType
      }

      document.getElementById(this.currentId(params)).style.display = ''
      return [true, '']
    }
    // TODO: 根据现有的排序方法设定默认值
    let table = {
      排序方式: {
        element: 'select',
        class: 'form-control',
        name: '排序方式',
        id: this.currentId(params) + '-orderType',
        innerHtml: [
          // { element: 'option', value: '', innerHtml: '选择排序方式' },
        ],
        onchange: 'GraphicES.index.change({reload: true});',
      },
      排序字段: {
        element: 'select',
        class: 'form-control',
        name: '排序字段',
        id: this.currentId(params) + '-orderColumn',
        innerHtml: [
          { element: 'option', value: '', innerHtml: '选择排序字段' },
        ],
        onchange: 'GraphicES.index.change({reload: true});',
      }
    }

    for (let [key, value] of Object.entries({desc: '倒序[由大到小]', asc: '正序[由小到大]'})) {
      let option = {
        element: 'option',
        value: key,
        innerHtml: value,
      }
      if (!!orderType) {
        if (orderType == key) option.selected = true;
      }
      table['排序方式'].innerHtml.push(option)
    }

    let array = Object.entries(params.currentIndex.索引映射).map(array =>{ 
      let tmp = [array[0], array[1].zh]
      if (['byte','short','integer','long','float','double','boolean','date'].includes(array[1].type)){
        tmp.push('推荐')
      }
      return tmp
    })

    let arrayItemList = new Graphic.ArrayItemList(array)

    let res = arrayItemList.polishStr()
    if (!res[0]) return res
    let index = 0
    for (let [key, value] of Object.entries(params.currentIndex.索引映射)) {
      let option = {
        element: 'option',
        value: key,
        // innerHtml: key,
        innerHtml: res[1][index],
      }
      index+=1

      if (!!orderColumn) {
        if (orderColumn == key) option.selected = true;
      }
      table['排序字段'].innerHtml.push(option)
    }

    return Graphic.bootstrap5.baseTable({ 渲染对象: table, 父元素: this.element, 元素ID: this.currentId(params) })
  }

}