let metrics = {
  "平均|单位净值": {
    "avg": { "field": "单位净值" }
  },
  "平均|累计净值": {
    "avg": { "field": "累计净值" }
  },
  "平均|日增长率": {
    "avg": { "field": "日增长率" }
  },
  "最低|累计净值": {
    "min": { "field": "累计净值" }
  },
  "最高|累计净值": {
    "max": { "field": "累计净值" }
  }
}

let esConfig = {
  配置: {
    请求: {
      主机: 'http://es_server',
    }
  },
  indexs: {
    fundInfos: {
      索引名称: 'fund_infos',
      其他: {
        是否暴露: true,
        索引描述: '基金基本信息',
      },
      聚合条件: {
        "根据每份累计分红金额划分":{
          "terms": {
            "field": "每份累计分红(元)"
          }
        },
        "根据基金类型划分":{
          "terms": {
            "field": "基金类型"
          }
        },
        "根据分红次数划分":{
          "terms": {
            "field": "分红次数"
          }
        },
        "根据基金经理人划分":{
          "terms": {
            "field": "基金经理人"
          }
        },

      }
    },
    dailyMarketFund: {
      索引名称: 'daily_market_funds',
      其他: {
        是否暴露: true,
        索引描述: '基金每日数据',
      },
      排序方式: { "净值日期": { "order": "asc" } },
      聚合条件: {
        "基金概况": {
          "terms": {
            "field": "基金简称"
          },
          "aggs": metrics
        },
        "基金年度概况": {
          "terms": {
            "field": "基金简称"
          },
          "aggs": {
            "nextLevel": {
              "date_histogram": {
                "field": "净值日期",
                "interval": "year",
                "min_doc_count": 1,
                "format": "yyyy年"
              },
              "aggs": metrics
            }
          }
        }
      },
      聚合图表: {
        "基金年度概况\\|*": function(params){
          let metrics = Object.keys(params.data[0]).filter(str => { return str.match(/.*\|.*/)})
          let dataByMetrics = {}
          metrics.forEach(key => {
            let tmp = {}
            dataByMetrics[key] = tmp
            params.data.forEach(item=>{
              tmp[item.名称] = item[key]
            })
          })
    
          Graphic.chartJs2.broken_line({
            元素ID: params.父节点.id + '-' + params.aggsName + '-' + 'broken_line',
            父元素: params.父节点,
            渲染对象: dataByMetrics,
            标题: params.aggsName
          })
        }
      }
    },
    linkArticles: {
      索引名称: 'link_articles',
      其他: {
        是否暴露: true,
        索引描述: '链接/文章',
      },
      聚合条件: {
        "根据标签分桶": {
          terms: {
            field: "标签",
            size: 10000
          }
        }
      },
      聚合图表: {
        根据标签分桶: function (params) {
          let formatedData = {}
          params.data.forEach(item => {
            formatedData[item.名称] = item.数量
          })
    
          Graphic.chartJs2.doughnut({
            元素ID: params.父节点.id + '-' + params.aggsName + '-' + 'doughnut',
            父元素: params.父节点,
            渲染对象: formatedData,
            标题: params.currentIndex.其他.索引描述 + ' | ' + params.aggsName
          })
    
          // Graphic.amcharts4.verticalGraph({
          //   元素ID: params.父节点.id + '-' + params.aggsName + '-' + 'verticalGraph',
          //   父元素: params.父节点,
          //   渲染对象: formatedData,
          //   标题: params.currentIndex.其他.索引描述 + ' | ' + params.aggsName
          // })
        },
      }
    },
  }
}

export default esConfig;