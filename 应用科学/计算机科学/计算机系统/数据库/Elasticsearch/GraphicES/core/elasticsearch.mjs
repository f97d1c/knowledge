import baseConfig from "../config/base.mjs"

export default class Elasticsearch {
  setAttributes(object, attributes) {
    for (let [key, value] of Object.entries(attributes || {})) {
      if (
        typeof value == 'object'
        && !Array.isArray(value)
        && (value.__proto__.toString() != "[object HTMLDivElement]")
      ) {
        this.setAttributes((object[key] ||= {}), value)
      } else {
        object[key] = value
      }
    }
  }

  constructor(params = {}) {
    let self = {}

    if (!!params.indexs) {
      for (let [key, value] of Object.entries(params.indexs)) {
        self[key] = new Elasticsearch(value)
      }
    } else {
      let defaultValue = {
        索引名称: undefined,
        索引全称: undefined,
        索引映射: undefined,
        聚合条件: undefined,
        聚合图表: {},
        排序方式: undefined,
        其他: {
          是否暴露: false,
          索引类型: '_doc',
          索引描述: '',
        },
        配置: {
          请求: Object.assign({}, baseConfig.elasticsearch.配置.请求)
        },
      }
      this.setAttributes(defaultValue, params)
      if (!!defaultValue.索引名称) {
        defaultValue.索引全称 ||= defaultValue.索引名称 + '/' + defaultValue.其他.索引类型
        defaultValue.配置.请求.访问地址 ||= defaultValue.索引全称 + '/_search'
      }
      for (let [key, value] of Object.entries(defaultValue)) {
        this[key] = value
      }
    }

    if (!!Object.keys(self).length != 0) {
      return self
    } else {
      this.indexMapping({}).then(result_ => {
        if (!result_[0]) return;
        this.索引映射 = result_[1]
      })
    }
  }

  async requestES(params) {
    let requestParams = Object.assign({}, this.配置.请求)
    params = Object.assign(requestParams, params)

    if (params.访问地址 == 'undefined') {
      return [false, '访问地址不能为空', { params: params }]
    }

    if (typeof params.请求头 == 'string') params.请求头 = JSON.parse(params.请求头)

    let requestOptions = {
      method: params.请求方式,
      headers: params.请求头,
      redirect: params.重定向时,
      mode: params.请求模式,
    };

    if (['POST', 'Post', 'post'].includes(params.请求方式)) {
      params.请求体 ||= {}
      if (typeof params.请求体 == 'string') params.请求体 = JSON.parse(params.请求体)
      requestOptions.body = JSON.stringify(params.请求体)
    }

    let requestPath = params.主机 + ':' + params.端口 + '/' + params.访问地址

    var result
    var _error = undefined
    await fetch(requestPath, requestOptions)
      .then(response => response.text())
      .then(res => {
        result = JSON.parse(res)
      }).catch(error => {
        _error = true
        result = error.toString()
      });
    if (!!_error) return [false, result]
    return [true, result]
  }

  async indexSearch(params) {
    if (['POST', 'Post', 'post'].includes(params.请求方式)) {
      params.请求体 ||= {}
      if (typeof params.请求体 == 'string') params.请求体 = JSON.parse(params.请求体)
      params.请求体.size ||= (params.返回结果数量 || 1000)

      if (typeof params.搜索参数 == 'string') params.搜索参数 = JSON.parse(params.搜索参数)
      if (!!params.搜索参数 && Object.keys(params.搜索参数).length != 0) params.请求体.query = params.搜索参数

      if (!!this.排序方式) params.请求体.sort = this.排序方式
      if (!!this.聚合条件) params.请求体.aggs = this.聚合条件
    }

    let res
    await this.requestES(params).then(result_ => {
      if (!result_[0]) return res = result_
      let result = result_[1]
      if ((result.hits.hits.length == 0)) {
        res = [false, '返回结果为空', { result: result }]
        return
      }
      let sources = []
      result.hits.hits.forEach(element => {
        sources.push(element._source)
      });

      if (!!result.aggregations) {
        let res = this.formatAggs(result.aggregations)
        if (res[0]) result.aggs = res[1]
      }
      res = [true, sources, { result: result }]
    })
    return res
  }

  async indexMapping(params) {
    if (!!!this.索引名称) return [false, '索引名不能为空']
    let res
    await this.requestES({ 请求方式: 'GET', 访问地址: this.索引名称 + '/_mapping' }).then(result_ => {
      if (!result_[0]) return res = result_
      let result = result_[1]

      if (!!!Object.values(result)[0]) return [false, '返回结果中无索引:' + this.索引名称 + '相关内容']
      let mappings = Object.values(result)[0].mappings.properties
      res = [true, mappings]
    })
    return res
  }

  async indexAggs() {
    if (!!!this.索引名称) return [false, '索引名不能为空']

    let defaultValue = {
      size: 10000,
    }
    params = Object.assign(defaultValue, params)
  }

  async getColumnValues(params) {
    if (!!!this.索引名称) return [false, '索引名不能为空']
    if (!!!params.columnName) return [false, '字段名不能为空']

    let defaultValue = {
      size: 10000,
    }
    params = Object.assign(defaultValue, params)

    let requestParams = {
      "size": 0,
      "aggs": {
        "getColumnValues": {
          "terms": {
            "field": params.columnName,
            "size": params.size
          }
        }
      }
    }

    let result
    await this.requestES({ 请求方式: 'POST', 访问地址: this.索引全称 + '/_search', 请求体: requestParams }).then(result_ => {
      result = result_
      if (!result_[0]) {
        return;
      } else if (!!result_[1].error) {
        result = [false, result_[1].error.reason, result_[1]]
        return;
      }
      result.splice(1, 0, result_[1].aggregations.getColumnValues.buckets.map(item => item.key))
    })
    return result;
  }

  formatAggs(aggregations, formatedAggs = {}) {
    let baseBuckets = (array, buckets) => {
      buckets.forEach(item => {
        let clone = Object.assign({}, item)
        if (!!(item.key_as_string && item.key)) {
          delete clone.key
        }

        let str = JSON.stringify(clone)
          .replace(/key_as_string/, '名称')
          .replace(/key/, '名称')
          .replace(/doc_count/, '数量')
          .replace(/{"value":([^}]*)}/g, '$1')
        array.push(JSON.parse(str))
      })
    }
    for (let [aggName, aggInfo] of Object.entries(aggregations)) {
      if (!!aggInfo.buckets[0].nextLevel) {
        aggInfo.buckets.forEach(item => {
          let tmp = {}
          let key = aggName + '|' + item.key
          tmp[key] = item.nextLevel
          this.formatAggs(tmp, formatedAggs)
        })
      } else {
        formatedAggs[aggName] = []
        baseBuckets(formatedAggs[aggName], aggInfo.buckets);
      }
    }
    return [true, formatedAggs]
  }

}