export default class AssemblyLine {
  constructor(params = {}) {
    let defaultValue = {
      links: [],
      nodes: [],
      treasure: {},
      logPools: {},
      currentId: function () { },
    }
    params = Object.assign(defaultValue, params)
    for (let [key, value] of Object.entries(params)) { this[key] = value }
  }

  async validate() {
    await this.createNodesObject()
    await this.createLinks()
    return [true, this]
  }

  async createNodesObject() {
    this.nodesObject = [this.treasure]
    let result = []
    for (let path of this.nodes) {
      await import(path).then((module) => {
        let module_name = path.split('/')[path.split('/').length - 1].split('.')[0]
        let res = new module.default(this.treasure)
        result[module_name] = res
        if (!res[0]) return;
        this[module_name] = res[1]
        this.nodesObject.push(res[1])
      })
    }
    return this.nodesObject
  }

  factoryLink(name) {
    this[name] = async () => {
      let methods = [
        'before' + name[0].toUpperCase() + name.slice(1),
        name,
        'after' + name[0].toUpperCase() + name.slice(1),
      ]
      let logs = this.createLogPool(name)
      for (let method of methods) {
        for (let object of this.nodesObject) {
          if (!((typeof object[method]) == 'function')) continue
          await this.execFunc(object, method, logs)
        }
      }

      if (logs.length > 0) {
        this.consoleLog()
        return [false, logs]
      }
      return [true, '']
    }
  }

  async createLinks() {
    for (let link of this.links) {
      this.factoryLink(link)
    }
  }

  createLogPool(link) {
    let results = []
    this.logPools[link] ||= {}
    this.logPools[link][(new Date().toLocaleString())] = results
    return results
  }

  async execFunc(object, method, logs) {
    let res
    if (object[method].constructor.name == 'AsyncFunction') {
      await object[method](this.treasure).then(result => {
        return res = result
      })
    } else {
      res = object[method](this.treasure)
    }

    res ||= [false, '函数未返回标准结果']

    if (res[0]) {
    } else {
      logs.push({ 是否成功: false, 当前方法: object.constructor.name + '#' + method, 当前ID: (this.currentId()||''), 说明: res[1] })
    }
    return res
  }

  consoleLog() {
    // console.clear()
    let tmp = []

    for (let [method, list] of Object.entries(this.logPools)) {
      for (let [time, logs] of Object.entries(list)) {
        logs.forEach(log => {
          tmp.push(Object.assign({ 时间: time, 触发方法: method }, log))
        })
      }
    }

    console.table(tmp.reverse())
  }

}