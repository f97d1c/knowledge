let baseConfig = {
  elasticsearch: {
    配置: {
      请求: {
        请求方式: 'POST',
        请求头: { 'content-type': 'application/json' },
        重定向时: 'follow',
        请求模式: 'cors',
        主机: 'http://ruby_server_yggc',
        端口: '9200',
        访问地址: undefined,
        返回结果数量: 5000,
        是否打印详情: false,
        搜索参数: {},
        请求体: {},
      }
    }
  }
}

export default baseConfig;