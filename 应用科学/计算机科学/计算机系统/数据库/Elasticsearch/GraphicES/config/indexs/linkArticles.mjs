let config = {
  索引名称: 'link_articles',
  其他: {
    是否暴露: true,
    索引描述: '链接/文章',
  },
  聚合条件: {
    "根据标签分桶": {
      terms: {
        field: "标签",
        size: 10000
      }
    }
  },
  聚合图表: {
    根据标签分桶: function (params) {
      let formatedData = {}
      params.data.forEach(item => {
        formatedData[item.名称] = item.数量
      })

      Graphic.chartJs2.doughnut({
        元素ID: params.fNode.id + '-' + params.aggsName + '-' + 'doughnut',
        父元素: params.fNode,
        渲染对象: formatedData,
        标题: params.currentIndex.其他.索引描述 + ' | ' + params.aggsName
      })

      Graphic.amcharts4.verticalGraph({
        元素ID: params.fNode.id + '-' + params.aggsName + '-' + 'verticalGraph',
        父元素: params.fNode,
        渲染对象: formatedData,
        标题: params.currentIndex.其他.索引描述 + ' | ' + params.aggsName
      })
    },
  }
}
export default config;