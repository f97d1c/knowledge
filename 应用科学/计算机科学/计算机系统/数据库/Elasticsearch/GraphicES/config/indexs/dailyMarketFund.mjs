let metrics = {
  "平均|单位净值": {
    "avg": { "field": "单位净值" }
  },
  "平均|累计净值": {
    "avg": { "field": "累计净值" }
  },
  "平均|日增长率": {
    "avg": { "field": "日增长率" }
  },
  "最低|累计净值": {
    "min": { "field": "累计净值" }
  },
  "最高|累计净值": {
    "max": { "field": "累计净值" }
  }
}

let config = {
  索引名称: 'daily_market_funds',
  其他: {
    是否暴露: true,
    索引描述: '每日数据',
  },
  排序方式: { "净值日期": { "order": "asc" } },
  聚合条件: {
    "基金概况": {
      "terms": {
        "field": "基金简称"
      },
      "aggs": metrics
    },
    "基金年度概况": {
      "terms": {
        "field": "基金简称"
      },
      "aggs": {
        "nextLevel": {
          "date_histogram": {
            "field": "净值日期",
            "interval": "year",
            "min_doc_count": 1,
            "format": "yyyy年"
          },
          "aggs": metrics
        }
      }
    }
  },
  聚合图表: {
    "基金年度概况\\|*": function(params){
      let metrics = Object.keys(params.data[0]).filter(str => { return str.match(/.*\|.*/)})
      let dataByMetrics = {}
      metrics.forEach(key => {
        let tmp = {}
        dataByMetrics[key] = tmp
        params.data.forEach(item=>{
          tmp[item.名称] = item[key]
        })
      })

      Graphic.chartJs2.broken_line({
        元素ID: params.fNode.id + '-' + params.aggsName + '-' + 'broken_line',
        父元素: params.fNode,
        渲染对象: dataByMetrics,
        标题: params.aggsName
      })
    }
  }
}
export default config;