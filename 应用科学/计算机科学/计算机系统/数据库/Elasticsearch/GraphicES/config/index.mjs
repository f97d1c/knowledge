import linkArticles from './indexs/linkArticles.mjs'
import uniteStructure from './indexs/uniteStructure.mjs'
import dailyMarketFund from './indexs/dailyMarketFund.mjs'

let esConfig = {
  indexs: {
    linkArticles: linkArticles,
    uniteStructure: uniteStructure,
    dailyMarketFund: dailyMarketFund,
  }
}

export default esConfig;