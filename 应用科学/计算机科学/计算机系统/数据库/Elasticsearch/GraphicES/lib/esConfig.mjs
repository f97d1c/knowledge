export default class EsConfig {
  constructor(params = {}) {
    let res = Graphic.toElement({ element: 'div', id: 'space-es-config' })
    if (!res[0]) return res
    params.fNode.appendChild(res[1])
    this.node = res[1]
    return [true, this]
  }

  init(params = {}){
    if (!!!params.configRequest) return [false, 'ES请求参数为空']
    if (Object.keys(params.elasticsearch || {}).length == 0) return [false, 'ES可用索引为空']

    let configRequest = {}
    for (let [key, value] of Object.entries(params.configRequest)) {
      configRequest[key] = { element: 'input', type: 'text', class: 'form-control', value: value, name: key }
    }

    let options = [{ element: 'option', value: '', innerHtml: '选择当前索引' }]
    for (let [key, value] of Object.entries(params.elasticsearch)) {
      let option = {
        element: 'option',
        value: key,
        innerHtml: value.其他.索引描述 + ': ' + value.索引全称,
      }
      if (params.unineCode == key) option.selected = true
      options.push(option)
    }

    configRequest['当前索引'] = {
      element: 'select',
      class: 'form-control',
      name: '当前索引',
      innerHtml: options,
      onchange: 'Space.index.change({unineCode: this.value});',
    }

    configRequest['其他'] = [
      {
        element: 'button',
        type: 'button',
        class: 'btn btn-primary mr-3 mt-2',
        innerHtml: '导出结果',
        onclick: 'Space.index.assemblyLine.exportExcel();',
      },
      {
        element: 'button',
        type: 'button',
        class: 'btn btn-primary mr-3 mt-2',
        innerHtml: '查询结果',
        onclick: 'Space.index.change({reload: true});',
      }
    ]

    let res = Graphic.bootstrap4.baseTable({ 渲染对象: configRequest, 父元素: this.node, 元素ID: 'space-es-config-request' })
    if (!res[0]) return res
    return [true, this]
  }

  beforeLoad(params){
    this.init(params)
    this.requestParams(params)
    return [true, '']
  }

  afterLoad(params){
    params.configRequest = params.requestParams
    return this.init(params)
  }

  // 请求参数
  requestParams(params) {
    let requestParams = {}
    this.node.querySelectorAll('input, select').forEach(item => {
      if (!!!item.value || !!!item.name) return;
      requestParams[item.name] = item.value
    })
    params.requestParams = requestParams
    return [true, params.requestParams]
  }

}