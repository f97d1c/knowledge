export default class IndexButton {
  constructor(params = {}) {
    let res = Graphic.toElement({ element: 'div', id: 'space-index-button' })
    if (!res[0]) return res
    params.fNode.appendChild(res[1])
    this.node = res[1]
    return [true, this]
  }

  init(params = {}){
    let buttons = {选择索引: []}

    for (let [key, value] of Object.entries(params.elasticsearch)) {
      let button = {
        element: 'button',
        type: 'button',
        value: key,
        class: 'btn btn-primary mr-3 mt-1',
        innerHtml: value.其他.索引描述,
        onclick: 'Space.index.change({unineCode: this.value});',
      }
      // if (params.unineCode == key) button.selected = true
      buttons.选择索引.push(button)
    }

    return Graphic.bootstrap4.baseTable({ 渲染对象: buttons, 父元素: this.node, 元素ID: 'space-index-button-table', 名称单元格占比: 10 })
  }
}