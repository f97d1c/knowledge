export default class AggsChart {
  constructor(params = {}) {
    let res = Graphic.toElement({element: 'div',id: 'space-aggs_chart'})
    if (!res[0]) return res
    params.fNode.appendChild(res[1])
    this.element = res[1]
    return [true, this]
  }

  currentResponse(params){
    return params.dataStore.indexResponse[params.unineCode]
  }

  beforeLoad(params){
    this.element.style.height = ''
    this.element.innerHTML = ''
    return [true, '']
  }
    
  afterLoad(params){
    if (typeof params.currentIndex.聚合图表 != 'object') return [false, '当前索引尚未实现聚合图表对象']
    if (!!!this.currentResponse(params)) return [false, '当前索引的原始返回数据为空']

    let aggs = this.currentResponse(params).result.aggs
    if (!!!aggs) return [false, '当前索引的数据聚合信息为空']

    for (let [key, value] of Object.entries(aggs)) {
      let aggKey = undefined
      Object.keys(params.currentIndex.聚合图表).forEach(item =>{
        if (key.match(new RegExp(item))) aggKey = item
      })

      let func = params.currentIndex.聚合图表[aggKey]
      if (!!!func) continue
      func(Object.assign(Object.assign({}, params), {fNode: this.element, aggsName: key, data: value}))
    }
    return Graphic.sudoku({父元素: this.element})
  }
}