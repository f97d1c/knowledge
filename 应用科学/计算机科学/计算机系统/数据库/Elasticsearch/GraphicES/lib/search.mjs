export default class Search {
  constructor(params = {}) {
    let res = Graphic.toElement({ element: 'div', id: 'space-search' })
    if (!res[0]) return res
    params.fNode.appendChild(res[1])
    this.element = res[1]
    return [true, this]
  }

  currentNode(params){
    let nodeId = (this.element.id + '-' + params.unineCode)
    let current = document.getElementById(nodeId)
    if (!!!current){
      current = document.createElement('tmp')
      current.setAttribute('id', nodeId)
    }
    return current
  }

  beforeLoad(params){
    return this.queryParams(params)
  }

  afterLoad(params) {
    let currentIndex = params.currentIndex
    if (!!!currentIndex.索引映射) return [false, '当前索引的字段映射为空']

    if (this.currentNode(params).nodeName != 'TMP'){
      this.currentNode(params).style.display = ''
      return [true, '']
    }

    let 索引映射 = Object.assign({}, currentIndex.索引映射)
    索引映射['其他'] = [
      {
        element: 'button',
        type: 'button',
        class: 'btn btn-danger mr-3 mt-2',
        innerHtml: '清空条件',
        onclick: "Graphic.getParentNode(this, 'TABLE').querySelectorAll('input').forEach(input => {input.value = ''});",
      },
      {
        element: 'button',
        type: 'button',
        class: 'btn btn-primary mr-3 mt-2',
        innerHtml: '查询结果',
        onclick: 'Space.index.change({reload: true});',
      }
    ]

    return Graphic.bootstrap4.formTable({
      元素ID: this.currentNode(params).id,
      父元素: this.element,
      渲染对象: 索引映射,
      每行元素个数: 2,
      标题: currentIndex.其他.索引描述 + "查询条件"
    })
  }

  // 搜索参数
  queryParams(params) {
    let esQuery = { "bool": { "must": [] } }
    let must = esQuery.bool.must
    this.currentNode(params).querySelectorAll('input').forEach(item => {
      if (!!!item.value) return;
      let es_query = item.getAttribute('es_query')
      if (!!!es_query) {
        let match = { "match": {} }
        match['match'][item.name] = item.value
        must.push(match)
        return;
      }

      es_query = JSON.parse(es_query, function (k, v) {
        if (typeof v == 'string' && v.match(/value/)) return v.replace('value', item.value);
        return v
      })
      must.push(es_query)
    })

    params.requestParams.搜索参数 = esQuery
    return [true, esQuery]
  }
}