export default class Result {
  constructor(params = {}) {
    let res = Graphic.toElement({element: 'div',id: 'space-result'})
    if (!res[0]) return res
    params.fNode.appendChild(res[1])
    this.element = res[1]
    return [true, this]
  }

  currentResult(params){
    return params.dataStore.indexResult[params.unineCode]
  }
  
  afterLoad(params){
    if (!!!this.currentResult(params)) return [false, '查询结果为空']
    return Graphic.bootstrap4.listTable({
      元素ID: this.element.id+'-'+params.unineCode,
      渲染对象: this.currentResult(params),
      父元素: this.element,
      自动换行: false,
      渲染顺序: '倒序' 
    })
  }

  beforeExportExcel(params){
    if (!!!this.currentResult(params)) return [false, '当前索引返回结果为空']
    params.dataStore['exportExcel'][params.currentIndex.其他.索引描述+' 数据信息'] = this.currentResult(params)
  }
}