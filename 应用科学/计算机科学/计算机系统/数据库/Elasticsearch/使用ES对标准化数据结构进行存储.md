
<!-- TOC -->

- [说明](#说明)
  - [映射(Mapping)](#映射mapping)
  - [分析(Analysis)](#分析analysis)
  - [领域特定查询语言(Query DSL)](#领域特定查询语言query-dsl)
  - [倒排索引](#倒排索引)
    - [不变性](#不变性)
  - [分析与分析器](#分析与分析器)
      - [标准分析器](#标准分析器)
      - [简单分析器](#简单分析器)
      - [空格分析器](#空格分析器)
      - [语言分析器](#语言分析器)
      - [什么时候使用分析器](#什么时候使用分析器)
  - [关于文档](#关于文档)
  - [文档元数据](#文档元数据)
    - [_index](#_index)
    - [_type](#_type)
    - [_id](#_id)
  - [分片](#分片)
  - [乐观并发控制](#乐观并发控制)
    - [通过外部系统使用版本控制](#通过外部系统使用版本控制)
  - [更新文档过程](#更新文档过程)
  - [集群健康](#集群健康)
  - [Groovy脚本编程](#groovy脚本编程)
  - [带请求体的GET请求](#带请求体的get请求)
  - [基础数据构建](#基础数据构建)
    - [关于requestES](#关于requestes)
    - [创建索引](#创建索引)
    - [类型映射](#类型映射)
    - [导入文档](#导入文档)
- [API文档](#api文档)
  - [映射(_mapping)](#映射_mapping)
    - [核心简单域类型](#核心简单域类型)
    - [自定义域映射](#自定义域映射)
      - [index](#index)
      - [analyzer](#analyzer)
    - [更新映射](#更新映射)
    - [查看映射](#查看映射)
    - [复杂核心域类型](#复杂核心域类型)
      - [多值域](#多值域)
      - [空域](#空域)
      - [多层级对象](#多层级对象)
        - [内部对象是如何索引的](#内部对象是如何索引的)
  - [搜索(_search)](#搜索_search)
    - [空搜索](#空搜索)
      - [took](#took)
      - [timed_out](#timed_out)
      - [_shards](#_shards)
      - [hits](#hits)
    - [多索引,多类型](#多索引多类型)
    - [根据_id查询结果](#根据_id查询结果)
    - [分页](#分页)
      - [在分布式系统中深度分页](#在分布式系统中深度分页)
    - [取回文档部分内容(_source)](#取回文档部分内容_source)
      - [不返回任何元数据](#不返回任何元数据)
    - [轻量搜索](#轻量搜索)
    - [查询表达式(Query DSL)](#查询表达式query-dsl)
      - [查询语句的结构](#查询语句的结构)
      - [合并查询语句](#合并查询语句)
      - [查询与过滤](#查询与过滤)
        - [性能差异](#性能差异)
        - [如何选择查询与过滤](#如何选择查询与过滤)
      - [查询方式](#查询方式)
        - [简单的匹配所有文档(match_all)](#简单的匹配所有文档match_all)
        - [标准查询(match)](#标准查询match)
        - [多个字段上执行相同的match查询(multi_match)](#多个字段上执行相同的match查询multi_match)
          - [查询字段名称的模糊匹配](#查询字段名称的模糊匹配)
          - [提升单个字段的权重](#提升单个字段的权重)
        - [短语搜索(match_phrase)](#短语搜索match_phrase)
        - [范围查询(range)](#范围查询range)
          - [日期范围](#日期范围)
          - [字符串范围](#字符串范围)
        - [精确值匹配(term)](#精确值匹配term)
        - [多值匹配(terms)](#多值匹配terms)
        - [有值/无值查询(exists/missing)](#有值无值查询existsmissing)
          - [对象上的存在与缺失](#对象上的存在与缺失)
        - [增加语句权重(boost)](#增加语句权重boost)
        - [游标查询(scroll)](#游标查询scroll)
        - [高亮搜索(highlight)](#高亮搜索highlight)
        - [词频分析(aggs aggregations)](#词频分析aggs-aggregations)
          - [分级汇总](#分级汇总)
      - [组合多查询](#组合多查询)
        - [bool查询](#bool查询)
          - [评分方式](#评分方式)
        - [避免因条件影响得分(filter)](#避免因条件影响得分filter)
        - [constant_score查询](#constant_score查询)
        - [分离最大化查询(dis_max)](#分离最大化查询dis_max)
          - [考虑其他语句评分(tie_breaker)](#考虑其他语句评分tie_breaker)
    - [排序(sort)](#排序sort)
      - [多级排序](#多级排序)
      - [多值字段的排序](#多值字段的排序)
      - [字符串排序与多字段](#字符串排序与多字段)
    - [相关性](#相关性)
      - [检索词频率/反向文档频率(TF/IDF)](#检索词频率反向文档频率tfidf)
        - [检索词频率](#检索词频率)
        - [反向文档频率](#反向文档频率)
        - [字段长度准则](#字段长度准则)
      - [理解评分标准](#理解评分标准)
  - [验证查询(_validate)](#验证查询_validate)
  - [(_get)](#_get)
  - [(_index)](#_index)
  - [删除(_delete)](#删除_delete)
  - [批量操作(_bulk)](#批量操作_bulk)
  - [更新(_update)](#更新_update)
    - [更新部分内容](#更新部分内容)
    - [使用脚本更新](#使用脚本更新)
      - [更新的文档可能尚不存在](#更新的文档可能尚不存在)
    - [删除文档](#删除文档)
      - [通过脚本](#通过脚本)
  - [(_mget)](#_mget)
  - [(_analyze)](#_analyze)
    - [测试分析器](#测试分析器)
      - [ik_max_word](#ik_max_word)
      - [ik_smart](#ik_smart)
    - [紧凑和对齐文本(_cat)](#紧凑和对齐文本_cat)
      - [查看节点状态](#查看节点状态)
      - [查看所有索引](#查看所有索引)
  - [索引管理](#索引管理)
    - [删除索引](#删除索引)
    - [索引设置(_settings)](#索引设置_settings)
      - [更新配置](#更新配置)
    - [配置分析器](#配置分析器)
    - [自定义分析器](#自定义分析器)
    - [类型和映射](#类型和映射)
    - [根对象](#根对象)
      - [properties](#properties)
        - [type](#type)
        - [index](#index-1)
        - [analyzer](#analyzer-1)
      - [元数据](#元数据)
        - [文档内容(_source)](#文档内容_source)
      - [动态映射](#动态映射)
      - [自定义动态映射](#自定义动态映射)
        - [日期检测(date_detection)](#日期检测date_detection)
    - [动态模板(dynamic_templates)](#动态模板dynamic_templates)
    - [重新索引(_reindex)](#重新索引_reindex)
    - [索引别名和零停机](#索引别名和零停机)
      - [单个别名操作(_alias)](#单个别名操作_alias)
      - [原子级别名操作(_aliases)](#原子级别名操作_aliases)
- [聚合](#聚合)
  - [桶(Buckets)与指标(Metrics)](#桶buckets与指标metrics)
    - [桶](#桶)
    - [指标(Metrics)](#指标metrics)
    - [桶和指标的组合](#桶和指标的组合)
  - [基础数据准备](#基础数据准备)
    - [中文模式](#中文模式)
    - [英文模式](#英文模式)
  - [为聚合指定名称](#为聚合指定名称)
    - [关于聚合字段内容分词问题](#关于聚合字段内容分词问题)
  - [量度聚合(待重构)](#量度聚合待重构)
    - [平均值聚合(Avg Aggregation)](#平均值聚合avg-aggregation)
    - [近似聚合](#近似聚合)
      - [基数聚合(Cardinality Aggregation)](#基数聚合cardinality-aggregation)
        - [精确度(precision_threshold)](#精确度precision_threshold)
        - [速度优化](#速度优化)
      - [百分位计算(Percentiles Aggregation)](#百分位计算percentiles-aggregation)
    - [扩展统计聚合( Extended Stats Aggregation)](#扩展统计聚合-extended-stats-aggregation)
    - [地理边界聚合(Geo Bounds Aggregation)](#地理边界聚合geo-bounds-aggregation)
    - [地理重心聚合(Geo Centroid Aggregation)](#地理重心聚合geo-centroid-aggregation)
    - [最大值聚合(Max Aggregation)](#最大值聚合max-aggregation)
    - [最小值聚合(Min Aggregation)](#最小值聚合min-aggregation)
    - [Percentile Ranks Aggregation](#percentile-ranks-aggregation)
    - [Scripted Metric Aggregation](#scripted-metric-aggregation)
    - [Stats Aggregation](#stats-aggregation)
    - [总和聚合(Sum Aggregation)](#总和聚合sum-aggregation)
    - [Top hits Aggregation](#top-hits-aggregation)
    - [Value Count Aggregation](#value-count-aggregation)
  - [添加度量指标](#添加度量指标)
  - [嵌套桶](#嵌套桶)
  - [条形图(histogram)](#条形图histogram)
  - [按时间统计(date_histogram)](#按时间统计date_histogram)
  - [关于范围扩展(extended_bounds)](#关于范围扩展extended_bounds)
  - [范围限定的聚合](#范围限定的聚合)
    - [全局桶](#全局桶)
  - [过滤桶(filter)](#过滤桶filter)
  - [后过滤器(post_filter)](#后过滤器post_filter)
  - [多桶排序](#多桶排序)
    - [内置排序](#内置排序)
    - [按度量排序](#按度量排序)
    - [基于 *深度* 度量排序](#基于-深度-度量排序)
  - [通过聚合发现异常指标(SigTerms)](#通过聚合发现异常指标sigterms)
  - [优化聚合查询](#优化聚合查询)
    - [深度优先与广度优先(Depth-First Versus Breadth-First)](#深度优先与广度优先depth-first-versus-breadth-first)
  - [Doc Values&Fielddata](#doc-valuesfielddata)
    - [Doc Values](#doc-values)
      - [列式存储的压缩](#列式存储的压缩)
      - [禁用DocValues](#禁用docvalues)
      - [聚合与分析](#聚合与分析)
        - [分析字符串和Fielddata(Analyzed strings and Fielddata)](#分析字符串和fielddataanalyzed-strings-and-fielddata)
        - [高基数内存的影响(High-Cardinality Memory Implications)](#高基数内存的影响high-cardinality-memory-implications)
      - [限制内存使用](#限制内存使用)
        - [选择堆大小(Choosing a Heap Size)](#选择堆大小choosing-a-heap-size)
        - [Fielddata的大小](#fielddata的大小)
        - [监控fielddata(Monitoring fielddata)](#监控fielddatamonitoring-fielddata)
          - [按索引使用](#按索引使用)
          - [按节点使用](#按节点使用)
          - [按索引节点](#按索引节点)
        - [断路器](#断路器)
          - [可用的断路器(Available Circuit Breakers)](#可用的断路器available-circuit-breakers)
    - [Fielddata](#fielddata)
      - [Fielddata的过滤](#fielddata的过滤)
      - [预加载fielddata](#预加载fielddata)
      - [全局序号(Global Ordinals)](#全局序号global-ordinals)
        - [构建全局序号](#构建全局序号)
        - [预构建全局序号](#预构建全局序号)
        - [索引预热器](#索引预热器)
  - [内存管理](#内存管理)
- [数据建模](#数据建模)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明
## 映射(Mapping)

> 描述数据在每个字段内如何存储.

## 分析(Analysis)

全文是如何处理使之可以被搜索的.

## 领域特定查询语言(Query DSL)

Elasticsearch中强大灵活的查询语言.

## 倒排索引

Elasticsearch使用一种称为倒排索引的结构,它适用于快速的全文搜索.<br>
一个倒排索引由文档中所有不重复词的列表构成,对于其中每个词,有一个包含它的文档列表.

例如,假设有两个文档,每个文档的content域包含如下内容:

Doc_1: The quick brown fox jumped over the lazy dog

Doc_2: Quick brown foxes leap over lazy dogs in summer

为了创建倒排索引,首先将每个文档的content域拆分成单独的词(称它为词条或tokens),<br>
创建一个包含所有不重复词条的排序列表,然后列出每个词条出现在哪个文档.<br>
结果如下所示:

Term|Doc_1|Doc_2
-|-|-
Quick||X
The|X|
brown|X|X
dog|X|
dogs||X
fox|X|
foxes||X
in||X
jumped|X|
lazy|X|X
leap||X
over|X|X
quick|X|
summer||X
the|X|


现在,如果想搜索quick brown,只需要查找包含每个词条的文档:

Term|Doc_1|Doc_2
-|-|-
brown|X|X
quick|X|
Total|2|1

两个文档都匹配,但是第一个文档比第二个匹配度更高.<br>
如果使用仅计算匹配词条数量的简单相似性算法,<br>
那么,可以说,对于查询的相关性来讲,第一个文档比第二个文档更佳.

但是,目前的倒排索引有一些问题:

0. Quick和quick以独立的词条出现,然而用户可能认为它们是相同的词.
0. fox和foxes非常相似,就像dog和dogs;有相同的词根.
0. jumped和leap, 尽管没有相同的词根,但意思很相近.是同义词.

使用前面的索引搜索 +Quick +fox不会得到任何匹配文档(+ 前缀表明这个词必须存在).<br>
只有同时出现Quick和fox的文档才满足这个查询条件,<br>
但是第一个文档包含quickfox ,第二个文档包含Quick foxes.

用户可以合理的期望两个文档与查询匹配.可以做的更好.

如果将词条规范为标准模式,那么可以找到与用户搜索的词条不完全一致,但具有足够相关性的文档.<br>
例如:

0. Quick可以小写化为quick.
0. foxes可以词干提取 --变为词根的格式-- 为fox.<br>类似的,dogs可以为提取为dog.
0. jumped和leap是同义词,可以索引为相同的单词jump.

现在索引看上去像这样:

Term|Doc_1|Doc_2
-|-|-
brown|X|X
dog|X|X
fox|X|X
in||X
jump|X|X
lazy|X|X
over|X|X
quick|X|X
summer||X
the|X|X

这还远远不够,搜索 +Quick +fox仍然会失败,因为在索引中,已经没有Quick了.<br>
但是,如果对搜索的字符串使用与content域相同的标准化规则,<br>
会变成查询 +quick +fox ,这样两个文档都会匹配!

这非常重要.只能搜索在索引中出现的词条,所以索引文本和查询字符串必须标准化为相同的格式.

### 不变性

倒排索引被写入磁盘后是 *不可改变* 的:它永远不会修改.<br>
不变性有重要的价值:

0. 不需要锁.如果从来不更新索引,就不需要担心多进程同时修改数据的问题.
0. 一旦索引被读入内核的文件系统缓存,便会留在哪里,由于其不变性.<br>
只要文件系统缓存中还有足够的空间,那么大部分读请求会直接请求内存,而不会命中磁盘.<br>
这提供了很大的性能提升.
0. 其它缓存(像filter缓存),在索引的生命周期内始终有效.<br>
它们不需要在每次数据改变时被重建,因为数据不会变化.
0. 写入单个大的倒排索引允许数据被压缩,减少磁盘I/O和需要被缓存到内存的索引的使用量.
当然,一个不变的索引也有不好的地方.<br>
主要事实是它是不可变的! 不能修改它.<br>
如果需要让一个新的文档可被搜索,需要重建整个索引.<br>
这要么对一个索引所能包含的数据量造成了很大的限制,要么对索引可被更新的频率造成了很大的限制.

## 分析与分析器

在 *倒排索引* 中分词和标准化的过程称为**分析** .

分析包含下面的过程:

0. 首先,将一块文本分成适合于倒排索引的独立的词条.
0. 之后,将这些词条统一化为标准格式以提高它们的 *可搜索性* ,或者recall
分析器执行上面的工作.<br>

分析器实际上是将三个功能封装到了一个包里:

0. *字符过滤器* 首先,字符串按顺序通过每个字符过滤器 .<br>
任务是在分词前整理字符串.<br>
一个字符过滤器可以用来去掉HTML,或者将 & 转化成and.

0. *分词器* 其次,字符串被分词器分为单个的词条.<br>
一个简单的分词器遇到空格和标点的时候,可能会将文本拆分成词条.

0. *Token过滤器* 最后,词条按顺序通过每个token过滤器 .<br>
这个过程可能会改变词条(例如,小写化Quick),删除词条(例如, 像a, and,the等无用词),或者增加词条(例如,像jump和leap这种同义词).

Elasticsearch提供了开箱即用的字符过滤器、分词器和token过滤器.<br>
 这些可以组合起来形成自定义的分析器以用于不同的目的.
 
 ### 内置分析器

Elasticsearch还附带了可以直接使用的预包装的分析器.<br>
接下来会列出最重要的分析器.<br>
为了证明它们的差异,看看每个分析器会从下面的字符串得到哪些词条:

```
"Set the shape to semi-transparent by calling set_trans(5)"
```

#### 标准分析器

标准分析器是Elasticsearch默认使用的分析器.<br>
它是分析各种语言文本最常用的选择.<br>
它根据Unicode联盟定义的单词边界划分文本.<br>
删除绝大部分标点.<br>
最后,将词条小写.<br>
它会产生:
```
set, the, shape, to, semi, transparent, by, calling, set_trans, 5
```

#### 简单分析器

简单分析器在任何不是字母的地方分隔文本,将词条小写.<br>
它会产生:

```
set, the, shape, to, semi, transparent, by, calling, set, trans
```

#### 空格分析器

空格分析器在空格的地方划分文本.<br>
它会产生:

```
Set, the, shape, to, semi-transparent, by, calling, set_trans(5)
```

#### 语言分析器

特定语言分析器可用于很多语言.<br>
它们可以考虑指定语言的特点.<br>
例如, 英语分析器附带了一组英语无用词(常用单词,例如and或者the,它们对相关性没有多少影响),它们会被删除.<br>
由于理解英语语法的规则,这个分词器可以提取英语单词的词干.<br>
它会产生下面的词条:

```
set, shape, semi, transpar, call, set_tran, 5
```

注意看transparent、calling和set_trans已经变为词根格式.

#### 什么时候使用分析器

当索引一个文档,它的全文域被分析成词条以用来创建倒排索引.<br>
但是,当在全文域搜索的时候,需要将查询字符串通过相同的分析过程,<br>
以保证搜索的词条格式与索引中的词条格式一致.

全文查询,理解每个域是如何定义的,因此它们可以做正确的事:

0. 当查询一个全文域时, 会对查询字符串应用相同的分析器,以产生正确的搜索词条列表.
0. 当查询一个精确值域时,不会分析查询字符串,而是搜索指定的精确值.

## 关于文档

在大多数应用中,多数实体或对象可以被序列化为包含键值对的JSON对象.<br>
一个键可以是一个字段或字段的名称,一个值可以是一个字符串,一个数字,一个布尔值,<br>
另一个对象,一些数组值,或一些其它特殊类型诸如表示日期的字符串,或代表一个地理位置的对象.

通常情况下,使用的术语对象和文档是可以互相替换的.<br>
不过,有一个区别: 一个对象仅仅是类似于hash、hashmap、<br>
字典或者关联数组的JSON对象,对象中也可以嵌套其他的对象.<br>
对象可能包含了另外一些对象.<br>
在Elasticsearch中,术语文档有着特定的含义.<br>
它是指最顶层或者根对象, 这个根对象被序列化成JSON并存储到Elasticsearch中,指定了唯一ID.

## 文档元数据

一个文档不仅仅包含它的文档 ,也包含元文档——有关文档的信息.<br>
 三个必须的元文档元素如下:

0. _index: 文档在哪存放
0. _type: 文档表示的对象类别
0. _id: 文档唯一标识

### _index

实际上,在Elasticsearch中,文档是被存储和索引在分片中,而一个索引仅仅是逻辑上的命名空间,<br>
这个命名空间由一个或者多个分片组合在一起.<br>
然而,这是一个内部细节,应用程序根本不应该关心分片,对于应用程序而言,只需知道文档位于一个索引内.<br>
Elasticsearch会处理所有的细节.<br>

### _type

### _id

ID是一个字符串,当它和_index以及_type组合就可以唯一确定Elasticsearch中的一个文档.<br>
当创建一个新的文档,要么提供自己的_id,要么让Elasticsearch帮生成.<br>

如果文档没有自然的ID,Elasticsearch可以帮自动生成ID.<br>
自动生成的ID是URL-safe、基于Base64编码且长度为20个字符的GUID字符串.<br>
这些GUID字符串由可修改的FlakeID模式生成,这种模式允许多个节点并行生成唯一ID,且互相之间的冲突概率几乎为零.

## 分片

往Elasticsearch添加文档时需要用到索引——保存相关文档的地方.<br>
索引实际上是指向一个或者多个物理分片的逻辑命名空间 .

一个分片是一个底层的工作单元 ,它仅保存了全部文档中的一部分.<br>
在分片内部机制中,一个分片是一个Lucene的实例,以及它本身就是一个完整的搜索引擎.<br>
文档被存储和索引到分片内,但是应用程序是直接与索引而不是与分片进行交互.

Elasticsearch是利用分片将文档分发到集群内各处的.<br>
分片是文档的容器,文档保存在分片内,分片又被分配到集群内的各个节点里.<br>
当集群规模扩大或者缩小时,Elasticsearch会自动的在各节点中迁移分片,使得文档仍然均匀分布在集群里.

一个分片可以是主分片或者副本分片.<br>
索引内任意一个文档都归属于一个主分片,所以主分片的数目决定着索引能够保存的最大文档量.

一个副本分片只是一个主分片的拷贝.<br>
副本分片作为硬件故障时保护文档不丢失的冗余备份,并为搜索和返回文档等读操作提供服务.

在索引建立的时候就已经确定了主分片数,但是副本分片数可以随时修改(7.X修改返回异常).

索引在默认情况下会被分配5个主分片,将分配3个主分片和一份副本(每个主分片拥有一个副本分片):

```bash
requestES PUT 'link_articles/' '
{
   "settings": {
      "number_of_shards": 3,
      "number_of_replicas": 1
   }
}
'
```

```json
// requestES GET '_cluster/health'
{
  "cluster_name": "docker-cluster",
  "status": "yellow",
  "timed_out": false,
  "number_of_nodes": 1,
  "number_of_data_nodes": 1,
  "active_primary_shards": 4,
  "active_shards": 4,
  "relocating_shards": 0,
  "initializing_shards": 0,
  "unassigned_shards": 4,
  "delayed_unassigned_shards": 0,
  "number_of_pending_tasks": 0,
  "number_of_in_flight_fetch": 0,
  "task_max_waiting_in_queue_millis": 0,
  "active_shards_percent_as_number": 50.0
}
```

集群的健康状况为yellow则表示全部主分片都正常运行(集群可以正常服务所有请求),<br>
但是副本分片没有全部处在正常状态.<br>
实际上,所有副本分片都是unassigned——它们都没有被分配到任何节点.<br>
在同一个节点上既保存原始文档又保存副本是没有意义的,因为一旦失去了那个节点,也将丢失该节点上的所有副本文档.

当前集群是正常运行的,但是在硬件故障时有丢失文档的风险.<br>

## 乐观并发控制

Elasticsearch是分布式的.<br>
当文档创建、更新或删除时, 新版本的文档必须复制到集群中的其他节点.<br>
Elasticsearch也是异步和并发的,这意味着这些复制请求被并行发送,并且到达目的地时也许顺序是乱的.<br>
Elasticsearch需要一种方法确保文档的旧版本不会覆盖新的版本.

当之前讨论index,GET和delete请求时,指出每个文档都有一个_version(版本)号,当文档被修改时版本号递增.<br>
Elasticsearch使用这个_version号来确保变更以正确顺序得到执行.<br>
如果旧版本的文档在新版本之后到达,它可以被简单的忽略.

可以利用_version号来确保应用中相互冲突的变更不会导致文档丢失.<br>
通过指定想要修改文档的version号来达到这个目的.<br>
如果该版本不是当前版本号,请求将会失败.<br>

### 通过外部系统使用版本控制

一个常见的设置是使用其它文档库作为主要的文档存储,<br>
使用Elasticsearch做文档检索,<br>
这意味着主文档库的所有更改发生时都需要被复制到Elasticsearch,<br>
如果多个进程负责这一文档同步,可能遇到类似于之前描述的并发问题.

如果主文档库已经有了版本号—或一个能作为版本号的字段值比如timestamp,<br>
那么就可以在Elasticsearch中通过增加version_type=external到查询字符串的方式重用这些相同的版本号,<br>
版本号必须是大于零的整数, 且小于9.2E+18—一个Java中long类型的正值.

外部版本号的处理方式和之前讨论的内部版本号的处理方式有些不同,Elasticsearch不是检查当前_version和请求中指定的版本号是否相同, 而是检查当前_version是否小于指定的版本号.<br>
如果请求成功,外部的版本号作为文档的新_version进行存储.

外部版本号不仅在索引和删除请求是可以指定,而且在创建新文档时也可以指定.

## 更新文档过程

文档是不可变的:不能被修改,只能被替换.<br>
updateAPI必须遵循同样的规则.<br>
从外部来看,在一个文档的某个位置进行部分更新.<br>
然而在内部, updateAPI简单使用与之前描述相同的检索-修改-重建索引的处理过程.<br>
区别在于这个过程发生在分片内部,这样就避免了多次请求的网络开销.<br>
通过减少检索和重建索引步骤之间的时间,也减少了其他进程的变更带来冲突的可能性.<br>

## 集群健康

```bash
requestES GET '_cluster/health'
```

status字段指示着当前集群在总体上是否工作正常.它的三种颜色含义如下:

0.green所有的主分片和副本分片都正常运行.
0.yellow所有的主分片都正常运行,但不是所有的副本分片都正常运行.
0.red有主分片没能正常运行.

## Groovy脚本编程

对于那些API不能满足需求的情况,Elasticsearch允许使用脚本编写自定义的逻辑.<br>
许多API都支持脚本的使用,包括搜索、排序、聚合和文档更新.<br>
脚本可以作为请求的一部分被传递,从特殊的 .scripts索引中检索,或者从磁盘加载脚本.

默认的脚本语言是Groovy,一种快速表达的脚本语言,在语法上与JavaScript类似.<br>
它在ElasticsearchV1.3.0版本首次引入并运行在沙盒中,<br>
然而Groovy脚本引擎存在漏洞, 允许攻击者通过构建Groovy脚本,<br>
在ElasticsearchJavaVM运行时脱离沙盒并执行shell命令.<br>
因此它已经被默认禁用.

此外,可以通过设置集群中的所有节点的config/elasticsearch.yml文件来禁用动态Groovy脚本:

```yml
script.groovy.sandbox.enabled: false
```

这将关闭Groovy沙盒,从而防止动态Groovy脚本作为请求的一部分被接受, 或者从特殊的 .scripts索引中被检索.<br>
当然,仍然可以使用存储在每个节点的config/scripts/ 目录下的Groovy脚本.

如果架构和安全性不需要担心漏洞攻击,例如Elasticsearch终端仅暴露和提供给可信赖的应用, 当它是应用需要的特性时,可以选择重新启用动态脚本.

[详细资料](https://www.elastic.co/guide/en/elasticsearch/reference/5.6/modules-scripting.html)

## 带请求体的GET请求

某些特定语言(特别是JavaScript)的HTTP库是不允许GET请求带有请求体的.<br>
事实上,一些使用者对于GET请求可以带请求体感到非常的吃惊.

而事实是这个RFC文档RFC7231 -- 一个专门负责处理HTTP语义和内容的文档,<br>
并没有规定一个带有请求体的GET请求应该如何处理!结果是,一些HTTP服务器允许这样子,<br>
而有一些特别是一些用于缓存和代理的服务器则不允许.

对于一个查询请求,Elasticsearch的工程师偏向于使用GET方式,<br>
因为觉得它比POST能更好的描述信息检索(retrieving information)的行为.<br>
然而,因为带请求体的GET请求并不被广泛支持,所以searchAPI同时支持POST请求.<br>
类似的规则可以应用于任何需要带请求体的GETAPI.

## 基础数据构建

### 关于requestES

该方法具体内容为:

```bash
function requestES(){
  host='localhost'
  port=9200
  if [[ ${2} == *"?"* ]]; then
    requestUl="$host:$port/${2}"
  else
    requestUl="$host:$port/${2}?pretty=true"
  fi
  start_at="开始时间: $(date '+%Y-%m-%d %H:%M:%S')\n"
  if [ ! -z "$3" ]; then
    # Json格式化处理需要安装: npm install -g json
    printf "\n\n$start_at请求方式: $1\n请求路径: $2\n完整路径: $requestUl\n参数:\n$(echo $3|json)"
    printf "\e[2J\e[H\e[m\n返回结果:\n"
    curl -H "Content-Type: application/json" -X $1 $requestUl -d "$3"
  elif [ ! -z "$2" ]; then
    printf "\n\n$start_at请求方式: $1, 请求路径: $2\n完整路径: $requestUl"
    printf "\e[2J\e[H\e[m\n返回结果:\n"
    curl -X $1 $requestUl
  else
    requestUl="$host:$port/${1}?pretty=true"
    printf "\n\n$start_at请求路径: $1\n完整路径: $requestUl"
    printf "\e[2J\e[H\e[m\n返回结果:\n"
    curl "$requestUl"
  fi
}
```
### 创建索引

```bash
requestES DELETE 'link_articles_20210507'
requestES PUT 'link_articles_20210507'
requestES PUT 'link_articles_20210507/_alias/link_articles'
```

### 类型映射

```bash
requestES PUT 'link_articles_20210507/_mapping' '
{
  "properties": {
    "名称": {
      "type": "text"
    },
    "链接": {
      "type": "text"
    },
    "创建时间": {
      "type": "date"
    },
    "标签": {
      "type": "keyword"
    }
  } 
}'
```

### 导入文档

```bash
requestES POST 'link_articles/_doc' '
{
  "名称":"基于树莓派MCC172的神经肌肉生物力学测试系统",
  "标签":["计算机硬件","树莓派","树莓派实验室"],
  "链接":"https://shumeipai.nxez.com/2021/01/25/neuromuscular-biomechanics-test-system-based-on-raspberry-pi-mcc-172.html"
}'
```

脚本导入文档:

```bash
while IFS= read -r line; do
  md5=$(echo -n "$line" |md5sum)
  requestES POST "link_articles/_doc/${md5:0:32}" "$line"
done < /home/ff4c00/Space/知识体系/应用科学/计算机科学/理论计算机科学/编程语言理论/Ruby/Gems/Nokogiri/Scout/outputs/standard_format_data.txt

# 多线程并发写入
# 需要安装parallel
# sudo apt install parallel
export -f requestES

cat /home/ff4c00/Space/知识体系/应用科学/计算机科学/理论计算机科学/编程语言理论/Ruby/Gems/Nokogiri/Scout/outputs/standard_format_data.txt|parallel-j 3 '
  md5=$(echo -n{}|md5sum)
  requestES POST "link_articles/_doc/${md5:0:32}" {}'
```

# API文档

## 映射(_mapping)

为了能够将时间域视为时间,数字域视为数字,字符串域视为全文或精确值字符串,<br>
Elasticsearch需要知道每个域中数据的类型.<br>
这个信息包含在映射中.

如数据输入和输出中解释的,索引中每个文档都有类型 .<br>
每种类型都有它自己的映射,或者模式定义 .<br>
映射定义了类型中的域,每个域的数据类型,以及Elasticsearch如何处理这些域.<br>
映射也用于配置与类型有关的元数据.<br>

### 核心简单域类型

Elasticsearch支持如下简单域类型:

0. 字符串: string
0. 整数: byte, short, integer, long
0. 浮点数: float, double
0. 布尔型: boolean
0. 日期: date

当索引一个包含新域的文档—之前未曾出现--Elasticsearch会使用动态映射,<br>
通过JSON中基本数据类型,尝试猜测域类型,使用如下规则:

JSON type|域type|
-|-|
布尔型:true或者false|boolean|
整数: 123|long|
浮点数: 123.45|double|
字符串,有效日期: 2014-09-15|date|
字符串: foo bar|string

这意味着如果通过引号("123")索引一个数字,它会被映射为string类型,而不是long.<br>
但是,如果这个域已经映射为long,那么Elasticsearch会尝试将这个字符串转化为long,如果无法转化,则抛出一个异常.

### 自定义域映射

尽管在很多情况下基本域数据类型已经够用,但经常需要为单独域自定义映射,特别是字符串域.<br>
自定义映射允许执行下面的操作:

0. 全文字符串域和精确值字符串域的区别
0. 使用特定语言分析器
0. 优化域以适应部分匹配
0. 指定自定义数据格式
0. 还有更多

域最重要的属性是type.对于不是string的域,一般只需要设置type:

```json
// ...
  "名称": {
    "type": "text"
  }
// ...
```

默认,string类型域会被认为包含全文.<br>
就是说,它们的值在索引前,会通过一个分析器,针对于这个域的查询在搜索前也会经过一个分析器.

string域映射的两个最重要属性是index和analyzer.

#### index

index属性控制怎样索引字符串.它可以是下面三个值:

0. analyzed: 首先分析字符串,然后索引它.<br>
换句话说,以全文索引这个域.
0. not_analyzed: 索引这个域,所以它能够被搜索,但索引的是精确值.<br>
不会对它进行分析.
0. no: 不索引这个域.<br>
这个域不会被搜索到.

string域index属性默认是analyzed.<br>
如果想映射这个字段为一个精确值,需要设置它为not_analyzed:

```json
{
  "tag": {
    "type": "text",
    "index": "not_analyzed"
  }
}
```


其他简单类型(例如long, double ,date等)也接受index参数,<br>
但有意义的值只有no和not_analyzed, 因为它们永远不会被分析.

#### analyzer

对于analyzed字符串域,用analyzer属性指定在搜索和索引时使用的分析器.<br>
默认,Elasticsearch使用standard分析器, 但可以指定一个内置的分析器替代它:

```json
// ...
  "名称": {
    "type": "text",
    "analyzer": "ik_max_word",
    "search_analyzer": "ik_max_word",
    "fielddata":"true"
  },
// ...
```

### 更新映射

当首次创建一个索引的时候,可以指定类型的映射.<br>
也可以使用 /_mapping为新类型(或者为存在的类型更新映射)增加映射.

尽管可以增加一个存在的映射,不能修改存在的域映射.<br>
如果一个域的映射已经存在,那么该域的数据可能已经被索引.<br>
如果意图修改这个域的映射,索引的数据可能会出错,不能被正常的搜索.

可以更新一个映射来添加一个新域,但不能将一个存在的域从analyzed改为not_analyzed.

### 查看映射

```bash
requestES GET 'link_articles/_mapping/'
```

### 复杂核心域类型

除了提到的简单标量数据类型,JSON还有null值,数组,和对象,这些Elasticsearch都是支持的.

#### 多值域

很有可能,希望标签域包含多个标签.<br>
可以以数组的形式索引标签:

```json
{"标签": ["金融","基金","喜马拉雅","雪球","玩转指数基金"]}
```

对于数组,没有特殊的映射需求.<br>
任何域都可以包含0、1或者多个值,就像全文域分析得到多个词条.

这暗示 **数组中所有的值必须是相同数据类型** 的.<br>
不能将日期和字符串混在一起.<br>
如果通过索引数组来创建新的域,Elasticsearch会用数组中第一个值的数据类型作为这个域的类型 .

当从Elasticsearch得到一个文档,每个数组的顺序和当初索引文档时一样.<br>
得到的_source域,包含与索引的一模一样的JSON文档.

但是,数组是以多值域索引的—可以搜索,但是无序的.<br>
在搜索的时候,不能指定 *第一个* 或者 *最后一个* .<br>
更确切的说,把数组想象成装在袋子里的值.

#### 空域

数组可以为空.这相当于存在零值.<br>
事实上,在Lucene中是不能存储null值的,所以认为存在null值的域为空域.

下面三种域被认为是空的,它们将不会被索引:

```json
{
  "null_value": null,
  "empty_array": [],
  "array_with_null_value": [ null ]
}
```

#### 多层级对象

讨论的最后一个JSON原生数据类是对象 -- 在其他语言中称为哈希,哈希map,字典或者关联数组.

内部对象经常用于嵌入一个实体或对象到其它对象中.例如:

```json
{
  "名称":"基于树莓派MCC172的神经肌肉生物力学测试系统",
  "标签":["计算机硬件","树莓派","树莓派实验室"],
  "链接":"https://shumeipai.nxez.com/2021/01/25/neuromuscular-biomechanics-test-system-based-on-raspberry-pi-mcc-172.html"
}
```

##### 内部对象是如何索引的

Lucene不理解内部对象.<br>
Lucene文档是由一组键值对列表组成的.<br>
为了能让Elasticsearch有效地索引内部类,它把文档转化成这样:

```json
{
  "名称":"基于树莓派MCC172的神经肌肉生物力学测试系统",
  "标签":["计算机硬件","树莓派","树莓派实验室"],
  "类型":"链接/文章",
  "属性.链接":"https://shumeipai.nxez.com/2021/01/25/neuromuscular-biomechanics-test-system-based-on-raspberry-pi-mcc-172.html"
}
```

内部域可以通过名称引用(例如, 链接).<br>
为了区分同名的两个域,可以使用全路径 (例如, 属性.链接).

在前面简单扁平的文档中,没有user和user.name域.<br>
Lucene索引只有标量和简单值,没有复杂数据结构.

## 搜索(_search)

### 空搜索

> 搜索API的最基础的形式是没有指定任何查询的空搜索,<br>
它简单地返回集群中所有索引下的所有文档.

```bash
requestES 'link_articles/_doc/_search'
```

```json
{
  "took": 0,
  "timed_out": false,
  "_shards": {
    "total": 1,
    "successful": 1,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": {
      "value": 4272,
      "relation": "eq"
    },
    "max_score": 1.0,
    "hits": [
      {
        "_index": "link_articles",
        "_type": "_doc",
        "_id": "7ec4f942457a53bc57643479b9c5598e",
        "_score": 1.0,
        "_source": {
          "名称": "什么是富时中国A50指数",
          "标签": ["金融","基金","喜马拉雅","雪球","玩转指数基金"],
          "链接": "https://www.ximalaya.com/shangye/31885744/333847658"
          ,
          "创建时间": "2021-03-06T10:47:09+08:00:00"
        },
        // ...
      }
    ]
  }
}
```

#### took

took值表示执行整个搜索请求耗费了多少毫秒.<br>

#### timed_out

timed_out值表示查询是否超时.<br>
默认情况下,搜索请求不会超时.

如果低响应时间比完成结果更重要,可以指定timeout为10或者10ms(10毫秒),或者1s(1秒):

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "timeout":"10ms"
}
'
```

在请求超时之前,Elasticsearch将会返回已经成功从每个分片获取的结果.


应当注意的是timeout不是停止执行查询,它仅仅是告知正在协调的节点返回到目前为止收集的结果并且关闭连接.<br>
在后台,其他的分片可能仍在执行查询即使是结果已经被发送了.

使用超时是因为SLA(服务等级协议)对是很重要的,而不是因为想去中止长时间运行的查询.

#### _shards 

_shards部分表示在查询中参与分片的总数,<br>
以及这些分片成功了多少个失败了多少个.<br>
正常情况下不希望分片失败,但是分片失败是可能发生的.<br>
如果遭遇到一种灾难级别的故障,在这个故障中丢失了相同分片的原始数据和副本,<br>
那么对这个分片将没有可用副本来对搜索请求作出响应.<br>
假若这样,Elasticsearch将报告这个分片是失败的,<br>
但是会继续返回剩余分片的结果.<br>

#### hits

返回结果中最重要的部分是hits,<br>
它包含total字段来表示匹配到的文档总数,<br>
并且一个hits数组包含所查询结果的前十个文档.

在hits数组中每个结果包含文档的_index、_type、_id,加上_source字段.<br>
这意味着可以直接从返回的搜索结果中使用整个文档.<br>
这不像其他的搜索引擎,仅仅返回文档的ID,需要单独去获取文档.<br>

每个结果还有一个_score,它衡量了文档与查询的匹配程度.<br>
默认情况下,首先返回最相关的文档结果,就是说,返回的文档是按照_score降序排列的.<br>
在这个例子中,没有指定任何查询,故所有的文档具有相同的相关性,因此对所有的结果而言1是中性的_score.

max_score值是与查询所匹配文档的_score的最大值.

### 多索引,多类型

如果不对某一特殊的索引或者类型做限制,就会搜索集群中的所有文档.<br>
Elasticsearch转发搜索请求到每一个主分片或者副本分片,<br>
汇集查询出的前10个结果,并且返回.

然而,经常的情况下,想在一个或多个特殊的索引并且在一个或者多个特殊的类型中进行搜索.<br>
可以通过在URL中指定特殊的索引和类型达到这种效果,如下所示:

* *6.X版本开始每个索引(Index)下只能有一个类型(Type).* *

```bash
# 在所有的索引中搜索所有的类型
/_search

# 在gb索引中搜索所有的类型
/gb/_search

# 在gb和us索引中搜索所有的文档
/gb,us/_search

# 在任何以g或者u开头的索引中搜索所有的类型
/g *,u* /_search

# 在gb索引中搜索user类型
/gb/user/_search

# 在gb和us索引中搜索user和tweet类型
/gb,us/user,tweet/_search

# 在所有的索引中搜索user和tweet类型
/_all/user,tweet/_search
```

当在单一的索引下进行搜索的时候,Elasticsearch转发请求到索引的每个分片中,<br>
可以是主分片也可以是副本分片,然后从每个分片中收集结果.<br>
多索引搜索恰好也是用相同的方式工作的—只是会涉及到更多的分片.

搜索一个索引有五个主分片和搜索五个索引各有一个分片准确来所说是等价的.

### 根据_id查询结果

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "ids": {
      "type": "_doc",
      "values": ["1", "4", "100"]
    }
  }
}'
```

### 分页


和SQL使用LIMIT关键字返回单个page结果的方法相同,<br>
Elasticsearch接受from和size参数:

0. size: 显示应该返回的结果数量,默认是10
0. from: 显示应该跳过的初始结果数量,默认是0

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "size": 1
}'
```

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "size": 1,
  "from": 10
}'
```

考虑到分页过深以及一次请求太多结果的情况,结果集在返回之前先进行排序.<br>
但请记住一个请求经常跨越多个分片,每个分片都产生自己的排序结果,<br>
这些结果需要进行集中排序以保证整体顺序是正确的.

#### 在分布式系统中深度分页

理解为什么深度分页是有问题的,可以假设在一个有5个主分片的索引中搜索.<br>
当请求结果的第一页(结果从1到10),每一个分片产生前10的结果,并且返回给协调节点,<br>
协调节点对50个结果排序得到全部结果的前10个.

现在假设请求第1000页—结果从10001到10010.<br>
所有都以相同的方式工作除了每个分片不得不产生前10010个结果以外.<br>
然后协调节点对全部50050个结果排序最后丢弃掉这些结果中的50040个结果.

可以看到,在分布式系统中,对结果排序的成本随分页的深度成指数上升.<br>
这就是web搜索引擎对任何查询都不要返回超过1000个结果的原因.

### 取回文档部分内容(_source)

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "size": 1,
  "_source": ["名称", "标签"]
}'
```

```json
// ...
"_source": {
  "名称": "基金买的越多,风险就越分散吗?",
  "标签": ["金融","基金","喜马拉雅","雪球","玩转指数基金"]
}
// ...
```

#### 不返回任何元数据

*这时必须指定文档ID*.

```bash
requestES GET 'link_articles/_doc/f04c862a1465ccbdb228d858a57c82f5/_source'
```

```json
{
  "名称": "基金买的越多,风险就越分散吗?",
  "标签": [
    "金融",
    "基金",
    "喜马拉雅",
    "雪球",
    "玩转指数基金"
  ],
  "链接": "https://www.ximalaya.com/shangye/31885744/334054209"
  ,
  "创建时间": "2021-03-06T10:47:09+08:00:00"
}
```

### 轻量搜索

有两种形式的搜索API:

0. *轻量的* 查询字符串版本,要求在查询字符串中传递所有的参数.
0. 更完整的请求体版本,要求使用JSON格式和更丰富的查询表达式作为搜索语言.

```bash
requestES GET 'link_articles/_doc/_search?size=1&from=10&pretty'

requestES GET 'link_articles/_doc/_search' '
{
  "size": 1,
  "from": 10
}'
```

查询字符串搜索允许任何用户在索引的任意字段上执行可能较慢且重量级的查询,<br>
这可能会暴露隐私信息,甚至将集群拖垮.

因为这些原因,不推荐直接向用户暴露查询字符串搜索功能,除非对于集群和数据来说非常信任.

相反,经常在生产环境中更多地使用功能全面的requestbody查询API,<br>
除了能完成以上所有功能,还有一些附加功能.<br>

### 查询表达式(Query DSL)

查询表达式(Query DSL)是一种非常灵活又富有表现力的查询语言.<br>
Elasticsearch使用它可以以简单的JSON接口来展现Lucene功能的绝大部分.<br>
在应用中,应该用它来编写查询语句.<br>
它可以使查询语句更灵活、更精确、易读和易调试.

要使用这种查询表达式,只需将查询语句传递给query参数.

使用match_all查询, 正如其名字一样,匹配所有文档:

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "match_all": {}
  }
}'
```

#### 查询语句的结构

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": 查询语句
}'
```

一个查询语句的典型结构:

```json
{
  "QUERY_NAME": {
    "ARGUMENT": "VALUE",
    "ARGUMENT": "VALUE",
    // ...
  }
}
```

如果是针对某个字段,那么它的结构如下:

```json
{
  "QUERY_NAME": {
    "FIELD_NAME": {
      "ARGUMENT": "VALUE",
      "ARGUMENT": "VALUE",
      // ...
    }
  }
}
```

match查询名称中包含 *基金* 的文档:

```json
{
  "match": {
    "名称": "基金"
  }
}
```

完整查询语句:

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "match": {
      "名称": "基金"
    }
  }
}'
```

#### 合并查询语句

查询语句(Query clauses) 就像一些简单的组合块,这些组合块可以彼此之间合并组成更复杂的查询.<br>
这些语句可以是如下形式:

0. 叶子语句(Leaf clauses)(就像match语句) 被用于将查询字符串和一个字段(或者多个字段)对比.
0. 复合(Compound)语句主要用于合并其它查询语句.<br>
比如,一个bool语句允许在需要的时候组合其它语句,无论是must匹配、must_not匹配还是should匹配,同时它可以包含过滤器(filters):

```bash
# 查询标签中必须(must)包含(match) 计算机硬件,名称中必须(must)包含(match) 树莓派, 并根据范围(range)筛选(filter)出其中创建时间小于(lt)昨天(now-1d/d)的文档:
requestES GET 'link_articles/_doc/_search' '
{
  "query": { 
    "bool": {
      "must": [
        {"match": {"标签": "计算机硬件"}},
        {"match": {"名称": "树莓派"}}
      ],
      "filter":{"range": {"创建时间": {"lt": "now-1d/d"}}}
    }
  }
}'
```

一条复合语句可以合并任何其它查询语句,包括复合语句,了解这一点是很重要的.<br>
这就意味着,复合语句之间可以互相嵌套,可以表达非常复杂的逻辑.

例如,以下查询是为了找出信件正文包含business opportunity的星标邮件,<br>
或者在收件箱正文包含business opportunity的非垃圾邮件:

```json
{
  "bool": {
    "must": {"match": {"email": "business opportunity"}},
    "should": [
      {"match": {"starred":true}},
      {"bool": {
        "must": {"match": {"folder": "inbox"}},
        "must_not": {"match": {"spam":true}}
      }}
    ],
    "minimum_should_match": 1
  }
}
```

#### 查询与过滤

> 自Elasticsearch问世以来,查询与过滤(queries and filters)就独自成为Elasticsearch的组件.<br>
但从Elasticsearch2.0开始,过滤(filters)已经从技术上被排除了,<br>
同时所有的查询(queries)拥有变成不评分查询的能力.<br>
然而,为了明确和简单,用 "filter"这个词表示不评分、只过滤情况下的查询.<br>
可以把 "filter"、"filtering query" 和 "non-scoring query" 这几个词视为相同的.<br>
相似的,如果单独地不加任何修饰词地使用 "query" 这个词,指的是评分查询(scoring query).

Elasticsearch使用的查询语言(DSL)拥有一套查询组件,<br>
这些组件可以以无限组合的方式进行搭配.<br>
这套组件可以在以下两种情况下使用:过滤情况(filtering context)和查询情况(query context).

当使用于过滤情况时,查询被设置成一个 *不评分*或者*过滤* 查询.<br>
即,这个查询只是简单的问一个问题: *这篇文档是否匹配?* .<br>
回答也是非常的简单,yes或者no,二者必居其一.

0. created时间是否在2013与2014这个区间?
0. status字段是否包含published这个单词?
0. lat_lon字段表示的位置是否在指定点的10km范围内?

当使用于查询情况时,查询就变成了一个 *评分* 的查询.<br>
和不评分的查询类似,也要去判断这个文档是否匹配,<br>
同时它还需要判断这个文档匹配的有多好(匹配程度如何).<br>
此查询的典型用法是用于查找以下文档:

0. 查找与full text search这个词语最佳匹配的文档
0. 包含run这个词,也能匹配runs、running、jog或者sprint
0. 包含quick、brown和fox这几个词,词之间离的越近,文档相关性越高
0. 标有lucene、search或者java标签—标签越多,相关性越高

一个评分查询计算每一个文档与此查询的相关程度,<br>
同时将这个相关程度分配给表示相关性的字段_score,并且按照相关性对匹配到的文档进行排序.<br>
这种相关性的概念是非常适合全文搜索的情况,因为全文搜索几乎没有完全 *正确* 的答案.

##### 性能差异

过滤查询(Filtering queries)只是简单的检查包含或者排除,这就使得计算起来非常快.<br>
考虑到至少有一个过滤查询(filtering query)的结果是 *稀少的* (很少匹配的文档),<br>
并且经常使用不评分查询(non-scoring queries),结果会被缓存到内存中以便快速读取,所以有各种各样的手段来优化查询结果.

相反,评分查询(scoring queries)不仅仅要找出匹配的文档,还要计算每个匹配文档的相关性,<br>
计算相关性使得它们比不评分查询费力的多.<br>
同时,查询结果并不缓存.

多亏倒排索引(inverted index),一个简单的评分查询在匹配少量文档时可能与一个涵盖百万文档的filter表现的一样好,甚至会更好.<br>
但是在一般情况下,一个filter会比一个评分的query性能更优异,并且每次都表现的很稳定.

过滤(filtering)的目标是减少那些需要通过评分查询(scoring queries)进行检查的文档.<br>

##### 如何选择查询与过滤

通常的规则是,使用查询(query)语句来进行全文搜索或者其它任何需要影响相关性得分的搜索.<br>
除此以外的情况都使用过滤(filters).

#### 查询方式
##### 简单的匹配所有文档(match_all)

在没有指定查询方式时,它是默认的查询:

```json
{"match_all": {}}
```

##### 标准查询(match)

无论在任何字段上进行的是全文搜索还是精确查询,match查询是可用的标准查询.<br>
match常用于搜索全文字段中的单个词.

如果在一个全文字段上使用match查询,在执行查询前,它将用正确的分析器去分析查询字符串:

```json
{"match": {"标签": "计算机硬件,基金"}}
```

下面的查询等价与上面的:

```json
{
  "bool": {
    "should": [
      {"term": {"标签": "计算机硬件"}},
      {"term": {"标签": "基金"}}
    ]
  }
}
```

因为match查询必须查找两个词(["计算机硬件","基金"]),它在内部实际上先执行两次term查询,<br>
然后将两次查询的结果合并作为最终结果输出.<br>
为了做到这点,它将两个term查询包入一个bool查询中. 

以上示例表示一个重要信息:即任何文档只要标签字段里包含指定词项中的至少一个词就能匹配,<br>
被匹配的词项越多,文档就越相关.

match查询还可以接受operator操作符作为输入参数,默认情况下该操作符是or.<br>
可以将它修改成and让所有指定词项都必须匹配:


```json
{
  "match": {
    "标签": {
      "query": "计算机硬件,基金",
      "operator": "and"
    }
  }
}
```

如果在一个精确值的字段上使用它,例如数字、日期、布尔或者一个not_analyzed字符串字段,那么它将会精确匹配给定的值.

对于精确值的查询,可能需要使用filter语句来取代query,因为filter将会被缓存.

##### 多个字段上执行相同的match查询(multi_match)

multi_match查询可以在多个字段上执行相同的match查询:


```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "multi_match": {
      "query": "金融",
      "fields": ["名称", "标签"]
    }
  }
}'
```

等价于:

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "bool": {
      "should": [
        {"match": {"名称": "金融"}},
        {"match": {"标签": "金融"}}
      ]
    }
  }
}'
```

###### 查询字段名称的模糊匹配 

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "multi_match": {
      "query": "金融",
      "fields": [" *称", "标* "]
    }
  }
}'
```

###### 提升单个字段的权重

可以使用 ^ 字符语法为单个字段提升权重,在字段名称的末尾添加 ^boost,其中boost是一个浮点数:

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "multi_match": {
      "query": "金融",
      "fields": [" *称^3", "标* "]
    }
  }
}'
```

##### 短语搜索(match_phrase)

搜索内容中空格间隔的词语不将作为或运算,而是作为一个空格间隔的短语,必须完全匹配.

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "match_phrase": {"名称": "树莓派,基金"}
  }
}'
```
##### 范围查询(range)

range查询找出那些落在指定区间内的数字或者时间:

```json
{"range": {"创建时间": {"lt": "now-1d/d"}}}
```

被允许的操作符如下:

操作符|含义
-|-
gt|大于
gte|大于等于
lt|小于
lte|小于等于

###### 日期范围

当使用它处理日期字段时,range查询支持对日期计算(date math) 进行操作,比方说,如果想查找时间戳在过去一小时内的所有文档:

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "range": {
      "创建时间": {
        "gt": "now-1h"
      }
    }
  }
}'
```

这个过滤器会一直查找时间戳在过去一个小时内的所有文档,让过滤器作为一个时间滑动窗口(slidingwindow)来过滤文档.

日期计算还可以被应用到某个具体的时间,并非只能是一个像now这样的占位符.<br>
只要在某个日期后加上一个双管符号 (||) 并紧跟一个日期数学表达式就能做到:

*字符串中的时间格式一定要与文档保存的格式完全一致.*

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "range": {
      "创建时间": {
        "lt": "now-1h",
        "gt": "2014-01-01T00:00:00+08:00:00||+1M"
      }
    }
  }
}'
```

###### 字符串范围

> 数字和日期字段的索引方式使高效地范围计算成为可能.<br>
但字符串却并非如此,要想对其使用范围过滤,<br>
Elasticsearch实际上是在为范围内的每个词项都执行term过滤器,这会比日期或数字的范围过滤慢许多.<br>
字符串范围在过滤低基数(lowcardinality) 字段(即只有少量唯一词项)时可以正常工作,<br>
但是唯一词项越多,字符串范围的计算会越慢.

##### 精确值匹配(term)

> 当进行精确值查找时,会使用过滤器(filters).<br>
过滤器很重要,因为它们执行速度非常快,<br>
不会计算相关度(直接跳过了整个评分阶段)而且很容易被缓存.<br>
* *在进行精确值查找时,尽可能多的使用过滤式查询** .

* *term和terms是包含(contains)操作,而非等值(equals)判断.* *

term查询被用于精确值匹配,这些精确值可能是数字、时间、布尔或者那些 **not_analyzed** 的字符串:

```json
{"term": {"标签": "计算机硬件"}}
```

* *term查询对于输入的文本不分析 ,所以它将给定的值进行精确查询.* *

通常当查找一个精确值的时候,不希望对查询进行评分计算.<br>
只希望对文档进行包括或排除的计算,所以会使用constant_score查询以非评分模式来执行term查询并以一作为统一评分.

最终组合的结果是一个constant_score查询,它包含一个term查询:

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "constant_score": { 
      "filter": {
        "term": { 
          "标签": "计算机硬件"
        }
      }
    }
  }
}'
```

##### 多值匹配(terms)

terms查询和term查询一样,但它允许指定多值进行匹配.<br>
如果这个字段包含了指定值中的任何一个值,那么这个文档满足条件:


```json
{"terms": {"标签": [ "计算机硬件", "基金", "知乎日报" ] }}
```

##### 有值/无值查询(exists/missing)

exists查询和missing查询被用于查找那些指定字段中有值(exists)或无值(missing)的文档.<br>
这与SQL中的IS_NULL(missing)和NOTIS_NULL(exists)在本质上具有共性:

```json
{
  "exists": {
    "field": "标签"
  }
}

```

###### 对象上的存在与缺失

不仅可以过滤核心类型, exists andmissing查询还可以处理一个对象的内部字段.<br>
以下面文档为例:

```json
{
  "name": {
    "first": "John",
    "last": "Smith"
  }
}
```

不仅可以检查name.first和name.last的存在性,也可以检查name,不过在映射中,如上对象的内部是个扁平的字段与值(field-value)的简单键值结构,类似下面这样:

```json
{
  "name.first": "John",
  "name.last": "Smith"
}
```

那么如何用exists或missing查询name字段呢?name字段并不真实存在于倒排索引中.

原因是当执行下面这个过滤的时候:

```json
{
  "exists": {"field": "name"}
}
```

实际执行的是:

```json
{
  "bool": {
    "should": [
      {"exists": {"field": "name.first"}},
      {"exists": {"field": "name.last"}}
    ]
  }
}
```

这也就意味着,如果first和last都是空,那么name这个命名空间才会被认为不存在.

##### 增加语句权重(boost)

通过指定boost来控制任何查询语句的相对的权重,boost的默认值为1,大于1会提升一个语句的相对权重.

boost参数被用来提升一个语句的相对权重(boost值大于1)或降低相对权重(boost值处于0到1之间),<br>
但是这种提升或降低并不是线性的,换句话说,如果一个boost值为2,并不能获得两倍的评分_score.

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "bool": {
      "should": [
        {
          "match": {
            "标签": {
              "query":"计算机硬件",
              "boost": 3
            }
          }
        },
        {
          "match": {
           "标签": {
             "query":"基金",
              "boost": 2
           }
         }
        }
      ]
    }
  }
}'
```

boost值比较合理的区间处于1到10之间,当然也有可能是15.<br>
如果为boost指定比这更高的值,将不会对最终的评分结果产生更大影响,因为评分是被归一化的(normalized).

##### 游标查询(scroll)

scroll查询可以用来对Elasticsearch有效地执行大批量的文档查询,而又不用付出深度分页那种代价.

游标查询允许先做查询初始化,然后再批量地拉取结果.<br>
这有点儿像传统数据库中的cursor.

游标查询会取某个时间点的快照数据.<br>
查询初始化之后索引上的任何变化会被它忽略.<br>
它通过保存旧的数据文件来实现这个特性,结果就像保留初始化时的索引视图一样.

深度分页的代价根源是结果集全局排序,如果去掉全局排序的特性的话查询结果的成本就会很低.<br>
游标查询用字段_doc来排序.<br>
这个指令让Elasticsearch仅仅从还有结果的分片返回下一批结果.

启用游标查询可以通过在查询的时候设置参数scroll的值为期望的游标查询的过期时间.<br>
游标查询的过期时间会在每次做查询的时候刷新,所以这个时间只需要足够处理当前批的结果就可以了,而不是处理查询结果的所有文档的所需时间.<br>
这个过期时间的参数很重要,因为保持这个游标查询窗口需要消耗资源,所以期望如果不再需要维护这种资源就该早点儿释放掉.<br>
设置这个超时能够让Elasticsearch在稍后空闲的时候自动释放这部分资源.

```bash
# 保持游标查询窗口一分钟
# 关键字_doc是最有效的排序顺序
requestES GET 'link_articles/_doc/_search?scroll=1m&pretty' '
{
  "query": {"match_all": {}},
  "sort": ["_doc"], 
  "size": 1000
}'
```

这个查询的返回结果包括一个字段_scroll_id, 它是一个base64编码的长字符串 .<br>
现在能传递字段_scroll_id到_search/scroll查询接口获取下一批结果:

```bash
# 该接口未能成功调用
GET /_search/scroll
{
  "scroll_id": "DXF1ZXJ5QW5kRmV0Y2gBAAAAAAAAAD4WYm9laVYtZndUQlNsdDcwakFMNjU1QQ=="
}
```

##### 高亮搜索(highlight)

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": { 
    "match": {"名称": "基金"}
  },
  "highlight": {
    "fields": {
      "名称": {}
    }
  },
  "size": 1
}'
```

返回值

```json
// ...
"highlight": {
  "名称": [
    "新<em>基金</em> *热火朝天 *,老<em>基金</em>* 可遇不可求* "
  ]
}
// ...
```

##### 词频分析(aggs aggregations)

这里的词频是指文档中的出现频率,而不是指搜索频率.

text类型默认不能用于聚合,可在创建索引时添加fielddata属性:

```json
// ...
"名称": {
  // ...
  "fielddata":"true"
}
```

```bash
# 查询名称字段频率前100名的词汇
requestES GET 'link_articles/_doc/_search' '
{
  "size": 0,
  "aggs": {   
    "messages": {   
      "terms": {   
        "size": 100,
        "field": "名称"
      }  
    }  
  }
}
'
```

查询结果不是事先生成的,而是根据匹配当前查询的文档即时生成的.

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "match": {
      "名称": "基金"
    }
  },
  "size": 0,
  "aggs": {   
    "messages": {   
      "terms": {   
        "size": 100,
        "field": "名称"
      }  
    }  
  }
}
'
```

###### 分级汇总

目前没有对应的实际场景,教程示例:

```bash
GET /megacorp/employee/_search
{
  "aggs": {
    "all_interests": {
      "terms": {"field": "interests"},
      "aggs": {
        "avg_age": {
          "avg": {"field": "age"}
        }
      }
    }
  }
}
```

```json
// ...
"all_interests": {
  "buckets": [
  {
    "key": "music",
    "doc_count": 2,
    "avg_age": {
      "value": 28.5
    }
  },
  {
    "key": "forestry",
    "doc_count": 1,
    "avg_age": {
      "value": 35
    }
  },
  {
    "key": "sports",
    "doc_count": 1,
    "avg_age": {
      "value": 25
    }
  }
  ]
}
```


#### 组合多查询

##### bool查询

现实的查询需求从来都没有那么简单,<br>
它们需要在多个字段上查询多种多样的文本,并且根据一系列的标准来过滤.<br>
为了构建类似的高级查询,需要一种能够将多查询组合成单一查询的查询方法.

可以用bool查询来实现需求.<br>
这种查询将多查询组合在一起,成为用户自己想要的布尔查询.<br>
它接收以下参数:

参数|作用
-|-
must|文档必须匹配这些条件才能被包含进来.
must_not|文档必须不匹配这些条件才能被包含进来.<br>must_not语句不会影响评分,它的作用只是将不相关的文档排除.
should|如果满足这些语句中的任意语句,<br>将增加_score,否则,无任何影响.<br>它们主要用于修正每个文档的相关性得分.
filter|必须匹配,但它以不评分、过滤模式来进行.<br>这些语句对评分没有贡献,只是根据过滤标准来排除或包含文档.
minimum_should_match|控制需要匹配的should语句的数量,它既可以是一个绝对的数字,又可以是个百分比.

查找名称字段必须(must)匹配金融,
那些名称中包含A股或指数或者创建时间小于昨天的的文档,应该(should)比另外那些文档拥有更高的排名,
而且标签中一定不(must_not)包含计算机硬件:

```json
{
  "bool": {
    "must": {"match": {"名称": "金融"}},
    "should": [
      {"match": {"名称": "A股"}},
      {"match": {"名称": "指数"}},
      {"range": {"创建时间": {"lt": "now-1d/d"}}}
    ],
    "must_not": {"match": {"标签": "计算机硬件"}}
  }
}
```

如果没有must语句,那么至少需要能够匹配其中的一条should语句.<br>
但如果存在至少一条must语句,则对should语句的匹配没有要求.

###### 评分方式

bool查询采取more-matches-is-better匹配越多越好的方式,<br>
所以每条match语句的评分结果会被加在一起,从而为每个文档提供最终的分数_score.<br>
能与两条语句同时匹配的文档比只与一条语句匹配的文档得分要高.


```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "bool": {
      "must": {"match": {"名称": "金融"}},
      "should": [
        {"match": {"名称": "A股"}},
        {"match": {"名称": "指数"}},
        {"bool": {
          "should": [
            {"match": {"名称": "沪深"}}
          ]
        }}
      ],
      "must_not": {"match": {"标签": "计算机硬件"}},
      "filter": {"range": {"创建时间": {"lt": "now-1d/d"}}}
    }
  }
}'
```

为什么将匹配沪深的条件语句放入另一个独立的bool查询中呢?所有的三个match查询都是should语句,所以为什么不将沪深语句与其他语句放在同一层呢?

答案在于评分的计算方式.<br>
bool查询运行每个match查询,再把评分加在一起,然后将结果与所有匹配的语句数量相乘,最后除以所有的语句数量.<br>
处于同一层的每条语句具有相同的权重.<br>
在前面这个例子中,包含 *沪深* 语句的bool查询,只占总评分的三分之一.<br>
如果将 *沪深*语句与其他语句放入同一层,那么*A股*和*指数* 语句只贡献四分之一评分.



##### 避免因条件影响得分(filter)

如果不想因为文档的时间而影响得分,可以用filter语句来重写前面的例子:

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query": {
    "bool": {
      "must": {"match": {"名称": "金融"}},
      "should": [
        {"match": {"名称": "A股"}},
        {"match": {"名称": "指数"}}
      ],
      "must_not": {"match": {"标签": "计算机硬件"}},
      "filter": {"range": {"创建时间": {"lt": "now-1d/d"}}}
    }
  }
}'
```

通过将range查询移到filter语句中,将它转成不评分的查询,将不再影响文档的相关性排名.<br>
由于它现在是一个不评分的查询,可以使用各种对filter查询有效的优化手段来提升性能.

所有查询都可以借鉴这种方式.<br>
将查询移到bool查询的filter语句中,这样它就自动的转成一个不评分的filter了.

如果需要通过多个不同的标准来过滤文档,bool查询本身也可以被用做不评分的查询.<br>
简单地将它放置到filter语句中并在内部构建布尔逻辑:

```json
{
  "bool": {
    "must": {"match": {"名称": "金融"}},
    "should": [
      {"match": {"名称": "指数"}}
    ],
    "must_not": {"match": {"标签": "计算机硬件"}},
    "filter": {
      "bool": {
        "must": [
          {"match": {"名称": "A股"}},
          {"range": {"创建时间": {"lt": "now-1d/d"}}}
        ]
      }
    }
  }
}
```

##### constant_score查询

尽管没有bool查询使用这么频繁,constant_score查询也是工具箱里有用的查询工具.<br>
它将一个不变的常量评分应用于所有匹配的文档.<br>
它被经常用于只需要执行一个filter而没有其它查询(例如,评分查询)的情况下.

可以使用它来取代只有filter语句的bool查询.<br>
在性能上是完全相同的,但对于提高查询简洁性和清晰度有很大帮助.

```json
{
  "constant_score": {
    "filter": {
      "term": {"名称": "基金"} 
    }
  }
}
```
term查询被放置在constant_score中,转成不评分的filter.<br>
这种方式可以用来取代只有filter语句的bool查询.<br>

filter查询表明只希望获取相匹配的文档,并没有试图确定这些文档的相关性.<br>
实际上文档将按照随机顺序返回,并且每个文档都会评为零分.<br>
如果评分为零造成了困扰,可以使用constant_score查询进行替代.

##### 分离最大化查询(dis_max)

假设有个网站允许用户搜索博客的内容,以下面两篇博客内容文档为例:

```
PUT /my_index/my_type/1
{
    "title": "Quick brown rabbits",
    "body": "Brown rabbits are commonly seen."
}

PUT /my_index/my_type/2
{
    "title": "Keeping pets healthy",
    "body": "My quick brown fox eats rabbits on a regular basis."
}
```

用户输入词组 *Brown fox* 然后点击搜索按钮.<br>
事先,并不知道用户的搜索项是会在title还是在body字段中被找到,但是,用户很有可能是想搜索相关的词组.<br>
用肉眼判断,文档2的匹配度更高,因为它同时包括要查找的两个词:

现在运行以下bool查询:

```json
{
  "query": {
    "bool": {
      "should": [
        {"match": {"title": "Brown fox"}},
        {"match": {"body": "Brown fox"}}
      ]
    }
  }
}
```

但是发现查询的结果是文档1的评分更高:

```json
{
  "hits": [
     {
        "_id": "1",
        "_score": 0.14809652,
        "_source": {
           "title": "Quick brown rabbits",
           "body": "Brown rabbits are commonly seen."
        }
     },
     {
        "_id": "2",
        "_score": 0.09256032,
        "_source": {
           "title": "Keeping pets healthy",
           "body": "My quick brown fox eats rabbits on a regular basis."
        }
     }
  ]
}
```

为了理解导致这样的原因,需要回想一下bool是如何计算评分的:

0. 它会执行should语句中的两个查询.
0. 加和两个查询的评分.
0. 乘以匹配语句的总数.
0. 除以所有语句总数(这里为:2).

文档1的两个字段都包含brown这个词,所以两个match语句都能成功匹配并且有一个评分.<br>
文档2的body字段同时包含brown和fox这两个词,但title字段没有包含任何词.<br>
这样,body查询结果中的高分,加上title查询中的0分,然后乘以二分之一,就得到比文档1更低的整体评分.

在本例中,title和body字段是相互竞争的关系,所以就需要找到单个最佳匹配的字段.

如果不是简单将每个字段的评分结果加在一起,而是将 *最佳匹配字段的评分作为查询的整体评分* ,结果会怎样?<br>
这样返回的结果可能是: 同时包含brown和fox的单个字段比反复出现相同词语的多个不同字段有更高的相关度.

不使用bool查询,可以使用dis_max即分离最大化查询(Disjunction Max Query) .<br>
分离(Disjunction)的意思是或(or) ,<br>
这与可以把结合(conjunction)理解成与(and) 相对应.<br>
分离最大化查询(Disjunction Max Query)指的是: <br>
将任何与任一查询匹配的文档作为结果返回,但 *只将最佳匹配的评分作为查询的评分结果返回* .

```json
{
  "query": {
    "dis_max": {
      "queries": [
        {"match": {"title": "Brown fox"}},
        {"match": {"body": "Brown fox"}}
      ]
    }
  }
}
```

得到想要的结果为:

```json
{
  "hits": [
    {
      "_id": "2",
      "_score": 0.21509302,
      "_source": {
         "title": "Keeping pets healthy",
         "body": "My quick brown fox eats rabbits on a regular basis."
      }
    },
    {
      "_id": "1",
      "_score": 0.12713557,
      "_source": {
         "title": "Quick brown rabbits",
         "body": "Brown rabbits are commonly seen."
      }
    }
  ]
}
```

###### 考虑其他语句评分(tie_breaker)

当用户搜索 *quick pets* 时会发生什么呢?在前面的例子中,两个文档都包含词quick,但是只有文档2包含词pets,两个文档中都不具有同时包含两个词的相同字段 .

如下,一个简单的dis_max查询会采用单个最佳匹配字段,而忽略其他的匹配:

```json
{
  "query": {
    "dis_max": {
      "queries": [
        {"match": {"title": "Quick pets"}},
        {"match": {"body": "Quick pets"}}
      ]
    }
  }
}
```

```json
{
  "hits": [
     {
        "_id": "1",
        "_score": 0.12713557, 
        "_source": {
           "title": "Quick brown rabbits",
           "body": "Brown rabbits are commonly seen."
        }
     },
     {
        "_id": "2",
        "_score": 0.12713557, 
        "_source": {
           "title": "Keeping pets healthy",
           "body": "My quick brown fox eats rabbits on a regular basis."
        }
     }
   ]
}
```

注意两个评分是完全相同的.<br>
可能期望同时匹配title和body字段的文档比只与一个字段匹配的文档的相关度更高,但事实并非如此,因为dis_max查询只会简单地使用单个最佳匹配语句的评分_score作为整体评分.<br>
可以通过指定tie_breaker这个参数将其他匹配语句的评分也考虑其中:

```json
{
  "query": {
    "dis_max": {
      "queries": [
        {"match": {"title": "Quick pets"}},
        {"match": {"body": "Quick pets"}}
      ],
      "tie_breaker": 0.3
    }
  }
}
```

```json
{
  "hits": [
     {
        "_id": "2",
        "_score": 0.14757764, 
        "_source": {
           "title": "Keeping pets healthy",
           "body": "My quick brown fox eats rabbits on a regular basis."
        }
     },
     {
        "_id": "1",
        "_score": 0.124275915, 
        "_source": {
           "title": "Quick brown rabbits",
           "body": "Brown rabbits are commonly seen."
        }
     }
   ]
}
```

tie_breaker参数提供了一种dis_max和bool之间的折中选择,它的评分方式如下:

0. 获得最佳匹配语句的评分_score.
0. 将其他匹配语句的评分结果与tie_breaker相乘.
0. 对以上评分求和并规范化.

有了tie_breaker,会考虑所有匹配语句,但最佳匹配语句依然占最终结果里的很大一部分.

tie_breaker可以是0到1之间的浮点数,其中0代表使用dis_max最佳匹配语句的普通逻辑,1表示所有匹配语句同等重要.<br>
最佳的精确值需要根据数据与查询调试得出,但是合理值应该与零接近(处于0.1 - 0.4之间),这样就不会颠覆dis_max最佳匹配性质的根本.

### 排序(sort)

为了按照相关性来排序,需要将相关性表示为一个数值.<br>
在Elasticsearch中, 相关性得分由一个浮点数进行表示,并在搜索结果中通过_score参数返回, 默认排序是_score降序.<br>

有时,相关性评分对来说并没有意义,而是希望根据某个字段进行排序,<br>
可以使用sort参数进行实现:

```bash
# 默认升序
requestES GET 'link_articles/_doc/_search' '
{
  "query": {"match_all": {}},
  "sort": "创建时间"
}'

requestES GET 'link_articles/_doc/_search' '
{
  "query": {"match_all": {}},
  "sort": {"创建时间": {"order":"desc"}}
}'
```

#### 多级排序

匹配的结果首先按照日期排序,然后按照相关性排序:

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "query":{
    "bool": {
      "must": {"match": {"名称": "金融"}},
      "should": [
        {"match": {"名称": "A股"}},
        {"match": {"名称": "指数"}},
        {"range": {"创建时间": {"lt": "now-1d/d"}}}
      ],
      "must_not": {"match": {"标签": "计算机硬件"}}
    }
  },
  "sort": [
    {"创建时间": {"order": "asc"}},
    {"_score": {"order": "desc"}}
  ]
}'
```

#### 多值字段的排序

一种情形是字段有多个值的排序,需要记住这些值并没有固有的顺序,<br>
一个多值的字段仅仅是多个值的包装,这时应该选择哪个进行排序呢?

对于数字或日期,可以将多值字段减为单值,这可以通过使用min、max、avg或是sum排序模式 .<br>
例如可以按照每个date字段中的最早日期进行排序,通过以下方法:

```bash
requestES GET 'link_articles/_doc/_search' '
{
  "sort": {
    "创建时间": {
      "order": "desc",
      "mode": "min"
    }
  }
}'
```

#### 字符串排序与多字段

被解析的字符串字段也是多值字段, 但是很少会按照想要的方式进行排序.<br>
如果想分析一个字符串,如fine old art , 这包含3项.<br>
很可能想要按第一项的字母排序,然后按第二项的字母排序,诸如此类,但是Elasticsearch在排序过程中没有这样的信息.

可以使用min和max排序模式(默认是min),但是这会导致排序以art或是old,任何一个都不是所希望的.

为了以字符串字段进行排序,这个字段应仅包含一项: 整个not_analyzed字符串.<br>
但是仍需要analyzed字段,这样才能以全文进行查询

一个简单的方法是用两种方式对同一个字符串进行索引,这将在文档中包括两个字段:analyzed用于搜索,not_analyzed用于排序

但是保存相同的字符串两次在_source字段是浪费空间的.<br>
真正想要做的是传递一个单字段但是却用两种方式索引它.<br>
所有的_core_field类型 (strings, numbers, Booleans, dates) 接收一个fields参数

该参数允许转化一个简单的映射如:

```json
{
  "tweet": { 
    "type": "text",
    "analyzer": "english",
    "fields": {
      "raw": { 
        "type": "text",
        "index": "not_analyzed"
      }
    }
}
```
	
tweet主字段与之前的一样: 是一个analyzed全文字段.<br>
新的tweet.raw子字段是not_analyzed.

只要重新索引了数据,使用tweet字段用于搜索,tweet.raw字段用于排序:

```json
{
  "query": {
    "match": {
      "tweet": "elasticsearch"
    }
  },
  "sort": "tweet.raw"
}
```

### 相关性

默认情况下,返回结果是按相关性倒序排列的.<br>
但是什么是相关性? 相关性如何计算?

每个文档都有相关性评分,用一个正浮点数字段_score来表示 .<br>
_score的评分越高,相关性越高.

查询语句会为每个文档生成一个_score字段.<br>
评分的计算方式取决于查询类型不同的查询语句用于不同的目的:<br>
fuzzy查询会计算与关键词的拼写相似程度,<br>
terms查询会计算找到的内容与关键词组成部分匹配的百分比,<br>
但是通常说的relevance是用来计算全文本字段的值相对于全文本检索词相似程度的算法.<br>

#### 检索词频率/反向文档频率(TF/IDF)

Elasticsearch的相似度算法被定义为检索词频率/反向文档频率(TF/IDF),包括以下内容:

##### 检索词频率

检索词在该字段出现的频率?出现频率越高,相关性也越高.<br>
字段中出现过5次要比只出现过1次的相关性高.

##### 反向文档频率

每个检索词在索引中出现的频率?频率越高,相关性越低.<br>
检索词出现在多数文档中会比出现在少数文档中的权重更低.<br>

##### 字段长度准则

字段的长度是多少?长度越长,相关性越低.<br>
检索词出现在一个短的title要比同样的词出现在一个长的content字段权重更大.<br>

单个查询可以联合使用TF/IDF和其他方式,比如短语查询中检索词的距离或模糊查询里的检索词相似度.

相关性并不只是全文本检索的专利.<br>
也适用于yes|no的子句,匹配的子句越多,相关性评分越高.

如果多条查询子句被合并为一条复合查询语句,比如bool查询,则每个查询子句计算得出的评分会被合并到总的相关性评分中.

#### 理解评分标准

当调试一条复杂的查询语句时,想要理解_score究竟是如何计算是比较困难的.<br>
Elasticsearch在每个查询语句中都有一个explain参数,将explain设为true就可以得到更详细的信息.


```bash
requestES GET 'link_articles/_explain/f04c862a1465ccbdb228d858a57c82f5?pretty' '
{
  "query": {"match": {"名称": "基金"}}
}'
```

```json
{
  "_index": "link_articles",
  "_type": "_doc",
  "_id": "f04c862a1465ccbdb228d858a57c82f5",
  "matched": true,
  "explanation": {
    "value": 3.0644224,
    "description": "weight(名称:基金in0) [PerFieldSimilarity], result of:",
    "details": [
      {
        "value": 3.0644224,
        "description": "score(freq=1.0), computed as boost * idf * tf from:",
        "details": [
          {
            "value": 2.2,
            "description": "boost",
            "details": [ ]
          },
          {
            "value": 3.0598996,
            "description": "idf, computed as log(1 + (N - n + 0.5) / (n + 0.5)) from:",
            "details": [
              {
                "value": 210,
                "description": "n, number of documents containing term",
                "details": [ ]
              },
              {
                "value": 4488,
                "description": "N, total number ofdocumentswith field",
                "details": [ ]
              }
            ]
          },
          {
            "value": 0.4552173,
            "description": "tf, computed as freq / (freq + k1 * (1 - b + b * dl / avgdl)) from:",
            "details": [
              {
                "value": 1.0,
                "description": "freq, occurrences oftermwithin document",
                "details": [ ]
              },
              {
                "value": 1.2,
                "description": "k1, term saturation parameter",
                "details": [ ]
              },
              {
                "value": 0.75,
                "description": "b, length normalization parameter",
                "details": [ ]
              },
              {
                "value": 12.0,
                "description": "dl, length of field",
                "details": [ ]
              },
              {
                "value": 12.043449,
                "description": "avgdl, average length of field",
                "details": [ ]
              }
            ]
          }
        ]
      }
    ]
  }
}
```



## 验证查询(_validate)

查询可以变得非常的复杂,尤其和不同的分析器与不同的字段映射结合时,理解起来就有点困难了.<br>
不过validate-queryAPI可以用来验证查询是否合法.<br>

```bash
requestES GET 'link_articles/_doc/_validate/query' '
{
  "query": {
    "constant_score": {
      "filter": {
        "term": {"名称": "基金"} 
      }
    }
  }
}'
```

explain参数可以提供更多关于查询不合法的信息:

```bash
requestES GET 'link_articles/_doc/_validate/query?explain&pretty' '
{
  "query": {
    "constant_score": {
      "filter": {
        "bool": {"名称": "基金"} 
      }
    }
  }
}'
```

## (_get)
## (_index)
## 删除(_delete)

## 批量操作(_bulk)
## 更新(_update)
### 更新部分内容

```bash
requestES POST 'link_articles/_doc/f04c862a1465ccbdb228d858a57c82f5/_update' '
{
  "doc":{
    "名称": "基金买的越多,风险就越分散吗? 答案是不能"
  }
}
'

# 检查文档内容
requestES GET 'link_articles/_doc/f04c862a1465ccbdb228d858a57c82f5'
```

### 使用脚本更新

脚本可以在updateAPI中用来改变_source的字段内容, 它在更新脚本中称为ctx._source.

```bash
requestES POST 'link_articles/_doc/f04c862a1465ccbdb228d858a57c82f5/_update' "
{
  \"script\": {
    \"source\": \"ctx._source['名称']='基金买的越多,风险就越分散吗? 答案是不能!'\"
  }
}
"

requestES POST 'link_articles/_doc/f04c862a1465ccbdb228d858a57c82f5/_update' "
{
  \"script\": {
    \"source\": \"ctx._source['版本']+=1\"
  }
}
"
```

#### 更新的文档可能尚不存在

假设需要在Elasticsearch中存储一个页面访问量计数器.<br>
每当有用户浏览网页,对该页面的计数器进行累加.<br>
但是,如果它是一个新网页,不能确定计数器已经存在.<br>
如果尝试更新一个不存在的文档,那么更新操作将会失败.

在这样的情况下,可以使用upsert参数,指定如果文档不存在就应该先创建它:

TODO: 待实际操作

```bash
POST /website/pageviews/1/_update
{
   "script": "ctx._source.views+=1",
   "upsert": {
       "views": 1
   }
}
```

### 删除文档

#### 通过脚本

TODO: 待实际操作

```bash
POST /website/blog/1/_update
{
   "script": "ctx.op = ctx._source.views == count ? 'delete' : 'none'",
    "params": {
        "count": 1
    }
}
```

## (_mget)
## (_analyze)

### 测试分析器

#### ik_max_word

```bash
requestES GET '_analyze' '{
  "analyzer": "ik_max_word",
  "text": "什么是富时中国A50指数"
}'
```

```json
{
  "tokens": [
    {
      "token": "什么",
      "start_offset": 0,
      "end_offset": 2,
      "type": "CN_WORD",
      "position": 0
    },
    {
      "token": "是",
      "start_offset": 2,
      "end_offset": 3,
      "type": "CN_CHAR",
      "position": 1
    },
    {
      "token": "富",
      "start_offset": 3,
      "end_offset": 4,
      "type": "CN_CHAR",
      "position": 2
    },
    {
      "token": "时中",
      "start_offset": 4,
      "end_offset": 6,
      "type": "CN_WORD",
      "position": 3
    },
    {
      "token": "中国",
      "start_offset": 5,
      "end_offset": 7,
      "type": "CN_WORD",
      "position": 4
    },
        {
      "token": "a50",
      "start_offset": 7,
      "end_offset": 10,
      "type": "LETTER",
      "position": 5
    },
    {
      "token": "50",
      "start_offset": 8,
      "end_offset": 10,
      "type": "ARABIC",
      "position": 6
    },
    {
      "token": "指数",
      "start_offset": 10,
      "end_offset": 12,
      "type": "CN_WORD",
      "position": 7
    },
    {
      "token": "指",
      "start_offset": 10,
      "end_offset": 11,
      "type": "COUNT",
      "position": 8
    },
    {
      "token": "数",
      "start_offset": 11,
      "end_offset": 12,
      "type": "CN_CHAR",
      "position": 9
    }
  ]
}
```

#### ik_smart

```bash
requestES GET '_analyze' '{
  "analyzer": "ik_smart",
  "text": "什么是富时中国A50指数"
}'
```

```json
{
  "tokens": [
    {
      "token": "什么",
      "start_offset": 0,
      "end_offset": 2,
      "type": "CN_WORD",
      "position": 0
    },
    {
      "token": "是",
      "start_offset": 2,
      "end_offset": 3,
      "type": "CN_CHAR",
      "position": 1
    },
    {
      "token": "富",
      "start_offset": 3,
      "end_offset": 4,
      "type": "CN_CHAR",
      "position": 2
    },
    {
      "token": "时",
      "start_offset": 4,
      "end_offset": 5,
      "type": "CN_CHAR",
      "position": 3
    },
    {
      "token": "中国",
      "start_offset": 5,
      "end_offset": 7,
      "type": "CN_WORD",
      "position": 4
    },  {
      "token": "a50",
      "start_offset": 7,
      "end_offset": 10,
      "type": "LETTER",
      "position": 5
    },
    {
      "token": "指数",
      "start_offset": 10,
      "end_offset": 12,
      "type": "CN_WORD",
      "position": 6
    }
  ]
}
```

### 紧凑和对齐文本(_cat)

#### 查看节点状态

```sh
curl 127.0.0.1:9201/_cat/nodes?h=ip,port,heapPercent,name
```

#### 查看所有索引
 
```sh
curl 127.0.0.1:9201/_cat/indices

curl "127.0.0.1:9201/_cat/indices?bytes=b&s=store.size:desc,index:asc&v=true"
```

## 索引管理

### 删除索引

```bash
requestES DELETE 'link_articles'
```

删除多个索引:

```bash
requestES DELETE 'index_one,index_two'
requestES DELETE 'index_*'
```

删除全部索引:

```bash
requestES DELETE '_all'
requestES DELETE '*'
```

能够用单个命令来删除所有数据可能会导致可怕的后果.<br>
如果想要避免意外的大量删除,可以在elasticsearch.yml做如下配置:

```yml
action.destructive_requires_name: true
```

这个设置使删除只限于特定名称指向的数据, 而不允许通过指定_all或通配符来删除指定索引库.<br>
同样可以通过Cluster State API动态的更新这个设置.

### 索引设置(_settings)

Elasticsearch提供了优化好的默认配置.<br>
除非理解这些配置的作用并且知道为什么要去修改,否则不要随意修改.

下面是两个最重要的设置:

0. number_of_shards: 每个索引的主分片数,默认值是5.<br>这个配置在索引创建后不能修改.
0. number_of_replicas: 每个主分片的副本数,默认值是1.<br>对于活动的索引库,这个配置可以随时修改.

```bash
requestES DELETE 'my_temp_index'

requestES PUT 'my_temp_index' '
{
  "settings": {
    "number_of_shards": 1,
    "number_of_replicas": 0
  }
}'

```

#### 更新配置

```bash
requestES PUT 'my_temp_index/_settings' '
{
  "index": {
    "number_of_replicas": 2
  }
}'
```

### 配置分析器

```bash

```

### 自定义分析器

```bash

```

### 类型和映射

```bash

```

### 根对象

映射的最高一层被称为根对象 ,它可能包含下面几项:

0. 一个properties节点,列出了文档中可能包含的每个字段的映射
0. 各种元数据字段,它们都以一个下划线开头,例如_type、_id和_source
0. 设置项,控制如何动态处理新的字段,例如analyzer、dynamic_date_formats和dynamic_templates
0. 其他设置,可以同时应用在根对象和其他object类型的字段上,例如enabled、dynamic和include_in_all


#### properties

文档字段和属性的三个最重要的设置:

##### type 

字段的数据类型,例如string或date

##### index

字段是否应当被当成全文来搜索(analyzed),或被当成一个准确的值(not_analyzed),还是完全不可被搜索(no).

##### analyzer

确定在索引和搜索时全文字段使用的analyzer.

```bash

```

#### 元数据 

##### 文档内容(_source)

默认地,Elasticsearch在_source字段存储代表文档体的JSON字符串.<br>
和所有被存储的字段一样,_source字段在被写入磁盘之前先会被压缩.

这个字段的存储意味着下面的这些:

0. 搜索结果包括了整个可用的文档——不需要额外的从另一个的数据仓库来取文档.
0. 如果没有_source字段,部分update请求不会生效.
0. 当映射改变时,需要重新索引数据,有了_source字段可以直接从Elasticsearch这样做,而不必从另一个(通常是速度更慢的)数据仓库取回所有文档.
0. 当不需要看到整个文档时,单个字段可以从_source字段提取和通过get或者search请求返回.
0. 调试查询语句更加简单,因为可以直接看到每个文档包括什么,而不是从一列id猜测它们的内容.<br>

然而,存储_source字段的确要使用磁盘空间.<br>
如果上面的原因没有一个是重要的,可以用下面的映射禁用_source字段:

```bash
requestES DELETE 'my_temp_index'

requestES PUT 'my_temp_index' '
{
  "mappings": {
    "_source": {
      "enabled": false
    }
  }
}'
```

#### 动态映射

当Elasticsearch遇到文档中以前未遇到的字段,它用dynamicmapping来确定字段的数据类型并自动把新的字段添加到类型映射.

有时这是想要的行为有时又不希望这样.<br>
通常没有人知道以后会有什么新字段加到文档,但是又希望这些字段被自动的索引.<br>
也许只想忽略它们.<br>
如果Elasticsearch是作为重要的数据存储,可能就会期望遇到新字段就会抛出异常,这样能及时发现问题.

可以用dynamic配置来控制这种行为 ,可接受的选项如下:

0. true: 动态添加新的字段—缺省
0. false: 忽略新的字段
0. strict: 如果遇到新字段抛出异常

配置参数dynamic可以用在根object或任何object类型的字段上.<br>
可以将dynamic的默认值设置为strict,而只在指定的内部对象中开启它.

下面示例中,如果遇到新字段,对象_doc就会抛出异常.<br>
而内部对象stash遇到新字段就会动态创建新字段:

```bash
requestES DELETE 'my_temp_index'

requestES PUT 'my_temp_index' '
{
  "mappings": {
    "dynamic": "strict",
    "properties": {
      "title": {"type": "text"},
      "stash": {
        "type": "object",
        "dynamic": true 
      }
    }
  }
}'
```

#### 自定义动态映射

如果想在运行时增加新的字段,可能会启用动态映射.<br>
然而,有时候,动态映射规则可能不太智能.<br>
幸运的是,可以通过设置去自定义这些规则,以便更好的适用于数据.

##### 日期检测(date_detection)

当Elasticsearch遇到一个新的字符串字段时,它会检测这个字段是否包含一个可识别的日期,比如2014-01-01 .<br>
如果它像日期,这个字段就会被作为date类型添加.<br>
否则,它会被作为string类型添加.

有些时候这个行为可能导致一些问题.<br>
想象下,有如下这样的一个文档:

```json
{"note": "2014-01-01"}
```

假设这是第一次识别note字段,它会被添加为date字段.<br>
但是如果下一个文档像这样:

```json
{"note": "Logged out"}
```

这显然不是一个日期,但为时已晚.<br>
这个字段已经是一个日期类型,这个不合法的日期将会造成一个异常.

日期检测可以通过在根对象上设置date_detection为false来关闭:

```bash
requestES DELETE 'my_temp_index'

requestES PUT 'my_temp_index' '
{
  "mappings": {
    "date_detection": false
  }
}'
```

使用这个映射,字符串将始终作为string类型.<br>
如果需要一个date字段,必须手动添加.

Elasticsearch判断字符串为日期的规则可以通过dynamic_date_formats setting来设置.

### 动态模板(dynamic_templates)

使用dynamic_templates,可以完全控制新检测生成字段的映射.<br>
甚至可以通过字段名称或数据类型来应用不同的映射.

每个模板都有一个名称,可以用来描述这个模板的用途,<br>
一个mapping来指定映射应该怎样使用,<br>
以及至少一个参数(如match)来定义这个模板适用于哪个字段.

模板按照顺序来检测,第一个匹配的模板会被启用.<br>
例如,给string类型字段定义两个模板:

es :以_es结尾的字段名需要使用spanish分词器.<br>

en :所有其他字段使用english分词器.<br>

将es模板放在第一位,因为它比匹配所有字符串字段的en模板更特殊:

```bash
requestES DELETE 'my_temp_index'

requestES PUT 'my_temp_index' '
{
  "mappings": {
    "dynamic_templates": [
      {"es": {
        "match": "*_es", 
        "match_mapping_type": "string",
        "mapping": {
          "type": "text",
          "analyzer": "spanish"
        }
      }},
      {"en": {
        "match": "*", 
        "match_mapping_type": "string",
        "mapping": {
          "type": "text",
          "analyzer": "english"
        }
      }}
    ]
  }
}'
```

match_mapping_type允许应用模板到特定类型的字段上,<br>
就像有标准动态映射规则检测的一样,(例如string或long).

*match参数只匹配字段名称*,<br>
path_match参数匹配字段在对象上的完整路径,<br>
所以address.*.name将匹配这样的字段:

```json
{
  "address": {
    "city": {
      "name": "NewYork"
    }
  }
}
```

unmatch和path_unmatch将被用于未被匹配的字段.

### 重新索引(_reindex)

将文档从源复制到目标.

源和目标可以是任何预先存在的索引,索引别名或数据流.<br>
但是,源和目的地必须不同.<br>
例如,不能将数据流重新索引到自身中.

需要_source为源中的所有文档启用重新索引.

在调用之前,应根据需要配置目标_reindex.<br>
Reindex不会从源或其关联的模板复制设置(在7.11版本实际使用中发现并不需要配置新索引).



映射,分片计数,副本等必须预先配置.

```bash
requestES POST '_reindex' '
{
  "source": {
    "index": "link_articles"
  },
  "dest": {
    "index": "unite_20210302"
  }
}'

requestES 'unite_20210302/_doc/_search'
```

### 索引别名和零停机

> 即使现在的索引设计已经很完美了,在生产环境中,还是有可能需要做一些修改的.<br>
在应用中使用别名而不是索引名.然后就可以在任何时候重建索引.<br>
别名的开销很小,应该广泛使用.

在前面提到的,重建索引的问题是必须更新应用中的索引名称.<br>
索引别名就是用来解决这个问题的!

索引别名就像一个快捷方式或软连接,可以指向一个或多个索引,<br>
也可以给任何一个需要索引名的API来使用.<br>
别名带给极大的灵活性,允许做下面这些:

在运行的集群中可以无缝的从一个索引切换到另一个索引.
现在,将解释怎样使用别名在零停机下从旧索引切换到新索引.

有两种方式管理别名:_alias用于单个操作,_aliases用于执行多个原子级操作.

#### 单个别名操作(_alias)

假设应用有一个叫standard_structure的索引.<br>
事实上,standard_structure是一个指向当前真实索引的别名.<br>
真实索引包含一个版本号: unite_20210302 ,unite_20210320等等.

首先,创建索引unite_20210302,然后将别名standard_structure指向它:

```bash
requestES DELETE 'unite_20210302'
requestES PUT 'unite_20210302'

# 设置别名standard_structure指向unite_20210302
requestES PUT 'unite_20210302/_alias/standard_structure'

# 查找别名指向哪个索引
requestES GET '*/_alias/standard_structure'

# 查找索引指向哪个别名
requestES GET 'unite_20210302/_alias/*'
```

#### 原子级别名操作(_aliases)

修改索引中一个字段的映射.<br>
不能修改现存的映射,必须重新索引数据.<br>
首先, 用新映射创建索引unite_20210320:

```bash
requestES PUT 'unite_20210320' '
{
  "mappings":{
    "properties": {
      "tags": {
        "type": "text",
        "index": false
      }
    }
  }
}'
```

然后将数据从unite_20210302索引到unite_20210320,下面的过程在重新索引数据中已经描述过.<br>
一旦确定文档已经被正确地重索引了,就将别名指向新的索引.

一个别名可以指向多个索引,所以在添加别名到新索引的同时必须从旧的索引中删除它.<br>
这个操作需要原子化,这意味着需要使用_aliases操作:

```bash
requestES POST '_aliases' '
{
  "actions": [
    {"remove": {"index": "unite_20210302", "alias": "standard_structure"}},
    {"add": {"index": "unite_20210320", "alias": "standard_structure"}}
  ]
}'
```

应用已经在零停机的情况下从旧索引迁移到新索引了.

# 聚合

聚合允许向数据提出一些复杂的问题.<br>
虽然功能完全不同于搜索,但它使用相同的数据结构.<br>
这意味着聚合的执行速度很快并且就像搜索一样几乎是实时的.

这对报告和仪表盘是非常强大的.<br>
可以实时显示数据,让立即回应,而不是对数据进行汇总(需要一周时间去运行的Hadoop任务),<br>
报告随着数据变化而变化,而不是预先计算的、过时的和不相关的.

最后,聚合和搜索是一起的.<br>
这意味着可以在单个请求里同时对相同的数据进行搜索/过滤和分析.<br>
并且由于聚合是在用户搜索的上下文里计算的,不只是显示四星酒店的数量,而是显示匹配查询条件的四星酒店的数量.

聚合是这样一种功能特性:一旦开始使用它,就能找到很多其他的可用场景.<br>
实时报表与分析对于很多组织来说都是核心功能(无论是应用于商业智能还是服务器日志).<br>


聚合是如此强大以至于许多公司已经专门为数据分析建立了大型Elasticsearch集群.

## 桶(Buckets)与指标(Metrics)

类似于DSL查询表达式,聚合也有可组合的语法:独立单元的功能可以被混合起来提供需要的自定义行为.<br>
这意味着只需要学习很少的基本概念,就可以得到几乎无尽的组合.

要掌握聚合,只需要明白两个主要的概念:

0. 桶(Buckets):满足特定条件的文档的集合.
0. 指标(Metrics):对桶内的文档进行统计计算.

这就是全部了!每个聚合都是一个或者多个桶和零个或者多个指标的组合.<br>
翻译成SQL语句就是:

```sql
SELECT COUNT(color) 
FROM table
GROUP BY color 
```

COUNT(color) 相当于指标.
GROUP BYcolor相当于桶.

桶在概念上类似于SQL的分组(GROUP BY),而指标则类似于COUNT() 、SUM() 、MAX() 等统计方法.<br>

### 桶

> *桶* 就是基于条件来划分文档.<br>
Elasticsearch里面桶的叫法和SQL里面分组的概念是类似的,<br>
一个桶就类似SQL里面的一个group,<br>
多级嵌套的aggregation, 类似SQL里面的多字段分组(group by field1,field2, …​..)(这里仅仅是概念类似,底层的实现原理是不一样的).


桶简单来说就是满足特定条件的文档的集合:

0. 一个雇员属于 *男性* 桶或者 *女性* 桶
0. 奥尔巴尼属于 *纽约* 桶
0. 日期2014-10-28属于 *十月* 桶

当聚合开始被执行,每个文档里面的值通过计算来决定符合哪个桶的条件.<br>
如果匹配到,文档将放入相应的桶并接着进行聚合操作.

桶也可以被嵌套在其他桶里面,提供层次化的或者有条件的划分方案.<br>
例如,辛辛那提会被放入俄亥俄州这个桶,而整个俄亥俄州桶会被放入美国这个桶.

Elasticsearch有很多种类型的桶,<br>
能让通过很多种方式来划分文档(时间、最受欢迎的词、年龄区间、地理位置等等).<br>
其实根本上都是通过同样的原理进行操作: **基于条件来划分文档** .

### 指标(Metrics)

桶能划分文档到有意义的集合,但是最终需要的是对这些桶内的文档进行一些指标的计算.<br>
分桶是一种达到目的的手段:它提供了一种给文档分组的方法来让可以计算感兴趣的指标.

大多数指标是简单的数学运算(例如最小值、平均值、最大值,还有汇总),这些是通过文档的值来计算.

### 桶和指标的组合

聚合是由桶和指标组成的.<br>
聚合可能只有一个桶,可能只有一个指标,或者可能两个都有.<br>
也有可能有一些桶嵌套在其他桶里面.<br>
例如,可以通过所属国家来划分文档(桶),然后计算每个国家的平均薪酬(指标).

由于桶可以被嵌套,可以实现非常多并且非常复杂的聚合:

0. 通过国家划分文档(桶)
0. 然后通过性别划分每个国家(桶)
0. 然后通过年龄区间划分每种性别(桶)
0. 最后,为每个年龄区间计算平均薪酬(指标)

最后将告诉每个 <国家, 性别, 年龄> 组合的平均薪酬.<br>
所有的这些都在一个请求内完成并且只遍历一次数据!

## 基础数据准备

> 聚合相关内容将根据 *商品库* 索引进行操作.<br>
TODO: 中文索引名存在乱码无法正常显示.

基础数据:

```ruby
file = File.open('tmp/商品数据.txt', 'w')
emalls = ['天猫','淘宝','京东','亚马逊中国','苏宁']
Product.includes(:emall, :catalog, :brand).find_eachdo|product|
 catalogs = product.catalog&.way_names.split('-')
 created_at = rand(365*3).days.ago(Date.today).strftime("%Y-%m-%dT#{"%02d" % rand(24)}:#{"%02d" % rand(60)}:#{"%02d" % rand(60)}+08:00:00")

  file.write({
    "名称": product.name,
    "一级品目": catalogs[0],
    "二级品目": catalogs[1],
    "三级品目": catalogs[2],
    "型号": product.model,
    "品牌": product.brand&.name,
    "价格": product.price == 0 ? rand(9999) : product.price,
    "供应商": emalls.sample,
    "状态": ["上架","下架"].sample,
    "唯一编码": Digest::MD5.hexdigest("#{product.id}#{product.sku}#{rand(999999)}"),
    "累计销量": rand(999999),
    "创建时间": created_at
  }.to_json+"\n")

end
file.close
```

### 中文模式

索引准备:

```bash
requestES DELETE '商品库_20210324'
requestES PUT '商品库_20210324'
requestES PUT '商品库_20210324/_alias/products'
```

类型映射:

```bash
requestES PUT '商品库_20210324/_mapping' '
{
  "properties": {
    "名称": {
      "type": "text",
      "analyzer": "ik_max_word",
      "search_analyzer": "ik_max_word",
      "fielddata":"true",
      "fields": {
        "完型": { 
          "type": "keyword"
        }
      }
    },
    "一级品目": {
      "type": "keyword"
    },
    "二级品目": {
      "type": "keyword"
    },
    "三级品目": {
      "type": "keyword"
    },
    "型号": {
      "type": "text",
      "analyzer": "ik_smart",
      "search_analyzer": "ik_smart",
      "fielddata":"true",
      "fields": {
        "完型": { 
          "type": "keyword"
        }
      }
    },
    "品牌": {
      "type": "text",
      "analyzer": "ik_smart",
      "search_analyzer": "ik_smart",
      "fielddata":"true",
      "fields": {
        "完型": { 
          "type": "keyword"
        }
      }
    },
    "价格": {
      "type": "double"
    },
    "供应商": {
      "type": "keyword"
    },
    "状态": {
      "type": "keyword"
    },
    "唯一编码": {
      "type": "text"
    },
    "创建时间": {
      "type": "date"
    }
  } 
}'
```


文档导入:

```bash
while IFS= read -r line; do
  md5=$(echo -n "$line" |md5sum)
  requestES POST "products/_doc/${md5:0:32}" "$line"
done < /home/ff4c00/Space/知识体系/应用科学/计算机科学/理论计算机科学/数据结构与算法/商品库_20210324.txt

requestES GET 'products/_doc/_search'
```

### 英文模式

索引准备:

```bash
requestES DELETE 'products_20210324'
requestES PUT 'products_20210324'
requestES PUT 'products_20210324/_alias/products'
```

类型映射:

```bash
requestES PUT 'products_20210324/_mapping' '
{
  "properties": {
    "name": {
      "type": "text",
      "analyzer": "ik_max_word",
      "search_analyzer": "ik_max_word",
      "fielddata":"true"
    },
    "catalog_1": {
      "type": "text",
      "analyzer": "ik_smart",
      "search_analyzer": "ik_smart",
      "fielddata":"true"
    },
    "catalog_2": {
      "type": "text",
      "analyzer": "ik_smart",
      "search_analyzer": "ik_smart",
      "fielddata":"true"
    },
    "catalog_3": {
      "type": "text",
      "analyzer": "ik_smart",
      "search_analyzer": "ik_smart",
      "fielddata":"true"
    },
    "model": {
      "type": "text",
      "fielddata":"true"
    },
    "brand_name": {
      "type": "text",
      "analyzer": "ik_smart",
      "search_analyzer": "ik_smart",
      "fielddata":"true"
    },
    "price": {
      "type": "double"
    },
    "emall_name": {
      "type": "text",
      "analyzer": "ik_smart",
      "search_analyzer": "ik_smart",
      "fielddata":"true",
      "fields": {
        "完型": { 
          "type": "keyword"
        }
      }
    },
    "state": {
      "type": "text",
      "fielddata":"true"
    },
    "sku": {
      "type": "text"
    },
    "created_at": {
      "type": "date"
    }
  } 
}'
```


文档导入:

```bash
while IFS= read -r line; do
  md5=$(echo -n "$line" |md5sum)
  requestES POST "products/_doc/${md5:0:32}" "$line"
done < /home/ff4c00/Space/知识体系/应用科学/计算机科学/理论计算机科学/数据结构与算法/products_20210324.txt

requestES GET 'products/_doc/_search'
```

## 为聚合指定名称

根据供应商名称分桶:

```bash
requestES GET 'products/_doc/_search' '
{
  "size": 0,
  "aggs": {   
    "根据供应商分桶": {   
      "terms": {
        "field": "供应商"
      }
    }  
  }
}'
```

### 关于聚合字段内容分词问题

聚合计算了每个词的数目.<br>
背后的原因很简单:聚合是基于倒排索引创建的,倒排索引是后置分析(post-analysis)的.<br>


当把这些文档加入到Elasticsearch中时,字符串 "New York" 被分析/分析成 ["new", "york"] .<br>
这些单独的tokens,都被用来填充聚合计数,所以最终看到new的数量而不是NewYork .<br>


```json
"供应商": {
  "type": "text",
  "analyzer": "ik_smart",
  "search_analyzer": "ik_smart",
  "fielddata":"true",
  "fields": {
    "完型": { 
      "type": "keyword"
    }
  }
},
```

```json
"aggs": {   
  "根据供应商分桶": {   
    "terms": {
      "field": "供应商.完型"
    }
  }
```

## 量度聚合(待重构)

### 平均值聚合(Avg Aggregation)

### 近似聚合

如果所有的数据都在一台机器上,那么生活会容易许多.<br>
CS201课上教的经典算法就足够应付这些问题.<br>
如果所有的数据都在一台机器上,那么也就不需要像Elasticsearch这样的分布式软件了.<br>
不过一旦开始分布式存储数据,就需要小心地选择算法.

有些算法可以分布执行,到目前为止讨论过的所有聚合都是单次请求获得精确结果的.<br>
这些类型的算法通常被认为是高度并行的 ,因为它们无须任何额外代价,就能在多台机器上并行执行.<br>
比如当计算max度量时,以下的算法就非常简单:

0. 把请求广播到所有分片.
查看每个文档的price字段.<br>
0. 如果price > current_max,将current_max替换成price.
0. 返回所有分片的最大price并传给协调节点.
0. 找到从所有分片返回的最大price.这是最终的最大值.

这个算法可以随着机器数的线性增长而横向扩展,无须任何协调操作(机器之间不需要讨论中间结果),而且内存消耗很小(一个整型就能代表最大值).

不幸的是,不是所有的算法都像获取最大值这样简单.<br>
更加复杂的操作则需要在算法的性能和内存使用上做出权衡.<br>
对于这个问题,有个三角因子模型:大数据、精确性和实时性.

需要选择其中两项:

0. 精确 + 实时: 数据可以存入单台机器的内存之中,可以随心所欲,使用任何想用的算法.<br>
结果会100% 精确,响应会相对快速.
0. 大数据 + 精确: 传统的Hadoop.可以处理PB级的数据并且为提供精确的答案,但它可能需要几周的时间才能为提供这个答案.
0. 大数据 + 实时: 近似算法为提供准确但不精确的结果.

Elasticsearch目前支持两种近似算法(cardinality和percentiles).<br>
它们会提供准确但不是100% 精确的结果.<br>
以牺牲一点小小的估算错误为代价,这些算法可以为换来高速的执行效率和极小的内存消耗.

对于大多数应用领域,能够实时返回高度准确的结果要比100% 精确结果重要得多.<br>
乍一看这可能是天方夜谭.<br>
但仔细考虑0.5% 误差所带来的影响:

0. 99% 的网站延时都在132ms以下.
0. 0.5% 的误差对以上延时的影响在正负0.66ms.
0. 近似计算的结果会在毫秒内返回,而 *完全正确* 的结果就可能需要几秒,甚至无法返回.

只要简单的查看网站的延时情况,难道会在意近似结果是132.66ms而不是132ms吗?<br>
当然,不是所有的领域都能容忍这种近似结果,但对于绝大多数来说是没有问题的.<br>
接受近似结果更多的是一种文化观念上的壁垒而不是商业或技术上的需要.


#### 基数聚合(Cardinality Aggregation)

它提供一个字段的基数,即该字段的distinct或者unique值的数目.<br>
展示为SQL即:

```sql
SELECT COUNT(DISTINCT color) FROM cars;
```

```bash
requestES GET 'products/_doc/_search' '
{
  "aggs": {   
    "根据去重后的供应商分桶": {   
      "cardinality": {
        "field": "供应商"
      }
    }  
  }
}'
```


```bash
requestES GET 'products/_doc/_search' '
{
  "query": {
    "bool": {
      "filter": {
        "match": {"三级品目": "硒鼓"}
      }
    }
  },
  "size": 0,
  "aggs": {
    "根据创建时间分桶(月)": {   
      "date_histogram":{ 
        "field": "创建时间",
        "interval": "month",
        "min_doc_count": 0,
        "format": "yyyy-MM-dd"
      },
      "aggs": {
        "每月硒鼓品目下新增商品数量指标": {
          "cardinality": {
            "field": "三级品目"
          }
        }
      }
    }
  }
}'
```

##### 精确度(precision_threshold)

cardinality度量是一个近似算法.<br>
 它是基于HyperLogLog++ (HLL)算法的.<br>
HLL会先对输入作哈希运算,然后根据哈希运算的结果中的bits做概率估算从而得到基数.

这个算法的特性 :

0. 可配置的精度,用来控制内存的使用(更精确＝更多内存).
0. 小的数据集精度是非常高的.
0. 可以通过配置参数,来设置去重需要的固定内存使用量.无论数千还是数十亿的唯一值,内存使用量只与配置的精确度相关.

要配置精度,必须指定precision_threshold参数的值.<br>
这个阈值定义了在何种基数水平下希望得到一个近乎精确的结果.

precision_threshold接受0–40000之间的数字,更大的值还是会被当作40000来处理.


```bash
requestES GET 'products/_doc/_search' '
{
  "size": 0,
  "aggs": {   
    "根据去重后的供应商分桶": {   
      "cardinality": {
        "field": "供应商",
        "precision_threshold": 100
      }
    }  
  }
}'
```

precision_threshold=100时会确保当字段唯一值在100以内时会得到非常准确的结果.<br>
尽管算法是无法保证这点的,但如果基数在阈值以下,几乎总是100% 正确的.<br>
高于阈值的基数会开始节省内存而牺牲准确度,同时也会对度量结果带入误差.

对于指定的阈值,HLL的数据结构会大概使用precision_threshold*8字节的内存,所以就必须在牺牲内存和获得额外的准确度间做平衡.

在实际应用中,100的阈值可以在唯一值为百万的情况下仍然将误差维持5% 以内.

##### 速度优化

如果想要获得唯一值的数目, 通常需要查询整个数据集合(或几乎所有数据).<br>
所有基于所有数据的操作都必须迅速,原因是显然的.<br>
HyperLogLog的速度已经很快了,它只是简单的对数据做哈希以及一些位操作.

但如果速度对至关重要,可以做进一步的优化.<br>
因为HLL只需要字段内容的哈希值,可以在索引时就预先计算好.<br>
就能在查询时跳过哈希计算然后将哈希值从fielddata直接加载出来.


预先计算哈希值只对内容很长或者基数很高的字段有用,计算这些字段的哈希值的消耗在查询时是无法忽略的.

尽管数值字段的哈希计算是非常快速的,存储它们的原始值通常需要同样(或更少)的内存空间.<br>
这对低基数的字符串字段同样适用,Elasticsearch的内部优化能够保证每个唯一值只计算一次哈希.

基本上说,预先计算并不能保证所有的字段都更快,它 *只对那些具有高基数和/或者内容很长的字符串字段有作用* .<br>
需要记住的是,预计算只是简单的将查询消耗的时间提前转移到索引时,<br>
并非没有任何代价,区别在于可以选择在什么时候做这件事,要么在索引时,要么在查询时.<br>

要想这么做,需要为数据增加一个新的多值字段.<br>
先删除索引,再增加一个包括哈希值字段的映射,然后重新索引:

```json
// ...
  "三级品目": {
    "type": "keyword",
    "fields": {
      "哈希值": {
        "type": "murmur3" 
      }
    }
  },
// ...
```

```json
// ...
  "每月硒鼓品目下新增商品数量指标": {
    "cardinality": {
      "field": "三级品目.哈希值"
    }
  }
// ...
```

现在cardinality度量会读取 *三级品目.哈希值* 里的值(预先计算的哈希值),取代动态计算原始值的哈希.

单个文档节省的时间是非常少的,但是如果聚合一亿数据,每个字段多花费10纳秒的时间,<br>
那么在每次查询时都会额外增加1秒,<br>
如果要在非常大量的数据里面使用cardinality,<br>
可以权衡使用预计算的意义,是否需要提前计算hash,从而在查询时获得更好的性能,<br>
做一些性能测试来检验预计算哈希是否适用于应用场景.

#### 百分位计算(Percentiles Aggregation)

百分位数通常用来找出异常.<br>
在(统计学)的正态分布下,第0.13和第99.87的百分位数代表与均值距离三倍标准差的值.<br>
任何处于三倍标准差之外的数据通常被认为是不寻常的,因为它与平均值相差太大.

目前针对百分位的理解尚无对应的实际应用场景.

### 扩展统计聚合( Extended Stats Aggregation)

### 地理边界聚合(Geo Bounds Aggregation)

### 地理重心聚合(Geo Centroid Aggregation)

### 最大值聚合(Max Aggregation)

### 最小值聚合(Min Aggregation)

### Percentile Ranks Aggregation

### Scripted Metric Aggregation

### Stats Aggregation

### 总和聚合(Sum Aggregation)

### Top hits Aggregation

### Value Count Aggregation

## 添加度量指标

以供应商名称分桶并添加商品价格指标:

```bash
requestES GET 'products/_doc/_search' '
{
  "size": 0,
  "aggs": {   
    "根据供应商分桶": {   
      "terms": {
        "field": "供应商"
      },
      "aggs": { 
        "商品平均价格指标": { 
          "avg": {
            "field": "价格" 
          }
        }
      }
    }  
  }
}'
```

## 嵌套桶

将桶嵌套进另外一个桶所能得到的结果

```bash
requestES GET 'products/_doc/_search' '
{
  "size": 0,
  "aggs": {   
    "根据供应商分桶": {   
      "terms": {
        "field": "供应商"
      },
      "aggs": { 
        "商品平均价格指标": { 
          "avg": {"field": "价格"}
        },
        "商品最低价格指标": { 
          "min": {"field": "价格"}
        },      
        "商品最高价格指标": { 
          "max": {"field": "价格"}
        },
        "供应商三级品目商品数量指标(排名前五)":{
          "terms": {
            "field": "三级品目",
            "size": 5
          },
          "aggs": {
            "品目内商品平均价格指标": { 
              "avg": {"field": "价格"}
            },
            "品目内商品最低价格指标": { 
              "min": {"field": "价格"}
            },      
            "品目内商品最高价格指标": { 
              "max": {"field": "价格"}
            }
          }
        }
      }
    }  
  }
}'
```

## 条形图(histogram)

```bash
requestES GET 'products/_doc/_search' '
{
  "size": 0,
  "aggs": {
    "根据价格分桶(间隔1000)": {   
      "histogram":{ 
        "field": "价格",
        "interval": 1000,
        "min_doc_count": 1
      },
      "aggs": { 
        "商品平均价指标": { 
          "avg": {
            "field": "价格" 
          }
        }
      }
    }  
  }
}'
```

## 按时间统计(date_histogram)


```bash
requestES GET 'products/_doc/_search' '
{
  "size": 0,
  "aggs": {
    "根据创建时间分桶(月)": {   
      "date_histogram":{ 
        "field": "创建时间",
        "interval": "month",
        "min_doc_count": 1,
        "format": "yyyy-MM-dd"
      }
    },
    "根据创建时间分桶(年)": {
      "date_histogram":{
        "field": "创建时间",
        "interval": "year",
        "min_doc_count": 1,
        "format": "yyyy"
      }
    } 
  }
}'
```

## 关于范围扩展(extended_bounds)

> **范围扩展无法起到限制/过滤结果的作用** .<br>
例如按年根据创建时间统计,结果时间范围为: 2018-2021,<br>
如果设置为: "extended_bounds": {"min": "2011","max": "2019"},<br>
则会显示2011-2021年的统计结果,目的在于防止因为缺失某一区间的数据,<br>
则跳过不返回该段的结果,确保直方图或折线图的完整性.

```bash
requestES GET 'products/_doc/_search' '
{
  "size": 0,
  "aggs": {
    "根据创建时间分桶": {
      "date_histogram":{
        "field": "创建时间",
        "interval": "year",
        "min_doc_count": 0,
        "format": "yyyy",
        "extended_bounds": {
          "min": "2011",
          "max": "2017"
        }
      }
    }  
  }
}'
```

## 范围限定的聚合

> **Elasticsearch认为 *没有指定查询* 和 *查询所有文档* 是等价的** .

聚合可以与搜索请求同时执行,需要理解一个概念: 范围.<br>
默认情况下,聚合与查询是对同一范围进行操作的,<br>
也就是说, *聚合是基于查询匹配的文档集合进行计算的* .<br>

将 "size": 0,参数去掉可以看到搜索结果返回了结果集和聚合集.

```bash
requestES GET 'products/_doc/_search' '
{
  "aggs": {
    "根据创建时间分桶": {
      "date_histogram":{
        "field": "创建时间",
        "interval": "year",
        "min_doc_count": 0,
        "format": "yyyy"
      }
    }  
  }
}'
```

```bash
requestES GET 'products/_doc/_search' '
{
  "query": {
    "range": {
      "创建时间": {
        "gte": "2018-01-01T00:00:00+08:00:00",
        "lte": "2018-12-31T23:59:59+08:00:00"
      }
    }
  },
  "size": 0,
  "aggs": {
    "根据创建时间分桶(2018年/季度)": {
      "date_histogram":{
        "field": "创建时间",
        "interval": "quarter",
        "min_doc_count": 1,
        "format": "yyyy-MM"
      }
    } 
  }
}'
```

### 全局桶

通常希望聚合是在查询范围内的,但有时也想要搜索它的子集,而聚合的对象却是所有数据.<br>
全局桶包含所有的文档,它无视查询的范围.

```bash
requestES GET 'products/_doc/_search' '
{
  "query": {
    "range": {
      "创建时间": {
        "gte": "2018-01-01T00:00:00+08:00:00",
        "lte": "2018-12-31T23:59:59+08:00:00"
      }
    }
  },
  "size": 0,
  "aggs": {
    "根据创建时间分桶(2018年/季度)": {
      "date_histogram":{
        "field": "创建时间",
        "interval": "quarter",
        "min_doc_count": 1,
        "format": "yyyy-MM"
      }
    },
    "全局桶": {
      "global": {},
      "aggs": {
        "根据创建时间分桶(季度)": {
          "date_histogram":{
            "field": "创建时间",
            "interval": "quarter",
            "min_doc_count": 1,
            "format": "yyyy-MM"
          }
        }
      }
    } 
  }
}'
```

## 过滤桶(filter)

指定一个过滤桶(filter),当文档满足过滤桶的条件时,将其加入到桶内.<br>
filter桶和其他桶的操作方式一样,所以可以随意将其他桶和度量嵌入其中.<br>
所有嵌套的组件都会 *继承* 这个过滤,这使可以按需针对聚合过滤出选择部分.


```bash
requestES GET 'products/_doc/_search' '
{
  "query": {
    "range": {
      "创建时间": {
        "gte": "2018-01-01T00:00:00+08:00:00",
        "lte": "2018-12-31T23:59:59+08:00:00"
      }
    }
  },
  "size": 0,
  "aggs": {
    "过滤供应商来自京东的分桶": {
      "filter": { 
        "match": {
          "供应商": "京东"
        }
      },
      "aggs": {
        "查询范围内京东商品的创建时间": {
          "date_histogram":{
            "field": "创建时间",
            "interval": "quarter",
            "min_doc_count": 1,
            "format": "yyyy-MM"
          }
        }
      }
    },
    "过滤供应商来自淘宝的分桶": {
      "filter": { 
        "match": {
          "供应商": "淘宝"
        }
      },
      "aggs": {
        "淘宝商品平均价格指标": { 
          "avg": {"field": "价格"}
        },
        "淘宝商品最低价格指标": { 
          "min": {"field": "价格"}
        },      
        "淘宝商品最高价格指标": { 
          "max": {"field": "价格"}
        }
      }
    } 
  }
}'
```

## 后过滤器(post_filter)

只过滤搜索结果,不过滤聚合结果.<br>
它是接收一个过滤器的顶层搜索请求元素.<br>
这个过滤器在查询 *之后* 执行(这正是该过滤器的名字的由来:它在查询之后post执行).<br>
正因为它在查询之后执行,它对查询范围没有任何影响,所以对聚合也不会有任何影响.<br>
* *post_filter只影响搜索结果** .

下面查询:

0. 价格在128-1024范围内的商品
0. 对范围内的商品进行聚合
0. 仅返回范围内供应商为京东的查询结果

```bash
requestES GET 'products/_doc/_search' '
{
  "query": {
    "range": {
      "价格": {
        "gte": 128,
        "lte": 1024
      }
    }
  },
  "size": 0,
  "post_filter": {    
    "term": {
      "供应商": "京东"
    }
  },
  "aggs": {
    "商品平均价格指标": { 
      "avg": {"field": "价格"}
    },
    "商品最低价格指标": { 
      "min": {"field": "价格"}
    },      
    "商品最高价格指标": { 
      "max": {"field": "价格"}
    }
  }
}'
```

当需要对搜索结果和聚合结果做不同的过滤时,<br>
才应该使用post_filter,不要再普通搜索中使用post_filter.<br>
post_filter的特性是在查询之后执行,<br>
任何过滤对性能带来的好处(比如缓存)都会完全失去.<br>
在需要不同过滤时,post_filter只与聚合一起使用.<br>

## 多桶排序

### 内置排序

聚合引入了一个order对象, 它允许可以根据以下几个值中的一个值进行排序:

0. _count: 按文档数排序.对terms、histogram、date_histogram有效.
0. _term: 按词项的字符串值的字母顺序排序.只在terms内使用.
0. _key: 按每个桶的键值数值排序(理论上与_term类似). 只在histogram和date_histogram内使用.

```bash
requestES GET 'products/_doc/_search' '
{
  "size": 0,
  "aggs": {   
    "根据供应商分桶": {   
      "terms": {
        "field": "供应商",
        "order":{
          "_count": "asc"
        }
      }
    }  
  }
}'
```

### 按度量排序

```bash
requestES GET 'products/_doc/_search' '
{
  "size": 0,
  "aggs": {   
    "根据供应商分桶": {   
      "terms": {
        "field": "供应商",
        "order": {
          "商品平均价格指标": "asc"
        }
      },
      "aggs": { 
        "商品平均价格指标": { 
          "avg": {
            "field": "价格" 
          }
        }
      }
    }  
  }
}'
```

### 基于 *深度* 度量排序

在一定条件下,也有可能对更深的度量进行排序,比如孙子桶或从孙桶.

可以定义更深的路径,将度量用尖括号( > )嵌套起来,像这样: my_bucket>another_bucket>metric

需要提醒的是嵌套路径上的每个桶都必须是 **单值** 的.<br>
* *filter桶生成一个单值桶** ,所有与过滤条件匹配的文档都在桶中.<br>
多值桶(如:terms)动态生成许多桶,无法通过指定一个确定路径来识别.

```bash
requestES GET 'products/_doc/_search' '
{
  "size": 0,
  "aggs": {   
    "根据供应商分桶": {   
      "terms": {
        "field": "供应商",
        "order": {
          "供应商三级品目商品数量指标>硒鼓品目内商品平均价格指标": "asc"
        }
      },
      "aggs": {
        "供应商三级品目商品数量指标":{
          "filter": { 
            "match": {
              "三级品目": "硒鼓"
            }
          },
          "aggs": {
            "硒鼓品目内商品平均价格指标": { 
              "avg": {"field": "价格"}
            }
          }
        }
      }
    }  
  }
}'
```

## 通过聚合发现异常指标(SigTerms)

significant_terms (SigTerms)聚合与其他聚合都不相同.<br>
目前为止看到的所有聚合在本质上都是简单的数学计算.<br>
将不同这些构造块相互组合在一起,可以创建复杂的聚合以及数据报表.<br>


significant_terms有着不同的工作机制.<br>
对有些人来说,它甚至看起来有点像机器学习.<br>
significant_terms聚合可以在数据集中找到一些异常的指标.<br>


如何解释这些不常见的行为?这些异常的数据指标通常比预估出现的频次要更频繁,<br>
这些统计上的异常指标通常象征着数据里的某些有趣信息.<br>


例如,假设负责检测和跟踪信用卡欺诈,客户打电话过来抱怨信用卡出现异常交易,它们的帐户已经被盗用.<br>
这些交易信息只是更严重问题的症状.<br>
在最近的某些地区,一些商家有意的盗取客户的信用卡信息,或者它们自己的信息无意中也被盗取.<br>


任务是找到危害的共同点,如果有100个客户抱怨交易异常,<br>
很有可能都属于同一个商户,而这家商户有可能就是罪魁祸首.<br>


当然,这里面还有一些特例.<br>
例如,很多客户在它们近期交易历史记录中会有很大的商户如亚马逊,<br>
可以将亚马逊排除在外,然而,在最近一些有问题的信用卡的商家里面也有亚马逊.<br>


这是一个普通的共同商户的例子.<br>
每个人都共享这个商户,无论有没有遭受危害.<br>
对它并不感兴趣.<br>


相反,有一些很小商户比如街角的一家药店,它们属于普通但不寻常的情况,只有一两个客户有交易记录.<br>
同样可以将这些商户排除,因为所有受到危害的信用卡都没有与这些商户发生过交易,可以肯定它们不是安全漏洞的责任方.<br>


真正想要的是不普通的共同商户.<br>
所有受到危害的信用卡都与它们发生过交易,但是在未受危害的背景噪声下,它们并不明显.<br>
这些商户属于统计异常,它们比应该出现的频率要高.<br>
这些不普通的共同商户很有可能就是需要调查的.<br>


significant_terms聚合就是做这些事情.<br>
它分析统计数据并通过对比正常数据找到可能有异常频次的指标.<br>


暴露的异常指标代表什么依赖的数据.<br>
对于信用卡数据,可能会想找出信用卡欺诈.<br>
对于电商数据,可能会想找出未被识别的人口信息,从而进行更高效的市场推广.<br>
如果正在分析日志,可能会发现一个服务器会抛出比它本应抛出的更多异常.<br>
significant_terms的应用还远不止这些.<br>


TODO: 目前尚不理解其算法规则.

```bash
requestES GET 'products/_doc/_search' '
{
  "query": {
    "bool": {
      "filter": {
        "match": {"三级品目": "硒鼓"}
      }
    }
  },
  "size": 0,
  "aggs": {
    "硒鼓商品排名": {
      "significant_terms": {
        "field": "名称.完型",
        "size": 106
      }
    }
  }
}'
```

## 优化聚合查询

terms桶基于数据动态构建桶,它并不知道到底生成了多少桶.<br>
大多数时候对单个字段的聚合查询还是非常快的,<br>
但是当需要同时聚合多个字段时,就可能会产生大量的分组,<br>
最终结果就是占用es大量内存,从而导致OOM的情况发生.<br>


假设现在有一些关于电影的数据集,每条数据里面会有一个数组类型的字段存储表演该电影的所有演员的名字.<br>

```json
{
  "actors": [
    "Fred Jones",
    "Mary Jane",
    "Elizabeth Worthing"
  ]
}
```

如果想要查询出演影片最多的十个演员以及与合作最多的演员,使用聚合是非常简单的:

```json
{
  "aggs": {
    "actors": {
      "terms": {
         "field": "actors",
         "size": 10
      },
      "aggs": {
        "costars": {
          "terms": {
            "field": "actors",
            "size": 5
          }
        }
      }
    }
  }
}
```

这看起来是一个简单的聚合查询,最终只返回50条数据!

但是,这个看上去简单的查询可以轻而易举地消耗大量内存.<br>
actors聚合会构建树的第一层,每个演员都有一个桶.<br>
然后,内套在第一层的每个节点之下,costar聚合会构建第二层,每个联合出演一个桶.<br>
这意味着每部影片会生成n<sup>2</sup> 个桶!

![](https://www.elastic.co/guide/cn/elasticsearch/guide/current/images/300_120_depth_first_1.svg)

用真实点的数据,设想平均每部影片有10名演员,每部影片就会生成102==100个桶.<br>
如果总共有20,000部影片,粗率计算就会生成2,000,000个桶.<br>


聚合只是简单的希望得到前十位演员和与联合出演者,总共50条数据.<br>
为了得到最终的结果,创建了一个有2,000,000桶的树,然后对其排序,取top10.

这时一定非常抓狂,在2万条数据下执行任何聚合查询都是毫无压力的.<br>
如果有2亿文档,想要得到前100位演员以及与合作最多的20位演员,作为查询的最终结果会出现什么情况呢?

可以推测聚合出来的分组数非常大,会使这种策略难以维持.<br>
世界上并不存在足够的内存来支持这种不受控制的聚合查询.<br>

### 深度优先与广度优先(Depth-First Versus Breadth-First)

Elasticsearch允许改变聚合的集合模式,就是为了应对这种状况.<br>
之前展示的策略叫做深度优先 ,它是默认设置, 先构建完整的树,然后修剪无用节点.<br>
深度优先的方式对于大多数聚合都能正常工作,但对于如演员和联合演员这样例子的情形就不太适用.<br>


为了应对这些特殊的应用场景,应该使用另一种集合策略叫做广度优先.<br>
这种策略的工作方式有些不同,它先执行第一层聚合, 再继续下一层聚合之前会先做修剪.<br>


actors聚合会首先执行,在这个时候,树只有一层,但已经知道了前10位的演员.<br>
这就没有必要保留其他的演员信息,因为它们无论如何都不会出现在前十位中.<br>


因为已经知道了前十名演员,可以安全的修剪其他节点.<br>
修剪后,下一层是基于它的执行模式读入的,<br>
重复执行这个过程直到聚合完成,这种场景下,广度优先可以大幅度节省内存.<br>

要使用广度优先,只需简单的通过参数collect开启:

```json
{
  "aggs": {
    "actors": {
      "terms": {
         "field": "actors",
         "size": 10,
         "collect_mode": "breadth_first" 
      },
      "aggs": {
        "costars": {
          "terms": {
            "field": "actors",
            "size": 5
          }
        }
      }
    }
  }
}
```

广度优先仅仅适用于 ***每个组的聚合数量远远小于当前总组数*** 的情况下,<br>
因为广度优先会在内存中缓存裁剪后的仅仅需要缓存的每个组的所有数据,<br>
以便于它的子聚合分组查询可以复用上级聚合的数据.

广度优先的内存使用情况与裁剪后的缓存分组数据量是成线性的.<br>
对于很多聚合来说,每个桶内的文档数量是相当大的.<br>
想象一种按月分组的直方图,总组数肯定是固定的,因为每年只有12个月,这个时候每个月下的数据量可能非常大.<br>
这使广度优先不是一个好的选择,这也是为什么深度优先作为默认策略的原因.<br>


针对上面演员的例子,如果数据量越大,那么默认的使用深度优先的聚合模式生成的总分组数就会非常多,但是预估二级的聚合字段分组后的数据量相比总的分组数会小很多所以这种情况下使用广度优先的模式能大大节省内存,从而通过优化聚合模式来大大提高了在某些特定场景下聚合查询的成功率.<br>


## Doc Values&Fielddata

聚合使用一个叫doc values的数据结构.<br>
Doc values可以使聚合更快、更高效并且内存友好,所以理解它的工作方式十分有益.<br>


Doc values的存在是因为倒排索引只对某些操作是高效的.<br>
倒排索引的优势在于查找包含某个项的文档,<br>
而对于从另外一个方向的相反操作并不高效,即:确定哪些项是否存在单个文档里,聚合需要这种次级的访问模式.<br>


对于以下倒排索引:

Term|Doc_1|Doc_2|Doc_3
-|-|-|-
brown|X|X|
dog|X||X
dogs||X|X
fox|X||X
foxes||X|
in||X|
jumped|X||X
lazy|X|X|
leap||X|
over|X|X|X
quick|X|X|X
summer||X|
the|X||X

如果想要获得所有包含brown的文档的词的完整列表,会创建如下查询:

```json
GET /my_index/_search
{
  "query": {
    "match": {
      "body": "brown"
    }
  },
  "aggs": {
    "popular_terms": {
      "terms": {
        "field": "body"
      }
    }
  }
}
```

查询部分简单又高效.<br>
倒排索引是根据项来排序的,所以首先在词项列表中找到brown,<br>
然后扫描所有列,找到包含brown的文档.<br>
可以快速看到Doc_1和Doc_2包含brown这个token.<br>


然后,对于聚合部分,需要找到Doc_1和Doc_2里所有唯一的词项.<br>
用倒排索引做这件事情代价很高: 会迭代索引里的每个词项并收集Doc_1和Doc_2列里面token.<br>
这很慢而且难以扩展:随着词项和文档的数量增加,执行时间也会增加.<br>


Doc values通过转置两者间的关系来解决这个问题.<br>
倒排索引将词项映射到包含它们的文档,doc values将文档映射到它们包含的词项:

Doc|Terms
-|-|
Doc_1|brown, dog, fox, jumped, lazy, over, quick, the
Doc_2|brown, dogs, foxes, in, lazy, leap, over, quick, summer
Doc_3|dog, dogs, fox, jumped, over, quick, the

当数据被转置之后,想要收集到Doc_1和Doc_2的唯一token会非常容易.<br>
获得每个文档行,获取所有的词项,然后求两个集合的并集.<br>


因此,搜索和聚合是相互紧密缠绕的.<br>
搜索使用倒排索引查找文档,聚合操作收集和聚合doc values里的数据.<br>



Doc values不仅可以用于聚合.<br>
任何需要查找某个文档包含的值的操作都必须使用它.<br>
除了聚合,还包括排序,访问字段值的脚本,父子关系处理.


### Doc Values

Doc Values是在索引时与倒排索引同时生成.<br>
也就是说Doc Values和倒排索引一样,基于Segement生成并且是不可变的.<br>
同时Doc Values和倒排索引一样序列化到磁盘,这样对性能和扩展性有很大帮助.<br>


Doc Values通过序列化把数据结构持久化到磁盘,<br>
可以充分利用操作系统的内存,而不是JVM的Heap.<br>
当workingset远小于系统的可用内存,系统会自动将Doc Values驻留在内存中,使得其读写十分快速,<br>
不过,当其远大于可用内存时,系统会根据需要从磁盘读取Doc Values,然后选择性放到分页缓存中.<br>
很显然,这样性能会比在内存中差很多,但是它的大小就不再局限于服务器的内存了.<br>
如果是使用JVM的Heap来实现那么只能是因为Out Of Memory导致程序崩溃了.<br>



因为Doc Values不是由JVM来管理,所以Elasticsearch实例可以配置一个很小的JVM Heap,这样给系统留出来更多的内存.<br>
同时更小的Heap可以让JVM更加快速和高效的回收.<br>


之前,会建议分配机器内存的50% 来给JVM Heap.<br>
但是对于Doc Values,这样可能不是最合适的方案了.<br>
以64gb内存的机器为例,可能给Heap分配4-16gb的内存更合适,而不是32gb.<br>


#### 列式存储的压缩

从广义来说,DocValues本质上是一个序列化的列式存储 .<br>
正如上一节所讨论的,列式存储适用于聚合、排序、脚本等操作.<br>


而且,这种存储方式也非常便于压缩,特别是数字类型.<br>
这样可以减少磁盘空间并且提高访问速度.<br>
现代CPU的处理速度要比磁盘快几个数量级(尽管即将到来的NVMe驱动器正在迅速缩小差距).<br>
所以必须减少直接存磁盘读取数据的大小,尽管需要额外消耗CPU运算用来进行解压.<br>


要了解它如何压缩数据的,来看一组数字类型的DocValues:

Doc|Terms
-|-
Doc_1|100
Doc_2|1000
Doc_3|1500
Doc_4|1200
Doc_5|300
Doc_6|1900
Doc_7|4200


按列布局意味着有一个连续的数据块: [100,1000,1500,1200,300,1900,4200] .<br>
因为已经知道都是数字(而不是像文档或行中看到的异构集合),<br>
所以可以使用统一的偏移来将紧紧排列.<br>


而且,针对这样的数字有很多种压缩技巧.<br>
这里每个数字都是100的倍数,<br>
DocValues会检测一个段里面的所有数值,<br>
并使用一个最大公约数,方便做进一步的数据压缩.<br>


如果保存100作为此段的除数,可以对每个数字都除以100,然后得到: [1,10,15,12,3,19,42] .<br>
现在这些数字变小了,只需要很少的位就可以存储下,也减少了磁盘存放的大小.<br>


DocValues在压缩过程中使用如下技巧.<br>
它会按依次检测以下压缩模式:

0. 如果所有的数值各不相同(或缺失),设置一个标记并记录这些值
0. 如果这些值小于256,将使用一个简单的编码表
0. 如果这些值大于256,检测是否存在一个最大公约数
0. 如果没有存在最大公约数,从最小的数值开始,统一计算偏移量进行编码

这些压缩模式不是传统的通用的压缩方式,比如DEFLATE或是LZ4.<br>
因为列式存储的结构是严格且良好定义的,<br>
可以通过使用专门的模式来达到比通用压缩算法(如LZ4)更高的压缩效果.<br>


貌似对数字很好,不知道字符串怎么样?通过借助顺序表(ordinal table),String类型也是类似进行编码的.<br>
String类型是去重之后存放到顺序表的,通过分配一个ID,然后通过数字类型的ID构建DocValues.<br>
这样String类型和数值类型可以达到同样的压缩效果.<br>


顺序表本身也有很多压缩技巧,比如固定长度、变长或是前缀字符编码等等.<br>


#### 禁用DocValues

DocValues默认对所有字段启用,除了analyzed strings.<br>
也就是说所有的数字、地理坐标、日期、IP和不分析(not_analyzed)字符类型都会默认开启.<br>


analyzed strings暂时还不能使用DocValues.<br>
文本经过分析流程生成很多Token,使得DocValues不能高效运行.<br>


因为DocValues默认启用,可以选择对数据集里面的大多数字段进行聚合和排序操作.<br>
但是如果知道永远也不会对某些字段进行聚合、排序或是使用脚本操作?<br>
尽管这并不常见,但是可以通过禁用特定字段的DocValues .<br>
这样不仅节省磁盘空间,也许会提升索引的速度.<br>


要禁用DocValues,在字段的映射(mapping)设置doc_values:false即可.<br>
例如,这里创建了一个新的索引,字段 "session_id" 禁用了DocValues:

```bash
requestES DELETE 'my_temp_index'

requestES PUT 'my_temp_index' '
{
  "mappings": {
    "properties": {
      "session_id": {
        "type": "text",
        "index": false,
        "doc_values": false
      }
    }
  }
}'
```

通过设置doc_values: false ,这个字段将不能被用于聚合、排序以及脚本操作.

####  聚合与分析

##### 分析字符串和Fielddata(Analyzed strings and Fielddata)

DocValues不支持analyzed字符串字段,因为它们不能很有效的表示多值字符串.<br>
DocValues最有效的是,当每个文档都有一个或几个tokens时,<br>
但不是无数的,分析字符串(想象一个PDF,可能有几兆字节并有数以千计的独特tokens).<br>


出于这个原因,DocValues不生成分析的字符串,<br>
然而,这些字段仍然可以使用聚合,那怎么可能呢?

答案是一种被称为fielddata的数据结构.<br>
与DocValues不同,fielddata构建和管理100% 在内存中,常驻于JVM内存堆.<br>
这意味着它本质上是不可扩展的,有很多边缘情况下要提防.<br>


历史版本中,fielddata是所有字段的默认设置.<br>
但是Elasticsearch已迁移到DocValues以减少OOM的几率.<br>
分析的字符串是仍然使用fielddata的最后一块阵地.<br>
最终目标是建立一个序列化的数据结构类似于DocValues ,可以处理高维度的分析字符串,逐步淘汰fielddata.<br>


##### 高基数内存的影响(High-Cardinality Memory Implications)

避免分析字段的另外一个原因就是:高基数字段在加载到fielddata时会消耗大量内存.<br>
分析的过程会经常(尽管不总是这样)生成大量的token,这些token大多都是唯一的.<br>
这会增加字段的整体基数并且带来更大的内存压力.<br>


有些类型的分析对于内存来说极度不友好,想想n-gram的分析过程, NewYork会被n-gram分析成以下token:

0. ne
0. ew
0. w 
0. y
0. yo
0. or
0. rk


可以想象n-gram的过程是如何生成大量唯一token的,特别是在分析成段文本的时候.<br>
当这些数据加载到内存中,会轻而易举的将堆空间消耗殆尽.<br>


因此,在聚合字符串字段之前,请评估情况:

这是一个not_analyzed字段吗?如果是,可以通过DocValues节省内存.<br>


否则,这是一个analyzed字段,它将使用fielddata并加载到内存中.<br>
这个字段因为ngrams有一个非常大的基数?如果是,这对于内存来说极度不友好.<br>


#### 限制内存使用

一旦分析字符串被加载到fielddata,会一直在那里,直到被驱逐(或者节点崩溃).<br>
由于这个原因,留意内存的使用情况,了解它是如何以及何时加载的,怎样限制对集群的影响是很重要的.<br>


Fielddata是延迟加载.<br>
如果从来没有聚合一个分析字符串,就不会加载fielddata到内存中.<br>
此外,fielddata是基于字段加载的, 这意味着只有很活跃地使用字段才会增加fielddata的负担.<br>


然而,这里有一个令人惊讶的地方.<br>
假设查询是高度选择性和只返回命中的100个结果.<br>
大多数人认为fielddata只加载100个文档.<br>
实际情况是,*fielddata会加载索引中(针对该特定字段的) 所有的文档,而不管查询的特异性*.<br>
逻辑是这样:如果查询会访问文档X、Y和Z,那很有可能会在下一个查询中访问其他文档.<br>


与DocValues不同,fielddata结构不会在索引时创建.<br>
相反,它是在查询运行时,动态填充.<br>
这可能是一个比较复杂的操作,可能需要一些时间.<br>
将所有的信息一次加载,再将其维持在内存中的方式要比反复只加载一个fielddata的部分代价要低.<br>


JVM堆是有限资源的,应该被合理利用.<br>
限制fielddata对堆使用的影响有多套机制,这些限制方式非常重要,因为堆栈的乱用会导致节点不稳定(感谢缓慢的垃圾回收机制),甚至导致节点宕机(通常伴随Out Of Memory异常).<br>

##### 选择堆大小(Choosing a Heap Size)

在设置Elasticsearch堆大小时需要通过 $ES_HEAP_SIZE环境变量应用两个规则:

0. 不要超过可用RAM的50%.<br>
Lucene能很好利用文件系统的缓存,它是通过系统内核管理的.<br>
如果没有足够的文件系统缓存空间,性能会受到影响.<br>
此外,专用于堆的内存越多意味着其他所有使用DocValues的字段内存越少.
0. 不要超过32GB.<br>
如果堆大小小于32GB,JVM可以利用指针压缩,这可以大大降低内存的使用:每个指针4字节而不是8字节.<br>

##### Fielddata的大小

indices.fielddata.cache.size控制为fielddata分配的堆空间大小.<br>
当发起一个查询,分析字符串的聚合将会被加载到fielddata,如果这些字符串之前没有被加载过.<br>
如果结果中fielddata大小超过了指定大小 ,其他的值将会被回收从而获得空间.

默认情况下,设置都是unbounded,Elasticsearch永远都不会从fielddata中回收数据.

这个默认设置是刻意选择的:fielddata不是临时缓存.<br>
它是驻留内存里的数据结构,必须可以快速执行访问,而且构建它的代价十分高昂.<br>
如果每个请求都重载数据,性能会十分糟糕.<br>


一个有界的大小会强制数据结构回收数据.<br>
会看何时应该设置这个值,但请首先阅读以下警告:

0. 这个设置是一个安全卫士,而非内存不足的解决方案.

0. 如果没有足够空间可以将fielddata保留在内存中,Elasticsearch就会时刻从磁盘重载数据,并回收其他数据以获得更多空间.<br>
内存的回收机制会导致重度磁盘I/O,并且在内存中生成很多垃圾,这些垃圾必须在晚些时候被回收掉.

设想正在对日志进行索引,每天使用一个新的索引.<br>
通常只对过去一两天的数据感兴趣,尽管会保留老的索引,但很少需要查询它们.<br>
不过如果采用默认设置,旧索引的fielddata永远不会从缓存中回收!<br>
fieldata会保持增长直到fielddata发生断熔(请参阅断路器),这样就无法载入更多的fielddata.<br>


这个时候,被困在了死胡同.<br>
但仍然可以访问旧索引中的fielddata,也无法加载任何新的值.<br>
相反,应该回收旧的数据,并为新值获得更多空间.<br>


为了防止发生这样的事情,可以通过在config/elasticsearch.yml文件中增加配置为fielddata设置一个上限:

```yaml
# 可以设置堆大小的百分比,也可以是某个值,例如: 5gb.
indices.fielddata.cache.size: 20% 
```

有了这个设置,最久未使用(LRU)的fielddata会被回收为新数据腾出空间.<br>


##### 监控fielddata(Monitoring fielddata)

无论是仔细监控fielddata的内存使用情况, 还是看有无数据被回收都十分重要.<br>
高的回收数可以预示严重的资源问题以及性能不佳的原因.<br>


Fielddata的使用可以被监控:

###### 按索引使用

```bash
requestES GET '/_stats/fielddata?fields=*&pretty'
```

###### 按节点使用

```bash
requestES GET '_nodes/stats/indices/fielddata?fields=*&pretty'
```

###### 按索引节点

```bash
requestES GET '_nodes/stats/indices/fielddata?level=indices&fields=*&pretty'
```

##### 断路器

fielddata大小是在数据加载之后检查的.<br>
如果一个查询试图加载比可用内存更多的信息到fielddata中会发生什么?<br>
答案很丑陋:会碰到Out Of Memory Exception.<br>


Elasticsearch包括一个fielddata断熔器,这个设计就是为了处理上述情况.<br>
断熔器通过内部检查(字段的类型、基数、大小等等)来估算一个查询需要的内存.<br>
它然后检查要求加载的fielddata是否会导致fielddata的总量超过堆的配置比例.<br>


如果估算查询的大小超出限制,就会触发断路器,查询会被中止并返回异常.<br>
这都发生在数据加载之前 ,也就意味着不会引起Out Of Memory Exception.<br>

###### 可用的断路器(Available Circuit Breakers)

Elasticsearch有一系列的断路器,它们都能保证内存不会超出限制:

0. indices.breaker.fielddata.limit<br>
fielddata断路器默认设置堆的60% 作为fielddata大小的上限.
0. indices.breaker.request.limit<br>
request断路器估算需要完成其他请求部分的结构大小,例如创建一个聚合桶,默认限制是堆内存的40%.
0. indices.breaker.total.limit<br>
total揉合request和fielddata断路器保证两者组合起来不会使用超过堆内存的70%.

断路器的限制可以在文件config/elasticsearch.yml中指定,可以动态更新一个正在运行的集群:

```json
PUT /_cluster/settings
{
  "persistent": {
    // 这个限制是按对内存的百分比设置的
    "indices.breaker.fielddata.limit": "40%" 
  }
}
```

最好为断路器设置一个相对保守点的值.<br>
记住fielddata需要与request断路器共享堆内存、索引缓冲内存和过滤器缓存.<br>
Lucene的数据被用来构造索引,以及各种其他临时的数据结构.<br>
正因如此,它默认值非常保守,只有60% .<br>
过于乐观的设置可能会引起潜在的堆栈溢出(OOM)异常,这会使整个节点宕掉.<br>


另一方面,过度保守的值只会返回查询异常,应用程序可以对异常做相应处理.<br>
异常比服务器崩溃要好.<br>
这些异常应该也能促进对查询进行重新评估:为什么单个查询需要超过堆内存的60% 之多?


在Fielddata的大小中,提过关于给fielddata的大小加一个限制,从而确保旧的无用fielddata被回收的方法.<br>
indices.fielddata.cache.size和indices.breaker.fielddata.limit之间的关系非常重要.<br>
如果断路器的限制低于缓存大小,没有数据会被回收.<br>
为了能正常工作,断路器的限制必须要比缓存大小要高.<br>

值得注意的是:断路器是根据总堆内存大小估算查询大小的,而非根据实际堆内存的使用情况.<br>
这是由于各种技术原因造成的(例如,堆可能看上去是满的但实际上可能只是在等待垃圾回收,这使难以进行合理的估算).<br>
但作为终端用户,这意味着设置需要保守,因为它是根据总堆内存必要的,而不是可用堆内存.<br>

### Fielddata
#### Fielddata的过滤

设想正在运行一个网站允许用户收听喜欢的歌曲.<br>
为了可以更容易的管理自己的音乐库,用户可以为歌曲设置任何喜欢的标签,<br>
这样就会有很多歌曲被附上rock(摇滚) 、hiphop(嘻哈) 和electronica(电音) ,<br>
但也会有些歌曲被附上my_16th_birthday_favorite_anthem这样的标签.<br>


现在设想想要为用户展示每首歌曲最受欢迎的三个标签,很有可能rock这样的标签会排在三个中的最前面,<br>
而my_16th_birthday_favorite_anthem则不太可能得到评级.<br>
尽管如此,为了计算最受欢迎的标签,必须强制将这些一次性使用的项加载到内存中.<br>


fielddata过滤,可以控制这种状况.<br>
知道自己只对最流行的项感兴趣,所以可以简单地避免加载那些不太有意思的长尾项:

```json
PUT /music/_mapping/song
{
  "properties": {
    "tag": {
      "type": "text",
      //fielddata关键字允许配置fielddata处理该字段的方式.
      "fielddata": { 
        "filter": {
          //frequency过滤器允许基于项频率过滤加载fielddata.
          "frequency": {
            // 只加载那些至少在本段文档中出现1% 的项.
            "min": 0.01,
            // 忽略任何文档个数小于500的段
            "min_segment_size": 500  
          }
        }
      }
    }
  }
}
```

有了这个映射,只有那些至少在本段文档中出现超过1% 的项才会被加载到内存中.<br>
也可以指定一个最大词频,它可以被用来排除常用项,比如停用词.<br>


这种情况下,词频是按照段来计算的.<br>
这是实现的一个限制:fielddata是按段来加载的,所以可见的词频只是该段内的频率.<br>
但是,这个限制也有些有趣的特性:它可以让受欢迎的新项迅速提升到顶部.<br>


比如一个新风格的歌曲在一夜之间受大众欢迎,可能想要将这种新风格的歌曲标签包括在最受欢迎列表中,<br>
但如果倚赖对索引做完整的计算获取词频,就必须等到新标签变得像rock和electronica)一样流行.<br>
由于频度过滤的实现方式,新加的标签会很快作为高频标签出现在新段内,也当然会迅速上升到顶部.<br>


min_segment_size参数要求Elasticsearch忽略某个大小以下的段.<br>
如果一个段内只有少量文档,它的词频会非常粗略没有任何意义.<br>
小的分段会很快被合并到更大的分段中,某一刻超过这个限制,将会被纳入计算.<br>


通过频次来过滤项并不是唯一的选择,也可以使用正则式来决定只加载那些匹配的项.<br>
例如,可以用regex过滤器处理twitte上的消息只将以 # 号开始的标签加载到内存中.<br>
这假设使用的分析器会保留标点符号,像whitespace分析器.<br>

Fielddata过滤对内存使用有巨大的影响,权衡也是显而易见的:**实际上是在忽略数据**.<br>
但对于很多应用,这种权衡是合理的,因为这些数据根本就没有被使用到.<br>
内存的节省通常要比包括一个大量而无用的长尾项更为重要.
####  预加载fielddata

Elasticsearch加载内存fielddata的默认行为是延迟加载 .<br>
当Elasticsearch第一次查询某个字段时,<br>
它将会完整加载这个字段所有Segment中的倒排索引到内存中,以便于以后的查询能够获取更好的性能.<br>


对于小索引段来说,这个过程的需要的时间可以忽略.<br>
但如果有一些5GB的索引段,并希望加载10GB的fielddata到内存中,这个过程可能会要数十秒.<br>
已经习惯亚秒响应的用户很难会接受停顿数秒卡着没反应的网站.<br>


有三种方式可以解决这个延时高峰:

0. 预加载fielddata
0. 预加载全局序号
0. 缓存预热

所有的变化都基于同一概念:预加载fielddata,这样在用户进行搜索时就不会碰到延迟高峰.<br>


第一个工具称为预加载(与默认的延迟加载相对).<br>
随着新分段的创建(通过刷新、写入或合并等方式), 启动字段预加载可以使那些对搜索不可见的分段里的fielddata提前加载.<br>


这就意味着首次命中分段的查询不需要促发fielddata的加载,因为fielddata已经被载入到内存.<br>
避免了用户遇到搜索卡顿的情形.<br>


预加载是按字段启用的,所以可以控制具体哪个字段可以预先加载:

```json
PUT /music/_mapping
{
  "tags": {
    "type": "text",
    "fielddata": {
      // 设置fielddata.loading:eager可以告诉Elasticsearch预先将此字段的内容载入内存中
      "loading": "eager" 
    }
  }
}
```

Fielddata的载入可以使用update-mappingAPI对已有字段设置lazy或eager两种模式.<br>


预加载只是简单的将载入fielddata的代价转移到索引刷新的时候,而不是查询时,从而大大提高了搜索体验.<br>


体积大的索引段会比体积小的索引段需要更长的刷新时间.<br>
通常,体积大的索引段是由那些已经对查询可见的小分段合并而成的,所以较慢的刷新时间也不是很重要.<br>

#### 全局序号(Global Ordinals)

有种可以用来降低字符串fielddata内存使用的技术叫做序号.<br>


设想有十亿文档,每个文档都有自己的status状态字段,<br>
状态总共有三种:status_pending、status_published、status_deleted.<br>
如果为每个文档都保留其状态的完整字符串形式,那么每个文档就需要使用14到16字节,或总共15GB.<br>


取而代之的是可以指定三个不同的字符串,对其排序、编号:0,1,2.<br>


Ordinal|Term
-|-
0|status_deleted
1|status_pending
2|status_published

序号字符串在序号列表中只存储一次,每个文档只要使用数值编号的序号来替代它原始的值.<br>


Doc|Ordinal
-|-
0|1# pending
1|1# pending
2|2# published
3|0# deleted

这样可以将内存使用从15GB降到1GB以下!

但这里有个问题,记得fielddata是按分段来缓存的.<br>
如果一个分段只包含两个状态(status_deleted和status_published).<br>
那么结果中的序号(0和1)就会与包含所有三个状态的分段不一样.<br>


如果尝试对status字段运行terms聚合,需要对实际字符串的值进行聚合,也就是说需要识别所有分段中相同的值.<br>
一个简单粗暴的方式就是对每个分段执行聚合操作,返回每个分段的字符串值,再将它们归纳得出完整的结果.<br>
尽管这样做可行,但会很慢而且大量消耗CPU.<br>


取而代之的是使用一个被称为全局序号的结构.<br>
全局序号是一个构建在fielddata之上的数据结构,它只占用少量内存.<br>
唯一值是跨所有分段识别的,然后将它们存入一个序号列表中,正如描述过的那样.<br>


现在,terms聚合可以对全局序号进行聚合操作,将序号转换成真实字符串值的过程只会在聚合结束时发生一次.<br>
这会将聚合(和排序)的性能提高三到四倍.<br>

##### 构建全局序号

全局序号分布在索引的所有段中,所以如果新增或删除一个分段时,需要对全局序号进行重建.<br>
重建需要读取每个分段的每个唯一项,基数越高(即存在更多的唯一项)这个过程会越长.<br>


全局序号是构建在内存fielddata和DocValues之上的.<br>
实际上,它们正是DocValues性能表现不错的一个主要原因.<br>


和fielddata加载一样,全局序号默认也是延迟构建的.<br>
首个需要访问索引内fielddata的请求会促发全局序号的构建.<br>
由于字段的基数不同,这会导致给用户带来显著延迟这一糟糕结果.<br>
一旦全局序号发生重建,仍会使用旧的全局序号,直到索引中的分段产生变化:在刷新、写入或合并之后.<br>

##### 预构建全局序号

单个字符串字段可以通过配置预先构建全局序号:

```json
PUT /music/_mapping
{
  "song_title": {
    "type": "text",
    "fielddata": {
      // 设置eager_global_ordinals也暗示着fielddata是预加载的
      "loading": "eager_global_ordinals" 
    }
  }
}
```

正如fielddata的预加载一样,预构建全局序号发生在新分段对于搜索可见之前.<br>



序号的构建只被应用于字符串.<br>
数值信息(integers(整数)、geopoints(地理经纬度)、dates(日期)等等)不需要使用序号映射,因为这些值自己本质上就是序号映射.<br>


因此,只能为字符串字段预构建其全局序号.

也可以对DocValues进行全局序号预构建:

```json
PUT /music/_mapping/_song
{
  "song_title": {
    "type": "text",
    "doc_values": true,
    "fielddata": {
      // 这种情况下,fielddata没有载入到内存中,而是DocValues被载入到文件系统缓存中.
      "loading": "eager_global_ordinals" 
    }
  }
}
```

与fielddata预加载不一样,预建全局序号会对数据的实时性产生影响,构建一个高基数的全局序号会使一个刷新延时数秒.<br>
选择在于是每次刷新时付出代价,还是在刷新后的第一次查询时.<br>
如果经常索引而查询较少,那么在查询时付出代价要比每次刷新时要好.<br>
如果写大于读,那么在选择在查询时重建全局序号将会是一个更好的选择.<br>


针对实际场景优化全局序号的重建频次.<br>
如果有高基数字段需要花数秒钟重建,增加refresh_interval的刷新的时间从而可以使全局序号保留更长的有效期,这也会节省CPU资源,因为重建的频次下降了.<br>


##### 索引预热器

预热器早于fielddata预加载和全局序号预加载之前出现,它们仍然有其存在的理由.<br>
一个索引预热器允许指定一个查询和聚合须要在新分片对于搜索可见之前执行.<br>
这个想法是通过预先填充或预热缓存让用户永远无法遇到延迟的波峰.<br>


原来,预热器最重要的用法是确保fielddata被预先加载,因为这通常是最耗时的一步.<br>
现在可以通过前面讨论的那些技术来更好的控制它,<br>
但是预热器还是可以用来预建过滤器缓存,当然也还是能选择用它来预加载fielddata.<br>


注册一个预热器然后解释发生了什么:

```json
// 预热器被关联到索引(music)上,使用接入口_warmer以及ID(warmer_1)
PUT /music/_doc/warmer_1 
{
  "query": {
    "bool": {
      "filter": {
        "bool": {
          // 	为三种最受欢迎的曲风预建过滤器缓存.
          "should": [ 
            {"term": {"tag": "rock"}},
            {"term": {"tag": "hiphop"}},
            {"term": {"tag": "electronics"}}
          ]
        }
      }
    }
  },
  "aggs": {
    "price": {
      "histogram": {
        // 字段price的fielddata和全局序号会被预加载
        "field": "price", 
        "interval": 10
      }
    }
  }
}
```

预热器是根据具体索引注册的,每个预热器都有唯一的ID,因为每个索引可能有多个预热器.<br>


然后可以指定查询,任何查询.<br>
它可以包括查询、过滤器、聚合、排序值、脚本,任何有效的查询表达式都毫不夸张.<br>
这里的目的是想注册那些可以代表用户产生流量压力的查询,从而将合适的内容载入缓存.<br>


当新建一个分段时,Elasticsearch将会执行注册在预热器中的查询.<br>
执行这些查询会强制加载缓存,只有在所有预热器执行完,这个分段才会对搜索可见.<br>


与预加载类似,预热器只是将冷缓存的代价转移到刷新的时候.<br>
当注册预热器时,做出明智的决定十分重要.<br>
为了确保每个缓存都被读入,可以加入上千的预热器,但这也会使新分段对于搜索可见的时间急剧上升.<br>


实际中,会选择少量代表大多数用户的查询,然后注册它们.<br>



## 内存管理

内存的管理形式可以有多种形式,这取决于特定的应用场景:

0. 在规划时,组织好数据,使聚合运行在not_analyzed字符串而不是analyzed字符串,这样可以有效的利用DocValues.
0. 在测试时,验证分析链不会在之后的聚合计算中创建高基数字段.
0. 在搜索时,合理利用近似聚合和数据过滤.
0. 在节点层,设置硬内存大小以及动态的断熔限制.
0. 在应用层,通过监控集群内存的使用情况和FullGC的发生频率,来调整是否需要给集群资源添加更多的机器节点.
0. 大多数实施会应用到以上一种或几种方法.<br>
确切的组合方式与特定的系统环境高度相关.

无论采取何种方式,对于现有的选择进行评估,并同时创建短期和长期计划,都十分重要.<br>
先决定当前内存的使用情况和需要做的事情(如果有),通过评估数据增长速度,来决定未来半年或者一年的集群的规划,使用何种方式来扩展.<br>


最好在建立集群之前就计划好这些内容,而不是在集群堆内存使用90%的时候再临时抱佛脚.

# 数据建模



# 参考资料

> [elastic|Elasticsearch: 权威指南(2.X版)](https://www.elastic.co/guide/cn/elasticsearch/guide/current/index.html)

> [Github|elasticsearch-analysis-ik](https://github.com/medcl/elasticsearch-analysis-ik)

> [铭毅天下|Elasticsearch词频统计实现与原理解读](https://mp.weixin.qq.com/s/gCx01wzvsC0zKNmjKWx77Q)

> [CSDN|【Mark】elasticsearch聚合结果被分词处理方案](https://blog.csdn.net/mr_wanter/article/details/80994488)