class Elasticsearch {

  details = {}
  sources = []
  structures = []
  searchFor = {}
  requestConfig = null

  handleResult(result) {

    this.details.响应时长 = result.took.toString() + ' (毫秒)'
    this.details.是否超时 = (result.timed_out ? '是' : '否')
    this.details.返回数量 = result.hits.hits.length
    this.sources = []

    result.hits.hits.forEach(element => {
      let item = Object.assign({ esId: element._id }, element._source)
      this.sources.push(item)
    });

    return [true, this.sources]
  }

  analysisStructures() {
    let source = this.sources[Math.floor(Math.random() * this.sources.length)]

    if((!source || this.sources.length == 0) && (this.structures != {}) && (this.searchFor != {})){
      for (const [key, value] of Object.entries(this.searchFor)){
        this.structures[key].searchValue = value
      }
      return
    }

    for (const [key, value] of Object.entries((source || {}))) {

      let info = {}
      switch (typeof (value)) {
        case 'undefined':
        case 'boolean':
        case 'number':
        case 'bigint':
        case 'string':
        case 'symbol':
        case 'function':
          info.typeof = typeof (value)
          break;
        case 'object':
          if (Array.isArray(value)) {
            info.typeof = 'array'
            let range = new Set(this.sources.map(source => {return source[key]}).flat())
            info.range = range
          }
          break;
        default:
          break;
      }
      info.typeof ||= typeof (value)
      info.searchValue = (this.searchFor[key] || '')

      this.structures[key] = info
    }
  }

  printfSources() {
    let printfs = []
    this.sources.forEach(source => {
      let source_ = {}
      for (const [key, value] of Object.entries(source)) {
        var value_ = null

        switch (typeof (value)) {
          case 'object':
            value_ = JSON.stringify(value)
            break;
          default:
            value_ = value
            break;
        }
        source_[key] = value_
      }

      printfs.push(source_)
    })
    console.table(printfs)
  }

  async requestES(params, callBack) {

    this.requestConfig ||= {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      redirect: 'follow',
      mode: 'cors',
      host: 'http://192.168.6.9',
      port: '9200',
      route: 'unite_structure/_doc/_search',
      resultCount: 100,
      consolePrint: false,
      searchFor: {},
    }

    this.requestConfig.params ||= {"size": this.requestConfig.resultCount}

    params = Object.assign(this.requestConfig, params)
    this.searchFor = params.searchFor

    let requestOptions = {
      method: params.method,
      headers: params.headers,
      redirect: params.redirect,
      mode: params.mode,
      body: JSON.stringify(params.params),
    };

    let requestPath = params.host + ':' + params.port + '/' + params.route

    this.details = {
      '开始时间': new Date().toJSON(),
      '结束时间': null,
      '请求方式': params.method,
      '完整路径': requestPath,
      '请求头部': JSON.stringify(params.headers),
      '响应时长': null,
      '请求Body': JSON.stringify(params.params),
      // 便于页面显示body参数 这里微调
      '完整参数': JSON.stringify(Object.assign(Object.assign({}, requestOptions), {body:params.params})),
    }

    var result
    await fetch(requestPath, requestOptions)
      .then(response => response.text())
      .then(res => {
        result = JSON.parse(res)
      })
      .catch(error => {
        console.log('error', error)
        return [false, '网络请求异常', error];
      });

    let res = this.handleResult(result)

    this.details.结束时间 = (new Date().toJSON())

    if (params.consolePrint){
      console.clear()
      console.table(this.details)
    }

    if (res[0]) {
      if (params.consolePrint){
        // this.printfSources()
        console.table(this.sources)
      }
      this.analysisStructures()
    }

    if(!!callBack) return callBack([true, this])

  }

}