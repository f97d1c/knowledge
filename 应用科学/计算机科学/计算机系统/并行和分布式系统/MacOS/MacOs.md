
<!-- TOC -->

- [说明](#说明)
  - [目录构成](#目录构成)
- [应用程序](#应用程序)
  - [财务](#财务)
  - [参考](#参考)
  - [工具](#工具)
    - [Iterm2(终端工具)](#iterm2终端工具)
      - [快捷键](#快捷键)
      - [智能选中](#智能选中)
    - [访达](#访达)
      - [DiskImageMounter.app](#diskimagemounterapp)
      - [默认打开方式](#默认打开方式)
    - [pdftk-server(终端PDF编辑软件)](#pdftk-server终端pdf编辑软件)
    - [Calibre(.mobi文件编辑器)](#calibremobi文件编辑器)
    - [Aria2(高速下载 类似wget命令行)](#aria2高速下载-类似wget命令行)
    - [Automator(自动操作)](#automator自动操作)
      - [快速操作](#快速操作)
        - [添加快捷键打开应用程序](#添加快捷键打开应用程序)
    - [gnu-sed](#gnu-sed)
    - [fswatch(文件更改监视器)](#fswatch文件更改监视器)
    - [coreutils](#coreutils)
    - [Bash](#bash)
      - [关于 ~/.bash_profile](#关于-bash_profile)
      - [升级Bash](#升级bash)
        - [白名单](#白名单)
        - [设置默认Shell](#设置默认shell)
    - [e2fsprogs(磁盘格式化工具)](#e2fsprogs磁盘格式化工具)
      - [将U盘格式化为ext4格式](#将u盘格式化为ext4格式)
  - [健康健美](#健康健美)
  - [教育](#教育)
  - [旅游](#旅游)
  - [软件开发工具](#软件开发工具)
    - [vscode](#vscode)
      - [快捷键](#快捷键-1)
    - [Xcode](#xcode)
  - [商务](#商务)
  - [社交](#社交)
  - [摄影与录像](#摄影与录像)
    - [Soundflower](#soundflower)
      - [下载安装](#下载安装)
      - [调整音频MIDI设置](#调整音频midi设置)
      - [调整声音设置](#调整声音设置)
      - [调整录制时的设置](#调整录制时的设置)
  - [生活](#生活)
    - [Safari](#safari)
      - [快捷键](#快捷键-2)
        - [网页](#网页)
        - [阅读列表](#阅读列表)
        - [书签边栏和书签视图](#书签边栏和书签视图)
  - [体育](#体育)
  - [天气](#天气)
  - [图形和设计](#图形和设计)
  - [效率](#效率)
  - [新闻](#新闻)
  - [医疗](#医疗)
  - [音乐](#音乐)
  - [游戏](#游戏)
  - [娱乐](#娱乐)
- [系统偏好设置](#系统偏好设置)
  - [网络](#网络)
    - [查看网络连接方式](#查看网络连接方式)
    - [修改对应连接方式的DNS](#修改对应连接方式的dns)
    - [清空连接方式的DNS](#清空连接方式的dns)
    - [清空DNS缓存](#清空dns缓存)
  - [蓝牙](#蓝牙)
    - [还原蓝牙模块](#还原蓝牙模块)
  - [键盘](#键盘)
    - [常用快捷键](#常用快捷键)
    - [剪切、拷贝、粘贴和其他常用快捷键](#剪切拷贝粘贴和其他常用快捷键)
    - [睡眠、退出登录和关机快捷键](#睡眠退出登录和关机快捷键)
    - [访达和系统快捷键](#访达和系统快捷键)
    - [文稿快捷键](#文稿快捷键)
    - [自定义App快捷键](#自定义app快捷键)
  - [时间机器](#时间机器)
    - [基于 Linux(Ubuntu) 搭建 Time Machine 服务器](#基于-linuxubuntu-搭建-time-machine-服务器)
      - [基于Netatalk](#基于netatalk)
        - [Netatalk](#netatalk)
        - [avahi-daemon](#avahi-daemon)
        - [nss-mdns](#nss-mdns)
      - [基于 Samba](#基于-samba)
        - [安装 Samba](#安装-samba)
        - [连接服务器](#连接服务器)
        - [创建磁盘镜像文件](#创建磁盘镜像文件)
        - [将空白映像拷贝到共享文件夹](#将空白映像拷贝到共享文件夹)
        - [挂载磁盘](#挂载磁盘)
        - [将磁盘设置为备份磁盘](#将磁盘设置为备份磁盘)
          - [Invalid destination path](#invalid-destination-path)
    - [本地备份管理](#本地备份管理)
      - [查看](#查看)
      - [删除](#删除)
- [常见问题](#常见问题)
  - [xcrun](#xcrun)
    - [invalid active developer path](#invalid-active-developer-path)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## 目录构成

按照Mac系统功能结构进行目录编排.

# 应用程序

## 财务

## 参考

## 工具

### Iterm2(终端工具)

#### 快捷键

快捷键|作用
-|-
⌘+← ⌘+→ 或 <br> ⌘+{ ⌘+}|切换 tab
⌘+数字|直接定位到该 tab
⌘+t|新建 tab
⌘+[ ⌘+]|顺序切换 pane
⌘+Option+方向键|按方向切换 pane
⌘+d|水平切分屏幕
⌘+Shift+d|垂直切分屏幕
⌘+f|智能查找,支持正则查找
⌘++|弹出自动补齐窗口,列出曾经使用过的命令
⌘+Shift+h|弹出历史粘贴记录窗口
⌘+Shift+|弹出历史命令记录窗口
⌘+Option+e|全屏展示所有的 tab,可以搜索
⌘+/|高亮当前鼠标的位置
⌘+enter|全屏

#### 智能选中

> 在 iTerm2 中,选中即复制.即任何选中状态的字符串都被放到了系统剪切板中.

在 iTerm2 中,双击选中,三击选中整行,四击智能选中(智能规则可配置),可以识别网址,引号引起的字符串,邮箱地址等.

### 访达

#### DiskImageMounter.app

> DiskImageMounter是一个从10.3开始用来处理Mac OS X中的磁盘映像的实用工具.<br>
和BOMArchiveHelper一样,双击它之后并不会出现GUI; 事实上,这样完全没有任何意义.<br>
它只能在/System/Library/CoreServices/DiskImageMounter.app中被找到.<br>
DiskImageMounter通过发起一个守护进程来处理磁盘映像或者或联系一个当前正在运行的后台程序并将其安装到磁盘中.<br>
这个程序所能显示的唯一一个GUI是一个有progress bar的窗口并列出一些选项(取消或跳过认证),当它不能列出映像时会显示一个错误报告.

在创建工作流(*.workflow)时, 如果需要挂载磁盘镜像,需要指定以该app打开.具体路径为:

```
/System/Library/CoreServices/DiskImageMounter.app
```

#### 默认打开方式

![](https://upload-images.jianshu.io/upload_images/5430305-833f15d9abb26b10.png?imageMogr2/auto-orient/strip|imageView2/2/w/552)

### pdftk-server(终端PDF编辑软件)

```bash
# 提取1-15页为一个文件
pdftk input.pdf cat 1-15 output new.pdf

# 提取第1至3,第5,第6至10页,并合并为一个pdf文件
pdftk input.pdf cat 1-3 5 6-10 output combined.pdf

# 合并(concatenate) 前面所有的pdf为output.pdf
pdftk file1.pdf file2.pdf ... cat output new.pdf

# 拆分PDF的每一页为一个新文件 并按照指定格式设定文件名
pdftk input.pdf burst output new_%d.pdf

# 按照通配符,合并大量PDF文件
pdftk *.pdf cat output combined.pdf

# 去除第 13 页,其余的保存为新PDF
pdftk in.pdf cat 1-12 14-end output out1.pdf

# 扫描一本书,odd.pdf 为书的全部奇数页,even.pdf 为书的全部偶数页,下面的命令可以将两个 pdf 合并成页码正常的书
pdftk A=odd.pdf B=even.pdf shuffle A B output collated.pdf

# 按180°旋转所有页面
pdftk input.pdf cat 1-endsouth output output.pdf

# 按顺时针90°旋转第三页,其他页不变
pdftk input.pdf cat 1-2 3east 4-end output output.pdf

# 输入密码转换成无密码PDF  pdftk secured.pdf input_pw foopass output unsecured.pdf
```

### Calibre(.mobi文件编辑器)

### Aria2(高速下载 类似wget命令行)

### Automator(自动操作)

#### 快速操作

##### 添加快捷键打开应用程序

![](https://upload-images.jianshu.io/upload_images/2312315-a4c07b5000b20018.png?imageMogr2/auto-orient/strip|imageView2/2/w/618)

![](https://upload-images.jianshu.io/upload_images/2312315-9c776ae4b8d77a69.png?imageMogr2/auto-orient/strip|imageView2/2/w/646)

[详细步骤](https://www.jianshu.com/p/aa69812b2303)

### gnu-sed

MacOs中sed命令使用的是基于BSD SED,<br>
使用扩展正则表达式(-E)是基于BNU SED,<br>
因此扩展正则表达式将无法使用.

```
brew install gnu-sed
```

### fswatch(文件更改监视器)

> fswatch是文件更改监视器,当指定文件或目录的内容被修改时,它将接收通知.<br>用于替代linux内核提供的inotify.

```
brew install fswatch
```

简单示例

```bash
fswatch $file_path | while read file
do
  echo "${file} was changed."
done
```

### coreutils

> 获取所有命令带有*g*前缀的GNU命令

如:OS X中的date不同于GNU的coreutils date程序.<br>
可以安装coreutils(包括gnu-date),然后将获得一个date支持毫秒的版本.

```bash
date '+%Y-%m-%d %H:%M:%S.%N'
# => 2020-08-05 16:30:11.N

gdate '+%Y-%m-%d %H:%M:%S.%N'
# => 2020-08-05 16:30:14.480400000
```

### Bash

#### 关于 ~/.bash_profile

MacOS中使用 ~/.bash_profile 文件 代替 ~/.bashrc文件.

可在 ~/.bash_profile 文件中加入:

```bash
[ -r ~/.bashrc ] && source ~/.bashrc
```

#### 升级Bash

> 目前(2020年)MacOS默认的bash为2007年的3.2版本.

苹果在其操作系统中包含如此旧版本的Bash的原因与许可有关.<br>
从4.0版本(3.2的后继版本)开始,Bash使用GNU通用公共许可证v3(GPLv3),Apple不(想)支持.<br>
GNUBash的3.2版是Apple接受的带有GPLv2的最新版本,因此坚持使用这个版本.

```
bash --version                       

GNU bash, version 3.2.57(1)-release (x86_64-apple-darwin19)
Copyright (C) 2007 Free Software Foundation, Inc.
```

Bash升级**非必须**,笔者也是因为某些3.2版不支持的新特性才注意到这一点.

升级过程如下:

```bash
brew install bash

# 验证安装 系统上现在应有两个版本的Bash
which -a bash

# => /usr/local/bin/bash
# => /bin/bash

# 第一个是新版本,第二个是旧版本
/usr/local/bin/bash --version

# GNU bash, version 5.0.18(1)-release (x86_64-apple-darwin19.5.0)
# Copyright (C) 2019 Free Software Foundation, Inc.
# License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>

# This is free software; you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

# 打开新窗口
# 在PATH变量中,默认的新版/usr/local/bin目录的Path路径会在旧版本/bin目录之前,因此,只需输入bash即可使用新版
bash --version

```

##### 白名单

UNIX包含一项安全功能,该功能将可用作loginshell(即登录系统后使用的shell程序)的Shell程序限制在 *受信任* Shell程序列表中.

由于要将新设置的BashShell用作默认Shell,因此它必须能够充当登录Shell.<br>
这意味着,必须将其添加到 /etc/shells文件中.

```bash
sudo tee -a /etc/shells <<-'EOF'
/usr/local/bin/bash
EOF
```

##### 设置默认Shell

此时,如果打开一个新的终端窗口,则仍会使用Bash 3.2.<br>
这是因为 /bin/bash仍设置为默认Shell程序.<br>
要将其更改为新的shell,请执行以下命令:

```
chsh -s /usr/local/bin/bash
```

打开新的窗口进行验证:

```bash
echo $BASH_VERSION

# => 5.0.18(1)-release
```

该chsh命令仅为执行命令的用户(当前用户)更改默认Shell程序.<br>
如果也想更改其他用户的默认Shell,则可以通过登录其他用户的身份来重复此命令.<br>
最重要的是,也许你可能想要更改root用户的默认Shell程序,可以执行以下操作:

```
sudo chsh -s /usr/local/bin/bash
```

如果以root用户身份打开Shell,它将使用新的Bash版本.

### e2fsprogs(磁盘格式化工具)

安装 e2fsprogs:

```
brew install e2fsprogs
```

#### 将U盘格式化为ext4格式

输入以下命令以找到盘符:

```
diskutil list
```

推出目标盘符:

```
diskutil unmountdisk 目标盘符
```

输入以下命令(4 代表 ext4 格式,也可以是 3 即 ext3 格式):

```
sudo $(brew --prefix e2fsprogs)/sbin/mkfs.ext4 目标盘符
```

回车,输入 Mac 密码再回车,过程需要稍等片刻,<br>
等待完成即可拔掉 U盘,这时候 U盘格式已经为 ext4 了.

## 健康健美

## 教育

## 旅游

## 软件开发工具

### vscode

#### 快捷键

快捷键|作用
-|-
⇧+⌘+⌥+上/下箭头<br>FN+⌘+上/下箭头|光标批量操作

### Xcode

软件较大,不推荐通过App Store安装,容易一直卡在等待安装,<br>可通过下载后自行安装:

[官网下载链接](https://developer.apple.com/downloads/index.action?name=Xcode)

## 商务

## 社交

## 摄影与录像

### Soundflower

> 解决原生录屏工具(Shift+Comand+5)没有声音问题.

#### 下载安装

[下载链接](https://github.com/mattingalls/Soundflower/releases)

#### 调整音频MIDI设置

打开「音频 MIDI 设置」,点击左下角的加号,分别添加聚焦设备和多输出设备,如:

* 聚焦设备:选择内建麦克风和 Soundflower(64ch)
* 多输出设备:选择内建输出和 Soundflower(64ch)

*注意是64ch,选择2ch录制的视频会有回音*

![](https://cdn.sspai.com/2020/04/05/14d8cc9bd7fe4d9a637427d9d08201fe.png)

如果使用了外接音频设备(如耳机),则将「内建麦克风/内建输出」的勾选换成「设备名字」即可,注意区分输入和输出。

#### 调整声音设置

打开「系统偏好设置」,再打开「声音」,设置如下:

* 输出:选择「多输出设备」
* 输入:选择「聚焦设备」

#### 调整录制时的设置

打开「截屏」,更改「选项」中「麦克风」一栏的设置:

* 选择聚焦设备:同时录制系统声音和麦克风声音
* 选择 Soundflower(2ch)或 Soundflower(64ch):只录制系统声音

## 生活

### Safari

#### 快捷键

常用快捷键|作用
-|-
⇧+⌘+N|新建无痕窗口

##### 网页   

快捷键|作用
-|-
⇧+⌘+\ |显示所有标签页
⇧+⌘+点击链接/书签<br>搜索栏中键入后,按下 ⇧+⌘+Return|在新标签页中打开页面,并使该标签页成为活跃标签页
⇧+⌘+[/]/左/右键|使前/后标签页成为活跃标签页
⌘+1~9|选择前 9 个标签页中的一个
⌥+点击选中标签页上的关闭按钮|关闭全部标签页,只保留选中标签
⇧+⌘+T|重新打开上次关闭的标签页或窗口
前往主页|⌘+Home 键
Esc|在智能搜索栏中键入时,恢复当前网页地址<br>关闭阅读器<br>退出全屏幕视图
⌥+点击下载链接|下载链接文件
⌘++/-|缩放网站内容

##### 阅读列表 

快捷键|作用
-|-
⌃+⌘+2|显示或隐藏阅读列表边栏
⇧+⌘+D|添加当前页面
⇧+点击页面的链接|添加链接页面
双指左滑|移除页面

##### 书签边栏和书签视图

快捷键|作用
-|-
⌃+⌘+1|显示或隐藏书签边栏

## 体育

## 天气

## 图形和设计

## 效率

## 新闻

## 医疗

## 音乐

## 游戏

## 娱乐

# 系统偏好设置

## 网络

### 查看网络连接方式

```sh
sudo networksetup -listallnetworkservices

# An asterisk (*) denotes that a network service is disabled.
# USB 10/100 LAN
# AX88179 USB 3.0 to Gigabit Ethernet
# USB 10/100/1000 LAN
# AX88x72A
# AX88179A
# Wi-Fi
# Thunderbolt Bridge
# Thunderbolt Ethernet
# PgyMacClient
```

### 修改对应连接方式的DNS

多个DNS,空格隔开:

```sh
sudo networksetup -setdnsservers "USB 10/100/1000 LAN" 10.168.1.204 233.5.5.5
```

### 清空连接方式的DNS

```sh
sudo networksetup -setdnsservers "连接方式名称" empty
```

### 清空DNS缓存

```sh
dscacheutil -flushcache
```

## 蓝牙

### 还原蓝牙模块

> 当蓝牙设备无法搜索到时可以尝试还原蓝牙模块.

Shift + Option + 点击顶部菜单栏蓝牙图标 -> 还原蓝牙模块

![](https://img-blog.csdn.net/20160801102557645)

如果顶部没有显示蓝牙图标:

系统偏好设置 –> 蓝牙 -> 在菜单栏中显示蓝牙-> 勾选.

![](https://img-blog.csdn.net/20160801101806915)
## 键盘

### 常用快捷键

快捷键|作用
-|-
***⇧+⌘+/***|打开当前应用程序的帮助搜索页面<br>结果列表会对应具体操作路径
⌃+⌘+F(full)|进入/退出 全屏模式
⌃+⌘+空格|选择表情符号
⌘+空格|对文件及应用程序等进行检索
⌃+空格|切换输入法
⌃+ ←/→|左/右切换桌面

### 剪切、拷贝、粘贴和其他常用快捷键

快捷键|作用
-|-
⌘+X|剪切所选项并拷贝到剪贴板.
⌘+C|将所选项拷贝到剪贴板.这同样适用于*访达*中的文件.
⌘+V|将剪贴板的内容粘贴到当前文稿或应用中.这同样适用于*访达*中的文件.
⌘+Z|撤销上一个命令.随后可以按 ⇧+⌘+Z 来重做,从而反向执行撤销命令.在某些应用中,可以撤销和重做多个命令.
⌘+A|全选各项.
⌘+F|查找文稿中的项目或打开*查找*窗口.
⌘+G|再次查找 查找之前所找到项目出现的下一个位置.要查找出现的上一个位置,请按 ⇧+⌘+G.
⌘+H|隐藏最前面的应用的窗口.要查看最前面的应用但隐藏所有其他应用,请按 ⌥+⌘+H.
⌘+M|将最前面的窗口最小化至*程序坞*.要最小化最前面的应用的所有窗口,请按 ⌥+⌘+M.
⌘+O|打开所选项,或打开一个对话框以选择要打开的文件.
⌘+P|打印当前文稿.
⌘+S|存储当前文稿.
⌘+T|打开新标签页.
⌘+W|关闭最前面的窗口.要关闭应用的所有窗口,请按下 ⌥+⌘+W.
⌥+⌘+Esc|强制退出应用.
⌘+空格键|显示或隐藏*聚焦*搜索栏.要从*访达*窗口执行*聚焦*搜索,请按 ⌘+⌥+空格键.(如果使用多个输入源以便用不同的语言键入内容,这些快捷键会更改输入源而非显示*聚焦*.了解如何更改冲突的键盘快捷键.)
⌃+⌘+空格键|显示字符检视器,可以从中选择表情符号和其他符号.
⌃+⌘+F|全屏使用应用(如果应用支持).
空格键|使用快速查看来预览所选项.
⌘+Tab|在打开的应用中切换到下一个最近使用的应用.
⇧+⌘+5|在 macOS Mojave 中,拍摄屏幕快照或录制屏幕.在更早的 macOS 版本中,请使用 ⇧+⌘+3  或<br>⇧+⌘+4 来拍摄屏幕快照.进一步了解屏幕快照.
⇧+⌘+N|在*访达*中创建一个新文件夹.
⌘+逗号 (,)|打开最前面的应用的偏好设置.

### 睡眠、退出登录和关机快捷键

> 在这些快捷键中,可能需要按住其中一些快捷键稍长时间.这样可以避免无意中启用快捷键.

快捷键|作用
-|-
电源按钮|按下可将 Mac 开机或将 Mac 从睡眠状态唤醒.按住这个按钮 1.5 秒可使 Mac 进入睡眠状态.*继续按住则会强制 Mac 关机.
⌥+⌘+电源按钮* 或<br>⌥+⌘+Media Eject(⌥+⌘+介质推出键)|将 Mac 置于睡眠状态.
⌃+⇧+电源按钮* 或<br>⌃+⇧+Media Eject(⌃+⇧+介质推出键)|将显示器置于睡眠状态.
⌃+电源按钮* 或<br>⌃+Media Eject(⌃+介质推出键)|显示一个对话框,询问是要重新启动、睡眠还是关机.
⌃+⌘+Power 电源按钮|强制 Mac 重新启动,系统不会提示是否要存储任何打开且未存储的文稿.
⌃+⌘+Media Eject(⌃+⌘+介质推出键)|退出所有应用,然后重新启动 Mac.如果任何打开的文稿有未存储的更改,系统会询问是否要存储这些更改.
⌃+⌥+⌘+电源按钮* 或<br>⌃+⌥+⌘+Media Eject(⌃+⌥+⌘+介质推出键)|退出所有应用,然后将 Mac 关机.如果任何打开的文稿有未存储的更改,系统会询问是否要存储这些更改.
⇧+⌘+Q|退出登录 macOS 用户帐户.系统将提示确认.要在不确认的情况下立即退出登录,请按下 ⌥+⇧+⌘+Q.

*不适用于触控 ID 传感器.*

### 访达和系统快捷键

快捷键|作用
-|-
⌘+D|复制所选文件.
⌘+E|推出所选磁盘或宗卷.
⌘+F|在*访达*窗口中开始*聚焦*搜索.
⌘+I|显示所选文件的*显示简介*窗口.
⌘+R|(1) 如果在*访达*中选择了某个别名|显示所选别名对应的原始文件.(2) 在某些应用(如*日历* 或<br>Safari 浏览器)中,刷新或重新载入页面.(3) 在*软件更新*偏好设置中,再次检查有没有软件更新.
⇧+⌘+C|打开*电脑*窗口.
⇧+⌘+D|打开*桌面*文件夹.
⇧+⌘+F|打开*最近使用*窗口,其中显示了最近查看或更改过的所有文件.
⇧+⌘+G|打开*前往文件夹*窗口.
⇧+⌘+H|打开当前 macOS 用户帐户的个人文件夹.
⇧+⌘+I|打开 iCloud 云盘.
⇧+⌘+K|打开*网络*窗口.
⌥+⌘+L|打开*下载*文件夹.
⇧+⌘+N|新建文件夹.
⇧+⌘+O|打开*文稿*文件夹.
⇧+⌘+P|在*访达*窗口中显示或隐藏预览面板.
⇧+⌘+R|打开*隔空投送*窗口.
⇧+⌘+T|显示或隐藏*访达*窗口中的标签页栏.
Ctrl+⇧+⌘+T|将所选的*访达*项目添加到*程序坞*(OS X Mavericks 或更高版本)
⇧+⌘+U|打开*实用工具*文件夹.
⌥+⌘+D|显示或隐藏*程序坞*.
⌃+⌘+T|将所选项添加到边栏(OS X Mavericks 或更高版本).
⌥+⌘+P|隐藏或显示*访达*窗口中的路径栏.
⌥+⌘+S|隐藏或显示*访达*窗口中的边栏.
⌘+斜线 (/)|隐藏或显示*访达*窗口中的状态栏.
⌘+J|显示*显示*选项.
⌘+K|打开*连接服务器*窗口.
⌘+L|为所选项制作替身.
⌘+N|打开一个新的*访达*窗口.
⌥+⌘+N|新建智能文件夹.
⌘+T|在当前*访达*窗口中有单个标签页开着的状态下显示或隐藏标签页栏.
⌥+⌘+T|在当前*访达*窗口中有单个标签页开着的状态下显示或隐藏工具栏.
⌥+⌘+V|移动|将剪贴板中的文件从原始位置移动到当前位置.
⌘+Y|使用*快速查看*预览所选文件.
⌥+⌘+Y|显示所选文件的快速查看幻灯片显示.
⌘+1|以图标方式显示*访达*窗口中的项目.
⌘+2|以列表方式显示*访达*窗口中的项目.
⌘+3|以分栏方式显示*访达*窗口中的项目.
⌘+4|以封面流方式显示*访达*窗口中的项目.
⌘+左中括号 ([)|前往上一文件夹.
⌘+右中括号 (])|前往下一个文件夹.
⌘+上箭头|打开包含当前文件夹的文件夹.
⌘+⌃+上箭头|在新窗口中打开包含当前文件夹的文件夹.
⌘+下箭头|打开所选项.
右箭头|打开所选文件夹.这个快捷键仅在列表视图中有效.
左箭头|关闭所选文件夹.这个快捷键仅在列表视图中有效.
⌘+Delete|将所选项移到废纸篓.
⇧+⌘+Delete|清倒废纸篓.
⌥+⇧+⌘+Delete|清倒废纸篓而不显示确认对话框.
⌘+调高亮度|打开或关闭目标显示器模式.
⌘+调低亮度|当 Mac 连接到多台显示器时,打开或关闭视频镜像.
⌥+调高亮度|打开*显示器*偏好设置.这个快捷键可与任一亮度键搭配使用.
⌃+调高亮度 或<br>⌃+调低亮度|更改外部显示器的亮度(如果显示器支持).
⌥+⇧+调高亮度 或<br>⌥+⇧+调低亮度|以较小的步幅调节显示器亮度.如果显示器支持,可以将 ⌃ 键添加到此快捷键,以便在外置显示器上进行调节.
⌥+*调度中心*|打开*调度中心*偏好设置.
⌘+*调度中心*|显示桌面.
⌃+下箭头|显示最前面的应用的所有窗口.
⌥+调高音量|打开*声音*偏好设置.这个快捷键可与任一音量键搭配使用.
⌥+⇧+调高音量 或<br>⌥+⇧+调低音量|以较小的步幅调节音量.
⌥+键盘调高亮度|打开*键盘*偏好设置.这个快捷键可与任一键盘亮度键搭配使用.
⌥+⇧+键盘调高亮度 或<br>⌥+⇧+键盘调低亮度|以较小的步幅调节键盘亮度.
连按 ⌥ 键|在单独的窗口中打开项目,然后关闭原始窗口.
连按 ⌘ 键|在单独的标签页或窗口中打开文件夹.
按住 ⌘ 键拖移到另一个宗卷|将拖移的项目移到另一个宗卷,而不是拷贝它.
按住 ⌥ 键拖移|拷贝托移的项目.拖移项目时指针会随之变化.
按住 ⌥+⌘ 键拖移|为拖移的项目制作替身.拖移项目时指针会随之变化.
按住 ⌥ 键点按开合三角|打开所选文件夹内的所有文件夹.此快捷键仅在列表视图中有效.
按住 ⌘ 键点按窗口标题|查看包含当前文件夹的文件夹.

[了解如何使用 ⌘  或⇧ 在*访达*中选择多个项目.](https://support.apple.com/zh-cn/guide/mac-help/select-items-mchlp1378/mac)

点按*访达*菜单栏中的*前往*菜单查看用于打开许多常用文件夹(如*应用程序*、*文稿*、*下载*、*实用工具*和*iCloud 云盘*)的快捷键.

### 文稿快捷键

这些快捷键的行为可能因使用的应用而异.

快捷键|作用
-|-
⌘+B|以粗体显示所选文本,或者打开或关闭粗体显示功能.
⌘+I|以斜体显示所选文本,或者打开或关闭斜体显示功能.
⌘+K|添加网页链接.
⌘+U|对所选文本加下划线,或者打开或关闭加下划线功能.
⌘+T|显示或隐藏*字体*窗口.
⌘+D|从*打开*对话框或*存储*对话框内选择*桌面*文件夹.
⌃+⌘+D|显示或隐藏所选字词的定义.
⇧+⌘+冒号 (:)|显示*拼写和语法*窗口.
⌘+分号 (;)|查找文稿中拼写错误的字词.
⌥+Delete|删除插入点左边的字词.
⌃+H|删除插入点左边的字符.也可以使用 Delete 键.
⌃+D|删除插入点右边的字符.也可以使用 Fn+Delete.
Fn+Delete|在没有向前删除   键的键盘上向前删除.也可以使用 ⌃+D.
⌃+K|删除插入点与行或段落末尾处之间的文本.
Fn+上箭头|Page Up|向上滚动一页.
Fn+下箭头|Page Down|向下滚动一页.
Fn+左箭头|Home|滚动到文稿开头.
Fn+右箭头|End|滚动到文稿末尾.
⌘+上箭头|将插入点移至文稿开头.
⌘+下箭头|将插入点移至文稿末尾.
⌘+左箭头|将插入点移至当前行的行首.
⌘+右箭头|将插入点移至当前行的行尾.
⌥+左箭头|将插入点移至上一字词的词首.
⌥+右箭头|将插入点移至下一字词的词尾.
⇧+⌘+上箭头|选中插入点与文稿开头之间的文本.
⇧+⌘+下箭头|选中插入点与文稿末尾之间的文本.
⇧+⌘+左箭头|选中插入点与当前行行首之间的文本.
⇧+⌘+右箭头|选中插入点与当前行行尾之间的文本.
⇧+上箭头|将文本选择范围扩展到上一行相同水平位置的最近字符处.
⇧+下箭头|将文本选择范围扩展到下一行相同水平位置的最近字符处.
⇧+左箭头|将文本选择范围向左扩展一个字符.
⇧+右箭头|将文本选择范围向右扩展一个字符.
⌥+⇧+上箭头|将文本选择范围扩展到当前段落的段首,再按一次则扩展到下一段落的段首.
⌥+⇧+下箭头|将文本选择范围扩展到当前段落的段尾,再按一次则扩展到下一段落的段尾.
⌥+⇧+左箭头|将文本选择范围扩展到当前字词的词首,再按一次则扩展到后一字词的词首.
⌥+⇧+左箭头|将文本选择范围扩展到当前字词的词尾,再按一次则扩展到后一字词的词尾.
⌃+A|移至行或段落的开头.
⌃+E|移至行或段落的末尾.
⌃+F|向前移动一个字符.
⌃+B|向后移动一个字符.
⌃+L|将光标或所选内容置于可见区域中央.
⌃+P|上移一行.
⌃+N|下移一行.
⌃+O|在插入点后新插入一行.
⌃+T|将插入点后面的字符与插入点前面的字符交换.
⌘+左花括号 ({)|左对齐.
⌘+右花括号 (})|右对齐.
⇧+⌘+竖线 (\|)|居中对齐.
⌥+⌘+F|前往搜索栏.
⌥+⌘+T|显示或隐藏应用中的工具栏.
⌥+⌘+C|拷贝样式 将所选项的格式设置拷贝到剪贴板.
⌥+⌘+V|粘贴样式 将拷贝的样式应用到所选项.
⌥+⇧+⌘+V|粘贴并匹配样式|将周围内容的样式应用到粘贴在该内容中的项目.
⌥+⌘+I|显示或隐藏检查器窗口.
⇧+⌘+P|页面设置 显示用于选择文稿设置的窗口.
⇧+⌘+S|显示*存储为*对话框或复制当前文稿.
⇧+⌘+减号 (+)|缩小所选项.
⇧+⌘+加号 (+)|放大所选项.⌘+等号 (=) 可实现相同的功能.
⇧+⌘+问号 (?)|打开*帮助*菜单.

### 自定义App快捷键

> 用于针对app内根据操作项名称设置快捷键.

需注意的是这里的操作项名称是指顶部菜单栏中的各选项名称.

比如为safair浏览器打开审查元素设置快捷键,如果将快捷键名称设置为页面右键选项中的 *审查元素是没用的*.

需要设置 顶部菜单栏中开发里面叫做 *显示网页检查器* 操作项名称才可以.

## 时间机器

### 基于 Linux(Ubuntu) 搭建 Time Machine 服务器

基本流程:

0. 搭建服务分享一个文件夹 
0. 在 Mac 上加载这个文件夹
0. 利用这个文件夹创建备份

#### 基于Netatalk

##### Netatalk

> [netatalk](http://netatalk.sourceforge.net)用来把Linux/Unix伪装成AFP协议传输的文件服务器.

***考虑到 netatalk 近两年更新频率较低, 放弃该方案.***

```
sudo apt-get install netatalk
```

修改Netatalk的配置文件:

```bash
sudo nano /etc/netatalk/AppleVolumes.default
# 在末尾加上:
硬盘挂载路径 "别名" options:tm
```

重启Netatalk服务:

```
sudo service netatalk restart
```

##### avahi-daemon

> 用来将部署的文件服务器伪装成变成了一台Mac文件服务器.

```
sudo apt-get install avahi-daemon
```

创建服务:

```
sudo vim /etc/avahi/services/afpd.service
```

粘入:

```xml
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
	<name replace-wildcards="yes">%h</name>
	<service>
    <type>_afpovertcp._tcp</type>
    <port>548</port>
  </service>
  <service>
    <type>_device-info._tcp</type>
      <port>0</port>
    <txt-record>model=Xserve</txt-record>
  </service>
</service-group>
```

重启avahi-daemon服务:

```
sudo service avahi-daemon restart
```

##### nss-mdns

> 用来配合刚刚设置好的avahi-daemon.

```
sudo apt-get install libnss-mdns
```

修改nss-mdns配置文件:

```
sudo vim /etc/nsswitch.conf
```

在「hosts」这一行的结尾添加两项,「mdns4」和「mdns」:

![](https://pic4.zhimg.com/80/v2-2a754f13d4d47d1b0c64c657f1187d13_720w.jpg)

再次重启avahi-daemon服务:

```
sudo service avahi-daemon restart
```

#### 基于 Samba

##### 安装 Samba

参考 Linux.md 中内容.

##### 连接服务器

Finder中 CMD+K

```
smb://主机IP或主机名/共享名   
smb://192.168.6.2/TimeMachine   
```

##### 创建磁盘镜像文件

打开「磁盘工具」,选择菜单栏中的 「新建映像」,选择「空白映像…」,然后如图所示,填入信息:

![](https://cdn.sspai.com/2019/11/23/a1423b8bf9d17375318cce2283a051fc.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)

其中「大小」可以根据实际需求来填写.

##### 将空白映像拷贝到共享文件夹

在 finder 中先推出这个磁盘(他现在是在你保存的位置上,如果你的设置如上图,那么这个磁盘就是在桌面),然后在保存的位置中,将这个磁盘文件拖入 SMB 共享文件夹的对应位置:

![](https://cdn.sspai.com/2019/11/23/3d74c204143d6e2bede0f8f9684cd1ed.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)

##### 挂载磁盘

双击在 SMB 共享文件夹中的这个映像文件,然后他就会挂在在你的 Mac 上:

![](https://cdn.sspai.com/2019/11/23/7efcb6b7ab734d30c9f38b0f2165452f.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)

##### 将磁盘设置为备份磁盘

通过 terminal 将这个磁盘设置为 TimeMachine 的备份磁盘:

```
sudo tmutil setdestination /磁盘挂载点
```

在磁盘工具中,选择这个磁盘,点击右键,选择「显示简介」,看到挂载信息:

![](https://cdn.sspai.com/2019/11/23/686bfb6c3be4bdcd3bbc0d883dbfb8dc.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)

输入完命令后,再输入你的Mac密码,即可成功挂载.

###### Invalid destination path

> 挂载路径格式错误.

> ***注意是挂载点(目录路径) 不是磁盘映像地址(/Volumes/TimeMachine/\*\*.sparseimage),简介中没有显示的话 磁盘工具中有.***

```
/Volumes/TimeMachine/**.sparseimage: Invalid destination path. (error 22)
The backup destination could not be set.
```

### 本地备份管理

#### 查看

```
sudo tmutil listlocalsnapshots /

Snapshots for volume group containing disk /:
tmutil deletelocalsnapshots 2020-09-29-135206
tmutil deletelocalsnapshots 2020-10-09-092403
tmutil deletelocalsnapshots 2020-10-09-102409
tmutil deletelocalsnapshots 2020-10-09-115734
tmutil deletelocalsnapshots 2020-10-09-135441
tmutil deletelocalsnapshots 2020-10-09-145444
tmutil deletelocalsnapshots 2020-10-09-155413
```

#### 删除

删除只需要给出时间戳即可:

```
tmutil deletelocalsnapshots 2020-09-29-135206
Deleted local snapshot '2020-09-29-135206'
```

# 常见问题

## xcrun

### invalid active developer path

> xcrun: error: invalid active developer path (/Library/Developer/⌘LineTools), missing xcrun at: /Library/Developer/⌘LineTools/usr/bin/xcrun

解决方法,重装xcode:

```
xcode-select --install
```

如果没有解决问题,执行以下命令

```
sudo xcode-select -switch /
```

# 参考资料

> [CSDN | iTerm2牛逼的功能](https://blog.csdn.net/w893932747/article/details/86672089)

> [简书 | 解决MacOS升级后出现xcrun: error: invalid active developer path, missing xcrun的问题](https://www.jianshu.com/p/50b6771eb853)

> [苹果 | Mac 键盘快捷键](https://support.apple.com/zh-cn/HT201236)

> [苹果 |《Safari 浏览器使用手册》](https://support.apple.com/zh-cn/guide/safari/cpsh003/mac)

> [STORM SPIRIT |你应该知道的 iTerm2 使用方法--MAC终端工具](http://wulfric.me/2015/08/iterm2/)

> [segmentfault | Mac vscode快捷键](https://segmentfault.com/a/1190000012811886)

> [知乎 | 搭建Time Machine 服务器](https://zhuanlan.zhihu.com/p/31088141)

> [少数派 | 树莓派、Windows 设备都可以做你 Mac 的「时间机器」——利用 SMB 协议进行 Time Machine 备份](https://sspai.com/post/57539)

> [简书 | Mac OS X 访问连接SAMBA共享磁盘](https://www.jianshu.com/p/4f785ae6c29c)

> [维基百科 | DiskImageMounter](https://zh.wikipedia.org/wiki/DiskImageMounter)

> [简书 | 在Mac OS X 自定义快捷键打开应用](https://www.jianshu.com/p/aa69812b2303)

> [pdflabs | pdftk-server](https://www.pdflabs.com/tools/pdftk-server/)

> [科学网 | [转载]pdftk常用命令](http://blog.sciencenet.cn/blog-562867-1193616.html)

> [简书 | Mac 设置默认打开方式](https://www.jianshu.com/p/6bf9005ccda1)

> [码农家园 | 关于macos:Mac上的Sed正则表达式问题,在Linux上运行正常](https://www.codenong.com/6761361/)

> [Github | fswatch](https://github.com/emcrisostomo/fswatch)

> [stackexchange | Time in milliseconds since epoch in the terminal](https://apple.stackexchange.com/questions/135742/time-in-milliseconds-since-epoch-in-the-terminal)

> [掘金 | [译] 在 macOS 上升级 Bash](https://juejin.im/post/6844903972294262791)

> [少数派 | Mac 录屏没有声音？试试用 Soundflower 解决｜一日一技](https://sspai.com/post/59854)

> [知乎 | MAC如何用QuickTime屏录,同时保留声音？](https://zhuanlan.zhihu.com/p/37365718)

> [知乎 | 清理 TimeMachine 本地快照的方法](https://zhuanlan.zhihu.com/p/39689057)

> [CSDN | Mac重置蓝牙模块](https://blog.csdn.net/h1101723183/article/details/52083499)

> [CSDN | mac命令行方式操作DNS](https://blog.csdn.net/blackocular/article/details/77220035)

> [liyajun | macOS下如何将U盘格式化为ext4格式](https://www.liyajun.com/format-ext4/)