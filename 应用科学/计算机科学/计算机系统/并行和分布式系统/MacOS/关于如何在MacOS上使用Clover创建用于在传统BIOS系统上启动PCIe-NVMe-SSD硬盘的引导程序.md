<!-- TOC -->

- [步骤](#步骤)
  - [下载安装包(pkg)](#下载安装包pkg)
  - [格式化USB](#格式化usb)
  - [在USB上安装引导加载程序](#在usb上安装引导加载程序)
  - [安装后任务](#安装后任务)
  - [在Clover引导加载程序中配置持久设置](#在clover引导加载程序中配置持久设置)
    - [从USB启动Clover引导加载程序](#从usb启动clover引导加载程序)
    - [查看USB上的日志文件](#查看usb上的日志文件)
    - [更新CONFIG.PLIST文件](#更新configplist文件)
    - [要验证其是否已正确更新](#要验证其是否已正确更新)
  - [完全跳过引导加载程序](#完全跳过引导加载程序)
  - [完毕](#完毕)
- [参考资料](#参考资料)

<!-- /TOC -->


# 步骤

## 下载安装包(pkg)

在[版本库](https://github.com/CloverHackyColor/CloverBootloader/releases/)下载Clover_rXXXX.pkg安装包.

## 格式化USB

在格式化 USB 之前,通过运行`diskutil list`命令获取正确的磁盘标识符非常重要.

将格式化 USB 并将卷命名为CLOVER_USB. 完成后应自行重新安装 USB.

```sh
diskutil eraseDisk FAT32 CLOVER_USB 磁盘标识符
```

## 在USB上安装引导加载程序

打开Clover_rXXXX.pkg以开始安装向导.<br>
按照屏幕上的说明继续操作,直到到达“安装类型”步骤.<br>
单击“更改安装位置... ”并选择CLOVER_USB介质.<br>
然后“继续”.

将返回到“安装类型”屏幕,这次点击“自定义”.

确保选择以下软件包来安装它们:

* Boot Sectors > Install `boot0af` in MBR
* BIOS Drivers, 64 bit > File Systems drivers [全选] ☑️
* UEFI Drivers > File System drivers [全选] ☑️
* UEFI Drivers > Additional drivers [全选] ☑️
* CloverConfigPlistValidator

选择完成后,单击“安装”,应该不会超过一分钟左右,然后就应该完成向导的操作.

## 安装后任务

将路径中安装的驱动程序复制EFI/CLOVER/drivers/UEFI到该EFI/CLOVER/drivers/BIOS路径.<br>
必须拥有NvmExpressDxe.efi驱动程序,以便引导加载程序可以检测传统 BIOS 系统上的 PCIe NVMe SSD.

```sh
cp -n /Volumes/CLOVER_USB/EFI/CLOVER/drivers/UEFI/* /Volumes/CLOVER_USB/EFI/CLOVER/drivers/BIOS
```

复制文件后,就完成了引导加载程序 USB 的创建.现在可以卸载.

## 在Clover引导加载程序中配置持久设置

> 在实际的旧版 BIOS 系统上启动 Clover 引导加载程序后,<br>
可能已经注意到,每次都必须手动选择所需的卷,因为它不会自动为引导检测到的卷.<br>
这是预期的行为,因为必须进行一些自定义.

### 从USB启动Clover引导加载程序

首先,需要在旧版 BIOS 系统上启动引导加载程序.<br>
到达Clover 操作系统启动管理器主屏幕后,应该能够查看一些选项.<br>
按F1键查看帮助菜单.

会看到可以将现有内容保存preboot.log到EFI/CLOVER/misc/目录中.<br>
按F2键将其保存preboot.log到USB,因为后续步骤将需要它.<br>
可能需要再按几次以确保其已保存.

在继续关闭计算机之前,可能需要突出显示列表中的卷(实际上不是单击它,因为它会引导到操作系统),然后按空格键以显示UUID信息.<br>
只需简单记住第一个和最后几个字符,这样就可以自信地确保引用正确的UUID,因为稍后会在文件中找到它preboot.log,以便方便地复制和粘贴.

完成后,可以硬关机,然后拔掉 USB.

### 查看USB上的日志文件

```sh
sed -n '/ScanLoader/,/AddCustomTool/p' /Volumes/CLOVER_USB/EFI/CLOVER/misc/preboot.log
```

如果没有显示任何内容,那么可能需要通过运行来手动检查该文件:
```sh
less /Volumes/CLOVER_USB/EFI/CLOVER/misc/preboot.log
```
如果它为空,则很可能没有按照上一节中的说明按F2.

如果看到输出,则需要查看:

```sh
=== [ ScanLoader ] ======================
# 在块内,应该能够看到(XX是数字):
- [XX]: 'SOME RANDOM NAME'
=== [ AddCustomTool ] ===================
```

需要查找的实际行从以下位置开始,Loader entry created for因为每行都包含 UUID.<br>
以下是 UUID 的示例57272A5A-7EFE-4404-9CDA-C33761D0DB3C.<br>
需要复制在上一步中识别的 UUID,以便稍后粘贴到下一部分中.

### 更新CONFIG.PLIST文件

```sh
YOUR_UUID=57272A5A-7EFE-4404-9CDA-C33761D0DB3C
sed -i.bkup "s/LastBootedVolume/${YOUR_UUID}/" /Volumes/CLOVER_USB/EFI/CLOVER/config.plist
```

如果搞砸了(例如错误的 UUID 值)或者由于卷被重新格式化而需要再次设置新的 UUID,可以通过运行以下命令来简单地恢复,而不是从头开始.

```sh
cp /Volumes/CLOVER_USB/EFI/CLOVER/config.plist.bkup /Volumes/CLOVER_USB/EFI/CLOVER/config.plist
```

DefaultVolume请注意,下次重新格式化首选卷时(例如在 SSD 上重新安装操作系统),需要再次更新键值,因为每次都会创建新的 UUID.

### 要验证其是否已正确更新

```sh
grep -A 1 DefaultVolume /Volumes/CLOVER_USB/EFI/CLOVER/config.plist
```

## 完全跳过引导加载程序

Clover 引导加载程序需要五秒钟才能引导到首选卷.<br>
但如果想完全跳过引导加载程序,则需要将config.plist中Timeout值更改为0.<br>
如果超时整数设置为0,则在开机时按住任意键可调用引导加载程序 GUI.

## 完毕

现在一切都准备好了,可以卸载USB.

# 参考资料

> [bashtheshell | How to Create Clover Bootloader USB on macOS (for PCIe NVMe SSD booting on Legacy BIOS system):](https://gist.github.com/bashtheshell/a26a94964e79b7c838758920c7940971)