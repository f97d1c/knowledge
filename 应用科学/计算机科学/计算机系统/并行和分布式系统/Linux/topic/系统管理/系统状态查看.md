> 平均负载

平均负载是指在以 `单核负载` 正常水平 `1.0` 为指标去衡量当前系统负载状况的名词.

系统正常水平应当为 *CPU核心数n\*1.0* 即 n.0

平均负载值(系统负载值/核心数)|状态
-|-|
\>0.7|系统负载较低,无需进行优化.
0.7~1.0|注意异常进程,准备有效的降低负载措施.
<1.0|系统负载过高,着手降低负载.

**这里的负载值一般以15分钟和5分钟的负载值作为主要参考指标**.

> 查看CPU核心数

*总核数 = 物理CPU个数 * 每颗物理CPU的核数*

*总逻辑CPU数 = 物理CPU个数 * 每颗物理CPU的核数 * 超线程数*

```bash
# 查看CPU物理个数
grep 'physical id' /proc/cpuinfo | sort -u
# => physical id	: 0

# 查看每个物理CPU的核数
grep 'core id' /proc/cpuinfo | sort -u | wc -l
# => 2

# 查看总的逻辑CPU个数
grep 'processor' /proc/cpuinfo | sort -u | wc -l
# => 4 
```
<!-- TOC -->

- [系统运行时间](#系统运行时间)
  - [uptime](#uptime)
- [系统版本相关信息](#系统版本相关信息)
  - [uname](#uname)
- [目录和文件占用空间](#目录和文件占用空间)
  - [du](#du)
- [各挂载点空间](#各挂载点空间)
  - [df](#df)
- [内存可用情况](#内存可用情况)
  - [free](#free)
- [虚拟内存统计信息](#虚拟内存统计信息)
  - [vmstat](#vmstat)
- [网络连接状态](#网络连接状态)
  - [telnet](#telnet)
- [进程间关系](#进程间关系)
  - [pstree](#pstree)
- [参考资料](#参考资料)

<!-- /TOC -->

# 系统运行时间

## uptime

```bash
uptime
# 当前时间   已运行时间,  用户登录数, 1分钟, 5分钟, 15分钟内系统的平均负载
# => 22:51:18 up 5 min,  1 user,   load average: 0.69, 0.68, 0.34
```

# 系统版本相关信息

## uname

```bash
uname -a
# => Linux Nood 4.15.0-36-generic #39-Ubuntu SMP Mon Sep 24 16:19:09 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
```

# 目录和文件占用空间

## du

显示当前目录下每个目录及其文件占用空间.

```bash
# --max-depth 指定显示层级
du -h --max-depth=1 
# =>
8.0K	./TODO
140K	./Linux
6.2M	./FrontEnd
1.9M	./BackEnd
35M	./.git
44K	./DB
56K	./Other
43M	.
```
# 各挂载点空间

## df

查看各个挂载点总空间以及可用空间.

```bash
df -h
# =>
文件系统        容量     已用  可用  已用% 挂载点
udev            3.9G     0  3.9G    0% /dev
tmpfs           789M  1.6M  787M    1% /run
/dev/sda1       110G  9.3G   95G    9% /
tmpfs           3.9G   13M  3.9G    1% /dev/shm
tmpfs           5.0M  4.0K  5.0M    1% /run/lock
tmpfs           3.9G     0  3.9G    0% /sys/fs/cgroup
tmpfs           789M   64K  789M    1% /run/user/1000
```
# 内存可用情况

## free

```bash
free
# =>
              总计         已用        空闲      共享        缓冲/缓存    可用
内存:     8070440     2246328     3988972      455032     1835140     5081536
交换:     2097148           0     2097148
```

# 虚拟内存统计信息

## vmstat

```bash
vmstat
# =>
procs -----------memory----------  ---swap-- -----io---- -system-- ------cpu-----
 r  b 交换  空闲    缓冲    缓存     si   so    bi    bo   in   cs  us sy id wa st
 0  0  0 3964476 101264 1755592    0    0    84    26  157   653  6  2 91  0  0
```

参数|说明|异常点
-|-|-|
si|每秒从交换区写到内存的大小|经常大于0,说明内存可能不够用
so|每秒写入交换区的内存大小|经常大于0,说明内存可能不够用
us|用户进程执行时间百分比|值比较高时,说明用户进程消耗的CPU资源较多
wa|IO等待时间百分比|值较高时,说明IO等待情况严重


# 网络连接状态

## telnet

常用来确定远程服务器的某个端口是否能访问.

```bash
telnet ip port
```
# 进程间关系

## pstree

查看到进程间的父子关系.

# 参考资料

> [微信文章 | Linux常用命令--系统状态篇](https://mp.weixin.qq.com/s/1OXg1QBypTM5lXgyfOLI0g)


















