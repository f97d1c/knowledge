<!-- TOC -->

- [什么是内网穿透](#什么是内网穿透)
  - [什么是内网](#什么是内网)
  - [什么是外网](#什么是外网)
- [花生壳](#花生壳)
  - [下载安装包](#下载安装包)
  - [切换到root用户下](#切换到root用户下)
    - [安装](#安装)
    - [常用命令](#常用命令)
    - [卸载](#卸载)
  - [相关配置](#相关配置)
- [ngrok](#ngrok)
  - [注册帐号](#注册帐号)
  - [初始化](#初始化)
    - [下载](#下载)
    - [设置令牌](#设置令牌)
  - [设置监听端口](#设置监听端口)
- [参考资料](#参考资料)

<!-- /TOC -->

# 什么是内网穿透

> 外网与内网的计算机节点实现连接通信过程中,通过映射端口手段,<br>
让外网的电脑找到处于内网的电脑.

## 什么是内网

> 内网由内部建立的局域网络或办公网络,<br>
比如一家公司或一个家庭有多台电脑,就可以利用不同网络布局将多台电脑或其它设备连接起来构成一个局部范围的共享资源或者办公的网络,<br>
这样就可以称之为内网.

## 什么是外网

> 外网,就是通过一个网关或网桥与其它网络系统连接,<br>
相对于自己的内网来说,连接的其它网络系统就称为外部网络,也叫外网.

# 花生壳

## 下载安装包

在官网[下载页面](http://hsk.oray.com/download/)下载对应版本

## 切换到root用户下

### 安装

```bash
dpkg -i 软件包名称
```

### 常用命令

phddns [参数]

参数列表|作用
-|-
start|启动
stop|停止
restart|重启
status|查看状态(`SN码`)

### 卸载

```bash
dpkg -r phddns
```

## 相关配置

登录官网后添加设备通过`SN码`(启动成功后会显示) 及默认密码 *admin* 即可进行设备绑定.

内网穿透等后续在网页进行操作即可.

# ngrok

## 注册帐号

可通过github授权登陆.

## 初始化

### 下载

具体步骤参考[这个页面](https://dashboard.ngrok.com/get-started).

[Linux-amd64](https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip)

```bash
# X86_64
wget -t 5 -c https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip

unzip ngrok-stable-linux-amd64.zip

# arm 版
wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-arm.zip

unzip ngrok-stable-linux-arm.zip
```

### 设置令牌

在[这里](https://dashboard.ngrok.com/auth)获取账户的令牌并在文件目录下执行

## 设置监听端口

```bash
./ngrok http 端口号  
```

# 参考资料

> [ngrok官网](https://ngrok.com/)

> []()

> []()

> []()

> []()

> []()

> []()