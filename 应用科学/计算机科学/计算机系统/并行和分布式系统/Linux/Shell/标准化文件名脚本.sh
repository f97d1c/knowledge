
exit 255;

function standard_name(){
cat << EOF
{名称=>"$1", 标签=>["喜剧","科幻","美剧"], 类型=>["视频","电视剧"], 其他=>{系列名=>"太空部队", 所属季=>02}, 版本=>20211015}.$2
EOF
}

# 原始名称变更为标准文件
find *.mp4 -type f -print0 | while IFS= read -r -d $'\0' file; do 
  file_name=${file##*/}
  prue_name=${file_name%.*}
  suffix=${file##*.}
  new_name=$(echo "$prue_name" | sed "s/\[zxzj.fun\]//g")
  new_full_name=$(standard_name $new_name $suffix)
  echo "$new_full_name"
  eval "mv $(printf '%q' "$file_name") $(printf '%q' "$new_full_name")"
done

# 现有标准化文件名称变更
find *.mp4 -type f -print0 | while IFS= read -r -d $'\0' file; do 
  file_name=${file##*/}
  prue_name=${file_name%.*}
  suffix=${file##*.}
  origin_name=$(echo "$prue_name" | sed 's/{名称=>"//g' | sed 's/", 标签=>\[.*//g')
  new_full_name=$(standard_name $origin_name $suffix)
  echo "$new_full_name"
  eval "mv $(printf '%q' "$file_name") $(printf '%q' "$new_full_name")"
done


# 新增文件逐项提交
total_count=$(echo "$(git ls-files -o  --exclude-standard)" | wc -l)
index=0
echo "$(git ls-files -o  --exclude-standard)" | while IFS= read -r line || [ -n "$line" ]; do
  git push origin master
  if [ $? -ne 0 ];then 
    IFS=$'\n' array=($(git log --pretty=format:"%H" 2>/dev/null))
    if [ ${#array[@]} -gt 0 ];then 
      echo "[异常] Git 连接异常, 尝试重新连接."
      continue
    fi
  fi

  # 随机休眠30秒范围内任意数值
  # sleep_second=$(($RANDOM%30))
  # echo "休眠$sleep_second秒"
  # sleep $sleep_second
  index=$((index+=1))
  
  file_name=$(echo "$line" | sed 's/^\"//g' | sed 's/\"$//g' | sed 's/\\//g')
  IFS=' ' md5=($(md5sum "$file_name"))
  echo "添加文件: $file_name"
  git add "$file_name"
  git commit -m "自动化提交_${md5[0]}_$(date +%Y/%m/%dT%H:%M:%S)"
  echo "[$index/$total_count $(date +%Y/%m/%dT%H:%M:%S)]: 自动化提交_${md5[0]}_$(date +%Y/%m/%dT%H:%M:%S)"
  git push origin master
done

# 文件名改为MD5值并变更为标准文件

function standard_name(){
cat << EOF
{名称=>"$1", 标签=>["喜剧","搞笑"], 类型=>["视频","短视频"], 其他=>{}, 版本=>20211015}.$2
EOF
}

find *.mp4 -type f -print0 | while IFS= read -r -d $'\0' file; do 
  file_name=${file##*/}
  file_name=$(echo "$file_name" | sed 's/^\"//g' | sed 's/\"$//g' | sed 's/\\//g')
  prue_name=${file_name%.*}
  suffix=${file##*.}
  new_name=$(echo "$prue_name" | sed "s/\[zxzj.fun\]//g")
  md5=($(md5sum "$file_name"))
  new_full_name=$(standard_name ${md5[0]} $suffix)
  eval "mv $(printf '%q' "$file_name") $(printf '%q' "$new_full_name")"
done


# 根据正则查找匹配文件
find . -path "*版本\=\>20211015\}.ncm" -type f -print0 | while IFS= read -r -d $'\0' file; do 
  file_name=${file##*/}
  eval "mv $(printf '%q' "$file_name") ../003"
done


# 根据文件的最后一次提交commit_id按时间升序生成索引

function update_file_commit_index_old(){
  index_files=($(find /home/$(id -u -n) -path "*标准化文件名/流媒体文件索引.json" 2>/dev/null))
  index_file=${index_files[0]}

  file_index=$(mktemp)
  echo '{}' > $file_index

  find * -type f -print0 | while IFS= read -r -d $'\0' file; do 
    file_name=${file##*/}
    commit_id=$(git log -n 1 --pretty=format:%H -- "$file_name")
    if [ ! "$commit_id" ];then continue;fi
    result=$(jq --arg key $commit_id --arg value "$file_name" ' .["\($key)"] = $value' $file_index)
    jq -e '.' <<<"$result" 1>/dev/null
    if [[ $? -eq 0 ]]; then
      echo "$result" > $file_index
    else
      echo -e "[异常]: 跳过写入:\ncommit_id: $commit_id\nfile_name:$file_name"
    fi
  done

  IFS=$'\n' array=($(git log --pretty=format:"%H"))

  remote_key=$(git remote get-url --push $(git remote))

  echo "$(jq --arg key $remote_key ' .["\($key)"] = {}' $index_file)" > $index_file

  for (( index=${#array[@]}-1 ; index>=0 ; index-- )) ; do
    commit_id=${array[index]}
    value=$(jq -r --arg key $commit_id ' .["\($key)"]' $file_index)
    
    if [ "$value" == 'null' ];then
      echo "[跳过] 已过期ID: $commit_id"
      continue
    fi

    echo "$(jq --arg key $remote_key --arg commit_id $commit_id  --arg value $value ' .["\($key)"]["\($commit_id)"] = $value' $index_file)" > $index_file
  done

  rm -f $file_index
}

update_file_commit_index

# 根据流媒体索引更新仓库
rtsp_index_conf=$(curl -s 'https://gitlab.com/f2ce2b/resources/-/raw/master/%E6%95%B0%E6%8D%AE%E6%96%87%E4%BB%B6/%E6%A0%87%E5%87%86%E5%8C%96%E6%96%87%E4%BB%B6%E5%90%8D/%E6%B5%81%E5%AA%92%E4%BD%93%E6%96%87%E4%BB%B6%E7%B4%A2%E5%BC%95.json')

remote_key=$(git remote get-url --push $(git remote))

IFS=$'\n' commit_ids=($(jq -r --arg key $remote_key ' .["\($key)"] | keys_unsorted[]' <<<$rtsp_index_conf))

last_commit_id=$(git log --pretty=format:"%H" 2>/dev/null | head -n 1)

if [ "$last_commit_id" ];then
  for index in "${!commit_ids[@]}";do
    if [ "${commit_ids[$index]}" == "$last_commit_id" ];then
      unset commit_ids[$index]
      break
    else 
      unset commit_ids[$index]
    fi
  done
fi

for index in "${!commit_ids[@]}";do
  commit_id=${commit_ids[$index]}
  echo "[$index/${#commit_ids[@]}][$(date +%Y/%m/%dT%H:%M:%S)]: 开始下拉$commit_id"
  count=0
  while [ $count -lt 15 ];do
    git pull $(git remote) $commit_id
    if [[ $? -eq 0 ]]; then
      unset commit_ids[$index]
      count=$((count+=100))
    else
      echo "[$count]拉取失败,继续尝试拉取."
      count=$((count+=1))
    fi
  done
done
