<!-- TOC -->

- [说明](#%E8%AF%B4%E6%98%8E)
  - [新建shell](#%E6%96%B0%E5%BB%BAshell)
  - [注释](#%E6%B3%A8%E9%87%8A)
  - [多行注释](#%E5%A4%9A%E8%A1%8C%E6%B3%A8%E9%87%8A)
  - [执行远程网络脚本](#%E6%89%A7%E8%A1%8C%E8%BF%9C%E7%A8%8B%E7%BD%91%E7%BB%9C%E8%84%9A%E6%9C%AC)
    - [传递参数](#%E4%BC%A0%E9%80%92%E5%8F%82%E6%95%B0)
  - [登录未加载~/.bashrc文件](#%E7%99%BB%E5%BD%95%E6%9C%AA%E5%8A%A0%E8%BD%BDbashrc%E6%96%87%E4%BB%B6)
    - [检查默认shell](#%E6%A3%80%E6%9F%A5%E9%BB%98%E8%AE%A4shell)
    - [检查~/.profile文件](#%E6%A3%80%E6%9F%A5profile%E6%96%87%E4%BB%B6)
- [变量](#%E5%8F%98%E9%87%8F)
  - [定义变量](#%E5%AE%9A%E4%B9%89%E5%8F%98%E9%87%8F)
    - [命名规范](#%E5%91%BD%E5%90%8D%E8%A7%84%E8%8C%83)
  - [使用变量](#%E4%BD%BF%E7%94%A8%E5%8F%98%E9%87%8F)
  - [判断是否为空](#%E5%88%A4%E6%96%AD%E6%98%AF%E5%90%A6%E4%B8%BA%E7%A9%BA)
  - [拆分变量](#%E6%8B%86%E5%88%86%E5%8F%98%E9%87%8F)
  - [作用域](#%E4%BD%9C%E7%94%A8%E5%9F%9F)
  - [默认值](#%E9%BB%98%E8%AE%A4%E5%80%BC)
  - [运行时参数](#%E8%BF%90%E8%A1%8C%E6%97%B6%E5%8F%82%E6%95%B0)
    - [选项化处理](#%E9%80%89%E9%A1%B9%E5%8C%96%E5%A4%84%E7%90%86)
- [数据类型](#%E6%95%B0%E6%8D%AE%E7%B1%BB%E5%9E%8B)
  - [字符串](#%E5%AD%97%E7%AC%A6%E4%B8%B2)
    - [单引号](#%E5%8D%95%E5%BC%95%E5%8F%B7)
    - [双引号](#%E5%8F%8C%E5%BC%95%E5%8F%B7)
      - [字符串拼接](#%E5%AD%97%E7%AC%A6%E4%B8%B2%E6%8B%BC%E6%8E%A5)
    - [获取字符串长度](#%E8%8E%B7%E5%8F%96%E5%AD%97%E7%AC%A6%E4%B8%B2%E9%95%BF%E5%BA%A6)
    - [正则匹配](#%E6%AD%A3%E5%88%99%E5%8C%B9%E9%85%8D)
    - [切割为数组](#%E5%88%87%E5%89%B2%E4%B8%BA%E6%95%B0%E7%BB%84)
      - [示例-1](#%E7%A4%BA%E4%BE%8B-1)
      - [示例-2](#%E7%A4%BA%E4%BE%8B-2)
      - [回车符分隔](#%E5%9B%9E%E8%BD%A6%E7%AC%A6%E5%88%86%E9%9A%94)
    - [字符串大小写转换表达式](#%E5%AD%97%E7%AC%A6%E4%B8%B2%E5%A4%A7%E5%B0%8F%E5%86%99%E8%BD%AC%E6%8D%A2%E8%A1%A8%E8%BE%BE%E5%BC%8F)
    - [获取字符串MD5值](#%E8%8E%B7%E5%8F%96%E5%AD%97%E7%AC%A6%E4%B8%B2md5%E5%80%BC)
    - [蛇形转驼峰格式](#%E8%9B%87%E5%BD%A2%E8%BD%AC%E9%A9%BC%E5%B3%B0%E6%A0%BC%E5%BC%8F)
    - [截取字符串](#%E6%88%AA%E5%8F%96%E5%AD%97%E7%AC%A6%E4%B8%B2)
    - [grep](#grep)
      - [条件或](#%E6%9D%A1%E4%BB%B6%E6%88%96)
    - [关于EOF](#%E5%85%B3%E4%BA%8Eeof)
    - [JSON字符串处理jq](#json%E5%AD%97%E7%AC%A6%E4%B8%B2%E5%A4%84%E7%90%86jq)
      - [默认值//](#%E9%BB%98%E8%AE%A4%E5%80%BC)
      - [格式化输出.](#%E6%A0%BC%E5%BC%8F%E5%8C%96%E8%BE%93%E5%87%BA)
        - [单行输出](#%E5%8D%95%E8%A1%8C%E8%BE%93%E5%87%BA)
      - [变量](#%E5%8F%98%E9%87%8F)
        - [变量值转数字tonumber](#%E5%8F%98%E9%87%8F%E5%80%BC%E8%BD%AC%E6%95%B0%E5%AD%97tonumber)
      - [关键值.关键字/."关键字"](#%E5%85%B3%E9%94%AE%E5%80%BC%E5%85%B3%E9%94%AE%E5%AD%97%E5%85%B3%E9%94%AE%E5%AD%97)
      - [数组操作.[], .[]?, .[2], .[10:15]](#%E6%95%B0%E7%BB%84%E6%93%8D%E4%BD%9C--2-1015)
      - [数组/对象构造[], {}](#%E6%95%B0%E7%BB%84%E5%AF%B9%E8%B1%A1%E6%9E%84%E9%80%A0-)
      - [值的长度length](#%E5%80%BC%E7%9A%84%E9%95%BF%E5%BA%A6length)
      - [数组中的键keys](#%E6%95%B0%E7%BB%84%E4%B8%AD%E7%9A%84%E9%94%AEkeys)
        - [令人费解的排序问题](#%E4%BB%A4%E4%BA%BA%E8%B4%B9%E8%A7%A3%E7%9A%84%E6%8E%92%E5%BA%8F%E9%97%AE%E9%A2%98)
      - [输入多个过滤器,](#%E8%BE%93%E5%85%A5%E5%A4%9A%E4%B8%AA%E8%BF%87%E6%BB%A4%E5%99%A8)
      - [管道输出|](#%E7%AE%A1%E9%81%93%E8%BE%93%E5%87%BA)
      - [数组遍历map](#%E6%95%B0%E7%BB%84%E9%81%8D%E5%8E%86map)
      - [条件筛选selectfoo](#%E6%9D%A1%E4%BB%B6%E7%AD%9B%E9%80%89selectfoo)
        - [筛选结果转为数组](#%E7%AD%9B%E9%80%89%E7%BB%93%E6%9E%9C%E8%BD%AC%E4%B8%BA%E6%95%B0%E7%BB%84)
      - [条件句if-then-else-end](#%E6%9D%A1%E4%BB%B6%E5%8F%A5if-then-else-end)
      - [字符串插值](#%E5%AD%97%E7%AC%A6%E4%B8%B2%E6%8F%92%E5%80%BC)
      - [改](#%E6%94%B9)
        - [json变量argjson](#json%E5%8F%98%E9%87%8Fargjson)
      - [输出结果与column配合打印输出](#%E8%BE%93%E5%87%BA%E7%BB%93%E6%9E%9C%E4%B8%8Ecolumn%E9%85%8D%E5%90%88%E6%89%93%E5%8D%B0%E8%BE%93%E5%87%BA)
      - [截取数组元素个数](#%E6%88%AA%E5%8F%96%E6%95%B0%E7%BB%84%E5%85%83%E7%B4%A0%E4%B8%AA%E6%95%B0)
    - [逐行读取字符串内容](#%E9%80%90%E8%A1%8C%E8%AF%BB%E5%8F%96%E5%AD%97%E7%AC%A6%E4%B8%B2%E5%86%85%E5%AE%B9)
    - [Base64编码](#base64%E7%BC%96%E7%A0%81)
  - [数字](#%E6%95%B0%E5%AD%97)
    - [浮点运算](#%E6%B5%AE%E7%82%B9%E8%BF%90%E7%AE%97)
      - [精确到指定小数位数](#%E7%B2%BE%E7%A1%AE%E5%88%B0%E6%8C%87%E5%AE%9A%E5%B0%8F%E6%95%B0%E4%BD%8D%E6%95%B0)
    - [格式化](#%E6%A0%BC%E5%BC%8F%E5%8C%96)
    - [随机数](#%E9%9A%8F%E6%9C%BA%E6%95%B0)
  - [数组](#%E6%95%B0%E7%BB%84)
    - [定义数组](#%E5%AE%9A%E4%B9%89%E6%95%B0%E7%BB%84)
    - [读取数组](#%E8%AF%BB%E5%8F%96%E6%95%B0%E7%BB%84)
    - [获取数组长度](#%E8%8E%B7%E5%8F%96%E6%95%B0%E7%BB%84%E9%95%BF%E5%BA%A6)
    - [数组遍历](#%E6%95%B0%E7%BB%84%E9%81%8D%E5%8E%86)
    - [删除元素](#%E5%88%A0%E9%99%A4%E5%85%83%E7%B4%A0)
    - [脚本参数转数组](#%E8%84%9A%E6%9C%AC%E5%8F%82%E6%95%B0%E8%BD%AC%E6%95%B0%E7%BB%84)
    - [拼接字符串](#%E6%8B%BC%E6%8E%A5%E5%AD%97%E7%AC%A6%E4%B8%B2)
    - [指定范围](#%E6%8C%87%E5%AE%9A%E8%8C%83%E5%9B%B4)
  - [时间](#%E6%97%B6%E9%97%B4)
    - [获取指定时间](#%E8%8E%B7%E5%8F%96%E6%8C%87%E5%AE%9A%E6%97%B6%E9%97%B4)
      - [获取昨日](#%E8%8E%B7%E5%8F%96%E6%98%A8%E6%97%A5)
      - [今天是当前周的第几天](#%E4%BB%8A%E5%A4%A9%E6%98%AF%E5%BD%93%E5%89%8D%E5%91%A8%E7%9A%84%E7%AC%AC%E5%87%A0%E5%A4%A9)
      - [获取本周指定时间](#%E8%8E%B7%E5%8F%96%E6%9C%AC%E5%91%A8%E6%8C%87%E5%AE%9A%E6%97%B6%E9%97%B4)
      - [本月第一天](#%E6%9C%AC%E6%9C%88%E7%AC%AC%E4%B8%80%E5%A4%A9)
      - [本月最后一天](#%E6%9C%AC%E6%9C%88%E6%9C%80%E5%90%8E%E4%B8%80%E5%A4%A9)
  - [文件](#%E6%96%87%E4%BB%B6)
    - [临时文件](#%E4%B8%B4%E6%97%B6%E6%96%87%E4%BB%B6)
      - [临时文件的安全问题](#%E4%B8%B4%E6%97%B6%E6%96%87%E4%BB%B6%E7%9A%84%E5%AE%89%E5%85%A8%E9%97%AE%E9%A2%98)
      - [临时文件的最佳实践](#%E4%B8%B4%E6%97%B6%E6%96%87%E4%BB%B6%E7%9A%84%E6%9C%80%E4%BD%B3%E5%AE%9E%E8%B7%B5)
      - [安全创建临时文件mktemp](#%E5%AE%89%E5%85%A8%E5%88%9B%E5%BB%BA%E4%B8%B4%E6%97%B6%E6%96%87%E4%BB%B6mktemp)
        - [创建临时目录-d](#%E5%88%9B%E5%BB%BA%E4%B8%B4%E6%97%B6%E7%9B%AE%E5%BD%95-d)
        - [指定临时文件所在的目录-p](#%E6%8C%87%E5%AE%9A%E4%B8%B4%E6%97%B6%E6%96%87%E4%BB%B6%E6%89%80%E5%9C%A8%E7%9A%84%E7%9B%AE%E5%BD%95-p)
        - [指定临时文件的文件名模板-t](#%E6%8C%87%E5%AE%9A%E4%B8%B4%E6%97%B6%E6%96%87%E4%BB%B6%E7%9A%84%E6%96%87%E4%BB%B6%E5%90%8D%E6%A8%A1%E6%9D%BF-t)
    - [读取文件](#%E8%AF%BB%E5%8F%96%E6%96%87%E4%BB%B6)
      - [缺失最后一行](#%E7%BC%BA%E5%A4%B1%E6%9C%80%E5%90%8E%E4%B8%80%E8%A1%8C)
    - [获取文件行数wc](#%E8%8E%B7%E5%8F%96%E6%96%87%E4%BB%B6%E8%A1%8C%E6%95%B0wc)
    - [获取文件夹内最新文件名称](#%E8%8E%B7%E5%8F%96%E6%96%87%E4%BB%B6%E5%A4%B9%E5%86%85%E6%9C%80%E6%96%B0%E6%96%87%E4%BB%B6%E5%90%8D%E7%A7%B0)
    - [路径存在空格情况](#%E8%B7%AF%E5%BE%84%E5%AD%98%E5%9C%A8%E7%A9%BA%E6%A0%BC%E6%83%85%E5%86%B5)
    - [awk](#awk)
      - [镜像列表内容处理的实践](#%E9%95%9C%E5%83%8F%E5%88%97%E8%A1%A8%E5%86%85%E5%AE%B9%E5%A4%84%E7%90%86%E7%9A%84%E5%AE%9E%E8%B7%B5)
      - [引用外部变量](#%E5%BC%95%E7%94%A8%E5%A4%96%E9%83%A8%E5%8F%98%E9%87%8F)
      - [文件名称的特殊字符转义](#%E6%96%87%E4%BB%B6%E5%90%8D%E7%A7%B0%E7%9A%84%E7%89%B9%E6%AE%8A%E5%AD%97%E7%AC%A6%E8%BD%AC%E4%B9%89)
- [运算符](#%E8%BF%90%E7%AE%97%E7%AC%A6)
  - [基本运算符](#%E5%9F%BA%E6%9C%AC%E8%BF%90%E7%AE%97%E7%AC%A6)
    - [算术运算符](#%E7%AE%97%E6%9C%AF%E8%BF%90%E7%AE%97%E7%AC%A6)
    - [关系运算符](#%E5%85%B3%E7%B3%BB%E8%BF%90%E7%AE%97%E7%AC%A6)
    - [布尔运算符](#%E5%B8%83%E5%B0%94%E8%BF%90%E7%AE%97%E7%AC%A6)
    - [逻辑运算符](#%E9%80%BB%E8%BE%91%E8%BF%90%E7%AE%97%E7%AC%A6)
    - [字符串运算符](#%E5%AD%97%E7%AC%A6%E4%B8%B2%E8%BF%90%E7%AE%97%E7%AC%A6)
    - [浮点运算](#%E6%B5%AE%E7%82%B9%E8%BF%90%E7%AE%97)
    - [文件测试运算符](#%E6%96%87%E4%BB%B6%E6%B5%8B%E8%AF%95%E8%BF%90%E7%AE%97%E7%AC%A6)
      - [示例](#%E7%A4%BA%E4%BE%8B)
  - [特殊符号](#%E7%89%B9%E6%AE%8A%E7%AC%A6%E5%8F%B7)
    - [输入输出重定向](#%E8%BE%93%E5%85%A5%E8%BE%93%E5%87%BA%E9%87%8D%E5%AE%9A%E5%90%91)
      - [将右侧的单词传递给左侧命令的标准输入](#%E5%B0%86%E5%8F%B3%E4%BE%A7%E7%9A%84%E5%8D%95%E8%AF%8D%E4%BC%A0%E9%80%92%E7%BB%99%E5%B7%A6%E4%BE%A7%E5%91%BD%E4%BB%A4%E7%9A%84%E6%A0%87%E5%87%86%E8%BE%93%E5%85%A5)
      - [重定向深入讲解](#%E9%87%8D%E5%AE%9A%E5%90%91%E6%B7%B1%E5%85%A5%E8%AE%B2%E8%A7%A3)
      - [/dev/null 文件](#devnull-%E6%96%87%E4%BB%B6)
        - [脚本内容重定向](#%E8%84%9A%E6%9C%AC%E5%86%85%E5%AE%B9%E9%87%8D%E5%AE%9A%E5%90%91)
    - [管道符](#%E7%AE%A1%E9%81%93%E7%AC%A6)
      - [管道命令符](#%E7%AE%A1%E9%81%93%E5%91%BD%E4%BB%A4%E7%AC%A6)
      - [命令行的通配符](#%E5%91%BD%E4%BB%A4%E8%A1%8C%E7%9A%84%E9%80%9A%E9%85%8D%E7%AC%A6)
- [流程控制](#%E6%B5%81%E7%A8%8B%E6%8E%A7%E5%88%B6)
  - [if](#if)
    - [判断软件是否安装](#%E5%88%A4%E6%96%AD%E8%BD%AF%E4%BB%B6%E6%98%AF%E5%90%A6%E5%AE%89%E8%A3%85)
  - [for 循环](#for-%E5%BE%AA%E7%8E%AF)
  - [while 语句](#while-%E8%AF%AD%E5%8F%A5)
    - [无限循环](#%E6%97%A0%E9%99%90%E5%BE%AA%E7%8E%AF)
    - [循环内使用read](#%E5%BE%AA%E7%8E%AF%E5%86%85%E4%BD%BF%E7%94%A8read)
  - [until 循环](#until-%E5%BE%AA%E7%8E%AF)
  - [case](#case)
  - [跳出循环](#%E8%B7%B3%E5%87%BA%E5%BE%AA%E7%8E%AF)
  - [trap](#trap)
    - [使用 trap 捕获信号](#%E4%BD%BF%E7%94%A8-trap-%E6%8D%95%E8%8E%B7%E4%BF%A1%E5%8F%B7)
      - [系统信号](#%E7%B3%BB%E7%BB%9F%E4%BF%A1%E5%8F%B7)
- [函数](#%E5%87%BD%E6%95%B0)
  - [文件重命名mv](#%E6%96%87%E4%BB%B6%E9%87%8D%E5%91%BD%E5%90%8Dmv)
    - [显示详细信息-v](#%E6%98%BE%E7%A4%BA%E8%AF%A6%E7%BB%86%E4%BF%A1%E6%81%AF-v)
    - [仅在新路径不存在时移动-n](#%E4%BB%85%E5%9C%A8%E6%96%B0%E8%B7%AF%E5%BE%84%E4%B8%8D%E5%AD%98%E5%9C%A8%E6%97%B6%E7%A7%BB%E5%8A%A8-n)
  - [自定义函数](#%E8%87%AA%E5%AE%9A%E4%B9%89%E5%87%BD%E6%95%B0)
    - [参数](#%E5%8F%82%E6%95%B0)
  - [查找指定文件find](#%E6%9F%A5%E6%89%BE%E6%8C%87%E5%AE%9A%E6%96%87%E4%BB%B6find)
    - [指定文件夹层级](#%E6%8C%87%E5%AE%9A%E6%96%87%E4%BB%B6%E5%A4%B9%E5%B1%82%E7%BA%A7)
    - [排除某一目录](#%E6%8E%92%E9%99%A4%E6%9F%90%E4%B8%80%E7%9B%AE%E5%BD%95)
  - [打印输出](#%E6%89%93%E5%8D%B0%E8%BE%93%E5%87%BA)
    - [printf](#printf)
      - [清空屏幕](#%E6%B8%85%E7%A9%BA%E5%B1%8F%E5%B9%95)
      - [格式替代符](#%E6%A0%BC%E5%BC%8F%E6%9B%BF%E4%BB%A3%E7%AC%A6)
      - [转义序列](#%E8%BD%AC%E4%B9%89%E5%BA%8F%E5%88%97)
      - [字体颜色](#%E5%AD%97%E4%BD%93%E9%A2%9C%E8%89%B2)
        - [规则映射表](#%E8%A7%84%E5%88%99%E6%98%A0%E5%B0%84%E8%A1%A8)
  - [加载其他文件source](#%E5%8A%A0%E8%BD%BD%E5%85%B6%E4%BB%96%E6%96%87%E4%BB%B6source)
    - [以超管身份加载脚本](#%E4%BB%A5%E8%B6%85%E7%AE%A1%E8%BA%AB%E4%BB%BD%E5%8A%A0%E8%BD%BD%E8%84%9A%E6%9C%AC)
    - [判断文件内容是否已加载](#%E5%88%A4%E6%96%AD%E6%96%87%E4%BB%B6%E5%86%85%E5%AE%B9%E6%98%AF%E5%90%A6%E5%B7%B2%E5%8A%A0%E8%BD%BD)
    - [判断脚本是否为source执行BASH_SOURCE](#%E5%88%A4%E6%96%AD%E8%84%9A%E6%9C%AC%E6%98%AF%E5%90%A6%E4%B8%BAsource%E6%89%A7%E8%A1%8Cbash_source)
  - [调用其他脚本fork, exec, source](#%E8%B0%83%E7%94%A8%E5%85%B6%E4%BB%96%E8%84%9A%E6%9C%ACfork-exec-source)
  - [交互式](#%E4%BA%A4%E4%BA%92%E5%BC%8F)
    - [读取命令行输入read](#%E8%AF%BB%E5%8F%96%E5%91%BD%E4%BB%A4%E8%A1%8C%E8%BE%93%E5%85%A5read)
  - [参数项分隔xargs](#%E5%8F%82%E6%95%B0%E9%A1%B9%E5%88%86%E9%9A%94xargs)
    - [多行输出](#%E5%A4%9A%E8%A1%8C%E8%BE%93%E5%87%BA)
    - [](#)
    - [多进程并行执行--max-procs 进程数](#%E5%A4%9A%E8%BF%9B%E7%A8%8B%E5%B9%B6%E8%A1%8C%E6%89%A7%E8%A1%8C--max-procs-%E8%BF%9B%E7%A8%8B%E6%95%B0)
  - [parallel](#parallel)
  - [将文件内容存入数组mapfile](#%E5%B0%86%E6%96%87%E4%BB%B6%E5%86%85%E5%AE%B9%E5%AD%98%E5%85%A5%E6%95%B0%E7%BB%84mapfile)
- [其他](#%E5%85%B6%E4%BB%96)
  - [演变历程](#%E6%BC%94%E5%8F%98%E5%8E%86%E7%A8%8B)
    - [Thompsonshell1971](#thompsonshell1971)
    - [Bourneshellsh,1977](#bourneshellsh1977)
  - [Bash内部命令](#bash%E5%86%85%E9%83%A8%E5%91%BD%E4%BB%A4)
    - [创建别名alias](#%E5%88%9B%E5%BB%BA%E5%88%AB%E5%90%8Dalias)
    - [命令行键盘映射bind](#%E5%91%BD%E4%BB%A4%E8%A1%8C%E9%94%AE%E7%9B%98%E6%98%A0%E5%B0%84bind)
    - [执行shell内部命令builtin](#%E6%89%A7%E8%A1%8Cshell%E5%86%85%E9%83%A8%E5%91%BD%E4%BB%A4builtin)
    - [返回子程序调用堆栈信息caller](#%E8%BF%94%E5%9B%9E%E5%AD%90%E7%A8%8B%E5%BA%8F%E8%B0%83%E7%94%A8%E5%A0%86%E6%A0%88%E4%BF%A1%E6%81%AFcaller)
    - [执行内部命令command](#%E6%89%A7%E8%A1%8C%E5%86%85%E9%83%A8%E5%91%BD%E4%BB%A4command)
    - [声明和显示变量declare](#%E5%A3%B0%E6%98%8E%E5%92%8C%E6%98%BE%E7%A4%BA%E5%8F%98%E9%87%8Fdeclare)
    - [输出/打印echo](#%E8%BE%93%E5%87%BA%E6%89%93%E5%8D%B0echo)
      - [避免'sudo echo x >' 时'Permission denied'](#%E9%81%BF%E5%85%8Dsudo-echo-x--%E6%97%B6permission-denied)
    - [临时关闭或者激活内部命令enable](#%E4%B8%B4%E6%97%B6%E5%85%B3%E9%97%AD%E6%88%96%E8%80%85%E6%BF%80%E6%B4%BB%E5%86%85%E9%83%A8%E5%91%BD%E4%BB%A4enable)
    - [显示shell内部命令的帮助信息help](#%E6%98%BE%E7%A4%BAshell%E5%86%85%E9%83%A8%E5%91%BD%E4%BB%A4%E7%9A%84%E5%B8%AE%E5%8A%A9%E4%BF%A1%E6%81%AFhelp)
    - [数学运算let](#%E6%95%B0%E5%AD%A6%E8%BF%90%E7%AE%97let)
    - [局部变量local](#%E5%B1%80%E9%83%A8%E5%8F%98%E9%87%8Flocal)
    - [退出登录logout](#%E9%80%80%E5%87%BA%E7%99%BB%E5%BD%95logout)
    - [从标准输入读取行并赋值到数组mapfile](#%E4%BB%8E%E6%A0%87%E5%87%86%E8%BE%93%E5%85%A5%E8%AF%BB%E5%8F%96%E8%A1%8C%E5%B9%B6%E8%B5%8B%E5%80%BC%E5%88%B0%E6%95%B0%E7%BB%84mapfile)
      - [选项](#%E9%80%89%E9%A1%B9)
    - [格式化输出printf](#%E6%A0%BC%E5%BC%8F%E5%8C%96%E8%BE%93%E5%87%BAprintf)
      - [参数](#%E5%8F%82%E6%95%B0)
      - [格式替代符](#%E6%A0%BC%E5%BC%8F%E6%9B%BF%E4%BB%A3%E7%AC%A6)
      - [转义序列](#%E8%BD%AC%E4%B9%89%E5%BA%8F%E5%88%97)
    - [从键盘读取变量的值read](#%E4%BB%8E%E9%94%AE%E7%9B%98%E8%AF%BB%E5%8F%96%E5%8F%98%E9%87%8F%E7%9A%84%E5%80%BCread)
    - [键盘输入到数组readarray](#%E9%94%AE%E7%9B%98%E8%BE%93%E5%85%A5%E5%88%B0%E6%95%B0%E7%BB%84readarray)
    - [重新加载文件source](#%E9%87%8D%E6%96%B0%E5%8A%A0%E8%BD%BD%E6%96%87%E4%BB%B6source)
    - [指定命令的类型type](#%E6%8C%87%E5%AE%9A%E5%91%BD%E4%BB%A4%E7%9A%84%E7%B1%BB%E5%9E%8Btype)
    - [typeset](#typeset)
    - [可用资源限制ulimit](#%E5%8F%AF%E7%94%A8%E8%B5%84%E6%BA%90%E9%99%90%E5%88%B6ulimit)
      - [实例](#%E5%AE%9E%E4%BE%8B)
    - [取消命令别名unalias](#%E5%8F%96%E6%B6%88%E5%91%BD%E4%BB%A4%E5%88%AB%E5%90%8Dunalias)
- [打包](#%E6%89%93%E5%8C%85)
  - [Debian](#debian)
    - [HelloWorld](#helloworld)
- [参考资料](#%E5%8F%82%E8%80%83%E8%B5%84%E6%96%99)

<!-- /TOC -->

# 说明

## 新建shell

```sh
echo '#!/bin/bash' > "${file_name}.sh＂
chmod +x "${file_name}.sh"

# 如果一定要让执行shell脚本的子shell读取.bashrc的话,可以给shell脚本第一行的解释器加上参数:

#!/bin/bash --login
```

## 注释

以 # 开头的行就是注释,会被解释器忽略.

## 多行注释

```sh
:<<EOF
注释内容...
注释内容...
注释内容...
EOF

# EOF 也可以使用其他符号

:<<'
注释内容...
注释内容...
注释内容...
'

:<<!
注释内容...
注释内容...
注释内容...
!

```

## 执行远程网络脚本

```sh
curl -s -L https://gitlab.com/ff4c00/software/-/raw/master/oracle/init_oracle_support.sh | bash
```

### 传递参数

执行网络脚本也可以传递参数或选项参数:

> If the -s option is present, or if no arguments remain after option processing, then commands are read from the standard input. This option allows the positional parameters to be set when invoking an interactive shell. -- man bash

两个破折号(-)告诉bash不要将其后的任何内容作为bash的参数进行处理:

```sh
curl -s -L https://gitlab.com/ff4c00/space/-/raw/master/space.sh | bash -s -- -m exclude_large_files
curl http://example.com/script.sh | bash -s -- arg1 arg2
```


## 登录未加载~/.bashrc文件

### 检查默认shell

如果默认shell非bash的话参考macos文件中bash相关内容设置.

```sh
echo $SHELL
```

### 检查~/.profile文件

Linux和Mac OS X终端程序使用的Shell配置文件.<br>
包含shell环境的定义,例如环境变量,要执行的脚本和其他指令,<br>
用于存储在Shell程序启动时加载的预定义设置.

当 ~/.bash_profile 或 ~/.bash_login文件存在时,不会读取该文件.

如果用户登录后没有source bashrc文件,<br>
并且不存在~/.profile文件,执行下面命令:

```sh
sudo tee -a ~/.profile <<-'EOF'
# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
        . "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
EOF
```

# 变量

## 定义变量

定义变量时,变量名不加美元符号($):

```sh
variable='variable'
```

除了显式地直接赋值,还可以用语句给变量赋值,如:

```sh
for file in `ls /etc`
# 或
for file in $(ls /etc)
```

### 命名规范

0. ***变量名和等号之间不能有空格***
0. 命名只能使用英文字母,数字和下划线,首个字符不能以数字开头
0. 中间不能有空格,可以使用下划线(_)
0. 不能使用标点符号
0. 关键字可以用help命令查看

## 使用变量

使用一个定义过的变量,只要在变量名前面加美元符号即可,如:

```sh
echo $variable
echo ${variable}
```

## 判断是否为空

```sh
para1=  
if [ ! $para1 ]; then  
  echo "IS NULL"  
else  
  echo "NOT NULL"  
fi 
```
## 拆分变量

更多拆分方式查看 字符串>正则匹配 中相关内容

```sh
cd /var/log/squid
echo ${PWD##*/}
squid
```

## 作用域 

类型|作用 
-|- 
局部变量|局部变量在脚本或命令中定义,仅在当前shell实例中有效,其他shell启动的程序不能访问局部变量. 
环境变量|所有的程序,包括shell启动的程序,都能访问环境变量,有些程序需要环境变量来保证其正常运行.必要的时候shell脚本也可以定义环境变量.
shell变量|shell变量是由shell程序设置的特殊变量.shell变量中有一部分是环境变量,有一部分是局部变量,这些变量保证了shell的正常运行.

## 默认值

```sh
# 设置默认遍历文件夹
default_path="$pangu_path/tilix"
foo=$1
# 如果foo没有被声明或为null或为空字符串时each_path=$default_path
each_path=${foo:-$default_path}
```

## 运行时参数

在执行 Shell 脚本时,向脚本传递参数,脚本内获取参数的格式为:$n.n 代表一个数字,1 为执行脚本的第一个参数,以此类推……

参数处理|说明
-|-
$#|传递到脚本的参数个数
$*|以一个单字符串显示所有向脚本传递的参数.<br>如"$*"用「"」括起来的情况、以"$1 $2 … $n"的形式输出所有参数.
\$$|脚本运行的当前进程ID号
$!|后台运行的最后一个进程的ID号
$@|与$*相同,但是使用时加引号,并在引号中返回每个参数.<br>如"$@"用「"」括起来的情况、以"$1" "$2" … "$n" 的形式输出所有参数.
$-|显示Shell使用的当前选项,与set命令功能相同.
$?|显示最后命令的退出状态.0表示没有错误,其他任何值表明有错误.

$* 与 $@ 区别:

特点|区别
-|-
相同点|都是引用所有参数.
不同点|只有在双引号中体现出来.假设在脚本运行时写了三个参数 1、2、3,,则 " * " 等价于 "1 2 3"(传递了一个参数),而 "@" 等价于 "1" "2" "3"(传递了三个参数).

### 选项化处理

有两个预先定义的变量,OPTARG表示选项值,OPTIND表示参数索引位置,类似于$1.

n后面有:,表示该选项需要参数,而h后面没有:,表示不需要参数.

最开始的一个冒号,表示出现错误时保持静默,并抑制正常的错误消息.

```sh
#!/usr/bin/env bash
# -n 名称
# -a 作者
# -h 帮助
while getopts ":n:a:h" optname
do
  case "$optname" in
    "n")
      echo "get option -n,value is $OPTARG"
      ;;
    "q")
      echo "get option -a ,value is $OPTARG"
      ;;
    "h")
      echo "get option -h,eg:./test.sh -n 编程珠玑 -a 守望先生"
      ;;
    ":")
      echo "No argument value for option $OPTARG"
      ;;
    "?")
      echo "Unknown option $OPTARG"
      ;;
    *)
      echo "Unknown error while processing options"
      ;;
  esac
  #echo "option index is $OPTIND"
done
```

# 数据类型

## 字符串

字符串可以用单引号,也可以用双引号,也可以不用引号.

### 单引号

单引号字符串的限制:

0. 单引号里的任何字符都会原样输出,单引号字符串中的变量是无效的
0. 单引号字串中不能出现单独一个的单引号(对单引号使用转义符后也不行),但可成对出现,作为字符串拼接使用.

### 双引号

双引号的优点:

0. 双引号里可以有变量.
0. 双引号里可以出现转义字符.

#### 字符串拼接

```sh
greeting="hello, "$your_name" !"
greeting_1="hello, ${your_name} !"
```


### 获取字符串长度

```sh

string="runoob is a great site"
# 获取字符串长度
echo ${#string} # => 22 

# 提取子字符串
# 以下实例从字符串第 2 个字符开始截取 4 个字符:
echo ${string:1:4} # => unoo
# 查找子字符串
# 查找字符 i 或 o 的位置(哪个字母先出现就计算哪个)
echo `expr index "$string" io`  # => 4
```

### 正则匹配

```sh
file='/dir1/dir2/dir3/my.file.txt'
```

操作|作用|返回值
-|-|-
${file#*/}|拿掉第一条 / 及其左边的字符串|# => dir1/dir2/dir3/my.file.txt
${file##*/}|拿掉最后一条 / 及其左边的字符串|# => my.file.txt
${file#*.}|拿掉第一个 . 及其左边的字符串|# => file.txt
${file##*.}|拿掉最后一个 . 及其左边的字符串|# => txt
${file\%/*}|拿掉最后条 / 及其右边的字符串|# => /dir1/dir2/dir3
${file%%/*}|拿掉第一条 / 及其右边的字符串|# => (空值)
${file\%.*}|拿掉最后一个 . 及其右边的字符串|# => /dir1/dir2/dir3/my.file
${file%%.*}|拿掉第一个 . 及其右边的字符串|# => /dir1/dir2/dir3/my


### 切割为数组

#### 示例-1

```sh
str="git￥https://gitlab.com/ff4c00/software.git￥software/oracle/instantclient_12_1.tar.gz"  
arr=(${str//￥/ })
get_method=${arr[0]}  
get_url=${arr[1]}
target_file_path=${arr[2]}
```

#### 示例-2

```sh
IFS=',' read -r -a array <<< "k002,lib,info,系统信息"
echo ${array[@]}
```

#### 回车符分隔

> 简而言之,IFS=$'\n'将换行符分配\n给变量IFS.<br>
$'string'构造是一种引用机制,用于像转义序列一样解码 ANSI C.<br>
这种语法来自ksh93,并且可以移植到现代 shell 中,例如bash, zsh, pdksh, busybox sh.<br>
此语法不是由 POSIX 定义的,但已被SUS issue 7接受.

```sh
IFS=$'\n' read -a items <<< $commit_files; unset IFS
```

### 字符串大小写转换表达式

```
var=toronto
echo "${var^}"
# => Toronto
echo "${var^[n-z]}"
# => Toronto
echo "${var^^[a-m]}"
# => toronto
echo "${var^^[n-q]}"
# => tOrONtO
echo "${var^^}"
# => TORONTO
```

### 获取字符串MD5值

MD5值始终是32个字符.

```sh
md5=$(echo -n 'TEST' | md5sum)
echo ${md5:0:32}
```

### 蛇形转驼峰格式

```shell
echo 'project_text' | sed "s/_/ /g" | sed 's/\b[a-z]/\U&/g' | sed "s/ //g"
```

### 截取字符串

```sh
${var#*/}
${var##*/}
${var%/*}
${var%%/*}
${var:start:len}
${var:start}
${var:0-start:len}
${var:0-start}
```

```sh
md5=$(cat $source_file_path | md5sum)
echo ${md5:0:32}
```

### grep

#### 条件*或*

```sh
# 查询包含model name或Model的内容
cat /proc/cpuinfo | grep "model name\|Model"
```

### 关于EOF

```sh
tee "$kun_main_path/app/kun/版本号.sh" <<EOF
#!/bin/bash --login
kun_vsersion="$version"
echo '当前版本: $version'
EOF

# 最终写入内容
# #!/bin/bash --login
# kun_vsersion="2021.8.6"
# echo '当前版本: 2021.8.6'
```

```sh
tee "$kun_main_path/app/kun/版本号.sh" <<-'EOF'
#!/bin/bash --login
kun_vsersion="$version"
echo '当前版本: $version'
EOF

# 最终写入内容
# #!/bin/bash --login
# vsersion=$version
# echo '当前版本: $version'
```

### JSON字符串处理(jq)

#### 默认值(//)

```sh
# 当值不存在时将返回默认值: []
jq -r '.["页面参数"]["详情"]["关联显示"]//[]' <<<$params
```
#### 格式化输出(.)

```sh
jq '.' <<<'{"foo":{"bar":{"baz":123}}}'

{
  "foo": {
    "bar": {
      "baz": 123
    }
  }
}
```

##### 单行输出

```sh
jq -c '.' <<<$song_info
```

#### 变量

##### 变量值转数字(tonumber)

```sh
song_info=$(jq --arg index $index '.[$index | tonumber]' <<<$song_infos)
```

#### 关键值(.关键字/."关键字")

```sh
jq '.foo' <<<'{"foo":42,"bar":"less interesting data"}'
# => 42

jq '."bar"' <<<'{"foo":42,"bar":"less interesting data"}'
# => "less interesting data"
```

#### 数组操作(.[], .[]?, .[2], .[10:15])

```sh
jq '.[1]' <<<'[{"name":"JSON", "good":true}, {"name":"XML", "good":false}]'

{
  "name": "XML",
  "good": false
}
```

#### 数组/对象构造([], {})

```sh
jq '{user, title: .titles[]}' <<<'{"user":"stedolan","titles":["JQ Primer", "More JQ"]}'

{
  "user": "stedolan",
  "title": "JQ Primer"
}
{
  "user": "stedolan",
  "title": "More JQ"
}
```

#### 值的长度(length)

```sh
jq '.[] | length' <<<'[[1,2], "string", {"a":2}, null]'

2
6
1
0
```

#### 数组中的键(keys)

```sh
jq 'keys' <<<'{"abc": 1, "abcd": 2, "Foo": 3}'

[
  "Foo",
  "abc",
  "abcd"
]
```

##### 令人费解的排序问题

jq的键排序规则:

0. keys枚举按字母顺序排序的键.
0. .[]根据键的输入顺序枚举值.

如果使用在一次传递中keys提取对象的键,然后在另一传递.[]中提取其值,则相应的输出元素可能不匹配.<br>


jqv1.5引入了该keys_unsorted/0功能,它启用了一个简单的解决方案:

```sh
# Sample input with unordered keys.
# Sorting the values results in the same order as sorting the keys,
# so the output order of values below implies the key enumeration order that was applied.
json='{ "c":3, "a":1, "b":2 }'
```

按输入顺序打印键,使用keys_unsorted/0:

```sh
echo "$json" | jq -r 'keys_unsorted[]'
c
a
b
```

按输入顺序打印值,它[]总是这样做:

```sh
echo "$json" | jq -r '.[]'
3
1
2
```

#### 输入多个过滤器(,)

```sh
jq '.foo, .bar' <<<'{"foo": 42, "bar": "something else", "baz": true}'

42
"something else"
```

#### 管道输出(|)

```sh
jq '.[] | .name' <<<'[{"name":"JSON", "good":true}, {"name":"XML", "good":false}]'

"JSON"
"XML"
```

#### 数组遍历(map)

```sh
jq 'map(.)' <<<'[1,5,3,0,7]'

[
  1,
  5,
  3,
  0,
  7
]

jq 'map(.+1)' <<<'[1,5,3,0,7]'

[
  2,
  6,
  4,
  1,
  8
]
```
#### 条件筛选(select(foo))

```sh
jq 'map(select(. >= 2))' <<<'[1,5,3,0,7]'

[
  5,
  3,
  7
]
```

##### 筛选结果转为数组

当有多个符合筛选条件的结果时,默认结果格式为:

```sh
jq -r --arg song_name "$item" '.[] | select(."歌曲名称" | test($song_name))' <<<$songs_info

{
  "歌曲标识": 1056154,
  "歌曲名称": "20090131",
  "歌手标识": [
    28414
  ],
  "歌手名称": [
    "Armand Tanzarian"
  ],
  "专辑标识": 110240,
  "专辑名称": "Words Of The President",
  "专辑封面": "https://p1.music.126.net/hKxJamR1TV_novAv2pzdsQ==/851021999941829.jpg"
}
{
  "歌曲标识": 476598254,
  "歌曲名称": "1-800-273-8255",
  "歌手标识": [
    1015122,
    1074089,
    301757
  ],
  "歌手名称": [
    "Logic",
    "Alessia Cara",
    "Khalid"
  ],
  "专辑标识": 35445457,
  "专辑名称": "Everybody",
  "专辑封面": "https://p2.music.126.net/srKt1tv8ebGGMZVhtEaOZQ==/18720284976418403.jpg"
}
```

使用 jq '[原条件]'可将结果包装为数组:

```sh
jq -r --arg song_name "$item" '[.[] | select(."歌曲名称" | test($song_name))]' <<<$songs_info

[
  {
    "歌曲标识": 1056154,
    "歌曲名称": "20090131",
    "歌手标识": [
      28414
    ],
    "歌手名称": [
      "Armand Tanzarian"
    ],
    "专辑标识": 110240,
    "专辑名称": "Words Of The President",
    "专辑封面": "https://p1.music.126.net/hKxJamR1TV_novAv2pzdsQ==/851021999941829.jpg"
  },
  {
    "歌曲标识": 476598254,
    "歌曲名称": "1-800-273-8255",
    "歌手标识": [
      1015122,
      1074089,
      301757
    ],
    "歌手名称": [
      "Logic",
      "Alessia Cara",
      "Khalid"
    ],
    "专辑标识": 35445457,
    "专辑名称": "Everybody",
    "专辑封面": "https://p2.music.126.net/srKt1tv8ebGGMZVhtEaOZQ==/18720284976418403.jpg"
  }
]
```

#### 条件句(if-then-else-end)

```sh
jq 'if . == 0 then "zero" elif . == 1 then "one" else "many" end' <<<'2'

"many"

jq '
  if . == 0 then "zero" 
  elif . == 1 then "one" 
  else "many" 
  end
' <<<'2'

"many"
```

#### 字符串插值

```sh
jq '"The input was \(.), which is one less than \(.+1)"' <<<'42'

"The input was 42, which is one less than 43"
```

#### 改

##### json变量(argjson)

argjson 参数用于保存json格式变量以供命令使用.

```sh
jq --arg standard_version $standard_version --argjson song_infos "$song_infos" '."\($standard_version)" += $song_infos' $store_songs
```

#### 输出结果与column配合打印输出

```sh
jq -r '.[] | [."歌曲名称", (."歌手名称" | join("/"))] | @tsv' <<<$song_infos | column -ts $'\t'
```

#### 截取数组元素个数

```sh
jq -r '.[10:15]' <<<$song_infos
```

### 逐行读取字符串内容

```sh
while read -r commit_item; do
  echo $commit_item
done < <(echo "$commit_files")
```

### Base64编码

```sh
# 默认每76个字符换行 -w 0 表示禁止换行
base64 -w 0 <<<'{ "名称": "测试用例", "描述": "针对described功能进行测试", "参数": { "仓库名称": { "类型": "string", "默认值": "", "变量名": "depository_name", "说明": "" }, "适配平台架构": { "类型": "string", "默认值": "linux/amd64,linux/arm64,linux/arm/v7", "变量名": "" }, "时区文件路径": { "类型": "string", "默认值": "/usr/share/zoneinfo/Asia/Shanghai", "变量名": "" }, "系统版本": { "类型": "decimal(2)", "默认值": "18.04", "说明": "", "变量名": "system_version" }, "镜像标签": { "类型": "string", "默认值": "ubuntu-18.04-20210919", "说明": "", "变量名": "image_tag" } } }'

# => eyAi5ZCN56ewIjogIua1i+ivleeUqOS+iyIsICLmj4/ov7AiOiAi6ZKI5a+5ZGVzY3JpYmVk5Yqf6IO96L+b6KGM5rWL6K+VIiwgIuWPguaVsCI6IHsgIuS7k+W6k+WQjeensCI6IHsgIuexu+WeiyI6ICJzdHJpbmciLCAi6buY6K6k5YC8IjogIiIsICLlj5jph4/lkI0iOiAiZGVwb3NpdG9yeV9uYW1lIiwgIuivtOaYjiI6ICIiIH0sICLpgILphY3lubPlj7DmnrbmnoQiOiB7ICLnsbvlnosiOiAic3RyaW5nIiwgIum7mOiupOWAvCI6ICJsaW51eC9hbWQ2NCxsaW51eC9hcm02NCxsaW51eC9hcm0vdjciLCAi5Y+Y6YeP5ZCNIjogIiIgfSwgIuaXtuWMuuaWh+S7tui3r+W+hCI6IHsgIuexu+WeiyI6ICJzdHJpbmciLCAi6buY6K6k5YC8IjogIi91c3Ivc2hhcmUvem9uZWluZm8vQXNpYS9TaGFuZ2hhaSIsICLlj5jph4/lkI0iOiAiIiB9LCAi57O757uf54mI5pysIjogeyAi57G75Z6LIjogImRlY2ltYWwoMikiLCAi6buY6K6k5YC8IjogIjE4LjA0IiwgIuivtOaYjiI6ICIiLCAi5Y+Y6YeP5ZCNIjogInN5c3RlbV92ZXJzaW9uIiB9LCAi6ZWc5YOP5qCH562+IjogeyAi57G75Z6LIjogInN0cmluZyIsICLpu5jorqTlgLwiOiAidWJ1bnR1LTE4LjA0LTIwMjEwOTE5IiwgIuivtOaYjiI6ICIiLCAi5Y+Y6YeP5ZCNIjogImltYWdlX3RhZyIgfSB9IH0K

base64 -d <<<'eyAi5ZCN56ewIjogIua1i+ivleeUqOS+iyIsICLmj4/ov7AiOiAi6ZKI5a+5ZGVzY3JpYmVk5Yqf6IO96L+b6KGM5rWL6K+VIiwgIuWPguaVsCI6IHsgIuS7k+W6k+WQjeensCI6IHsgIuexu+WeiyI6ICJzdHJpbmciLCAi6buY6K6k5YC8IjogIiIsICLlj5jph4/lkI0iOiAiZGVwb3NpdG9yeV9uYW1lIiwgIuivtOaYjiI6ICIiIH0sICLpgILphY3lubPlj7DmnrbmnoQiOiB7ICLnsbvlnosiOiAic3RyaW5nIiwgIum7mOiupOWAvCI6ICJsaW51eC9hbWQ2NCxsaW51eC9hcm02NCxsaW51eC9hcm0vdjciLCAi5Y+Y6YeP5ZCNIjogIiIgfSwgIuaXtuWMuuaWh+S7tui3r+W+hCI6IHsgIuexu+WeiyI6ICJzdHJpbmciLCAi6buY6K6k5YC8IjogIi91c3Ivc2hhcmUvem9uZWluZm8vQXNpYS9TaGFuZ2hhaSIsICLlj5jph4/lkI0iOiAiIiB9LCAi57O757uf54mI5pysIjogeyAi57G75Z6LIjogImRlY2ltYWwoMikiLCAi6buY6K6k5YC8IjogIjE4LjA0IiwgIuivtOaYjiI6ICIiLCAi5Y+Y6YeP5ZCNIjogInN5c3RlbV92ZXJzaW9uIiB9LCAi6ZWc5YOP5qCH562+IjogeyAi57G75Z6LIjogInN0cmluZyIsICLpu5jorqTlgLwiOiAidWJ1bnR1LTE4LjA0LTIwMjEwOTE5IiwgIuivtOaYjiI6ICIiLCAi5Y+Y6YeP5ZCNIjogImltYWdlX3RhZyIgfSB9IH0K'

# => { "名称": "测试用例", "描述": "针对described功能进行测试", "参数": { "仓库名称": { "类型": "string", "默认值": "", "变量名": "depository_name", "说明": "" }, "适配平台架构": { "类型": "string", "默认值": "linux/amd64,linux/arm64,linux/arm/v7", "变量名": "" }, "时区文件路径": { "类型": "string", "默认值": "/usr/share/zoneinfo/Asia/Shanghai", "变量名": "" }, "系统版本": { "类型": "decimal(2)", "默认值": "18.04", "说明": "", "变量名": "system_version" }, "镜像标签": { "类型": "string", "默认值": "ubuntu-18.04-20210919", "说明": "", "变量名": "image_tag" } } }
```

## 数字

### 浮点运算

#### 精确到指定小数位数

```sh
a=12
b=7

expr $a / $b # => 1

echo "scale=2;$a/$b" | bc # => 1.71
```

### 格式化

```sh
echo $(printf "%06d" 13)
# => 000013
```

### 随机数

> $RANDOM 是一个内部 Bash 函数(不是常数),它返回一个范围为 0 - 32767 的伪随机整数.<br>
它不应 用于生成加密密钥.

```sh
echo $((10 + $RANDOM % 50))
```

## 数组

### 定义数组

括号来表示数组,数组元素用"空格"符号分割开:

```sh
array_name=(值1 值2 ... 值n)

array_name=(
值1 
值2 
... 
值n
)

array_name[0]=value0
array_name[1]=value1
array_name[n]=value
```

### 读取数组


```sh
array_name[n]

# 使用 @ 符号可以获取数组中的所有元素
array_name[@]
```

### 获取数组长度

```sh
# 取得数组单个元素的长度
length=${#array_name[n]}

# 取得数组元素的个数
length=${#array_name[@]}
# 或者
length=${#array_name[*]}
```

### 数组遍历

```sh
# 遍历(带数组下标):
for i in "${!arr[@]}";
do
  printf "%s\t%s\n" "$i" "${arr[$i]}"  
done
```

```sh
for ((i=0;i<${#arr[*]};i++))
do
done
```

### 删除元素

```sh
array=(pluto pippo test)
delete=pluto
surplus=${array[@]/$delete}
echo $surplus

# 去除首个元素
surplus=${array[@]/${array[0]}}

# 去除前两个
tmp=${array[@]/${array[0]}}
echo ${tmp[@]/${array[1]}}
```

### 脚本参数转数组

```sh
# params=$@ => 这样是错误的
params=("$@")
```

### 拼接字符串

```sh
ids=(1 2 3 4)

echo "${ids[*]}" | awk '{gsub(/\s/, "/");print}'
```

### 指定范围

```sh
array=( $(seq 1 30 ) )
echo ${array[@]}
# => 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
echo ${array[@]:0:20}
# => 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 

echo ${array[@]:20:${#array[*]}}
# => 21 22 23 24 25 26 27 28 29 30
```

## 时间

> shell只有字符串,数字,数组三种数据类型,并不存在时间类型.

```sh
$(date +%Y%m%d)
# => 20200722

$(date +%Y-%m-%d)
# => 2020-07-22

date '+%Y-%m-%d %H:%M:%S'
# => 2020-08-05 15:05:58

date '+%Y-%m-%d %H:%M:%S.%N'
```

### 获取指定时间

#### 获取昨日

```sh
date -d '1 days ago' +%Y%m%d
```

#### 今天是当前周的第几天

```sh
date +%w
```

#### 获取本周指定时间

```sh
today=`date +%Y%m%d`
whichday=`date -d $today +%w`

# 当前周一:
monday=`date -d "$today -$[${whichday}-1] days" +%Y%m%d`

# 当前周一:
date -d "$(date +%Y%m%d) -$[$(date +%w)-1] days" +%Y%m%d
# 本周五
date -d "$monday + 5 days" +%Y%m%d

# 上周五:
date -d "$(date +%Y%m%d) -$[$(date +%w)+2] days" +%Y%m%d

date -d "$monday - 3 days" +%Y%m%d

```

#### 本月第一天

```sh
date +%Y%m01
```

#### 本月最后一天

```sh
date -d"$(date -d"1 month" +%Y%m01) -1 day" +%Y%m%d
```

## 文件

### 临时文件

> shell中调用while等循环语句时会启动子进程.<br>
在子进程中可以读取父进程的变量但是重新为父进程中的变量赋值后,<br>
子进程结束回到父进程后变量值不会得到改变.<br>
解决措施之一就是将子进程内容写入临时文件.

#### 临时文件的安全问题

> 直接创建临时文件,尤其在/tmp目录里面,往往会导致安全问题.<br>
首先,/tmp目录是所有人可读写的,任何用户都可以往该目录里面写文件.<br>
创建的临时文件也是所有人可读的.<br>
其次,如果攻击者知道临时文件的文件名,他可以创建符号链接,链接到临时文件,可能导致系统运行异常.<br>
攻击者也可能向脚本提供一些恶意数据.<br>
因此,临时文件最好使用不可预测、每次都不一样的文件名.<br>
最后,临时文件使用完毕,应该删除.<br>
但是,脚本意外退出时,往往会忽略清理临时文件.

#### 临时文件的最佳实践

脚本生成临时文件,应该遵循下面的规则:

0. 创建前检查文件是否已经存在.
0. 确保临时文件已成功创建.
0. 临时文件必须有权限的限制.
0. 临时文件要使用不可预测的文件名.
0. 脚本退出时,要删除临时文件(使用trap命令).

#### 安全创建临时文件(mktemp)

mktemp命令就是为安全创建临时文件而设计的.<br>
虽然在创建临时文件之前,它不会检查临时文件是否存在,但是它支持唯一文件名和清除机制,因此可以减轻安全攻击的风险.<br>
直接运行mktemp命令,就能生成一个临时文件.

```sh
# mktemp命令生成的临时文件名是随机的,而且权限是只有用户本人可读写.
mktemp
# => /tmp/tmp.4GcsWSG4vj

ls -l /tmp/tmp.4GcsWSG4vj
# => -rw------- 1 ruanyf ruanyf 0 12月 28 12:49 /tmp/tmp.4GcsWSG4vj
```

为了确保临时文件创建成功,mktemp命令后面最好使用 OR 运算符(||),指定创建失败时退出脚本.

```sh
tmpfile=$(mktemp) || exit 1
echo "Our temp file is $tmpfile"
```

为了保证脚本退出时临时文件被删除,可以使用trap命令指定退出时的清除操作.

```sh
trap 'rm -f "$tmpfile"' EXIT
```

##### 创建临时目录(-d)

-d参数可以创建一个临时目录.

##### 指定临时文件所在的目录(-p)

-p参数可以指定临时文件所在的目录.<br>
默认是使用$TMPDIR环境变量指定的目录,如果这个变量没设置,那么使用/tmp目录.

```sh
mktemp -p /home/ruanyf/
# => /home/ruanyf/tmp.FOKEtvs2H3
```

##### 指定临时文件的文件名模板(-t)

-t参数可以指定临时文件的文件名模板,模板的末尾必须至少包含三个连续的X字符,表示随机字符,建议至少使用六个X.<br>
默认的文件名模板是tmp.后接十个随机字符.

```sh
mktemp -t mytemp.XXXXXXX
# => /tmp/mytemp.yZ1HgZV
```

### 读取文件

#### 缺失最后一行

> 标准规定文本文件必须以换行符结尾,否则可能无法正确读取最后一个换行符之后的数据.

```sh
# 最后一行没有以换行符结束时 将缺失最后一行信息
while IFS= read -r line; do
  echo -n "$line"
done < file
```

当读取不完整的行时,read将失败,但$line仍会包含数据.<br>
额外的-n测试将检测到这一点,以便允许循环体输出不完整的行.<br>
在此后的迭代中,read将再次失败并$line为空字符串,从而终止循环.

```sh
# 最后一行没有以换行符结束时 仍能读取到
while IFS= read -r line || [ -n "$line" ]; do
  echo -n "$line"
done < file
```

### 获取文件行数(wc)

```sh
wc -l /dir/file.txt
# => 3272485 /dir/file.txt
wc -l < /dir/file.txt
# => 3272485
```

### 获取文件夹内最新文件名称

```sh
ls -Art | tail -n 1
# ls *.deb -Art | tail -n 1
```

### 路径存在空格情况

```sh
# 变量
file_path='/Users/ff4c00/Documents/Space/图书体系/F 经济/F8 财政、金融/F83 金融、银行/F830 金融、银行理论'
ls "$file_path"

# 手动输入
ls /Users/ff4c00/Documents/Space/图书体系/F\ 经济/F8\ 财政、金融/F83\ 金融、银行/F830\ 金融、银行理论
```

### awk

#### 镜像列表内容处理的实践

```sh
images="ff4c00/linux  ubuntu-18.04-20210907       f533dd4e1f5e  25 hours ago   533MB
ff4c00/linux  ubuntu-18.04-20210906       f1e1ddcbc622  45 hours ago   579MB
ff4c00/linux  ubuntu-18.04-20210902       7878648cd8bd  5 days ago     579MB
ff4c00/linux  ubuntu-18.04-20210901       f5a8d9e2d5cf  6 days ago     579MB
ff4c00/linux  ubuntu-18.04-20210831       0174d737a55e  7 days ago     579MB
ff4c00/linux  ubuntu-18.04-20210830       4af0252d2a20  8 days ago     579MB
ff4c00/linux  ubuntu-18.04-20210827       ce0d403de481  11 days ago    579MB
ff4c00/linux  ubuntu-18.04-20210826       e728f231bc31  12 days ago    578MB
ff4c00/linux  ubuntu-18.04-20210825       ad22300bbb04  13 days ago    194MB
ff4c00/linux  x86_64-u18.04-ali-20200616  6bbc75c5a568  14 months ago  300MB
ff4c00/linux  x86_64-u18.04-ali-20200603  54167db2306f  15 months ago  314MB
ff4c00/linux  x86_64-u18.04-ali-20190913  5d01afc0587b  24 months ago  309MB
ff4c00/linux  x86_64-u18.04-ali-20190712  ee19ebb64628  2 years ago    296MB
ff4c00/linux  x86_64-u18.04-ali-20190703  340030539ac0  2 years ago    687MB"
echo -e "$images"

# 正则匹配第二列标签名中以ubuntu-开头的镜像并输出第二列
echo -e "$images" | awk 'match($2,/^ubuntu-*/){print $2}'

# 同时输出第一和第二列
echo -e "$images" | awk 'match($2,/ubuntu-*/){print $1,$2}'

# 对输出内容进行格式化
echo -e "$images" | awk 'match($2,/ubuntu-*/){printf "%s:%s\n", $1,$2}'

# 对输出内容进行倒序排列
echo -e "$images" | awk 'match($2,/ubuntu-*/){printf "%s:%s\n", $1,$2}' | sort -r

# 只输出第一行内容
echo -e "$images" | awk 'match($2,/ubuntu-*/){printf "%s:%s\n", $1,$2}' | sort -r | head -n 1
```

#### 引用外部变量

```sh
file_name="{名称=>'浮夸', 标签=>['流行'], 类型=>'音频/歌曲', 其他=>{歌手=>['陈奕迅'], 标识=>'66282', 来源=>'网易云'}, 版本=>20211015}.flac"
echo $(printf '%q' "$file_name")
```

#### 文件名称的特殊字符转义

```sh
file_name="{名称=>'浮夸', 标签=>['流行'], 类型=>'音频/歌曲', 其他=>{歌手=>['陈奕迅'], 标识=>'66282', 来源=>'网易云'}, 版本=>20211015}.flac"
echo $(printf '%q' "$file_name")
```

# 运算符

## 基本运算符

种类|
-|
算数运算符|
关系运算符|
布尔运算符|
字符串运算符|
文件测试运算符|

### 算术运算符

原生bash不支持简单的数学运算,但是可以通过其他命令来实现,例如 awk 和 expr,expr 最常用.

expr 是一款表达式计算工具,使用它能完成表达式的求值操作.

例如,两个数相加(注意使用的是反引号 \` 而不是单引号 '):

```sh
val=`expr 2 + 2`
echo "两数之和为 : $val"
```

注意:

表达式和运算符之间要有空格,例如 2+2 是不对的,必须写成 2 + 2,这与大多数编程语言不一样.

完整的表达式要被 \` \` 包含,注意这个字符不是常用的单引号,在 Esc 键下边.

下表列出了常用的算术运算符,假定变量 a 为 10,变量 b 为 20:

运算符|说明|举例
-|-|-
+|加法|\`expr $a + $b\` 结果为 30
-|减法|\`expr $a - $b\` 结果为 -10
*|乘法|\`expr $a \* $b\` 结果为  200
/|除法|\`expr $b / $a\` 结果为 2
%|取余|\`expr $b % $a\` 结果为 0
=|赋值|a=$b 将把变量 b 的值赋给 a
==|相等|用于比较两个数字,相同则返回 true.<br>[ $a == $b ] 返回 false
!=|不相等|用于比较两个数字,不相同则返回 true.<br>[ $a != $b ] 返回 true

### 关系运算符

关系运算符只支持数字,不支持字符串,除非字符串的值是数字.

[ 注意空格 ]

假定变量 a 为 10,变量 b 为 20:

运算符|说明|举例
-|-|-
-eq|检测两个数是否相等,相等返回 true|[ $a -eq $b ] 返回 false
-ne|检测两个数是否不相等,不相等返回 true|[ $a -ne $b ] 返回 true
-gt|检测左边的数是否大于右边的,如果是,则返回 true|[ $a -gt $b ] 返回 false
-lt|检测左边的数是否小于右边的,如果是,则返回 true|[ $a -lt $b ] 返回 true
-ge|检测左边的数是否大于等于右边的,如果是,则返回 true|[ $a -ge $b ] 返回 false
-le|检测左边的数是否小于等于右边的,如果是,则返回 true|[ $a -le $b ] 返回 true

### 布尔运算符

假定变量 a 为 10,变量 b 为 20:

运算符|说明|举例
-|-|-
!|非运算|表达式为 true 则返回 false,否则返回 true<br>[ ! false ] 返回 true
-o|或运算|有一个表达式为 true 则返回 true<br>[ $a -lt 20 -o $b -gt 100 ] 返回 true
-a|与运算|两个表达式都为 true 才返回 true<br>[ $a -lt 20 -a $b -gt 100 ] 返回 false

### 逻辑运算符

```sh
# 避免使用没有if关键字形式的逻辑判断
# 如果在最后一行并且不符合条件 返回码会为1
([[ ! $code -eq 0 ]] && [[ ! $code -eq 8 ]]) && echo '[false, "返回值异常", '$code']' && exit $code;
# 建议使用
if [[ ! $code -eq 0 ]] && [[ ! $code -eq 8 ]];then output '跳出执行: 返回值异常'&&  echo '[false, "返回值异常", '$code']' && exit $code;fi
```

假定变量 a 为 10,变量 b 为 20:

运算符|说明|举例
-|-|-
&&|逻辑的 AND|[[ $a -lt 100 && $b -gt 100 ]] 返回 false
\|\||逻辑的 OR|[[ $a -lt 100 || $b -gt 100 ]]返回 true

### 字符串运算符

假定变量 a 为 "abc",变量 b 为 "efg":

运算符|说明|举例
-|-|-
=|检测两个字符串是否相等,相等返回 true|[ $a = $b ] 返回 false
!=|检测两个字符串是否相等,不相等返回 true|[ $a != $b ] 返回 true
-z|检测字符串长度是否为0,为0返回 true|[ -z $a ] 返回 false
-n|检测字符串长度是否为0,不为0返回 true|[ -n "$a" ] 返回 true
$|检测字符串是否为空,不为空返回 true|[ $a ] 返回 true

### 浮点运算

> bash 不支持浮点运算,如果需要进行浮点运算,需要借助bc,awk 处理.

### 文件测试运算符

文件测试运算符用于检测 Unix 文件的各种属性.


属性检测描述如下:

操作符|说明|举例
-|-|-
-b file|检测文件是否是块设备文件,如果是,则返回 true|[ -b $file ] 返回 false
-c file|检测文件是否是字符设备文件,如果是,则返回 true|[ -c $file ] 返回 false
-d file|检测文件是否是目录,如果是,则返回 true|[ -d $file ] 返回 false
-f file|检测文件是否是普通文件(既不是目录,也不是设备文件),如果是,则返回 true.|	[ -f $file ] 返回 true
-g file|检测文件是否设置了 SGID 位,如果是,则返回 true|[ -g $file ] 返回 false
-k file|检测文件是否设置了粘着位(Sticky Bit),如果是,则返回 true|[ -k $file ] 返回 false
-p file|检测文件是否是有名管道,如果是,则返回 true|[ -p $file ] 返回 false
-u file|检测文件是否设置了 SUID 位,如果是,则返回 true|[ -u $file ] 返回 false
-r file|检测文件是否可读,如果是,则返回 true|[ -r $file ] 返回 true
-w file|检测文件是否可写,如果是,则返回 true|[ -w $file ] 返回 true
-x file|检测文件是否可执行,如果是,则返回 true|[ -x $file ] 返回 true
-s file|检测文件是否为空(文件大小是否大于0),不为空返回 true.<br>	[ -s $file ] 返回 true
-e file|检测文件(包括目录)是否存在,如果是,则返回 true|[ -e $file ] 返回 true

#### 示例

```sh
# 若文件不存在则新建
config_file_path='/etc/shadowsocks/config.json'
[ -e $config_file_path ] || mkdir -p ${config_file_path%/*};touch $config_file_path
```

## 特殊符号

符号|说明|示例
-|-|-
&|任务在后台执行
&&|前一条命令执行成功时,才执行后一条命令|bin/elasticsearch -d && curl localhost:9200
;|不管前一句是否执行成功均会执行后一句
\|\||上一条命令执行失败后,才执行下一条命令
()|括号里面的内容作为一个整体执行<br>一条命令独占一个物理行或用;号隔开
\||表示管道,上一条命令的输出,作为下一条命令参数

### 输入输出重定向

大多数 UNIX 系统命令从终端接受输入并将所产生的输出发送回到终端.一个命令通常从一个叫标准输入的地方读取输入,默认情况下,这恰好是的终端.同样,一个命令通常将其输出写入到标准输出,默认情况下,这也是的终端.

重定向命令列表如下:


命令|说明
-|-
command > file|将输出重定向到 file
command < file|将输入重定向到 file
command >> file|将输出以追加的方式重定向到 file
n > file|将文件描述符为 n 的文件重定向到 file
n >> file|将文件描述符为 n 的文件以追加的方式重定向到 file
n >& m|将输出文件 m 和 n 合并
n <& m|将输入文件 m 和 n 合并
<< tag|将开始标记 tag 和结束标记 tag 之间的内容作为输入

需要注意的是文件描述符 0 通常是标准输入(STDIN),1 是标准输出(STDOUT),2 是标准错误输出(STDERR).

#### 将右侧的单词传递给左侧命令的标准输入

```sh
wc -l <<< $(echo -e "line1\nline2\nline3\n")
# => 3
```
#### 重定向深入讲解

一般情况下,每个 Unix/Linux 命令运行时都会打开三个文件:

0. 标准输入文件(stdin):stdin的文件描述符为0,Unix程序默认从stdin读取数据0. 标准输出文件(stdout):stdout 的文件描述符为1,Unix程序默认向stdout输出数据
0. 标准错误文件(stderr):stderr的文件描述符为2,Unix程序会向stderr流中写入错误信息
默认情况下,command > file 将 stdout 重定向到 file,command < file 将stdin 重定向到 file 

#### /dev/null 文件

如果希望执行某个命令,但又不希望在屏幕上显示输出结果,那么可以将输出重定向到 /dev/null:

```sh
command > /dev/null
```

/dev/null 是一个特殊的文件,写入到它的内容都会被丢弃;如果尝试从该文件读取内容,那么什么也读不到.但是 /dev/null 文件非常有用,将命令的输出重定向到它,会起到"禁止输出"的效果.

##### 脚本内容重定向

```sh
source "$target_path" 1>/dev/null 2>/dev/null
```

* 1 用于标准输出.将信息日志发送到 /dev/null
* 2 用于标准错误.将错误日志发送到 /dev/null

### 管道符

#### 管道命令符

#### 命令行的通配符

# 流程控制

## if

*sh的流程控制不可为空,如果else分支没有语句执行,就不要写这个else.*

语法格式:

```sh
if condition
then
    command1 
elif condition2 
then 
    command2 
else
    command3
fi
```

### 判断软件是否安装

```sh
if ! [ -x "$(command -v git)" ]; then
  echo 'Error: git is not installed.' >&2
  exit 1
fi
```

## for 循环

```sh
for value in item1 item2 ... itemN
do
    command1
    command2
    ...
    commandN
done
```

当变量值在列表里,for循环即执行一次所有命令,使用变量名获取列表中的当前取值.命令可为任何有效的shell命令和语句.in列表可以包含替换、字符串和文件名.

in列表是可选的,如果不用它,for循环使用命令行的位置参数.

## while 语句

```sh
while condition
do
    command
done
```

while循环可用于读取键盘信息.下面的例子中,输入信息被设置为变量FILM,按<Ctrl-D>结束循环.

```sh
echo '按下 <CTRL-D> 退出'
echo -n '输入最喜欢的网站名: '
while read FILM
do
    echo "是的！$FILM 是一个好网站"
done
```

### 无限循环

```sh
while :
do
  command
done

# 或者

while true
do
  command
done

# 或者

for (( ; ; ))
```

### 循环内使用read

一般条件下在while中使用read并不会等待用户输入,<br>
可指定从控制终端设备读取:

```sh
read -e choice </dev/tty
```

## until 循环

until 循环执行一系列命令直至条件为 true 时停止.

until 循环与 while 循环在处理方式上刚好相反.

一般 while 循环优于 until 循环,但在某些时候—也只是极少数情况下,until 循环更加有用.


## case

```sh
case 值 in
  模式1)
    command1
    command2
    ...
    commandN
  ;;
  模式2)
    command1
    command2
    ...
    commandN
  ;;
esac
```

case工作方式如上所示.取值后面必须为单词in,每一模式必须以右括号结束.取值可以为变量或常数.匹配发现取值符合某一模式后,其间所有命令开始执行直至 ;;

取值将检测匹配的每一个模式.一旦模式匹配,则执行完匹配模式相应命令后不再继续其他模式.如果无一匹配模式,使用星号 * 捕获该值,再执行后面的命令.

## 跳出循环

命令|作用
-|-
break|跳出所有循环(终止执行后面的所有循环)
continue|不会跳出所有循环,仅仅跳出当前循环

##  trap

### 使用 trap 捕获信号

trap命令用来在 Bash 脚本中响应系统信号.

最常见的系统信号就是 SIGINT(中断),即按 Ctrl + C 所产生的信号.<br>
trap命令的-l参数,可以列出所有的系统信号.

```sh
trap [动作] [信号]

# 捕获到 SIGINT 即 exit 0 正常退出
trap "exit 0" SIGINT;
```

如果trap需要触发多条命令,可以封装一个 Bash 函数.

```sh
function egress {
  command1
  command2
  command3
}

trap egress EXIT
```

#### 系统信号

> 不管是脚本正常执行结束,还是用户按 Ctrl + C 终止,都会产生EXIT信号.<br>
**trap命令必须放在脚本的开头.否则,它上方的任何命令导致脚本退出,都不会被它捕获.**

信号值|含义
-|-
HUP|编号1,脚本与所在的终端脱离联系.
INT|编号2,用户按下 Ctrl + C,意图让脚本中止运行.
QUIT|编号3,用户按下 Ctrl + 斜杠,意图退出脚本.
KILL|编号9,该信号用于杀死进程.
TERM|编号15,这是kill命令发出的默认信号.
EXIT|编号0,这不是系统信号,而是 Bash 脚本特有的信号,不管什么情况,只要退出脚本就会产生.



# 函数

## 文件重命名(mv)

```
mv 原始文件名 更新后文件路径
```

### 显示详细信息(-v)

### 仅在新路径不存在时移动(-n)

```sh
# ../网易云/
for filename in *; do
  file_path="$PWD/../网易云音乐/$filename"
  mv -vn "$filename" "$file_path"
done
```

## 自定义函数

函数调用必须在函数声明之后

```sh
[ function ] funname [()]

{
  action;
  [return int;]
}
```

0. 可以带function fun() 定义,也可以直接fun() 定义,不带任何参数.
0. 参数返回,可以显示加:return 返回,如果不加,将以最后一条命令运行结果,作为返回值. return后跟数值n(0-255)

### 参数

函数所需参数无需在方法名中定义:

```sh
function fun (){
  echo $1;
}
test='test'
fun $test;
# => test
```

## 查找指定文件(find)

在当前目录及子目录下查找abc.txt文件:

```sh
find ./ -name "abc.txt"
```

查找指定后缀文件:

```sh
find ./ -name "*.txt"
```

如果要查找多种文件类型,可以这样:

```sh
find ./ -regex ".*\.java\|.*\.xml"
```

或者:

```sh
find ./ -name "*.java" -o -name "*.xml"
```

### 指定文件夹层级

```sh
find /dev -maxdepth 1 -name 'abc-*'
```

### 排除某一目录

```sh
find /dev -name '.udev' -prune -o -name 'abc-*' -print
```

## 打印输出

### printf

#### 清空屏幕

```sh
printf \\e[2J\\e[H\\e[m
```

#### 格式替代符

printf 命令模仿 C 程序库(library)里的 printf() 程序<br>
printf 由 POSIX 标准所定义,因此使用 printf 的脚本比使用 echo 移植性好<br>
printf 使用引用文本或空格分隔的参数,外面可以在 printf 中使用格式化字符串,还可以制定字符串的宽度、左右对齐方式等.默认 printf 不会像 echo 自动添加换行符,可以手动添加 \n

语法:

```sh
printf 格式控制字符串 参数列表

printf "%-10s %-8s %-4s\n" 姓名 性别 体重kg  
printf "%-10s %-8s %-4.2f\n" 郭靖 男 66.1234 
printf "%-10s %-8s %-4.2f\n" 杨过 男 48.6543 
printf "%-10s %-8s %-4.2f\n" 郭芙 女 47.9876 
# => 
姓名     性别   体重kg
郭靖     男      66.12
杨过     男      48.65
郭芙     女      47.99

```

%s %c %d %f都是格式替代符<br>
%-10s 指一个宽度为10个字符(-表示左对齐,没有则表示右对齐),任何字符都会被显示在10个字符宽的字符内,如果不足则自动以空格填充,超过也会将内容全部显示出来.%-4.2f 指格式化为小数,其中.2指保留2位小数.

#### 转义序列

序列|说明
-|-
\a|警告字符,通常为ASCII的BEL字符
\b|后退
\c|抑制(不显示)输出结果中任何结尾的换行字符(只在%b格式指示符控制下的参数字符串中有效)<br>而且任何留在参数里的字符、任何接下来的参数以及任何留在格式字符串中的字符,都被忽略
\f|换页(formfeed)
\n|换行
\r|回车(Carriage return)
\t|水平制表符
\v|垂直制表符
\\|一个字面上的反斜杠字符
\ddd|表示1到3位数八进制值的字符.仅在格式字符串中有效
\0ddd|表示1到3位的八进制值字符

#### 字体颜色

```sh
echo -e "\033[30m 黑色字 \033[0m"
echo -e "\033[31m 红色字 \033[0m"
echo -e "\033[32m 绿色字 \033[0m"
echo -e "\033[33m 黄色字 \033[0m"
echo -e "\033[34m 蓝色字 \033[0m"
echo -e "\033[35m 紫色字 \033[0m"
echo -e "\033[36m 天蓝字 \033[0m"
echo -e "\033[37m 白色字 \033[0m"
echo -e "\033[40;37m 黑底白字 \033[0m"
echo -e "\033[41;37m 红底白字 \033[0m"
echo -e "\033[42;37m 绿底白字 \033[0m"
echo -e "\033[43;37m 黄底白字 \033[0m"
echo -e "\033[44;37m 蓝底白字 \033[0m"
echo -e "\033[45;37m 紫底白字 \033[0m"
echo -e "\033[46;37m 天蓝底白字 \033[0m"
echo -e "\033[47;30m 白底黑字 \033[0m"
```

##### 规则映射表

```sh
\[显示方式;前景色;背景色m
```

前景色|背景色|颜色
-|-|-
30|40|黑色
31|41|红色
32|42|绿色
33|43|黃色
34|44|蓝色
35|45|紫红色
36|46|青蓝色
37|47|白色

显示方式|意义
-|-
0|终端默认设置
1|高亮显示
4|使用下划线
5|闪烁
7|反白显示
8|不可见

```sh
\033[1;31;40m    # 1-高亮显示 31-前景色红色  40-背景色黑色
\033[0m          # 采用终端默认设置,即取消颜色设置
```

控制选项|含义
-|-
\33[0m|关闭所有属性
\33[1m|设置高亮度
\33[4m|下划线
\33[5m|闪烁
\33[7m|反显
\33[8m|消隐
\33[30m--\33[37m|设置前景色
\33[40m--\33[47m|设置背景色
\33[nA|光标上移n行
\33[nB|光标下移n行
\33[nC|光标右移n行
\33[nD|光标左移n行
\33[y;xH|设置光标位置
\33[2J|清屏
\33[K|清除从光标到行尾的内容
\33[s|保存光标位置
\33[u|恢复光标位置
\33[?25l|隐藏光标
\33[?25h|显示光标

```sh
printf "\033[0;31m\33[7m反显\33[0m\033[0m"

# 这样不行
# echo -e "\033[0;31m\33[7m反显\33[0m\033[0m"
```

## 加载其他文件(source)

> 和其他语言一样,Shell 也可以包含外部脚本.<br>这样可以很方便的封装一些公用的代码作为一个独立的文件.

语法格式如下:

```sh
. filename # 注意点号(.)和文件名中间有一空格

# 或
source filename
```

### 以超管身份加载脚本

```sh
sudo /bin/bash -c "source '$kun_main_path/lib/other/backup_file' $conf_path"
```

### 判断文件内容是否已加载

```sh
[[ ! "$(type -t print_table)" == 'function' ]] && source "./print_table.sh"
```

### 判断脚本是否为source执行(BASH_SOURCE)

```sh
if [ $(json '["0"]' <<<"$res") == 'false' ]; then
  echo $(json '["1"]' <<<"$res")
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
    return 255
  else
    exit 255
  fi
fi
```

## 调用其他脚本(fork, exec, source)

方法名|作用|示例
-|-|-
fork|shell中包含执行命令,子命令并不影响父级的命令,<br> 运行的时候开一个sub-shell执行调用的脚本,sub-shell执行的时候,parent-shell还在.<br>在子命令执行完后再执行父级命令.<br>子级的环境变量不会影响到父级.sub-shell执行完毕后返回parent-shell.<br> sub-shell从parent-shell继承环境变量.<br>但是sub-shell中的环境变量不会带回parent-shell|fork /directory/script.sh
exec|exec与fork不同,不需要新开一个sub-shell来执行被调用的脚本.<br>被调用的脚本与父脚本在同一个shell内执行.<br>但是使用exec调用一个新脚本以后, 父脚本中exec行之后的内容就不会再执行了.执行子级的命令后,不再执行父级命令.|exec /directory/script.sh
source|执行子级命令后继续执行父级命令,<br>同时子级设置的环境变量会影响到父级的环境变量.<br>与fork的区别是不新开一个sub-shell来执行被调用的脚本,而是在同一个shell中执行.<br>所以被调用的脚本中声明的变量和环境变量, <br>都可以在主脚本中得到和使用.|source /directory/script.sh

## 交互式

### 读取命令行输入(read)

倒计时五秒内输入名字:

```sh
if read -t 5 -p "please enter your name:" name 
then 
  echo "hello $name ,welcome to my script"
else
  echo "sorry,too slow"
fi
```

## 参数项分隔(xargs)

> xargs是一条Unix和类Unix操作系统的常用命令.<br>
它的作用是将参数列表转换成小块分段传递给其他命令以避免参数列表过长的问题.<br>
xargs的作用一般等同于大多数Unix shell中的反引号但更加灵活易用并可以正确处理输入中有空格等特殊字符的情况.<br>
对于经常产生大量输出的命令如find、locate和grep来说非常有用.

测试文件:

```sh
printf "行1\n行2\n行3\n行4\n行5\n行6" > testFile.txt
```

### 多行输出

```sh
cat testFile.txt | xargs -n3

# 行1 行2 行3
# 行4 行5 行6
```

### 

```sh
cat testFile.txt | xargs -I {} echo "{}," | xargs -n3

# 行1, 行2, 行3,
# 行4, 行5, 行6,
```

### 多进程并行执行(--max-procs 进程数)

xargs默认只用一个进程执行命令.<br>
如果命令要执行多次必须等上一次执行完才能执行下一次.

--max-procs 参数 指定同时用多少个进程并行执行命令.<br>
--max-procs 2 表示同时最多使用两个进程--max-procs 0表示不限制进程数.

## parallel

Bash中没有内置并行运行命令的方法.<br>
GNU Parallel是用于执行此操作的工具.

顾名思义GNU Parallel可用于并行构建和运行命令.<br>
可以使用不同的参数运行同一命令无论它们是文件名用户名主机名还是从文件读取的行.<br>
GNU Parallel提供了对许多最常见操作(输入行输入行的各个部分指定输入源的不同方式等)的简写引用.<br>
并行可以xargs从其输入源替换命令或将命令提供给Bash的多个不同实例.

GNU Parallel相较于xargs的优势,其中一点在于解决了xargs对于特殊字符的过度处理问题(比如无法处理JSON格式内容).


```sh
# 四线程读取文件行内容并创建ES索引
cat /home/ff4c00/Space/知识体系/应用科学/计算机科学/理论计算机科学/编程语言理论/Ruby/Gems/Nokogiri/Scout/outputs/standard_format_data.txt | parallel -j 4 '
  md5=$(echo -n {} | md5sum)
  RequestES POST "unite_structure/_doc/${md5:0:32}" {}'
```

## 将文件内容存入数组(mapfile)

```sh
echo {a..z} | tr " " "\n" >alpha.log
```

读取该文件并将每一行存储到数组myarr中(如果不指定,则存储到默认的MAPFILE数组中).

```sh
mapfile myarr < alpha.log

echo ${myarr[@]}
```

```sh
mapfile -t -c 3 -C "echo ${myarr[@]}" myarr <alpha.log

mapfile -n 3 myarr <alpha.log
mapfile -t -c 3 -C "echo" myarr < alpha.log
cat alpha.log | mapfile -t -c 3 -C "echo"
```

# 其他

## 演变历程

### Thompsonshell(1971)

Shell(或命令行解释器)具有很长历史了,但讨论从第一个UNIX®shell开始.<br>
(贝尔实验室)KenThompson于1971年开发了第一个用于UNIXshell,名为V6shell.<br>
类似于它在Multics中前身,这个shell(/bin/sh)是一个在内核外部执行独立用户程序.<br>
globbing(参数扩展模式匹配,比如 *.txt)等概念是在一个名为glob独立实用程序中实现,<br>
就像用于评估条件表达式if命令一样.<br>
这种独立性可保持shell很小,只需不到900行C源代码.

该shell为重定向(< > 和 >>)和管道(或 ^)引入了一种紧凑语法,这种语法已延续到现代shell中.<br>
也可以找到对调用顺序命令(使用 ;)和异步命令(使用 &)支持.

Thompsonshell缺少是编写脚本能力.<br>
它唯一用途就是用作一个交互式shell(命令解释器)来调用命令和查看结果.

### Bourneshell(sh,1977)

Bourneshell由StephenBourne在AT&T BellLabs为V7UNIX创建,<br>
它在如今仍然是一个有用shell(在一些情况下还被用作默认根shell).<br>
作者在研究ALGOL68编译器之后开发了Bourneshell,<br>
所以会发现它语法比其他shell更加类似于AlgorithmicLanguage (ALGOL).<br>
尽管使用C开发,源代码本身甚至使用了宏赋予它一种ALGOL68特色.

Bourneshell有两个主要目标:

0. 用作一个命令解释器来交互式执行操作系统命令
0. 用于编写脚本(编写可通过shell调用可重用脚本). 

除了取代Thompsonshell,Bourneshell还提供了相对于其前身多项优势.<br>
Bourne向脚本中引入了控制流、循环和变量,提供了一种更加强大语言来与操作系统交互(包括交互式和非交互式).<br>
该shell还允许使用shell脚本作为过滤器,提供对处理信号集成支持,但缺乏定义函数能力.<br>
最后,它整合了如今使用许多功能,包括命令替换(使用反引号)和用于将保留字符串文字嵌入到脚本中HERE文档.

Bourneshell不仅是向前发展重要一步,也是众多衍生shell基础,其中许多shell如今应用在典型Linux系统中.

![](https://www.ibm.com/developerworks/cn/linux/l-linux-shells/figure1.gif)


## Bash内部命令

### 创建别名(alias)

```sh
alias [-p] [name[=value] …]
```

### 命令行键盘映射(bind)

bind命令用于显示和设置命令行键盘序列绑定功能.<br>
通过这一命令,可以提高命令行中操作效率.<br>
可以利用bind命令了解有哪些按键组合与其功能,也可以自行指定要用哪些按键组合.

参数|含义
-|-
-d|显示按键配置内容.
-f<按键配置文件>|载入指定按键配置文件.
-l|列出所有功能.
-m<按键配置>|指定按键配置.
-q<功能>|显示指定功能按键.
-v|列出目前按键配置与其功能.

```sh
bind -x '"\C-l":ls -l'    #直接按CTRL+L就列出目录
```

### 执行shell内部命令(builtin)

> 用于执行指定的shell内部命令,并返回内部命令的返回值.<br>
builtin命令在使用时,将不能够再使用Linux中的外部命令.<br>
当系统中定义了与shell内部命令相同的函数时,使用builtin显式地执行shell内部命令,<br>
从而 **忽略定义的shell函数** .

### 返回子程序调用堆栈信息(caller)

> caller命令返回当前活动的子程序调用的上下文,即调用堆栈信息,<br>
包括shell函数和内建命令source执行的脚本.<br>
没有指定expr时,显示当前子程序调用的行号和源文件名.<br>
如果expr是一个非负整数,显示当前子程序调用的行号、子程序名和源文件名.<br>
caller命令打印出来的堆栈信息在调试程序时是很有帮助的.<br>
如果shell没有子程序调用或者expr是一个无效的位置时,call命令返回false.


### 执行内部命令(command)

> command命令类似于builtin,也是为了避免调用同名的shell函数,<br>
命令包括shell内建命令和环境变量PATH中的命令.

### 声明和显示变量(declare)

> 用于声明和显示已存在的shell变量.当不提供变量名参数时显示所有shell变量.<br>
declare命令若不带任何参数选项,则会显示所有shell变量及其值.<br>
declare的功能与typeset命令的功能是相同的.

```sh
declare(选项)(参数)
```

参数|含义
-|-
+/-| *-* 可用来指定变量的属性,*+* 则是取消变量所设的属性.
-f|仅显示函数.
r|将变量设置为只读.
x|指定的变量会成为环境变量,可供shell以外的程序来使用.
i|[设置值]可以是数值,字符串或运算式.

### 输出/打印(echo)

> 用于在shell中打印shell变量的值,或者直接输出指定的字符串.<br>
linux的echo命令,在shell编程中极为常用,<br>
在终端下打印变量value的时候也是常常用到的,一般起到一个提示的作用.

```sh
echo(选项)(参数)
```

使用-e选项时,若字符串中出现以下字符,则特别加以处理,而不会将它当成一般文字输出:

字符|含义
-|-
\a|发出警告声.
\b|删除前一个字符.
\c|最后不加上换行符号.
\f|换行但光标仍旧停留在原来的位置.
\n|换行且光标移至行首.
\r|光标移至行首,但不换行.
\t|插入tab.
\v|与\f相同.
\\|插入\字符.
\nnn|插入nnn(八进制)所代表的ASCII字符.

#### 避免'sudo echo x >' 时'Permission denied'

利用管道和 tee 命令,该命令可以从标准输入中读入信息并将其写入标准输出或文件中,具体用法如下:

```sh
echo a |sudo tee 1.txt
echo a |sudo tee -a 1.txt # -a 是追加的意思,等同于 >>
```
### 临时关闭或者激活内部命令(enable)

> 用于临时关闭或者激活指定的shell内部命令.<br>
若要执行的文件名称与shell内建命令相同,可用enable -n来关闭shell内建命令.<br>
若不加-n选项,enable可重新启动关闭的命令.

```sh
enable(选项)(参数)
```

选项|说明
-|-
-n|关闭指定的内部命令.
-a|显示所有激活的内部命令.
-f|从指定文件中读取内部命令.

### 显示shell内部命令的帮助信息(help)

用于显示shell内部命令的帮助信息.<br>
help命令只能显示shell内部的命令帮助信息.<br>
而对于外部命令的帮助信息只能使用man或者info命令查看.

```sh
help(选项)(参数)
```

选项:

-d: 显示每个模式的简短描述

-m: 以类似手册页的格式 显示每个模式的描述

-s: 仅显示每个模式的简短使用提要

### 数学运算(let)

let命令是bash中用于计算的工具,提供常用运算符还提供了方幂**运算符.<br>
在变量的房屋计算中不需要加上$来表示变量,如果表达式的值是非0,那么返回的状态值是0；否则,返回的状态值是1.

```sh
let a=5+4 b=9-3
echo $a $b
```

### 局部变量(local)

```sh
local [option] name[=value]
```

对于每个参数,将创建一个名为name的局部变量,并为其分配value.

### 退出登录(logout)

```sh
logout [n]
```

退出登录shell程序,将状态n返回给外壳程序的父级.

### 从标准输入读取行并赋值到数组(mapfile)

```sh
mapfile [-d delim] [-n count] [-O origin] [-s count] [-t] [-u fd] [-C callback] [-c quantum] [array]
```

#### 选项

```
-d delim       将delim设为行分隔符,代替默认的换行符
-n count       从标准输入中获取最多count行,如果count为零那么获取全部
-O origin      从数组下标为origin的位置开始赋值,默认的下标为0
-s count       跳过对前count行的读取
-t             读取时移除行分隔符delim(默认为换行符)
-u fd          从文件描述符fd中读取
-C callback    每当读取了quantum行时,调用callback语句
-c quantum     设定读取的行数为quantum

如果使用-C时没有同时使用-c指定quantum的值,那么quantum默认为5000.

当callback语句执行时,将数组下一个要赋值的下标以及读取的行作为额外的参数传递给callback语句.

如果使用-O时没有提供起始位置,那么mapfile会在实际赋值之前清空该数组.
```

### 格式化输出(printf)

printf命令格式化并输出结果到标准输出.

```sh
printf [-v var] format [arguments]
```

#### 参数

输出格式:指定数据输出时的格式

输出字符串:指定要输出的数据

#### 格式替代符

参数|作用
-|-
%b|相对应的参数被视为含有要被处理的转义序列之字符串.
%c|ASCII字符.显示相对应参数的第一个字符
%d, %i|十进制整数
%e, %E, %f|浮点格式
%g|%e或%f转换,看哪一个较短,则删除结尾的零
%G|%E或%f转换,看哪一个较短,则删除结尾的零
%o|不带正负号的八进制值
%s|字符串
%u|不带正负号的十进制值
%x|不带正负号的十六进制值,使用a至f表示10至15
%X|不带正负号的十六进制值,使用A至F表示10至15
%%|字面意义的%

#### 转义序列

参数|作用
-|-
\a|警告字符,通常为ASCII的BEL字符
\b|后退
\c|抑制(不显示)输出结果中任何结尾的换行字符(只在%b格式指示符控制下的参数字符串中有效),而且,任何留在参数里的字符、任何接下来的参数以及任何留在格式字符串中的字符,都被忽略
\f|换页(formfeed)
\n|换行
\r|回车(Carriage return)
\t|水平制表符
\v|垂直制表符
'\\' |一个字面上的反斜杠字符
\ddd|表示1到3位数八进制值的字符,仅在格式字符串中有效
\0ddd|表示1到3位的八进制值字符

### 从键盘读取变量的值(read)

read命令从键盘读取变量的值,通常用在shell脚本中与用户进行交互的场合.<br>
该命令可以一次读取多个变量的值,变量和输入的值都需要使用空格隔开.<br>
在read命令后面,如果没有指定变量名,读取的数据将被自动赋值给特定的变量REPLY.

```sh
read(选项)(参数)
```

选项:

选项|作用
-|-
-a aname|输入被分配给数组变量aname的顺序索引 ,从0开始.<br>所有元素都在赋值前从aname中删除.<br>其他名称参数将被忽略.
-d delim|delim的第一个字符用于终止输入行,而不是换行符.<br>如果delim是空字符串,read则在读取NUL字符时将终止一行.
-e|Readline用于获取该行.<br>Readline使用当前(如果以前未进行行编辑,则为默认)编辑设置,但使用Readline的默认文件名补全.
-i text|如果使用Readline读取行,则在开始编辑之前将文本放入编辑缓冲区.
-n nchars|read在读取nchars个字符之后返回,而不是等待完整的输入行,但是如果在分隔符之前读取的字符少于nchars个,则使用分隔符.<br>
-N nchars|read精确读取nchars个字符后返回,而不是等待完整的输入行,除非遇到EOF或 read超时.<br>输入中遇到的定界符不会被特殊对待,并且read直到读取nchars个字符后才导致返回 .<br>结果不按IFS;中的字符分割；目的是为变量分配准确的读取字符(反斜杠除外；请参见-r 下面的选项).
-p prompt|在尝试读取任何输入之前,显示提示,不带换行符.<br>仅当输入来自终端时才显示提示.
-r|如果指定了此选项,则反斜杠不会充当转义字符.<br>反斜杠被认为是该行的一部分.<br>特别是,反斜杠-换行符对然后不能用作行继续.
-s|静音模式.<br>如果输入来自终端,则不会回显字符.
-t timeout|read如果在超时秒内未读完整行输入(或指定数量的字符),则会导致超时并返回失败.<br> 超时 可能是一个十进制数字,小数点后面是小数部分.<br>仅当read从端子,管道或其他特殊文件读取输入时,此选项才有效.<br>从常规文件读取时无效.<br>如果read超时,read则将读取的任何部分输入保存到指定的变量名中.<br>如果超时为0,read立即返回,而无需尝试读取和数据.<br>如果输入在指定的文件描述符上可用,则退出状态为0,否则为非零.<br>如果超时,则退出状态大于128.
-u fd|从文件描述符fd读取输入.

### 键盘输入到数组(readarray)

从标准输入读取行到索引数组变量array中,或者从文件描述符fd中读取行.

```sh
readarray [-d delim] [-n count] [-O origin] [-s count] [-t] [-u fd] [-C callback] [-c quantum] [array]
```

### 重新加载文件(source)

```sh
source filename
```

### 指定命令的类型(type)

type命令用来显示指定命令的类型,判断给出的指令是内部指令还是外部指令.

```sh
type [-afptP] [指令名称]
```

命令类型:

* alias:别名.
* keyword:关键字,Shell保留字.
* function:函数,Shell函数.
* builtin:内建命令,Shell内建命令.
* file:文件,磁盘文件,外部命令.
* unfound:没有找到.

选项:

-t:输出*file*、*alias*或者*builtin*,分别表示给定的指令为*外部指令*、*命令别名*或者*内部指令*

-p:如果给出的指令为外部指令,则显示其绝对路径

-a:在环境变量*PATH*指定的路径中,显示给定指令的信息,包括命令别名

### (typeset)

提供该typeset命令是为了与Korn shell兼容.<br>
它是declare内置命令的同义词.

### 可用资源限制(ulimit)

ulimit命令用来限制系统用户对shell资源的访问.<br>
如果不懂什么意思,下面一段内容可以帮助理解:

假设有这样一种情况,当一台 Linux 主机上同时登陆了 10 个人,在系统资源无限制的情况下,这 10 个用户同时打开了 500 个文档,而假设每个文档的大小有 10M,这时系统的内存资源就会受到巨大的挑战.

而实际应用的环境要比这种假设复杂的多,例如在一个嵌入式开发环境中,各方面的资源都是非常紧缺的,对于开启文件描述符的数量,分配堆栈的大 小,CPU 时间,虚拟内存大小,等等,都有非常严格的要求.<br>
资源的合理限制和分配,不仅仅是保证系统可用性的必要条件,也与系统上软件运行的性能有着密不可分的联 系.<br>
这时,ulimit 可以起到很大的作用,它是一种简单并且有效的实现资源限制的方式.

ulimit 用于限制 shell 启动进程所占用的资源,支持以下各种类型的限制:所创建的内核文件的大小、进程数据块的大小、Shell 进程创建文件的大小、内存锁住的大小、常驻内存集的大小、打开文件描述符的数量、分配堆栈的最大大小、CPU 时间、单个用户的最大线程数、Shell 进程所能使用的最大虚拟内存.<br>
同时,它支持硬资源和软资源的限制.

作为临时限制,ulimit 可以作用于通过使用其命令登录的 shell 会话,在会话终止时便结束限制,并不影响于其他 shell 会话.<br>
而对于长期的固定限制,ulimit 命令语句又可以被添加到由登录 shell 读取的文件中,作用于特定的 shell 用户.

```sh
ulimit [-HSabcdefiklmnpqrstuvxPT]
```

参数|作用
-|-
-S|更改并报告与资源关联的软限制.
-H|更改并报告与资源关联的硬限制.
-a|报告所有当前极限.
-b|最大套接字缓冲区大小.
-c|创建的核心文件的最大大小.
-d|流程数据段的最大大小.
-e|最大调度优先级(*nice*).
-f|Shell及其子级写入的文件的最大大小.
-i|未决信号的最大数量.
-k|可以分配的最大队列数.
-l|可以锁定到内存的最大大小.
-m|最大居民集大小(许多系统不遵守此限制).
-n|打开文件描述符的最大数量(大多数系统不允许设置此值).
-p|管道缓冲区大小.
-q|POSIX消息队列中的最大字节数.
-r|最大实时调度优先级.
-s|最大堆栈大小.
-t|最大CPU时间(以秒为单位).
-u|单个用户可用的最大进程数.
-v|可用于外壳以及在某些系统上可用于其子组件的最大虚拟内存量.
-x|文件锁的最大数量.
-P|伪终端的最大数量.
-T|最大线程数.

#### 实例

```sh
[root@localhost ~]# ulimit -a
core file size          (blocks, -c) 0           #core文件的最大值为100 blocks
data seg size           (kbytes, -d) unlimited   #进程的数据段可以任意大
scheduling priority             (-e) 0
file size               (blocks, -f) unlimited   #文件可以任意大
pending signals                 (-i) 98304       #最多有98304个待处理的信号
max locked memory       (kbytes, -l) 32          #一个任务锁住的物理内存的最大值为32KB
max memory size         (kbytes, -m) unlimited   #一个任务的常驻物理内存的最大值
open files                      (-n) 1024        #一个任务最多可以同时打开1024的文件
pipe size            (512 bytes, -p) 8           #管道的最大空间为4096字节
POSIX message queues     (bytes, -q) 819200      #POSIX的消息队列的最大值为819200字节
real-time priority              (-r) 0
stack size              (kbytes, -s) 10240       #进程的栈的最大值为10240字节
cpu time               (seconds, -t) unlimited   #进程使用的CPU时间
max user processes              (-u) 98304       #当前用户同时打开的进程(包括线程)的最大个数为98304
virtual memory          (kbytes, -v) unlimited   #没有限制进程的最大地址空间
file locks                      (-x) unlimited   #所能锁住的文件的最大个数没有限制
```

### 取消命令别名(unalias)

unalias命令用来取消命令别名,是为shell内建命令.<br>
如果需要取消任意一个命令别名,则使用该命令别名作为指令的参数选项即可.<br>
如果使用-a选项,则表示取消所有已经存在的命令别名.

```sh
unalias(选项)(参数)
```

# 打包

## Debian

### HelloWorld

```sh
sudo apt-get install dh-make devscripts
touch hello
chmod u+x hello
mkdir hello-0.1
cp hello hello-0.1/

cd hello-0.1/
dh_make --indep --createorig
grep -v makefile debian/rules > debian/rules.new
mv debian/rules.new debian/rules
echo hello usr/bin > debian/install
echo "1.0" > debian/source/format
rm debian/*.ex
debuild -us -uc
cd ..
sudo dpkg -i hello_0.1-1_all.deb
hello

# 卸载
sudo dpkg --remove hello
```

# 参考资料

> [runoob | Shell教程](http://www.runoob.com/linux/linux-shell.html)

> [简书 | linux资料总章](https://www.jianshu.com/p/8157b5e5c6b0)

> [CSDN | linux shell 字符串操作(长度,查找,替换,匹配)详解](https://www.cnblogs.com/itcomputer/p/4884436.html)

> [CSDN | Shell脚本中调用另外一个脚本的方法](https://blog.csdn.net/sayyy/article/details/80519307)

> [CSDN | Linux Shell—— read命令](https://blog.csdn.net/chen_zhipeng/article/details/8435049)

> [CSDN | shell 分隔字符串成数组](https://blog.csdn.net/zhang_Red/article/details/8443944)

> [CSDN | 如何在linux下shell编写脚本中模拟键盘输入](https://blog.csdn.net/quicmous/article/details/77602810)

> [编程珠玑 | 这样处理shell脚本参数,爽多了！](https://mp.weixin.qq.com/s/g6RivHyI3huX7PM_HezzZg)

> [stackoverflow | Execute bash script from URL](https://stackoverflow.com/questions/5735666/execute-bash-script-from-url)

> [stackoverflow | Passing parameters to bash when executing a script fetched by curl](https://stackoverflow.com/questions/4642915/passing-parameters-to-bash-when-executing-a-script-fetched-by-curl)

> [CSDN | shell脚本获取昨天今天本周周一本周周日本月第一天本月最后一天](https://blog.csdn.net/feinifi/article/details/83826456)

> [CSDN | Linux shel浮点l除法,精确到指定小数位数](https://blog.csdn.net/naiveloafer/article/details/8783518)

> [zshell | bash 结束死循环的方法](https://zshell.cc/2017/11/05/linux-shell--bash结束死循环的方法/)

> [IBM | 评估 Linux 中的 shell](www.ibm.com/developerworks/cn/linux/l-linux-shells/)

> [Github | pure bash bible](https://github.com/dylanaraps/pure-bash-bible)

> [GNU | Bash Builtin Commands](https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html)

> [Linux命令大全 | bind命令](https://man.linuxde.net/bind)

> [Linux命令大全 | builtin命令](https://man.linuxde.net/builtin)

> [CSDN |【Bash百宝箱】shell内建命令之builtin、command、caller](https://blog.csdn.net/iEearth/article/details/52625636)

> [Linux命令大全 | declare命令](https://man.linuxde.net/declare)

> [Linux命令大全 | echo命令](https://man.linuxde.net/echo)

> [Linux命令大全 | enable命令](https://man.linuxde.net/enable)

> [Linux命令大全 | help命令](https://man.linuxde.net/help)

> [Linux命令大全 | let命令](https://man.linuxde.net/let)

> [wangchujiang | mapfile](https://wangchujiang.com/linux-command/c/mapfile.html)

> [Linux命令大全 | printf命令](https://man.linuxde.net/printf)

> [Linux命令大全 | read命令](https://man.linuxde.net/read)

> [Linux命令大全 | type命令](https://man.linuxde.net/type)

> [Linux命令大全 | ulimit命令](https://man.linuxde.net/ulimit)

> [askubuntu | How to get the MD5 hash of a string directly in the terminal?](https://askubuntu.com/questions/53846/how-to-get-the-md5-hash-of-a-string-directly-in-the-terminal)

> [stackoverflow | Maximum length for MD5 input/output](https://stackoverflow.com/questions/3394503/maximum-length-for-md5-input-output)

> [菜鸟教程 | Linux xargs 命令](https://www.runoob.com/linux/linux-comm-xargs.html)

> [阮一峰的网络日志 | xargs 命令教程](https://www.ruanyifeng.com/blog/2019/08/xargs-tutorial.html)

> [stackoverflow | Determine if a function exists in bash](https://stackoverflow.com/questions/85880/determine-if-a-function-exists-in-bash)

> [博客园 | Linux Shell 截取字符串](https://www.cnblogs.com/fengbohello/p/5954895.html)

> [CSDN | 避免’sudo echo x >’ 时’Permission denied’](https://blog.csdn.net/hejinjing_tom_com/article/details/7767127)

> [stackoverflow | Check if a Bash array contains a value](https://stackoverflow.com/questions/3685970/check-if-a-bash-array-contains-a-value)

> [askubuntu | How to package a simple bash script [duplicate]](https://askubuntu.com/questions/279686/how-to-package-a-simple-bash-script)

> [thegeekstuff | Linux Grep OR, Grep AND, Grep NOT Operator Examples](https://www.thegeekstuff.com/2011/10/grep-or-and-not-operators/)

> [onetipperday | arguments array for bash script](http://onetipperday.sterding.com/2012/08/arguments-array-for-bash-script.html)

> [阮一峰的网络日志 | Bash 脚本如何创建临时文件:mktemp 命令和 trap 命令教程](https://www.ruanyifeng.com/blog/2019/12/mktemp.html)

> [askubuntu | Is there a way to specify a certain range of numbers using array in a script? [closed]](https://askubuntu.com/questions/705126/is-there-a-way-to-specify-a-certain-range-of-numbers-using-array-in-a-script)

> [stackoverflow | How can I get the variable value inside the EOF tags?](https://stackoverflow.com/questions/17983586/how-can-i-get-the-variable-value-inside-the-eof-tags)

> [stackoverflow | Shell script read missing last line](https://stackoverflow.com/questions/12916352/shell-script-read-missing-last-line)

> [stackexchange | Why does this 'while' loop not recognize the last line?](https://unix.stackexchange.com/questions/482517/why-does-this-while-loop-not-recognize-the-last-line)

> [stackexchange | What does <<< mean? [duplicate]](https://unix.stackexchange.com/questions/80362/what-does-mean)

> [CSDN | linux中特效地(（显示颜)、粗体、下划线等效果）将文本输出到终端](https://blog.csdn.net/wangjianno2/article/details/48788041)

> [stackoverflow | How to detect if a script is being sourced](https://stackoverflow.com/questions/2683279/how-to-detect-if-a-script-is-being-sourced)

> [stackexchange | redirecting to /dev/null](https://unix.stackexchange.com/questions/119648/redirecting-to-dev-null)

> [stackoverflow | How to count lines in a document?](https://stackoverflow.com/questions/3137094/how-to-count-lines-in-a-document)

> [stackoverflow | Get most recent file in a directory on Linux](https://stackoverflow.com/questions/1015678/get-most-recent-file-in-a-directory-on-linux/23034261)

> [stackexchange | Why I can't escape spaces on a bash script? [duplicate]](https://unix.stackexchange.com/questions/108635/why-i-cant-escape-spaces-on-a-bash-script)

> [jqplay | home](https://jqplay.org/)

> [stackoverflow | jq sorts KEY and VALUES in different way - how can I enumerate them in the same order?](https://stackoverflow.com/questions/23120359/jq-sorts-key-and-values-in-different-way-how-can-i-enumerate-them-in-the-same)

> [stackoverflow | Escape filenames the same way Bash does it](https://stackoverflow.com/questions/5608112/escape-filenames-the-same-way-bash-does-it)

> [stackoverflow | get the first (or n'th) element in a jq json parsing](https://stackoverflow.com/questions/38500363/get-the-first-or-nth-element-in-a-jq-json-parsing)

> [github | How can I put integer as an argument in jq? #1197](https://github.com/stedolan/jq/issues/1197)

> [stackoverflow | Read user input inside a loop](https://stackoverflow.com/questions/6883363/read-user-input-inside-a-loop)

> [stackoverflow | Get outputs from jq on a single line](https://stackoverflow.com/questions/40396445/get-outputs-from-jq-on-a-single-line)

> [stackoverflow | How to format a JSON string as a table using jq?](https://stackoverflow.com/questions/39139107/how-to-format-a-json-string-as-a-table-using-jq)

> [github | jq Manual (development version)](https://stedolan.github.io/jq/manual/)

> [stackoverflow | How to only find files in a given directory, and ignore subdirectories using bash](https://stackoverflow.com/questions/7715485/how-to-only-find-files-in-a-given-directory-and-ignore-subdirectories-using-bas)

> [stackexchange | How can I read line by line from a variable in bash?](https://unix.stackexchange.com/questions/9784/how-can-i-read-line-by-line-from-a-variable-in-bash)

> [stackoverflow | Get or default function in JQ?](https://stackoverflow.com/questions/56555155/get-or-default-function-in-jq/56555296)

> [stackexchange | mv: Move file only if destination does not exist](https://unix.stackexchange.com/questions/248544/mv-move-file-only-if-destination-does-not-exist)