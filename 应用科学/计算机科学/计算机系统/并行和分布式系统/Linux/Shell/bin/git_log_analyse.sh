#!/bin/bash --login

# 通过git log 分析项目情况
# 分析项目开始至今以及本周代码行数变化情况 

# 数组: 绝对路径$分支名
# 开发分支应该放在数组的最后面,这样最后会checkout回常用分支
poject_paths=('华中科技::/home/ff4c00/space/code/yggc/huazhongkeji_oracle::master' '华中竞价::/home/ff4c00/space/code/yggc/huazhongkeji_oracle/vendor/online_bidding_with_audit::master' '武大直采::/home/ff4c00/space/code/yggc/wuhan_daxue::master::develop' '武大备案::/home/ff4c00/space/code/yggc/wuhan_daxue/vendor/wdba::master::develop' '山西综改::/home/ff4c00/space/code/yggc/taiyuanzonggai::master' '华南理工::/home/ff4c00/space/code/yggc/huananligong::master::test' '深圳科技::/home/ff4c00/space/code/yggc/shenzhen::sztu')

# 本周一时间
monday=$(date -d "$(date +%Y%m%d) -$[$(date +%w)-1] days" +%Y-%m-%d)
# 本周五时间
friday=$(date -d "$monday + 5 days" +%Y-%m-%d)

echo "$monday - $friday 时间段内,项目代码变动情况概览"

echo "项目 代码总计 分支 本周新增(行) 本周新增(率)(%) 代码生产率 新增缺陷 新增文档 备注"

for i in "${!poject_paths[@]}";
do
  arr=(${poject_paths[$i]//::/ })
  for ((j=2;j<${#arr[*]};j++))
  do
    cd ${arr[1]};
    git checkout -q ${arr[j]}
    
    histor_datas=$(git log --since="1995-03-22" --before="$friday" --pretty=tformat: --numstat | awk '{ add += $1; subs += $2; loc += $1 + $2 } END { printf "%s::%s::%s", add, subs, loc }')
    histor_datas=(${histor_datas//::/ })
    # 增加行数
    histor_add=${histor_datas[0]}
    # 删除行数
    histor_remove=${histor_datas[1]}
    # 总行数
    histor_total=${histor_datas[2]}

    part_datas=$(git log --since="$monday" --before="$friday" --pretty=tformat: --numstat | awk '{ add += $1; subs += $2; loc += $1 + $2 } END { printf "%s::%s::%s", add, subs, loc }')
    part_datas=(${part_datas//::/ })

    if [ ${#part_datas[*]} -eq 3 ]; then
      # 增加行数
      part_add=${part_datas[0]}
      # 删除行数
      part_remove=${part_datas[1]}
      # 总行数
      part_total=${part_datas[2]}
      # 新增率
      part_rate="0$(echo "scale=8;$part_total/$histor_total" | bc)"
      part_rate_100="0$(echo "scale=8;$part_total/$histor_total*100" | bc)%"
    else
      part_total=0 
      part_rate=0
      part_rate_100='0%'
    fi

    echo "${arr[0]} $histor_total ${arr[j]} $part_total $part_rate $part_rate_100  "

  done
done