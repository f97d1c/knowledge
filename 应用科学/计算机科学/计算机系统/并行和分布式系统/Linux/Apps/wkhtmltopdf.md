> wkhtmltopdf是一个通过webkit浏览器把网页生成pdf的开源组件

# 安装

## 下载

[官网](https://wkhtmltopdf.org/downloads.html)选择对应版本,复制下载链接.

```bash
wget https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.bionic_amd64.deb
```

### 相关依赖

```bash
sudo apt-get install xfonts-encodings xfonts-utils xfonts-base xfonts-75dpi
```

## 安装


```bash
dpkg -i jxvf wkhtmltox_0.12.5-1.bionic_amd64.deb
```

# 参考资料

> [官网](https://wkhtmltopdf.org/)

> [CSDN | ubuntu安装wkhtmltopdf](https://blog.csdn.net/shooray/article/details/50446421)