#!/bin/bash

# 次节点信息
function collect_secondary_node () {
  read -p "请输入次节点host地址:" host
  read -p "请输入次节点连接时所使用用户名称:" user_name
  read -p "请输入次节点ssh端口号:" port
  
  # ssh -p $port $user_name@$host
  
  # 生成rsa公私钥
  # TODO: 应先查找现有文件
  # ssh-keygen -t rsa

  # 密钥传输
  cat .ssh/id_rsa.pub | ssh -p port $user_name@$host 'mkdir ~/.ssh; cat >> .ssh/authorized_keys'
}

# 当前主机是否为主节点
read -p "当前主机是否为主节点?(y/n)" choice
case ${choice} in
  y|Y|yes|YES)
    collect_secondary_node
  ;;
  *)
    echo '请使用主节点进行此操作'
    exit 1
  ;;
esac

exit 0
