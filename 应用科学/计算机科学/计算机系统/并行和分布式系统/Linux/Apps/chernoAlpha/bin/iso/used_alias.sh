#!/bin/bash

echo '为用户写入常用别名'
tee -a /home/${OPERATOR_USER}/.bashrc <<-'EOF'
alias start_sshd="sudo /etc/init.d/ssh start"
alias git_cache='git config --global credential.helper '\''cache --timeout 36000000000'\'''
alias create_alias_for_this_folder="bash + x $SCRIPT_PATH/iso/create_alias_for_this_folder.sh"
alias rac='clear;rails c'
alias ras='clear;rails s -p$1 -b 0.0.0.0'
alias kill_ruby_server='bash + x $SCRIPT_PATH/ruby/kill_server.sh'
EOF