#!/bin/bash


function install_by_rvm () {
  echo '安装RVM'
  curl -sSL https://rvm.io/mpapis.asc | gpg --import -
  curl -sSL https://rvm.io/pkuczynski.asc | gpg --import -
  curl -sSL https://get.rvm.io | bash -s stable
  source ~/.rvm/scripts/rvm

  echo '设置RVM最大超时时间'
  echo "export rvm_max_time_flag=20" >> ~/.rvmrc
  source ~/.rvmrc


  echo '安装RVM相关依赖'
  rvm requirements

  echo '修改更新源'
  echo "ruby_url=https://cache.ruby-china.org/pub/ruby" > ~/.rvm/user/db

  echo '安装Ruby(${RUBY_VERSION})'
  rvm install ${RUBY_VERSION}

  echo '解决: You need to change your terminal emulator preferences to allow login shell.'
  echo '[[ -s "/home/${OPERATOR_USER}/.rvm/scripts/rvm" ]] && . "/home/${OPERATOR_USER}/.rvm/scripts/rvm"' >> /home/${OPERATOR_USER}/.bashrc
  source /home/${OPERATOR_USER}/.bashrc

  echo '指定Ruby默认版本(${RUBY_VERSION})'
  rvm use ${RUBY_VERSION} --default
}

function install_by_rbenv () {
  RUBY_VERSION=2.3.8
  if [ $(printf "%s\n" "$RUBY_VERSION" "2.4" | sort -V -r | head -1) = '2.4' ];then 
    echo '安装2.4版本前特殊依赖内容'
    # 2.3.x及以下选装旧版本OpenSSL
    apt-get install -y libssl1.0-dev
    
  fi

  echo '下载rbenv'
  git clone https://github.com/rbenv/rbenv.git ~/.rbenv

  echo '下载ruby-build'
  git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build

  echo '使用RubyChina镜像地址'
  git clone git://github.com/AndorChen/rbenv-china-mirror.git ~/.rbenv/plugins/rbenv-china-mirror

echo 'DNS地址写入'
sudo tee -a /etc/resolv.conf <<-'EOF'
nameserver 8.8.8.8
nameserver 8.8.4.4
EOF

echo '相关环境变量写入'
sudo tee -a ~/.bashrc <<-'EOF'
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
EOF

  export PATH="$HOME/.rbenv/bin:$PATH"
  eval "$(rbenv init -)"

  echo '安装Ruby(${RUBY_VERSION})'
  rbenv install ${RUBY_VERSION}
  rbenv versions
  if [[ ! $? -eq 0 ]];then echo "rvenv: ruby安装失败"; exit 1;fi
}

echo 'ruby基本环境依赖'
sudo apt install -y curl g++ gcc autoconf automake bison libc6-dev libffi-dev libgdbm-dev libncurses5-dev libsqlite3-dev libtool libyaml-dev make pkg-config sqlite3 zlib1g-dev libgmp-dev libreadline-dev libssl-dev nodejs 

# 安装oracle依赖
curl -s -L https://gitlab.com/ff4c00/software/-/raw/master/oracle/init_oracle_support.sh | bash

echo '项目依赖安装'
sudo apt install -y imagemagick ghostscript libmagickwand-dev

echo '安装Mysql数据库相关依赖'
# 这个必须要安装,不然镜像会大300多MB 
# TODO 具体什么原因待查找
sudo apt install -y mysql-client libmysqlclient-dev 

echo '通过rvm安装ruby'
install_by_rvm
if [[ ! $? -eq 0 ]];then 
  echo 'rvm安装失败, 尝试通过rbenv安装'
  install_by_rbenv
  if [[ ! $? -eq 0 ]];then echo "rbenv安装异常"; exit 1;fi
fi



echo '设置Gem更新源'
gem sources --add https://gems.ruby-china.com/ --remove https://rubygems.org/

echo '安装Bundler'
# 注意版本 已知1.16.2版本会引发: Traceback (most recent call last)
# gem uninstall bundler
# 可安装指定版本: gem install bundler -v 1.16.6

gem install bundler