#!/bin/bash

if [ ! $1 ];then  echo '未定义安装jdk版本';exit 1;fi

function install_open_jdk () {
  echo '开始安装open jdk'
  sudo apt-get update -y
  sudo apt-get upgrade -y
  sudo apt-get install openjdk-8-jdk -y
  echo "jdk相关信息:";java -version
}

case $1 in 
  'open_jdk')
    install_open_jdk
  ;;
  *)
    echo '未找到相关版本jdk安装方法！'
    exit 1
  ;;
esac
