#!/bin/bash

echo '设置.vimrc文件' 
touch /home/${OPERATOR_USER}/.vimrc

tee /home/${OPERATOR_USER}/.vimrc <<-'EOF'
set nocp
set ru
syntax on
set number
set autoindent
filetype on
set showmatch
set matchtime=5

"禁用折叠功能
set nofoldenable

"设置缩进
set cindent
set tabstop=2
set shiftwidth=2
"自动缩进
set smartindent

"每次退出插入模式时自动保存
"待解决: 普通模式下操作自动保存以及普通模式下禁用中文
au InsertLeave *.* write       
"au NormalLeave *.* write  

"快捷键切换tab
"设置 <leader>0-9 来快速切换tab(默认leader是反斜杠,即先按下\键,再按数字键)
"参考链接: https://zhuanlan.zhihu.com/p/20902166
noremap <silent><tab>n :tabnew<cr>
noremap <silent><tab>c :tabclose<cr>
noremap <silent><tab>N :tabn<cr>
noremap <silent><tab>p :tabp<cr>
noremap <silent><leader>t :tabnew<cr>
noremap <silent><leader>g :tabclose<cr>
noremap <silent><leader>1 :tabn 1<cr>
noremap <silent><leader>2 :tabn 2<cr>
noremap <silent><leader>3 :tabn 3<cr>
noremap <silent><leader>4 :tabn 4<cr>
noremap <silent><leader>5 :tabn 5<cr>
noremap <silent><leader>6 :tabn 6<cr>
noremap <silent><leader>7 :tabn 7<cr>
noremap <silent><leader>8 :tabn 8<cr>
noremap <silent><leader>9 :tabn 9<cr>
noremap <silent><leader>0 :tabn 10<cr>
noremap <silent><s-tab> :tabnext<CR>
inoremap <silent><s-tab> <ESC>:tabnext<CR>
"快捷键切换tab end 

"根据文件类型加载插件
filetype plugin on

"实时搜索
set incsearch

"F11进入粘贴模式
set pastetoggle=<F11>

"启用自动补全
filetype plugin indent on

"自动对齐
set autoindent

"鼠标可用
set mouse=a

"编码格式
set encoding=utf-8

"突出显示当前行
set cursorline


EOF
