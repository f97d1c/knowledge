
echo '安装pip'
sudo apt install -y python2.7 python-pip

echo '安装Shadowsocks'
pip install https://github.com/shadowsocks/shadowsocks/archive/master.zip

echo "当前版本: $(sudo ssserver --version)"

echo '创建配置文件'
[ -e $CONFIG_FILE_PATH ] || mkdir -p ${CONFIG_FILE_PATH%/*};touch $CONFIG_FILE_PATH

sudo tee $CONFIG_FILE_PATH <<-'EOF'
{
  "server":"::",
  "server_port":8388,
  "local_address": "127.0.0.1",
  "local_port":1080,
  "password":"BegiN_2UTiw81j2jsuw2k_END",
  "timeout":300,
  "method":"aes-256-cfb",
  "fast_open": false
}
EOF

# echo 'ssserver -c $CONFIG_FILE_PATH'

