#!/bin/bash

source $SCRIPT_PATH/iso/edit_sources/sources_list/ubuntu_18.04.sh
# export SYS_VERSION=$(lsb_release -r | awk '{print $2}')
export SYS_VERSION=$(cat /etc/issue | awk '{print $2}' | cut -d \. -f 1,2)

# 判断系统具体版本
function judge_model_x86_64 () {
  case $SYS_VERSION in 
  '18.04')
    write_sources_18.04_x86_64
  ;;
  *)
    echo "没有找到和系统具体版本($SYS_VERSION)相匹配的更新源！"
    exit 1
  ;;
  esac
}

function judge_model_armv7l () {
  case $SYS_VERSION in 
  '18.04')
    write_sources_18.04_armv7l
  ;;
  *)
    echo "没有找到和系统具体版本($SYS_VERSION)相匹配的更新源！"
    exit 1
  ;;
  esac
}

# 判断硬件架构
function choose_ubuntu_sources () {
  hardware_platform=$(uname -i)
  case $hardware_platform in
  'x86_64')
    judge_model_x86_64
  ;;
  'armv7l')
    judge_model_armv7l
  ;;
  *)
    echo "没有找到和硬件架构($hardware_platform)相匹配的更新源！"
    exit 1
  ;;
  esac
}