#!/bin/bash

# 用于宿主机系统初始化使用

if [ ! $USER == 'root' ]; then
  echo '请切换为root用户完成下面操作'
  exit 1
fi

if [ ! $SCRIPT_PATH ]; then
  read -p "请输入脚本所在路径:" path
  if [ ! $path ]
  then
    echo '参数不能为空'
    exit 1
  else
    echo "export SCRIPT_PATH=$path" >> /root/.bashrc
  fi
fi 

if [ ! $OPERATOR_USER ]; then
  read -p "请输入系统普通用户:" user
  if [ ! $user ]
  then
    echo '参数不能为空'
    exit 1
  else
    echo "export OPERATOR_USER=$user" >> /root/.bashrc
  fi
fi 

if [ ! $NEED_DOCKER ]; then
  read -p "是否需要安装docker?(y/n)" choice
  case ${choice} in
    y|Y|yes|YES)
      echo "export NEED_DOCKER=true" >> /root/.bashrc
    ;;
    *)
      echo "export NEED_DOCKER=false" >> /root/.bashrc
    ;;
  esac
fi

source /root/.bashrc

bash -x $SCRIPT_PATH/iso/edit_sources/edit_sources.sh 
bash -x $SCRIPT_PATH/iso/debian/update_system.sh 
echo '系统必要软件安装' 
apt-get install -fy vim sudo git openssh-server tmux htop net-tools apt-utils curl 
bash -x $SCRIPT_PATH/iso/tool/setting_tmux.sh 
bash -x $SCRIPT_PATH/iso/tool/setting_vim.sh 


su - ${OPERATOR_USER} <<EOF
bash -x $SCRIPT_PATH/iso/used_alias.sh
if [ $NEED_DOCKER == true ]; then
  echo '安装和设置docker'
  sudo apt-get install -fy docker.io docker-compose
  sudo groupadd docker
  sudo usermod -aG docker $OPERATOR_USER
fi
EOF




