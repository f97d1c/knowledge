#!/bin/bash

source $SCRIPT_PATH/iso/edit_sources/sources_ubuntu.sh

# 获取系统名称
# ubuntu镜像没有安装lsb-choose_ubuntu_sources
# 如果必须使用需要:
# 0. 修改默认更新源(18.04为例),添加: deb http://security.ubuntu.com/ubuntu/ bionic-security main 
# 1. 更新系统: apt update
# 2. apt-get install -y lsb-core
# sys_name=$(lsb_release -i | awk '{print $3}')
sys_name=$(cat /etc/issue | awk '{print $1}')

case $sys_name in 
'Ubuntu')
  choose_ubuntu_sources
;;
*)
  echo '未找到相关系统更新源文件！'
  exit 1
;;
esac

