#!/bin/bash --login

echo "$(date +'%Y-%m-%d %H:%M:%S') 加载环境变量"
source /home/${OPERATOR_USER}/.bashrc
source /home/${OPERATOR_USER}/.bash_profile

if [ ! "$1" ];then  echo '参数1-容器类型 不能为空';exit 1;fi
if [ ! "$2" ];then  echo '参数2-目标路径 不能为空';exit 1;fi

container_type=$1 
project_path=$2
project_name=${project_path##*/}

function install_support () {
  case $project_name in 
  'huazhongkeji_oracle')
    echo "SCRIPT_PATH:$SCRIPT_PATH"
    bash -x $SCRIPT_PATH/oracle/init_oracle_support.sh
    source /home/${OPERATOR_USER}/.bashrc
    source /home/${OPERATOR_USER}/.bash_profile
  ;;
  esac
}

function project_ruby_start () {

  install_support 
  if [[ ! $? -eq 0 ]];then echo "相关软件依赖安装异常"; exit 1;fi

  project_path="/home/${OPERATOR_USER}/space/code/$1"
  cd $project_path
  if [[ ! $? -eq 0 ]];then echo "未找到相关文件夹:$project_path"; exit 1;fi

  bundle doctor
  if [[ ! $? -eq 0 ]];then
    bundle 
    if [[ ! $? -eq 0 ]];then echo "项目bundle异常"; exit 1;fi
  fi

  rails s
  if [[ ! $? -eq 0 ]];then echo "项目启动异常"; exit 1;fi
}

case $container_type in 
'web-ruby'|'web')
  project_ruby_start $project_path
;;
*)
  echo '参数1-容器类型 未找到所对应加载方法'
  exit 1
;;
esac