res = `docker ps`
print "执行#{ (`echo $?`.to_i == 0) ? '成功' : '失败' } \n\n"
by_line = res.split(/\n/)
datas = by_line.map{|string| string.split('  ').delete_if{|s| s == ""}}

container_ips = `docker inspect --format='{{.NetworkSettings.Networks.chernoalpha_default.Aliases}}{{.NetworkSettings.Networks.chernoalpha_default.IPAddress}}' $(docker ps -a -q)`

container_ips = container_ips.split(/\n/).delete_if{|string| string.include?('no value')}

hashs = [
  {name: '序号', command: 'i'},
  {name: '容器ID', command: 'array[0].strip'},
  # {name: '状态', command: 'array[4].strip'},
  {name: '端口', command: "'http://'+array[5].strip"},
  {name: '容器名称', command: 'array[6].strip'},
  # {name: 'IP地址', command: "container_ips.select{|string| string.include?(array[0])}[0] =~ /([^\]]+)$/;$1"},
  # {name: 'IP地址', command: "container_ips.select{|string| string.include?(array[0])}[0].gsub(/\[.*\]/, '')"},
  {name: 'IP地址', command: "container_ips.select{|string| string.include?(array[0])}[0][-10..-1]"},
]

print "#{hashs.map{|hash|hash[:name]}.join('|')} \n"
print "#{Array.new(hashs.count, '-|').join('')} \n"

i = 1
datas.each_with_index do |array, index|
  next if index == 0
  tmp = []

  hashs.each do |hash|
    tmp << eval(hash[:command])
  end

  i+=1
  print "#{tmp.join('|')} \n"
end
