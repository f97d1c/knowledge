
<!-- TOC -->

- [目录结构](#目录结构)
- [命令示例](#命令示例)
  - [创建镜像](#创建镜像)
  - [启动镜像](#启动镜像)
  - [进入某个容器](#进入某个容器)
- [参考资料](#参考资料)

<!-- /TOC -->

# 目录结构

```
.
├── app
├──── dockerfiles
├────── example(具体目录)
├──── composes
├── bin(包装脚本)
├── config(配置文件)
├── env(环境变量)
├── lib(共享代码)
├── log(应用日志文件)
├── public(公开可访问目录,应用从这里开始)
├── README.md
├── test(单元,功能,集成测试文件)
├── tmp(运行时临时文件)
└── vendor(导入的代码)
```

# 命令示例

## 创建镜像

项目根目录下

```
docker-compose -f docker-compose-project_name.yml up/build [-d]
```

## 启动镜像

```
docker-compose -f docker-compose-project_name.yml run server_name
```

## 进入某个容器

容器启动后

```bash
# 通过下面命令查看运行中容器的name或id
docker ps

# 进入容器
docker attach 容器ID/容器名称
```

# 参考资料

> [docker-compose生成的容器立刻退出,exited with code 0](http://www.sail.name/2017/10/09/container-exited-with-code-0-created-by-docker-compose/)
