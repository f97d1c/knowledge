#!/usr/bin/env bash

ruby_version=""
while getopts ":v:a:h" optname
do
  case "$optname" in
    "v")
      ruby_version="$OPTARG"
      echo "设置ruby版本为: $ruby_version"
      ;;
    *)
      echo "未知参数项: $OPTIND"
      ;;
  esac
done

if [[ $ruby_version -eq "" ]];then echo '参数:-v(ruby版本)不能为空.';exit 1;fi

# 硬件平台架构
platform_version=$(uname -i)
# 基础系统版本标志
system_version='u18.04'
# 组合镜像标签名称
image_tag="$platform_version-$system_version-r$ruby_version-$(date +%Y%m%d)"
# 仓库名称
depository_name="ff4c00/ruby"
# 构建文件地址
docker_file_path='./app/docker/ruby/Dockerfile'
# 执行构建命令
docker build -t $depository_name:$image_tag -f $docker_file_path --build-arg RUBY_VERSION=$ruby_version --network host --rm --squash .

# 删除仓库或标签为none的镜像
docker rmi $(docker images | grep none | grep -v RESPOSITORY | awk '{print $3}')