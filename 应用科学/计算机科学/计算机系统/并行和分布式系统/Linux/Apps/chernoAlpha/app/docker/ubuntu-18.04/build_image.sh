# 硬件平台架构
platform_version=$(uname -i)
# 基础系统版本标志
system_version='u18.04'
# 更新源标志
source_from='ali'
# 组合镜像标签名称
image_tag="$platform_version-$system_version-$source_from-$(date +%Y%m%d)"
# 仓库名称
depository_name="ff4c00/linux"
# 构建文件地址
docker_file_path='./app/docker/ubuntu-18.04/Dockerfile'
# 时区文件路径
tzdata_localtime_file_time='/usr/share/zoneinfo/Asia/Shanghai'
# 执行构建命令
# docker build -t $depository_name:$image_tag -f $docker_file_path --build-arg TZDATA_LOCALTIME_FILE_TIME=$tzdata_localtime_file_time --squash .

docker buildx build --tag $depository_name:$image_tag --file $docker_file_path --build-arg TZDATA_LOCALTIME_FILE_TIME=$tzdata_localtime_file_time --allow security.insecure --load .