
<!-- TOC -->

- [构建](#构建)
  - [拉取镜像](#拉取镜像)
  - [运行容器](#运行容器)
  - [配置跨域](#配置跨域)
  - [进入容器](#进入容器)
  - [elasticsearch.yml](#elasticsearchyml)
  - [重启容器](#重启容器)
  - [ElasticSearch-Head](#elasticsearch-head)
  - [拉取镜像](#拉取镜像-1)
  - [运行容器](#运行容器-1)
- [参考资料](#参考资料)

<!-- /TOC -->

# 构建

## 拉取镜像

```bash
docker pull elasticsearch:6.8.1
```

## 运行容器

```bash
docker run -d --name els -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:6.8.1
```

## 配置跨域

## 进入容器

```bash
docker exec -it els /bin/bash
```

## elasticsearch.yml

```bash
vi config/elasticsearch.yml

# 加入跨域配置
http.cors.enabled: true
http.cors.allow-origin: "*"
```

## 重启容器

```bash
docker restart els
```


## ElasticSearch-Head

## 拉取镜像

```bash
docker pull mobz/elasticsearch-head:5
```

## 运行容器

```bash
docker run -d --name els_head -p 9100:9100 mobz/elasticsearch-head:5
```


# 参考资料

> [简书 | 使用Docker搭建ElasticSearch+Logstash+Kibana环境](https://www.jianshu.com/p/406a0f5aa03a)