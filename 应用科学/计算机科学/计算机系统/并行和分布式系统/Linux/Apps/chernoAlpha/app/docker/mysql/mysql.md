<!-- TOC -->

- [初始化数据库](#初始化数据库)
- [连接数据库](#连接数据库)
- [参考资料](#参考资料)

<!-- /TOC -->

# 初始化数据库

```bash
docker run -d -p 3307:3306 --name mysql -e MYSQL_ROOT_PASSWORD=指定root用户密码 -v 宿主机存储数据库数据目录:/var/lib/mysql 镜像库名:版本
```

```bash
docker run -d -p 3307:3306 --name mysql-5.7 -e MYSQL_ROOT_PASSWORD=123456 -v /home/ff4c00/db/mysql/mysql-5.7:/var/lib/mysql mysql:5.7.22
```


# 连接数据库

查看容器ip信息:

```bash
docker inspect --format='{{.NetworkSettings.IPAddress}}' 容器id/容器名称

# 获取所有
docker inspect --format='{{.NetworkSettings.IPAddress}}' $(docker ps -a -q)

docker inspect --format='{{.NetworkSettings.Networks.chernoalpha_default.Aliases}}{{.NetworkSettings.Networks.chernoalpha_default.IPAddress}}' $(docker ps -a -q) | grep mysql
```

宿主机通过ip连接数据库:

```bash
mysql -h 容器ip -u root -p
```

# 参考资料

> [CSDN | 使用Docker部署MySQL(数据持久化)](https://blog.csdn.net/u013096666/article/details/76522507)
