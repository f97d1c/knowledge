> Secure Shell(SSH)是一种加密 网络协议,用于在不安全的网络上安全地运行网络服务.<br>典型应用包括远程命令行 登录和远程命令执行,但可以使用SSH保护任何网络服务.<br>SSH 在客户端-服务器体系结构中通过不安全的网络提供安全通道,<br>将SSH客户端应用程序与SSH服务器连接.<br> --摘自[维基百科](https://en.wikipedia.org/wiki/Secure_Shell)

<!-- TOC -->

- [说明](#说明)
  - [起源](#起源)
  - [关于自动补全](#关于自动补全)
  - [检查服务器ssh是否可用](#检查服务器ssh是否可用)
    - [检查是否安装了ssh-server服务](#检查是否安装了ssh-server服务)
    - [安装ssh-server](#安装ssh-server)
    - [确认ssh-server是否启动](#确认ssh-server是否启动)
  - [配置openssh-server开机自动启动](#配置openssh-server开机自动启动)
  - [root用户开启ssh权限](#root用户开启ssh权限)
  - [连接服务器](#连接服务器)
  - [启动/停止/重启](#启动停止重启)
  - [传输文件](#传输文件)
    - [常用参数](#常用参数)
    - [从服务器下载文件](#从服务器下载文件)
    - [上传文件到服务器](#上传文件到服务器)
    - [断点续传(rsync)](#断点续传rsync)
  - [设置代理](#设置代理)
    - [代理IP更换为域名](#代理ip更换为域名)
  - [SSH存在的问题](#ssh存在的问题)
- [RSA免密登录](#rsa免密登录)
  - [hostA](#hosta)
    - [生成rsa公私钥](#生成rsa公私钥)
  - [hostB](#hostb)
    - [创建~/.ssh目录](#创建ssh目录)
  - [hostA](#hosta-1)
    - [拷贝公钥](#拷贝公钥)
  - [配置密钥对(通过config文件配置)](#配置密钥对通过config文件配置)
- [SSH agent信任列表](#ssh-agent信任列表)
  - [添加私钥](#添加私钥)
  - [查看已添加私钥](#查看已添加私钥)
- [.ssh文件夹](#ssh文件夹)
  - [config](#config)
    - [标准格式](#标准格式)
  - [known_hosts](#known_hosts)
    - [Tab补全显示原别名情况](#tab补全显示原别名情况)
- [Mosh](#mosh)
  - [安装](#安装)
    - [Ubuntu](#ubuntu)
    - [MacOS](#macos)
    - [依赖端口](#依赖端口)
  - [使用](#使用)
    - [链接服务器](#链接服务器)
    - [删除之前会话](#删除之前会话)
  - [相比ssh的优点](#相比ssh的优点)
- [端口转发](#端口转发)
  - [本地转发](#本地转发)
  - [远程转发](#远程转发)
    - [利用远程转发,实现代理功能](#利用远程转发实现代理功能)
- [常见问题](#常见问题)
  - [Permission denied](#permission-denied)
  - [mosh: Did not find remote IP address](#mosh-did-not-find-remote-ip-address)
  - [Warning: the ECDSA host key](#warning-the-ecdsa-host-key)
- [SSH文件传输协议(SFTP)](#ssh文件传输协议sftp)
  - [常用命令](#常用命令)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## 起源

SSH是一种网络协议,用于计算机之间的加密登录.<br>
最早的时候,互联网通信都是明文通信,一旦被截获,内容就暴露无疑.<br>
1995年,芬兰学者Tatu Ylonen设计了SSH协议,将登录信息全部加密,成为互联网安全的一个基本解决方案,迅速在全世界获得推广,目前已经成为Linux系统的标准配置.<br>


## 关于自动补全

通过 .ssh/config 配置的内容通常无法自动补全,<br>
可通过安装 bash-completion 解决:

```bash
brew install bash-completion # MacOs
sudo apt-get install bash-completion # Debian

echo '[ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion' >> ~/.bashrc
```

## 检查服务器ssh是否可用

### 检查是否安装了ssh-server服务

*默认只安装ssh-client服务* 

```bash
dpkg -l | grep ssh
```

### 安装ssh-server

如果上一步骤结果中没有 `openssh-server` 关键字,需要:

```bash
sudo apt-get install openssh-server
```

然后重复上一步骤

### 确认ssh-server是否启动

> 进程ssh-agent是客户端,sshd为服务器端.

```bash
ps -e | grep ssh
```

看到 *sshd* 说明ssh-server已经启动了.

如果没有则可以使用下面任意一种方式启动:

0. sudo /etc/init.d/ssh start
0. sudo service ssh start 

## 配置openssh-server开机自动启动

方式一:
```sh
sudo systemctl enable ssh
```

方式二:
打开/etc/rc.local文件,在exit 0语句前加入:

```bash
/etc/init.d/ssh start
```

## root用户开启ssh权限

```bash
sudo vim /etc/ssh/sshd_config
# 注释掉 PermitRootLogin without-password
# 添加 PermitRootLogin yes
```

## 连接服务器

```
ssh [-p 端口号(非默认22端口情况下)] user@hostname
```


## 启动/停止/重启

```bash
/etc/init.d/ssh start
/etc/init.d/ssh stop
/etc/init.d/ssh restart
```

## 传输文件

### 常用参数

参数|作用
-|-
r|批量上传/下载文件夹内文件
v|和大多数linux命令中的-v意思一样,用来显示进度.可以用来查看连接,认证,或是配置错误. 
C|使能压缩选项. 
P|选择端口.注意-p已经被rcp使用. 
4|强行使用IPV4地址. 
6|强行使用IPV6地址. 

### 从服务器下载文件

```bash
scp [-r 递归下载整个文件夹内容] <用户名>@<ssh服务器地址>:<文件路径> <本地文件名>
```

### 上传文件到服务器

```bash
scp [-P 端口] <本地文件名> <用户名>@<ssh服务器地址>:<上传保存路径即文件名>

scp -P 3256 $PWD/navugation_ul.png xxx@123.45.124.41:/home/xxx/images/navugation_ul.png

# 同时上传多个文件
scp $PWD/{icon_rank1.png,icon_rank2.png,icon_rank3.png,icon_radio1.png,icon_radio2.png,icon_tip.png} xxx@123.45.124.41:/home/xxx/images
```

### 断点续传(rsync)

```sh
rsync -P --rsh=ssh -h
```

* -P: 是包含了 *–partial –progress*, 部分传送和显示进度
* -rsh=ssh 表示使用ssh协议传送数据

## 设置代理

```bash
# ~/.ssh/config
Host 代理别名
  HostName 被代理网址
  User git
    # HTTP 代理
    ProxyCommand socat - PROXY:代理IP:%h:%p,proxyport=代理端口
    # Socks5 代理
    ProxyCommand nc -v -x 代理IP:代理端口 %h %p
```

如为 gitlab.com 网站git命令设置socks5代理:

```bash
Host gitlab.com
  HostName gitlab.com
  User git
    ProxyCommand nc -v -x 192.168.5.13:7891 %h %p
```

```bash
git pull origin master
# => Connection to gitlab.com 22 port [tcp/ssh] succeeded!
...
```

### 代理IP更换为域名

```cnf
Host gitlab.com
  HostName gitlab.com
  User git
  # ProxyCommand nc -v -x 192.168.8.142:7891 %h %p
  ProxyCommand bash -c "nc -v -x $(dig vpn.proxy.local +short):7891 %h %p"
  ServerAliveInterval 60
  ServerAliveCountMax 5
```

## SSH存在的问题

如果有人截获了登录请求,然后冒充远程主机,将伪造的公钥发给用户,那么用户很难辨别真伪.<br>
因为不像https协议,SSH协议的公钥是没有证书中心(CA)公证的,也就是说,都是自己签发的.<br>

可以设想,如果攻击者插在用户与远程主机之间(比如在公共的wifi区域),用伪造的公钥,获取用户的登录密码.<br>
再用这个密码登录远程主机,那么SSH的安全机制就荡然无存了.<br>
这种风险就是著名的 *中间人攻击(Man-in-the-middle attack)*.



# RSA免密登录

> 什么是ssh免密登录

假设hostA上的一个用户aliceA,以用户aliceB的身份ssh到hostB上,在这一过程中无需输入密码.


## hostA

### 生成rsa公私钥

以用户aliceA的身份登录到hostA上,<br>使用ssh-keygen生成一对rsa公私钥,生成的密钥对会存放在~/.ssh目录下.

```bash
ssh-keygen -t rsa -b 4096
```

## hostB

### 创建~/.ssh目录

在目标主机hostB上的aliceB用户目录下创建~/.ssh目录.

如果在aliceB@hostB上已经存在.ssh目录,这一步会被略过.

```bash
mkdir ~/.ssh
```

## hostA

### 拷贝公钥

将hostA上用户"aliceA"的公钥拷贝到aliceB@hostB上,来实现无密码ssh.

```bash
cat ~/.ssh/id_rsa.pub | ssh aliceB@hostB 'cat >> ~/.ssh/authorized_keys'
```

## 配置密钥对(通过config文件配置)

```bash
Host 配置别名
HostName IP地址
Port 22   # 端口号,默认为22
User root   # 登录账号
IdentityFile ~/.ssh/xxx.pem # 私钥文件在本地机上的存储路径
```

# SSH agent信任列表

> SSH agent 是一个密钥管理器,用来管理一个或多个密钥,并为需要使用 ssh key 的程序提供代理.<br>
如果私钥使用密码加密了的话,每一次使用 SSH 密钥对进行登录的时候,都必须输入正确的密码短语.<br>
而 SSH agent 程序能够将已解密的私钥缓存起来,在需要的时候提供给SSH 客户端.<br>
这样,就只需在使用 ssh-add 将私钥加入 SSH agent 缓存的时候,输入一次密码短语就可以了.<br>
这为经常使用 SSH 连接用户提供了不少便利.

## 添加私钥

```sh
ssh-add ~/私钥文件路径
```

## 查看已添加私钥

```sh
ssh-add -l
```
# .ssh文件夹

## config

> 记住所有的远程IP地址,不同的用户名,非标准端口和各种命令行选项非常困难的.<br>
一种选择是为每个远程服务器连接创建bash别名.<br>
但是,还有另一个更好,更简单的解决方案.<br>
OpenSSH允许设置每个用户的配置文件,可以在其中为所连接的每台远程计算机存储不同的SSH选项.

### 标准格式

```bash
Host 别名
  HostName IP地址
  Port 端口号
  User 登录账号
  IdentityFile 指定密钥认证使用的私钥文件路径 # 默认为 ~/.ssh/id_dsa, ~/.ssh/id_ecdsa, ~/.ssh/id_ed25519 或 ~/.ssh/id_rsa 中的一个
  UserKnownHostsFile 指定一个或多个用户认证主机缓存文件, 用来缓存通过认证的远程主机的密钥, 多个文件用空格分隔 # 默认缓存文件为: ~/.ssh/known_hosts, ~/.ssh/known_hosts2
  GlobalKnownHostsFile 指定一个或多个全局认证主机缓存文件,用来缓存通过认证的远程主机的密钥,多个文件用空格分隔 # 默认缓存文件为:/etc/ssh/ssh_known_hosts, /etc/ssh/ssh_known_hosts2.
```

## known_hosts

> ssh会把每个访问过计算机的公钥(public key)都记录在~/.ssh/known_hosts.<br>
当下次访问相同计算机时,OpenSSH会核对公钥.<br>
如果公钥不同,OpenSSH会发出警告,避免受到DNS Hijack之类的攻击.

### Tab补全显示原别名情况

当对config文件里面的别名进行更改后会出现tab补全显示新旧两者的情况.比如:

config中:

0. 原别名: home_server_arm64_8g_ubuntu_1
0. 新别名: home_server_arm64_8g_ubuntu_001

tab补全时还是会出现原别名的情况,进入文件将该记录删除即可.

# Mosh

> Mosh表示移动Shell(Mobile Shell),是一个用于从客户端跨互联网连接远程服务器的命令行工具.<br>
它能用于SSH连接,但是比Secure Shell功能更多.<br>
它是一个类似于SSH而带有更多功能的应用.<br>
程序最初由Keith Winstein编写,用于类Unix的操作系统中,发布于GNU GPL V3协议下.<br>
其***在SSH的基础上***采用了一个基于UDP的State Synchronization Protocol新协议来专门解决网络延迟的问题,<br>并且在交互视觉上Mosh也做了调整,采用了与SSH相反的方式「先显示,后传输执行」,<br>默认的SSH是每次敲击都要与服务器进行通信,但Mosh会先显示缓存输入然后再一并提交,<br>二者带来的体验差别 是显而易见的.

## 安装

### Ubuntu

```
sudo apt-get update; sudo apt-get install mosh
```

### MacOS

```
brew install mosh
```

### 依赖端口

mosh**额外**依赖UDP 60001端口, 需进行开放该端口.

## 使用

### 链接服务器

```
mosh 用户名@地址
```

### 删除之前会话

```bash
# Mosh: You have a detached Mosh session on this server (mosh [16994]).
# kill pid

kill 16994

# 或者
kill `pidof mosh-server`

```

## 相比ssh的优点

0. 它是一个支持漫游的远程终端程序.
0. 在所有主流的类 Unix 版本中可用,如 Linux、FreeBSD、Solaris、Mac OS X和Android
0. 支持不稳定连接
0. 支持智能的本地回显
0. 支持用户输入的行编辑
0. 响应式设计及在 wifi、3G、长距离连接下的鲁棒性
0. 在IP改变后保持连接.它使用UDP代替TCP(在SSH中使用),当连接被重置或者获得新的IP后TCP会超时,但是UDP仍然保持连接
0. 在很长的时候之后恢复会话时仍然保持连接
0. 没有网络延迟.立即显示用户输入和删除而没有延迟
0. 像SSH那样支持一些旧的方式登录
0. 包丢失处理机制
0. 会话的中断不会导致当前正在前端执行的命令中断,相当于所有的操作都是在screen命令中一样在后台执行.
0. 会话在中断过后,不会立刻退出,而是启用一个计时器,当网络恢复后会自动重新连接,同时会延续之前的会话,不会重新开启一个.

# 端口转发

SSH 不仅仅能够自动加密和解密 SSH 客户端与服务端之间的网络数据,<br>
同时,SSH 还能够提供了一个非常有用的功能,那就是端口转发,即将TCP 端口的网络数据,<br>
转发到指定的主机某个端口上,在转发的同时会对数据进行相应的加密及解密.<br>
如果工作环境中的防火墙限制了一些网络端口的使用,<br>
但是允许 SSH 的连接,那么也是能够通过使用SSH转发后的端口进行通信.<br>
转发,主要分为本地转发与远程转发两种类型.

转发参数:

参数|含义
-|-
-C|压缩数据
-f|后台认证用户/密码,通常和-N连用,不用登录到远程主机.
-N|不执行脚本或命令,通常与-f连用.<br>
-g|在-L/-R/-D参数中,允许远程主机连接到建立的转发的端口,<br>如果不加这个参数,只允许本地主机建立连接.
-L|本地端口:目标IP:目标端口
-D|动态端口转发
-R|远程端口转发
-T|不分配TTY只做代理用
-q|安静模式,不输出 错误/警告 信息

## 本地转发

> 有本地网络服务器的某个端口,转发到远程服务器某个端口.<br>
说白了就是,将发送到本地端口的请求,转发到目标端口.

```sh
ssh -L 本地网卡地址:本地端口:目标地址:目标端口 用户@目标地址
```

本地3306端口的请求转发到远程地址的3306端口上:

```sh
# ssh -L 本地地址:本地端口:远程地址:远程端口 用户@目标地  

# 仅本地可以访问
ssh -L 127.0.0.1:3306:localhost:3306 用户@目标地址
# 局域网内可以访问
ssh -L 0.0.0.0:3306:localhost:3306 用户@目标地址
```

等价于配置文件格式:

```cnf
LocalForward 0.0.0.0:3306 localhost:3306
```

处理流程:

0. 本地应用将数据发送到本地的127.0.0.1上面的3306端口.
0. 将本地3306端口的数据,通过SSH转发到目标地址的3306端口.
0. 目标地址将处理后的数据,原路返回到本地.

## 远程转发

> 由远程服务器的某个端口,转发到本地网络的服务器某个端口.<br>
即把发给远程机器的某个端口请求,转发到本地的机器上面.

```sh
ssh -R 远程网卡地址:远程端口:目标地址:目标端口
```

发送至192.168.13.149的8081端口的请求转发到本地的80端口上

```sh
# 本地执行
sudo ssh -f -N -R 8081:127.0.0.1:80 dequan@192.168.13.149
```

```sh
# 尚未成功应用
Host remote-forward
  HostName test.example.com
  RemoteForward remote-port target-host:target-port
```

### 利用远程转发,实现代理功能

> B能访问A, C不能访问A,但是能够访问B.如何让C利用B来访问A呢?<br>
让B机器作为代理,转发其他机器访问本地(B)的8081端口请求到A机器的80端口上面呢.

需要将B的监听,由127.0.0.1:8081,改为0:0.0.0:8081,<br>
修改sshd的配置/etc/ssh/sshd_config.

```sh
vim /etc/ssh/sshd_config
# 如果有
GatewayPorts no
# 改为
GatewayPorts yes
# 没有,添加即可

# 然后重启sshd
sudo service sshd restart
```

然后重新,设置动态转发,如下:

```sh
# A上执行:
ssh -f -g  -N -R 8081:127.0.0.1:80 dequan@192.168.13.149
```

# 常见问题

## Permission denied

> Permission denied, please try again.

> 问题源

***在输入密码正确的前提下***,如果出现该问题,原因在于被远程用户没有ssh权限.

> 解决方案1

当前用户的ssh权限,即修改 /etc/ssh/sshd_config 文件中:

```bash
PermitRootLogin without-password
 改为
PermitRootLogin yes
```

> 解决方案2

修改 /etc/ssh/sshd_config 文件:

```bash
 Change to no to disable tunnelled clear text passwords 
 把PasswordAuthentication的#号去掉就行了
PasswordAuthentication yes 
```

我的问题解决在于将密码0写成了o...

## mosh: Did not find remote IP address

> mosh: Did not find remote IP address (is SSH ProxyCommand disabled?)

当远程主机端口非22时,不能使用 --prot 指定端口.

```
--ssh="ssh -p 远程主机ssh端口"
```

```
mosh ff4c00@ruby_server_yggc_public_network --ssh="ssh -p 34256"
```

## Warning: the ECDSA host key 

> Warning: the ECDSA host key for 'home_server_arm64_8g_ubuntu_001' differs from the key for the IP address '10.168.1.204'

```sh
ssh-keygen -R 10.168.1.204
```

# SSH文件传输协议(SFTP)

> 在计算机领域,SSH文件传输协议(英语:SSH File Transfer Protocol,也称Secret File Transfer Protocol,中文:安全文件传送协议,英文:Secure FTP或字母缩写:SFTP)是一数据流连线,<br>
提供文件访问、传输和管理功能的网络传输协议.<br>
由互联网工程任务组(IETF)设计,透过SSH 2.0 的扩展提供安全文件传输能力,但也能够被其他协议使用.<br>
即使IETF在网络草案资料阶段时,这个协议是在SSH-2文件中描述,它能够使用在许多不同的应用程序,例如安全文件传输在传输层安全(TLS)和传输信息管理于虚拟专用网应用程序.<br>
这个协议是假设执行在安全信道,例如SSH,服务器已经认证客户端,并且客户端用户可利用协议.

## 常用命令

```sh
# 连接
sftp -p 用户名@地址

# 上传文件
put 本地文件的路径 将文件版保存到远程主机的路径

# 下载文件
get 远程主机下文件的路径 将文件保存到本地电脑的路径 
```

# 参考资料

> [Linux 中国 | Linux 下 SSH 命令实例指南](https://linux.cn/article-3858-1.html)

> [CSDN | 树莓派普通用户ssh出现Permission denied, please try again的解决方法](https://blog.csdn.net/hnlyzxy123/article/details/54375694)

> [CSDN | 远程登录 Linux 服务器报错:Permission denied, please try again.](https://blog.csdn.net/xsj_blog/article/details/72802837)

> [CSDN | 使用SSH传输文件/文件夹](https://blog.csdn.net/bedisdover/article/details/51622133)

> [Linux中国 | 如何在 Linux 中创建 SSH 别名](https://zhuanlan.zhihu.com/p/65655637?utm_source=wechat_session&utm_medium=social&utm_oi=638272867729674240)

> [Linux中国 | 如何实现 ssh 无密码登录](https://mp.weixin.qq.com/s?__biz=MjM5NjQ4MjYwMQ==&mid=205858279&idx=1&sn=c28e8dec2c01c4cde374cb6b5a3109bc&mpshare=1&scene=1&srcid=011603EvxEzFqbRyJ27kmUV9&pass_ticket=TghIXOUy4wDPvEmr3laK5VNeWliWR5%2BJypfUDMda7qmtLe%2FhuxgFxvMj8OsTMLFX#rd)

> [简书 | 使用mosh连接Linux服务器](https://www.jianshu.com/p/c207fb08411e)

> [程序园 | 使用Mosh来改善高延迟网络下的SSH体验](http://www.voidcn.com/article/p-dryugkbt-bmv.html)

> [Github | Error while connecting to remote server in ubuntu 16.04](https://github.com/mobile-shell/mosh/issues/890)

> [CSDN | scp传文件指定端口](https://blog.csdn.net/qq_29307291/article/details/72819802)

> [stackoverflow | How do I reattach to a detached mosh session?](https://stackoverflow.com/questions/17857733/how-do-i-reattach-to-a-detached-mosh-session)

> [CSDN | scp拷贝文件及文件夹](https://blog.csdn.net/imzkz/article/details/5414546)

> [stackoverflow | How do I reattach to a detached mosh session?](https://stackoverflow.com/questions/17857733/how-do-i-reattach-to-a-detached-mosh-session)

> [imciel | 设置 Git 代理](https://imciel.com/2016/06/28/git-proxy/)

> [stackexchange | Autocomplete server names for SSH and SCP](https://unix.stackexchange.com/questions/136351/autocomplete-server-names-for-ssh-and-scp/181603#181603)

> [simplified | How to enable bash completion in macOS](https://www.simplified.guide/macos/bash-completion)

> [不忘初心 方得始终 | 使用 SSH config 文件](https://daemon369.github.io/ssh/2015/03/21/using-ssh-config-file)

> [superuser | How to fix warning about ECDSA host key](https://superuser.com/questions/421004/how-to-fix-warning-about-ecdsa-host-key)

> [简书 | SSH下known_hosts的作用](https://www.jianshu.com/p/af5080794f30)

> [Linux公社 | SSH 只能用于远程 Linux 主机？那说明你见识太少了！](https://mp.weixin.qq.com/s/2Zz8U8J-L8HSTPt2l9bK9g)

> [简书 | Mac上管理多个Git账号](https://www.jianshu.com/p/e34410b47dd2)

> [github | [Linux][SSH] 配置端口转发](https://mdgsf.github.io/2019/08/01/linux-ssh-study/)

> [Thenext | deepin安装ssh服务并设置开机自启动](https://www.cnblogs.com/Thenext/p/15437824.html)