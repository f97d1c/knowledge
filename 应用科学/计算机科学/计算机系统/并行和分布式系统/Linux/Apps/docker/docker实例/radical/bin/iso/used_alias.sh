#!/bin/bash
tee -a /home/${OPERATOR_USER}/.bashrc <<-'EOF'
echo '为用户写入常用别名'
alias start_sshd="sudo /etc/init.d/ssh start"
alias git_cache='git config --global credential.helper '\''cache --timeout 36000000000'\'''
alias create_alias_for_this_folder="bash + x $RADICAL_BIN_PATH/iso/create_alias_for_this_folder.sh"
EOF