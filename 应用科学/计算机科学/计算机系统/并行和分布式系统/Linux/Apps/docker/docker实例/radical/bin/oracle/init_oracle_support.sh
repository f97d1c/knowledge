# 下载并解压依赖
# tar -xf  $PWD/${ORACLE_SUPPORT_DIR_NAME}.tar.gz 
# 创建目录
sudo mkdir -p /opt/oracle
# 复制文件
# sudo cp -rf ${ORACLE_SUPPORT_DIR_NAME}/ /opt/oracle/${ORACLE_SUPPORT_DIR_NAME}
sudo cp -rf /tmp/scripts/db/oracle/instantclient_12_1/ /opt/oracle/${ORACLE_SUPPORT_DIR_NAME}
# 创建链接
sudo ln -s /opt/oracle/${ORACLE_SUPPORT_DIR_NAME}/libclntsh.so.12.1 /opt/oracle/${ORACLE_SUPPORT_DIR_NAME}/libclntsh.so

# 写入环境变量
tee -a /home/${OPERATOR_USER}/.bashrc <<-'EOF'
export LD_LIBRARY_PATH=/opt/oracle/${ORACLE_SUPPORT_DIR_NAME} 
export NLS_LANG=AMERICAN_AMERICA.UTF8 # 汉字显示乱码
EOF

echo "127.0.0.1 $(hostname)" | sudo tee -a /etc/hosts

sudo apt-get install libaio1
