#!/bin/bash

echo '静默方式安装Mysql'
echo '下载mysql-server'
sudo apt-get install -yd mysql-server
echo ''
cd /var/cache/apt/archives/
# mysql-server-5.7_5.7.26-0ubuntu0.18.04.1_amd64.deb
# mysql-server-core-5.7_5.7.26-0ubuntu0.18.04.1_amd64.deb
# mysql-server_5.7.26-0ubuntu0.18.04.1_all.deb
# 如何匹配到第一条
echo '使用dpkg-preconfigure得到Mysql所需配置信息'
sudo dpkg-preconfigure /var/cache/apt/archives/mysql-server-5.7_5.7.26-0ubuntu0.18.04.1_amd64.deb


cat <<- DEBCONF| sudo debconf-set-selections
mysql-server mysql-server/root_password_again password jiacongfei
mysql-server mysql-server/root_password password jiacongfei
DEBCONF

sudo apt-get install -y mysql-server

# 参考资料
# [CSDN | 在Ubuntu下实现静默安装程序](https://blog.csdn.net/xkjcf/article/details/78700414)