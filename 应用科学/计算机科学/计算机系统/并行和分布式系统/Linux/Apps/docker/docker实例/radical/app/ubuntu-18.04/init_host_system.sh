#!/bin/bash

# 用于宿主机系统初始化使用

if [ ! $RADICAL_BIN_PATH ]; then
  read -p "请输入脚本所在路径:" path
  if [ ! $path ]
  then
    echo '参数不能为空'
    exit 1
  else
    echo "export RADICAL_BIN_PATH=$path" >> /root/.bashrc
  fi
fi 

if [ ! $OPERATOR_USER ]; then
  read -p "请输入系统普通用户:" user
  if [ ! $user ]
  then
    echo '参数不能为空'
    exit 1
  else
    echo "export OPERATOR_USER=$user" >> /root/.bashrc
  fi
fi 

if [ ! $NEED_DOCKER ]; then
  read -p "是否需要安装docker?(y/n)" choice
  case ${choice} in
    y|Y|yes|YES)
      echo "export NEED_DOCKER=true" >> /root/.bashrc
    ;;
    *)
      echo "export NEED_DOCKER=false" >> /root/.bashrc
    ;;
  esac
fi

source /root/.bashrc

bash -x $RADICAL_BIN_PATH/iso/debian/init_system.sh

echo '安装和设置docker'
su - ${OPERATOR_USER} <<EOF
sudo apt-get install -fy docker.io docker-compose
bash -x $RADICAL_BIN_PATH/iso/used_alias.sh
if [ $NEED_DOCKER == true ]; then
  sudo groupadd docker
  sudo usermod -aG docker $OPERATOR_USER
fi
EOF




