> 什么是Ansible

Ansible用于在多台机器之间同步软件的配置.

> 关于YAML

所有的YAML文件(无论和Ansible有没有关系)开始行都应该是 `---`.<br>
这是YAML格式的一部分, 表明一个文件的开始:

```yml
---
# 一个美味水果的列表
- Apple
- Orange
- Strawberry
- Mango
```

文件开头为 `-`, 这是YAML将文件解释为正确的文档的要求.<br>
YAML允许多个 *文档* 存在于一个文件中,每个*文档* 由一符号分割,<br>
但Ansible只需要一个文件存在一个文档即可, 因此这里需要存在于文件的开始行第一行.

YAML对空格非常敏感,并使用空格来将不同的信息分组在一起,<br>
在整个文件中应该只使用 `空格` 而不使用 `制表符`,并且必须使用一致的间距,才能正确读取文件.<br>
相同缩进级别的项目被视为同级元素.


> 注意事项

***配置文件中不要用Tab进行缩进,这将导致下面的内容全部失败***

<!-- TOC -->

- [搭建](#%E6%90%AD%E5%BB%BA)
  - [安装](#%E5%AE%89%E8%A3%85)
    - [通过PPA安装](#%E9%80%9A%E8%BF%87ppa%E5%AE%89%E8%A3%85)
    - [pip安装](#pip%E5%AE%89%E8%A3%85)
      - [下载get-pip.py](#%E4%B8%8B%E8%BD%BDget-pippy)
      - [运行](#%E8%BF%90%E8%A1%8C)
      - [检查](#%E6%A3%80%E6%9F%A5)
      - [配置国内镜像源](#%E9%85%8D%E7%BD%AE%E5%9B%BD%E5%86%85%E9%95%9C%E5%83%8F%E6%BA%90)
        - [常用国内源](#%E5%B8%B8%E7%94%A8%E5%9B%BD%E5%86%85%E6%BA%90)
        - [.pip](#pip)
        - [vim pip.conf](#vim-pipconf)
    - [通过源码安装](#%E9%80%9A%E8%BF%87%E6%BA%90%E7%A0%81%E5%AE%89%E8%A3%85)
  - [Git仓库](#git%E4%BB%93%E5%BA%93)
  - [初始化剧本](#%E5%88%9D%E5%A7%8B%E5%8C%96%E5%89%A7%E6%9C%AC)
    - [with_items](#withitems)
  - [任务手册](#%E4%BB%BB%E5%8A%A1%E6%89%8B%E5%86%8C)
    - [创建目录](#%E5%88%9B%E5%BB%BA%E7%9B%AE%E5%BD%95)
  - [全新的local.yml](#%E5%85%A8%E6%96%B0%E7%9A%84localyml)
  - [新建用户](#%E6%96%B0%E5%BB%BA%E7%94%A8%E6%88%B7)
  - [user.yml](#useryml)
  - [cron.yml](#cronyml)
  - [copy模块](#copy%E6%A8%A1%E5%9D%97)
  - [sudoers_ansible](#sudoersansible)
- [ansible-pull](#ansible-pull)
- [主机与组](#%E4%B8%BB%E6%9C%BA%E4%B8%8E%E7%BB%84)
  - [把一个组作为另一个组的子成员](#%E6%8A%8A%E4%B8%80%E4%B8%AA%E7%BB%84%E4%BD%9C%E4%B8%BA%E5%8F%A6%E4%B8%80%E4%B8%AA%E7%BB%84%E7%9A%84%E5%AD%90%E6%88%90%E5%91%98)
- [参考资料](#%E5%8F%82%E8%80%83%E8%B5%84%E6%96%99)

<!-- /TOC -->

# 搭建

## 安装

### 通过PPA安装

```bash
sudo apt-get install -y software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get install ansible
```

### pip安装

树莓派等无法通过ppa安装,可以选择pip安装.

***pip安装完成后在***

#### 下载get-pip.py

```bash
wget https://bootstrap.pypa.io/get-pip.py
```

#### 运行

```bash
sudo python get-pip.py
```

#### 检查

```bash
pip -V
```

如果能正常输出版本,说明安装成功.


#### 配置国内镜像源

pip 默认采用的源在国外,所以在使用 pip 安装包的时候下载速度缓慢且会碰到超时无法连接的情况,可以切换为国内的源,方便使用.

如果是临时使用,只需在install后加上-i参数即可:

```bash
sudo apt-get install python-yaml python-httplib2 libffi-dev libssl-dev
sudo pip install -i http://mirrors.aliyun.com/pypi/simple/  paramiko PyYAML Jinja2 httplib2

pip install -i http://mirrors.aliyun.com/pypi/simple/ ansible
```

##### 常用国内源

名称|地址
-|-
清华大学|https://pypi.tuna.tsinghua.edu.cn/simple/
阿里云|http://mirrors.aliyun.com/pypi/simple/
豆瓣|http://pypi.douban.com/simple/
华中理工大学|http://pypi.hustunique.com/simple/
山东理工大学|http://pypi.sdutlinux.org/simple/
中国科学技术大学|http://pypi.mirrors.ustc.edu.cn/simple/

##### .pip

```bash
cd ~
mkdir .pip
```

##### vim pip.conf

```bash
cd .pip
vim pip.conf
# 写入下面内容,具体内容以选择源为准
[global]
index-url=http://mirrors.aliyun.com/pypi/simple/
[install]
trusted-host=mirrors.aliyun.com
```

### 通过源码安装

```bash
git clone https://github.com/ansible/ansible.git --recursive
cd ./ansible
source ./hacking/env-setup
```

## Git仓库

1. 创建一个空的仓库,如ansible
2. 克隆到本地

## 初始化剧本

在仓库中初始化 local.yml 文件.<br>
local.yml 文件又被称之为 *剧本*.

local.yml是预设的剧本名称,在执行ansible-pull时如果仓库的根目录中找到名为local.yml的剧本,它将自动运行它.

写入内容:

```yml
- hosts: localhost
  become: true
  tasks:
    # name只是提供有关任务描述信息,在起到注释描述作用的同时,并将显示在执行过程的输出中
  - name: Install htop
    # 如果使用的是其他平台(非Debian)需要修改apt模块为对应指令
    apt: name=htop
```

上面的配置是安装htop的指令.

### with_items

如果配置安装多个软件可以按照下面格式进行优化, *注意with_items下面的缩进*
```yml
- hosts: localhost
  become: true
  tasks:
  - name: Install packages
    apt: name={{item}}
    with_items:
      - htop
      - mc
      - tmux
```

## 任务手册

随着向配置中不断的添加内容,local.yml文件将会变的相当的庞大和杂乱.<br>
可以利用任务手册,根据不同类型的配置将动作分为独立的文件.<br>
它和剧本playbook很像但内容更加的流线型.

### 创建目录

```bash
mkdir tasks
```

local.yml 剧本中的代码可以过渡为安装包文件的任务手册.<br>
把这个文件移动到task目录中,并重新命名.

```bash
mv local.yml tasks/packages.yml
```

精简除了独立任务本身之外的所有内容:

```yml
# /ansible/tasks/packages.yml
- name: Install packages
  apt: name={{item}}
  with_items:
    - htop
    - mc
    - tmux
```

## 全新的local.yml

现在仍然需要一个名为local.yml的文件,<br>
因为执行 ansible-pull 命令时仍然会去找这个文件.

新的local.yml扮演的是导入任务手册的索引的角色.

```yml
# /ansible/local.yml
  hosts: localhost
  become: true
  # pre_tasks任务的作用是在其他所有任务运行之前先运行某个任务
  pre_tasks:
      name: update repositories
      # 对系统的软件库进行更新(apt update命令)
      apt: update_cache=yes
      # 在过程报告中忽略所有变动
      changed_when: False
  tasks:
      include: tasks/packages.yml
```

```yml
- hosts: localhost
  become: true
  pre_tasks:
    - name: update repositories
      apt: update_cache=yes
      changed_when: False
  tasks:
    - include: tasks/packages.yml
```

## 新建用户

创建一个特殊的账户来应用Ansible配置.<br>
这个不是必要的,可以在自己的用户下运行Ansible配置.<br>
但是使用一个隔离的用户能够将其隔离到后台运行的一个系统进程中.

为这个用户声明了用户ID为900.这个不是必须的,但建议直接创建好 UID.<br>
因为在1000以下的UID在登录界面是不会显示的,这样是很棒的,因为根本没有需要去使用ansibe账户来登录桌面.<br>

```bash
# 创建前可以用下面命令来验证该id是否被占用
cat /etc/passwd |grep 900
# 新增用户并指定uid
sudo useradd -u 900 -d /usr/ansible  -g users -m  ansible
```

## user.yml

```yml
# /ansible/tasks/user.yml
- name: create ansible user
  # 下面的是创建的用户信息
  user: name=ansible uid=900
```

编辑local.yml文件,将这个新的任务手册添加进去:

```yml
  ...
  tasks:
    - include: tasks/user.yml
  ...
```

现在当运行ansible-pull命令的时候,<br>
一个名为ansible的用户将会在系统中被创建

## cron.yml

创建实际的定时作业来自动操作:

```yml
# /ansible/tasks/cron.yml
- name: install cron job (ansible-pull)
  cron: user="ansible" name="ansible provision" minute="*/10" job="/usr/bin/ansible-pull -o -U 远程仓库地址 > /dev/null"
```

将定时任务添加到local.yml文件中以便它能够被调用.<br>
将下面的一行添加到末尾:
```yml
  ...
  tasks:
  ...
    - include: tasks/cron.yml
```

现在当ansible-pull命令执行时,<br>
它将会以用户ansible每隔十分钟设置一个新的定时作业.<br>
但每隔十分钟运行一个Ansible作业并不是一个好的方式,<br>
因为这个将消耗很多的CPU资源.<br>
每隔十分钟来运行对于Ansible来说是毫无意义的,除非已经在Git仓库中改变一些东西.

上面这个问题的解决得益于,<br>
在定时作业中的命令 ansible-pill 添加了之前从未用到过的参数 `-o`.<br>
这个参数告诉Ansible只有在从上次 ansible-pull 被调用以后库有了变化后才会运行.<br>
如果库没有任何变化,它将不会做任何事情.通过这个方法,将不会无端的浪费CPU资源.

## copy模块

尽管已经添加了所有必须的配置要素来自动化 ansible-pull,它仍然还不能正常的工作.<br>
ansible-pull 命令需要 sudo 的权限来运行,<br>
这将允许它执行系统级的命令.然而创建的用户ansible并没有被设置为以sudo的权限来执行命令,<br>
因此当定时作业触发的时候,执行将会失败.<br>
通常可以使用命令visudo来手动的去设置用户ansible去拥有这个权限.

然而现在应该以Ansible的方式来操作,<br>
而且这将会是一个展示copy模块是如何工作的机会.<br>
copy模块允许从库复制一个文件到文件系统的任何位置.

将下面的的动作添加到文件末尾:

```yml
# /ansible/tasks/user.yml
  name: copy sudoers_ansible
  # 复制sudo的一个配置文件到/etc/sudoers.d/以便用户ansible能够以管理员的权限执行任务
  # 抓取一个名为sudoers_ansible(将在后续创建)的文件并将它复制为/etc/sudoers/ansible,并且拥有者为root
  copy: src=files/sudoers_ansible dest=/etc/sudoers.d/ansible owner=root group=root mode=0440
```

## sudoers_ansible

```bash
# 创建一个名为files的目录
mkdir files

# 在files下创建名为sudoers_ansible的文件
touch sudoers_ansible
# 包含以下内容
ansible ALL=(ALL) NOPASSWD: ALL
```

现在通过sudo允许用户ansible不需要密码提示就拥有完全控制权限.<br>
这将允许ansible-pull以后台任务的形式运行而不需要手动去运行:

```bash
sudo ansible-pull -U 远程仓库地址
```

<hr>

现在已经拥有了一个完整的可工作方案.<br>
当第一次设置一台新的笔记本或者台式机的时候,只需去手动的运行ansible-pull 命令,但仅仅是在第一次的时候.<br>
从第一次之后,用户ansible将会在后台接手后续的运行任务.<br>
当想对机器做变动的时候,只需要去上传最新配置到远程仓库.<br>
接着,当定时作业下次在每台机器上运行的时候,它将会拉取变动的部分并应用它们.<br>
这方法尽管有一点不同寻常,通常,应该会有一个包含机器列表和不同机器所属规则的清单文件.<br>
然而,ansible-pull的方法,就像在文章中描述的,是管理工作站配置的非常有效的方法.<br>

# ansible-pull

`ansible-pull`命令将从Git仓库下载配置并立即应用它.<br>
因此不需要维护服务器或库存清单,只需运行ansible-pull命令,给它一个Git仓库URL,它将完成剩下的工作.

```bash
sudo ansible-pull -U 仓库地址
```

如果执行成功,即htop安装成功在输出的最后一行将看到 *changed = 1*

# 主机与组

`/etc/ansible/hosts` 文件的格式与windows的ini配置文件类似:

```bash
mail.example.com

# 方括号[]中是组名,用于对系统进行分类,便于对不同系统进行个别的管理.
[webservers]
foo.example.com
bar.example.com

[dbservers]
# 如果有主机的SSH端口不是标准的22端口,可在主机名之后加上端口号,用冒号分隔
badwolf.example.com:5309
```

## 把一个组作为另一个组的子成员

```bash
[atlanta]
host1
host2

[raleigh]
host2
host3

[southeast:children]
atlanta
raleigh
```

# 参考资料

> [Linux 中国 | 如何使用 Ansible 管理的工作站配置](https://linux.cn/article-10434-1.html)

> [Linux 中国 | 使用 Ansible 来管理的工作站:配置自动化](https://linux.cn/article-10449-1.html)

> [Ansible中文权威指南](https://ansible-tran.readthedocs.io/en/latest/)

> [Ansible中文权威指南 | 用户与组](https://ansible-tran.readthedocs.io/en/latest/docs/intro_inventory.html?highlight=Inventory)

> [laylib | 树莓派Raspbian安装pip](http://laylib.com/archives/33.html)