<!-- TOC -->

- [准备](#准备)
  - [安装](#安装)
  - [tmux.conf](#tmuxconf)
  - [启动](#启动)
- [使用](#使用)
  - [分屏](#分屏)
    - [横向](#横向)
    - [纵向](#纵向)
  - [全屏/退出全屏](#全屏退出全屏)
  - [复制内容](#复制内容)
    - [窗口内粘贴](#窗口内粘贴)
  - [会话](#会话)
    - [列出会话](#列出会话)
    - [退出(非关闭)](#退出非关闭)
    - [进入已有会话](#进入已有会话)
    - [删除指定session](#删除指定session)
    - [断开其他已连接用户](#断开其他已连接用户)
- [脚本](#脚本)
  - [新建会话](#新建会话)
  - [选择选项卡](#选择选项卡)
  - [横切](#横切)
  - [竖切](#竖切)
  - [运行命令](#运行命令)
  - [示例](#示例)
- [参考资料](#参考资料)

<!-- /TOC -->

# 准备

## 安装
```
sudo apt-get install tmux
```

## tmux.conf

用户根目录下(~/)新建.tmux.conf隐藏文件.<br>
将tmux.conf中的内容复制进去.

```bash
touch ~/.tmux.conf
```

## 启动

在终端下输入下面命令即可启动:

```bash
tmux
```

# 使用

> **!** 以下部分命令为根据个人喜好配置(.tmux.conf)而来,并非全部通用.

## 分屏

### 横向

```
C-v -
```

### 纵向

```
C-v |
```

## 全屏/退出全屏

```
C-v z
```

## 复制内容

### 窗口内粘贴

0. 鼠标选中(不要松开)+enter完成复制
0. `C-v ]` 进行粘贴

## 会话

### 列出会话

```bash
tmux ls
```

### 退出(非关闭)

```
tmux detach
```

快捷键: `C-v d`

### 进入已有会话

```
tmux a -t session_index
```

### 删除指定session

```
tmux kill-session -t <name-of-my-session>
```

### 断开其他已连接用户

```sh
tmux a -dt 会话下标
```

# 脚本

## 新建会话

```sh
tmux new-session -d
```

## 选择选项卡

编号从0开始,<br>
选择选项卡后后续操作将在该选项内执行

```sh
tmux selectp -t 选项卡编号
```

## 横切

```sh
tmux split-window -h
```

## 竖切

```sh
tmux split-window -v
```

## 运行命令

```sh
# tmux send-keys -t 会话编号:窗口编号.选项卡编号 "命令" C-m
# C-m用于回车
tmux send-keys -t 0:0.0 "btop" C-m
```

## 示例

```sh
# tmux默认大小为80x24 当脚本创建好进入后将重新计算高宽
# 创建时提供终端大小以避免根据百分比大小分割窗格出现大小不一致情况
tmux new-session -d -x "$(tput cols)" -y "$(tput lines)"

tmux split-window -h

tmux selectp -t 1
tmux split-window -v -p 50

tmux selectp -t 1
tmux split-window -v -p 50

tmux selectp -t 3
tmux split-window -v -p 50

tmux send-keys -t 0:0.0 "btop" C-m
```

# 参考资料

> [CSDN|终端分屏软件 tmux简单教程](https://blog.csdn.net/longxibendi/article/details/38541005)

> [小土刀 | tmux 指南](https://gitlab.com/ff4c00/hijack/blob/80230c25d59ddfe5e035d31d7aa7fc474a679538/Linux/Apps/Tmux/tmux.md#L94)

> [segmentfault | Tmux常用功能总结](https://segmentfault.com/a/1190000007427965)

> [stackexchange | tmux not splitting panes with desired percentage size](https://unix.stackexchange.com/questions/569729/tmux-not-splitting-panes-with-desired-percentage-size)