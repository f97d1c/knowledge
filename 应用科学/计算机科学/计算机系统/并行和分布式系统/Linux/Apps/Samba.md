
<!-- TOC -->

- [安装](#安装)
- [创建共享目录](#创建共享目录)
  - [设置权限](#设置权限)
- [添加用户](#添加用户)
- [修改配置文件](#修改配置文件)
- [测试配置](#测试配置)
- [重启服务](#重启服务)

<!-- /TOC -->

# 安装


安装samba:

```
sudo apt-get upgrade && sudo apt-get update && sudo apt-get dist-upgrade && sudo apt-get install samba samba-common
```

# 创建共享目录

## 设置权限

# 添加用户

***这里的用户为系统中已存在用户***

```
sudo smbpasswd -a 用户名
```

# 修改配置文件

```bash
sudo vim /etc/samba/smb.conf
```

*配置文件貌似不支持尾部添加注释,未深入探究.*

```bash
[共享名]
comment = # 备注信息
browseable = yes # 用来指定该共享是否可以浏览
path = # 文件夹绝对路径
create mask = 0700 # 定义用户在此共享资源中创建的文件的权限
directory mask = 0700 # 在共享文档中创建的文件夹的权限
valid users = # 设定仅允许指定的用户或组访问此共享资源，其他所有用户都不能访问。
# Invalid users= # 设定不允许访问此共享资源的用户或组
# Admin users= root,fred # 设定拥有共享管理特权的用户。即这些用户拥有些共享资源的管理权限。 @开头则表示一个组， +开头表示为UNIX组， &开头表示为NIS组
# force group 和force user 规定创建的文件或文件夹的拥有者和组拥有者是谁 。一般这两个值来空，则表示拥有者和组拥有者为创建文件者.
# force user =  
# force group = 
max connections =10 # 设置此共享资源的最大连接数
public = no # 是否允许匿名访问
available = yes # 用来指定该共享资源是否可用
writable = yes # 是否允许对共享资源有写的权限，yes为允许写，no为只读
```

# 测试配置

testparm 命令用来测试smb.conf配置档的合法性

# 重启服务

```
sudo service smbd restart
```