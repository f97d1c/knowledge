
<!-- TOC -->

- [说明](#说明)
  - [数据库](#数据库)
  - [实例](#实例)
  - [表空间](#表空间)
  - [数据文件(dbf、ora)](#数据文件dbfora)
  - [sqlplus命令](#sqlplus命令)
    - [connect](#connect)
      - [操作系统身份验证](#操作系统身份验证)
      - [常见登录方式](#常见登录方式)
- [容器部署](#容器部署)
  - [liumiaocn版11.2.0(快速部署)](#liumiaocn版1120快速部署)
    - [获取镜像](#获取镜像)
    - [启动实例](#启动实例)
    - [登录并切换用户](#登录并切换用户)
    - [查看 SID](#查看-sid)
    - [使用sqlplus](#使用sqlplus)
- [创建数据库](#创建数据库)
  - [创建数据库实例](#创建数据库实例)
  - [创建表空间](#创建表空间)
  - [创建用户](#创建用户)
    - [赋予用户DBA权限](#赋予用户dba权限)
  - [创建表(数据段)](#创建表数据段)
- [监听服务(lsnrctl)](#监听服务lsnrctl)
  - [显示监听器的状态(status)](#显示监听器的状态status)
  - [启动指定的监听器(start)](#启动指定的监听器start)
  - [关闭指定的监听器(stop)](#关闭指定的监听器stop)
  - [列举监听器的服务信息(services)](#列举监听器的服务信息services)
  - [显示版本信息(version)](#显示版本信息version)
  - [重启监听(reload)](#重启监听reload)
  - [备份配置文件(save_config)](#备份配置文件save_config)
  - [退出lsnrctl命令(exit/quit)](#退出lsnrctl命令exitquit)
- [配置文件](#配置文件)
  - [tnsnames.ora](#tnsnamesora)
    - [sqlplus连接时的需要的@tnsname从哪里看?](#sqlplus连接时的需要的tnsname从哪里看)
  - [sqlnet.ora](#sqlnetora)
  - [listener.ora](#listenerora)
- [基本操作](#基本操作)
  - [查询所有表空间物理位置](#查询所有表空间物理位置)
  - [删除表空间](#删除表空间)
  - [修改用户默认表空间](#修改用户默认表空间)
  - [重启服务](#重启服务)
    - [sqlplus](#sqlplus)
  - [查看所有表索引](#查看所有表索引)
  - [查看数据库运行状态](#查看数据库运行状态)
- [常见错误](#常见错误)
  - [数据文件大小到达上限(ORA-01658)](#数据文件大小到达上限ora-01658)
  - [(ORA-00955)](#ora-00955)
  - [时区设置不一致(ORA-01805)](#时区设置不一致ora-01805)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## 数据库

Oracle数据库是数据的物理存储.<br>
就是一堆文件(包括数据文件ORA或者DBF、控制文件、联机日志、参数文件等).<br>
其实Oracle数据库的概念和其它数据库不一样,这里的数据库是一个操作系统只有一个库.<br>
可以看作是Oracle就只有一个大数据库.

## 实例

是访问数据库文件的一个手段.<br>
一个Oracle实例(Oracle Instance)由一系列的后台进程(Backguound Processes)和内存结构(Memory Structures)组成.<br>
一个数据库可以有多个实例来访问,但一个实例只能访问一个数据库.

安装Oracle后会有默认的实例,即ORCL.<br>
一般不创建多个实例,在默认实例下创建表空间和用户等.

## 表空间

表空间是一个用来管理数据存储逻辑概念,表空间只是和数据文件(ORA或者DBF文件)发生关系,数据文件是物理的,一个表空间可以包含多个数据文件,而一个数据文件只能隶属一个表空间.

## 数据文件(dbf、ora)

数据文件是数据库的物理存储单位.<br>
数据库的数据是存储在表空间中的,表正是在某一个或者多个数据文件中.<br>
而一个表空间可以由一个或多个数据文件组成,一个数据文件只能属于一个表空间.<br>
一旦数据文件被加入到某个表空间后,就不能删除这个文件,如果要删除某个数据文件,只能删除其所属于的表空间才行.

![](https://upload-images.jianshu.io/upload_images/3410141-1d94190bd3594675.jpg?imageMogr2/auto-orient/strip|imageView2/2/w/641/format/webp)

![](https://upload-images.jianshu.io/upload_images/3410141-10ef934888622bfc.jpg?imageMogr2/auto-orient/strip|imageView2/2/format/webp)

## sqlplus命令

### connect

> 连接 给定的用户名 至 Oracle数据库. 当运行CONNECT命令时,将执行站点配置文件glogin.sql和用户配置文件login.sql

```
CONN[ECT] [{logon | / | proxy} [AS {SYSOPER | SYSDBA | SYSASM}] [edition=value]]

CONNECT 用户名/密码@tnsname//数据库地址:端口
```

#### 操作系统身份验证

只要操作系统用户是OSDBA或OSOPER组的一部分,就可以特权默认用户身份连接到当前节点上的实例:

```
CONNECT / AS SYSDBA
```

#### 常见登录方式

0. sqlplus "/ as sysdba" 这是典型的操作系统认证，不需要listener进程，数据库即使不可用也可以登录.
0. sqlplus username/password 不需要listener进程，登录本机数据库，数据库实例启动即可.
0. sqlplus username/password@tnsname需要listener进程，最常见的远程登录模式，需要启动数据库实例和listener进程.

# 容器部署

## liumiaocn版11.2.0(快速部署)

### 获取镜像

```
docker pull liumiaocn/oracle:11.2.0
```

### 启动实例

```
docker run -d                       \
-p 38080:8080 -p 31521:1521         \
-e DEFAULT_SYS_PASS=liumiaocn       \
-e processes=500                    \
-e sessions=555                     \
-e transactions=611                 \
--name oracle-11g                   \
liumiaocn/oracle:11.2.0
```

### 登录并切换用户

```sh
docker exec -it oracle-11g sh
su - oracle

# docker exec -ti oracle-11g-xe su oracle
```

### 查看 SID

```
echo $ORACLE_SID
```

### 使用sqlplus

使用sqlplus以sysdba的身份登录:

```
sqlplus /nolog
connect /as sysdba
```

# 创建数据库

## 创建数据库实例

安装Oracle后会有默认的实例,即ORCL.<br>
一般不创建多个实例,在默认实例下创建表空间和用户等.

## 创建表空间

> 创建名为 *test* 的表空间,数据文件为 *test.dbf*. 

```sql
-- 创建表空间
create tablespace test 
-- 表空间物理文件名称
-- 文件名称可以为全路径,文件将存储在指定路径下
-- datafile '/u01/app/oracle/mount_data/test.dbf'
datafile 'test.dbf'
-- 大小 500M,每次 5M 自动增大,最大不限制
size 500M autoextend on next 5M maxsize unlimited; 

-- 查询当前用户拥有的所的有表空间
select tablespace_name from user_tablespaces;
```

## 创建用户

创建用户并指定表空间：

```sql
-- 创建用户
-- 注意这里的空间名必须大写(因为Oracle自动将表空间名字全部转为大写)
-- create user 用户名 identified by 密码;
create user test identified by test default tablespace TEST;
```

### 赋予用户DBA权限

```sql
-- 赋予用户DBA权限
-- connect 链接权限
-- resource 资源权限 提供给用户另外的权限以创建他们自己的表、序列、过程(procedure)、触发器(trigger)、索引(index)和簇(cluster)
-- dba 数据库管理员角色 包括无限制的空间限额和给其他用户授予各种权限的能力 system由dba用户拥有
grant connect,resource,dba to test;
```

## 创建表(数据段)

> 表空间的创建应该退出sysdba用户,以新建用户登录创建.

在test表控件下创建一个名为users的表：

```sql
create table users
(
    name varchar(12),
    age varchar(12)
)
tablespace test;

-- 查看表空间下的所有表
select tablespace_name, table_name from user_tables where tablespace_name = 'TEST';
```

# 监听服务(lsnrctl)

## 显示监听器的状态(status)

> 命令显示监听器是不是活动的,日志与跟踪文件的位置,监听器已经持续运行了多长时间,以及监听器所监听的任务。

## 启动指定的监听器(start)



## 关闭指定的监听器(stop)



## 列举监听器的服务信息(services)



## 显示版本信息(version)



## 重启监听(reload)

重新装入监听器,重新读取listener.ora文件,但不关闭监听器。如果该文件发生了变化,重新刷新监听器。

## 备份配置文件(save_config)

当从lsnrctl工具中对listener.ora文件进行了修改时,复制一个叫做listener.bak的listener.ora的文件。

## 退出lsnrctl命令(exit/quit)


# 配置文件

## tnsnames.ora

> tnsnames.ora 用在oracle client端，用户配置连接数据库的别名参数,就像系统中的hosts文件一样。提供了客户端连接某个数据库的详细信息，主机地址，端口，数据库实例名等。

位于: $ORACLE_HOME/network/admin/tnsnames.ora

### sqlplus连接时的需要的@tnsname从哪里看?

```
ORCL.com =
  (DESCRIPTION =
    (ADDRESS_LIST =
      (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.0.147)(PORT = 1521))
    )
    (CONNECT_DATA =
      (SERVICE_NAME = orcl)
    )
  )
```

配置文件中的SERVICE_NAME为连接时使用的tnsname,<br>
即使用 sqlplus username/password@orcl 即可连接成功

## sqlnet.ora

> sqlnet.ora 用在oracle client端，用于配置连接服务端oracle的相关参数

## listener.ora

> tnslsnr进程是监听、并接受远程连接数据库请求的监听进程。listener.ora是tnslsnr进程的配置文件，监听的参数都是从该配置文件中读取，该文件位于服务端。如果只需要在本地连接数据库，不接受远程连接，那么也不需要启动tnslsnr进程，也不需要去维护listener.ora文件。

# 基本操作


## 查询所有表空间物理位置

```sql
select name from v$datafile;
```

## 删除表空间

```sql
-- 删除空的表空间，但是不包含物理文件
drop tablespace tablespace_name;
-- 删除非空表空间，但是不包含物理文件
drop tablespace tablespace_name including contents;
-- 删除空表空间，包含物理文件
drop tablespace tablespace_name including datafiles;
-- 删除非空表空间，包含物理文件
drop tablespace tablespace_name including contents and datafiles;
-- 如果其他表空间中的表有外键等约束关联到了本表空间中的表的字段，就要加上CASCADE CONSTRAINTS
drop tablespace tablespace_name including contents and datafiles CASCADE CONSTRAINTS;
```

## 修改用户默认表空间

```sql
alter user user_name default tablespace tbs_name;
```

## 重启服务

### sqlplus

必须使用sysdba身份用户执行:

```sql
-- 输入命令，等待执行结果
shutdown immediate;

-- 提示数据库已关闭后输入
startup;
```

## 查看所有表索引

```sql
select sequence_owner, sequence_name from dba_sequences;


DBA_SEQUENCES -- all sequences that exist 
ALL_SEQUENCES  -- all sequences that you have permission to see
USER_SEQUENCES  -- all sequences that you own
```

## 查看数据库运行状态

```sql
select status from v$instance;
```

# 常见错误

## 数据文件大小到达上限(ORA-01658)

> OCIError: ORA-01658: unable to create INITIAL extent for segment in tablespace ***

## (ORA-00955)

> OCIError: ORA-00955: name is already used by an existing object

## 时区设置不一致(ORA-01805)

Ruby项目中如果使用的ruby-oci8,可以在初始化中加入以下内容

```ruby
OCI8::BindType::Mapping[Time] = OCI8::BindType::LocalTime
OCI8::BindType::Mapping[:date] = OCI8::BindType::LocalTime
OCI8::BindType::Mapping[:timestamp] = OCI8::BindType::LocalTime
OCI8::BindType::Mapping[:timestamp_ltz] = OCI8::BindType::LocalTime
```


# 参考资料

> [CSDN | 运行在容器中的Oracle XE - 11g](https://blog.csdn.net/liumiaocn/article/details/82733272)

> [陈树义 | Oracle中如何创建数据库](https://www.cnblogs.com/chanshuyi/p/3821023.html)

> [简书 | 基础概念：Oracle数据库、实例、用户、表空间、表之间的关系](https://www.jianshu.com/p/4dc47beb4d8a)

> [CSDN | sqlnet.ora、tnsnames.ora、listener.ora配置详解](https://blog.csdn.net/chunhua_love/article/details/13505239)

> [博客园 | oracle 删除表空间及数据文件方法](https://www.cnblogs.com/klb561/p/11062247.html)

> [CSDN | oracle 修改用户的默认表空间](https://blog.csdn.net/yxlg518/article/details/11396405)

> [CSDN | oracle 数据库命令行重启](https://blog.csdn.net/yxlg518/article/details/7489695)

> [stackoverflow | How can I get all sequences in an Oracle database?](https://stackoverflow.com/questions/21738117/how-can-i-get-all-sequences-in-an-oracle-database)

> [Github | ORA-01805 and 11gR2 #28](https://github.com/kubo/ruby-oci8/issues/28)