<!-- TOC -->

- [备份工具](#备份工具)
  - [Rsync](#rsync)
- [命令行编辑器](#命令行编辑器)
  - [Vim](#vim)
- [IDE](#ide)
- [即时通信工具](#即时通信工具)
  - [deepin wine](#deepin-wine)
    - [安装deepin-wine环境](#安装deepin-wine环境)
    - [安装deepin.com应用容器](#安装deepincom应用容器)
- [桌面环境](#桌面环境)
  - [Cinnamon](#cinnamon)
  - [Mate](#mate)
  - [GNOME](#gnome)
  - [KDE](#kde)
- [维护工具](#维护工具)
  - [GNOME Tweak Tool](#gnome-tweak-tool)
  - [Unity Tweak Tool](#unity-tweak-tool)
    - [菜单栏消失问题](#菜单栏消失问题)
  - [Stacer](#stacer)
  - [BleachBit](#bleachbit)
  - [indicator-sysmonitor](#indicator-sysmonitor)
- [终端工具](#终端工具)
  - [tilix](#tilix)
    - [常用选项](#常用选项)
    - [设置为默认终端](#设置为默认终端)
  - [GNOME](#gnome-1)
  - [Terminator](#terminator)
  - [Guake](#guake)
- [办公软件](#办公软件)
  - [WPS Office](#wps-office)
  - [indicator-stickynotes](#indicator-stickynotes)
- [屏幕截图工具](#屏幕截图工具)
  - [flameshot](#flameshot)
- [录屏工具](#录屏工具)
  - [SimpleScreenRecorder](#simplescreenrecorder)
- [虚拟化工具](#虚拟化工具)
  - [VirtualBox](#virtualbox)
    - [虚拟机无法识别宿主机Usb设备](#虚拟机无法识别宿主机usb设备)
    - [Ubuntu 18.x 安装问题](#ubuntu-18x-安装问题)
- [浏览器](#浏览器)
  - [Chrome](#chrome)
  - [Firefox](#firefox)
  - [Vivaldi](#vivaldi)
- [参考资料](#参考资料)

<!-- /TOC -->


# 备份工具

## Rsync

>Rsync是一个开源的,节约带宽的工具,它用于执行快速的增量文件传输.

# 命令行编辑器

## Vim

# IDE


# 即时通信工具

## deepin wine

### 安装deepin-wine环境

在[这个](https://github.com/wszqkzqk/deepin-wine-ubuntu)页面下载zip包(或用git方式克隆)

解压到本地文件夹,在文件夹中打开终端,输入sudo sh ./install.sh一键安装

### 安装deepin.com应用容器

在[这里](http://mirrors.aliyun.com/deepin/pool/non-free/d/)中下载想要的容器,点击deb安装即可.

以下为推荐容器:

[QQ](http://mirrors.aliyun.com/deepin/pool/non-free/d/deepin.com.qq.im/)<br>
[TIM](http://mirrors.aliyun.com/deepin/pool/non-free/d/deepin.com.qq.office/)<br>
[QQ轻聊版](http://mirrors.aliyun.com/deepin/pool/non-free/d/deepin.com.qq.im.light/)<br>
[微信](http://mirrors.aliyun.com/deepin/pool/non-free/d/deepin.com.wechat/)<br>
[Foxmail](http://mirrors.aliyun.com/deepin/pool/non-free/d/deepin.com.foxmail/)<br>
[百度网盘](http://mirrors.aliyun.com/deepin/pool/non-free/d/deepin.com.baidu.pan/)<br>
[360压缩](http://mirrors.aliyun.com/deepin/pool/non-free/d/deepin.cn.360.yasuo/)


# 桌面环境

## Cinnamon

> Cinnamon是GNOME 3的自由开源衍生产品,它遵循传统的桌面比拟desktop metaphor约定.

## Mate

> Mate桌面环境是GNOME 2的衍生和延续,目的是在Linux上通过使用传统的桌面比拟提供有一个吸引力的UI.

## GNOME

> GNOME是由一些免费和开源应用程序组成的桌面环境,它可以运行在任何Linux发行版和大多数BSD衍生版本上.

## KDE

> KDE由KDE社区开发,它为用户提供图形解决方案以控制操作系统并执行不同的计算任务.

# 维护工具

## GNOME Tweak Tool

> GNOME Tweak Tool是用于自定义和调整GNOME 3和GNOME Shell设置的流行工具.

```bash
sudo apt-get install gnome-tweak-tool
gnome-tweaks
```

## Unity Tweak Tool

> Unity Tweak Tool是一个图形化管理工具,我主要用来修改工作区数量.

### 菜单栏消失问题

```bash
# 重设compiz设置
dconf reset -f /org/compiz/
# 重启Unity
setsid unity
```

## Stacer
> Stacer是一款用于监控和优化Linux系统的免费开源应用程序

## BleachBit

> BleachBit是一个免费的磁盘空间清理器,它也可用作隐私管理器和系统优化器.

## indicator-sysmonitor

标题栏实时显示上下行网速、CPU及内存使用率

```
sudo add-apt-repository ppa:fossfreedom/indicator-sysmonitor
sudo apt-get update
sudo apt-get install indicator-sysmonitor
indicator-sysmonitor
```

# 终端工具

## tilix

目前在用终端

```bash
sudo apt install tilix
```

### 常用选项

选项|作用
-|-
-w,-working directory=|目录设置终端的工作目录
-p,-profile=profile_name|设置起始配置文件
-t,--title=|标题设置新终端的标题
-s,-session=session_name|打开指定的会话
-a,-action=action_name|向当前tilix实例发送操作
-e,-command=|命令将参数作为命令执行

### 设置为默认终端

在系统自带终端的设置中:

![](../assets/images/tilix设置为默认终端.png)


## GNOME

> GNOME终端是GNOME的默认终端模拟器.

## Terminator

> Terminator是一个功能丰富的终端程序,它基于GNOME终端,并且专注于整理终端功能.

## Guake

> Guake是GNOME桌面环境下一个轻量级的可下拉式终端.

# 办公软件

## WPS Office

> WPS Office是一款漂亮的办公套件,它有一个很具现代感的UI.

## indicator-stickynotes

桌面便签工具

```bash
sudo add-apt-repository ppa:umang/indicator-stickynotes
sudo apt-get update
sudo apt-get install indicator-stickynotes
```

# 屏幕截图工具

## flameshot

```bash
sudo apt-get install flameshot
```
设置>设备>键盘,设置一个自定义快捷键(拉到最下面)命令填写:flameshot gui

# 录屏工具

## SimpleScreenRecorder

> SimpleScreenRecorder面世时已经是录屏工具中的佼佼者,现在已成为Linux各个发行版中最有效、最易用的录屏工具之一.

# 虚拟化工具

## VirtualBox

> VirtualBox是一个用于操作系统虚拟化的开源应用程序,在服务器,台式机和嵌入式系统上都可以运行.

### 虚拟机无法识别宿主机Usb设备

首先将当前用户添加到vboxusers用户组(如果不添加则无法在虚拟机中使用Usb):

```
sudo usermod -a -G vboxusers 当前用户名
```

### Ubuntu 18.x 安装问题

```
...
Errors were encountered while processing:
 virtualbox-x.x
```

```
sudo apt-get instal libsdl1.2debian libcurl4
# 如果报错执行下面代码然后重新执行
sudo apt --fix-broken install
```

# 浏览器

## Chrome
> Google Chrome无疑是最受欢迎的浏览器.Chrome 以其速度、简洁、安全、美观而受人喜爱,它遵循了Google的界面设计风格,是web开发人员不可缺少的浏览器,同时它也是免费开源的.

## Firefox

> Firefox Quantum是一款漂亮、快速、完善并且可自定义的浏览器.它也是自由开源的,包含有开发人员所需要的工具,对于初学者也没有任何使用门槛.

## Vivaldi

> Vivaldi是一个基于Chrome的自由开源项目,旨在通过添加扩展来使Chrome的功能更加完善.色彩丰富的界面,性能良好、灵活性强是它的几大特点.

# 参考资料

> [Linux桌面4种可以分屏的终端模拟器](https://www.lulinux.com/archives/4708)

> [2019年wine QQ最完美解决方案](https://www.lulinux.com/archives/1319)

> [Ubuntu18.04 截图工具推荐](https://blog.csdn.net/xuqiang918/article/details/81193034)


> [Ubuntu 16.04中安装 Unity Tweak Tool](https://www.linuxidc.com/Linux/2016-05/130951.htm)

> [ubuntu16.04 中软件的菜单栏消失不见怎么办](https://blog.csdn.net/skant16/article/details/69808490)

> [Ubuntu 16.04 标题栏实时显示上下行网速、CPU及内存使用率--indicator-sysmonitor](http://www.cnblogs.com/Nice-Boy/p/5922258.html)

> [简书 | Ubuntu cron 定时执行任务](https://www.jianshu.com/p/d6d8d9f7f60c)
