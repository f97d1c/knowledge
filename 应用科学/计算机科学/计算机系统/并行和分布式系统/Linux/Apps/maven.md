
<!-- TOC -->

- [安装](#安装)
  - [下载](#下载)
  - [解压](#解压)
  - [添加环境变量](#添加环境变量)
  - [验证Maven配置](#验证maven配置)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装

## 下载

[官网](https://maven.apache.org/download.cgi)下载相应版本*.bin.tar.gz

## 解压

```bash
tar zxvf apache-maven-*.bin.tar.gz -C ~/space/software
```

## 添加环境变量

```bash
echo 'Maven写入环境变量';
tee -a /home/$(whoami)/.bashrc <<-'EOF'
  export MARVEN_HOME=/home/$(whoami)/space/software/apache-maven-3.6.2
  export PATH=$PATH:$MARVEN_HOME/bin
EOF

source ~/.bashrc

echo '验证环境变量'
echo "MARVEN_HOME: $MARVEN_HOME"
echo "PATH: $PATH"

```

## 验证Maven配置

```bash

```

# 参考资料

> [CSDN | Ubuntu下Maven安装和使用](https://blog.csdn.net/ac_dao_di/article/details/54233520)

