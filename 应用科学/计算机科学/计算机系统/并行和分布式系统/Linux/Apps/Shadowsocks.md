> 广泛用于突破防火长城(GFW),以浏览被封锁、遮蔽或干扰的内容.

<!-- TOC -->

- [说明](#说明)
  - [线路](#线路)
    - [CN2](#cn2)
    - [CN2 GIA和CN2 GT区分](#cn2-gia和cn2-gt区分)
    - [线路速度](#线路速度)
  - [vps](#vps)
- [搭建](#搭建)
  - [购买VPS](#购买vps)
    - [线路选择](#线路选择)
    - [关于IP被封问题](#关于ip被封问题)
    - [搬瓦工](#搬瓦工)
      - [常见的购买套餐](#常见的购买套餐)
      - [购买教程](#购买教程)
    - [Vultr](#vultr)
    - [Hostwinds](#hostwinds)
      - [常见套餐](#常见套餐)
      - [IP更换](#ip更换)
    - [对比](#对比)
    - [Just My Socks](#just-my-socks)
  - [Shadowsocks](#shadowsocks)
    - [安装pip](#安装pip)
    - [安装Shadowsocks](#安装shadowsocks)
    - [创建配置文件](#创建配置文件)
    - [启动](#启动)
    - [测试](#测试)
    - [配置Systemd管理Shadowsocks](#配置systemd管理shadowsocks)
    - [启动Shadowsocks](#启动shadowsocks)
    - [设置开机启动Shadowsocks](#设置开机启动shadowsocks)
    - [订阅链接](#订阅链接)
    - [优化](#优化)
      - [开启BBR加速](#开启bbr加速)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## 线路

### CN2

> CN2是中国电信下一代承载网<br>
CN2是一个多业务的承载网络,<br>
它能够支持数据、语音、视频多种业务融合的应用,<br>
是中国电信骨干网络和其商业客户之间的重要纽带,<br>
CN2线路全名叫Global Transit,<br>
缩写就是CN2 GT了.<br>
CN2线路一般在国内还是走202.97的电信163骨干网,在出国的时候才走59.43开头的CN2线路.<br>
一般称呼CN2 GT线路为CN2线路.


### CN2 GIA和CN2 GT区分

> 只有去程走59.43线路,那就是CN2 GT线路;往返都走59.43线路,就是CN2 GIA.

### 线路速度

香港线路 > CN2 GIA 线路 > CN2 线路 > 普通线路

## vps

> 虚拟专用服务器(VPS)是一个虚拟机作为由服务销售的互联网托管服务.<br>
VPS运行其自己的操作系统(OS)副本,<br>
客户可能对该操作系统实例具有超级用户级别的访问权限,<br>
因此他们可以安装在该OS上运行的几乎所有软件.
<br>在许多方面,它们在功能上等同于专用物理服务器,并且由软件定义,<br>
因此可以更轻松地创建和配置.<br>
它们的价格比同等的物理服务器低得多.<br>
但是,由于它们与其他VPS共享底层物理硬件,因此性能可能会降低,<br>
具体取决于任何其他正在执行的虚拟机的工作量.

# 搭建

## 购买VPS

### 线路选择

对于国外VPS商家来说,<br>
有的明确在产品名上标明是CN2 GIA线路(比如搬瓦工CN2 GIA套餐),就会一直走CN2 GIA线路.<br>
有一些是没有明确标明是CN2 GIA线路,只是商家开始的时候吸引中国客户(比如anyNode),<br>
或者沾机房的光突然走CN2 GIA线路(比如cloudcone),<br>
这种的什么时候不走CN2 GIA线路不好说.<br>
CN2线路也是如此,比如两年前洛杉矶QN机房默认走CN2线路,<br>
今年就开始不走了.建议在入手的时候要辨别清楚.

### 关于IP被封问题

有些厂商支持自主更换被封IP,有些则需要付费.

据了解 搬瓦工有免费更换IP有次数限制,超出则需付费更换.

### 搬瓦工

> 搬瓦工是一个对中国用户极度友好的 VPS 商家,有香港,CN2 GIA 优化线路,并且支持支付宝付款,当然也是支持退款的.

#### 常见的购买套餐

线路|CPU|内存|硬盘|带宽|流量|价格
-|-|-|-|-|-|-|-|
普通|2 核|1024 MB|20 GB|1 G|1 TB / 月|$49.99 / 年
CN2|1 核|1024 MB|20 GB|1 G|1000GB / 月|$49.99 / 年
CN2 GIA|1 核|512 MB|10 GB|1 G|300GB / 月|$39.99 / 年
CN2 GIA|1 核|512 MB|10 GB|1 G|500GB / 月|$49.99 / 年
CN2 GIA|2 核|1024 MB|20 GB|1 G|1000GB / 月|$25.99 / 季
CN2 GIA|2 核|1 GB|20 GB|2.5 G|1000GB / 月|$65.99 / 半年
香港|2 核|2048 MB|40 GB|1 G|500GB / 月|$89.99 / 月

#### 购买教程

参考 [Github | Shadowsocks搭建详细图文教程](https://github.com/233boy/ss/wiki/Shadowsocks%E6%90%AD%E5%BB%BA%E8%AF%A6%E7%BB%86%E5%9B%BE%E6%96%87%E6%95%99%E7%A8%8B#%E6%B7%BB%E5%8A%A0%E5%88%B0%E8%B4%AD%E7%89%A9%E8%BD%A6)

### Vultr

> vultr实际上是折算成小时来计费的,<br>
比如服务器是5美元1个月,那么每小时收费为5/30/24=0.0069美元 会自动从账号中扣费,只要保证账号有钱即可.<br>
如果部署的服务器实测后速度不理想,你可以把它删掉,重新换个地区的服务器来部署,方便且实用.<br>
因为新的服务器就是新的ip,所以当ip被墙时这个方法很有用.<br>
当ip被墙时,为了保证新开的服务器ip和原先的ip不一样,先开新服务器,开好后再删除旧服务器即可.<br>
在账号的Billing选项里可以看到账户余额.

### Hostwinds

> Hostwinds是非常有名的美国主机商,这款美国虚拟主机关键在于稳定,带宽高,访问速度快,价格便宜,同时还支持支付宝付款.<br>
这款美国虚拟主机拥有1000M带宽,秒杀其他主机商的几十M.

**购买了Hostwinds后要装BBR才能发挥最大的速度.**


主要特点如下:

0. 支持WordPress、Joomla、Drupal、Discuz!等常用软件,并支持一键安装
0. 下单后即时开通
0. 支持免费网站转移
0. 最近控制面板
0. 无限的子区域
0. 无限的电子邮件帐户
0. 无限的MYSQL数据库
0. 无限的FTP帐户
0. 免费专用IP地址(这点是其他虚拟主机不能够相比,其他虚拟主机都是共享IP,独立IP对SEO效果更好)
0. 免费SSL证书
0. 99.999％UPTIME保证
0. 支持7x24小时客服服务

Hostwinds主要数据中心位于美国和荷兰,拥有西雅图、达拉斯和阿姆斯特丹三个数据中心,如果要在国内访问,选择西雅图数据中心更快.

#### 常见套餐

套餐选择|基础版|高级版|终极版
-|-|-|-|
带宽|无限|无限|无限
存储空间|无限|无限|无限
主域名个数|1|4|无限
价格|$3.29/月|$4.23/月|$5.17/月

如果购买时间12月以上,还免费赠送域名.

上面主域名个数,不代表建站的个数,Hostwinds可以建无数个站点,只是主域名个数限制了,可以使用子域名建无数个站点.

#### IP更换

参考 [VPS234 | 美国VPS Hostwinds IP被屏蔽Ping不通解决新方法 - 免费换IP](https://www.vps234.com/hostwinds-ip-blocked-fix-isp/).

### 对比

下面根据价格相当($5/月 左右),对主要厂商进行对比:

厂商|搬瓦工|Hostwinds|Vultr|
-|-|-|-|
带宽|1G|1G|
线路|CN2 GIA||
流量|500GB|1TB|1TB
内存|521MB|1GB|1GB
CPU|1|1|1
存储|10 GB|30GB
更换IP|自主操作/免费/有次数限制|自主操作/免费/无次数限制
支付宝|支持|支持|支持
价格|$4.16/月($49.99/年)|$4.49/月|$5/月
购买链接||[购买](https://clients.hostwinds.com/buycloud/274)|




### Just My Socks

> 搬瓦工[官方](https://justmysocks1.net/members/cart.php?gid=1)出品的 Shadowsocks 代理服务,同样是 CN2 GIA 线路.

[搬瓦工 JMS](https://bwgjms.com) :非官方帮助平台.

## Shadowsocks

可以尝试下面这个一键部署脚本:

```bash
bash <(curl -s -L https://git.io/ss.sh)
```

### 安装pip

*** Python版本选择2.x 3.x版本启动容易遇到错误. ***
```
sudo apt install -y python2.7 python-pip
```

### 安装Shadowsocks

```bash
# pip install shadowsocks 版本为2.8 存在一些问题
pip install https://github.com/shadowsocks/shadowsocks/archive/master.zip

# 查看Shadowsocks版本
sudo ssserver --version
```

### 创建配置文件

```bash
# 注意修改密码“password”
config_file_path='/etc/shadowsocks/config.json'
[ -e $config_file_path ] || mkdir -p ${config_file_path%/*};touch $config_file_path

sudo tee $config_file_path <<-'EOF'
{
  "server":"::",
  "server_port":8388,
  "local_address": "127.0.0.1",
  "local_port":1080,
  "password":"mypassword",
  "timeout":300,
  "method":"aes-256-cfb",
  "fast_open": false
}
EOF
```

```json
{
  "server":"主机名或服务器IP(IPv4 / IPv6)",
  "server_port":"服务器端口号",
  "local_port":"本地端口号",
  "password":"用于加密传输的密码",
  "timeout":"连接超时(以秒为单位)",
  "method":"加密方法",
}
```

### 启动

```bash
ssserver -c $config_file_path
```

### 测试

0. 地址填写服务器IPv4地址或IPv6地址
0. 端口号为8388
0. 加密方法为aes-256-cfb
0. 密码为设置的密码.然后设置客户端使用全局模式
0. 浏览器登录Google试试应该能直接打开了.

### 配置Systemd管理Shadowsocks

```bash
sudo tee -a /etc/systemd/system/shadowsocks-server.service <<-'EOF'
[Unit]
Description=Shadowsocks Server
After=network.target

[Service]
ExecStart=/usr/local/bin/ssserver -c /etc/shadowsocks/config.json
Restart=on-abort

[Install]
WantedBy=multi-user.target
EOF
```

### 启动Shadowsocks

```
sudo systemctl start shadowsocks-server
```

### 设置开机启动Shadowsocks

```
sudo systemctl enable shadowsocks-server
```

### 订阅链接

> 订阅链接指的是:<br>
访问该链接,其返回结果为: 对基本格式url进行两次BASE64编码的内容.

基本链接格式:

```
ss://method:password@hostname:port
```

*URI不遵循RFC3986。这意味着此处的密码应为纯文本，而不是百分比编码.*

例如:

有一个192.168.100.1:8888使用bf-cfb加密方法和password 的服务器test/!@#:

普通URI应为:

```
ss://bf-cfb:test/!@#:@192.168.100.1:8888
```

生成BASE64编码的URI：

```js
base_66_url = "ss://" + btoa("bf-cfb:test/!@#:@192.168.100.1:8888")
// => ss://YmYtY2ZiOnRlc3QvIUAjOkAxOTIuMTY4LjEwMC4xOjg4ODg
```

将编码后的url再次进行BASE64编码:

```js
btoa(base_66_url)
// => c3M6Ly9ZbVl0WTJaaU9uUmxjM1F2SVVBak9rQXhPVEl1TVRZNExqRXdNQzR4T2pnNE9EZz0
```

本次生成结果即为标准订阅链接内容.


### 优化

#### 开启BBR加速

参考 Linux.md 中相关内容.

需要注意的是如果采用的是容器部署,由于Docker完全依赖宿主机内核, 只需在宿主机中开启即可.

# 参考资料

> [Github | Shadowsocks搭建详细图文教程](https://github.com/233boy/ss/wiki/Shadowsocks搭建详细图文教程)

> [知乎 | 以搬瓦工为例,浅谈CN2 GIA和CN2 GT线路的区别](https://zhuanlan.zhihu.com/p/68381011)

> [搬瓦工 JMS | Just My Socks 详细图文购买教程](https://bwgjms.com/post/how-to-buy-justmysocks/)

> [不忘初心,方得始终 | Ubuntu 16.04下Shadowsocks服务器端安装及优化](https://www.polarxiong.com/archives/Ubuntu-16-04下Shadowsocks服务器端安装及优化.html)

> [主机评测网 | 2019 搬瓦工IP被墙后该如何快速解封,免费或付费更换IP！](https://www.hostpingce.com/banwagong-ip-change/)

> [Github | 自建ss服务器教程](https://github.com/Alvin9999/new-pac/wiki/自建ss服务器教程)

> [知乎 | 2019后搬瓦工、Vultr、Linode、DigitalOcean还行吗？应该怎样选择VPS建站等](https://zhuanlan.zhihu.com/p/70025050)

> [VPS234 | 便宜美国虚拟主机推荐Hostwinds,无限流量](https://www.vps234.com/cheap-usa-web-hosting-recommend-hostwinds/)

> [CSDN | ubuntu18.04安装python2.7和python3.6 pip 和 pip3 不同版本共存](https://blog.csdn.net/gymaisyl/article/details/86563916)

> [shadowsocks | 指南](https://shadowsocks.org/en/download/servers.html)

> [vjsun | 手动自己创建自己的V2 SSR订阅方法](https://www.vjsun.com/239.html)