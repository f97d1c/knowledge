> Snap是Ubuntu母公司Canonical于2016年4月发布Ubuntu16.04时候引入的一种安全的、易于管理的、沙盒化的软件包格式,与传统的dpkg/apt有着很大的区别.

<!-- TOC -->

- [安装](#安装)
- [常用命令](#常用命令)
- [常用软件](#常用软件)
- [Docker和Snap的主要区别](#docker和snap的主要区别)
- [参考资料](#参考资料)

<!-- /TOC -->
# 安装

```
sudo apt install -y snapd snapcraft
```

# 常用命令

命令|作用
-|-
sudo snap list|列出已经安装的snap包
sudo snap find \<text to search>|搜索要安装的snap包
sudo snap install \<snap name>|安装一个snap包
sudo snap refresh \<snap name>|更新一个snap包,如果你后面不加包的名字的话那就是更新所有的snap包
sudo snap revert \<snap name>|把一个包还原到以前安装的版本
sudo snap remove \<snap name>|删除一个snap包

# 常用软件

名称|作用
-|-

# Docker和Snap的主要区别

Docker的特点:

0. 不同类型的容器看起来相同,但用于不同目的.
0. 容器在内核级别并不存在.
0. 我们可以独立地创建关于容器看到的用户,网络,磁盘和进程的虚拟.
0. 不同类型的容器实际上是关于创建的不同类型的虚拟.

Snaps的特点:

0. 不可变,但仍然是基础系统的一部分.
在网络方面集成,因此共享系统IP地址,与Docker不同,每个容器都有自己的IP地址.
0. 快照不会污染系统的其余部分.它在自己的盒子里.但它仍然可以看到(只读)系统的其余部分,这使它能够与系统进行通信和集成.

Docker不能运行桌面应用程序,但这是快照可以做的.<br>
第三方可以使用快照发送桌面应用程序,用户可以轻松地安装和更新它们.<br>
Docker容器不能(轻松地)在屏幕上以图形方式与用户交互,从用户的主目录加载文档,或通过用户的网络摄像头提供视频会议,快照可以(一旦获得许可).

# 参考资料

> [博客园 | Ubuntu下安装Snap](https://www.cnblogs.com/DragonStart/p/10369043.html)

> [Linux公社 | Ubuntu中snap包的安装,删除,更新使用入门教程](https://www.linuxidc.com/Linux/2018-05/152385.htm)

> [智库101 | Docker和Snap之间的主要区别是什么？](http://www.kbase101.com/question/10814.html)

> [简书 | Samba 安装与配置，以及配置说明参数详解](https://www.jianshu.com/p/f7fb4ad09c11)