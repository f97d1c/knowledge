> ZeroTier is a smart Ethernet switch for planet Earth.

<!-- TOC -->

- [说明](#说明)
  - [PLANET](#planet)
  - [MOON](#moon)
  - [LEAF](#leaf)
  - [常用命令](#常用命令)
- [搭建](#搭建)
  - [注册](#注册)
  - [创建网络](#创建网络)
  - [加入网络](#加入网络)
    - [Mac](#mac)
    - [Ubuntu](#ubuntu)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## PLANET 

> 行星服务器，Zerotier 根服务器

## MOON 

> 卫星服务器，用户自建的私有根服务器，起到代理加速的作用

## LEAF  

> 网络客户端，就是每台连接到网络节点。

## 常用命令

```bash
# 获取地址和服务状态
zerotier-cli status

# 加入、离开、列出网络
zerotier-cli join Network ID
zerotier-cli leave Network ID
zerotier-cli listnetworks
```

# 搭建

## 注册

[注册地址](https://my.zerotier.com/)

## 创建网络

点击导航栏的Networks进行创建.

节点加入网络后需在详情页勾选进行授权.

## 加入网络

### Mac

下载客户端后点击 Join Network 后输入 ID.

### Ubuntu

```bash
curl -s 'https://raw.githubusercontent.com/zerotier/ZeroTierOne/master/doc/contact%40zerotier.com.gpg' | gpg --import && \
if z=$(curl -s 'https://install.zerotier.com/' | gpg); then echo "$z" | sudo bash; fi

sudo zerotier-cli status
# => zerotier-cli: missing authentication token and authtoken.secret not found (or readable) in /var/lib/zerotier-one
# 执行: chmod 660 /var/lib/zerotier-one/{authtoken.secret,identity.secret}
```

# 参考资料

> [zhih | ZeroTier内网穿透教程](https://zhih.me/zerotier-getting-started/)

> [Github | zerotier-cli cannot be used as regular user since 1.4.0 update](https://github.com/zerotier/ZeroTierOne/issues/996)