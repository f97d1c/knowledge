<!-- TOC -->

- [说明](#说明)
  - [常用格式](#常用格式)
    - [复制远程目录至本地](#复制远程目录至本地)
  - [同步至不同名目录](#同步至不同名目录)
- [参数](#参数)
  - [归档模式(-a)](#归档模式-a)
  - [递归模式(-r)](#递归模式-r)
  - [显示详细过程(-v)](#显示详细过程-v)
  - [显示同步过程(-P)](#显示同步过程-p)
  - [传输时压缩(-z)](#传输时压缩-z)
  - [可读格式数字(-h)](#可读格式数字-h)
  - [删除无关文件(--delete)](#删除无关文件--delete)
  - [复制整个文件,不使用增量xfer算法(-W)](#复制整个文件不使用增量xfer算法-w)
  - [同步后删除原始文件(--remove-source-files)](#同步后删除原始文件--remove-source-files)
  - [设备间同步无权限(--rsync-path="sudo rsync")](#设备间同步无权限--rsync-pathsudo-rsync)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## 常用格式

### 复制远程目录至本地

```sh
rsync -arvzhP --delete user@host:源目录/ 目标目录
```

## 同步至不同名目录

```sh
rsync -a 源目录/ 目标目录
```

源目录的尾部添加斜杠避免在目标创建额外的目录级别.<br>
可以将源中的尾随/视为 *复制此目录的内容* 而不是 *按名称复制目录*.

# 参数

## 归档模式(-a)

以递归属性传输文件,并保持所有属性, **如果日后需要同步差异内容,需开启该选项**.

## 递归模式(-r)

对子目录一递归模式处理,主要针对目录来说,如果传输的是目录必须加上.

## 显示详细过程(-v)

打印一下信息出来,比如速率,文件数量.

## 显示同步过程(-P)

在同步过程中看到同步过程的状态,比如统计压迫同步的文件数量、传输速度.

## 传输时压缩(-z)

如果源目录下为众多小文件不建议压缩传输.

## 可读格式数字(-h)


## 删除无关文件(--delete)


## 复制整个文件,不使用增量xfer算法(-W)

此选项禁用 rsync 的 delta-transfer(增量复制) 算法,这会导致所有传输的文件被完整发送.<br>
如果在源机器和目标机器之间的带宽高于到磁盘的带宽时使用此选项,传输可能会更快(尤其是当 *磁盘* 实际上是网络文件系统时).<br>
当源和目标都被指定为本地路径时,这是默认设置,但前提是没有批处理写入选项生效.<br>

## 同步后删除原始文件(--remove-source-files)


## 设备间同步无权限(--rsync-path="sudo rsync")

```
rsync: change_dir * failed: Permission denied (13)
```

(原文)[https://askubuntu.com/questions/719439/using-rsync-with-sudo-on-the-destination-machine]中表示需要为rsync路径添加无需密码配置,<br>
目前环境中执行用户

# 参考资料

> [stackexchange | How to rsync a directory to a new directory with different name?](https://unix.stackexchange.com/questions/178078/how-to-rsync-a-directory-to-a-new-directory-with-different-name)

> [51cto | rsync常用参数](https://blog.51cto.com/u_13673885/2162517)

>[wanpeng | rsync常用参数及作用](https://www.wanpeng.life/1812.html)

> [samba | rsync](https://download.samba.org/pub/rsync/rsync.1)
