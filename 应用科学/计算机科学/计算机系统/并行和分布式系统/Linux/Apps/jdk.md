
<!-- TOC -->

- [安装](#安装)
  - [openjdk](#openjdk)
- [确定JDK安装位置](#确定jdk安装位置)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装

## openjdk

```bash
sudo apt-get update -y
sudo apt-get install openjdk-8-jdk -y
echo "jdk相关信息:";java -version
```

# 确定JDK安装位置

```bash
which java
#=> /usr/bin/java
ls -lrt /usr/bin/java
#=> lrwxrwxrwx 1 root root 22 9月  15 16:31 /usr/bin/java -> /etc/alternatives/java
ls -lrt /etc/alternatives/java
#=> lrwxrwxrwx 1 root root 46 9月  15 16:31 /etc/alternatives/java -> /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
ls -lrt /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
#=> -rwxr-xr-x 1 root root 6280 7月  19 02:52 /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
```

# 参考资料

> [Linux 公社 | Ubuntu 16.04安装Java JDK](https://www.linuxidc.com/Linux/2017-11/148695.htm)