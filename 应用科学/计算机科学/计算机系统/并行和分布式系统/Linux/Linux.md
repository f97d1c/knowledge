<!-- TOC -->

- [说明](#说明)
  - [互联网控制消息协议(ICMP, Internet Control Message Protocol)](#互联网控制消息协议icmp-internet-control-message-protocol)
  - [存活时间(TTL, Time To Live)](#存活时间ttl-time-to-live)
  - [跳数(Hop)](#跳数hop)
  - [互联网服务提供商(ISP, Internet Service Provider)](#互联网服务提供商isp-internet-service-provider)
  - [网络地址转换(NAT, Network Address Translation)](#网络地址转换nat-network-address-translation)
- [Linux系统启动过程](#linux系统启动过程)
  - [Linux系统启动过程简介](#linux系统启动过程简介)
  - [BIOS加电自检](#bios加电自检)
  - [引导加载程序](#引导加载程序)
    - [引导加载程序的启动](#引导加载程序的启动)
    - [GRUB配置](#grub配置)
  - [init进程](#init进程)
    - [init进程简介](#init进程简介)
    - [init进程的引导过程](#init进程的引导过程)
    - [配置自动运行服务](#配置自动运行服务)
  - [重启和关闭系统](#重启和关闭系统)
    - [关闭或重启系统(shutdown)](#关闭或重启系统shutdown)
    - [关闭系统(halt)](#关闭系统halt)
    - [重启系统(reboot)](#重启系统reboot)
    - [改变运行级别(init)](#改变运行级别init)
    - [通过图形界面关闭系统](#通过图形界面关闭系统)
  - [系统启动时常见的问题处理](#系统启动时常见的问题处理)
    - [进入Linux救援模式](#进入linux救援模式)
    - [GRUB被Windows覆盖](#grub被windows覆盖)
    - [重新分区后GRUB引导失败](#重新分区后grub引导失败)
- [文件目录管理](#文件目录管理)
  - [Linux文件系统的架构](#linux文件系统的架构)
    - [Linux文件系统简介](#linux文件系统简介)
    - [Linux支持的文件系统类型](#linux支持的文件系统类型)
    - [Linux的默认安装目录](#linux的默认安装目录)
  - [文件系统管理](#文件系统管理)
    - [打印树结构目录(tree)](#打印树结构目录tree)
    - [文件类型](#文件类型)
      - [查看文件类型](#查看文件类型)
      - [硬链接和软链接](#硬链接和软链接)
        - [硬链接](#硬链接)
        - [软链接](#软链接)
    - [建立目录(mkdir)](#建立目录mkdir)
    - [建立一个空文件(touch)](#建立一个空文件touch)
    - [显示当前目录(pwd)](#显示当前目录pwd)
    - [改变目录(cd)](#改变目录cd)
    - [列出目录内容(ls)](#列出目录内容ls)
    - [查看目录空间大小](#查看目录空间大小)
      - [可读参数(human -h)](#可读参数human--h)
    - [列出目录内容(dir和vdir)](#列出目录内容dir和vdir)
    - [移动和重命名(mv)](#移动和重命名mv)
    - [复制文件/目录(cp)](#复制文件目录cp)
    - [删除目录和文件(rmdir和rm)](#删除目录和文件rmdir和rm)
    - [查看已挂载文件系统](#查看已挂载文件系统)
    - [使用fstab文件自动挂载文件系统](#使用fstab文件自动挂载文件系统)
    - [查看文件/目录属性](#查看文件目录属性)
    - [文件名通配符](#文件名通配符)
    - [查找特定程序(whereis)](#查找特定程序whereis)
    - [查找文件(find)](#查找文件find)
    - [快速定位文件(locate)](#快速定位文件locate)
    - [转换目录和文件名大小写](#转换目录和文件名大小写)
    - [alias 创建别名](#alias-创建别名)
      - [传递自定义参数](#传递自定义参数)
    - [文件/目录权限管理](#文件目录权限管理)
      - [Linux文件/目录权限简介](#linux文件目录权限简介)
      - [权限设置针对的用户](#权限设置针对的用户)
      - [需要设置哪些权限](#需要设置哪些权限)
      - [查看文件/目录的属性](#查看文件目录的属性)
      - [改变文件所有权](#改变文件所有权)
        - [更改所属用户(chown)](#更改所属用户chown)
        - [chgrp](#chgrp)
      - [更改文件/目录的权限(chmod)](#更改文件目录的权限chmod)
      - [设置文件/目录的默认权限](#设置文件目录的默认权限)
      - [文件权限的八进制表示](#文件权限的八进制表示)
  - [文件管理](#文件管理)
    - [查看文本文件(cat和more)](#查看文本文件cat和more)
    - [阅读文件的开头和结尾(head和tail)](#阅读文件的开头和结尾head和tail)
    - [更好的文本阅读工具(less)](#更好的文本阅读工具less)
    - [查找文件内容](#查找文件内容)
      - [grep](#grep)
        - [多个关键字的 *与* 和 *或*](#多个关键字的-与-和-或)
      - [sed](#sed)
        - [替换内容中有斜杠](#替换内容中有斜杠)
        - [常用选项](#常用选项)
        - [命令类别](#命令类别)
        - [追加行(a \\)](#追加行a-\\)
        - [通过行号追加](#通过行号追加)
        - [通过正则追加](#通过正则追加)
        - [日志分析](#日志分析)
    - [EOF写入文件](#eof写入文件)
      - [delimited by end-of-file](#delimited-by-end-of-file)
    - [文件切割](#文件切割)
      - [split](#split)
    - [文件删除后仍然占用空间](#文件删除后仍然占用空间)
  - [输入输出重定向和管道](#输入输出重定向和管道)
    - [输出重定向](#输出重定向)
    - [输入重定向](#输入重定向)
    - [管道(|)](#管道)
- [软件包管理](#软件包管理)
  - [软件包管理系统简述](#软件包管理系统简述)
  - [管理*.deb软件包(dpkg)](#管理deb软件包dpkg)
    - [安装软件包](#安装软件包)
      - [解决依赖问题](#解决依赖问题)
    - [查看已安装的软件包](#查看已安装的软件包)
    - [卸载软件包](#卸载软件包)
  - [使用RPM软件包](#使用rpm软件包)
    - [RPM简介](#rpm简介)
    - [RPM命令的使用方法](#rpm命令的使用方法)
    - [安装RPM软件包](#安装rpm软件包)
    - [查看RPM软件包](#查看rpm软件包)
    - [升级软件包](#升级软件包)
    - [删除软件包](#删除软件包)
    - [查看程序是由哪个RPM包安装](#查看程序是由哪个rpm包安装)
  - [高级软件包工具(apt)](#高级软件包工具apt)
    - [apt简介](#apt简介)
    - [更新源](#更新源)
      - [ubuntu-ports和ubuntu区别](#ubuntu-ports和ubuntu区别)
      - [删除ppa](#删除ppa)
        - [删除源](#删除源)
        - [/etc/apt/sources.list.d](#etcaptsourceslistd)
    - [下载和安装软件包](#下载和安装软件包)
    - [更新软件](#更新软件)
    - [查询软件包信息](#查询软件包信息)
    - [删除软件包](#删除软件包-1)
    - [配置apt-get](#配置apt-get)
  - [从源代码编译软件](#从源代码编译软件)
    - [为什么要从源代码编译](#为什么要从源代码编译)
    - [下载和解压软件包](#下载和解压软件包)
      - [正确配置软件](#正确配置软件)
      - [编译源代码](#编译源代码)
      - [安装软件到硬盘](#安装软件到硬盘)
      - [出错了怎么办](#出错了怎么办)
    - [.bin文件安装](#bin文件安装)
- [环境管理](#环境管理)
  - [字符集管理(locale)](#字符集管理locale)
    - [设置语言环境](#设置语言环境)
    - [设置示例](#设置示例)
    - [locale: Cannot set LC_CTYPE to default locale: No such file or directory](#locale-cannot-set-lc_ctype-to-default-locale-no-such-file-or-directory)
    - [locale-gen: command not found](#locale-gen-command-not-found)
  - [查看基本信息(uname)](#查看基本信息uname)
  - [系统时区管理(tzdata)](#系统时区管理tzdata)
  - [环境变量文件](#环境变量文件)
  - [定时任务](#定时任务)
    - [cron](#cron)
      - [安装](#安装)
      - [常用命令](#常用命令)
    - [crontab](#crontab)
      - [常用命令](#常用命令-1)
      - [crontab文件](#crontab文件)
  - [service](#service)
    - [语法](#语法)
    - [选项](#选项)
    - [参数](#参数)
    - [实例](#实例)
      - [network](#network)
- [用户和用户组管理](#用户和用户组管理)
  - [用户管理概述](#用户管理概述)
    - [用户账号](#用户账号)
      - [用户账号文件(passwd,shadow)](#用户账号文件passwdshadow)
    - [用户组](#用户组)
      - [用户组文件(group,gshadow)](#用户组文件groupgshadow)
  - [普通用户管理](#普通用户管理)
    - [添加用户](#添加用户)
    - [更改用户密码](#更改用户密码)
    - [修改用户信息](#修改用户信息)
      - [修改用户名](#修改用户名)
      - [修改用户目录](#修改用户目录)
    - [删除用户](#删除用户)
    - [禁用用户](#禁用用户)
    - [配置用户Shell环境](#配置用户shell环境)
    - [批量添加用户](#批量添加用户)
    - [完整删除用户账号](#完整删除用户账号)
    - [记录用户操作(history)](#记录用户操作history)
    - [忘记root用户密码](#忘记root用户密码)
    - [误删用户账号](#误删用户账号)
    - [查看用户信息(id)](#查看用户信息id)
    - [用户间切换(su)](#用户间切换su)
  - [用户组管理](#用户组管理)
    - [创建用户组](#创建用户组)
    - [添加用户到用户组](#添加用户到用户组)
    - [修改用户组](#修改用户组)
    - [删除用户组](#删除用户组)
  - [受限的特权(sudo)](#受限的特权sudo)
  - [/etc/passwd文件](#etcpasswd文件)
    - [加密的口令](#加密的口令)
    - [UID号](#uid号)
    - [GID号](#gid号)
  - [/etc/shadow文件](#etcshadow文件)
  - [/etc/group文件](#etcgroup文件)
- [进程管理](#进程管理)
  - [什么是进程](#什么是进程)
  - [进程的属性](#进程的属性)
    - [进程的ID号(PID)](#进程的id号pid)
    - [父进程的PID(PPID)](#父进程的pidppid)
    - [真实和有效的用户ID(UID, EUID)](#真实和有效的用户iduid-euid)
    - [真实和有效的组ID(GID, EGID)](#真实和有效的组idgid-egid)
    - [谦让度和优先级](#谦让度和优先级)
  - [监视进程(ps)](#监视进程ps)
  - [即时跟踪进程信息(top)](#即时跟踪进程信息top)
  - [查看占用文件的进程(lsof)](#查看占用文件的进程lsof)
  - [向进程发送信号(kill)](#向进程发送信号kill)
  - [调整进程的谦让度(nice, renice)](#调整进程的谦让度nice-renice)
  - [/PROC文件系统](#proc文件系统)
  - [进程休眠(sleep)](#进程休眠sleep)
- [网络管理](#网络管理)
  - [获取IP地址](#获取ip地址)
    - [公共](#公共)
    - [私有](#私有)
  - [链路测试(mtr)](#链路测试mtr)
    - [安装](#安装-1)
    - [使用](#使用)
    - [可用命令选项](#可用命令选项)
  - [Netplan](#netplan)
    - [备份原配置文件](#备份原配置文件)
    - [确定网络设备名称](#确定网络设备名称)
    - [确定网关地址](#确定网关地址)
    - [确定DNS地址](#确定dns地址)
    - [配置静态IP地址](#配置静态ip地址)
    - [测试配置](#测试配置)
  - [重启网络](#重启网络)
  - [127.0.0.1和0.0.0.0地址](#127001和0000地址)
  - [开启BBR](#开启bbr)
    - [修改/etc/sysctl.conf](#修改etcsysctlconf)
    - [保存生效](#保存生效)
    - [检查BBR是否开启](#检查bbr是否开启)
    - [检查BBR是否启动成功](#检查bbr是否启动成功)
  - [配置翻墙(clash)](#配置翻墙clash)
    - [clash安装](#clash安装)
    - [Country.mmdb安装](#countrymmdb安装)
    - [管理配置文件](#管理配置文件)
    - [Web界面](#web界面)
    - [基础配置文件](#基础配置文件)
    - [启动服务](#启动服务)
    - [终端代理命令](#终端代理命令)
- [系统监控](#系统监控)
  - [top](#top)
  - [ps](#ps)
    - [查看LINUX进程内存占用情况](#查看linux进程内存占用情况)
  - [pingtop](#pingtop)
    - [安装](#安装-2)
    - [使用](#使用-1)
  - [nethogs](#nethogs)
  - [bmon](#bmon)
  - [iftop](#iftop)
    - [常用参数](#常用参数)
      - [端口显示](#端口显示)
      - [排序](#排序)
      - [其他](#其他)
- [磁盘管理](#磁盘管理)
  - [关于硬盘](#关于硬盘)
  - [Linux文件系统](#linux文件系统)
    - [ext3fs和ext4fs文件系统](#ext3fs和ext4fs文件系统)
    - [ReiserFS文件系统](#reiserfs文件系统)
    - [有关swap](#有关swap)
      - [误删Swap分区](#误删swap分区)
  - [挂载文件系统](#挂载文件系统)
    - [查看块设备](#查看块设备)
    - [Linux下设备的表示方法](#linux下设备的表示方法)
    - [挂载文件系统(mount)](#挂载文件系统mount)
      - [非同步模式](#非同步模式)
      - [同步模式](#同步模式)
    - [在启动的时候挂载文件系统(/etc/fstab文件)](#在启动的时候挂载文件系统etcfstab文件)
    - [卸载文件系统(umount)](#卸载文件系统umount)
      - [target is busy](#target-is-busy)
  - [查看磁盘使用情况(df)](#查看磁盘使用情况df)
  - [查看磁盘占用进程(fuser)](#查看磁盘占用进程fuser)
  - [检查和修复文件系统(fsck)](#检查和修复文件系统fsck)
  - [在磁盘上建立文件系统(mkfs)](#在磁盘上建立文件系统mkfs)
  - [使用USB设备](#使用usb设备)
  - [压缩工具](#压缩工具)
    - [gzip&gunzip](#gzipgunzip)
      - [gunzip](#gunzip)
    - [使用zip和unzip进行压缩](#使用zip和unzip进行压缩)
    - [使用bzip2和bunzip2进行压缩](#使用bzip2和bunzip2进行压缩)
    - [使用compress和uncompress进行压缩](#使用compress和uncompress进行压缩)
    - [支持rar格式](#支持rar格式)
  - [存档工具](#存档工具)
    - [文件打包(tar)](#文件打包tar)
      - [tar简介](#tar简介)
      - [选项项](#选项项)
        - [必选选项](#必选选项)
        - [可选选项](#可选选项)
      - [打包文件](#打包文件)
      - [查看归档文件的内容](#查看归档文件的内容)
      - [还原归档文件](#还原归档文件)
      - [往归档文件中追加新文件](#往归档文件中追加新文件)
      - [压缩归档文件](#压缩归档文件)
    - [转移文件(dd)](#转移文件dd)
    - [解压](#解压)
      - [.gz](#gz)
      - [.tar.gz](#targz)
  - [磁盘分区管理(fdisk)](#磁盘分区管理fdisk)
    - [磁盘分区简介](#磁盘分区简介)
      - [查看硬盘信息](#查看硬盘信息)
    - [使用parted进行分区管理](#使用parted进行分区管理)
    - [使用mkfs建立ext3fs文件系统](#使用mkfs建立ext3fs文件系统)
    - [测试分区](#测试分区)
    - [创建并激活交换分区](#创建并激活交换分区)
      - [配置fstab文件](#配置fstab文件)
      - [删除分区后系统无法启动](#删除分区后系统无法启动)
  - [高级硬盘管理(RAID和LVM)](#高级硬盘管理raid和lvm)
    - [独立磁盘冗余阵列(RAID)](#独立磁盘冗余阵列raid)
    - [逻辑卷管理(LVM)](#逻辑卷管理lvm)
      - [LVM简介](#lvm简介)
      - [物理卷管理](#物理卷管理)
      - [卷组管理](#卷组管理)
      - [逻辑卷管理](#逻辑卷管理)
  - [磁盘备份](#磁盘备份)
    - [为什么要做备份](#为什么要做备份)
    - [选择备份机制](#选择备份机制)
    - [选择备份介质](#选择备份介质)
    - [备份文件系统(dump)](#备份文件系统dump)
    - [从灾难中恢复(restore)](#从灾难中恢复restore)
    - [让备份定时自动完成(cron)](#让备份定时自动完成cron)
  - [备份工具](#备份工具)
    - [数据镜像备份(rsync)](#数据镜像备份rsync)
    - [pigz](#pigz)
  - [常见问题和常用命令](#常见问题和常用命令)
    - [无法卸载文件系统](#无法卸载文件系统)
    - [修复受损文件系统](#修复受损文件系统)
    - [修复文件系统超级块](#修复文件系统超级块)
    - [使用Windows分区](#使用windows分区)
    - [自动挂载所有Windows分区的脚本](#自动挂载所有windows分区的脚本)
  - [设置虚拟内存](#设置虚拟内存)
    - [查看虚拟内存大小](#查看虚拟内存大小)
    - [交换文件](#交换文件)
      - [创建/root/swapfile文件](#创建rootswapfile文件)
      - [创建交换文件](#创建交换文件)
      - [格式化交换文件](#格式化交换文件)
      - [启用交换文件](#启用交换文件)
      - [检查虚拟内存](#检查虚拟内存)
      - [开机自动加载虚拟内存](#开机自动加载虚拟内存)
      - [停用交换文件](#停用交换文件)
  - [磁盘加密](#磁盘加密)
    - [关于LUKS](#关于luks)
    - [cryptsetup](#cryptsetup)
      - [luksFormat](#luksformat)
      - [luksOpen](#luksopen)
      - [luksSuspend](#lukssuspend)
      - [luksClose](#luksclose)
      - [luksResume](#luksresume)
      - [luksAddKey](#luksaddkey)
      - [luksRemoveKey](#luksremovekey)
      - [luksKillSlot](#lukskillslot)
      - [luksUUID](#luksuuid)
      - [luksDump](#luksdump)
      - [luksHeaderBackup](#luksheaderbackup)
      - [luksHeaderRestore](#luksheaderrestore)
- [Shell编程](#shell编程)
  - [echo](#echo)
- [Linux系统安全](#linux系统安全)
- [内核管理](#内核管理)
  - [内核空间(kernel)](#内核空间kernel)
  - [用户空间(rootfs)](#用户空间rootfs)
    - [根文件系统](#根文件系统)
    - [常用目录](#常用目录)
- [其他](#其他-1)
  - [终端](#终端)
    - [常用快捷键](#常用快捷键)
- [发行版本](#发行版本)
  - [Debian](#debian)
    - [Ubuntu](#ubuntu)
  - [RedHat](#redhat)
    - [Yum](#yum)
      - [列出所有可更新的软件清单命令](#列出所有可更新的软件清单命令)
      - [更新所有软件命令](#更新所有软件命令)
      - [仅安装指定的软件命令](#仅安装指定的软件命令)
      - [仅更新指定的软件命令](#仅更新指定的软件命令)
      - [列出所有可安裝的软件清单命令](#列出所有可安裝的软件清单命令)
      - [删除软件包命令](#删除软件包命令)
      - [查找软件包命令](#查找软件包命令)
      - [清除缓存命令](#清除缓存命令)
        - [清除缓存目录下的软件包](#清除缓存目录下的软件包)
        - [清除缓存目录下的 headers](#清除缓存目录下的-headers)
        - [清除缓存目录下旧的 headers](#清除缓存目录下旧的-headers)
        - [清除缓存目录下的软件包及旧的 headers](#清除缓存目录下的软件包及旧的-headers)
    - [OracleLinux](#oraclelinux)
      - [基本软件包](#基本软件包)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## 互联网控制消息协议(ICMP, Internet Control Message Protocol)

互联网协议族的核心协议之一.<br>
它用于网际协议(IP)中发送控制消息,提供可能发生在通信环境中的各种问题反馈.<br>
通过这些信息,使管理者可以对所发生的问题作出诊断,然后采取适当的措施解决.

ICMP依靠IP来完成它的任务,它是IP的主要部分.<br>
它与传输协议(如TCP和UDP)显著不同: 它一般不用于在两点间传输数据.<br>
它通常不由网络程序直接使用,除了 ping 和 traceroute 这两个特别的例子.<br>
IPv4中的ICMP被称作ICMPv4,IPv6中的ICMP则被称作ICMPv6.
 
## 存活时间(TTL, Time To Live)

指一个数据包在经过一个路由器时,可传递的最长距离(跃点数).<br>
每当数据包经过一个路由器时,其存活次数就会被减一.<br>
当其存活次数为0时,路由器便会取消该数据包转发,IP网络的话,会向原数据包的发出者发送一个ICMP TTL数据包以告知跃点数超限.<br>
其设计目的是防止数据包因不正确的路由表等原因造成的无限循环而无法送达及耗尽网络资源.

## 跳数(Hop)

跳数: 网络中两个端路径上的节点,路由器的数目.

## 互联网服务提供商(ISP, Internet Service Provider)

指提供互联网访问服务的公司.<br>
通常大型的电信公司都会兼任互联网服务供应商,一些ISP则独立于电信公司之外.

互联网服务供应商透过固网早期的拨号连线、后来的ADSL(非对称数字用户线路)、<br>
FTTx(光纤)、数据专线或是移动网络等方式提供互联网访问服务予客户.<br>
一些供应商还提供域名登记、网页托管和主机托管等服务.<br>
互联网服务供应商可以是商业营利性质的,也可以是由政府以公共财政支持,或国有资产.

## 网络地址转换(NAT, Network Address Translation)

网络地址转换,是一种通过在数据包通过流量路由设备传输时修改数据包IP报头中的网络地址信息,<br>
将IP地址空间映射到另一个IP地址空间的方法.<br>
最初使用该技术是为了避免在移动网络或替换上游Internet服务提供商但无法路由网络地址空间时为每个主机分配一个新地址.<br>
在IPv4地址耗尽的情况下,它已成为保护全局地址空间的流行且必不可少的工具.<br>
一个可通过Internet路由的IP地址NAT网关可以用于整个专用网络.

IP伪装是一种在另一个通常是公共地址空间中的单个IP地址后面隐藏整个IP地址空间(通常由私有IP地址组成)的技术.<br>
隐藏的地址将更改为单个(公共)IP地址,作为传出IP数据包的源地址,因此它们显示为不是源自隐藏主机,而是源自路由设备本身.<br>
由于此技术可以节省IPv4地址空间,因此NAT一词实际上已成为IP伪装的代名词.

由于网络地址转换会修改数据包中的IP地址信息,因此NAT实现在各种寻址情况下的特定行为及其对网络流量的影响可能会有所不同.<br>
包含NAT实现的设备的供应商通常不会记录NAT行为的细节.

曾遇到过一种网络异常情况: 丢包率不高, 链路测试正常, 但访问外网地址时好时坏.<br>
直接原因最终未能排查出来,只能通过重做snat得以解决.

# Linux系统启动过程
## Linux系统启动过程简介



## BIOS加电自检



## 引导加载程序



### 引导加载程序的启动



### GRUB配置



## init进程



### init进程简介



### init进程的引导过程



### 配置自动运行服务



## 重启和关闭系统



### 关闭或重启系统(shutdown)



### 关闭系统(halt)



### 重启系统(reboot)



### 改变运行级别(init)



### 通过图形界面关闭系统



## 系统启动时常见的问题处理



### 进入Linux救援模式



### GRUB被Windows覆盖



### 重新分区后GRUB引导失败

# 文件目录管理

## Linux文件系统的架构

### Linux文件系统简介



### Linux支持的文件系统类型



### Linux的默认安装目录



## 文件系统管理

> 关于文件系统管理及文件管理两者的个人区分: <br>
文件管理指对具体某一文件内容进行操作,<br>
文件系统管理则是对文件目录和单个及多个文件进行管理操作.

### 打印树结构目录(tree)

参数|含义
-|-
-a|打印包含隐藏文件在内的所有文件
-L n| 打印n层目录
-N|原样输出(字符按原样而不是转义八进制数字)

### 文件类型



#### 查看文件类型

#### 硬链接和软链接

Linux链接分两种,一种被称为硬链接(Hard Link),另一种被称为符号链接(Symbolic Link).默认情况下,ln命令产生硬链接.

```bash
ln [选项] 源文件 目标文件
```

选项:

0. -s:建立软链接文件.如果不加 "-s" 选项,则建立硬链接文件;
0. -f:强制.如果目标文件已经存在,则删除目标文件后再建立链接文件;

##### 硬链接

> 硬链接实际上是为文件建一个别名,链接文件和原文件实际上是同一个文件.

> 特点

1. 它会在链接文件处创建一个和被链接文件一样大小的文件,类似于国外网站和国内镜像的关系.
2. 硬链接占用的空间和被链接文件一样大(其实就是同一片空间)
3. 修改链接文件和被链接文件中的其中一个,另外一个随之同样发生变化
4. 硬链接的对象不能是目录,也就是说被链接文件不能为目录
5. 硬链接的两个文件是独立的两个引用计数文件,他们共用同一份数据,所以他们的inode节点相同.
6. 删除硬链接中的任意一个文件,另外一个文件不会被删除.没有任何影响,链接文件一样可以访问,内容和被链接文件一模一样.

##### 软链接

> 通过软链接建立的链接文件与原文件并不是同一个文件,相当于原文件的快捷方式.

> 特点

0. 软连接的链接文件就是一个基本单元大小的文件,一般为3B,和被链接文件的大小没有关系.
0. 软链接的链接文件中存储的是被链接文件的元信息,路径或者inode节点.
0. 软连接的连接文件是一个独立的文件,有自己的元信息和inode节点.
0. 删除软链接的链接文件,被链接文件不会受到任何影响.
0. 删除软链接的被链接文件,链接文件会变成红色,这时打开链接文件会报错,报找不到被链接的文件这种错误.
0. 软链接可以链接任何类型的文件,包括目录和设备文件都可以作为被链接的对象.




### 建立目录(mkdir)

参数|含义
-|-
-p|递归创建多层级

```bash
mkdir -p 统计学/应用统计学/描述统计学/
```

### 建立一个空文件(touch)

### 显示当前目录(pwd)



### 改变目录(cd)



### 列出目录内容(ls)

### 查看目录空间大小

#### 可读参数(human -h)

-h 参数用于将文件/目录大小转换为可读单位.

```
df -h
ls -lh
```

### 列出目录内容(dir和vdir)



### 移动和重命名(mv)



### 复制文件/目录(cp)



### 删除目录和文件(rmdir和rm)

### 查看已挂载文件系统

### 使用fstab文件自动挂载文件系统

查看硬盘uuid:

```
blkid
```

编辑启动文件(/etc/fstab):

追加:

```bash
# UUID=硬盘uuid 挂载路径 ext4 defaults 0 0

UUID=5202aacf-9e77-4768-b4ec-d6009adbb179 /media/TMBack ext4 defaults 0 0
```

### 查看文件/目录属性

### 文件名通配符

### 查找特定程序(whereis)

### 查找文件(find)



### 快速定位文件(locate)

### 转换目录和文件名大小写

### alias 创建别名

> 用于为命令设置别名,生命周期在本次关机前有效.<br>
如果需要永久生效可以加入当前用户的.brachrc文件中.

```
# alias 别名='命令'
alias elasticsearch_start='~/space/software/elasticsearch-5.6.10/bin/elasticsearch -d'
```

#### 传递自定义参数

```
rails s -p3000
alias ras='rails s -p$1'
```




### 文件/目录权限管理



#### Linux文件/目录权限简介

#### 权限设置针对的用户



#### 需要设置哪些权限


#### 查看文件/目录的属性

#### 改变文件所有权

##### 更改所属用户(chown)

```sh
chown [-R] 账号名称:用户组名称 文件或目录

# 更改文件夹为当前用户及组所有
sudo chown -R $(id -u -n):$(id -g -n) 文件或目录
```

##### chgrp

#### 更改文件/目录的权限(chmod)

```
chmod 参数 XXX(权限) 文件/文件夹
```

> 参数

参数|作用
-|-
-R|修改文件夹及子文件夹权限

> 权限

XXX分别代表文件所有者、所有者所在群组以及其他用户的相关权限.

权限用数字表示,角色拥有权限即为读、写、执行三项权限值之和.

权限|权限值|说明
-|-|-
r(Read)|4|读取,对文件而言,具有读取文件内容的权限.<br>对目录来说,具有浏览目 录的权限.
w(Write)|2|写入,对文件而言,具有新增、修改文件内容的权限.<br>对目录来说,具有删除、移动目录内文件的权限.
x(Execute)|1|执行,对文件而言,具有执行文件的权限.<br>对目录了来说该用户具有进入目录的权限.

例如将foo及其子文件等设置为所有用户均可读、写、执行

```
chmod -R 777 foo/
```

#### 设置文件/目录的默认权限

#### 文件权限的八进制表示





## 文件管理



### 查看文本文件(cat和more)



### 阅读文件的开头和结尾(head和tail)



### 更好的文本阅读工具(less)



### 查找文件内容

#### grep

##### 多个关键字的 *与* 和 *或*

或:

```
crontab -l | grep -E 'suning|leading|jikou|shx'
```

与:

```bash
grep pattern1 files | grep pattern2 #显示既匹配 pattern1 又匹配 pattern2 的行.
```

#### sed

> sed是stream editor的简称,也就是流编辑器.<br>它一次处理一行内容,处理时,把当前处理的行存储在 *临时缓冲区中*,称为 *模式空间*(pattern space),<br>接着用sed命令 *处理缓冲区中的内容*,处理完成后,把缓冲区的内容送往屏幕.


```
sed [选项] '命令' 被操作文件
```

##### 替换内容中有斜杠

```sh
# 替换内容中有斜杠 只需改变分隔符即可
# 分隔符由/换成#(其他字符也可以,只要跟在s命令后面即可)
data_dir="/home/ubuntu/mnt/nfs/data"
relative_path="数据目录/股票"
echo $relative_path | sed -e "s#数据目录#$data_dir#g"
```

##### 常用选项

选项|作用
-|-
n|安静模式,只有经过sed特殊处理的那一行(或者动作)才会被列出来.<br>一般所有来自stdin的内容一般都会被列出到屏幕上.
e|直接在指令列模式上进行 sed 的动作编辑.
f|直接将sed的动作写在一个文件内,<br> `-f filename` 则可以执行filename内的sed命令.
r|让sed命令支持扩展的正则表达式(默认是基础正则表达式).
i|直接修改读取的文件内容,而不是由屏幕输出.


##### 命令类别

命令|区别|示例
-|-|-
匹配行a \s |在匹配行 *下方* **追加** 指定内容s|sed -i -E '/^\\{$/a \ \ "private": true,' package.json
匹配行i \s |在匹配行 *上方* **追加** 指定内容s
匹配行c \s |将匹配行内容 **替换** 为指定内容s<br>***整行内容进行替换***|sed -i -E '/(^.*"test":.*$)/c \ \ \ \ "test": "echo \"Error: no test specified\" && exit 1",' package.json
匹配行/d|将匹配行进行 **删除** <br>因为是删除所以无需指定内容|sed -i -E '/.\*"main":.\*/d' package.json
匹配行p|将匹配行进行打印输出<br>p命令一般和-n选项一起使用
s/匹配文本/需替换为内容/g|在 *全局范围* 内将所有 `匹配文本` 全部替换为 `需替换为内容`<br> 最后的g是global的意思,也就是全局替换,<br> 如果不加g,则只会替换每行的第一个匹配内容

这几个命令之间有一个共同点就是在内容匹配方面,都有三种匹配方式:

0. 具体行号匹配
0. 行号范围匹配
0. 正则匹配

以命令a进行示例:

本地创建test.txt文件,并写入下面内容:

```
this is first line
this is second line
this is third line
this is fourth line
this fifth line
happy everyday
end
```

##### 追加行(a \\)

a \:追加行(append), <br>a \的后面跟上字符串s(多行字符串可以用\n分隔),<br>则会在当前选择的行的后面都加上字符串s

##### 通过行号追加

```
sed '行号a \追加指定内容' 被操作文件
```

```
sed '1a \add one' test.txt
#=>
this is first line
add one
this is second line
this is third line
this is fourth line
this fifth line
happy everyday
end

```

**这里的行号既可以是具体的数值,<br>也可以表达特定的范围.**

如 `1,3`(第一行到第三行), `2,$`(第二行到最后一行中间所有的行)

##### 通过正则追加

```
sed '/正则表达式/a \追加内容' 被操作文件

```

##### 日志分析

需要分析的日志,主要分为:

0. 特定时间范围内的日志
0. 包含具体关键字的日志

```
sed -n '/21\/Mar\/2019:13:24/,/21\/Mar\/2019:13:34/p' access.log >> c.log
```

### EOF写入文件

#### delimited by end-of-file

> delimited by end-of-file (wanted `EOF')

原因在于结尾的EOF存在多余空格

### 文件切割

#### split

```
split -d -b 切割大小  切割文件  存储目录/文件名
split -d -b 1m  development.log  /home/ff4c00/test.log
```

### 文件删除后仍然占用空间

当使用 rm 删除了大文件,但是如果有进程打开了这个大文件,<br>
却没有关闭这个文件的句柄,<br>
那么 Linux 内核还是不会释放这个文件的磁盘空间,<br>
最后造成磁盘空间占用 100%,整个系统无法正常运行.

查找哪些已删除文件被程序占用:

```
lsof -n | grep deleted
```

这个是排查磁盘空间占用异常的一方面,解决办法就是杀掉占用进程,<br>
如果实在太多重启服务器好了.

## 输入输出重定向和管道



### 输出重定向



### 输入重定向



### 管道(|)

# 软件包管理

## 软件包管理系统简述



## 管理*.deb软件包(dpkg)



### 安装软件包

#### 解决依赖问题

```
sudo apt-get -f -y install
sudo dpkg -i XXX.deb
```


### 查看已安装的软件包



### 卸载软件包


## 使用RPM软件包



### RPM简介



### RPM命令的使用方法



### 安装RPM软件包



### 查看RPM软件包



### 升级软件包



### 删除软件包

### 查看程序是由哪个RPM包安装



## 高级软件包工具(apt)



### apt简介

### 更新源

#### ubuntu-ports和ubuntu区别

ubuntu-ports和ubuntu是指源地址中的最后层级地址:

```
http://mirrors.aliyun.com/ubuntu-ports/
http://mirrors.aliyun.com/ubuntu/
```

源地址|收录架构|收录版本|
-|-|-
ubuntu-ports|arm64<br>armhf<br>PowerPC<br>ppc64el<br>s390x|所有 Ubuntu 当前对该架构支持的版本,包括开发版
ubuntu|arm64<br>Intel x86|所有 Ubuntu 当前支持的版本,包括开发版

#### 删除ppa

##### 删除源

```bash
# user/ppa-name具体如何查询有待考证
#  http://ppa.launchpad.net/wine/wine-builds/ubuntu
# 之前是通过wine/wine-builds推断出来的
sudo add-apt-repository -r ppa:user/ppa-name
```

##### /etc/apt/sources.list.d

删除相关内容

### 下载和安装软件包

### 更新软件

```
sudo apt-get update;sudo apt-get upgrade;sudo apt-get dist-upgrade
```

### 查询软件包信息

### 删除软件包

参数|区别
-|-
remove|会删除软件包,但会保留配置文件
purge|会将软件包以及配置文件都删除

```
sudo apt-get remove package-name
sudo apt-get purge package-name
```

### 配置apt-get




## 从源代码编译软件



### 为什么要从源代码编译



### 下载和解压软件包



#### 正确配置软件



#### 编译源代码



#### 安装软件到硬盘



#### 出错了怎么办


### .bin文件安装

# 环境管理

## 字符集管理(locale)

> 用于管理系统安装的字符编码.

### 设置语言环境

```
export LC_ALL=zh_CN.UTF-8
export LANGUAGE=zh_CN.UTF-8
```

### 设置示例

```bash
LC_COLLATE #定义该环境的排序和比较规则
LC_CTYPE #用于字符分类和字符串处理,控制所有字符的处理方式,包括字符编码,字符是单字节还是多字节,如何打印等.是最重要的一个环境变量.

LC_MONETARY # 货币格式

LC_NUMERIC # 非货币的数字显示格式

LC_TIME # 时间和日期格式

LC_MESSAGES # 提示信息的语言.

# 另外还有一个LANGUAGE参数,它与LC_MESSAGES相似,但如果该参数一旦设置,则LC_MESSAGES参数就会失效.

# LANGUAGE参数可同时设置多种语言信息,如LANGUAGE="zh_CN.GB18030:zh_CN.GB2312:zh_CN".

LANG # LC_*的默认值,是最低级别的设置,如果LC_*没有设置,则使用该值.类似于 LC_ALL

LC_ALL # 它是一个宏,如果该值设置了,则该值会覆盖所有LC_*的设置值.注意,LANG的值不受该宏影响
```

### locale: Cannot set LC_CTYPE to default locale: No such file or directory

> 本地没有相关语言包.

```
sudo locale-gen zh_CN.UTF-8
```


### locale-gen: command not found

```
sudo apt-get clean && sudo apt-get update && sudo apt-get install -y locales
```

## 查看基本信息(uname)

> uname用于显示电脑以及操作系统的相关信息.

参数|作用
-|-
-a|显示全部的信息
-m|显示电脑类型
-n|显示在网络上的主机名称
-r|显示操作系统的发行编号
-s|显示操作系统名称
-v|显示操作系统的版本
-p|输出处理器类型
-i|输出硬件平台
-o|输出操作系统名称

## 系统时区管理(tzdata)

普通流程:

```bash
export DEBIAN_FRONTEND=noninteractive
sudo apt-get install -y tzdata
# 采用上海时区: 6,70

# 建立期望的时区链接(上海)
sudo ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

# 重新配置tzdata软件包,使得时区设置生效
sudo dpkg-reconfigure -f noninteractive tzdata

# -----
sudo apt update &&\
sudo ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime &&\
sudo apt-get install -y tzdata
```

## 环境变量文件

文件名|作用
-|-
/etc/profile|此文件为系统的每个用户设置环境信息,当用户第一次登录时,该文件被执行.<br>并从/etc/profile.d目录的配置文件中搜集shell的设置.
/etc/bashrc|为每一个运行bash shell的用户执行此文件.当bash shell被打开时,该文件被读取.
~/.bash_profile|每个用户都可使用该文件输入专用于自己使用的shell信息,当用户登录时,该文件仅仅执行一次!默认情况下,他设置一些环境变量,执行用户的.bashrc文件.
~/.bashrc|该文件包含专用于你的bash shell的bash信息,当登录时以及每次打开新的shell时,该文件被读取.

## 定时任务

### cron

> cron是一个Linux定时执行工具,可以在无需人工干预的情况下运行作业.

#### 安装

```
sudo apt-get install cron
```

#### 常用命令

命令|作用
-|-
service cron start|启动服务
service cron stop|关闭服务
service cron restart|重启服务
service cron reload|重新载入配置
service cron status|检查状态
pgrep cron|查看是否运行

### crontab

> crontab 命令用于安装、删除或者列出用于驱动cron后台进程的表格<br>
也就是说,用户把需要执行的命令序列放到crontab文件中以获得执行,<br>
每个用户都可以有自己的crontab文件.

#### 常用命令

命令|作用
-|-
crontab -u|设定某个用户的cron服务<br>如果不使用 -u user 的话,就是表示设定自己的时程表.
crontab -l|列出某个用户cron服务的详细内容
crontab -r|删除某个用户的cron服务
crontab -e|编辑某个用户的cron服务

#### crontab文件

基本格式:
```
M H D m d cmd
```

标题|含义
-|-
M|分钟(0-59)
H|小时(0-23)
D|天(1-31)
m|月(1-12)
d|一星期内的天(0~6,0为星期天)
cmd|要运行的程序,程序被送入sh执行,<br>这个shell只有USER,HOME,SHELL这三个环境变量

```bash
# 周一到周五 早九点至晚六点 每3小时 执行一次
'* 9-18/3 * * 1-5'
```

## service

> service命令是Redhat Linux兼容的发行版中用来控制系统服务的实用工具,<br>
它以启动、停止、重新启动和关闭系统服务,<br>
还可以显示所有系统服务的当前状态.

### 语法

```
service(选项)(参数)
```

### 选项

选项|作用
-|-
-h|显示帮助信息
--status-all|显示所服务的状态

### 参数

0. 服务名:自动要控制的服务名,即/etc/init.d目录下的脚本文件名.
0. 控制命令:系统服务脚本支持的控制命令.

### 实例

#### network

```bash
# 查看网络状态
service network status
# 重启网络
service network restart
```


# 用户和用户组管理
## 用户管理概述



### 用户账号



#### 用户账号文件(passwd,shadow)



### 用户组



#### 用户组文件(group,gshadow)



## 普通用户管理



### 添加用户

```
useradd --create-home -s /bin/bash -u 用户ID -g 组ID 用户名
```

### 更改用户密码

```
passwd user_name
```

### 修改用户信息

#### 修改用户名

```
usermod -l new_user_name old_user_name
```

#### 修改用户目录

```
# 将用户 user1 目录改为/users/us1
usermod -d /users/us1 user1
usermod -d /users/web_server_1 web_server_1
```

### 删除用户

-r 同时删除工作目录

```
userdel user_name
```

### 禁用用户



### 配置用户Shell环境

### 批量添加用户

### 完整删除用户账号

### 记录用户操作(history)


### 忘记root用户密码



### 误删用户账号

### 查看用户信息(id)



### 用户间切换(su)

## 用户组管理

### 创建用户组

```bash
groupadd -g 组ID 组名
```

### 添加用户到用户组

将用户 user1 加入到 users组中:

```
usermod -g users user1
```

### 修改用户组



### 删除用户组


## 受限的特权(sudo)



## /etc/passwd文件


### 加密的口令



### UID号



### GID号



## /etc/shadow文件



## /etc/group文件

# 进程管理



## 什么是进程



## 进程的属性



### 进程的ID号(PID)



### 父进程的PID(PPID)



### 真实和有效的用户ID(UID, EUID)



### 真实和有效的组ID(GID, EGID)



### 谦让度和优先级



## 监视进程(ps)



## 即时跟踪进程信息(top)



## 查看占用文件的进程(lsof)



## 向进程发送信号(kill)



## 调整进程的谦让度(nice, renice)



## /PROC文件系统

## 进程休眠(sleep)

sleep 参数n(可以为小数)

参数|说明
-|-
n|延迟n秒
nm|延迟n分钟
nh|延迟n小时
nd|延迟n天

# 网络管理

## 获取IP地址

### 公共

0. curl -4/-6 icanhazip.com
0. curl ifconfig.me
0. curl ipinfo.io/ip
0. curl api.ipify.org
0. curl checkip.dyndns.org
0. dig +short myip.opendns.com @resolver1.opendns.com
0. host myip.opendns.com resolver1.opendns.com
0. curl ident.me
0. curl bot.whatismyipaddress.com
0. curl ipecho.net/plain

### 私有

0. ip route get 1.2.3.4 | awk '{print $7}'
0. ifconfig -a
0. ip addr (ip a)
0. ip route get 1.2.3.4 | awk '{print $7}'
0. nmcli -p device show

## 链路测试(mtr)

> 链路测试为检测从主机本地到目标主机之间所有链路节点状况.

### 安装

```bash
# CentOS/RHEL 安装
yum install mtr
 
# Debian/Ubuntu 安装
sudo apt install mtr
 
# Fedora
sudo dnf install mtr
 
# Arch Linux
pacman -Syu
pacman -S mtr
 
# Mac OS X: 
brew install mtr
 
# OpenSUSE
zypper install mtr
```

### 使用

基础用法:

```bash
mtr www.baidu.com
```

针对具体端口测试:

```bash
mtr -P 443 -i 0.5 www.baidu.com
```

注意点:

0. 最下面到目标主机是否畅通
0. 未到达目标主机: 是否在一个环路中不断发送数据包(两个节点间不断循环转发).
0. 一般用最后一跳的实际延迟为准.

### 可用命令选项

```
-F, --filename FILE        从文件中读取主机名
-4                         仅使用IPv4
-6                         仅使用IPv6
-u, --udp                  使用UDP代替ICMP回显
-T, --tcp                  使用TCP代替ICMP echo
-a, --address ADDRESS      将输出套接字绑定到ADDRESS
-f, --first-ttl NUMBER     设置启动什么TTL
-m, --max-ttl NUMBER       最大跳数
-U, --max-unknown NUMBER   最大未知主机
-P, --port PORT            TCP,SCTP或UDP的目标端口号
-L, --localport LOCALPORT  UDP的源端口号
-s, --psize PACKETSIZE     设置用于探测的数据包大小
-B, --bitpattern NUMBER    设置要在有效载荷中使用的位模式
-i, --interval SECONDS     ICMP回显请求间隔
-G, --gracetime SECONDS    等待响应的秒数
-Q, --tos NUMBER           IP标头中的服务字段类型
-e, --mpls                 显示来自ICMP扩展的信息
-Z, --timeout SECONDS      秒保持探针插座打开
-r, --report               使用报告模式输出
-w, --report-wide          输出范围广泛的报告
-c, --report-cycles COUNT  设置发送的ping数
-j, --json                 输出json
-x, --xml                  输出xml
-C, --csv                  输出逗号分隔值
-l, --raw                  输出原始格式
-p, --split                分割输出
-t, --curses               使用curses终端界面
   --displaymode MODE      选择初始显示模式
-n, --no-dns               不解析主机名
-b, --show-ips             显示IP号和主机名
-o, --order FIELDS         选择输出字段
-y, --ipinfo NUMBER        在输出中选择IP信息
-z, --aslookup             显示AS号
-h, --help                 显示此帮助并退出
-v, --version              输出版本信息并退出
```


## Netplan

> Netplan 是一款使用在终端的配置网络工具.

### 备份原配置文件

```
sudo cp  /etc/netplan/01-network-manager-all.yaml /etc/netplan/01-network-manager-all.yaml.bak
```

### 确定网络设备名称

```
ip a
```

### 确定网关地址

```
ip route show
```

### 确定DNS地址

```
cat /etc/resolv.conf | grep 'nameserver'
```

### 配置静态IP地址

```yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    DEVICE_NAME: # 需要配置设备的实际名称
      dhcp4: yes/no # 是否启用 dhcp4
      addresses: [IP/NETMASK] # IP地址/掩码
      gateway4: GATEWAY # 网关的地址
      nameservers:
        Addresses: [NAMESERVER, NAMESERVER] # 由逗号分开的 DNS 服务器列表
```

### 测试配置

```
sudo netplan try
```

如果确信配置文件没有问题,可以跳过测试环节并且直接使用新的配置:

```
sudo netplan apply
```

可以使用 ip a 看看新的地址是否正确.

## 重启网络

```
sudo service networking restart
```

## 127.0.0.1和0.0.0.0地址

地址|作用
-|-
127.0.0.1|回环地址<br>该地址指电脑本身地址<br>其他主机通过该地址无法访问到目标服务器
0.0.0.0|在IP数据报中只能用作源IP地址<br>在服务器中,0.0.0.0指的是本机上的所有IPV4地址<br>如果一个主机有两个IP地址,192.168.1.1 和 10.1.2.1<br>并且该主机上的一个服务监听的地址是0.0.0.0<br>那么通过两个ip地址都能够访问该服务.

## 开启BBR

> Google BBR 是一款免费开源的TCP拥塞控制传输控制协议, 可以使 Linux 服务器显著提高吞吐量和减少 TCP 连接的延迟.

***内核版本需大于4.9***

### 修改/etc/sysctl.conf

```bash
sudo tee -a /etc/sysctl.conf <<-'EOF'
# 开启bbr加速
net.core.default_qdisc=fq
net.ipv4.tcp_congestion_control=bbr
EOF
```

### 保存生效

```bash
sysctl -p
```

### 检查BBR是否开启

```bash
sysctl net.ipv4.tcp_available_congestion_control
# 返回内容中包含bbr表名已开启
net.ipv4.tcp_available_congestion_control = bbr cubic reno
```

### 检查BBR是否启动成功

```bash
lsmod | grep bbr
# 返回下面内容标识启动成功
tcp_bbr                20480  14
```

## 配置翻墙(clash)

### clash安装

```sh
# 非最新链接 供应商对版本有要求
# x86_64:
clash_url='https://github.com/Dreamacro/clash/releases/download/v0.19.0/clash-linux-amd64-v0.19.0.gz'
# armv7l
# clash_url='https://github.com/Dreamacro/clash/releases/download/v0.19.0/clash-linux-armv7-v0.19.0.gz'
clash_path="/home/$USER/clash" # 必须是绝对路径
config_path="$clash_path/config" # 必须是绝对路径
mkdir $clash_path
cd $clash_path
wget -t 5 -c $clash_url
sudo gunzip *.gz
chmod +x clash*
mkdir $config_path
```

### Country.mmdb安装

> Country.mmdb为全球IP库,<br>
可以实现各个国家的IP信息解析和地理定位

```sh
# 该数据库每月更新
country_url='https://github.com/Dreamacro/maxmind-geoip/releases/download/20201212/Country.mmdb'
wget -t 5 -c -P $config_path $country_url
```
### 管理配置文件

```sh
config_url='http://168dydz.com/S/SS/2CA0E2DEFB7B39D9'
curl $config_url >| "$config_path/$(date +%Y%m).yaml"
```
### Web界面

> clash服务有一个RESTful API的服务(config.yaml中配置的external-controller端口),<br>
可以通过其访问web管理页面.

目前有个Bug,如果本地开着clash,即使是访问远程的ip端口还是显示本地的内容, 先把本地关了就好.

```sh
dashboard_path="$config_path/dashboard"
mkdir $dashboard_path
yacd_url='https://github.com/haishanh/yacd/archive/gh-pages.zip'
wget -t 5 -c -P $config_path $yacd_url
unzip *.zip 
mv yacd-gh-pages $dashboard_path
```

### 基础配置文件

clash可配置多份配置文件 只需在启动时-f指出即可.<br>
名为config.yaml的配置文件为基础文件,其他配置文件与其存在冲突的均以该文件参数内容为准

```bash
tee "$config_path/config.yaml" <<EOF
# HTTP 代理端口
port: 7890

# SOCKS5 代理端口
socks-port: 7891

# Linux 和 macOS 的 redir 代理端口
redir-port: 7892

# 规则模式:Rule(规则)/Global(全局代理)/ Direct(全局直连)
mode: Rule

# 设置日志输出级别 (默认级别:silent,即不输出任何内容,以避免因日志内容过大而导致程序内存溢出).
# # 5 个级别:silent/info/warning/error/debug.级别越高日志输出量越大,越倾向于调试,若需要请自行开启.
log-level: info

# Clash 的 RESTful API
external-controller: 0.0.0.0:9090

# 允许局域网的连接
allow-lan: true

# RESTful API 的口令
secret: ''

# 您可以将静态网页资源 参数应填写配置目录的相对路径或绝对路径.
external-ui: "$dashboard_path"

Proxy:

Proxy Group:

Rule:
- DOMAIN-SUFFIX,google.com,DIRECT
- DOMAIN-KEYWORD,google,DIRECT
- DOMAIN,google.com,DIRECT
- DOMAIN-SUFFIX,ad.com,REJECT
- GEOIP,CN,DIRECT
- MATCH,DIRECT
EOF
```

### 启动服务

```sh
./clash* -d config/ -f config/*.yaml
# ./clash* -d config/ -f config/config.yaml

# 0.19.0版本存在一个问题: *.yaml启动后无法打开web页面
# 得先 -f config/config.yaml 启动打开web页面后服务关掉再-f config/*.yaml才行
```

-d 用于指定配置文件目录

-f用于指定配置文件路径,默认为: 

```
$HOME/.config/clash/config.yaml
```

服务启动后在浏览器中打开:

```sh
ip:9090/ui
# 或
http://clash.razord.top/
# 123.57.155.17:9090/ui
```

### 终端代理命令

```
export https_proxy=http://127.0.0.1:7890 http_proxy=http://127.0.0.1:7890 all_proxy=socks5://127.0.0.1:7891
```
# 系统监控

## top

操作项|作用
-|-
Ctrl+E|切换内存展示单位

## ps

### 查看LINUX进程内存占用情况

```
# 实现按内存排序,由大到小
ps -e -o 'pid,comm,args,pcpu,rsz,vsz,stime,user,uid' | grep oracle |  sort -nrk5
```

&emsp;|&emsp;
-|-
rsz|实际内存
vsz|进程的虚拟大小

## pingtop

> 顾名思义,它会一次ping多台服务器,并在类似top的终端UI中显示结果.

### 安装

```
# 安装pip
sudo apt install -y python-pip
# 使用pip安装pingtop
pip install pingtop
```

### 使用

```
pingtop google.com baidu.com bing.com
```

## nethogs

nethogs是一款小巧的 *net top* 工具,<br>
可以显示每个进程所使用的带宽,并对列表排序,将耗用带宽最多的进程排在最上面.<br>
万一出现带宽使用突然激增的情况,用户迅速打开nethogs,就可以找到导致带宽使用激增的进程.<br>
nethogs可以报告程序的进程编号(PID)、用户和路径.

Ubuntu、Debian和Fedora用户可以从默认软件库获得.<br>
CentOS用户则需要Epel.

```
sudo apt update; sudo apt install nethogs
sudo nethogs
```

## bmon

bmon(带宽监控器)是一款类似nload的工具,它可以显示系统上所有网络接口的流量负载.<br>
输出结果还含有图表和剖面,附有数据包层面的详细信息.


![](https://s3.51cto.com/wyfs02/M02/23/ED/wKiom1NHVY3jnEy6AADs1KELymM553.jpg)

bmon支持许多选项,能够制作HTML格式的报告.<br>
欲知更多信息,请参阅参考手册页.

## iftop

iftop用于网卡机器级别的流量监控,可以实时显示当前机器和其他主机之间的网络流量.

iftop默认监控第一个可用网卡的流量,可以通过参数指定要监控的网卡比如iftop -i eth0.网卡名称可以通过ifconfig获取.

![](https://user-gold-cdn.xitu.io/2019/10/21/16decf924a49c6bc?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

每两行代表和每个host之间不同方向的网络流量,第一列是源主机,第二列是目标主机,第三列分别是最近2s, 10s和40s的平均网络流量.默认按10s的网络流量进行排序.

TX:发送,RX:接收.

底部显示全局的流量统计,cum表示运行至今的累计情况,peak表示峰值数据, rates 表示最近2s,10s和40s秒平均网络流量.

### 常用参数

#### 端口显示

```
N - toggle service resolution
S - 是否显示源主机端口           
D - 是否显示目标主机端口
p - 是否显示端口号
```

#### 排序

```
 1/2/3 - 根据2s,10s和40s的流量排序
 < - 按源主机进行排序
 > - 按目标主机排序
 o - 冻结当前排序,避免机器排序出现变化,方便观察固定主机流量
```

#### 其他

```
 P - 暂停刷新
 h - 显示帮助
 b - 是否显示进度条和刻度尺
 B - 循环切换按2s,10s, 40s显示进度条
 T - 显示或者隐藏统计总量
 j/k - 滚动显示
 f - 编辑过滤器代码
 l - 屏幕文本搜索过滤
 ! - 执行Shell命令
 q - 退出
```

# 磁盘管理

## 关于硬盘


## Linux文件系统



### ext3fs和ext4fs文件系统



### ReiserFS文件系统



### 有关swap

#### 误删Swap分区


## 挂载文件系统

### 查看块设备

```sh
sudo lsblk
```

### Linux下设备的表示方法

在 Linux 系统中磁盘设备文件的命名规则为: 

```
主设备号 + 次设备号 + 磁盘分区号
```

一般表示为: sd[a-z]x 

主设备号代表设备的类型,相同的主设备号表示同类型的设备.

当前常见磁盘的主设备号为 `sd`.<br>
次设备号代表同类设备中的序号,用 "a-z" 表示.

比如 /dev/sda 表示第一块磁盘,<br>
/dev/sdb 表示第二块磁盘.
x 表示磁盘分区编号.<br>
在每块磁盘上可能会划分多个分区,针对每个分区,Linux用 */dev/sdbx* 表示,这里的x表示第二块磁盘的第x个分区.


### 挂载文件系统(mount)

```
mount [选项] 设备文件地址 挂载目录
```

选项|说明
-|-
-V|显示程序版本
-h|显示辅助讯息
-v|显示较讯息,通常和 -f 用来除错.
-a|将 /etc/fstab 中定义的所有档案系统挂上.
-F|这个命令通常和 -a 一起使用,它会为每一个 mount 的动作产生一个行程负责执行.<br>在系统需要挂上大量 NFS 档案系统时可以加快挂上的动作.
-f|通常用在除错的用途.<br>它会使 mount 并不执行实际挂上的动作,而是模拟整个挂上的过程.<br>通常会和 -v 一起使用.
-n|一般而言,mount 在挂上后会在 /etc/mtab 中写入一笔资料.<br>但在系统中没有可写入档案系统存在的情况下可以用这个选项取消这个动作.
-s-r|等于 -o ro
-w|等于 -o rw
-L|将含有特定标签的硬盘分割挂上.
-U|将档案分割序号为 的档案系统挂下.<br>-L 和 -U 必须在/proc/partition 这种档案存在时才有意义.
-t|指定档案系统的型态,通常不必指定.<br>mount 会自动选择正确的型态.
-o async|打开非同步模式,所有的档案读写动作都会用非同步模式执行.
-o sync|在同步模式下执行.
-o atime、-o noatime|当 atime 打开时,系统会在每次读取档案时更新档案的『上一次调用时间』.<br>当使用 flash 档案系统时可能会选项把这个选项关闭以减少写入的次数.
-o auto、-o noauto|打开/关闭自动挂上模式.
-o defaults|使用预设的选项 rw, suid, dev, exec, auto, nouser, and async.
-o dev、-o nodev-o exec、-o noexec|允许执行档被执行.
-o suid、-o nosuid|允许执行档在 root 权限下执行.
-o user、-o nouser|使用者可以执行 mount/umount 的动作.
-o remount|将一个已经挂下的档案系统重新用不同的方式挂上.<br>例如原先是唯读的系统,现在用可读写的模式重新挂上.
-o ro|用唯读模式挂上.
-o rw|用可读写模式挂上.
-o loop=|使用 loop 模式用来将一个档案当成硬盘分割挂上系统.

#### 非同步模式

async挂载选项指定文件系统的输入和输出是异步完成的.<br>
当文件复制到设置了“异步”选项的可移动介质(如软盘驱动器)时,发出复制命令后的一段时间内,更改将被物理地写入软盘.<br>
如果设置了“异步”选项,并且在不使用“卸载”命令的情况下删除了介质,则所做的某些更改可能会丢失.

#### 同步模式

sync选项对设备的寿命不利.<br>
如果不选择该选项,内核将对写入进行重新排序并成批写入.<br>
使用该sync选项,内核按应用程序请求的顺序写入每个扇区.

### 在启动的时候挂载文件系统(/etc/fstab文件)



### 卸载文件系统(umount)

```
umount 磁盘路径
```

#### target is busy

> umount ... target is busy

三种方法:

```bash
fuser -kvm 挂载路径
# 强行解除挂载 
umount -l 挂载路径 
```


## 查看磁盘使用情况(df)

> 用于报告剩余空闲磁盘空间.

参数|含义
-|-
h|以人类友好格式显示文件系统硬盘空间使用情况(默认使用1K大小的块为单位来表示使用情况).
m|以MB为单位来显示文件系统磁盘空间使用情况.
i|列出节点而不是块的使用情况.
T|显示文件系统类型.
t|仅显示指定类型的文件系统.<br>只列出ext4文件系统:<br>df -t ext4
x|从结果中去排除指定类型的文件系统.(示例参考`-t`)
hT|显示某个目录的硬盘空间使用情况以及它的挂载点.

## 查看磁盘占用进程(fuser)

> fuser命令用于报告进程使用的文件和网络套接字.fuser命令列出了本地进程的进程号,那些本地进程使用file,参数指定的本地或远程文件.对于阻塞特别设备,此命令列出了使用该设备上任何文件的进程.

```
fuser [选项] 参数
```

选项|含义
-|-
-a|显示命令行中指定的所有文件.
-k|杀死访问指定文件的所有进程.
-i|杀死进程前需要用户进行确认.
-l|列出所有已知信号名.
-m|指定一个被加载的文件系统或一个被加载的块设备.
-n|选择不同的名称空间.
-u|在每个进程后显示所属的用户名.

参数可以是: 文件/目录名或者TCP、UDP端口号.


每个进程号后面都跟随一个字母,该字母指示进程如何使用文件:

跟随字母|含义
-|-
c|指示进程的工作目录.
e|指示该文件为进程的可执行文件(即进程由该文件拉起).
f|指示该文件被进程打开,默认情况下f字符不显示.
F|指示该文件被进程打开进行写入,默认情况下F字符不显示.
r|指示该目录为进程的根目录.
m|指示进程使用该文件进行内存映射,抑或该文件为共享库文件,被进程映射进内存.


## 检查和修复文件系统(fsck)



## 在磁盘上建立文件系统(mkfs)



## 使用USB设备



## 压缩工具

### gzip&gunzip

#### gunzip

> gunzip命令用来解压缩文件.<br>
gunzip是个使用广泛的解压缩程序,它用于解开被gzip压缩过的文件,这些压缩文件预设最后的扩展名为.gz.<br>
事实上gunzip就是gzip的硬连接,因此不论是压缩或解压缩,都可通过gzip指令单独完成.

```bash
gunzip 选项 文件列表
```

选项|作用
-|-
-a, --ascii|使用ASCII文字模式
-d, --decompress|指定解压后存放目录
-c, --stdout, --to-stdout|把解压后的文件输出到标准输出设备
-f, --force|强行解开压缩文件,不理会文件名称或硬连接是否存在以及该文件是否为符号连接
-h, --help|在线帮助
-l, --list|列出压缩文件的相关信息
-L, --license|显示版本与版权信息
-n, --no-name|解压缩时,若压缩文件内含有原来的文件名称及时间戳记,则将其忽略不予处理
-N, --name|解压缩时,若压缩文件内含有原来的文件名称及时间戳记,则将其回存到解开的文件上
-q, --quiet|不显示警告信息
-r, --recursive|递归处理,将指定目录下的所有文件及子目录一并处理
-S<压缩字尾字符串>, --suffix<压缩字尾字符串>|更改压缩字尾字符串
-t, --test|测试压缩文件是否正确无误
-v, --verbose|显示指令执行过程
-V, --version|显示版本信息

### 使用zip和unzip进行压缩



### 使用bzip2和bunzip2进行压缩



### 使用compress和uncompress进行压缩



### 支持rar格式



## 存档工具



### 文件打包(tar)

#### tar简介

#### 选项项

##### 必选选项

***必选选项是独立的命令,压缩解压都要用到其中一个,可以和别的命令连用,但一次只能用其中一个.***

选项|含义
-|-
c|建立一个压缩文件的选项指令(create 的意思)
x|解开一个压缩文件的选项指令
t|查看 tarfile 里面的文件
r|向压缩归档文件末尾追加文件
u|更新原压缩包中的文件

##### 可选选项

下面的选项是根据需要在压缩或解压档案时可选的.

选项|含义
-|-
z|有gzip属性,即需要用 gzip 压缩
j|有bz2属性,即需要用 bzip2 压缩
Z|有compress属性的
v|压缩的过程中显示文件(显示所有过程)！这个常用,但不建议用在背景执行过程！
O|将文件解开到标准输出
f|使用档名,请留意,在f之后要立即接档名!不要再加选项!<br>例如:使用『 tar -zcvfP tfile sfile』就是错误的写法,要写成『 tar -zcvPf tfile sfile』才对喔！
p|使用原文件的原来属性(属性不会依据使用者而变)
P|可以使用绝对路径来压缩！
N|比后面接的日期(yyyy/mm/dd)还要新的才会被打包进新建的文件中<br>--exclude FILE:在压缩的过程中,不要将 FILE 打包！
C|将文件解压缩到指定文件夹

#### 打包文件

```
tar -czf file_name.tar.gz file_name/*
```

#### 查看归档文件的内容



#### 还原归档文件

将文件还原到~/test目录下:

```
tar -xf $target_file_path -C ~/test
```

#### 往归档文件中追加新文件



#### 压缩归档文件

### 转移文件(dd)

### 解压

#### .gz

.gz压缩包(不带tar),用gzip命令即可

```
gzip(选项)(参数)
```

> 选项

选项|含义
-|-
-a或<br>--ascii|使用ASCII文字模式.
-d或<br>--decompress或<br>----uncompress|解开压缩文件.
-f或<br>--force|强行压缩文件.不理会文件名称或<br>硬连接是否存在以及该文件是否为符号连接.
-h或<br>--help|在线帮助.
-l或<br>--list|列出压缩文件的相关信息.
-L或<br>--license|显示版本与版权信息.
-n或<br>--no-name|压缩文件时,不保存原来的文件名称及时间戳记.
-N或<br>--name|压缩文件时,保存原来的文件名称及时间戳记.
-q或<br>--quiet|不显示警告信息.
-r或<br>--recursive|递归处理,将指定目录下的所有文件及子目录一并处理.
-S或<br><压缩字尾字符串>或<br>--suffix<压缩字尾字符串>|更改压缩字尾字符串.
-t或<br>--test|测试压缩文件是否正确无误.
-v或<br>--verbose|显示指令执行过程.
-V或<br>--version|显示版本信息.
-<压缩效率>|压缩效率是一个介于1~9的数值,预设值为“6”,指定愈大的数值,压缩效率就会愈高.
--best|此参数的效果和指定“-9”参数相同.
--fast|此参数的效果和指定“-1”参数相同.

> 参数

```
gzip(选项) 解压/压缩位置 目标位置
```

如果不指定目标位置,将解压到当前工作文件夹里.

#### .tar.gz

需要使用tar命令的-z和-f选项(解压需要-x) 
格式:

```
tar -zxf XXX.tar.gz -C 解压位置
```

后面的-C是大写C,如果不指定解压位置需要去掉-C,系统会把压缩包中所有文件解压到当前工作文件夹.

## 磁盘分区管理(fdisk)

### 磁盘分区简介

#### 查看硬盘信息

```
fdisk -l /dev/sd*
```

### 使用parted进行分区管理

### 使用mkfs建立ext3fs文件系统


### 测试分区

### 创建并激活交换分区

#### 配置fstab文件

#### 删除分区后系统无法启动

## 高级硬盘管理(RAID和LVM)



### 独立磁盘冗余阵列(RAID)



### 逻辑卷管理(LVM)

#### LVM简介



#### 物理卷管理



#### 卷组管理



#### 逻辑卷管理

## 磁盘备份



### 为什么要做备份



### 选择备份机制



### 选择备份介质



### 备份文件系统(dump)



### 从灾难中恢复(restore)



### 让备份定时自动完成(cron)

## 备份工具

### 数据镜像备份(rsync)

> rsync命令是一个远程数据同步工具,可通过LAN/WAN快速同步多台主机间的文件.<br>
rsync使用"rsync算法"来使本地和远程两个主机之间的文件达到同步,<br>
这个算法只传送两个文件的不同部分,而不是每次都整份传送,因此速度相当快.<br>
rsync是一个功能非常强大的工具,其命令也有很多功能特色选项.

> [Linux命令大全-rsync命令](http://man.linuxde.net/rsync)

### pigz

> pigz是支持并行压缩的gzip,pigz默认用当前逻辑cpu个数来并发压缩,无法检测个数的话,则默认并发8个线程,
也可以使用-p指定线程数.需要注意的是其CPU使用比较高.

## 常见问题和常用命令



### 无法卸载文件系统



### 修复受损文件系统



### 修复文件系统超级块



### 使用Windows分区


### 自动挂载所有Windows分区的脚本

## 设置虚拟内存

> Error occurred during initialization of VM<br>
Could not reserve enough space for 2097152KB object heap

原因在于虚拟内存太小.

### 查看虚拟内存大小

```
free -m
# =>
total used free shared buff/cache available
Mem:     923  157    8      4        758       747
Swap:      0   0     0
```

<hr>

Linux系统实现虚拟内存有两种方法:

0. 交换分区(swap分区)
0. 交换文件

### 交换文件

#### 创建/root/swapfile文件

```
sudo touch /root/swapfile
```

#### 创建交换文件

```
# if表示input_file输入文件
# of表示output_file输出文件
# bsbs表示block_size块大小,单位为百kb
# count表示计数

# 采用数据块大小为1M,数据块数目为2048,这样分配的空间就是2G大小:
sudo dd if=/dev/zero of=/root/swapfile bs=1M count=2048
```

#### 格式化交换文件

```
sudo mkswap /root/swapfile
```


#### 启用交换文件

```
sudo swapon /root/swapfile
```

#### 检查虚拟内存

```
free -m
```

#### 开机自动加载虚拟内存

在/etc/fstab文件中加入如下命令:

```
sudo tee -a /etc/fstab <<-'EOF'
/root/swapfile swap swap defaults 0 0
EOF
```

重启后生效reboot

#### 停用交换文件

0. 先删除/etc/fstab文件中添加的交换文件行
0. 停用交换文件: swapoff /root/swapfile
0. 删除交换文件: rm -fr /root/swapfile


## 磁盘加密

### 关于LUKS

LUKS(Linux Unified Key Setup)是硬盘加密的标准.<br>
它标准化了分区头以及批量数据的格式.<br>
LUKS可以管理多个密码,这些密码可以有效地撤消,并且可以防止使用PBKDF2进行字典攻击.

### cryptsetup

> cryptsetup是linux下的一个分区加密工具,它通过调用内核中的 dm-crypt 来实现磁盘加密的功能.

#### luksFormat

> 初始化LUKS分区并通过提示或通过密钥文件设置初始密钥.

#### luksOpen

> 用于解密加密设备文件.

```
cryptsetup luksOpen [选项] 设备文件地址 解密后映射名称
```

解密完成后可在 /dev/mapper/ 路径下找到解密后的设备文件

选项|说明
-|-
--key-file|
-keyfile-size|
-readonly|

#### luksSuspend

> 挂起活动设备(冻结了所有IO操作)并从内核擦除加密密钥.

#### luksClose

> 关闭映射,锁定加密卷.

```
cryptsetup luksClose /dev/mapper/映射名称
```

#### luksResume

> 恢复挂起的设备并恢复加密密钥.

#### luksAddKey

> 添加一个新的密钥文件/密码.必须提供现有密码或密钥文件.

```
cryptsetup luksAddKey [选项] 设备文件 [<新密钥文件>]
```

选项|说明
-|-
--key-file|
-keyfile-size|
-new-keyfile-size|
-key-slot|

#### luksRemoveKey

> 从LUKS设备中删除提供的密钥或密钥文件.

#### luksKillSlot

> 从LUKS设备上擦除指定编号的密钥.

#### luksUUID

> 如果设备文件具有LUKS标头,则打印UUID.
如果指定了--uuid选项,则设置新的UUID.

```
cryptsetup luksUUID 设备文件
```

#### luksDump

> 转储LUKS分区的标头信息.

#### luksHeaderBackup

> 存储LUKS header 和keyslot areas的二进制备份.

***使用此备份文件(和旧的密码短语知识),即使从真实设备中删除了旧的密码短语,也可以解密数据.***

#### luksHeaderRestore

从指定的文件LUKS header 和keyslot areas的二进制备份.



# Shell编程

## echo

常用参数|含义
-|-
-n|输出末尾禁止换行
-e|启用反斜杠转义
-E|禁用反斜杠转义(默认)


# Linux系统安全

# 内核管理

## 内核空间(kernel)

> Linux内核(英语:Linux kernel)是一种开源的类Unix操作系统宏内核.<br>
整个Linux操作系统家族基于该内核部署在传统计算机平台.<br>
从技术上说,Linux 只是一个符合POSIX 标准的内核.<br>
它提供了一套应用程序接口(API),通过接口用户程序能与内核及硬件交互.<br>
仅仅一个内核并不是一套完整的操作系统.<br>
有一套基于 Linux 内核的完整操作系统叫作Linux 操作系统,或是GNU/Linux(在该系统中包含了很多GNU 计划的系统组件).


## 用户空间(rootfs)

> 尽管内核是linux的核心,但文件却是用户与操作系统交互所采用的主要工具.<br>
这对linux来说尤其如此,这是因为在UNIX传统中,它使用文件I/O机制管理硬件设备和数据文件.

### 根文件系统

> 根文件系统首先是一种文件系统,该文件系统不仅具有普通文件系统的存储数据文件的功能,<br>
但是相对于普通的文件系统,它的特殊之处在于,它是内核启动时所挂载(mount)的第一个文件系统.<br>
内核代码映像文件保存在根文件系统中,<br>
而系统引导启动程序会在根文件系统挂载之后从中把一些基本的初始化脚本和服务等加载到内存中去运行.

根文件系统包含系统启动时所必须的目录和关键性的文件,<br>
以及使其他文件系统得以挂载(mount)所必要的文件.

例如:

0. init进程的应用程序必须运行在根文件系统上.
0. 根文件系统提供了根目录“/”.
0. linux挂载分区时所依赖的信息存放于根文件系统/etc/fstab这个文件中.
0. shell命令程序必须运行在根文件系统上,譬如ls、cd等命令.

一套linux体系,只有内核本身是不能工作的,必须要rootfs(上的etc目录下的配置文件、/bin /sbin等目录下的shell命令,还有/lib目录下的库文件等···)相配合才能工作.

### 常用目录

根文件系统至少包括以下目录:

0. /etc/:存储重要的配置文件.
0. /bin/:存储常用且开机时必须用到的执行文件.
0. /sbin/:存储着开机过程中所需的系统执行文件.
0. /lib/:存储/bin/及/sbin/的执行文件所需的链接库,以及Linux的内核模块.
0. /dev/:存储设备文件.

五大目录必须存储在根文件系统上,缺一不可.

# 其他

## 终端

### 常用快捷键

快捷键|含义
-|-
C-c|终端进程或命令
C-z|将当前运行程序送到后台执行
C-a|跳转到行首
C-e|跳转到行尾
C-u|清除光标位置到行首内容
C-k|清除光标位置到行尾内容
C-w|清除光标位置到词首内容
C-y|撤销上次清除内容
C-p|查看上一个执行命令
C-n|查看下一个执行命令
C-r|搜索历史命令
C-l|清屏

# 发行版本

## Debian

### Ubuntu

## RedHat

### Yum

> yum(Yellow dog Updater, Modified)是一个在Fedora和RedHat以及SUSE中的Shell前端软件包管理器.<br>
基于RPM包管理,能够从指定的服务器自动下载RPM包并且安装,可以自动处理依赖性关系,并且一次安装所有依赖的软体包,无须繁琐地一次次下载、安装.

#### 列出所有可更新的软件清单命令

```bash
yum check-update
```

#### 更新所有软件命令

```bash
yum update
```

#### 仅安装指定的软件命令

```bash
yum install package_name
```

#### 仅更新指定的软件命令

```bash
yum update package_name
```

#### 列出所有可安裝的软件清单命令

```bash
yum list
```

#### 删除软件包命令

```bash
yum remove package_name
```

#### 查找软件包命令

```bash
yum search keyword
```

#### 清除缓存命令

##### 清除缓存目录下的软件包

```bash
yum clean packages
```

##### 清除缓存目录下的 headers

```bash
yum clean headers
```

##### 清除缓存目录下旧的 headers

```bash
yum clean oldheaders
```

##### 清除缓存目录下的软件包及旧的 headers

```bash
yum clean, yum clean all (= yum clean packages; yum clean oldheaders)
```

### OracleLinux

#### 基本软件包

检查是否安装了适当的基本软件包:

```
rpm -q oraclelinux-release-*
```

如果oraclelinux-release-\<rel>尚未安装,根据 该[文档](https://public-yum.oracle.com/getting-started.html#installing-software-from-oracle-linux-yum-server)说明进行安装.

# 参考资料

> [京东 | Linux从入门到精通+Linux系统管理与网络管理+Shell从入门到精通+Linux Shell命令行及脚本编程](https://item.m.jd.com/product/11827284.html)

> [CSDN | linux 下获得当前目录,上级目录,文件夹名](https://blog.csdn.net/blog_lunatic/article/details/39339581)

> [Bash 4.0 新增的字符串大小写转换表达式](https://www.zybuluo.com/haokuixi/note/77707)

> [Linux中国 | sed 命令详解](https://mp.weixin.qq.com/s?__biz=MjM5NjQ4MjYwMQ==&mid=400341569&idx=3&sn=8febe5e13149dd90f890441a998e136d&mpshare=1&scene=1&srcid=0115ZifbikoWRJptuy8proKL&pass_ticket=IIhQlu%2F%2BGFc2ieUh8wpWDDo2p8dyPmno%2BrdVZWBVQGyuHosZHBALnWZPs5KVEEVz#rd)

> [CSDN | 127.0.0.1和0.0.0.0地址的区别](https://blog.csdn.net/ythunder/article/details/61931080)

> [Linux 公社 | Ubuntu报"xxx is not in the sudoers file.This incident will be reported" 错误解决方法](https://www.linuxidc.com/Linux/2016-07/133066.htm)

> [OSCHINA | EOF 后面的空格](https://my.oschina.net/huayd/blog/137214)

> [Linux 公社 | Ubuntu报"xxx is not in the sudoers file.This incident will be reported" 错误解决方法](https://www.linuxidc.com/Linux/2016-07/133066.htm)

> [Linux中国 | ping 多台服务器并在类似 top 的界面中显示](https://zhuanlan.zhihu.com/p/64915287)

> [CSDN | 在自动化运维中设置apt-get install tzdata的noninteractive方法](https://blog.csdn.net/taiyangdao/article/details/80512997)

> [CSDN | linux ps top 命令 VSZ,RSS,TTY,STAT, VIRT,RES,SHR,DATA的含义](https://blog.csdn.net/zjc156m/article/details/38920321)

> [博客园 | 查看LINUX进程内存占用情况](https://www.cnblogs.com/gaojun/p/3406096.html)

> [简书 | Linux设置虚拟内存](https://www.jianshu.com/p/fae46241ba0c)

> [博客园 | .bash_profile和.bashrc的区别(如何设置生效)](https://www.cnblogs.com/persist/p/5197561.html)

> [云网牛站 | 在Ubuntu 18.04系统中使用Netplan工具配置网络](https://ywnz.com/linuxjc/3280.html)

> [stackoverflow | docker ubuntu /bin/sh: 1: locale-gen: not found](https://stackoverflow.com/questions/39760663/docker-ubuntu-bin-sh-1-locale-gen-not-found)

> [菜鸟YY | Ubuntu 18.04/18.10开启BBR加速](https://www.noobyy.com/1245.html)

> [CSDN | 浅谈linux中的根文件系统(rootfs的原理和介绍)](https://blog.csdn.net/LEON1741/article/details/78159754)

> [维基百科 | Linux内核](https://zh.wikipedia.org/wiki/Linux%E5%86%85%E6%A0%B8)

> [Linux命令大全 | service命令](https://man.linuxde.net/service)

> [博客园 | 磁盘设备在 Linux 下的表示方法](https://www.cnblogs.com/zzy-9318/p/10105010.html)

> [菜鸟教程 | Linux mount命令](https://www.runoob.com/linux/linux-comm-mount.html)

> [stackexchange | How to remove a USB drive without worrying if its been unmounted? [duplicate]](https://unix.stackexchange.com/questions/90657/how-to-remove-a-usb-drive-without-worrying-if-its-been-unmounted/90670#90670)

> [die | cryptsetup(8) - Linux man page](https://linux.die.net/man/8/cryptsetup)

> [DarkSun | 使用cryptsetup创建加密磁盘](http://blog.lujun9972.win/blog/2018/04/12/%E4%BD%BF%E7%94%A8cryptsetup%E5%88%9B%E5%BB%BA%E5%8A%A0%E5%AF%86%E7%A3%81%E7%9B%98/index.html)

> [Linux命令大全 | fuser命令](https://man.linuxde.net/fuser)

> [Linux命令大全 | gzip命令](https://man.linuxde.net/gzip)

> [CSDN | 解压.gz和.tar.gz文件](https://blog.csdn.net/qq_38486203/article/details/80067744)

> [CSDN | Linux: grep多个关键字“与”和“或”](https://blog.csdn.net/mmbbz/article/details/51035401)

> [serverfault | Add a Home directory for already created user when no direct root login available](https://serverfault.com/questions/576354/add-a-home-directory-for-already-created-user-when-no-direct-root-login-availabl)

> [51CTO | Linux服务器上监控网络带宽的18个常用命令和工具](https://blog.51cto.com/lwm666/2450559)

> [掘金 | Linux网络流量监控工具](https://juejin.im/post/6844903974638714887)

> [CSDN | linux磁盘已满,<br>
查看哪个文件占用多](https://blog.csdn.net/a854517900/article/details/80824966)

> [码农家园 | Ubuntu下安装clash](https://www.codenong.com/cs106423935/)

> [Linux使用clash代理 | Naruto210](https://wmc1999.top/2020/08/21/linux-shi-yong-clash-dai-li/)

> [Github | clash](https://github.com/Dreamacro/clash/wiki/configuration)

> [cnblogs | linux环境使用clash实现网络代理访问外网](https://www.cnblogs.com/sueyyyy/p/12424178.html)

> [菜鸟教程 | Linux yum 命令](https://www.runoob.com/linux/linux-yum.html)

> [gtwang | MTR: Linux 網路診斷工具使用教學](https://blog.gtwang.org/linux/mtr-linux-network-diagnostic-tool/)

> [博客园 | 如何用MTR诊断网络问题](https://www.cnblogs.com/xzkzzz/p/7413177.html)

> [commandnotfound | mtr 命令详解](https://commandnotfound.cn/linux/1/463/mtr-%E5%91%BD%E4%BB%A4)

> [维基百科 | 互联网控制消息协议](https://zh.wikipedia.org/wiki/%E4%BA%92%E8%81%94%E7%BD%91%E6%8E%A7%E5%88%B6%E6%B6%88%E6%81%AF%E5%8D%8F%E8%AE%AE)

> [维基百科 | 存活时间](https://zh.wikipedia.org/wiki/%E5%AD%98%E6%B4%BB%E6%99%82%E9%96%93)

>[维基百科 | Network address translation](https://en.wikipedia.org/wiki/Network_address_translation)

> [linoxide | How to Connect to WiFi from Terminal on Ubuntu 20.04](https://linoxide.com/connect-wifi-from-terminal-on-ubuntu-20-04/)

> [opensource | How to find your IP address in Linux](https://opensource.com/article/18/5/how-find-ip-address-linux)

> [mofei12138 | sed替换内容中有斜杠该怎么处理](https://blog.csdn.net/xingjingb/article/details/118075915)