
<!-- TOC -->

- [信息收集](#信息收集)
  - [Nmap](#nmap)
  - [Lynis](#lynis)
  - [Nessus](#nessus)
  - [SocialEngineeringToolkit(SET)](#socialengineeringtoolkitset)
  - [Skipfish](#skipfish)
  - [Maltego](#maltego)
- [漏洞评估](#漏洞评估)
  - [WPScan](#wpscan)
- [Web应用](#web应用)
  - [BeEF](#beef)
  - [Nikto](#nikto)
  - [KingPhisher](#kingphisher)
- [密码攻击](#密码攻击)
  - [JohntheRipper](#johntheripper)
- [漏洞利用](#漏洞利用)
- [网络监听](#网络监听)
  - [Yersinia](#yersinia)
  - [Snort](#snort)
  - [BurpSuiteScanner](#burpsuitescanner)
- [访问维护](#访问维护)
- [报告工具](#报告工具)
  - [MetasploitFramework](#metasploitframework)
- [系统服务](#系统服务)
  - [sqlmap](#sqlmap)
  - [Aircrack-ng](#aircrack-ng)
  - [Wireshark](#wireshark)
- [无线攻击](#无线攻击)
- [逆向工程](#逆向工程)
  - [Apktool](#apktool)
- [压力测试](#压力测试)
- [硬件破解](#硬件破解)
- [法证调查](#法证调查)
  - [AutopsyForensicBrowser](#autopsyforensicbrowser)
- [参考资料](#参考资料)

<!-- /TOC -->

# 信息收集

> 这类工具可用来收集目标的DNS、IDS/IPS、网络扫描、操作系统、路由、SSL、SMB、VPN、VoIP、SNMP信息和E-mail地址.

## Nmap

> Nmap(即“网络映射器(NetworkMapper)”)是KaliLinux上最受欢迎的信息收集工具之一.<br>
换句话说,它可以获取有关主机的信息:其IP地址、操作系统检测以及网络安全的详细信息(如开放的端口数量及其含义).<br>

## Lynis

> Lynis是安全审计、合规性测试和系统强化的强大工具.<br>
也可以将其用于漏洞检测和渗透测试.<br>

它将根据检测到的组件扫描系统.<br>
例如,如果它检测到Apache——它将针对入口信息运行与Apache相关的测试.<br>

它还提供防火墙规避和欺骗功能.<br>

## Nessus

> 如果的计算机连接到了网络,Nessus可以帮助找到潜在攻击者可能利用的漏洞.<br>
如果是多台连接到网络的计算机的管理员,则可以使用它并保护这些计算机.

但是,它不再是免费的工具了,可以从官方网站免费试用7天.<br>

## SocialEngineeringToolkit(SET)

> 如果正在进行相当严格的渗透测试,那么这应该是应该检查的最佳工具之一.<br>
社交工程是一个大问题,使用SET工具,可以帮助防止此类攻击.<br>

## Skipfish

> 与WPScan类似,但它不仅仅专注于WordPress.<br>
Skipfish是一个Web应用扫描程序,可以为提供几乎所有类型的Web应用程序的洞察信息.<br>
它快速且易于使用.<br>
此外,它的递归爬取方法使它更好用.

Skipfish生成的报告可以用于专业的Web应用程序安全评估.

## Maltego

Maltego是一种令人印象深刻的数据挖掘工具,用于在线分析信息并连接信息点(如果有的话).<br>
根据这些信息,它创建了一个有向图,以帮助分析这些数据之间的链接.<br>

它已预装,但必须注册才能选择要使用的版本.<br>
如果个人使用,社区版就足够了(只需要注册一个帐户),但如果想用于商业用途,则需要订阅classic或XL版本.

# 漏洞评估

> 这类工具都可以扫描目标系统上的漏洞.<br>
部分工具可以检测Cisco网络系统缺陷,有些还可以评估各种数据库系统的安全问题.<br>
很多模糊测试软件都属于漏洞评估工具.

## WPScan

> WordPress是最好的开源CMS之一,而这个工具是最好的免费WordpPress安全审计工具.<br>
它是免费的,但不是开源的.<br>

如果想知道一个WordPress博客是否在某种程度上容易受到攻击,WPScan就是的朋友.<br>

此外,它还为提供了所用的插件的详细信息.<br>
一个安全性很好的博客可能不会暴露给很多细节,但它仍然是WordPress安全扫描找到潜在漏洞的最佳工具.<br>

# Web应用

> 即与Web应用有关的工具.<br>
它包括CMS(内容管理系统)扫描器、数据库漏洞利用程序、Web应用模糊测试、Web应用代理、Web爬虫及Web漏洞扫描器.

## BeEF

> BeEF(浏览器利用框架(BrowserExploitationFramework))是另一个令人印象深刻的工具.<br>
它专为渗透测试人员量身定制,用于评估Web浏览器的安全性.<br>

这是最好的KaliLinux工具之一,因为很多用户在谈论Web安全时希望了解并修复客户端的问题.<br>

## Nikto

> Nikto是一款功能强大的Web服务器扫描程序——这使其成为最好的KaliLinux工具之一.<br>
它会检查存在潜在危险的文件/程序、过时的服务器版本等等.<br>

## KingPhisher

> 网络钓鱼攻击现在非常普遍.<br>
KingPhisher工具可以通过模拟真实的网络钓鱼攻击来帮助测试和提升用户意识.<br>
出于显而易见的原因,在模拟一个组织的服务器内容前,需要获得许可.

# 密码攻击

> 无论是在线攻击还是离线破解,只要是能够实施密码攻击的工具都属于密码攻击类工具.

## JohntheRipper

> JohntheRipper是KaliLinux上流行的密码破解工具.<br>
它也是自由开源的.<br>
但是,如果对社区增强版不感兴趣,可以用于商业用途的专业版.<br>

# 漏洞利用

> 这类工具可以利用在目标系统中发现的漏洞.<br>
攻击网络、Web和数据库漏洞的软件,都属于漏洞利用(exploitation)工具.<br>
Kali?中的某些软件可以针对漏洞情况进行社会工程学攻击.

# 网络监听

> 这类工具用于监听网络和Web流量.<br>
网络监听需要进行网络欺骗,所以Ettercap和Yersinia这类软件也归于这类软件.

## Yersinia

> Yersinia是一个有趣的框架,用于在网络上执行第2层攻击(第2层是指OSI模型的数据链路层).<br>
如果希望的网络安全,则必须考虑所有七个层.<br>
但是,此工具侧重于第2层和各种网络协议,包括STP、CDP,DTP等.<br>

## Snort

> 即使它是一个开源的入侵防御系统,也提供实时流量分析和数据包记录功能.

## BurpSuiteScanner

> BurpSuiteScanner是一款出色的网络安全分析工具.<br>
与其它Web应用程序安全扫描程序不同,Burp提供了GUI和一些高级工具.

社区版仅将功能限制为一些基本的手动工具.<br>
对于专业人士,必须考虑升级.<br>
与前面的工具类似,这也不是开源的.

# 访问维护

> 这类工具帮助渗透人员维持他们对目标主机的访问权.<br>
某些情况下,渗透人员必须先获取主机的最高权限才能安装这类软件.<br>
这类软件包括用于在Web应用和操作系统安装后门的程序,以及隧道类工具.

# 报告工具

> 如果您需要撰写渗透测试的报告文件,您应该用得上这些软件.

## MetasploitFramework

> MetsploitFramework(MSF)是最常用的渗透测试框架.<br>
它提供两个版本:一个开源版,另外一个是其专业版.<br>
使用此工具,可以验证漏洞、测试已知漏洞并执行完整的安全评估.<br>

# 系统服务

> 这是渗透人员在渗透测试时可能用到的常见服务类软件,它包括Apache服务、MySQL服务、SSH服务和Metasploit服务.<br>

为了降低渗透测试人员筛选工具的难度,Kali Linux 单独划分了一类软件－Top 10 Security Tools,即10大首选安全工具.<br>
这10大工具分别是aircrack-ng、burp-suite、hydra、john、maltego、metasploit、nmap、sqlmap、wireshark和zaproxy.<br>

除了可用于渗透测试的各种工具以外,Kali Linux还整合了以下几类工具.

## sqlmap

> 如果正在寻找一个开源渗透测试工具——sqlmap是最好的之一.<br>
它可以自动化利用SQL注入漏洞的过程,并帮助接管数据库服务器.<br>

## Aircrack-ng

> Aircrack-ng是评估WiFi网络安全性的工具集合.<br>
它不仅限于监控和获取信息——还包括破坏网络(WEP、WPA1和WPA2)的能力.<br>

如果忘记了自己的WiFi网络的密码,可以尝试使用它来重新获得访问权限.<br>
它还包括各种无线攻击能力,可以使用它们来定位和监控WiFi网络以增强其安全性.<br>

## Wireshark

> Wireshark是KaliLinux上最受欢迎的网络分析仪.<br>
它也可以归类为用于网络嗅探的最佳KaliLinux工具之一.<br>

# 无线攻击

> 可攻击蓝牙、RFID／NFC和其他无线设备的工具.

# 逆向工程

> 可用于调试程序或反汇编的工具.

## Apktool

> Apktool确实是KaliLinux上用于逆向工程Android应用程序的流行工具之一.<br>
应该正确利用它——出于教育目的.<br>

# 压力测试

> 用于各类压力测试的工具集.<br>
它们可测试网络、无线、Web和VoIP系统的负载能力.

# 硬件破解

> 用于调试Android和Arduino程序的工具.

# 法证调查

> 即电子取证的工具.<br>
它的各种工具可以用于制作硬盘磁盘镜像、文件分析、硬盘镜像分析.<br>
如需使用这类程序,首先要在启动菜单里选择Kali Linux Forensics | No Drives or Swap Mount.<br>
在开启这个选项以后,Kali Linux不会自动加载硬盘驱动器,以保护硬盘数据的完整性.

## AutopsyForensicBrowser

> Autopsy是一个数字取证工具,用于调查计算机上发生的事情.<br>
也可以使用它从SD卡恢复图像.

#参考资料

>[Linux中国|用于黑客渗透测试的21个最佳KaliLinux工具](https://zhuanlan.zhihu.com/p/65933902)


> [博客园 | Kali Linux工具分类](https://www.cnblogs.com/RoseKing/p/7275297.html)
