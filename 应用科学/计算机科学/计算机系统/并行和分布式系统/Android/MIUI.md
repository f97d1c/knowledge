
# 常见问题

## 无法登录Google账号

目前(2020-06-12), MIUI中会搭载Google服务套装, 不再需要使用Google安装器去修复依赖, 只不过是默认没有启用,需要进入:

设置-应用设置-应用管理, 搜索google, 点击进去停止禁用即可.

![](https://www.vediotalk.com/wp-content/uploads/2020/02/9-473x1024.jpeg)

# 参考资料

> [vediotalk | MIUI 11 |MIUI 11小米手机通谷歌服务google play](https://www.vediotalk.com/archives/9271)