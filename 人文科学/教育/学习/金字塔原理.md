> 《金字塔原理》这本书，为了理解并运用这个思维方式，作者从表达的逻辑、思考的逻辑、解决问题的逻辑、演示的逻辑这四个方面来进行拆分并阐述。

![](https://pic2.zhimg.com/v2-60dc50c20f7b31b708ac2acf26583882_r.jpg)

<!-- TOC -->

- [表达的逻辑](#表达的逻辑)
  - [基本原则](#基本原则)
    - [结论先行](#结论先行)
    - [以上统下](#以上统下)
    - [归类分组](#归类分组)
    - [逻辑递进](#逻辑递进)
  - [内部逻辑关系](#内部逻辑关系)
    - [序言](#序言)
      - [说明背景](#说明背景)
      - [指出冲突](#指出冲突)
      - [引发疑问](#引发疑问)
      - [给出答案](#给出答案)
    - [纵向相关](#纵向相关)
    - [横向相关](#横向相关)
      - [演绎推理](#演绎推理)
        - [三段论](#三段论)
      - [归纳推理](#归纳推理)
        - [MECE原则](#mece原则)
          - [相互独立 相互排斥](#相互独立-相互排斥)
          - [完全穷尽 没有遗漏](#完全穷尽-没有遗漏)
  - [如何构建金字塔结构](#如何构建金字塔结构)
    - [自上而下](#自上而下)
      - [提出主题思想](#提出主题思想)
      - [设想受众的主要疑问](#设想受众的主要疑问)
      - [写序言: 背景-冲突-疑问-回答](#写序言-背景-冲突-疑问-回答)
      - [与受众进行Q&A式对话](#与受众进行qa式对话)
      - [对受众新的疑问重复Q&A式对话](#对受众新的疑问重复qa式对话)
    - [自下而上](#自下而上)
      - [尽可能列出所有思考的要点](#尽可能列出所有思考的要点)
      - [找出关系进行分类](#找出关系进行分类)
      - [总结概括要点,提炼观点](#总结概括要点提炼观点)
      - [观点补充,完善思想](#观点补充完善思想)
- [思考的逻辑](#思考的逻辑)
  - [应用逻辑顺序](#应用逻辑顺序)
    - [时间/步骤](#时间步骤)
    - [空间/结构](#空间结构)
    - [程度/重要性](#程度重要性)
  - [概括各组思想](#概括各组思想)
    - [避免使用 ***缺乏思想*** 的句子](#避免使用-缺乏思想-的句子)
    - [说明行动产生的结果/影响](#说明行动产生的结果影响)
    - [找出结论之间的共性](#找出结论之间的共性)
- [解决问题的逻辑](#解决问题的逻辑)
  - [界定问题](#界定问题)
    - [设想问题产生的领域](#设想问题产生的领域)
    - [什么事情的发生打乱了该领域的稳定(困扰/困惑)](#什么事情的发生打乱了该领域的稳定困扰困惑)
    - [确定非期待结果(现状 R<sub>1</sub>)](#确定非期待结果现状-rsub1sub)
    - [确定期待结果(目标 R<sub>2</sub>)](#确定期待结果目标-rsub2sub)
    - [确定是否已经采取了解决问题的行动](#确定是否已经采取了解决问题的行动)
    - [确定分析所要回答的疑问](#确定分析所要回答的疑问)
  - [结构化分析问题](#结构化分析问题)
    - [从信息资料入手](#从信息资料入手)
      - [提出各种假设](#提出各种假设)
      - [设立多个子主题并根据结果进行排除](#设立多个子主题并根据结果进行排除)
      - [通过实验得出明确结论](#通过实验得出明确结论)
      - [相应的采取补救措施](#相应的采取补救措施)
    - [设计诊断框架](#设计诊断框架)
    - [使用诊断框架](#使用诊断框架)
    - [建立逻辑树](#建立逻辑树)
      - [是否有问题](#是否有问题)
      - [问题在哪里](#问题在哪里)
      - [为什么存在](#为什么存在)
      - [能做什么](#能做什么)
      - [应该怎么做](#应该怎么做)
    - [是非问题分析](#是非问题分析)
- [演示的逻辑](#演示的逻辑)
  - [书面上](#书面上)
    - [突出显示文章的框架结构](#突出显示文章的框架结构)
    - [上下文之间要有过渡](#上下文之间要有过渡)
  - [PPT中](#ppt中)
    - [设计文字幻灯片](#设计文字幻灯片)
    - [设计图标幻灯片](#设计图标幻灯片)
    - [故事梗概](#故事梗概)
  - [字里行间](#字里行间)
    - [画脑图](#画脑图)
    - [把图像复制成文字](#把图像复制成文字)
- [参考资料](#参考资料)

<!-- /TOC -->

# 表达的逻辑

## 基本原则

### 结论先行

### 以上统下

### 归类分组

### 逻辑递进


## 内部逻辑关系

### 序言

#### 说明背景
#### 指出冲突
#### 引发疑问
#### 给出答案
### 纵向相关

### 横向相关

#### 演绎推理

##### 三段论
#### 归纳推理

##### MECE原则
###### 相互独立 相互排斥
###### 完全穷尽 没有遗漏


## 如何构建金字塔结构

### 自上而下

#### 提出主题思想
#### 设想受众的主要疑问
#### 写序言: 背景-冲突-疑问-回答
#### 与受众进行Q&A式对话 
#### 对受众新的疑问重复Q&A式对话

### 自下而上

#### 尽可能列出所有思考的要点
#### 找出关系进行分类
#### 总结概括要点,提炼观点
#### 观点补充,完善思想


# 思考的逻辑

## 应用逻辑顺序

### 时间/步骤

### 空间/结构

### 程度/重要性


## 概括各组思想

### 避免使用 ***缺乏思想*** 的句子

### 说明行动产生的结果/影响

### 找出结论之间的共性

# 解决问题的逻辑

## 界定问题

### 设想问题产生的领域

### 什么事情的发生打乱了该领域的稳定(困扰/困惑)

### 确定非期待结果(现状 R<sub>1</sub>)

### 确定期待结果(目标 R<sub>2</sub>)

### 确定是否已经采取了解决问题的行动

### 确定分析所要回答的疑问


## 结构化分析问题

### 从信息资料入手

#### 提出各种假设
#### 设立多个子主题并根据结果进行排除
#### 通过实验得出明确结论
#### 相应的采取补救措施
### 设计诊断框架

### 使用诊断框架

### 建立逻辑树

#### 是否有问题
#### 问题在哪里
#### 为什么存在
#### 能做什么
#### 应该怎么做

### 是非问题分析

# 演示的逻辑

## 书面上

### 突出显示文章的框架结构

### 上下文之间要有过渡


## PPT中

### 设计文字幻灯片

### 设计图标幻灯片

### 故事梗概


## 字里行间

### 画脑图

### 把图像复制成文字




# 参考资料

> [XMind思维导图 | 什么是费曼技巧？](https://www.zhihu.com/question/20585936/answer/632981745)
