#!/bin/bash --login

ruby .代码库/.ruby.rb $PWD

if [[ "$1" == "push" ]];then
    find . -type f -name ".*.json" -exec git add {} \;

    tree | head -n -2 | sed 's/[0-9]\+-//g' > .体系结构.txt
    find . -type f -name ".*.txt" -exec git add {} \;

    git commit -m "$(date '+%Y-%m-%d %H:%M:%S.%3N')"
    git push
fi
