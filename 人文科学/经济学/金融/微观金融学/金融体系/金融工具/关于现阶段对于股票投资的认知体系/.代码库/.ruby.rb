require 'json'
require 'pry'

unless ARGV[0]
  print "$1: 目标路径不能为空"
  exit 255
end

目标目录 = ARGV[0]
ARGV[0] = {}
ARGV[0]['目标目录'] = 目标目录
ARGV[0]['原始目录结构'] = {}
ARGV[0]['格式化后结构'] = {}


递归处理目录树 = lambda do |对象, 结果, 路径=[]|
  next if 对象['name'] == '.gitkeep'
  格式化名称 = 对象['name'].gsub(/\d|\-|\./, '')

  if 对象['type'] == 'directory' && (对象['contents']||[]).length > 0
    对象['contents'].each do |content|
    递归处理目录树.call(content, (结果[格式化名称] ||= {}), [路径, 对象['name']].flatten)
    end
  elsif (对象['type'] == 'directory')
    结果[格式化名称] = {}
  elsif 对象['type'] == 'file'
    文件路径 = [路径, 对象['name']].flatten.join('/')
    content = File.open(文件路径){|file| file.read}
    File.open(文件路径, 'w'){|file| file.write((JSON.pretty_generate(JSON.parse(content)) rescue content))}
    结果[格式化名称] = content
  elsif 对象['type'] == 'link'
    # 结果[对象['name']] = 对象['target']
  end
end

Dir["#{目标目录}/.代码库/**/*.rb"].sort.each do |filename|
  require filename
end