
递归处理结果 = lambda do |对象, 路径=[]|

  if 对象.is_a?(Hash)
    对象.each do |key, value|
      递归处理结果.call(value, [路径, key].flatten)
    end
  else
    next if (JSON.parse(对象) rescue false)
    next if 对象.include?('"')
    next unless 对象.split("\n").size > 1

    File.open(路径.join('/'), 'w'){|f| f.write(JSON.pretty_generate(对象.split("\n")))}
  end
end

递归处理结果.call(ARGV[0]['原始目录结构'])
