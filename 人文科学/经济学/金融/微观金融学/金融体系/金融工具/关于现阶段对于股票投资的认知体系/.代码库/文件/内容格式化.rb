
体系结构分类 = {}
递归处理结果 = lambda do |对象, 结果, 路径=[]|
  if 对象.is_a?(Hash)
    对象.each do |key, value|
      递归处理结果.call(value, 结果, [路径, key].flatten)
    end
  else
    结果[路径[-1]] ||= {}
    结果[路径[-1]][路径[-2]] = (JSON.parse(对象) rescue 对象)
  end
end

递归处理结果.call(ARGV[0]['格式化后结构'], 体系结构分类)

体系结构分类.each do |分类名, 内容|
  File.open("#{ARGV[0]['目标目录']}/.#{分类名}.json", 'w'){|f| f.write(JSON.pretty_generate(内容))}
end