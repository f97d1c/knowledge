递归处理目录树 = lambda do |对象, 路径|
  return unless 对象['type'] == 'directory' && (对象['contents']||[]).length > 0
  return unless 对象['contents'].map{|content| content['type']}.include?('directory')
  
  对象['contents'].select{|content| content['type'] == 'directory' && /^\d{2,}\-.*/ =~ content['name']}.each_with_index do |content, 下标|
    实际名称 = content['name']
    理论名称 = [sprintf('%02d', 下标+1), 实际名称.split('-')[1]].join('-')
    next if 实际名称 == 理论名称
    content['name'] = 理论名称
    `mv "#{[路径, 实际名称].flatten.join('/')}" "#{[路径, 理论名称].flatten.join('/')}"`
  end

  对象['contents'].each do |content|
    递归处理目录树.call(content, [路径, content['name']].flatten)
  end
end


Dir["#{ARGV[0]['目标目录']}/*"].each do |全路径|
  next unless File::directory?(全路径)
  递归处理目录树.call(JSON.parse(`tree -a -J #{全路径}`)[0], 全路径.split('/'))
end
