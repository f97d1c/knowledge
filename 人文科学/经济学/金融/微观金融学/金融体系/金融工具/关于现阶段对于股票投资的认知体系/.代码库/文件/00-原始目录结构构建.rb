
递归处理目录树 = lambda do |对象, 结果, 路径=[]|
  next if 对象['name'] == '.gitkeep'
  if 对象['type'] == 'directory' && (对象['contents']||[]).length > 0
    对象['contents'].each do |content|
    递归处理目录树.call(content, (结果[对象['name']] ||= {}), [路径, 对象['name']].flatten)
    end
  elsif (对象['type'] == 'directory')
    结果[对象['name']] = {}
  elsif 对象['type'] == 'file'
    文件路径 = [路径, 对象['name']].flatten.join('/')
    content = File.open(文件路径){|file| file.read}
    结果[对象['name']] = content
  elsif 对象['type'] == 'link'
    # 结果[对象['name']] = 对象['target']
  end
end

Dir["#{ARGV[0]['目标目录']}/*"].each do |全路径|
  next unless File::directory?(全路径)
  目录树数据 = JSON.parse(`tree -a -J #{全路径}`)[0]
  result = {}
  递归处理目录树.call(目录树数据, result)
  ARGV[0]['原始目录结构'][全路径] = result.values[0]
end
