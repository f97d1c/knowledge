
递归处理结果 = lambda do |对象, 结果, 路径=[]|
  对象.each do |key, value|
    if /^\d{2,}\-.*/ =~ key
      格式化名称 = key.gsub(/\d|\-/, '')
    elsif /^\..*/ =~ key
      格式化名称 = key.gsub('.', '')
    else
      格式化名称 = key
    end
    if value.is_a?(Hash)
      递归处理结果.call(value, (结果[格式化名称]||={}), [路径, 格式化名称].flatten)
    else
      结果[格式化名称] = value
    end
  end
end

ARGV[0]['原始目录结构'].each do |全路径, 树结构|
  result = {}
  递归处理结果.call(树结构, result)
  ARGV[0]['格式化后结构'][全路径.split('/')[-1].split('-')[-1]] = result
end