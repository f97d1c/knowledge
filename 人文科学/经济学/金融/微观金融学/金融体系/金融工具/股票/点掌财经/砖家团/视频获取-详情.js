// 该代码需在详情页面的console里面执行
// index_result取自 视频获取-详情.json

let index_result = {
  "页面链接": "https://zjt.aniu.tv/experts_index_type_4_eid_288656_p_1.shtml",
  "结果集": [
    [
      {
        "innerHTML": "薛松：切勿被情绪左右！被他人操控！",
        "href": "https://v.aniu.tv/video/play/id/102027/t/2.shtml"
      },
      {
        "innerHTML": "薛松：基建方向会是长久之计吗？",
        "href": "https://v.aniu.tv/video/play/id/102028/t/2.shtml"
      },
      {
        "innerHTML": "薛松：影响煤炭表现的重要因素有哪些？",
        "href": "https://v.aniu.tv/video/play/id/102029/t/2.shtml"
      },
      {
        "innerHTML": "薛松：只要你相信，市场就没问题",
        "href": "https://v.aniu.tv/video/play/id/94020/t/2.shtml"
      },
      {
        "innerHTML": "薛松：交易的关键是你如何应对风险？",
        "href": "https://v.aniu.tv/video/play/id/94101/t/2.shtml"
      },
      {
        "innerHTML": "薛松：3倍标准差的布林通道有何用？",
        "href": "https://v.aniu.tv/video/play/id/94102/t/2.shtml"
      },
      {
        "innerHTML": "薛松：市场恐慌往往以理性的形式传播",
        "href": "https://v.aniu.tv/video/play/id/94109/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这个位置坚定做多！",
        "href": "https://v.aniu.tv/video/play/id/93265/t/2.shtml"
      },
      {
        "innerHTML": "薛松：依赖他人的交易系统，只有死路一条",
        "href": "https://v.aniu.tv/video/play/id/93146/t/2.shtml"
      },
      {
        "innerHTML": "薛松：情绪指标分析，市场将见低点",
        "href": "https://v.aniu.tv/video/play/id/93148/t/2.shtml"
      },
      {
        "innerHTML": "薛松：资金抄底港股，不利于A股止跌",
        "href": "https://v.aniu.tv/video/play/id/93149/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美元吸血全球，南向资金抄底港股",
        "href": "https://v.aniu.tv/video/play/id/93150/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不要误解指数与你手中个股的关系",
        "href": "https://v.aniu.tv/video/play/id/92373/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美元是如何收割世界的？",
        "href": "https://v.aniu.tv/video/play/id/92440/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美国加息之前，市场依旧反复",
        "href": "https://v.aniu.tv/video/play/id/92444/t/2.shtml"
      },
      {
        "innerHTML": "薛松：市场以反复为主",
        "href": "https://v.aniu.tv/video/play/id/92229/t/2.shtml"
      },
      {
        "innerHTML": "薛松：控制好仓位，才能控制好心态",
        "href": "https://v.aniu.tv/video/play/id/92296/t/2.shtml"
      },
      {
        "innerHTML": "薛松：次新这样理解",
        "href": "https://v.aniu.tv/video/play/id/90741/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这才是估值特征",
        "href": "https://v.aniu.tv/video/play/id/90742/t/2.shtml"
      },
      {
        "innerHTML": "薛松：几乎所有A股参与者都不知道的事情！",
        "href": "https://v.aniu.tv/video/play/id/90743/t/2.shtml"
      },
      {
        "innerHTML": "薛松：股市的钱赚不完，适合自己最重要",
        "href": "https://v.aniu.tv/video/play/id/90624/t/2.shtml"
      },
      {
        "innerHTML": "薛松：上证还在反弹周期中，下周更精彩！",
        "href": "https://v.aniu.tv/video/play/id/90034/t/2.shtml"
      },
      {
        "innerHTML": "薛松：看空之三大做空力量",
        "href": "https://v.aniu.tv/video/play/id/90035/t/2.shtml"
      },
      {
        "innerHTML": "薛松：坚持下来的都是对的！",
        "href": "https://v.aniu.tv/video/play/id/89375/t/2.shtml"
      },
      {
        "innerHTML": "薛松：抄底前有必要条件",
        "href": "https://v.aniu.tv/video/play/id/89259/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不怕套套不怕怕不套！",
        "href": "https://v.aniu.tv/video/play/id/89284/t/2.shtml"
      },
      {
        "innerHTML": "薛松：谈谈公告那些事，套路真不少！",
        "href": "https://v.aniu.tv/video/play/id/88777/t/2.shtml"
      },
      {
        "innerHTML": "薛松：抄底这件事急不得！",
        "href": "https://v.aniu.tv/video/play/id/88779/t/2.shtml"
      },
      {
        "innerHTML": "薛松：市场涨跌才是常态，勿用个人预期绑架市场",
        "href": "https://v.aniu.tv/video/play/id/88780/t/2.shtml"
      },
      {
        "innerHTML": "薛松：九安这辈子到头了！",
        "href": "https://v.aniu.tv/video/play/id/88811/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这样应对人民币贬值风险！",
        "href": "https://v.aniu.tv/video/play/id/88669/t/2.shtml"
      },
      {
        "innerHTML": "薛松：忙者不会，会者不忙",
        "href": "https://v.aniu.tv/video/play/id/88671/t/2.shtml"
      },
      {
        "innerHTML": "薛松：刻意找低点？练就集齐所有退市股能力！",
        "href": "https://v.aniu.tv/video/play/id/88672/t/2.shtml"
      },
      {
        "innerHTML": "薛松：想抄底的人从来抄不到底！",
        "href": "https://v.aniu.tv/video/play/id/90860/t/2.shtml"
      },
      {
        "innerHTML": "薛松：解读人民币贬值风险！",
        "href": "https://v.aniu.tv/video/play/id/90859/t/2.shtml"
      },
      {
        "innerHTML": "薛松：缩量肯定不是上涨趋势！",
        "href": "https://v.aniu.tv/video/play/id/88179/t/2.shtml"
      },
      {
        "innerHTML": "薛松：节奏不同打法不同，买入持有最废策略之一！",
        "href": "https://v.aniu.tv/video/play/id/88181/t/2.shtml"
      },
      {
        "innerHTML": "薛松：港股互联网底部放量，捡筹码！",
        "href": "https://v.aniu.tv/video/play/id/88182/t/2.shtml"
      },
      {
        "innerHTML": "薛松：拳头力量再大，打不到人没用！",
        "href": "https://v.aniu.tv/video/play/id/88184/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：多观察，找不会骗你的人",
        "href": "https://v.aniu.tv/video/play/id/80694/t/2.shtml"
      },
      {
        "innerHTML": "薛松：没有太大泡沫的行业",
        "href": "https://v.aniu.tv/video/play/id/80770/t/2.shtml"
      },
      {
        "innerHTML": "薛松：看到不利因素顶着的人可以赚大钱",
        "href": "https://v.aniu.tv/video/play/id/80775/t/2.shtml"
      },
      {
        "innerHTML": "薛松：要把赢钱的预期降下来",
        "href": "https://v.aniu.tv/video/play/id/80544/t/2.shtml"
      },
      {
        "innerHTML": "薛松：市场还是蛮低迷的！",
        "href": "https://v.aniu.tv/video/play/id/80555/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你赢不了钱，是因为你是好人",
        "href": "https://v.aniu.tv/video/play/id/80558/t/2.shtml"
      },
      {
        "innerHTML": "薛松：T+1日内助涨！",
        "href": "https://v.aniu.tv/video/play/id/79895/t/2.shtml"
      },
      {
        "innerHTML": "薛松：今天盘面很弱，一点也不强！",
        "href": "https://v.aniu.tv/video/play/id/79911/t/2.shtml"
      },
      {
        "innerHTML": "薛松：做指数就是不要弹性！",
        "href": "https://v.aniu.tv/video/play/id/79913/t/2.shtml"
      },
      {
        "innerHTML": "薛松：金融讲穿了就是债务",
        "href": "https://v.aniu.tv/video/play/id/79936/t/2.shtml"
      },
      {
        "innerHTML": "薛松：防止资本在债务端的无序扩张",
        "href": "https://v.aniu.tv/video/play/id/79937/t/2.shtml"
      },
      {
        "innerHTML": "薛松：银根收紧，什么泡沫都没了",
        "href": "https://v.aniu.tv/video/play/id/79938/t/2.shtml"
      },
      {
        "innerHTML": "薛松：下注少一点！",
        "href": "https://v.aniu.tv/video/play/id/79746/t/2.shtml"
      },
      {
        "innerHTML": "薛松：顺价格传导机制，往制造业走",
        "href": "https://v.aniu.tv/video/play/id/79757/t/2.shtml"
      },
      {
        "innerHTML": "薛松：券商是风险最小的机构",
        "href": "https://v.aniu.tv/video/play/id/79775/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美元指数下来，总有东西要上去",
        "href": "https://v.aniu.tv/video/play/id/79797/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不太懂恶意炒作和善意炒作",
        "href": "https://v.aniu.tv/video/play/id/79798/t/2.shtml"
      },
      {
        "innerHTML": "薛松：三季度看三季报没多大意义",
        "href": "https://v.aniu.tv/video/play/id/79810/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数可能到上影线附近！",
        "href": "https://v.aniu.tv/video/play/id/79098/t/2.shtml"
      },
      {
        "innerHTML": "薛松：九月上影线有量，十月上影线没量！",
        "href": "https://v.aniu.tv/video/play/id/79097/t/2.shtml"
      },
      {
        "innerHTML": "薛松：周期股股价最高的时候，市盈率最低",
        "href": "https://v.aniu.tv/video/play/id/79113/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不要拿港股的估值说事！",
        "href": "https://v.aniu.tv/video/play/id/79139/t/2.shtml"
      },
      {
        "innerHTML": "薛松：人无我有，人有我新，人新我转",
        "href": "https://v.aniu.tv/video/play/id/79165/t/2.shtml"
      },
      {
        "innerHTML": "薛松：滞胀阶段的布局！",
        "href": "https://v.aniu.tv/video/play/id/79166/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数出现极端走势，缓一缓才是高手",
        "href": "https://v.aniu.tv/video/play/id/78941/t/2.shtml"
      },
      {
        "innerHTML": "薛松：好的操盘手一定是顺势操盘的！",
        "href": "https://v.aniu.tv/video/play/id/78942/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为低买点留资金",
        "href": "https://v.aniu.tv/video/play/id/78944/t/2.shtml"
      },
      {
        "innerHTML": "薛松：近期利多低位品种",
        "href": "https://v.aniu.tv/video/play/id/78945/t/2.shtml"
      },
      {
        "innerHTML": "薛松：套牢的人怕踏空，踏空的人怕套牢",
        "href": "https://v.aniu.tv/video/play/id/78984/t/2.shtml"
      },
      {
        "innerHTML": "薛松：市场是投机赌博心态！",
        "href": "https://v.aniu.tv/video/play/id/79002/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数是趋势，个股是震荡",
        "href": "https://v.aniu.tv/video/play/id/78170/t/2.shtml"
      },
      {
        "innerHTML": "薛松：跌下来没人怕是不正常的",
        "href": "https://v.aniu.tv/video/play/id/78176/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数缩量没有共振，很难判断",
        "href": "https://v.aniu.tv/video/play/id/78058/t/2.shtml"
      },
      {
        "innerHTML": "薛松：就怕散户胆子大！",
        "href": "https://v.aniu.tv/video/play/id/78065/t/2.shtml"
      },
      {
        "innerHTML": "薛松：按自己节奏交易，问指数只是心理咨询",
        "href": "https://v.aniu.tv/video/play/id/77331/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你敢低吸那就腰斩，你敢高抛那就踏空",
        "href": "https://v.aniu.tv/video/play/id/77358/t/2.shtml"
      },
      {
        "innerHTML": "薛松：反弹越早，越往下掉",
        "href": "https://v.aniu.tv/video/play/id/76740/t/2.shtml"
      },
      {
        "innerHTML": "薛松：日线指标易造假 但时间难造假",
        "href": "https://v.aniu.tv/video/play/id/76741/t/2.shtml"
      },
      {
        "innerHTML": "薛松：机关算尽太聪明,反误了卿卿性命",
        "href": "https://v.aniu.tv/video/play/id/76742/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：国内乳业没有核心竞争力！",
        "href": "https://v.aniu.tv/video/play/id/88185/t/2.shtml"
      },
      {
        "innerHTML": "薛松：当下不要谨慎，重拳出击！",
        "href": "https://v.aniu.tv/video/play/id/88074/t/2.shtml"
      },
      {
        "innerHTML": "薛松：天天满仓，妥妥的韭菜行为！",
        "href": "https://v.aniu.tv/video/play/id/88099/t/2.shtml"
      },
      {
        "innerHTML": "薛松：风口上猪都能飞，就别谈基本面了！",
        "href": "https://v.aniu.tv/video/play/id/88100/t/2.shtml"
      },
      {
        "innerHTML": "薛松：想抄底的人从来抄不到底！",
        "href": "https://v.aniu.tv/video/play/id/87648/t/2.shtml"
      },
      {
        "innerHTML": "薛松：浅析公司负债影响",
        "href": "https://v.aniu.tv/video/play/id/87653/t/2.shtml"
      },
      {
        "innerHTML": "薛松：敢于说“我不会”的才是高人！",
        "href": "https://v.aniu.tv/video/play/id/87654/t/2.shtml"
      },
      {
        "innerHTML": "薛松：辅助线这么看，输的慢一点！",
        "href": "https://v.aniu.tv/video/play/id/87655/t/2.shtml"
      },
      {
        "innerHTML": "薛松：给你便宜你不敢要，这才是底！",
        "href": "https://v.aniu.tv/video/play/id/87664/t/2.shtml"
      },
      {
        "innerHTML": "薛松：一脚踩下去不是底，你慌不慌？",
        "href": "https://v.aniu.tv/video/play/id/87524/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这个月存在超级机会，问就是没到！",
        "href": "https://v.aniu.tv/video/play/id/87526/t/2.shtml"
      },
      {
        "innerHTML": "薛松：牌不好就别入场，赢人前先赢牌！",
        "href": "https://v.aniu.tv/video/play/id/87528/t/2.shtml"
      },
      {
        "innerHTML": "薛松：别问牛市何时来，把握节奏才是王道！",
        "href": "https://v.aniu.tv/video/play/id/87529/t/2.shtml"
      },
      {
        "innerHTML": "薛松：炒作就看4个字，知识无用论！",
        "href": "https://v.aniu.tv/video/play/id/87544/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你真的为恐慌留过资金吗？",
        "href": "https://v.aniu.tv/video/play/id/87084/t/2.shtml"
      },
      {
        "innerHTML": "薛松：发现“浪”是自学习",
        "href": "https://v.aniu.tv/video/play/id/87087/t/2.shtml"
      },
      {
        "innerHTML": "薛松：赢了上半场未必能赢下半场！",
        "href": "https://v.aniu.tv/video/play/id/86967/t/2.shtml"
      },
      {
        "innerHTML": "薛松：本能的不安全感在逆境中会有很多误判",
        "href": "https://v.aniu.tv/video/play/id/86968/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这就是妖股风格！",
        "href": "https://v.aniu.tv/video/play/id/86969/t/2.shtml"
      },
      {
        "innerHTML": "薛松：没有出现量价大背离，很正常！",
        "href": "https://v.aniu.tv/video/play/id/86428/t/2.shtml"
      },
      {
        "innerHTML": "薛松：天时地利人和，底部有整理形态！",
        "href": "https://v.aniu.tv/video/play/id/86429/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数型行情，20日均线是买点机会！",
        "href": "https://v.aniu.tv/video/play/id/85819/t/2.shtml"
      },
      {
        "innerHTML": "薛松：昨天医药的事情有外力作用",
        "href": "https://v.aniu.tv/video/play/id/85820/t/2.shtml"
      },
      {
        "innerHTML": "薛松：掐头去尾吃鱼身！",
        "href": "https://v.aniu.tv/video/play/id/85700/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这是基础的棋谱！",
        "href": "https://v.aniu.tv/video/play/id/85709/t/2.shtml"
      },
      {
        "innerHTML": "薛松：上证目前是复合顶，时间不到不重要！",
        "href": "https://v.aniu.tv/video/play/id/85710/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不看空市场，时间上等待缩量",
        "href": "https://v.aniu.tv/video/play/id/85130/t/2.shtml"
      },
      {
        "innerHTML": "薛松：怎么选在高点离场？",
        "href": "https://v.aniu.tv/video/play/id/85141/t/2.shtml"
      },
      {
        "innerHTML": "薛松：年初永远的神？",
        "href": "https://v.aniu.tv/video/play/id/85014/t/2.shtml"
      },
      {
        "innerHTML": "薛松：反着散户买别墅靠大海！",
        "href": "https://v.aniu.tv/video/play/id/85015/t/2.shtml"
      },
      {
        "innerHTML": "薛松：我比较凉薄，券商不准补仓！",
        "href": "https://v.aniu.tv/video/play/id/81426/t/2.shtml"
      },
      {
        "innerHTML": "薛松：游客不足，打也行，不打也行",
        "href": "https://v.aniu.tv/video/play/id/81427/t/2.shtml"
      },
      {
        "innerHTML": "薛松：对指数没观点，做对了就坚持",
        "href": "https://v.aniu.tv/video/play/id/81272/t/2.shtml"
      },
      {
        "innerHTML": "薛松：发现一个现象，利用一个现象",
        "href": "https://v.aniu.tv/video/play/id/81292/t/2.shtml"
      },
      {
        "innerHTML": "薛松：准备滞胀的节奏！",
        "href": "https://v.aniu.tv/video/play/id/81294/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数的大动在于面积不在幅度",
        "href": "https://v.aniu.tv/video/play/id/81370/t/2.shtml"
      },
      {
        "innerHTML": "薛松：比定投更好的策略！",
        "href": "https://v.aniu.tv/video/play/id/80681/t/2.shtml"
      },
      {
        "innerHTML": "薛松：最容易赚钱的方式就是趋势投资",
        "href": "https://v.aniu.tv/video/play/id/80682/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不要以为缩量跟你没关系",
        "href": "https://v.aniu.tv/video/play/id/80692/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：博弈高手 掌握规律并打破规律！",
        "href": "https://v.aniu.tv/video/play/id/76617/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数向上空间不大 市场风口此起彼伏",
        "href": "https://v.aniu.tv/video/play/id/76618/t/2.shtml"
      },
      {
        "innerHTML": "薛松：凡事没有绝对 没有统一答案",
        "href": "https://v.aniu.tv/video/play/id/76034/t/2.shtml"
      },
      {
        "innerHTML": "薛松：学会适应反差 别让心态影响你",
        "href": "https://v.aniu.tv/video/play/id/76037/t/2.shtml"
      },
      {
        "innerHTML": "薛松：没有新生代 何来元宇宙？",
        "href": "https://v.aniu.tv/video/play/id/76042/t/2.shtml"
      },
      {
        "innerHTML": "薛松：吃的不是地沟油 是自由！",
        "href": "https://v.aniu.tv/video/play/id/76046/t/2.shtml"
      },
      {
        "innerHTML": "薛松：没有两个相同的走势 抽象是为更好的类比",
        "href": "https://v.aniu.tv/video/play/id/75934/t/2.shtml"
      },
      {
        "innerHTML": "薛松：做交易的人 最怕无量",
        "href": "https://v.aniu.tv/video/play/id/75936/t/2.shtml"
      },
      {
        "innerHTML": "薛松：对信息下注 不做不懂的东西",
        "href": "https://v.aniu.tv/video/play/id/75991/t/2.shtml"
      },
      {
        "innerHTML": "薛松：没有研报的公司 你少碰！",
        "href": "https://v.aniu.tv/video/play/id/76031/t/2.shtml"
      },
      {
        "innerHTML": "薛松：武功练到极致 也不存在绝对",
        "href": "https://v.aniu.tv/video/play/id/75420/t/2.shtml"
      },
      {
        "innerHTML": "薛松：市场繁荣起来 谁吃谁无所谓！",
        "href": "https://v.aniu.tv/video/play/id/75421/t/2.shtml"
      },
      {
        "innerHTML": "薛松：含泪总结 炒股必知的两件事情！",
        "href": "https://v.aniu.tv/video/play/id/75425/t/2.shtml"
      },
      {
        "innerHTML": "薛松：两件事情都说对了",
        "href": "https://v.aniu.tv/video/play/id/75300/t/2.shtml"
      },
      {
        "innerHTML": "薛松：理性的学习是偶然 感性上认知是必然",
        "href": "https://v.aniu.tv/video/play/id/75308/t/2.shtml"
      },
      {
        "innerHTML": "薛松：将优势抹杀掉的是情绪性操作",
        "href": "https://v.aniu.tv/video/play/id/75309/t/2.shtml"
      },
      {
        "innerHTML": "薛松：缩量上攻出现了！",
        "href": "https://v.aniu.tv/video/play/id/74659/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不要总想着操作！",
        "href": "https://v.aniu.tv/video/play/id/74660/t/2.shtml"
      },
      {
        "innerHTML": "薛松：天然气板块早已进入上升通道",
        "href": "https://v.aniu.tv/video/play/id/74662/t/2.shtml"
      },
      {
        "innerHTML": "薛松：要允许一定的投机存在！",
        "href": "https://v.aniu.tv/video/play/id/74664/t/2.shtml"
      },
      {
        "innerHTML": "薛松：忙者不会 会者不忙",
        "href": "https://v.aniu.tv/video/play/id/74710/t/2.shtml"
      },
      {
        "innerHTML": "薛松：券商板块进入高温阶段",
        "href": "https://v.aniu.tv/video/play/id/74532/t/2.shtml"
      },
      {
        "innerHTML": "薛松：论三线开花技术",
        "href": "https://v.aniu.tv/video/play/id/74534/t/2.shtml"
      },
      {
        "innerHTML": "薛松：欲作诗 先熟读唐诗三百首",
        "href": "https://v.aniu.tv/video/play/id/74535/t/2.shtml"
      },
      {
        "innerHTML": "薛松：价值判断很难实现 更多是利益判断",
        "href": "https://v.aniu.tv/video/play/id/74536/t/2.shtml"
      },
      {
        "innerHTML": "薛松：股市操练大全 小白的最爱！",
        "href": "https://v.aniu.tv/video/play/id/74585/t/2.shtml"
      },
      {
        "innerHTML": "薛松：上面动动嘴 下面跑断腿",
        "href": "https://v.aniu.tv/video/play/id/74589/t/2.shtml"
      },
      {
        "innerHTML": "薛松：原来格力自己在买！",
        "href": "https://v.aniu.tv/video/play/id/74597/t/2.shtml"
      },
      {
        "innerHTML": "薛松：留得量能在 不怕没机会",
        "href": "https://v.aniu.tv/video/play/id/74785/t/2.shtml"
      },
      {
        "innerHTML": "薛松：对后期杀跌力度不要掉以轻心",
        "href": "https://v.aniu.tv/video/play/id/73931/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不要幻想成为巴菲特",
        "href": "https://v.aniu.tv/video/play/id/73932/t/2.shtml"
      },
      {
        "innerHTML": "薛松：白头市场是成熟市场 细说一通有惊喜",
        "href": "https://v.aniu.tv/video/play/id/73934/t/2.shtml"
      },
      {
        "innerHTML": "薛松：又是200点！顺应市场！",
        "href": "https://v.aniu.tv/video/play/id/73786/t/2.shtml"
      },
      {
        "innerHTML": "薛松：从市场行为感知稳定迹象",
        "href": "https://v.aniu.tv/video/play/id/73787/t/2.shtml"
      },
      {
        "innerHTML": "薛松：过于细节则跌倒 模糊是上策",
        "href": "https://v.aniu.tv/video/play/id/73788/t/2.shtml"
      },
      {
        "innerHTML": "薛松：多问便是狂妄！股市存在源于未知性！",
        "href": "https://v.aniu.tv/video/play/id/73789/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不疯魔不成活",
        "href": "https://v.aniu.tv/video/play/id/73852/t/2.shtml"
      },
      {
        "innerHTML": "薛松：沉没成本的精髓",
        "href": "https://v.aniu.tv/video/play/id/73864/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你闻所未闻的能源知识",
        "href": "https://v.aniu.tv/video/play/id/73183/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2016-06-01",
        "href": "https://v.aniu.tv/video/play/id/4295/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-05-31",
        "href": "https://v.aniu.tv/video/play/id/4278/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-05-26",
        "href": "https://v.aniu.tv/video/play/id/4194/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-05-26",
        "href": "https://v.aniu.tv/video/play/id/4195/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-05-26",
        "href": "https://v.aniu.tv/video/play/id/4196/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-05-26",
        "href": "https://v.aniu.tv/video/play/id/4204/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-05-25",
        "href": "https://v.aniu.tv/video/play/id/4186/t/2.shtml"
      },
      {
        "innerHTML": "昨夜华尔街2016-05-24",
        "href": "https://v.aniu.tv/video/play/id/4142/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2016-05-24",
        "href": "https://v.aniu.tv/video/play/id/4143/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-05-24",
        "href": "https://v.aniu.tv/video/play/id/4181/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-05-12",
        "href": "https://v.aniu.tv/video/play/id/3986/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-05-12",
        "href": "https://v.aniu.tv/video/play/id/3987/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-05-12",
        "href": "https://v.aniu.tv/video/play/id/3988/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-05-12",
        "href": "https://v.aniu.tv/video/play/id/4053/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-05-11",
        "href": "https://v.aniu.tv/video/play/id/3981/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-05-05",
        "href": "https://v.aniu.tv/video/play/id/3872/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-05-05",
        "href": "https://v.aniu.tv/video/play/id/3873/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-05-05",
        "href": "https://v.aniu.tv/video/play/id/3874/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-05-05",
        "href": "https://v.aniu.tv/video/play/id/3881/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-05-04",
        "href": "https://v.aniu.tv/video/play/id/3863/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-04-28",
        "href": "https://v.aniu.tv/video/play/id/3786/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-04-28",
        "href": "https://v.aniu.tv/video/play/id/3787/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-04-28",
        "href": "https://v.aniu.tv/video/play/id/3799/t/2.shtml"
      },
      {
        "innerHTML": "昨夜华尔街2016-04-27",
        "href": "https://v.aniu.tv/video/play/id/3759/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2016-04-27",
        "href": "https://v.aniu.tv/video/play/id/3763/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-04-27",
        "href": "https://v.aniu.tv/video/play/id/3810/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-04-21",
        "href": "https://v.aniu.tv/video/play/id/3680/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-04-21",
        "href": "https://v.aniu.tv/video/play/id/3681/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-04-21",
        "href": "https://v.aniu.tv/video/play/id/3682/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-04-21",
        "href": "https://v.aniu.tv/video/play/id/3693/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-04-28",
        "href": "https://v.aniu.tv/video/play/id/3785/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-04-20",
        "href": "https://v.aniu.tv/video/play/id/3673/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-04-14",
        "href": "https://v.aniu.tv/video/play/id/3576/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-04-14",
        "href": "https://v.aniu.tv/video/play/id/3577/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-04-14",
        "href": "https://v.aniu.tv/video/play/id/3578/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-04-14",
        "href": "https://v.aniu.tv/video/play/id/3599/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-04-13",
        "href": "https://v.aniu.tv/video/play/id/3572/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-04-07",
        "href": "https://v.aniu.tv/video/play/id/3470/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-04-07",
        "href": "https://v.aniu.tv/video/play/id/3471/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2019-10-22",
        "href": "https://v.aniu.tv/video/play/id/41412/t/2.shtml"
      },
      {
        "innerHTML": "聚焦新动能2019-10-20",
        "href": "https://v.aniu.tv/video/play/id/41278/t/2.shtml"
      },
      {
        "innerHTML": "交易封神榜2019-10-20",
        "href": "https://v.aniu.tv/video/play/id/41279/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投资市场的规律来源于人性不变",
        "href": "https://v.aniu.tv/video/play/id/41197/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-10-17",
        "href": "https://v.aniu.tv/video/play/id/41168/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-10-16",
        "href": "https://v.aniu.tv/video/play/id/41152/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-10-15",
        "href": "https://v.aniu.tv/video/play/id/41094/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-10-10",
        "href": "https://v.aniu.tv/video/play/id/40893/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-10-10",
        "href": "https://v.aniu.tv/video/play/id/40897/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-10-10",
        "href": "https://v.aniu.tv/video/play/id/40899/t/2.shtml"
      },
      {
        "innerHTML": "薛松：牛市真正到来的特点",
        "href": "https://v.aniu.tv/video/play/id/40881/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-10-09",
        "href": "https://v.aniu.tv/video/play/id/40882/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-10-08",
        "href": "https://v.aniu.tv/video/play/id/40846/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数最多到2900上方，反弹即将开始！",
        "href": "https://v.aniu.tv/video/play/id/40675/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-09-26",
        "href": "https://v.aniu.tv/video/play/id/40629/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-09-26",
        "href": "https://v.aniu.tv/video/play/id/40630/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-09-26",
        "href": "https://v.aniu.tv/video/play/id/40631/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-09-25",
        "href": "https://v.aniu.tv/video/play/id/40619/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-09-24",
        "href": "https://v.aniu.tv/video/play/id/40542/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-09-24",
        "href": "https://v.aniu.tv/video/play/id/40543/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-09-24",
        "href": "https://v.aniu.tv/video/play/id/40544/t/2.shtml"
      },
      {
        "innerHTML": "王雨厚：现在市场上这两类股票有机会！",
        "href": "https://v.aniu.tv/video/play/id/40538/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-09-24",
        "href": "https://v.aniu.tv/video/play/id/40599/t/2.shtml"
      },
      {
        "innerHTML": "薛松：保险股为何只能做投资",
        "href": "https://v.aniu.tv/video/play/id/40373/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-09-19",
        "href": "https://v.aniu.tv/video/play/id/40390/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-09-18",
        "href": "https://v.aniu.tv/video/play/id/40375/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-09-17",
        "href": "https://v.aniu.tv/video/play/id/40325/t/2.shtml"
      },
      {
        "innerHTML": "杨殿方：等待MACD金叉出现，大盘将破3288！",
        "href": "https://v.aniu.tv/video/play/id/40206/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-09-12",
        "href": "https://v.aniu.tv/video/play/id/40211/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-09-12",
        "href": "https://v.aniu.tv/video/play/id/40212/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-09-12",
        "href": "https://v.aniu.tv/video/play/id/40213/t/2.shtml"
      },
      {
        "innerHTML": "千鹤：关注走势级别才能掌控好节奏",
        "href": "https://v.aniu.tv/video/play/id/40181/t/2.shtml"
      },
      {
        "innerHTML": "薛松：注意行情结束的标志",
        "href": "https://v.aniu.tv/video/play/id/40180/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-09-11",
        "href": "https://v.aniu.tv/video/play/id/40210/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-09-10",
        "href": "https://v.aniu.tv/video/play/id/40194/t/2.shtml"
      },
      {
        "innerHTML": "薛松：做股票不能贪便宜",
        "href": "https://v.aniu.tv/video/play/id/39944/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-09-04",
        "href": "https://v.aniu.tv/video/play/id/39952/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-09-03",
        "href": "https://v.aniu.tv/video/play/id/39878/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-08-29",
        "href": "https://v.aniu.tv/video/play/id/39690/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：灵魂四问",
        "href": "https://v.aniu.tv/video/play/id/58068/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2021-01-12",
        "href": "https://v.aniu.tv/video/play/id/58065/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2021-01-11",
        "href": "https://v.aniu.tv/video/play/id/58028/t/2.shtml"
      },
      {
        "innerHTML": "薛松：动物世界视角看股市",
        "href": "https://v.aniu.tv/video/play/id/57873/t/2.shtml"
      },
      {
        "innerHTML": "薛松：馅饼与陷阱 聪明鱼普通鱼",
        "href": "https://v.aniu.tv/video/play/id/57897/t/2.shtml"
      },
      {
        "innerHTML": "主题投资2021-01-07",
        "href": "https://v.aniu.tv/video/play/id/57849/t/2.shtml"
      },
      {
        "innerHTML": "主题投资2020-12-30",
        "href": "https://v.aniu.tv/video/play/id/57483/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-12-30",
        "href": "https://v.aniu.tv/video/play/id/57472/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-12-29",
        "href": "https://v.aniu.tv/video/play/id/57394/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你还是原来的你",
        "href": "https://v.aniu.tv/video/play/id/55805/t/2.shtml"
      },
      {
        "innerHTML": "薛松：晃晃悠 跑不赢",
        "href": "https://v.aniu.tv/video/play/id/55741/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这就是在复刻八月份的行情",
        "href": "https://v.aniu.tv/video/play/id/55611/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数很气人的对吧",
        "href": "https://v.aniu.tv/video/play/id/55538/t/2.shtml"
      },
      {
        "innerHTML": "薛松：市场很理性，要挣谁的钱",
        "href": "https://v.aniu.tv/video/play/id/55484/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-11-25",
        "href": "https://v.aniu.tv/video/play/id/55446/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-11-24",
        "href": "https://v.aniu.tv/video/play/id/55449/t/2.shtml"
      },
      {
        "innerHTML": "薛松：承认吧，别再骗自己了",
        "href": "https://v.aniu.tv/video/play/id/55328/t/2.shtml"
      },
      {
        "innerHTML": "薛松：老股民 够花吗",
        "href": "https://v.aniu.tv/video/play/id/55132/t/2.shtml"
      },
      {
        "innerHTML": "薛松：前15分钟 看一眼",
        "href": "https://v.aniu.tv/video/play/id/55072/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-11-18",
        "href": "https://v.aniu.tv/video/play/id/55081/t/2.shtml"
      },
      {
        "innerHTML": "薛松：黎明前不叫黑暗",
        "href": "https://v.aniu.tv/video/play/id/55008/t/2.shtml"
      },
      {
        "innerHTML": "薛松：中石化",
        "href": "https://v.aniu.tv/video/play/id/54938/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-11-17",
        "href": "https://v.aniu.tv/video/play/id/55007/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-11-11",
        "href": "https://v.aniu.tv/video/play/id/54690/t/2.shtml"
      },
      {
        "innerHTML": "我也想花一百万就像花一毛钱那样",
        "href": "https://v.aniu.tv/video/play/id/54568/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-11-10",
        "href": "https://v.aniu.tv/video/play/id/54650/t/2.shtml"
      },
      {
        "innerHTML": "薛松：有什么可骂的",
        "href": "https://v.aniu.tv/video/play/id/54380/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-11-04",
        "href": "https://v.aniu.tv/video/play/id/54312/t/2.shtml"
      },
      {
        "innerHTML": "薛松：one day day 会吗",
        "href": "https://v.aniu.tv/video/play/id/54153/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-11-03",
        "href": "https://v.aniu.tv/video/play/id/54222/t/2.shtml"
      },
      {
        "innerHTML": "薛松：胡说八道",
        "href": "https://v.aniu.tv/video/play/id/54091/t/2.shtml"
      },
      {
        "innerHTML": "薛松：近期可以的",
        "href": "https://v.aniu.tv/video/play/id/54029/t/2.shtml"
      },
      {
        "innerHTML": "薛松：蚂蚁牛吗",
        "href": "https://v.aniu.tv/video/play/id/53886/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-10-28",
        "href": "https://v.aniu.tv/video/play/id/53883/t/2.shtml"
      },
      {
        "innerHTML": "薛松：瞬间三四十",
        "href": "https://v.aniu.tv/video/play/id/53813/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-10-27",
        "href": "https://v.aniu.tv/video/play/id/53850/t/2.shtml"
      },
      {
        "innerHTML": "薛松：突破阳线会有吗",
        "href": "https://v.aniu.tv/video/play/id/53759/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-10-21",
        "href": "https://v.aniu.tv/video/play/id/53535/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-10-20",
        "href": "https://v.aniu.tv/video/play/id/53475/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2018-01-10",
        "href": "https://v.aniu.tv/video/play/id/17300/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-01-09",
        "href": "https://v.aniu.tv/video/play/id/17267/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为什么说最大止损位不能超过20%",
        "href": "https://v.aniu.tv/video/play/id/17119/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-01-04",
        "href": "https://v.aniu.tv/video/play/id/17120/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-01-04",
        "href": "https://v.aniu.tv/video/play/id/17121/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-01-04",
        "href": "https://v.aniu.tv/video/play/id/17122/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-01-03",
        "href": "https://v.aniu.tv/video/play/id/17110/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-01-02",
        "href": "https://v.aniu.tv/video/play/id/17068/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-12-28",
        "href": "https://v.aniu.tv/video/play/id/16976/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-12-28",
        "href": "https://v.aniu.tv/video/play/id/16977/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-12-28",
        "href": "https://v.aniu.tv/video/play/id/16978/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-12-27",
        "href": "https://v.aniu.tv/video/play/id/16968/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-12-26",
        "href": "https://v.aniu.tv/video/play/id/16936/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-12-21",
        "href": "https://v.aniu.tv/video/play/id/16791/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-12-21",
        "href": "https://v.aniu.tv/video/play/id/16793/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-12-21",
        "href": "https://v.aniu.tv/video/play/id/16794/t/2.shtml"
      },
      {
        "innerHTML": "薛松：震荡缩量无大涨",
        "href": "https://v.aniu.tv/video/play/id/16773/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-12-20",
        "href": "https://v.aniu.tv/video/play/id/16780/t/2.shtml"
      },
      {
        "innerHTML": "薛松：耐心等待放量后入场",
        "href": "https://v.aniu.tv/video/play/id/16742/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-12-19",
        "href": "https://v.aniu.tv/video/play/id/16751/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为什么要设止损位",
        "href": "https://v.aniu.tv/video/play/id/16600/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-12-14",
        "href": "https://v.aniu.tv/video/play/id/16616/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-12-14",
        "href": "https://v.aniu.tv/video/play/id/16617/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-12-14",
        "href": "https://v.aniu.tv/video/play/id/16618/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-12-13",
        "href": "https://v.aniu.tv/video/play/id/16605/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-12-12",
        "href": "https://v.aniu.tv/video/play/id/16570/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-12-07",
        "href": "https://v.aniu.tv/video/play/id/16428/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-12-07",
        "href": "https://v.aniu.tv/video/play/id/16429/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-12-07",
        "href": "https://v.aniu.tv/video/play/id/16430/t/2.shtml"
      },
      {
        "innerHTML": "薛松：缩量对于市场的利弊",
        "href": "https://v.aniu.tv/video/play/id/16436/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-12-06",
        "href": "https://v.aniu.tv/video/play/id/16420/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-12-05",
        "href": "https://v.aniu.tv/video/play/id/16383/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-11-30",
        "href": "https://v.aniu.tv/video/play/id/16242/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-11-30",
        "href": "https://v.aniu.tv/video/play/id/16243/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-11-30",
        "href": "https://v.aniu.tv/video/play/id/16244/t/2.shtml"
      },
      {
        "innerHTML": "薛松：3300点能否成为有效低点",
        "href": "https://v.aniu.tv/video/play/id/16250/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-11-29",
        "href": "https://v.aniu.tv/video/play/id/16229/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-11-28",
        "href": "https://v.aniu.tv/video/play/id/16192/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-11-23",
        "href": "https://v.aniu.tv/video/play/id/16063/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：不认就气死你  这口才小编认了",
        "href": "https://v.aniu.tv/video/play/id/49683/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-07-02",
        "href": "https://v.aniu.tv/video/play/id/49634/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-07-02",
        "href": "https://v.aniu.tv/video/play/id/49635/t/2.shtml"
      },
      {
        "innerHTML": "千鹤：七月行情欲扬先抑，当下不要盲目追高",
        "href": "https://v.aniu.tv/video/play/id/49756/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指标可以查字典，教你如何使用RSI指标",
        "href": "https://v.aniu.tv/video/play/id/49757/t/2.shtml"
      },
      {
        "innerHTML": "千鹤：只做看得懂的那一段不要赚尽每一分钱",
        "href": "https://v.aniu.tv/video/play/id/49759/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-01",
        "href": "https://v.aniu.tv/video/play/id/49610/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-06-30",
        "href": "https://v.aniu.tv/video/play/id/49560/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你不及格 他有的是办法洗你",
        "href": "https://v.aniu.tv/video/play/id/49505/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-06-23",
        "href": "https://v.aniu.tv/video/play/id/49382/t/2.shtml"
      },
      {
        "innerHTML": "张展博：变盘风险来临，警惕风险",
        "href": "https://v.aniu.tv/video/play/id/49216/t/2.shtml"
      },
      {
        "innerHTML": "张展博:对于市场风险不可掉以轻心",
        "href": "https://v.aniu.tv/video/play/id/49217/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-06-18",
        "href": "https://v.aniu.tv/video/play/id/49207/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-06-18",
        "href": "https://v.aniu.tv/video/play/id/49208/t/2.shtml"
      },
      {
        "innerHTML": "薛松：早在20年前",
        "href": "https://v.aniu.tv/video/play/id/49178/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-06-17",
        "href": "https://v.aniu.tv/video/play/id/49173/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-06-16",
        "href": "https://v.aniu.tv/video/play/id/49174/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-06-10",
        "href": "https://v.aniu.tv/video/play/id/48902/t/2.shtml"
      },
      {
        "innerHTML": "薛松：科技 你都敢投 你是不是膨胀了",
        "href": "https://v.aniu.tv/video/play/id/48817/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-06-09",
        "href": "https://v.aniu.tv/video/play/id/48853/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-06-04",
        "href": "https://v.aniu.tv/video/play/id/48666/t/2.shtml"
      },
      {
        "innerHTML": "薛松：窄幅震荡出黑马",
        "href": "https://v.aniu.tv/video/play/id/48641/t/2.shtml"
      },
      {
        "innerHTML": "陈文：2900点是中短期重要的支撑位",
        "href": "https://v.aniu.tv/video/play/id/48620/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-06-03",
        "href": "https://v.aniu.tv/video/play/id/48631/t/2.shtml"
      },
      {
        "innerHTML": "薛松：我的命由我 但是输赢由天",
        "href": "https://v.aniu.tv/video/play/id/48472/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-06-02",
        "href": "https://v.aniu.tv/video/play/id/48489/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-05-28",
        "href": "https://v.aniu.tv/video/play/id/48368/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-05-27",
        "href": "https://v.aniu.tv/video/play/id/48326/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-05-26",
        "href": "https://v.aniu.tv/video/play/id/48252/t/2.shtml"
      },
      {
        "innerHTML": "薛松：我就这么一招 教给你们 看懂了吗",
        "href": "https://v.aniu.tv/video/play/id/48160/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-05-21",
        "href": "https://v.aniu.tv/video/play/id/48133/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-05-21",
        "href": "https://v.aniu.tv/video/play/id/48134/t/2.shtml"
      },
      {
        "innerHTML": "严明阳：指数问题不大，重点关注两会行情",
        "href": "https://v.aniu.tv/video/play/id/48227/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投资B股 他错了吗  除了没挣钱 他啥都对",
        "href": "https://v.aniu.tv/video/play/id/48105/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-05-20",
        "href": "https://v.aniu.tv/video/play/id/48088/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-05-19",
        "href": "https://v.aniu.tv/video/play/id/48044/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-05-13",
        "href": "https://v.aniu.tv/video/play/id/47852/t/2.shtml"
      },
      {
        "innerHTML": "薛松：我看好得很! 猜 那个谁为什么没来",
        "href": "https://v.aniu.tv/video/play/id/47889/t/2.shtml"
      },
      {
        "innerHTML": "薛松：薛不服-三大不服 上证指数为什么那么低迷",
        "href": "https://v.aniu.tv/video/play/id/47841/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "财富广角2018-05-10",
        "href": "https://v.aniu.tv/video/play/id/20027/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-05-09",
        "href": "https://v.aniu.tv/video/play/id/20017/t/2.shtml"
      },
      {
        "innerHTML": "市场物极必反的机会来了",
        "href": "https://v.aniu.tv/video/play/id/19974/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-05-08",
        "href": "https://v.aniu.tv/video/play/id/19985/t/2.shtml"
      },
      {
        "innerHTML": "中证全指仍然是空头排列",
        "href": "https://v.aniu.tv/video/play/id/19830/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-05-03",
        "href": "https://v.aniu.tv/video/play/id/19843/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-05-03",
        "href": "https://v.aniu.tv/video/play/id/19844/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-05-03",
        "href": "https://v.aniu.tv/video/play/id/19845/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-05-02",
        "href": "https://v.aniu.tv/video/play/id/19835/t/2.shtml"
      },
      {
        "innerHTML": "芯片企业要想成功，一定是一将成名万将枯。",
        "href": "https://v.aniu.tv/video/play/id/19734/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-04-26",
        "href": "https://v.aniu.tv/video/play/id/19747/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-04-26",
        "href": "https://v.aniu.tv/video/play/id/19748/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-04-26",
        "href": "https://v.aniu.tv/video/play/id/19749/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-04-25",
        "href": "https://v.aniu.tv/video/play/id/19739/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-04-24",
        "href": "https://v.aniu.tv/video/play/id/19700/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-04-19",
        "href": "https://v.aniu.tv/video/play/id/19580/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-04-19",
        "href": "https://v.aniu.tv/video/play/id/19579/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-04-19",
        "href": "https://v.aniu.tv/video/play/id/19581/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-04-18",
        "href": "https://v.aniu.tv/video/play/id/19571/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-04-17",
        "href": "https://v.aniu.tv/video/play/id/19533/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-04-12",
        "href": "https://v.aniu.tv/video/play/id/19400/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-04-12",
        "href": "https://v.aniu.tv/video/play/id/19401/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-04-12",
        "href": "https://v.aniu.tv/video/play/id/19402/t/2.shtml"
      },
      {
        "innerHTML": "薛松：未来大小盘个股一定会齐头并进",
        "href": "https://v.aniu.tv/video/play/id/19413/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-04-11",
        "href": "https://v.aniu.tv/video/play/id/19391/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-04-10",
        "href": "https://v.aniu.tv/video/play/id/19360/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-04-04",
        "href": "https://v.aniu.tv/video/play/id/19283/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-04-03",
        "href": "https://v.aniu.tv/video/play/id/19260/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-03-29",
        "href": "https://v.aniu.tv/video/play/id/19113/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-03-29",
        "href": "https://v.aniu.tv/video/play/id/19112/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-03-29",
        "href": "https://v.aniu.tv/video/play/id/19114/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-03-28",
        "href": "https://v.aniu.tv/video/play/id/19104/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-03-27",
        "href": "https://v.aniu.tv/video/play/id/19070/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-03-22",
        "href": "https://v.aniu.tv/video/play/id/18927/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-03-22",
        "href": "https://v.aniu.tv/video/play/id/18928/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-03-22",
        "href": "https://v.aniu.tv/video/play/id/18929/t/2.shtml"
      },
      {
        "innerHTML": "薛松：今天的涨停板都是一种形态",
        "href": "https://v.aniu.tv/video/play/id/18940/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-03-21",
        "href": "https://v.aniu.tv/video/play/id/18919/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-03-20",
        "href": "https://v.aniu.tv/video/play/id/18879/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "量化热点股2017-09-28",
        "href": "https://v.aniu.tv/video/play/id/14788/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-09-28",
        "href": "https://v.aniu.tv/video/play/id/14789/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-09-28",
        "href": "https://v.aniu.tv/video/play/id/14790/t/2.shtml"
      },
      {
        "innerHTML": "薛松：KDJ的K值J值特点",
        "href": "https://v.aniu.tv/video/play/id/14753/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-09-27",
        "href": "https://v.aniu.tv/video/play/id/14774/t/2.shtml"
      },
      {
        "innerHTML": "薛松：如何正确操作低位股和高位股",
        "href": "https://v.aniu.tv/video/play/id/14597/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-09-21",
        "href": "https://v.aniu.tv/video/play/id/14585/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-09-21",
        "href": "https://v.aniu.tv/video/play/id/14587/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-09-21",
        "href": "https://v.aniu.tv/video/play/id/14586/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-09-20",
        "href": "https://v.aniu.tv/video/play/id/14574/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-09-19",
        "href": "https://v.aniu.tv/video/play/id/14540/t/2.shtml"
      },
      {
        "innerHTML": "薛松：信念+运气才能走向成功",
        "href": "https://v.aniu.tv/video/play/id/14409/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-09-14",
        "href": "https://v.aniu.tv/video/play/id/14398/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-09-14",
        "href": "https://v.aniu.tv/video/play/id/14399/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-09-14",
        "href": "https://v.aniu.tv/video/play/id/14400/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-09-13",
        "href": "https://v.aniu.tv/video/play/id/14373/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-09-12",
        "href": "https://v.aniu.tv/video/play/id/14347/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-09-07",
        "href": "https://v.aniu.tv/video/play/id/14222/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-09-07",
        "href": "https://v.aniu.tv/video/play/id/14223/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-09-07",
        "href": "https://v.aniu.tv/video/play/id/14224/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-09-06",
        "href": "https://v.aniu.tv/video/play/id/14206/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-09-05",
        "href": "https://v.aniu.tv/video/play/id/14167/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-09-01",
        "href": "https://v.aniu.tv/video/play/id/14066/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-09-01",
        "href": "https://v.aniu.tv/video/play/id/14067/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-09-01",
        "href": "https://v.aniu.tv/video/play/id/14068/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-08-31",
        "href": "https://v.aniu.tv/video/play/id/14028/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-08-31",
        "href": "https://v.aniu.tv/video/play/id/14029/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-08-31",
        "href": "https://v.aniu.tv/video/play/id/14030/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-08-30",
        "href": "https://v.aniu.tv/video/play/id/14006/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-08-29",
        "href": "https://v.aniu.tv/video/play/id/13988/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-08-24",
        "href": "https://v.aniu.tv/video/play/id/13864/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-08-24",
        "href": "https://v.aniu.tv/video/play/id/13865/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-08-24",
        "href": "https://v.aniu.tv/video/play/id/13866/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-08-23",
        "href": "https://v.aniu.tv/video/play/id/13853/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-08-22",
        "href": "https://v.aniu.tv/video/play/id/13816/t/2.shtml"
      },
      {
        "innerHTML": "薛松：道氏理论看创业板",
        "href": "https://v.aniu.tv/video/play/id/13705/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-08-17",
        "href": "https://v.aniu.tv/video/play/id/13697/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-08-17",
        "href": "https://v.aniu.tv/video/play/id/13698/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-08-17",
        "href": "https://v.aniu.tv/video/play/id/13699/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "量化热点股2018-10-25",
        "href": "https://v.aniu.tv/video/play/id/27392/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-10-25",
        "href": "https://v.aniu.tv/video/play/id/27393/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-10-25",
        "href": "https://v.aniu.tv/video/play/id/27394/t/2.shtml"
      },
      {
        "innerHTML": "板块中挑选强势个股要考虑如下标准！！",
        "href": "https://v.aniu.tv/video/play/id/27366/t/2.shtml"
      },
      {
        "innerHTML": "壳成为市场香饽饽 一分钟看懂地方政府为何买壳。",
        "href": "https://v.aniu.tv/video/play/id/27368/t/2.shtml"
      },
      {
        "innerHTML": "央行印发的钱如何发行出去的？",
        "href": "https://v.aniu.tv/video/play/id/27370/t/2.shtml"
      },
      {
        "innerHTML": "薛松：房地产股的炒作规律要牢记",
        "href": "https://v.aniu.tv/video/play/id/27369/t/2.shtml"
      },
      {
        "innerHTML": "薛松：四要诀助你抢抓涨停板！！",
        "href": "https://v.aniu.tv/video/play/id/27367/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-10-24",
        "href": "https://v.aniu.tv/video/play/id/27384/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-10-23",
        "href": "https://v.aniu.tv/video/play/id/27304/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-10-18",
        "href": "https://v.aniu.tv/video/play/id/27021/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-10-18",
        "href": "https://v.aniu.tv/video/play/id/27022/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-10-18",
        "href": "https://v.aniu.tv/video/play/id/27020/t/2.shtml"
      },
      {
        "innerHTML": "股民必知的股票投机原理",
        "href": "https://v.aniu.tv/video/play/id/26995/t/2.shtml"
      },
      {
        "innerHTML": "如何借助美元走势研判国内商品价格走势？",
        "href": "https://v.aniu.tv/video/play/id/27002/t/2.shtml"
      },
      {
        "innerHTML": "薛松：当板块出现重大利好时，如何选出龙头股？",
        "href": "https://v.aniu.tv/video/play/id/26998/t/2.shtml"
      },
      {
        "innerHTML": "薛松：一分钟学会捕捉题材股领头羊",
        "href": "https://v.aniu.tv/video/play/id/26996/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-10-17",
        "href": "https://v.aniu.tv/video/play/id/27009/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-10-16",
        "href": "https://v.aniu.tv/video/play/id/26953/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-10-11",
        "href": "https://v.aniu.tv/video/play/id/26649/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-10-11",
        "href": "https://v.aniu.tv/video/play/id/26650/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-10-11",
        "href": "https://v.aniu.tv/video/play/id/26651/t/2.shtml"
      },
      {
        "innerHTML": "薛松：黄金股有投资价值，谁能真正避险？",
        "href": "https://v.aniu.tv/video/play/id/26622/t/2.shtml"
      },
      {
        "innerHTML": "【薛松】熊转牛后的买股策略",
        "href": "https://v.aniu.tv/video/play/id/26620/t/2.shtml"
      },
      {
        "innerHTML": "千鹤：川藏铁路主题机会很大",
        "href": "https://v.aniu.tv/video/play/id/26619/t/2.shtml"
      },
      {
        "innerHTML": "千鹤：每个股民都要懂的弱势下的生存之道",
        "href": "https://v.aniu.tv/video/play/id/26618/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-10-10",
        "href": "https://v.aniu.tv/video/play/id/26641/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-10-09",
        "href": "https://v.aniu.tv/video/play/id/26578/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-09-27",
        "href": "https://v.aniu.tv/video/play/id/26288/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-09-27",
        "href": "https://v.aniu.tv/video/play/id/26289/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-09-27",
        "href": "https://v.aniu.tv/video/play/id/26290/t/2.shtml"
      },
      {
        "innerHTML": "林整华：21天线是多空分水岭",
        "href": "https://v.aniu.tv/video/play/id/26264/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为什么中南文化就是个逃命坡，为什么可以搞",
        "href": "https://v.aniu.tv/video/play/id/26263/t/2.shtml"
      },
      {
        "innerHTML": "林整华：指数就个小回调，请搞清楚股价上涨逻辑",
        "href": "https://v.aniu.tv/video/play/id/26262/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-09-26",
        "href": "https://v.aniu.tv/video/play/id/26280/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-09-25",
        "href": "https://v.aniu.tv/video/play/id/26175/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-09-20",
        "href": "https://v.aniu.tv/video/play/id/26036/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-09-20",
        "href": "https://v.aniu.tv/video/play/id/26037/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-09-20",
        "href": "https://v.aniu.tv/video/play/id/26038/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "股往金来2016-11-29",
        "href": "https://v.aniu.tv/video/play/id/7844/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-11-29",
        "href": "https://v.aniu.tv/video/play/id/7870/t/2.shtml"
      },
      {
        "innerHTML": "薛松：资金不踊跃能否维持上行趋势",
        "href": "https://v.aniu.tv/video/play/id/7668/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-11-24",
        "href": "https://v.aniu.tv/video/play/id/7698/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-11-24",
        "href": "https://v.aniu.tv/video/play/id/7699/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-11-24",
        "href": "https://v.aniu.tv/video/play/id/7700/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-11-23",
        "href": "https://v.aniu.tv/video/play/id/7660/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-11-22",
        "href": "https://v.aniu.tv/video/play/id/7614/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-11-17",
        "href": "https://v.aniu.tv/video/play/id/7441/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-11-17",
        "href": "https://v.aniu.tv/video/play/id/7442/t/2.shtml"
      },
      {
        "innerHTML": "薛松：近期的指数高点其实都有规律",
        "href": "https://v.aniu.tv/video/play/id/7452/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-11-17",
        "href": "https://v.aniu.tv/video/play/id/7443/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-11-16",
        "href": "https://v.aniu.tv/video/play/id/7410/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-11-15",
        "href": "https://v.aniu.tv/video/play/id/7374/t/2.shtml"
      },
      {
        "innerHTML": "薛松：今天的跳空大涨是不是买点",
        "href": "https://v.aniu.tv/video/play/id/7201/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-11-10",
        "href": "https://v.aniu.tv/video/play/id/7238/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-11-10",
        "href": "https://v.aniu.tv/video/play/id/7239/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-11-10",
        "href": "https://v.aniu.tv/video/play/id/7240/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-11-09",
        "href": "https://v.aniu.tv/video/play/id/7225/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-11-08",
        "href": "https://v.aniu.tv/video/play/id/7190/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数阳包阴新高是否志在必得",
        "href": "https://v.aniu.tv/video/play/id/7032/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-11-03",
        "href": "https://v.aniu.tv/video/play/id/7015/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-11-03",
        "href": "https://v.aniu.tv/video/play/id/7016/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-11-03",
        "href": "https://v.aniu.tv/video/play/id/7017/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-11-02",
        "href": "https://v.aniu.tv/video/play/id/6990/t/2.shtml"
      },
      {
        "innerHTML": "薛松：互联网+政务服务建设机会挖掘",
        "href": "https://v.aniu.tv/video/play/id/6959/t/2.shtml"
      },
      {
        "innerHTML": "昨夜华尔街2016-11-01",
        "href": "https://v.aniu.tv/video/play/id/6937/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2016-11-01",
        "href": "https://v.aniu.tv/video/play/id/6938/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-11-01",
        "href": "https://v.aniu.tv/video/play/id/6971/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-10-28",
        "href": "https://v.aniu.tv/video/play/id/6881/t/2.shtml"
      },
      {
        "innerHTML": "薛松：情绪反转市场会有哪些变化",
        "href": "https://v.aniu.tv/video/play/id/6824/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-10-27",
        "href": "https://v.aniu.tv/video/play/id/6833/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-10-27",
        "href": "https://v.aniu.tv/video/play/id/6834/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-10-27",
        "href": "https://v.aniu.tv/video/play/id/6835/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-10-27",
        "href": "https://v.aniu.tv/video/play/id/6846/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-10-26",
        "href": "https://v.aniu.tv/video/play/id/6807/t/2.shtml"
      },
      {
        "innerHTML": "薛松：3100点的大盘还会震荡多久",
        "href": "https://v.aniu.tv/video/play/id/6814/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-10-25",
        "href": "https://v.aniu.tv/video/play/id/6788/t/2.shtml"
      },
      {
        "innerHTML": "薛松：连续两天回调预示了什么",
        "href": "https://v.aniu.tv/video/play/id/6685/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "股市诊疗2017-11-23",
        "href": "https://v.aniu.tv/video/play/id/16064/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-11-23",
        "href": "https://v.aniu.tv/video/play/id/16065/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-11-22",
        "href": "https://v.aniu.tv/video/play/id/16044/t/2.shtml"
      },
      {
        "innerHTML": "薛松：均线拐头，调整延续",
        "href": "https://v.aniu.tv/video/play/id/15879/t/2.shtml"
      },
      {
        "innerHTML": "薛松：长期投资不等于一直持股",
        "href": "https://v.aniu.tv/video/play/id/15880/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-11-16",
        "href": "https://v.aniu.tv/video/play/id/15870/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-11-16",
        "href": "https://v.aniu.tv/video/play/id/15871/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-11-16",
        "href": "https://v.aniu.tv/video/play/id/15872/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-11-15",
        "href": "https://v.aniu.tv/video/play/id/15856/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-11-14",
        "href": "https://v.aniu.tv/video/play/id/15821/t/2.shtml"
      },
      {
        "innerHTML": "薛松：低位启动量的背后逻辑",
        "href": "https://v.aniu.tv/video/play/id/15722/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-11-09",
        "href": "https://v.aniu.tv/video/play/id/15710/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-11-09",
        "href": "https://v.aniu.tv/video/play/id/15711/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-11-09",
        "href": "https://v.aniu.tv/video/play/id/15712/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-11-08",
        "href": "https://v.aniu.tv/video/play/id/15687/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-11-07",
        "href": "https://v.aniu.tv/video/play/id/15655/t/2.shtml"
      },
      {
        "innerHTML": "薛松：布林线在大盘分析中的运用",
        "href": "https://v.aniu.tv/video/play/id/15537/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-11-02",
        "href": "https://v.aniu.tv/video/play/id/15529/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-11-02",
        "href": "https://v.aniu.tv/video/play/id/15530/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-11-02",
        "href": "https://v.aniu.tv/video/play/id/15531/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-11-01",
        "href": "https://v.aniu.tv/video/play/id/15517/t/2.shtml"
      },
      {
        "innerHTML": "薛松，毛羽：智投巧妙结合两种模式的操作",
        "href": "https://v.aniu.tv/video/play/id/15483/t/2.shtml"
      },
      {
        "innerHTML": "主题投资2017-10-31",
        "href": "https://v.aniu.tv/video/play/id/15487/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-10-26",
        "href": "https://v.aniu.tv/video/play/id/15334/t/2.shtml"
      },
      {
        "innerHTML": "薛松：主力的低位推量打法",
        "href": "https://v.aniu.tv/video/play/id/15358/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-10-26",
        "href": "https://v.aniu.tv/video/play/id/15335/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-10-26",
        "href": "https://v.aniu.tv/video/play/id/15336/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-10-25",
        "href": "https://v.aniu.tv/video/play/id/15325/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-10-24",
        "href": "https://v.aniu.tv/video/play/id/15294/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-10-19",
        "href": "https://v.aniu.tv/video/play/id/15171/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-10-19",
        "href": "https://v.aniu.tv/video/play/id/15178/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-10-19",
        "href": "https://v.aniu.tv/video/play/id/15179/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-10-18",
        "href": "https://v.aniu.tv/video/play/id/15162/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-10-17",
        "href": "https://v.aniu.tv/video/play/id/15134/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-10-12",
        "href": "https://v.aniu.tv/video/play/id/14975/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-10-12",
        "href": "https://v.aniu.tv/video/play/id/14976/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-10-12",
        "href": "https://v.aniu.tv/video/play/id/14977/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-10-11",
        "href": "https://v.aniu.tv/video/play/id/14966/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-10-10",
        "href": "https://v.aniu.tv/video/play/id/14933/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "画说基本面2019-12-13",
        "href": "https://v.aniu.tv/video/play/id/43306/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-12-11",
        "href": "https://v.aniu.tv/video/play/id/43145/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-12-10",
        "href": "https://v.aniu.tv/video/play/id/43101/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-11-26",
        "href": "https://v.aniu.tv/video/play/id/42686/t/2.shtml"
      },
      {
        "innerHTML": "薛松：消费电子板块值得关注，方向这样看",
        "href": "https://v.aniu.tv/video/play/id/42676/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2019-11-28",
        "href": "https://v.aniu.tv/video/play/id/42683/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2019-11-28",
        "href": "https://v.aniu.tv/video/play/id/42684/t/2.shtml"
      },
      {
        "innerHTML": "首席投顾2019-11-28",
        "href": "https://v.aniu.tv/video/play/id/42685/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-11-27",
        "href": "https://v.aniu.tv/video/play/id/42645/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为什么股民经常抄底抄在半山腰？",
        "href": "https://v.aniu.tv/video/play/id/42881/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你们不要嘲笑中石油，这种定性股票永远可以买！",
        "href": "https://v.aniu.tv/video/play/id/42470/t/2.shtml"
      },
      {
        "innerHTML": "薛松：微观角度，1分钟级别该如何操作？",
        "href": "https://v.aniu.tv/video/play/id/42469/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-11-21",
        "href": "https://v.aniu.tv/video/play/id/42423/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-11-21",
        "href": "https://v.aniu.tv/video/play/id/42425/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-11-21",
        "href": "https://v.aniu.tv/video/play/id/42426/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-11-20",
        "href": "https://v.aniu.tv/video/play/id/42398/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-11-19",
        "href": "https://v.aniu.tv/video/play/id/42294/t/2.shtml"
      },
      {
        "innerHTML": "薛松：股价下跌公司越解释越说明有股价有水分",
        "href": "https://v.aniu.tv/video/play/id/42170/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-11-13",
        "href": "https://v.aniu.tv/video/play/id/42110/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-11-12",
        "href": "https://v.aniu.tv/video/play/id/42208/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为什么股民经常抄底抄在半山腰？",
        "href": "https://v.aniu.tv/video/play/id/41951/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为何黄金是最值得投资的品种？",
        "href": "https://v.aniu.tv/video/play/id/41973/t/2.shtml"
      },
      {
        "innerHTML": "薛松：现在行情越来越确定意味着什么？",
        "href": "https://v.aniu.tv/video/play/id/41911/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-11-07",
        "href": "https://v.aniu.tv/video/play/id/42368/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-11-07",
        "href": "https://v.aniu.tv/video/play/id/42369/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-11-07",
        "href": "https://v.aniu.tv/video/play/id/42370/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-11-06",
        "href": "https://v.aniu.tv/video/play/id/41943/t/2.shtml"
      },
      {
        "innerHTML": "薛松：无需天天盯着指数做个股",
        "href": "https://v.aniu.tv/video/play/id/41772/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-11-05",
        "href": "https://v.aniu.tv/video/play/id/41909/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-10-30",
        "href": "https://v.aniu.tv/video/play/id/41697/t/2.shtml"
      },
      {
        "innerHTML": "薛松：牛市真正到来的特点",
        "href": "https://v.aniu.tv/video/play/id/41582/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-10-29",
        "href": "https://v.aniu.tv/video/play/id/41635/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-10-24",
        "href": "https://v.aniu.tv/video/play/id/41401/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-10-24",
        "href": "https://v.aniu.tv/video/play/id/41402/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-10-24",
        "href": "https://v.aniu.tv/video/play/id/41403/t/2.shtml"
      },
      {
        "innerHTML": "薛松：DMI 指标的两段机会抓常山北明",
        "href": "https://v.aniu.tv/video/play/id/41408/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数震荡，把握短线机会",
        "href": "https://v.aniu.tv/video/play/id/41406/t/2.shtml"
      },
      {
        "innerHTML": "【薛松】快速急拉后注意看这一指标！",
        "href": "https://v.aniu.tv/video/play/id/41391/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-10-23",
        "href": "https://v.aniu.tv/video/play/id/41413/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2017-03-08",
        "href": "https://v.aniu.tv/video/play/id/10000/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-03-07",
        "href": "https://v.aniu.tv/video/play/id/9981/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-03-02",
        "href": "https://v.aniu.tv/video/play/id/9864/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-03-02",
        "href": "https://v.aniu.tv/video/play/id/9865/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-03-02",
        "href": "https://v.aniu.tv/video/play/id/9866/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-03-01",
        "href": "https://v.aniu.tv/video/play/id/9853/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-02-28",
        "href": "https://v.aniu.tv/video/play/id/9819/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数展开调整如何判断止跌点",
        "href": "https://v.aniu.tv/video/play/id/9702/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-02-23",
        "href": "https://v.aniu.tv/video/play/id/9690/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-02-23",
        "href": "https://v.aniu.tv/video/play/id/9691/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-02-23",
        "href": "https://v.aniu.tv/video/play/id/9693/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-02-22",
        "href": "https://v.aniu.tv/video/play/id/9672/t/2.shtml"
      },
      {
        "innerHTML": "昨日华尔街2017-02-21",
        "href": "https://v.aniu.tv/video/play/id/9627/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-02-21",
        "href": "https://v.aniu.tv/video/play/id/9643/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-02-16",
        "href": "https://v.aniu.tv/video/play/id/9526/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-02-16",
        "href": "https://v.aniu.tv/video/play/id/9527/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-02-16",
        "href": "https://v.aniu.tv/video/play/id/9528/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-02-15",
        "href": "https://v.aniu.tv/video/play/id/9504/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-02-14",
        "href": "https://v.aniu.tv/video/play/id/9479/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-02-09",
        "href": "https://v.aniu.tv/video/play/id/9367/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-02-09",
        "href": "https://v.aniu.tv/video/play/id/9368/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-02-09",
        "href": "https://v.aniu.tv/video/play/id/9369/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-02-08",
        "href": "https://v.aniu.tv/video/play/id/9351/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-02-07",
        "href": "https://v.aniu.tv/video/play/id/9322/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-01-26",
        "href": "https://v.aniu.tv/video/play/id/9207/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-01-26",
        "href": "https://v.aniu.tv/video/play/id/9208/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-01-26",
        "href": "https://v.aniu.tv/video/play/id/9209/t/2.shtml"
      },
      {
        "innerHTML": "薛松：如何运用RSI指标抓涨停",
        "href": "https://v.aniu.tv/video/play/id/9217/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-01-25",
        "href": "https://v.aniu.tv/video/play/id/9199/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-01-24",
        "href": "https://v.aniu.tv/video/play/id/9181/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-01-19",
        "href": "https://v.aniu.tv/video/play/id/9061/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-01-19",
        "href": "https://v.aniu.tv/video/play/id/9062/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-01-19",
        "href": "https://v.aniu.tv/video/play/id/9063/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-01-18",
        "href": "https://v.aniu.tv/video/play/id/9045/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-01-17",
        "href": "https://v.aniu.tv/video/play/id/8998/t/2.shtml"
      },
      {
        "innerHTML": "薛松：KDJ的底部抬高信号",
        "href": "https://v.aniu.tv/video/play/id/8920/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-01-12",
        "href": "https://v.aniu.tv/video/play/id/8912/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-01-12",
        "href": "https://v.aniu.tv/video/play/id/8911/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-01-12",
        "href": "https://v.aniu.tv/video/play/id/8910/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "为什么公司开始做食品级二氧化碳？",
        "href": "https://v.aniu.tv/video/play/id/20923/t/2.shtml"
      },
      {
        "innerHTML": "互联网医院没有法律法规限制，这样的投资机会安全吗？",
        "href": "https://v.aniu.tv/video/play/id/20924/t/2.shtml"
      },
      {
        "innerHTML": "市场以来一大波反弹吗？",
        "href": "https://v.aniu.tv/video/play/id/20925/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-06-06",
        "href": "https://v.aniu.tv/video/play/id/20897/t/2.shtml"
      },
      {
        "innerHTML": "创业板位置适合介入吗？",
        "href": "https://v.aniu.tv/video/play/id/20871/t/2.shtml"
      },
      {
        "innerHTML": "风电会跟光伏发电一样吗？",
        "href": "https://v.aniu.tv/video/play/id/20872/t/2.shtml"
      },
      {
        "innerHTML": "补贴应该怎么补贴",
        "href": "https://v.aniu.tv/video/play/id/20873/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-06-05",
        "href": "https://v.aniu.tv/video/play/id/20853/t/2.shtml"
      },
      {
        "innerHTML": "怎样找到买点",
        "href": "https://v.aniu.tv/video/play/id/20678/t/2.shtml"
      },
      {
        "innerHTML": "买点在哪里",
        "href": "https://v.aniu.tv/video/play/id/20677/t/2.shtml"
      },
      {
        "innerHTML": "中国的股票尽量不要看净资产收益率",
        "href": "https://v.aniu.tv/video/play/id/20679/t/2.shtml"
      },
      {
        "innerHTML": "这次抄底抄的不值钱",
        "href": "https://v.aniu.tv/video/play/id/20680/t/2.shtml"
      },
      {
        "innerHTML": "反弹无量，先观察！",
        "href": "https://v.aniu.tv/video/play/id/20681/t/2.shtml"
      },
      {
        "innerHTML": "林整华看好低位的次新股",
        "href": "https://v.aniu.tv/video/play/id/20684/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-05-31",
        "href": "https://v.aniu.tv/video/play/id/20663/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-05-31",
        "href": "https://v.aniu.tv/video/play/id/20664/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-05-31",
        "href": "https://v.aniu.tv/video/play/id/20665/t/2.shtml"
      },
      {
        "innerHTML": "怎么把握买入的机会？",
        "href": "https://v.aniu.tv/video/play/id/20639/t/2.shtml"
      },
      {
        "innerHTML": "新增风电业务的对公司的影响",
        "href": "https://v.aniu.tv/video/play/id/20653/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-05-30",
        "href": "https://v.aniu.tv/video/play/id/20625/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-05-29",
        "href": "https://v.aniu.tv/video/play/id/20605/t/2.shtml"
      },
      {
        "innerHTML": "薛松：大盘出现这些形态一定要先离场",
        "href": "https://v.aniu.tv/video/play/id/20467/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-05-24",
        "href": "https://v.aniu.tv/video/play/id/20377/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-05-24",
        "href": "https://v.aniu.tv/video/play/id/20378/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-05-24",
        "href": "https://v.aniu.tv/video/play/id/20379/t/2.shtml"
      },
      {
        "innerHTML": "盘面仍然处于强势状态",
        "href": "https://v.aniu.tv/video/play/id/20417/t/2.shtml"
      },
      {
        "innerHTML": "市场状态如何改变",
        "href": "https://v.aniu.tv/video/play/id/20418/t/2.shtml"
      },
      {
        "innerHTML": "读懂创业板指数",
        "href": "https://v.aniu.tv/video/play/id/20419/t/2.shtml"
      },
      {
        "innerHTML": "市场见底的三个信号",
        "href": "https://v.aniu.tv/video/play/id/20420/t/2.shtml"
      },
      {
        "innerHTML": "薛松教你用总资产收益率选牛股！",
        "href": "https://v.aniu.tv/video/play/id/20423/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-05-23",
        "href": "https://v.aniu.tv/video/play/id/20351/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-05-22",
        "href": "https://v.aniu.tv/video/play/id/20328/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-05-17",
        "href": "https://v.aniu.tv/video/play/id/20209/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-05-17",
        "href": "https://v.aniu.tv/video/play/id/20210/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-05-17",
        "href": "https://v.aniu.tv/video/play/id/20211/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-05-16",
        "href": "https://v.aniu.tv/video/play/id/20185/t/2.shtml"
      },
      {
        "innerHTML": "薛松看好涨价概念股",
        "href": "https://v.aniu.tv/video/play/id/20039/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-05-10",
        "href": "https://v.aniu.tv/video/play/id/20025/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-05-10",
        "href": "https://v.aniu.tv/video/play/id/20026/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "财富广角2017-06-29",
        "href": "https://v.aniu.tv/video/play/id/12550/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-06-28",
        "href": "https://v.aniu.tv/video/play/id/12533/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-06-27",
        "href": "https://v.aniu.tv/video/play/id/12504/t/2.shtml"
      },
      {
        "innerHTML": "薛松：绝不补仓才能在股市里生存",
        "href": "https://v.aniu.tv/video/play/id/12483/t/2.shtml"
      },
      {
        "innerHTML": "主题投资2017-06-26",
        "href": "https://v.aniu.tv/video/play/id/12472/t/2.shtml"
      },
      {
        "innerHTML": "薛松：真正的交易者是怎么操作的",
        "href": "https://v.aniu.tv/video/play/id/12390/t/2.shtml"
      },
      {
        "innerHTML": "薛松：7月过后3000点将再也不见",
        "href": "https://v.aniu.tv/video/play/id/12395/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-06-22",
        "href": "https://v.aniu.tv/video/play/id/12378/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-06-22",
        "href": "https://v.aniu.tv/video/play/id/12379/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-06-22",
        "href": "https://v.aniu.tv/video/play/id/12380/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-06-21",
        "href": "https://v.aniu.tv/video/play/id/12360/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-06-20",
        "href": "https://v.aniu.tv/video/play/id/12327/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-06-15",
        "href": "https://v.aniu.tv/video/play/id/12212/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-06-15",
        "href": "https://v.aniu.tv/video/play/id/12213/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-06-15",
        "href": "https://v.aniu.tv/video/play/id/12214/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-06-14",
        "href": "https://v.aniu.tv/video/play/id/12203/t/2.shtml"
      },
      {
        "innerHTML": "昨夜华尔街2017-06-13",
        "href": "https://v.aniu.tv/video/play/id/12144/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2017-06-13",
        "href": "https://v.aniu.tv/video/play/id/12145/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-06-13",
        "href": "https://v.aniu.tv/video/play/id/12162/t/2.shtml"
      },
      {
        "innerHTML": "薛松：特力A的涨停其实是投石问路",
        "href": "https://v.aniu.tv/video/play/id/12050/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-06-08",
        "href": "https://v.aniu.tv/video/play/id/12059/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-06-08",
        "href": "https://v.aniu.tv/video/play/id/12060/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-06-08",
        "href": "https://v.aniu.tv/video/play/id/12061/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-06-07",
        "href": "https://v.aniu.tv/video/play/id/12038/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-06-06",
        "href": "https://v.aniu.tv/video/play/id/12011/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-06-01",
        "href": "https://v.aniu.tv/video/play/id/11883/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-06-01",
        "href": "https://v.aniu.tv/video/play/id/11885/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-06-01",
        "href": "https://v.aniu.tv/video/play/id/11884/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-05-31",
        "href": "https://v.aniu.tv/video/play/id/11874/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-05-24",
        "href": "https://v.aniu.tv/video/play/id/11773/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-05-23",
        "href": "https://v.aniu.tv/video/play/id/11744/t/2.shtml"
      },
      {
        "innerHTML": "薛松：地量水平该如何判断",
        "href": "https://v.aniu.tv/video/play/id/11626/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-05-18",
        "href": "https://v.aniu.tv/video/play/id/11622/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-05-18",
        "href": "https://v.aniu.tv/video/play/id/11623/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-05-18",
        "href": "https://v.aniu.tv/video/play/id/11625/t/2.shtml"
      },
      {
        "innerHTML": "昨夜华尔街2017-05-17",
        "href": "https://v.aniu.tv/video/play/id/11585/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2017-05-17",
        "href": "https://v.aniu.tv/video/play/id/11586/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-05-17",
        "href": "https://v.aniu.tv/video/play/id/11613/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-05-16",
        "href": "https://v.aniu.tv/video/play/id/11573/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：券商可以嚣张点",
        "href": "https://v.aniu.tv/video/play/id/68587/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美元空手套白狼，到岸价格提升迫在眉睫",
        "href": "https://v.aniu.tv/video/play/id/68601/t/2.shtml"
      },
      {
        "innerHTML": "薛松：既定法则下的方向",
        "href": "https://v.aniu.tv/video/play/id/68605/t/2.shtml"
      },
      {
        "innerHTML": "薛松：均线的真正用处",
        "href": "https://v.aniu.tv/video/play/id/68627/t/2.shtml"
      },
      {
        "innerHTML": "薛松：问题不随意接  盘中不随意“下注”",
        "href": "https://v.aniu.tv/video/play/id/68628/t/2.shtml"
      },
      {
        "innerHTML": "薛松：半导体进入认同阶段，还在看空？",
        "href": "https://v.aniu.tv/video/play/id/68630/t/2.shtml"
      },
      {
        "innerHTML": "薛松：他们是15年的幸存者！",
        "href": "https://v.aniu.tv/video/play/id/68631/t/2.shtml"
      },
      {
        "innerHTML": "薛松：长者为尊 向老股民致敬",
        "href": "https://v.aniu.tv/video/play/id/68632/t/2.shtml"
      },
      {
        "innerHTML": "薛松：部分真相是谎言 新闻是非常态！",
        "href": "https://v.aniu.tv/video/play/id/68633/t/2.shtml"
      },
      {
        "innerHTML": "薛松：光伏政策即价值投资",
        "href": "https://v.aniu.tv/video/play/id/68634/t/2.shtml"
      },
      {
        "innerHTML": "薛松：价值投资行为 买进便宜的好公司",
        "href": "https://v.aniu.tv/video/play/id/68635/t/2.shtml"
      },
      {
        "innerHTML": "薛松：全世界的通胀没有起来，感谢中国！",
        "href": "https://v.aniu.tv/video/play/id/68636/t/2.shtml"
      },
      {
        "innerHTML": "薛松：交出的业绩不值得研究，聪明人都是做预期！",
        "href": "https://v.aniu.tv/video/play/id/68637/t/2.shtml"
      },
      {
        "innerHTML": "薛松：光伏需求弹性数据，谁写准跟谁一辈子！",
        "href": "https://v.aniu.tv/video/play/id/68641/t/2.shtml"
      },
      {
        "innerHTML": "薛松：老股民了，没有转折和低位的不敢追",
        "href": "https://v.aniu.tv/video/play/id/68643/t/2.shtml"
      },
      {
        "innerHTML": "薛松：人有黄金时，你也可超时",
        "href": "https://v.aniu.tv/video/play/id/68477/t/2.shtml"
      },
      {
        "innerHTML": "薛松：基于转基因逻辑下的机会",
        "href": "https://v.aniu.tv/video/play/id/68491/t/2.shtml"
      },
      {
        "innerHTML": "薛松：价值投资的活学活用",
        "href": "https://v.aniu.tv/video/play/id/67920/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为三哥发声，四千点会到的",
        "href": "https://v.aniu.tv/video/play/id/67926/t/2.shtml"
      },
      {
        "innerHTML": "薛松：上证50让你跌，你能跌多少",
        "href": "https://v.aniu.tv/video/play/id/67939/t/2.shtml"
      },
      {
        "innerHTML": "薛松：我想为这些公司点赞，也值得点赞",
        "href": "https://v.aniu.tv/video/play/id/67944/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你知道C浪的可怕吗？快深狠",
        "href": "https://v.aniu.tv/video/play/id/67805/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美元、A股背道而驰",
        "href": "https://v.aniu.tv/video/play/id/67806/t/2.shtml"
      },
      {
        "innerHTML": "薛松：中枢位置3480，告诉你们了",
        "href": "https://v.aniu.tv/video/play/id/67819/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你是投资者，你该这么想",
        "href": "https://v.aniu.tv/video/play/id/67844/t/2.shtml"
      },
      {
        "innerHTML": "薛松：最简单的方法，线上持股，线下持币",
        "href": "https://v.aniu.tv/video/play/id/67845/t/2.shtml"
      },
      {
        "innerHTML": "薛松：正视自己，凭运气赚的钱将来会凭实力吐回去",
        "href": "https://v.aniu.tv/video/play/id/67424/t/2.shtml"
      },
      {
        "innerHTML": "薛松：难道是疫情越严重指数越涨飞？",
        "href": "https://v.aniu.tv/video/play/id/67447/t/2.shtml"
      },
      {
        "innerHTML": "薛松：盘面似曾相识，我准备错一次",
        "href": "https://v.aniu.tv/video/play/id/67282/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美元单边大跌，大盘4000不是梦",
        "href": "https://v.aniu.tv/video/play/id/67283/t/2.shtml"
      },
      {
        "innerHTML": "薛松：疫情终会过去，电影终会崛起",
        "href": "https://v.aniu.tv/video/play/id/67295/t/2.shtml"
      },
      {
        "innerHTML": "薛松：股价波动对你有何影响？",
        "href": "https://v.aniu.tv/video/play/id/66784/t/2.shtml"
      },
      {
        "innerHTML": "薛松：调整100点？你应庆幸手中的“不幸”",
        "href": "https://v.aniu.tv/video/play/id/66785/t/2.shtml"
      },
      {
        "innerHTML": "薛松：锂资源的机会与限制",
        "href": "https://v.aniu.tv/video/play/id/66789/t/2.shtml"
      },
      {
        "innerHTML": "薛松：股票为什么会涨？",
        "href": "https://v.aniu.tv/video/play/id/66790/t/2.shtml"
      },
      {
        "innerHTML": "薛松：六一已至 市场怎么看？",
        "href": "https://v.aniu.tv/video/play/id/66697/t/2.shtml"
      },
      {
        "innerHTML": "薛松：钓鱼线出现了吗？",
        "href": "https://v.aniu.tv/video/play/id/66698/t/2.shtml"
      },
      {
        "innerHTML": "薛松：跑马的故事",
        "href": "https://v.aniu.tv/video/play/id/66699/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这两项武功千万不能学！",
        "href": "https://v.aniu.tv/video/play/id/66734/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告-2015-06-05",
        "href": "https://v.aniu.tv/video/play/id/512/t/2.shtml"
      },
      {
        "innerHTML": "热点公告-2015-06-04",
        "href": "https://v.aniu.tv/video/play/id/498/t/2.shtml"
      },
      {
        "innerHTML": "热点公告-2015-06-03",
        "href": "https://v.aniu.tv/video/play/id/500/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2020-05-12",
        "href": "https://v.aniu.tv/video/play/id/47791/t/2.shtml"
      },
      {
        "innerHTML": "薛松：在市场操作要知进退",
        "href": "https://v.aniu.tv/video/play/id/47660/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-05-07",
        "href": "https://v.aniu.tv/video/play/id/47596/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-05-07",
        "href": "https://v.aniu.tv/video/play/id/47597/t/2.shtml"
      },
      {
        "innerHTML": "缪竹梁：指数稳步上行，可适当调高风偏",
        "href": "https://v.aniu.tv/video/play/id/47594/t/2.shtml"
      },
      {
        "innerHTML": "缪竹梁：4 月“大吊车”实战课成功率高达90%，原因竟然是？",
        "href": "https://v.aniu.tv/video/play/id/47607/t/2.shtml"
      },
      {
        "innerHTML": "薛松：沉默了  中国石油 选它 是因为它不正常",
        "href": "https://v.aniu.tv/video/play/id/47562/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-05-06",
        "href": "https://v.aniu.tv/video/play/id/47543/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-04-29",
        "href": "https://v.aniu.tv/video/play/id/47446/t/2.shtml"
      },
      {
        "innerHTML": "薛松：沉默了  中国石油 选它 是因为它不正常",
        "href": "https://v.aniu.tv/video/play/id/47420/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-04-28",
        "href": "https://v.aniu.tv/video/play/id/47401/t/2.shtml"
      },
      {
        "innerHTML": "薛松：三三法则-有人说我票不正常",
        "href": "https://v.aniu.tv/video/play/id/47370/t/2.shtml"
      },
      {
        "innerHTML": "薛松：细处看出心态",
        "href": "https://v.aniu.tv/video/play/id/47368/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2020-04-27",
        "href": "https://v.aniu.tv/video/play/id/47355/t/2.shtml"
      },
      {
        "innerHTML": "择时有技2020-04-27",
        "href": "https://v.aniu.tv/video/play/id/47356/t/2.shtml"
      },
      {
        "innerHTML": "股谈老司机2020-04-27",
        "href": "https://v.aniu.tv/video/play/id/47357/t/2.shtml"
      },
      {
        "innerHTML": "薛松：每天开盘一小时 确认对方威猛 还是一路挨打",
        "href": "https://v.aniu.tv/video/play/id/47324/t/2.shtml"
      },
      {
        "innerHTML": "薛叨叨：要满仓 你天天打人 不行就跑",
        "href": "https://v.aniu.tv/video/play/id/47271/t/2.shtml"
      },
      {
        "innerHTML": "薛松：我们等这一天很久了 你们自己看着办吧",
        "href": "https://v.aniu.tv/video/play/id/47205/t/2.shtml"
      },
      {
        "innerHTML": "薛松：黄金未来如此确定  只做一天你就错了",
        "href": "https://v.aniu.tv/video/play/id/47170/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-04-22",
        "href": "https://v.aniu.tv/video/play/id/47166/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你真觉得是个大机会 那就是叫没得做",
        "href": "https://v.aniu.tv/video/play/id/47092/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-04-21",
        "href": "https://v.aniu.tv/video/play/id/47110/t/2.shtml"
      },
      {
        "innerHTML": "薛松：开空单 这钱我不要了 我决定去赌期权",
        "href": "https://v.aniu.tv/video/play/id/47046/t/2.shtml"
      },
      {
        "innerHTML": "交易封神榜2020-04-19",
        "href": "https://v.aniu.tv/video/play/id/47063/t/2.shtml"
      },
      {
        "innerHTML": "下周透视2020-04-19",
        "href": "https://v.aniu.tv/video/play/id/47064/t/2.shtml"
      },
      {
        "innerHTML": "强势追踪2020-04-19",
        "href": "https://v.aniu.tv/video/play/id/47065/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-04-15",
        "href": "https://v.aniu.tv/video/play/id/46909/t/2.shtml"
      },
      {
        "innerHTML": "薛松：散户越没能力越爱单押",
        "href": "https://v.aniu.tv/video/play/id/46859/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-04-14",
        "href": "https://v.aniu.tv/video/play/id/46840/t/2.shtml"
      },
      {
        "innerHTML": "薛松：选十个股票，涨停九个，买了唯一跌停的",
        "href": "https://v.aniu.tv/video/play/id/46810/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-04-13",
        "href": "https://v.aniu.tv/video/play/id/46841/t/2.shtml"
      },
      {
        "innerHTML": "薛松：短期热度起来了 没买后悔？ 拿一周试试",
        "href": "https://v.aniu.tv/video/play/id/46735/t/2.shtml"
      },
      {
        "innerHTML": "薛松：我牛 不是的  是深圳那边有鱼",
        "href": "https://v.aniu.tv/video/play/id/46668/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-04-09",
        "href": "https://v.aniu.tv/video/play/id/46674/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-04-09",
        "href": "https://v.aniu.tv/video/play/id/46675/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-04-09",
        "href": "https://v.aniu.tv/video/play/id/46676/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-04-09",
        "href": "https://v.aniu.tv/video/play/id/46677/t/2.shtml"
      },
      {
        "innerHTML": "申振：关注跨界题材，无需过度追求RCS类股票！",
        "href": "https://v.aniu.tv/video/play/id/46683/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2016-01-14",
        "href": "https://v.aniu.tv/video/play/id/2399/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-01-14",
        "href": "https://v.aniu.tv/video/play/id/2357/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-01-13",
        "href": "https://v.aniu.tv/video/play/id/2351/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-01-07",
        "href": "https://v.aniu.tv/video/play/id/2260/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-01-07",
        "href": "https://v.aniu.tv/video/play/id/2261/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-01-07",
        "href": "https://v.aniu.tv/video/play/id/2262/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-01-07",
        "href": "https://v.aniu.tv/video/play/id/2269/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-01-06",
        "href": "https://v.aniu.tv/video/play/id/2242/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-01-02",
        "href": "https://v.aniu.tv/video/play/id/2585/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-12-30",
        "href": "https://v.aniu.tv/video/play/id/2176/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-12-28",
        "href": "https://v.aniu.tv/video/play/id/2144/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2015-12-24",
        "href": "https://v.aniu.tv/video/play/id/2078/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2015-12-24",
        "href": "https://v.aniu.tv/video/play/id/2079/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2015-12-24",
        "href": "https://v.aniu.tv/video/play/id/2080/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-12-24",
        "href": "https://v.aniu.tv/video/play/id/2096/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-12-23",
        "href": "https://v.aniu.tv/video/play/id/2071/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-12-21",
        "href": "https://v.aniu.tv/video/play/id/2028/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2015-12-18",
        "href": "https://v.aniu.tv/video/play/id/1978/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-12-18",
        "href": "https://v.aniu.tv/video/play/id/1992/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2015-12-17",
        "href": "https://v.aniu.tv/video/play/id/1969/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2015-12-17",
        "href": "https://v.aniu.tv/video/play/id/1970/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2015-12-17",
        "href": "https://v.aniu.tv/video/play/id/1971/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-12-16",
        "href": "https://v.aniu.tv/video/play/id/1962/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-12-14",
        "href": "https://v.aniu.tv/video/play/id/1928/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-12-11",
        "href": "https://v.aniu.tv/video/play/id/1906/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2015-12-10",
        "href": "https://v.aniu.tv/video/play/id/1858/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2015-12-10",
        "href": "https://v.aniu.tv/video/play/id/1859/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2015-12-10",
        "href": "https://v.aniu.tv/video/play/id/1860/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-12-10",
        "href": "https://v.aniu.tv/video/play/id/1867/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-12-09",
        "href": "https://v.aniu.tv/video/play/id/1852/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-12-07",
        "href": "https://v.aniu.tv/video/play/id/1822/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2015-12-03",
        "href": "https://v.aniu.tv/video/play/id/1753/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2015-12-03",
        "href": "https://v.aniu.tv/video/play/id/1754/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2015-12-03",
        "href": "https://v.aniu.tv/video/play/id/1755/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-12-03",
        "href": "https://v.aniu.tv/video/play/id/1762/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-30",
        "href": "https://v.aniu.tv/video/play/id/1705/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-27",
        "href": "https://v.aniu.tv/video/play/id/1660/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-26",
        "href": "https://v.aniu.tv/video/play/id/1644/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2015-11-26",
        "href": "https://v.aniu.tv/video/play/id/1635/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2017-08-16",
        "href": "https://v.aniu.tv/video/play/id/13681/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-08-15",
        "href": "https://v.aniu.tv/video/play/id/13661/t/2.shtml"
      },
      {
        "innerHTML": "薛松：月线的顶底分型",
        "href": "https://v.aniu.tv/video/play/id/13554/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-08-10",
        "href": "https://v.aniu.tv/video/play/id/13539/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-08-10",
        "href": "https://v.aniu.tv/video/play/id/13540/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-08-10",
        "href": "https://v.aniu.tv/video/play/id/13541/t/2.shtml"
      },
      {
        "innerHTML": "薛松：做投资最重要的就是信念",
        "href": "https://v.aniu.tv/video/play/id/13551/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-08-09",
        "href": "https://v.aniu.tv/video/play/id/13531/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-08-08",
        "href": "https://v.aniu.tv/video/play/id/13501/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-08-03",
        "href": "https://v.aniu.tv/video/play/id/13379/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-08-03",
        "href": "https://v.aniu.tv/video/play/id/13380/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-08-03",
        "href": "https://v.aniu.tv/video/play/id/13381/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-08-02",
        "href": "https://v.aniu.tv/video/play/id/13369/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-08-01",
        "href": "https://v.aniu.tv/video/play/id/13336/t/2.shtml"
      },
      {
        "innerHTML": "薛松：Fama-French三因子模型",
        "href": "https://v.aniu.tv/video/play/id/13211/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-07-27",
        "href": "https://v.aniu.tv/video/play/id/13204/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-07-27",
        "href": "https://v.aniu.tv/video/play/id/13205/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-07-27",
        "href": "https://v.aniu.tv/video/play/id/13206/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-07-26",
        "href": "https://v.aniu.tv/video/play/id/13193/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-07-25",
        "href": "https://v.aniu.tv/video/play/id/13164/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-07-20",
        "href": "https://v.aniu.tv/video/play/id/13041/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-07-20",
        "href": "https://v.aniu.tv/video/play/id/13042/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-07-20",
        "href": "https://v.aniu.tv/video/play/id/13043/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-07-19",
        "href": "https://v.aniu.tv/video/play/id/13029/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-07-18",
        "href": "https://v.aniu.tv/video/play/id/13001/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-07-13",
        "href": "https://v.aniu.tv/video/play/id/12879/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-07-13",
        "href": "https://v.aniu.tv/video/play/id/12880/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-07-13",
        "href": "https://v.aniu.tv/video/play/id/12881/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-07-12",
        "href": "https://v.aniu.tv/video/play/id/12869/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-07-11",
        "href": "https://v.aniu.tv/video/play/id/12840/t/2.shtml"
      },
      {
        "innerHTML": "薛松：大牛股方大炭素是怎样炼成的",
        "href": "https://v.aniu.tv/video/play/id/12731/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-07-06",
        "href": "https://v.aniu.tv/video/play/id/12717/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-07-06",
        "href": "https://v.aniu.tv/video/play/id/12718/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-07-06",
        "href": "https://v.aniu.tv/video/play/id/12719/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-07-05",
        "href": "https://v.aniu.tv/video/play/id/12704/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-07-04",
        "href": "https://v.aniu.tv/video/play/id/12670/t/2.shtml"
      },
      {
        "innerHTML": "薛松：煤炭板块的走强早有预兆",
        "href": "https://v.aniu.tv/video/play/id/12567/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-06-29",
        "href": "https://v.aniu.tv/video/play/id/12548/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-06-29",
        "href": "https://v.aniu.tv/video/play/id/12549/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：学会空杯效应，投资路上才能走更远",
        "href": "https://v.aniu.tv/video/play/id/45399/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-03-04",
        "href": "https://v.aniu.tv/video/play/id/45544/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-03-03",
        "href": "https://v.aniu.tv/video/play/id/45381/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2020-03-03",
        "href": "https://v.aniu.tv/video/play/id/45382/t/2.shtml"
      },
      {
        "innerHTML": "首席投顾2020-03-03",
        "href": "https://v.aniu.tv/video/play/id/45383/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-03-03",
        "href": "https://v.aniu.tv/video/play/id/45416/t/2.shtml"
      },
      {
        "innerHTML": "薛松：赚钱最重要的是指数环境",
        "href": "https://v.aniu.tv/video/play/id/45512/t/2.shtml"
      },
      {
        "innerHTML": "薛松:印度蝗灾过去了？在我这没过去",
        "href": "https://v.aniu.tv/video/play/id/45346/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2020-03-02",
        "href": "https://v.aniu.tv/video/play/id/45336/t/2.shtml"
      },
      {
        "innerHTML": "择时有技2020-03-02",
        "href": "https://v.aniu.tv/video/play/id/45337/t/2.shtml"
      },
      {
        "innerHTML": "季梦杰：新基建的机会来了？只待市场确认！",
        "href": "https://v.aniu.tv/video/play/id/45492/t/2.shtml"
      },
      {
        "innerHTML": "薛松：满头大汗的投资者跑到股市先不要买股票",
        "href": "https://v.aniu.tv/video/play/id/45493/t/2.shtml"
      },
      {
        "innerHTML": "薛百科：蝗虫 沙漠蝗 草原蝗",
        "href": "https://v.aniu.tv/video/play/id/45326/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-02-28",
        "href": "https://v.aniu.tv/video/play/id/45312/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2020-02-28",
        "href": "https://v.aniu.tv/video/play/id/45313/t/2.shtml"
      },
      {
        "innerHTML": "首席投顾2020-02-28",
        "href": "https://v.aniu.tv/video/play/id/45314/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-02-28",
        "href": "https://v.aniu.tv/video/play/id/45324/t/2.shtml"
      },
      {
        "innerHTML": "张津铭：止跌与否关键看经济数据",
        "href": "https://v.aniu.tv/video/play/id/45452/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-02-27",
        "href": "https://v.aniu.tv/video/play/id/45280/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-02-27",
        "href": "https://v.aniu.tv/video/play/id/45281/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-02-27",
        "href": "https://v.aniu.tv/video/play/id/45282/t/2.shtml"
      },
      {
        "innerHTML": "顾爱珺：耐心等待科技类个股夯实现阶段底部，寻找合适的入场时机！",
        "href": "https://v.aniu.tv/video/play/id/45430/t/2.shtml"
      },
      {
        "innerHTML": "顾爱珺：科技股没有结束 震荡向上",
        "href": "https://v.aniu.tv/video/play/id/45431/t/2.shtml"
      },
      {
        "innerHTML": "顾爱珺：放量上涨缩量小跌 结构良好",
        "href": "https://v.aniu.tv/video/play/id/45432/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-02-26",
        "href": "https://v.aniu.tv/video/play/id/45252/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2020-02-26",
        "href": "https://v.aniu.tv/video/play/id/45253/t/2.shtml"
      },
      {
        "innerHTML": "首席投顾2020-02-26",
        "href": "https://v.aniu.tv/video/play/id/45254/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-02-26",
        "href": "https://v.aniu.tv/video/play/id/45255/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2020-02-25",
        "href": "https://v.aniu.tv/video/play/id/45210/t/2.shtml"
      },
      {
        "innerHTML": "择时有技2020-02-25",
        "href": "https://v.aniu.tv/video/play/id/45211/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-02-25",
        "href": "https://v.aniu.tv/video/play/id/45239/t/2.shtml"
      },
      {
        "innerHTML": "薛松：没有一种风格是永远可以赚钱的",
        "href": "https://v.aniu.tv/video/play/id/45160/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-02-24",
        "href": "https://v.aniu.tv/video/play/id/45154/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2020-02-24",
        "href": "https://v.aniu.tv/video/play/id/45156/t/2.shtml"
      },
      {
        "innerHTML": "首席投顾2020-02-24",
        "href": "https://v.aniu.tv/video/play/id/45157/t/2.shtml"
      },
      {
        "innerHTML": "薛松：学会空杯效应，投资路上才能走更远",
        "href": "https://v.aniu.tv/video/play/id/45167/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2020-02-21",
        "href": "https://v.aniu.tv/video/play/id/45071/t/2.shtml"
      },
      {
        "innerHTML": "择时有技2020-02-21",
        "href": "https://v.aniu.tv/video/play/id/45072/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-02-20",
        "href": "https://v.aniu.tv/video/play/id/45016/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：何为赛道？待在道上！",
        "href": "https://v.aniu.tv/video/play/id/71872/t/2.shtml"
      },
      {
        "innerHTML": "薛松：跌时重质涨时重势 何时选银行？",
        "href": "https://v.aniu.tv/video/play/id/71883/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数3-5周下不去",
        "href": "https://v.aniu.tv/video/play/id/71891/t/2.shtml"
      },
      {
        "innerHTML": "薛松：恐慌性缩量是如何形成的",
        "href": "https://v.aniu.tv/video/play/id/71909/t/2.shtml"
      },
      {
        "innerHTML": "薛松：炒股如玩游戏！要有提前预备！",
        "href": "https://v.aniu.tv/video/play/id/71676/t/2.shtml"
      },
      {
        "innerHTML": "薛松：敢问跌到何处 无非是找心理安慰",
        "href": "https://v.aniu.tv/video/play/id/71677/t/2.shtml"
      },
      {
        "innerHTML": "薛松：成交量大亦不大 关键看匹配度",
        "href": "https://v.aniu.tv/video/play/id/71678/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你凭何说这是C浪？",
        "href": "https://v.aniu.tv/video/play/id/71679/t/2.shtml"
      },
      {
        "innerHTML": "薛松：学会留余量 七月过去八月静好",
        "href": "https://v.aniu.tv/video/play/id/71681/t/2.shtml"
      },
      {
        "innerHTML": "薛松：PCB板块机会可以明确！",
        "href": "https://v.aniu.tv/video/play/id/71682/t/2.shtml"
      },
      {
        "innerHTML": "薛松：处置效应下的正常与不正常",
        "href": "https://v.aniu.tv/video/play/id/71730/t/2.shtml"
      },
      {
        "innerHTML": "薛松：高喊高看200点！",
        "href": "https://v.aniu.tv/video/play/id/71748/t/2.shtml"
      },
      {
        "innerHTML": "薛松：市场底在政策底后 中信证券喊话有何意味？",
        "href": "https://v.aniu.tv/video/play/id/71750/t/2.shtml"
      },
      {
        "innerHTML": "薛松：细数长流！看破不道破的哲学！",
        "href": "https://v.aniu.tv/video/play/id/71751/t/2.shtml"
      },
      {
        "innerHTML": "薛松：博弈好比郭德纲借充电宝",
        "href": "https://v.aniu.tv/video/play/id/71755/t/2.shtml"
      },
      {
        "innerHTML": "薛松：炒作蓝筹或是胡作非为！别说自己是价值投资！",
        "href": "https://v.aniu.tv/video/play/id/71761/t/2.shtml"
      },
      {
        "innerHTML": "薛松：部分投资者爱在浑水里游泳",
        "href": "https://v.aniu.tv/video/play/id/71762/t/2.shtml"
      },
      {
        "innerHTML": "薛松：缩量看结构 放量则不然",
        "href": "https://v.aniu.tv/video/play/id/71178/t/2.shtml"
      },
      {
        "innerHTML": "薛松：论BOLL线的精髓 其深意需推敲",
        "href": "https://v.aniu.tv/video/play/id/71179/t/2.shtml"
      },
      {
        "innerHTML": "薛松：早前A股为何没有长牛？",
        "href": "https://v.aniu.tv/video/play/id/71182/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数下跌空间有限 大盘股亦是如此",
        "href": "https://v.aniu.tv/video/play/id/71057/t/2.shtml"
      },
      {
        "innerHTML": "薛松：新手胜于胆，老手则无胆",
        "href": "https://v.aniu.tv/video/play/id/71058/t/2.shtml"
      },
      {
        "innerHTML": "薛松：高跟鞋形态之精髓",
        "href": "https://v.aniu.tv/video/play/id/71078/t/2.shtml"
      },
      {
        "innerHTML": "薛松：天赋难得 工作与教育难两全",
        "href": "https://v.aniu.tv/video/play/id/71080/t/2.shtml"
      },
      {
        "innerHTML": "薛松：面对内幕消息的两种应对方法",
        "href": "https://v.aniu.tv/video/play/id/71111/t/2.shtml"
      },
      {
        "innerHTML": "薛松：顶底的魅力！细品你适合左侧还是右侧",
        "href": "https://v.aniu.tv/video/play/id/70863/t/2.shtml"
      },
      {
        "innerHTML": "薛松：顶底的魅力！细品你适合左侧还是右侧！",
        "href": "https://v.aniu.tv/video/play/id/70386/t/2.shtml"
      },
      {
        "innerHTML": "薛松：见量是左侧 缩量是个事！",
        "href": "https://v.aniu.tv/video/play/id/70388/t/2.shtml"
      },
      {
        "innerHTML": "薛松：怕踏空者套牢，怕套牢者踏空",
        "href": "https://v.aniu.tv/video/play/id/70389/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美元并不强势 两大理由支撑",
        "href": "https://v.aniu.tv/video/play/id/70390/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为何本周一必然不会跳水？",
        "href": "https://v.aniu.tv/video/play/id/70401/t/2.shtml"
      },
      {
        "innerHTML": "薛松：手把手教你道氏理论",
        "href": "https://v.aniu.tv/video/play/id/70402/t/2.shtml"
      },
      {
        "innerHTML": "薛松：技术分析三个碗 你愿意翻吗？",
        "href": "https://v.aniu.tv/video/play/id/70428/t/2.shtml"
      },
      {
        "innerHTML": "薛松：沪市并未放量 真正放量在这个板块",
        "href": "https://v.aniu.tv/video/play/id/69846/t/2.shtml"
      },
      {
        "innerHTML": "薛松：次新股的秘密 当年为何会到6000点？",
        "href": "https://v.aniu.tv/video/play/id/69849/t/2.shtml"
      },
      {
        "innerHTML": "薛松：话B股史 观当今科创！",
        "href": "https://v.aniu.tv/video/play/id/69850/t/2.shtml"
      },
      {
        "innerHTML": "薛松：向来挑剔的他 竟对一科创板公司赞不绝口",
        "href": "https://v.aniu.tv/video/play/id/69867/t/2.shtml"
      },
      {
        "innerHTML": "薛松：次新品种蕴藏大机会！",
        "href": "https://v.aniu.tv/video/play/id/69868/t/2.shtml"
      },
      {
        "innerHTML": "薛松：何时股价涨得快？做对事跟对人！",
        "href": "https://v.aniu.tv/video/play/id/69922/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "量化热点股2018-12-06",
        "href": "https://v.aniu.tv/video/play/id/29693/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-12-06",
        "href": "https://v.aniu.tv/video/play/id/29694/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-12-06",
        "href": "https://v.aniu.tv/video/play/id/29695/t/2.shtml"
      },
      {
        "innerHTML": "薛松：讲解缺口的本质！",
        "href": "https://v.aniu.tv/video/play/id/29671/t/2.shtml"
      },
      {
        "innerHTML": "薛松：如何寻找薛氏通道启动信号？",
        "href": "https://v.aniu.tv/video/play/id/29669/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-12-05",
        "href": "https://v.aniu.tv/video/play/id/29738/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-12-04",
        "href": "https://v.aniu.tv/video/play/id/29610/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-11-28",
        "href": "https://v.aniu.tv/video/play/id/29290/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-11-27",
        "href": "https://v.aniu.tv/video/play/id/29210/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-11-22",
        "href": "https://v.aniu.tv/video/play/id/28929/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-11-22",
        "href": "https://v.aniu.tv/video/play/id/28931/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-11-22",
        "href": "https://v.aniu.tv/video/play/id/28932/t/2.shtml"
      },
      {
        "innerHTML": "薛松：阳包阴的正确用法",
        "href": "https://v.aniu.tv/video/play/id/28914/t/2.shtml"
      },
      {
        "innerHTML": "薛松：旗面整理的涨停为什么不能追？",
        "href": "https://v.aniu.tv/video/play/id/28903/t/2.shtml"
      },
      {
        "innerHTML": "平衡市的抛压真的存在吗？",
        "href": "https://v.aniu.tv/video/play/id/28910/t/2.shtml"
      },
      {
        "innerHTML": "你真的知道你亏损的真正原因吗？",
        "href": "https://v.aniu.tv/video/play/id/28907/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-11-21",
        "href": "https://v.aniu.tv/video/play/id/28897/t/2.shtml"
      },
      {
        "innerHTML": "昨夜华尔街2018-11-20",
        "href": "https://v.aniu.tv/video/play/id/28774/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2018-11-20",
        "href": "https://v.aniu.tv/video/play/id/28775/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-11-20",
        "href": "https://v.aniu.tv/video/play/id/28849/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这是每次操作前必备的工作",
        "href": "https://v.aniu.tv/video/play/id/28753/t/2.shtml"
      },
      {
        "innerHTML": "薛松：黄金盘底蓄势，明年有望创新高！！",
        "href": "https://v.aniu.tv/video/play/id/28748/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-11-14",
        "href": "https://v.aniu.tv/video/play/id/28516/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-11-13",
        "href": "https://v.aniu.tv/video/play/id/28450/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-11-08",
        "href": "https://v.aniu.tv/video/play/id/28149/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-11-08",
        "href": "https://v.aniu.tv/video/play/id/28200/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-11-08",
        "href": "https://v.aniu.tv/video/play/id/28201/t/2.shtml"
      },
      {
        "innerHTML": "反弹格局下，大盘走势往往有这些特征！！",
        "href": "https://v.aniu.tv/video/play/id/28122/t/2.shtml"
      },
      {
        "innerHTML": "能否连板，重点看这两大特征！！",
        "href": "https://v.aniu.tv/video/play/id/28123/t/2.shtml"
      },
      {
        "innerHTML": "薛松：突发性热点介入时机分析",
        "href": "https://v.aniu.tv/video/play/id/28124/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-11-07",
        "href": "https://v.aniu.tv/video/play/id/28134/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-11-06",
        "href": "https://v.aniu.tv/video/play/id/28084/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-10-31",
        "href": "https://v.aniu.tv/video/play/id/27764/t/2.shtml"
      },
      {
        "innerHTML": "昨夜华尔街2018-10-30",
        "href": "https://v.aniu.tv/video/play/id/27631/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2018-10-30",
        "href": "https://v.aniu.tv/video/play/id/27632/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-10-30",
        "href": "https://v.aniu.tv/video/play/id/27696/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美元强势，大宗商品价格回落，涨价股继续承压！！",
        "href": "https://v.aniu.tv/video/play/id/27595/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美股入冬，下跌趋势短期难扭转！",
        "href": "https://v.aniu.tv/video/play/id/27594/t/2.shtml"
      },
      {
        "innerHTML": "公告追踪2018-10-27",
        "href": "https://v.aniu.tv/video/play/id/27555/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "量化热点股2018-03-15",
        "href": "https://v.aniu.tv/video/play/id/18760/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-03-15",
        "href": "https://v.aniu.tv/video/play/id/18761/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-03-15",
        "href": "https://v.aniu.tv/video/play/id/18762/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2018-03-13",
        "href": "https://v.aniu.tv/video/play/id/18694/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-03-13",
        "href": "https://v.aniu.tv/video/play/id/18720/t/2.shtml"
      },
      {
        "innerHTML": "公告追踪2018-03-10",
        "href": "https://v.aniu.tv/video/play/id/18647/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-02-14",
        "href": "https://v.aniu.tv/video/play/id/18226/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-02-13",
        "href": "https://v.aniu.tv/video/play/id/18198/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2018-02-09",
        "href": "https://v.aniu.tv/video/play/id/18111/t/2.shtml"
      },
      {
        "innerHTML": "薛松：超跌反弹应该选择哪种标的",
        "href": "https://v.aniu.tv/video/play/id/18052/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-02-08",
        "href": "https://v.aniu.tv/video/play/id/18068/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-02-08",
        "href": "https://v.aniu.tv/video/play/id/18069/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-02-08",
        "href": "https://v.aniu.tv/video/play/id/18070/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-02-07",
        "href": "https://v.aniu.tv/video/play/id/18057/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-02-06",
        "href": "https://v.aniu.tv/video/play/id/18016/t/2.shtml"
      },
      {
        "innerHTML": "薛松：小散们应该如何应对市场大跌",
        "href": "https://v.aniu.tv/video/play/id/17889/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-02-01",
        "href": "https://v.aniu.tv/video/play/id/17881/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-02-01",
        "href": "https://v.aniu.tv/video/play/id/17883/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-02-01",
        "href": "https://v.aniu.tv/video/play/id/17882/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-01-31",
        "href": "https://v.aniu.tv/video/play/id/17873/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-01-30",
        "href": "https://v.aniu.tv/video/play/id/17829/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-01-25",
        "href": "https://v.aniu.tv/video/play/id/17695/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-01-25",
        "href": "https://v.aniu.tv/video/play/id/17696/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-01-25",
        "href": "https://v.aniu.tv/video/play/id/17697/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-01-24",
        "href": "https://v.aniu.tv/video/play/id/17687/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-01-23",
        "href": "https://v.aniu.tv/video/play/id/17616/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-01-23",
        "href": "https://v.aniu.tv/video/play/id/17617/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-01-23",
        "href": "https://v.aniu.tv/video/play/id/17618/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-01-23",
        "href": "https://v.aniu.tv/video/play/id/17643/t/2.shtml"
      },
      {
        "innerHTML": "薛松：如何判断市场上车点",
        "href": "https://v.aniu.tv/video/play/id/17490/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-01-18",
        "href": "https://v.aniu.tv/video/play/id/17500/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-01-18",
        "href": "https://v.aniu.tv/video/play/id/17501/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-01-18",
        "href": "https://v.aniu.tv/video/play/id/17502/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-01-17",
        "href": "https://v.aniu.tv/video/play/id/17492/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-01-16",
        "href": "https://v.aniu.tv/video/play/id/17446/t/2.shtml"
      },
      {
        "innerHTML": "薛松：九连阳之后行情会有哪些变化",
        "href": "https://v.aniu.tv/video/play/id/17295/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-01-11",
        "href": "https://v.aniu.tv/video/play/id/17308/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-01-11",
        "href": "https://v.aniu.tv/video/play/id/17309/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-01-11",
        "href": "https://v.aniu.tv/video/play/id/17310/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：有钱谁还炒股？ 都去思考人生了！",
        "href": "https://v.aniu.tv/video/play/id/69049/t/2.shtml"
      },
      {
        "innerHTML": "薛松：北上欲来亦不济 30点没太大文章",
        "href": "https://v.aniu.tv/video/play/id/68902/t/2.shtml"
      },
      {
        "innerHTML": "薛松：金融炼金术枯燥 而我独乐其中",
        "href": "https://v.aniu.tv/video/play/id/68905/t/2.shtml"
      },
      {
        "innerHTML": "薛松：财经本身不是最好话题！",
        "href": "https://v.aniu.tv/video/play/id/68914/t/2.shtml"
      },
      {
        "innerHTML": "薛松：状元共性启示录",
        "href": "https://v.aniu.tv/video/play/id/68915/t/2.shtml"
      },
      {
        "innerHTML": "薛松：从詹皇说起 天赋诚可贵努力价更高！",
        "href": "https://v.aniu.tv/video/play/id/68917/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为何不参加吐槽大会？赛道很重要！",
        "href": "https://v.aniu.tv/video/play/id/68918/t/2.shtml"
      },
      {
        "innerHTML": "薛松：行业竞争格局是门大学问！",
        "href": "https://v.aniu.tv/video/play/id/68924/t/2.shtml"
      },
      {
        "innerHTML": "薛松：反复纠错，别被养套杀！",
        "href": "https://v.aniu.tv/video/play/id/68930/t/2.shtml"
      },
      {
        "innerHTML": "薛松：残忍的真相 所有的超额利润都是利用群众",
        "href": "https://v.aniu.tv/video/play/id/68931/t/2.shtml"
      },
      {
        "innerHTML": "薛松：牛顿都输 何况我乎！",
        "href": "https://v.aniu.tv/video/play/id/68936/t/2.shtml"
      },
      {
        "innerHTML": "薛松：枪能打准是关键 其他不重要",
        "href": "https://v.aniu.tv/video/play/id/68937/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不要妨碍别人跳车的积极性 给他们点赞",
        "href": "https://v.aniu.tv/video/play/id/68938/t/2.shtml"
      },
      {
        "innerHTML": "薛松：冒险不算事 但不要投入过多的筹码",
        "href": "https://v.aniu.tv/video/play/id/68940/t/2.shtml"
      },
      {
        "innerHTML": "薛松：关于科创50灵魂的拷问",
        "href": "https://v.aniu.tv/video/play/id/68954/t/2.shtml"
      },
      {
        "innerHTML": "薛松：幸存者偏差之下 成功具有偶然性",
        "href": "https://v.aniu.tv/video/play/id/68797/t/2.shtml"
      },
      {
        "innerHTML": "薛松：只做热的不做凉的才有机会成功",
        "href": "https://v.aniu.tv/video/play/id/68798/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投资要用理智脑而不是爬行脑",
        "href": "https://v.aniu.tv/video/play/id/68802/t/2.shtml"
      },
      {
        "innerHTML": "薛松：有些人来股市无非是为了买一个希望",
        "href": "https://v.aniu.tv/video/play/id/68806/t/2.shtml"
      },
      {
        "innerHTML": "薛松：每次都去买最热门的板块 输钱概率很大",
        "href": "https://v.aniu.tv/video/play/id/68807/t/2.shtml"
      },
      {
        "innerHTML": "薛松：本能的害怕不确定性造成很多人无法成功",
        "href": "https://v.aniu.tv/video/play/id/68808/t/2.shtml"
      },
      {
        "innerHTML": "薛松：因信息优势曾经造富的人",
        "href": "https://v.aniu.tv/video/play/id/68771/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投资市场的目标与偶像",
        "href": "https://v.aniu.tv/video/play/id/68772/t/2.shtml"
      },
      {
        "innerHTML": "薛松：利用信息收集与信息处理赚钱",
        "href": "https://v.aniu.tv/video/play/id/68774/t/2.shtml"
      },
      {
        "innerHTML": "薛松：弟子不必不如师才有传承",
        "href": "https://v.aniu.tv/video/play/id/68775/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不要让散户和媒体带你的节奏",
        "href": "https://v.aniu.tv/video/play/id/68779/t/2.shtml"
      },
      {
        "innerHTML": "薛松：什么是价值投资？",
        "href": "https://v.aniu.tv/video/play/id/68720/t/2.shtml"
      },
      {
        "innerHTML": "薛松：对核心资产要有清晰认识！",
        "href": "https://v.aniu.tv/video/play/id/68727/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美国的“无限流动性”",
        "href": "https://v.aniu.tv/video/play/id/68729/t/2.shtml"
      },
      {
        "innerHTML": "薛松：金融市场风险大 单边或将强制平仓！",
        "href": "https://v.aniu.tv/video/play/id/68730/t/2.shtml"
      },
      {
        "innerHTML": "薛松：跌100点对你有意义？",
        "href": "https://v.aniu.tv/video/play/id/68736/t/2.shtml"
      },
      {
        "innerHTML": "薛松：盘中跌十几点，有什么值得大惊小怪？",
        "href": "https://v.aniu.tv/video/play/id/68739/t/2.shtml"
      },
      {
        "innerHTML": "薛松：新热点煤化工 也是一条路",
        "href": "https://v.aniu.tv/video/play/id/68741/t/2.shtml"
      },
      {
        "innerHTML": "薛松：经济学的“了不起”之处！",
        "href": "https://v.aniu.tv/video/play/id/68752/t/2.shtml"
      },
      {
        "innerHTML": "薛松：广量指标判断板块效应强弱",
        "href": "https://v.aniu.tv/video/play/id/68754/t/2.shtml"
      },
      {
        "innerHTML": "薛松：规律被散户掌握了，则规律改变",
        "href": "https://v.aniu.tv/video/play/id/68755/t/2.shtml"
      },
      {
        "innerHTML": "薛松：学会复盘，修正自己的“枪”靠打猎吃饭",
        "href": "https://v.aniu.tv/video/play/id/68758/t/2.shtml"
      },
      {
        "innerHTML": "薛松：科技不是钱 商用才是，买单的不是情怀 而是划算",
        "href": "https://v.aniu.tv/video/play/id/68759/t/2.shtml"
      },
      {
        "innerHTML": "薛松：鸿蒙涨势依旧，大宗轮番反弹",
        "href": "https://v.aniu.tv/video/play/id/68579/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：养猪也会 猪肉价格让不让飞",
        "href": "https://v.aniu.tv/video/play/id/50112/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-07-14",
        "href": "https://v.aniu.tv/video/play/id/50095/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-07-14",
        "href": "https://v.aniu.tv/video/play/id/50096/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-14",
        "href": "https://v.aniu.tv/video/play/id/50099/t/2.shtml"
      },
      {
        "innerHTML": "刘伟鹏：这就是震荡，不是调整！",
        "href": "https://v.aniu.tv/video/play/id/50136/t/2.shtml"
      },
      {
        "innerHTML": "刘伟鹏：一旦市场调整，就是蓝筹低吸的机会",
        "href": "https://v.aniu.tv/video/play/id/50137/t/2.shtml"
      },
      {
        "innerHTML": "赢在博弈2020-07-12",
        "href": "https://v.aniu.tv/video/play/id/50064/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-07-13",
        "href": "https://v.aniu.tv/video/play/id/50051/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-07-13",
        "href": "https://v.aniu.tv/video/play/id/50052/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-13",
        "href": "https://v.aniu.tv/video/play/id/50055/t/2.shtml"
      },
      {
        "innerHTML": "下周透视2020-07-12",
        "href": "https://v.aniu.tv/video/play/id/50061/t/2.shtml"
      },
      {
        "innerHTML": "强势追踪2020-07-12",
        "href": "https://v.aniu.tv/video/play/id/50062/t/2.shtml"
      },
      {
        "innerHTML": "投顾当家2020-07-12",
        "href": "https://v.aniu.tv/video/play/id/50065/t/2.shtml"
      },
      {
        "innerHTML": "薛松：都说好了  你们就mai吧",
        "href": "https://v.aniu.tv/video/play/id/50013/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-10",
        "href": "https://v.aniu.tv/video/play/id/49991/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-07-09",
        "href": "https://v.aniu.tv/video/play/id/49922/t/2.shtml"
      },
      {
        "innerHTML": "纪宗逸：市场资金充足，请保持好仓位",
        "href": "https://v.aniu.tv/video/play/id/49934/t/2.shtml"
      },
      {
        "innerHTML": "纪宗逸：比较稳妥的投资者还是按照价值洼地的模式走",
        "href": "https://v.aniu.tv/video/play/id/49947/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-09",
        "href": "https://v.aniu.tv/video/play/id/49974/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-07-08",
        "href": "https://v.aniu.tv/video/play/id/49878/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-07-08",
        "href": "https://v.aniu.tv/video/play/id/49879/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投机一定要想清楚再做",
        "href": "https://v.aniu.tv/video/play/id/49891/t/2.shtml"
      },
      {
        "innerHTML": "骆明钟：大宗商品行情仅仅只是开始",
        "href": "https://v.aniu.tv/video/play/id/49903/t/2.shtml"
      },
      {
        "innerHTML": "骆明钟：资源股机会展现",
        "href": "https://v.aniu.tv/video/play/id/49904/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-08",
        "href": "https://v.aniu.tv/video/play/id/49882/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-07-07",
        "href": "https://v.aniu.tv/video/play/id/49835/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-07-07",
        "href": "https://v.aniu.tv/video/play/id/49836/t/2.shtml"
      },
      {
        "innerHTML": "刘伟鹏：低位低估股票还将延续上涨",
        "href": "https://v.aniu.tv/video/play/id/49856/t/2.shtml"
      },
      {
        "innerHTML": "薛松：什么是消纳的过程",
        "href": "https://v.aniu.tv/video/play/id/49855/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为什么操作要放在中午？",
        "href": "https://v.aniu.tv/video/play/id/49854/t/2.shtml"
      },
      {
        "innerHTML": "主题投资2020-07-07",
        "href": "https://v.aniu.tv/video/play/id/49853/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-07",
        "href": "https://v.aniu.tv/video/play/id/49840/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2020-07-06",
        "href": "https://v.aniu.tv/video/play/id/49722/t/2.shtml"
      },
      {
        "innerHTML": "首席投顾2020-07-06",
        "href": "https://v.aniu.tv/video/play/id/49723/t/2.shtml"
      },
      {
        "innerHTML": "主题投资2020-07-06",
        "href": "https://v.aniu.tv/video/play/id/49733/t/2.shtml"
      },
      {
        "innerHTML": "薛松：海龟交易法把握趋势大行情",
        "href": "https://v.aniu.tv/video/play/id/49823/t/2.shtml"
      },
      {
        "innerHTML": "黄岑栋：牛市大涨行情，关注这几大核心要素！",
        "href": "https://v.aniu.tv/video/play/id/49822/t/2.shtml"
      },
      {
        "innerHTML": "薛松：别被指数带坏心态！",
        "href": "https://v.aniu.tv/video/play/id/49818/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-06",
        "href": "https://v.aniu.tv/video/play/id/49724/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：细数一些玩笑",
        "href": "https://v.aniu.tv/video/play/id/61969/t/2.shtml"
      },
      {
        "innerHTML": "薛松：建仓时间点即将到来",
        "href": "https://v.aniu.tv/video/play/id/61998/t/2.shtml"
      },
      {
        "innerHTML": "薛松：下周五前请务必满仓！！！",
        "href": "https://v.aniu.tv/video/play/id/61999/t/2.shtml"
      },
      {
        "innerHTML": "薛松：抄底要与众不同",
        "href": "https://v.aniu.tv/video/play/id/62000/t/2.shtml"
      },
      {
        "innerHTML": "薛松：市场在当前位置这才是关键！",
        "href": "https://v.aniu.tv/video/play/id/62002/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这种事件很难成为决定性因素",
        "href": "https://v.aniu.tv/video/play/id/62004/t/2.shtml"
      },
      {
        "innerHTML": "薛松：可以说人懒 但不能说人蠢",
        "href": "https://v.aniu.tv/video/play/id/61838/t/2.shtml"
      },
      {
        "innerHTML": "薛松：秦始皇出巡的启示",
        "href": "https://v.aniu.tv/video/play/id/61861/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美国十年国债收益率对股市有何影响",
        "href": "https://v.aniu.tv/video/play/id/61429/t/2.shtml"
      },
      {
        "innerHTML": "薛松：管不住手不妨试试这样",
        "href": "https://v.aniu.tv/video/play/id/61430/t/2.shtml"
      },
      {
        "innerHTML": "薛松：稳住，我们能赢！",
        "href": "https://v.aniu.tv/video/play/id/61434/t/2.shtml"
      },
      {
        "innerHTML": "薛松：“猪队友”行情有一半可能出现",
        "href": "https://v.aniu.tv/video/play/id/61435/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投资要有信心",
        "href": "https://v.aniu.tv/video/play/id/61472/t/2.shtml"
      },
      {
        "innerHTML": "薛松：平均收益往往是很多投资者不屑的",
        "href": "https://v.aniu.tv/video/play/id/61007/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你若非刘备 诸葛亮出世也不济",
        "href": "https://v.aniu.tv/video/play/id/61009/t/2.shtml"
      },
      {
        "innerHTML": "薛松：坚持自己认定的投资思路",
        "href": "https://v.aniu.tv/video/play/id/61010/t/2.shtml"
      },
      {
        "innerHTML": "薛松：敢于站在少数面 才能挣大钱",
        "href": "https://v.aniu.tv/video/play/id/61061/t/2.shtml"
      },
      {
        "innerHTML": "薛松：学会反情绪操作",
        "href": "https://v.aniu.tv/video/play/id/60639/t/2.shtml"
      },
      {
        "innerHTML": "薛松：如何通过情绪面选取买点",
        "href": "https://v.aniu.tv/video/play/id/60640/t/2.shtml"
      },
      {
        "innerHTML": "薛松：锚定效应消失市场才会平稳",
        "href": "https://v.aniu.tv/video/play/id/60670/t/2.shtml"
      },
      {
        "innerHTML": "薛松：T+0制度的影响",
        "href": "https://v.aniu.tv/video/play/id/60674/t/2.shtml"
      },
      {
        "innerHTML": "薛松：股市人生的理想与现实",
        "href": "https://v.aniu.tv/video/play/id/60603/t/2.shtml"
      },
      {
        "innerHTML": "薛松：目标达成未必好 专业人不轻易出手",
        "href": "https://v.aniu.tv/video/play/id/60612/t/2.shtml"
      },
      {
        "innerHTML": "薛松：买的时候是大爷 抛的时候呢？",
        "href": "https://v.aniu.tv/video/play/id/60688/t/2.shtml"
      },
      {
        "innerHTML": "薛松：基民开始调侃经理 情绪化阶段已至",
        "href": "https://v.aniu.tv/video/play/id/60291/t/2.shtml"
      },
      {
        "innerHTML": "薛松：长期挣的钱应是心态平稳的",
        "href": "https://v.aniu.tv/video/play/id/60292/t/2.shtml"
      },
      {
        "innerHTML": "薛松：勿做巨婴 要做自强投资人",
        "href": "https://v.aniu.tv/video/play/id/60686/t/2.shtml"
      },
      {
        "innerHTML": "薛松：金融市场有最大的特征",
        "href": "https://v.aniu.tv/video/play/id/60216/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数上行至4200难度很大",
        "href": "https://v.aniu.tv/video/play/id/60225/t/2.shtml"
      },
      {
        "innerHTML": "薛松：选股的简单秘诀",
        "href": "https://v.aniu.tv/video/play/id/60237/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2021-02-24",
        "href": "https://v.aniu.tv/video/play/id/60265/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2021-02-23",
        "href": "https://v.aniu.tv/video/play/id/60200/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2021-02-10",
        "href": "https://v.aniu.tv/video/play/id/59773/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2021-02-09",
        "href": "https://v.aniu.tv/video/play/id/59719/t/2.shtml"
      },
      {
        "innerHTML": "薛松：对于散户们的灵魂拷问",
        "href": "https://v.aniu.tv/video/play/id/59435/t/2.shtml"
      },
      {
        "innerHTML": "薛松：我的2月份股市战略计划！",
        "href": "https://v.aniu.tv/video/play/id/59439/t/2.shtml"
      },
      {
        "innerHTML": "薛松：即使我也会踩雷 元芳你怎么看",
        "href": "https://v.aniu.tv/video/play/id/59441/t/2.shtml"
      },
      {
        "innerHTML": "薛松：能不能扔个手绢认个输？",
        "href": "https://v.aniu.tv/video/play/id/59482/t/2.shtml"
      },
      {
        "innerHTML": "薛松：先了解原理 再问问题",
        "href": "https://v.aniu.tv/video/play/id/59333/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2015-11-25",
        "href": "https://v.aniu.tv/video/play/id/1628/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-24",
        "href": "https://v.aniu.tv/video/play/id/1594/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-20",
        "href": "https://v.aniu.tv/video/play/id/1558/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-18",
        "href": "https://v.aniu.tv/video/play/id/1519/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-16",
        "href": "https://v.aniu.tv/video/play/id/5461/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-16",
        "href": "https://v.aniu.tv/video/play/id/6003/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-12",
        "href": "https://v.aniu.tv/video/play/id/1442/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-12",
        "href": "https://v.aniu.tv/video/play/id/1442/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-11",
        "href": "https://v.aniu.tv/video/play/id/1417/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-11",
        "href": "https://v.aniu.tv/video/play/id/1417/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-09",
        "href": "https://v.aniu.tv/video/play/id/1369/t/2.shtml"
      },
      {
        "innerHTML": "公告追踪2015-11-07",
        "href": "https://v.aniu.tv/video/play/id/1343/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-05",
        "href": "https://v.aniu.tv/video/play/id/1314/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-11-04",
        "href": "https://v.aniu.tv/video/play/id/1298/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-10-28",
        "href": "https://v.aniu.tv/video/play/id/1232/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-10-26",
        "href": "https://v.aniu.tv/video/play/id/1170/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-10-22",
        "href": "https://v.aniu.tv/video/play/id/1149/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-10-21",
        "href": "https://v.aniu.tv/video/play/id/1133/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-10-19",
        "href": "https://v.aniu.tv/video/play/id/1106/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-10-16",
        "href": "https://v.aniu.tv/video/play/id/1080/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-10-15",
        "href": "https://v.aniu.tv/video/play/id/1083/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-10-14",
        "href": "https://v.aniu.tv/video/play/id/1069/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-10-12",
        "href": "https://v.aniu.tv/video/play/id/1054/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-10-09",
        "href": "https://v.aniu.tv/video/play/id/1042/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-10-08",
        "href": "https://v.aniu.tv/video/play/id/1034/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-30",
        "href": "https://v.aniu.tv/video/play/id/1037/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-28",
        "href": "https://v.aniu.tv/video/play/id/1022/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-25",
        "href": "https://v.aniu.tv/video/play/id/1015/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-24",
        "href": "https://v.aniu.tv/video/play/id/1011/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-23",
        "href": "https://v.aniu.tv/video/play/id/1003/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-21",
        "href": "https://v.aniu.tv/video/play/id/997/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-18",
        "href": "https://v.aniu.tv/video/play/id/980/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-17",
        "href": "https://v.aniu.tv/video/play/id/977/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-16",
        "href": "https://v.aniu.tv/video/play/id/969/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-14",
        "href": "https://v.aniu.tv/video/play/id/961/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-11",
        "href": "https://v.aniu.tv/video/play/id/942/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-10",
        "href": "https://v.aniu.tv/video/play/id/936/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-09",
        "href": "https://v.aniu.tv/video/play/id/934/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-09-07",
        "href": "https://v.aniu.tv/video/play/id/922/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2019-05-29",
        "href": "https://v.aniu.tv/video/play/id/36682/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-05-28",
        "href": "https://v.aniu.tv/video/play/id/36645/t/2.shtml"
      },
      {
        "innerHTML": "薛松：大盘目前需要阳线提振信心",
        "href": "https://v.aniu.tv/video/play/id/36466/t/2.shtml"
      },
      {
        "innerHTML": "薛松：现在市场该如何操作？",
        "href": "https://v.aniu.tv/video/play/id/36464/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-05-23",
        "href": "https://v.aniu.tv/video/play/id/36535/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-05-23",
        "href": "https://v.aniu.tv/video/play/id/36536/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-05-23",
        "href": "https://v.aniu.tv/video/play/id/36537/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-05-22",
        "href": "https://v.aniu.tv/video/play/id/36456/t/2.shtml"
      },
      {
        "innerHTML": "主题投资2019-05-21",
        "href": "https://v.aniu.tv/video/play/id/36408/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-05-21",
        "href": "https://v.aniu.tv/video/play/id/36409/t/2.shtml"
      },
      {
        "innerHTML": "薛松：选择有超跌反弹潜质的，要理性不能盲从",
        "href": "https://v.aniu.tv/video/play/id/35932/t/2.shtml"
      },
      {
        "innerHTML": "薛松：香港股民为何爱买蓝筹股？",
        "href": "https://v.aniu.tv/video/play/id/35927/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-05-09",
        "href": "https://v.aniu.tv/video/play/id/36055/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-05-09",
        "href": "https://v.aniu.tv/video/play/id/36056/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-05-09",
        "href": "https://v.aniu.tv/video/play/id/36057/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-05-08",
        "href": "https://v.aniu.tv/video/play/id/35924/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-05-07",
        "href": "https://v.aniu.tv/video/play/id/35878/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-04-30",
        "href": "https://v.aniu.tv/video/play/id/35745/t/2.shtml"
      },
      {
        "innerHTML": "【快评】薛松：弱市反弹，垃圾股先来",
        "href": "https://v.aniu.tv/video/play/id/35502/t/2.shtml"
      },
      {
        "innerHTML": "薛松：什么样的股票能做？要做自己熟悉的！",
        "href": "https://v.aniu.tv/video/play/id/35499/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-04-25",
        "href": "https://v.aniu.tv/video/play/id/35542/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-04-25",
        "href": "https://v.aniu.tv/video/play/id/35543/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-04-25",
        "href": "https://v.aniu.tv/video/play/id/35544/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-04-17",
        "href": "https://v.aniu.tv/video/play/id/35176/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-04-16",
        "href": "https://v.aniu.tv/video/play/id/35123/t/2.shtml"
      },
      {
        "innerHTML": "薛松：如果成交缩量 可能出现震荡",
        "href": "https://v.aniu.tv/video/play/id/34863/t/2.shtml"
      },
      {
        "innerHTML": "【快评】张曙：承接之前的盘整，震荡的4月",
        "href": "https://v.aniu.tv/video/play/id/34887/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-04-11",
        "href": "https://v.aniu.tv/video/play/id/34991/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-04-11",
        "href": "https://v.aniu.tv/video/play/id/34992/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-04-11",
        "href": "https://v.aniu.tv/video/play/id/34993/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2019-04-11",
        "href": "https://v.aniu.tv/video/play/id/34994/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2019-04-11",
        "href": "https://v.aniu.tv/video/play/id/34995/t/2.shtml"
      },
      {
        "innerHTML": "【快评】杨继农：交易要有体系",
        "href": "https://v.aniu.tv/video/play/id/34841/t/2.shtml"
      },
      {
        "innerHTML": "【快评】薛松：教你均线放仓位！",
        "href": "https://v.aniu.tv/video/play/id/34840/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2019-04-10",
        "href": "https://v.aniu.tv/video/play/id/34869/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2019-04-10",
        "href": "https://v.aniu.tv/video/play/id/34870/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-04-10",
        "href": "https://v.aniu.tv/video/play/id/34874/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-04-09",
        "href": "https://v.aniu.tv/video/play/id/34827/t/2.shtml"
      },
      {
        "innerHTML": "薛松：个股转折的两个标准怎么看",
        "href": "https://v.aniu.tv/video/play/id/34669/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2019-07-16",
        "href": "https://v.aniu.tv/video/play/id/38217/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-07-10",
        "href": "https://v.aniu.tv/video/play/id/38006/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-07-09",
        "href": "https://v.aniu.tv/video/play/id/37965/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-07-04",
        "href": "https://v.aniu.tv/video/play/id/37774/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-07-04",
        "href": "https://v.aniu.tv/video/play/id/37775/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-07-04",
        "href": "https://v.aniu.tv/video/play/id/37776/t/2.shtml"
      },
      {
        "innerHTML": "薛松：长线风险不比短线风险小",
        "href": "https://v.aniu.tv/video/play/id/37768/t/2.shtml"
      },
      {
        "innerHTML": "薛松：EXPMA结合DMI使用技巧",
        "href": "https://v.aniu.tv/video/play/id/37765/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-07-03",
        "href": "https://v.aniu.tv/video/play/id/37757/t/2.shtml"
      },
      {
        "innerHTML": "薛松：沿着上交所问询函研究基本面",
        "href": "https://v.aniu.tv/video/play/id/37693/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-07-02",
        "href": "https://v.aniu.tv/video/play/id/37718/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-06-26",
        "href": "https://v.aniu.tv/video/play/id/37524/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-06-25",
        "href": "https://v.aniu.tv/video/play/id/37483/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-06-24",
        "href": "https://v.aniu.tv/video/play/id/37446/t/2.shtml"
      },
      {
        "innerHTML": "薛松：该如何正确游戏行业？",
        "href": "https://v.aniu.tv/video/play/id/37295/t/2.shtml"
      },
      {
        "innerHTML": "刘彬：接下来可能重心往小品种ETF调换一些",
        "href": "https://v.aniu.tv/video/play/id/37293/t/2.shtml"
      },
      {
        "innerHTML": "薛松：买卖点没那么复杂，就这么做！",
        "href": "https://v.aniu.tv/video/play/id/37296/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-06-20",
        "href": "https://v.aniu.tv/video/play/id/37344/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-06-20",
        "href": "https://v.aniu.tv/video/play/id/37345/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-06-20",
        "href": "https://v.aniu.tv/video/play/id/37346/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-06-19",
        "href": "https://v.aniu.tv/video/play/id/37286/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-06-18",
        "href": "https://v.aniu.tv/video/play/id/37223/t/2.shtml"
      },
      {
        "innerHTML": "薛松：政策需要保持威慑力",
        "href": "https://v.aniu.tv/video/play/id/37176/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-06-12",
        "href": "https://v.aniu.tv/video/play/id/37083/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-06-11",
        "href": "https://v.aniu.tv/video/play/id/37030/t/2.shtml"
      },
      {
        "innerHTML": "韩愈：鸡肋行情看白马股",
        "href": "https://v.aniu.tv/video/play/id/36922/t/2.shtml"
      },
      {
        "innerHTML": "韩愈：满足这些条件的就是比较好的标的",
        "href": "https://v.aniu.tv/video/play/id/36921/t/2.shtml"
      },
      {
        "innerHTML": "薛松：现在该用什么心态看待市场？",
        "href": "https://v.aniu.tv/video/play/id/36910/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-06-06",
        "href": "https://v.aniu.tv/video/play/id/36944/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-06-06",
        "href": "https://v.aniu.tv/video/play/id/36945/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-06-06",
        "href": "https://v.aniu.tv/video/play/id/36946/t/2.shtml"
      },
      {
        "innerHTML": "画说基本面2019-06-06",
        "href": "https://v.aniu.tv/video/play/id/36950/t/2.shtml"
      },
      {
        "innerHTML": "主题投资2019-06-06",
        "href": "https://v.aniu.tv/video/play/id/36951/t/2.shtml"
      },
      {
        "innerHTML": "主题投资2019-06-05",
        "href": "https://v.aniu.tv/video/play/id/36903/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-06-05",
        "href": "https://v.aniu.tv/video/play/id/36904/t/2.shtml"
      },
      {
        "innerHTML": "画说基本面2019-06-04",
        "href": "https://v.aniu.tv/video/play/id/36876/t/2.shtml"
      },
      {
        "innerHTML": "主题投资2019-06-04",
        "href": "https://v.aniu.tv/video/play/id/36877/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-06-04",
        "href": "https://v.aniu.tv/video/play/id/36878/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投资者要学会资金管理",
        "href": "https://v.aniu.tv/video/play/id/36697/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "股市诊疗2016-09-01",
        "href": "https://v.aniu.tv/video/play/id/5760/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-09-01",
        "href": "https://v.aniu.tv/video/play/id/5761/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-09-01",
        "href": "https://v.aniu.tv/video/play/id/5773/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-08-30",
        "href": "https://v.aniu.tv/video/play/id/5727/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-08-31",
        "href": "https://v.aniu.tv/video/play/id/5751/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-08-25",
        "href": "https://v.aniu.tv/video/play/id/5631/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-08-25",
        "href": "https://v.aniu.tv/video/play/id/5632/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-08-25",
        "href": "https://v.aniu.tv/video/play/id/5633/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-08-25",
        "href": "https://v.aniu.tv/video/play/id/5641/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-08-23",
        "href": "https://v.aniu.tv/video/play/id/5598/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-08-18",
        "href": "https://v.aniu.tv/video/play/id/5514/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-08-18",
        "href": "https://v.aniu.tv/video/play/id/5515/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-08-18",
        "href": "https://v.aniu.tv/video/play/id/5516/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-08-17",
        "href": "https://v.aniu.tv/video/play/id/5505/t/2.shtml"
      },
      {
        "innerHTML": "热点公告 2016-08-16",
        "href": "https://v.aniu.tv/video/play/id/5481/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-08-11",
        "href": "https://v.aniu.tv/video/play/id/5398/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-08-11",
        "href": "https://v.aniu.tv/video/play/id/5399/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-08-11",
        "href": "https://v.aniu.tv/video/play/id/5400/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-08-11",
        "href": "https://v.aniu.tv/video/play/id/5409/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-08-10",
        "href": "https://v.aniu.tv/video/play/id/5388/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-08-09",
        "href": "https://v.aniu.tv/video/play/id/5365/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-08-04",
        "href": "https://v.aniu.tv/video/play/id/5293/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-08-04",
        "href": "https://v.aniu.tv/video/play/id/5280/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-08-04",
        "href": "https://v.aniu.tv/video/play/id/5282/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-08-04",
        "href": "https://v.aniu.tv/video/play/id/5283/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-08-03",
        "href": "https://v.aniu.tv/video/play/id/5272/t/2.shtml"
      },
      {
        "innerHTML": "昨夜华尔街2016-08-02",
        "href": "https://v.aniu.tv/video/play/id/5232/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-08-02",
        "href": "https://v.aniu.tv/video/play/id/5251/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2016-08-02",
        "href": "https://v.aniu.tv/video/play/id/5234/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-07-28",
        "href": "https://v.aniu.tv/video/play/id/5179/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-07-28",
        "href": "https://v.aniu.tv/video/play/id/5171/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-07-28",
        "href": "https://v.aniu.tv/video/play/id/5170/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-07-28",
        "href": "https://v.aniu.tv/video/play/id/5169/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-07-27",
        "href": "https://v.aniu.tv/video/play/id/5156/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-07-26",
        "href": "https://v.aniu.tv/video/play/id/5138/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-07-21",
        "href": "https://v.aniu.tv/video/play/id/5069/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-07-21",
        "href": "https://v.aniu.tv/video/play/id/5057/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-07-21",
        "href": "https://v.aniu.tv/video/play/id/5056/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-07-21",
        "href": "https://v.aniu.tv/video/play/id/5055/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：航空股受利好刺激 逢空低买",
        "href": "https://v.aniu.tv/video/play/id/34665/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-04-04",
        "href": "https://v.aniu.tv/video/play/id/34723/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-04-04",
        "href": "https://v.aniu.tv/video/play/id/34724/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-04-04",
        "href": "https://v.aniu.tv/video/play/id/34725/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-04-03",
        "href": "https://v.aniu.tv/video/play/id/34678/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投资者要学会先“挨打”，才要去学如何“进攻”",
        "href": "https://v.aniu.tv/video/play/id/34655/t/2.shtml"
      },
      {
        "innerHTML": "薛松：股票不只是要懂得买入和持有，还要懂得买错该怎么办？",
        "href": "https://v.aniu.tv/video/play/id/34594/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-04-02",
        "href": "https://v.aniu.tv/video/play/id/34617/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-03-14",
        "href": "https://v.aniu.tv/video/play/id/33811/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-03-14",
        "href": "https://v.aniu.tv/video/play/id/33812/t/2.shtml"
      },
      {
        "innerHTML": "贤哥：炒股挣钱必须先了解这三点！",
        "href": "https://v.aniu.tv/video/play/id/33794/t/2.shtml"
      },
      {
        "innerHTML": "贤哥：周期股投资逻辑讲解",
        "href": "https://v.aniu.tv/video/play/id/33792/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-03-14",
        "href": "https://v.aniu.tv/video/play/id/33810/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-03-13",
        "href": "https://v.aniu.tv/video/play/id/33800/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-03-12",
        "href": "https://v.aniu.tv/video/play/id/33751/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-03-06",
        "href": "https://v.aniu.tv/video/play/id/33496/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-03-05",
        "href": "https://v.aniu.tv/video/play/id/33443/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-02-28",
        "href": "https://v.aniu.tv/video/play/id/33192/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-02-28",
        "href": "https://v.aniu.tv/video/play/id/33193/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-02-28",
        "href": "https://v.aniu.tv/video/play/id/33194/t/2.shtml"
      },
      {
        "innerHTML": "徐世荣：三月份热门板块提前获知",
        "href": "https://v.aniu.tv/video/play/id/33178/t/2.shtml"
      },
      {
        "innerHTML": "薛松：当上证单日涨幅超过5%时，后期走势往往这样！！",
        "href": "https://v.aniu.tv/video/play/id/33199/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-02-27",
        "href": "https://v.aniu.tv/video/play/id/33183/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-02-26",
        "href": "https://v.aniu.tv/video/play/id/33119/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-02-20",
        "href": "https://v.aniu.tv/video/play/id/32829/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-02-19",
        "href": "https://v.aniu.tv/video/play/id/32784/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-02-14",
        "href": "https://v.aniu.tv/video/play/id/32513/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-02-14",
        "href": "https://v.aniu.tv/video/play/id/32514/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-02-14",
        "href": "https://v.aniu.tv/video/play/id/32515/t/2.shtml"
      },
      {
        "innerHTML": "薛松：汽车板块考虑轻量化配件的公司",
        "href": "https://v.aniu.tv/video/play/id/32492/t/2.shtml"
      },
      {
        "innerHTML": "窦维德：热情指标这样使用！！",
        "href": "https://v.aniu.tv/video/play/id/32489/t/2.shtml"
      },
      {
        "innerHTML": "薛松：大阳线战法讲解！",
        "href": "https://v.aniu.tv/video/play/id/32483/t/2.shtml"
      },
      {
        "innerHTML": "窦维德：如何选出创投龙头股？",
        "href": "https://v.aniu.tv/video/play/id/32482/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-02-13",
        "href": "https://v.aniu.tv/video/play/id/32504/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-02-12",
        "href": "https://v.aniu.tv/video/play/id/32445/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-01-31",
        "href": "https://v.aniu.tv/video/play/id/32230/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-01-31",
        "href": "https://v.aniu.tv/video/play/id/32231/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-01-31",
        "href": "https://v.aniu.tv/video/play/id/32232/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-01-30",
        "href": "https://v.aniu.tv/video/play/id/32219/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "量化热点股2020-01-23",
        "href": "https://v.aniu.tv/video/play/id/44560/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-01-23",
        "href": "https://v.aniu.tv/video/play/id/44561/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-01-23",
        "href": "https://v.aniu.tv/video/play/id/44562/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-01-23",
        "href": "https://v.aniu.tv/video/play/id/44611/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-01-22",
        "href": "https://v.aniu.tv/video/play/id/44575/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-01-21",
        "href": "https://v.aniu.tv/video/play/id/44537/t/2.shtml"
      },
      {
        "innerHTML": "薛松：小仓位炼心态 投资顺其自然",
        "href": "https://v.aniu.tv/video/play/id/44349/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投资在赢面大的机会上",
        "href": "https://v.aniu.tv/video/play/id/44337/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-01-16",
        "href": "https://v.aniu.tv/video/play/id/44362/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-01-16",
        "href": "https://v.aniu.tv/video/play/id/44363/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-01-16",
        "href": "https://v.aniu.tv/video/play/id/44364/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-01-15",
        "href": "https://v.aniu.tv/video/play/id/44301/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数，级别，指标的应用",
        "href": "https://v.aniu.tv/video/play/id/44272/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-01-14",
        "href": "https://v.aniu.tv/video/play/id/44257/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-01-09",
        "href": "https://v.aniu.tv/video/play/id/44094/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-01-09",
        "href": "https://v.aniu.tv/video/play/id/44095/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-01-09",
        "href": "https://v.aniu.tv/video/play/id/44096/t/2.shtml"
      },
      {
        "innerHTML": "林整华：短线回落不改上行趋势",
        "href": "https://v.aniu.tv/video/play/id/44108/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-01-08",
        "href": "https://v.aniu.tv/video/play/id/44059/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-01-07",
        "href": "https://v.aniu.tv/video/play/id/44043/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-01-02",
        "href": "https://v.aniu.tv/video/play/id/43859/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-01-02",
        "href": "https://v.aniu.tv/video/play/id/43860/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-01-02",
        "href": "https://v.aniu.tv/video/play/id/43861/t/2.shtml"
      },
      {
        "innerHTML": "画说基本面2019-12-31",
        "href": "https://v.aniu.tv/video/play/id/43839/t/2.shtml"
      },
      {
        "innerHTML": "柯昌武：明年预期利率市场化，市场行情会上新的台阶！！",
        "href": "https://v.aniu.tv/video/play/id/43847/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-12-31",
        "href": "https://v.aniu.tv/video/play/id/43825/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数，级别，指标的应用",
        "href": "https://v.aniu.tv/video/play/id/43764/t/2.shtml"
      },
      {
        "innerHTML": "聚焦新动能2019-12-29",
        "href": "https://v.aniu.tv/video/play/id/43721/t/2.shtml"
      },
      {
        "innerHTML": "交易封神榜2019-12-29",
        "href": "https://v.aniu.tv/video/play/id/43722/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-12-27",
        "href": "https://v.aniu.tv/video/play/id/43709/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-12-26",
        "href": "https://v.aniu.tv/video/play/id/43670/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-12-25",
        "href": "https://v.aniu.tv/video/play/id/43632/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-12-24",
        "href": "https://v.aniu.tv/video/play/id/43591/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-12-19",
        "href": "https://v.aniu.tv/video/play/id/43427/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-12-19",
        "href": "https://v.aniu.tv/video/play/id/43428/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-12-19",
        "href": "https://v.aniu.tv/video/play/id/43429/t/2.shtml"
      },
      {
        "innerHTML": "薛松：强势股不要恐高，大涨后还有空间！",
        "href": "https://v.aniu.tv/video/play/id/43435/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-12-18",
        "href": "https://v.aniu.tv/video/play/id/43395/t/2.shtml"
      },
      {
        "innerHTML": "公告追踪2019-12-14",
        "href": "https://v.aniu.tv/video/play/id/43317/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2017-01-11",
        "href": "https://v.aniu.tv/video/play/id/8892/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-01-10",
        "href": "https://v.aniu.tv/video/play/id/8868/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2017-01-10",
        "href": "https://v.aniu.tv/video/play/id/8853/t/2.shtml"
      },
      {
        "innerHTML": "昨夜华尔街2017-01-10",
        "href": "https://v.aniu.tv/video/play/id/8852/t/2.shtml"
      },
      {
        "innerHTML": "薛松：如何看个股当天的分价表",
        "href": "https://v.aniu.tv/video/play/id/8767/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-01-05",
        "href": "https://v.aniu.tv/video/play/id/8756/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-01-05",
        "href": "https://v.aniu.tv/video/play/id/8755/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-01-05",
        "href": "https://v.aniu.tv/video/play/id/8754/t/2.shtml"
      },
      {
        "innerHTML": "数据一对一2017-01-05",
        "href": "https://v.aniu.tv/video/play/id/8753/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-01-04",
        "href": "https://v.aniu.tv/video/play/id/8730/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-01-03",
        "href": "https://v.aniu.tv/video/play/id/8703/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-12-29",
        "href": "https://v.aniu.tv/video/play/id/8627/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-12-29",
        "href": "https://v.aniu.tv/video/play/id/8628/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-12-29",
        "href": "https://v.aniu.tv/video/play/id/8629/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-12-28",
        "href": "https://v.aniu.tv/video/play/id/8612/t/2.shtml"
      },
      {
        "innerHTML": "昨夜华尔街2016-12-27",
        "href": "https://v.aniu.tv/video/play/id/8558/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2016-12-27",
        "href": "https://v.aniu.tv/video/play/id/8561/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-12-27",
        "href": "https://v.aniu.tv/video/play/id/8585/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-12-22",
        "href": "https://v.aniu.tv/video/play/id/8446/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-12-22",
        "href": "https://v.aniu.tv/video/play/id/8447/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-12-22",
        "href": "https://v.aniu.tv/video/play/id/8448/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-12-21",
        "href": "https://v.aniu.tv/video/play/id/8432/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-12-20",
        "href": "https://v.aniu.tv/video/play/id/8405/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-12-15",
        "href": "https://v.aniu.tv/video/play/id/8274/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-12-15",
        "href": "https://v.aniu.tv/video/play/id/8279/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-12-15",
        "href": "https://v.aniu.tv/video/play/id/8280/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-12-14",
        "href": "https://v.aniu.tv/video/play/id/8266/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-12-13",
        "href": "https://v.aniu.tv/video/play/id/8236/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-12-08",
        "href": "https://v.aniu.tv/video/play/id/8112/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-12-08",
        "href": "https://v.aniu.tv/video/play/id/8113/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-12-08",
        "href": "https://v.aniu.tv/video/play/id/8114/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-12-07",
        "href": "https://v.aniu.tv/video/play/id/8088/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-12-06",
        "href": "https://v.aniu.tv/video/play/id/8069/t/2.shtml"
      },
      {
        "innerHTML": "薛松：题材股，何日君再来",
        "href": "https://v.aniu.tv/video/play/id/7914/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-12-01",
        "href": "https://v.aniu.tv/video/play/id/7928/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-12-01",
        "href": "https://v.aniu.tv/video/play/id/7929/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-12-01",
        "href": "https://v.aniu.tv/video/play/id/7930/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-11-30",
        "href": "https://v.aniu.tv/video/play/id/7888/t/2.shtml"
      },
      {
        "innerHTML": "昨夜华尔街2016-11-29",
        "href": "https://v.aniu.tv/video/play/id/7843/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2017-05-02",
        "href": "https://v.aniu.tv/video/play/id/11247/t/2.shtml"
      },
      {
        "innerHTML": "薛松：常见的时间周期",
        "href": "https://v.aniu.tv/video/play/id/11207/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-04-27",
        "href": "https://v.aniu.tv/video/play/id/11168/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-04-27",
        "href": "https://v.aniu.tv/video/play/id/11169/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-04-27",
        "href": "https://v.aniu.tv/video/play/id/11172/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-04-26",
        "href": "https://v.aniu.tv/video/play/id/11156/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-04-25",
        "href": "https://v.aniu.tv/video/play/id/11123/t/2.shtml"
      },
      {
        "innerHTML": "薛松：散户该不该入市",
        "href": "https://v.aniu.tv/video/play/id/11046/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-04-20",
        "href": "https://v.aniu.tv/video/play/id/10997/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-04-20",
        "href": "https://v.aniu.tv/video/play/id/10998/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-04-20",
        "href": "https://v.aniu.tv/video/play/id/10999/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-04-19",
        "href": "https://v.aniu.tv/video/play/id/10987/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-04-18",
        "href": "https://v.aniu.tv/video/play/id/10948/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-04-13",
        "href": "https://v.aniu.tv/video/play/id/10811/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-04-13",
        "href": "https://v.aniu.tv/video/play/id/10812/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-04-13",
        "href": "https://v.aniu.tv/video/play/id/10813/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-04-12",
        "href": "https://v.aniu.tv/video/play/id/10806/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-04-11",
        "href": "https://v.aniu.tv/video/play/id/10762/t/2.shtml"
      },
      {
        "innerHTML": "薛松：资金不断收紧市场会如何变化",
        "href": "https://v.aniu.tv/video/play/id/10727/t/2.shtml"
      },
      {
        "innerHTML": "股票老左2017-04-10",
        "href": "https://v.aniu.tv/video/play/id/10734/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2017-04-10",
        "href": "https://v.aniu.tv/video/play/id/10739/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2017-04-10",
        "href": "https://v.aniu.tv/video/play/id/10741/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-04-06",
        "href": "https://v.aniu.tv/video/play/id/10668/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-04-06",
        "href": "https://v.aniu.tv/video/play/id/10669/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-04-06",
        "href": "https://v.aniu.tv/video/play/id/10670/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-04-05",
        "href": "https://v.aniu.tv/video/play/id/10646/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-03-29",
        "href": "https://v.aniu.tv/video/play/id/10537/t/2.shtml"
      },
      {
        "innerHTML": "昨日华尔街2017-03-28",
        "href": "https://v.aniu.tv/video/play/id/10489/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2017-03-28",
        "href": "https://v.aniu.tv/video/play/id/10490/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-03-28",
        "href": "https://v.aniu.tv/video/play/id/10506/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-03-23",
        "href": "https://v.aniu.tv/video/play/id/10386/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-03-23",
        "href": "https://v.aniu.tv/video/play/id/10387/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-03-23",
        "href": "https://v.aniu.tv/video/play/id/10388/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-03-22",
        "href": "https://v.aniu.tv/video/play/id/10367/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-03-21",
        "href": "https://v.aniu.tv/video/play/id/10348/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2017-03-14",
        "href": "https://v.aniu.tv/video/play/id/10149/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2017-03-09",
        "href": "https://v.aniu.tv/video/play/id/10025/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2017-03-09",
        "href": "https://v.aniu.tv/video/play/id/10024/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2017-03-09",
        "href": "https://v.aniu.tv/video/play/id/10023/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：牛市对支撑位的正确认知",
        "href": "https://v.aniu.tv/video/play/id/51028/t/2.shtml"
      },
      {
        "innerHTML": "薛松：很多时候跟风 也是为了能活着",
        "href": "https://v.aniu.tv/video/play/id/50941/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-08-04",
        "href": "https://v.aniu.tv/video/play/id/50943/t/2.shtml"
      },
      {
        "innerHTML": "薛松：把握股票加速上涨为什么那么难？",
        "href": "https://v.aniu.tv/video/play/id/50912/t/2.shtml"
      },
      {
        "innerHTML": "薛松：宁愿放生  大概率都不太好",
        "href": "https://v.aniu.tv/video/play/id/50825/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-07-30",
        "href": "https://v.aniu.tv/video/play/id/50750/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-07-30",
        "href": "https://v.aniu.tv/video/play/id/50751/t/2.shtml"
      },
      {
        "innerHTML": "薛松：有没有机会，就看成交量能不能跟上",
        "href": "https://v.aniu.tv/video/play/id/50763/t/2.shtml"
      },
      {
        "innerHTML": "薛松：当前市场获利盘正在处于消化当中",
        "href": "https://v.aniu.tv/video/play/id/50764/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-29",
        "href": "https://v.aniu.tv/video/play/id/50712/t/2.shtml"
      },
      {
        "innerHTML": "薛松：来跟上节奏  我们一起摇摆",
        "href": "https://v.aniu.tv/video/play/id/50690/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-28",
        "href": "https://v.aniu.tv/video/play/id/50666/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这才是本轮行情主逻辑",
        "href": "https://v.aniu.tv/video/play/id/50644/t/2.shtml"
      },
      {
        "innerHTML": "薛松：别xia我们 去按住他们的手吧",
        "href": "https://v.aniu.tv/video/play/id/50568/t/2.shtml"
      },
      {
        "innerHTML": "薛松：三三二七  不po不要考虑pao",
        "href": "https://v.aniu.tv/video/play/id/50446/t/2.shtml"
      },
      {
        "innerHTML": "主题投资2020-07-22",
        "href": "https://v.aniu.tv/video/play/id/50457/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-22",
        "href": "https://v.aniu.tv/video/play/id/50437/t/2.shtml"
      },
      {
        "innerHTML": "薛松：盘面走势强 原来是他们",
        "href": "https://v.aniu.tv/video/play/id/50412/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-07-21",
        "href": "https://v.aniu.tv/video/play/id/50385/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-07-21",
        "href": "https://v.aniu.tv/video/play/id/50386/t/2.shtml"
      },
      {
        "innerHTML": "薛松：震荡行情，不是很重要，但也不必过分惊慌",
        "href": "https://v.aniu.tv/video/play/id/50407/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数对于个股最大冲击已经过去了",
        "href": "https://v.aniu.tv/video/play/id/50410/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-21",
        "href": "https://v.aniu.tv/video/play/id/50389/t/2.shtml"
      },
      {
        "innerHTML": "交易封神榜2020-07-19",
        "href": "https://v.aniu.tv/video/play/id/50337/t/2.shtml"
      },
      {
        "innerHTML": "下周透视2020-07-19",
        "href": "https://v.aniu.tv/video/play/id/50338/t/2.shtml"
      },
      {
        "innerHTML": "强势追踪2020-07-19",
        "href": "https://v.aniu.tv/video/play/id/50339/t/2.shtml"
      },
      {
        "innerHTML": "薛松：别被指数带坏心态！",
        "href": "https://v.aniu.tv/video/play/id/50293/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-17",
        "href": "https://v.aniu.tv/video/play/id/50258/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-07-16",
        "href": "https://v.aniu.tv/video/play/id/50212/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-07-16",
        "href": "https://v.aniu.tv/video/play/id/50213/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2020-07-16",
        "href": "https://v.aniu.tv/video/play/id/50216/t/2.shtml"
      },
      {
        "innerHTML": "首席投顾2020-07-16",
        "href": "https://v.aniu.tv/video/play/id/50217/t/2.shtml"
      },
      {
        "innerHTML": "薛松：短期洗盘可能会让很多人难受",
        "href": "https://v.aniu.tv/video/play/id/50223/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投资者之所以有抛盘是因为损失还在承受之内",
        "href": "https://v.aniu.tv/video/play/id/50225/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-07-15",
        "href": "https://v.aniu.tv/video/play/id/50157/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-07-15",
        "href": "https://v.aniu.tv/video/play/id/50159/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这才是本轮行情主逻辑",
        "href": "https://v.aniu.tv/video/play/id/50179/t/2.shtml"
      },
      {
        "innerHTML": "骆明钟：震荡调整不改强势市场",
        "href": "https://v.aniu.tv/video/play/id/50177/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-07-15",
        "href": "https://v.aniu.tv/video/play/id/50162/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "量化热点股 2018-08-16",
        "href": "https://v.aniu.tv/video/play/id/24329/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗 2018-08-16",
        "href": "https://v.aniu.tv/video/play/id/24330/t/2.shtml"
      },
      {
        "innerHTML": "财富广角 2018-08-16",
        "href": "https://v.aniu.tv/video/play/id/24331/t/2.shtml"
      },
      {
        "innerHTML": "热点公告 2018-08-15",
        "href": "https://v.aniu.tv/video/play/id/24307/t/2.shtml"
      },
      {
        "innerHTML": "热点公告 2018-08-14",
        "href": "https://v.aniu.tv/video/play/id/24248/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股 2018-08-09",
        "href": "https://v.aniu.tv/video/play/id/23976/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗 2018-08-09",
        "href": "https://v.aniu.tv/video/play/id/23977/t/2.shtml"
      },
      {
        "innerHTML": "财富广角 2018-08-09",
        "href": "https://v.aniu.tv/video/play/id/23978/t/2.shtml"
      },
      {
        "innerHTML": "热点公告 2018-08-08",
        "href": "https://v.aniu.tv/video/play/id/23957/t/2.shtml"
      },
      {
        "innerHTML": "薛松：安全是医药类注意的首要问题  “退市”更多伤害的流通股股东们",
        "href": "https://v.aniu.tv/video/play/id/23869/t/2.shtml"
      },
      {
        "innerHTML": "薛松：养老金虽未入市 但促使其他场外资金入场",
        "href": "https://v.aniu.tv/video/play/id/23867/t/2.shtml"
      },
      {
        "innerHTML": "付少琪：反弹期待已久 接下来主要贸易摩擦",
        "href": "https://v.aniu.tv/video/play/id/23866/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-08-07",
        "href": "https://v.aniu.tv/video/play/id/23888/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-08-02",
        "href": "https://v.aniu.tv/video/play/id/23591/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-08-02",
        "href": "https://v.aniu.tv/video/play/id/23592/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-08-02",
        "href": "https://v.aniu.tv/video/play/id/23593/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-08-01",
        "href": "https://v.aniu.tv/video/play/id/23583/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-07-31",
        "href": "https://v.aniu.tv/video/play/id/23481/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-07-26",
        "href": "https://v.aniu.tv/video/play/id/23230/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-07-26",
        "href": "https://v.aniu.tv/video/play/id/23231/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-07-26",
        "href": "https://v.aniu.tv/video/play/id/23232/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-07-25",
        "href": "https://v.aniu.tv/video/play/id/23217/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-07-24",
        "href": "https://v.aniu.tv/video/play/id/23137/t/2.shtml"
      },
      {
        "innerHTML": "林整华：若股指站稳2828点 果断做多！",
        "href": "https://v.aniu.tv/video/play/id/22927/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-07-19",
        "href": "https://v.aniu.tv/video/play/id/22907/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-07-19",
        "href": "https://v.aniu.tv/video/play/id/22908/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-07-19",
        "href": "https://v.aniu.tv/video/play/id/22909/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-07-18",
        "href": "https://v.aniu.tv/video/play/id/22894/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-07-17",
        "href": "https://v.aniu.tv/video/play/id/22816/t/2.shtml"
      },
      {
        "innerHTML": "薛松：边际效应逐渐降低 股指大概率反抽3000点",
        "href": "https://v.aniu.tv/video/play/id/22552/t/2.shtml"
      },
      {
        "innerHTML": "追涨杀跌的小技巧",
        "href": "https://v.aniu.tv/video/play/id/22555/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-07-12",
        "href": "https://v.aniu.tv/video/play/id/22573/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-07-12",
        "href": "https://v.aniu.tv/video/play/id/22574/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-07-12",
        "href": "https://v.aniu.tv/video/play/id/22575/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-07-11",
        "href": "https://v.aniu.tv/video/play/id/22561/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-07-10",
        "href": "https://v.aniu.tv/video/play/id/22466/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-07-05",
        "href": "https://v.aniu.tv/video/play/id/22225/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-07-05",
        "href": "https://v.aniu.tv/video/play/id/22226/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-07-05",
        "href": "https://v.aniu.tv/video/play/id/22239/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：不以一时论短长 巴菲特也亏过钱",
        "href": "https://v.aniu.tv/video/play/id/63661/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为什么选择指数基金？",
        "href": "https://v.aniu.tv/video/play/id/63704/t/2.shtml"
      },
      {
        "innerHTML": "薛松：术业有专攻！做自己擅长的领域",
        "href": "https://v.aniu.tv/video/play/id/63716/t/2.shtml"
      },
      {
        "innerHTML": "薛松：指数长存 百年老字号难得",
        "href": "https://v.aniu.tv/video/play/id/63719/t/2.shtml"
      },
      {
        "innerHTML": "薛松：论历史上的泡沫 股市炒的是预期",
        "href": "https://v.aniu.tv/video/play/id/63440/t/2.shtml"
      },
      {
        "innerHTML": "薛松：有些投资者幸好没有成为警察",
        "href": "https://v.aniu.tv/video/play/id/63333/t/2.shtml"
      },
      {
        "innerHTML": "薛松：论博弈的复杂性",
        "href": "https://v.aniu.tv/video/play/id/63334/t/2.shtml"
      },
      {
        "innerHTML": "薛松：看破不说破 只能告诉你们这些了",
        "href": "https://v.aniu.tv/video/play/id/63335/t/2.shtml"
      },
      {
        "innerHTML": "薛松：敢于拓展 勇于试错",
        "href": "https://v.aniu.tv/video/play/id/63381/t/2.shtml"
      },
      {
        "innerHTML": "薛松：学技术很容易变成反面派",
        "href": "https://v.aniu.tv/video/play/id/63396/t/2.shtml"
      },
      {
        "innerHTML": "薛松：股票市场中的反向运动",
        "href": "https://v.aniu.tv/video/play/id/63397/t/2.shtml"
      },
      {
        "innerHTML": "薛松：真正的重仓、上杠杆是要大对的时候",
        "href": "https://v.aniu.tv/video/play/id/63398/t/2.shtml"
      },
      {
        "innerHTML": "薛松：该阶段的见底信号是波动率放大",
        "href": "https://v.aniu.tv/video/play/id/63406/t/2.shtml"
      },
      {
        "innerHTML": "薛松：对于期权的见解",
        "href": "https://v.aniu.tv/video/play/id/63285/t/2.shtml"
      },
      {
        "innerHTML": "薛松：我朋友被打的经历",
        "href": "https://v.aniu.tv/video/play/id/63288/t/2.shtml"
      },
      {
        "innerHTML": "薛松：趋势高点往往是老手作祟",
        "href": "https://v.aniu.tv/video/play/id/63290/t/2.shtml"
      },
      {
        "innerHTML": "薛松：我永远可以相信史玉柱",
        "href": "https://v.aniu.tv/video/play/id/63291/t/2.shtml"
      },
      {
        "innerHTML": "薛松：极致的规律随时会改变",
        "href": "https://v.aniu.tv/video/play/id/63292/t/2.shtml"
      },
      {
        "innerHTML": "薛松：要想创业 先学打工",
        "href": "https://v.aniu.tv/video/play/id/63304/t/2.shtml"
      },
      {
        "innerHTML": "薛松：什么是优秀标的？",
        "href": "https://v.aniu.tv/video/play/id/63311/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美国十年国债收益率对股市有何影响",
        "href": "https://v.aniu.tv/video/play/id/63137/t/2.shtml"
      },
      {
        "innerHTML": "薛松：领涨重名选股法",
        "href": "https://v.aniu.tv/video/play/id/62885/t/2.shtml"
      },
      {
        "innerHTML": "薛松：新手只爱暴扣 老手归于平淡",
        "href": "https://v.aniu.tv/video/play/id/62889/t/2.shtml"
      },
      {
        "innerHTML": "薛松：论历史上的泡沫 股市炒的是预期",
        "href": "https://v.aniu.tv/video/play/id/62913/t/2.shtml"
      },
      {
        "innerHTML": "薛松：四、五月行情不过如此",
        "href": "https://v.aniu.tv/video/play/id/62748/t/2.shtml"
      },
      {
        "innerHTML": "薛松：热点预计将开启",
        "href": "https://v.aniu.tv/video/play/id/62757/t/2.shtml"
      },
      {
        "innerHTML": "薛松：最理想标的选取条件",
        "href": "https://v.aniu.tv/video/play/id/62758/t/2.shtml"
      },
      {
        "innerHTML": "薛松：可以告诉你 但是不能说",
        "href": "https://v.aniu.tv/video/play/id/62767/t/2.shtml"
      },
      {
        "innerHTML": "薛松：关注创业板ETF",
        "href": "https://v.aniu.tv/video/play/id/62454/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为何我说没有理想品种",
        "href": "https://v.aniu.tv/video/play/id/62456/t/2.shtml"
      },
      {
        "innerHTML": "薛松：实不相瞒 真不能细说",
        "href": "https://v.aniu.tv/video/play/id/62343/t/2.shtml"
      },
      {
        "innerHTML": "薛松：三问三答",
        "href": "https://v.aniu.tv/video/play/id/62357/t/2.shtml"
      },
      {
        "innerHTML": "薛松：认清自己 预期不要太高",
        "href": "https://v.aniu.tv/video/play/id/62358/t/2.shtml"
      },
      {
        "innerHTML": "薛松：公开股评形成博弈 事情逐渐变得复杂",
        "href": "https://v.aniu.tv/video/play/id/62360/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不要在意指数空间",
        "href": "https://v.aniu.tv/video/play/id/62369/t/2.shtml"
      },
      {
        "innerHTML": "薛松：那就以一段绕口令开启我的表演！",
        "href": "https://v.aniu.tv/video/play/id/61924/t/2.shtml"
      },
      {
        "innerHTML": "薛松：股神是否会满仓？",
        "href": "https://v.aniu.tv/video/play/id/61929/t/2.shtml"
      },
      {
        "innerHTML": "薛松：逆向思维才是机会所在！",
        "href": "https://v.aniu.tv/video/play/id/61931/t/2.shtml"
      },
      {
        "innerHTML": "薛松：交易所才知道的真相",
        "href": "https://v.aniu.tv/video/play/id/61932/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2015-09-02",
        "href": "https://v.aniu.tv/video/play/id/914/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-31",
        "href": "https://v.aniu.tv/video/play/id/906/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-31",
        "href": "https://v.aniu.tv/video/play/id/906/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-28",
        "href": "https://v.aniu.tv/video/play/id/890/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-27",
        "href": "https://v.aniu.tv/video/play/id/885/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-26",
        "href": "https://v.aniu.tv/video/play/id/878/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-24",
        "href": "https://v.aniu.tv/video/play/id/870/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-21",
        "href": "https://v.aniu.tv/video/play/id/860/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-20",
        "href": "https://v.aniu.tv/video/play/id/851/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-19",
        "href": "https://v.aniu.tv/video/play/id/844/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-17",
        "href": "https://v.aniu.tv/video/play/id/834/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-14",
        "href": "https://v.aniu.tv/video/play/id/841/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-13",
        "href": "https://v.aniu.tv/video/play/id/814/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-12",
        "href": "https://v.aniu.tv/video/play/id/810/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-08-10",
        "href": "https://v.aniu.tv/video/play/id/796/t/2.shtml"
      },
      {
        "innerHTML": "热点公告-2015-08-07",
        "href": "https://v.aniu.tv/video/play/id/785/t/2.shtml"
      },
      {
        "innerHTML": "热点公告-2015-08-07",
        "href": "https://v.aniu.tv/video/play/id/777/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-30",
        "href": "https://v.aniu.tv/video/play/id/754/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-29",
        "href": "https://v.aniu.tv/video/play/id/750/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-27",
        "href": "https://v.aniu.tv/video/play/id/746/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-23",
        "href": "https://v.aniu.tv/video/play/id/718/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-22",
        "href": "https://v.aniu.tv/video/play/id/715/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-20",
        "href": "https://v.aniu.tv/video/play/id/706/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-17",
        "href": "https://v.aniu.tv/video/play/id/685/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-16",
        "href": "https://v.aniu.tv/video/play/id/680/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-15",
        "href": "https://v.aniu.tv/video/play/id/676/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-13",
        "href": "https://v.aniu.tv/video/play/id/666/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-10",
        "href": "https://v.aniu.tv/video/play/id/651/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-09",
        "href": "https://v.aniu.tv/video/play/id/647/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-07",
        "href": "https://v.aniu.tv/video/play/id/635/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-06",
        "href": "https://v.aniu.tv/video/play/id/631/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-03",
        "href": "https://v.aniu.tv/video/play/id/615/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-02",
        "href": "https://v.aniu.tv/video/play/id/613/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2015-07-01",
        "href": "https://v.aniu.tv/video/play/id/607/t/2.shtml"
      },
      {
        "innerHTML": "热点公告-2015-06-30",
        "href": "https://v.aniu.tv/video/play/id/594/t/2.shtml"
      },
      {
        "innerHTML": "热点公告-2015-06-26",
        "href": "https://v.aniu.tv/video/play/id/581/t/2.shtml"
      },
      {
        "innerHTML": "热点公告-2015-06-11",
        "href": "https://v.aniu.tv/video/play/id/529/t/2.shtml"
      },
      {
        "innerHTML": "热点公告-2015-06-10",
        "href": "https://v.aniu.tv/video/play/id/525/t/2.shtml"
      },
      {
        "innerHTML": "热点公告-2015-06-08",
        "href": "https://v.aniu.tv/video/play/id/519/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "反弹可能触及3000点 持续两周",
        "href": "https://v.aniu.tv/video/play/id/22127/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-07-04",
        "href": "https://v.aniu.tv/video/play/id/22215/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-07-03",
        "href": "https://v.aniu.tv/video/play/id/22153/t/2.shtml"
      },
      {
        "innerHTML": "指数震荡筑底需耐心 大小票切换控节奏",
        "href": "https://v.aniu.tv/video/play/id/21864/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-06-28",
        "href": "https://v.aniu.tv/video/play/id/21889/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-06-28",
        "href": "https://v.aniu.tv/video/play/id/21890/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-06-28",
        "href": "https://v.aniu.tv/video/play/id/21891/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-06-27",
        "href": "https://v.aniu.tv/video/play/id/21831/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-06-27",
        "href": "https://v.aniu.tv/video/play/id/21881/t/2.shtml"
      },
      {
        "innerHTML": "市场继续调整属于大概率",
        "href": "https://v.aniu.tv/video/play/id/21518/t/2.shtml"
      },
      {
        "innerHTML": "反弹受压5日线，多头变空头",
        "href": "https://v.aniu.tv/video/play/id/21519/t/2.shtml"
      },
      {
        "innerHTML": "看24节气炒股票",
        "href": "https://v.aniu.tv/video/play/id/21527/t/2.shtml"
      },
      {
        "innerHTML": "券商是股权质押最大的受害者",
        "href": "https://v.aniu.tv/video/play/id/21528/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-06-21",
        "href": "https://v.aniu.tv/video/play/id/21536/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-06-21",
        "href": "https://v.aniu.tv/video/play/id/21538/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-06-21",
        "href": "https://v.aniu.tv/video/play/id/21539/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-06-20",
        "href": "https://v.aniu.tv/video/play/id/21463/t/2.shtml"
      },
      {
        "innerHTML": "创业板指数创新低后，积极看好创业板",
        "href": "https://v.aniu.tv/video/play/id/21441/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-06-19",
        "href": "https://v.aniu.tv/video/play/id/21394/t/2.shtml"
      },
      {
        "innerHTML": "创业板马上要反弹？",
        "href": "https://v.aniu.tv/video/play/id/21273/t/2.shtml"
      },
      {
        "innerHTML": "券商的机会在哪里？",
        "href": "https://v.aniu.tv/video/play/id/21272/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-06-14",
        "href": "https://v.aniu.tv/video/play/id/21254/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-06-14",
        "href": "https://v.aniu.tv/video/play/id/21255/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-06-14",
        "href": "https://v.aniu.tv/video/play/id/21256/t/2.shtml"
      },
      {
        "innerHTML": "高速公路的公司风险点在哪？",
        "href": "https://v.aniu.tv/video/play/id/21244/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-06-13",
        "href": "https://v.aniu.tv/video/play/id/21201/t/2.shtml"
      },
      {
        "innerHTML": "A股港股化趋势明显",
        "href": "https://v.aniu.tv/video/play/id/21179/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2018-06-12",
        "href": "https://v.aniu.tv/video/play/id/21138/t/2.shtml"
      },
      {
        "innerHTML": "连续5天判断大盘错误",
        "href": "https://v.aniu.tv/video/play/id/20949/t/2.shtml"
      },
      {
        "innerHTML": "连续5天判断大盘错误",
        "href": "https://v.aniu.tv/video/play/id/20949/t/2.shtml"
      },
      {
        "innerHTML": "券商股近期不会有大行情",
        "href": "https://v.aniu.tv/video/play/id/20950/t/2.shtml"
      },
      {
        "innerHTML": "看好高市净率个股持续走强",
        "href": "https://v.aniu.tv/video/play/id/20951/t/2.shtml"
      },
      {
        "innerHTML": "回避海南，关注大消费和涨价",
        "href": "https://v.aniu.tv/video/play/id/20952/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2018-06-07",
        "href": "https://v.aniu.tv/video/play/id/20934/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2018-06-07",
        "href": "https://v.aniu.tv/video/play/id/20935/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2018-06-07",
        "href": "https://v.aniu.tv/video/play/id/20936/t/2.shtml"
      },
      {
        "innerHTML": "攻击量能未能有效放大，需要进一步观察",
        "href": "https://v.aniu.tv/video/play/id/20955/t/2.shtml"
      },
      {
        "innerHTML": "谨防指标钝化的下杀，行情大分化格局",
        "href": "https://v.aniu.tv/video/play/id/20956/t/2.shtml"
      },
      {
        "innerHTML": "茅台破万亿的看法",
        "href": "https://v.aniu.tv/video/play/id/20922/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：什么叫好？",
        "href": "https://v.aniu.tv/video/play/id/59352/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2021-02-03",
        "href": "https://v.aniu.tv/video/play/id/59321/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2021-02-02",
        "href": "https://v.aniu.tv/video/play/id/59367/t/2.shtml"
      },
      {
        "innerHTML": "薛松：买的时候是大爷 抛的时候呢？",
        "href": "https://v.aniu.tv/video/play/id/59037/t/2.shtml"
      },
      {
        "innerHTML": "薛松：把握股价运行 不要凉凉",
        "href": "https://v.aniu.tv/video/play/id/59038/t/2.shtml"
      },
      {
        "innerHTML": "薛松：论理性经济人",
        "href": "https://v.aniu.tv/video/play/id/59039/t/2.shtml"
      },
      {
        "innerHTML": "薛松：点点理论",
        "href": "https://v.aniu.tv/video/play/id/59066/t/2.shtml"
      },
      {
        "innerHTML": "薛松：戏说港股爆款现象",
        "href": "https://v.aniu.tv/video/play/id/59067/t/2.shtml"
      },
      {
        "innerHTML": "薛松：聪明人不仅仅要看报表！",
        "href": "https://v.aniu.tv/video/play/id/59073/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2021-01-28",
        "href": "https://v.aniu.tv/video/play/id/59028/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2021-01-28",
        "href": "https://v.aniu.tv/video/play/id/59029/t/2.shtml"
      },
      {
        "innerHTML": "薛松：勿做巨婴 要做自强投资人",
        "href": "https://v.aniu.tv/video/play/id/58963/t/2.shtml"
      },
      {
        "innerHTML": "薛松：凡事先找自己问题",
        "href": "https://v.aniu.tv/video/play/id/58964/t/2.shtml"
      },
      {
        "innerHTML": "薛松：砍仓是好东西 股神还是韭菜？",
        "href": "https://v.aniu.tv/video/play/id/58965/t/2.shtml"
      },
      {
        "innerHTML": "薛松：可考虑投资基金的基金",
        "href": "https://v.aniu.tv/video/play/id/58966/t/2.shtml"
      },
      {
        "innerHTML": "薛松：涨也爆仓 跌亦爆仓",
        "href": "https://v.aniu.tv/video/play/id/58967/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2021-01-27",
        "href": "https://v.aniu.tv/video/play/id/58961/t/2.shtml"
      },
      {
        "innerHTML": "薛松：早被人消耗完了",
        "href": "https://v.aniu.tv/video/play/id/59481/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2021-01-26",
        "href": "https://v.aniu.tv/video/play/id/58869/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投资基金的基金",
        "href": "https://v.aniu.tv/video/play/id/59014/t/2.shtml"
      },
      {
        "innerHTML": "薛松：记吃不记打，吃肉固然爽",
        "href": "https://v.aniu.tv/video/play/id/58617/t/2.shtml"
      },
      {
        "innerHTML": "薛松：熊市为友 牛市为患",
        "href": "https://v.aniu.tv/video/play/id/58619/t/2.shtml"
      },
      {
        "innerHTML": "薛松：热水已过时！我负责泼冷水！",
        "href": "https://v.aniu.tv/video/play/id/58629/t/2.shtml"
      },
      {
        "innerHTML": "薛松：水浒视角解说",
        "href": "https://v.aniu.tv/video/play/id/58553/t/2.shtml"
      },
      {
        "innerHTML": "薛松：刘思涵为什么爱穿高跟鞋？",
        "href": "https://v.aniu.tv/video/play/id/58554/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2021-01-20",
        "href": "https://v.aniu.tv/video/play/id/58545/t/2.shtml"
      },
      {
        "innerHTML": "薛松：关公人刀论",
        "href": "https://v.aniu.tv/video/play/id/58498/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2021-01-19",
        "href": "https://v.aniu.tv/video/play/id/58449/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2021-01-19",
        "href": "https://v.aniu.tv/video/play/id/58450/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2021-01-19",
        "href": "https://v.aniu.tv/video/play/id/58455/t/2.shtml"
      },
      {
        "innerHTML": "薛松：关于机构的思考",
        "href": "https://v.aniu.tv/video/play/id/58422/t/2.shtml"
      },
      {
        "innerHTML": "聚焦新动能2021-01-17",
        "href": "https://v.aniu.tv/video/play/id/58462/t/2.shtml"
      },
      {
        "innerHTML": "交易封神榜2021-01-17",
        "href": "https://v.aniu.tv/video/play/id/58463/t/2.shtml"
      },
      {
        "innerHTML": "薛松：土匪都讲战略！我们更有有所计划",
        "href": "https://v.aniu.tv/video/play/id/58219/t/2.shtml"
      },
      {
        "innerHTML": "薛松：牛市下的灵魂拷问",
        "href": "https://v.aniu.tv/video/play/id/58220/t/2.shtml"
      },
      {
        "innerHTML": "薛松：抱团？没那么简单！",
        "href": "https://v.aniu.tv/video/play/id/58148/t/2.shtml"
      },
      {
        "innerHTML": "薛松：临界点效应的市场",
        "href": "https://v.aniu.tv/video/play/id/58158/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2021-01-13",
        "href": "https://v.aniu.tv/video/play/id/58144/t/2.shtml"
      },
      {
        "innerHTML": "薛松：对于信息公告我有话要说",
        "href": "https://v.aniu.tv/video/play/id/58067/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "财富广角2016-04-07",
        "href": "https://v.aniu.tv/video/play/id/3472/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-04-07",
        "href": "https://v.aniu.tv/video/play/id/3496/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-04-07",
        "href": "https://v.aniu.tv/video/play/id/3480/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-03-31",
        "href": "https://v.aniu.tv/video/play/id/3395/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-03-31",
        "href": "https://v.aniu.tv/video/play/id/3396/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-03-31",
        "href": "https://v.aniu.tv/video/play/id/3397/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-03-31",
        "href": "https://v.aniu.tv/video/play/id/3403/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-03-30",
        "href": "https://v.aniu.tv/video/play/id/3390/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-03-24",
        "href": "https://v.aniu.tv/video/play/id/3288/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-03-24",
        "href": "https://v.aniu.tv/video/play/id/3296/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-03-24",
        "href": "https://v.aniu.tv/video/play/id/3304/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-03-24",
        "href": "https://v.aniu.tv/video/play/id/3305/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-03-23",
        "href": "https://v.aniu.tv/video/play/id/3279/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-03-17",
        "href": "https://v.aniu.tv/video/play/id/3190/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-03-17",
        "href": "https://v.aniu.tv/video/play/id/3191/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-03-17",
        "href": "https://v.aniu.tv/video/play/id/3196/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-03-17",
        "href": "https://v.aniu.tv/video/play/id/4770/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-03-16",
        "href": "https://v.aniu.tv/video/play/id/3183/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-03-10",
        "href": "https://v.aniu.tv/video/play/id/3094/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-03-10",
        "href": "https://v.aniu.tv/video/play/id/3095/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-03-10",
        "href": "https://v.aniu.tv/video/play/id/3096/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-03-10",
        "href": "https://v.aniu.tv/video/play/id/3104/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-03-09",
        "href": "https://v.aniu.tv/video/play/id/3086/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-03-03",
        "href": "https://v.aniu.tv/video/play/id/2989/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-03-03",
        "href": "https://v.aniu.tv/video/play/id/2990/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-03-03",
        "href": "https://v.aniu.tv/video/play/id/2991/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-03-03",
        "href": "https://v.aniu.tv/video/play/id/2997/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-02-25",
        "href": "https://v.aniu.tv/video/play/id/2883/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-02-25",
        "href": "https://v.aniu.tv/video/play/id/2884/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-02-25",
        "href": "https://v.aniu.tv/video/play/id/2887/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-02-25",
        "href": "https://v.aniu.tv/video/play/id/2894/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-02-24",
        "href": "https://v.aniu.tv/video/play/id/2877/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-01-21",
        "href": "https://v.aniu.tv/video/play/id/2504/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-01-21",
        "href": "https://v.aniu.tv/video/play/id/2505/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-01-21",
        "href": "https://v.aniu.tv/video/play/id/2513/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-01-21",
        "href": "https://v.aniu.tv/video/play/id/2502/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-01-20",
        "href": "https://v.aniu.tv/video/play/id/2487/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-01-14",
        "href": "https://v.aniu.tv/video/play/id/2359/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-01-14",
        "href": "https://v.aniu.tv/video/play/id/2360/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2016-07-20",
        "href": "https://v.aniu.tv/video/play/id/5040/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-07-14",
        "href": "https://v.aniu.tv/video/play/id/4942/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-07-14",
        "href": "https://v.aniu.tv/video/play/id/4941/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-07-14",
        "href": "https://v.aniu.tv/video/play/id/4940/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-07-14",
        "href": "https://v.aniu.tv/video/play/id/4949/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-07-13",
        "href": "https://v.aniu.tv/video/play/id/4931/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-07-12",
        "href": "https://v.aniu.tv/video/play/id/4912/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-07-07",
        "href": "https://v.aniu.tv/video/play/id/4838/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-07-07",
        "href": "https://v.aniu.tv/video/play/id/4832/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-07-07",
        "href": "https://v.aniu.tv/video/play/id/4831/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-07-07",
        "href": "https://v.aniu.tv/video/play/id/4830/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-07-06",
        "href": "https://v.aniu.tv/video/play/id/4822/t/2.shtml"
      },
      {
        "innerHTML": "昨夜华尔街2016-07-05",
        "href": "https://v.aniu.tv/video/play/id/4781/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-07-05",
        "href": "https://v.aniu.tv/video/play/id/4798/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2016-07-05",
        "href": "https://v.aniu.tv/video/play/id/4782/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-06-30",
        "href": "https://v.aniu.tv/video/play/id/4715/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-06-30",
        "href": "https://v.aniu.tv/video/play/id/4714/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-06-30",
        "href": "https://v.aniu.tv/video/play/id/4716/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-06-29",
        "href": "https://v.aniu.tv/video/play/id/4702/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-06-28",
        "href": "https://v.aniu.tv/video/play/id/4684/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-06-23",
        "href": "https://v.aniu.tv/video/play/id/4609/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-06-23",
        "href": "https://v.aniu.tv/video/play/id/4602/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-06-23",
        "href": "https://v.aniu.tv/video/play/id/4601/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-06-23",
        "href": "https://v.aniu.tv/video/play/id/4600/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-06-22",
        "href": "https://v.aniu.tv/video/play/id/4589/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-06-21",
        "href": "https://v.aniu.tv/video/play/id/4566/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-06-20",
        "href": "https://v.aniu.tv/video/play/id/4536/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-06-17",
        "href": "https://v.aniu.tv/video/play/id/4508/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-06-17",
        "href": "https://v.aniu.tv/video/play/id/4507/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-06-17",
        "href": "https://v.aniu.tv/video/play/id/4506/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-06-16",
        "href": "https://v.aniu.tv/video/play/id/4490/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-06-16",
        "href": "https://v.aniu.tv/video/play/id/4489/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-06-16",
        "href": "https://v.aniu.tv/video/play/id/4488/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-06-16",
        "href": "https://v.aniu.tv/video/play/id/4499/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-06-08",
        "href": "https://v.aniu.tv/video/play/id/4454/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-06-07",
        "href": "https://v.aniu.tv/video/play/id/4378/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-06-02",
        "href": "https://v.aniu.tv/video/play/id/4302/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-06-02",
        "href": "https://v.aniu.tv/video/play/id/4303/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-06-02",
        "href": "https://v.aniu.tv/video/play/id/4304/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "王雨厚：这是一个具有想象空间的新题材",
        "href": "https://v.aniu.tv/video/play/id/46681/t/2.shtml"
      },
      {
        "innerHTML": "申振：留意涉及跨界的投资机会",
        "href": "https://v.aniu.tv/video/play/id/46661/t/2.shtml"
      },
      {
        "innerHTML": "画说基本面2020-04-09",
        "href": "https://v.aniu.tv/video/play/id/46658/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-04-09",
        "href": "https://v.aniu.tv/video/play/id/46660/t/2.shtml"
      },
      {
        "innerHTML": "薛松：臣妾做的到吗，你们是自由的",
        "href": "https://v.aniu.tv/video/play/id/46608/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2020-04-08",
        "href": "https://v.aniu.tv/video/play/id/46631/t/2.shtml"
      },
      {
        "innerHTML": "择时有技2020-04-08",
        "href": "https://v.aniu.tv/video/play/id/46632/t/2.shtml"
      },
      {
        "innerHTML": "股谈老司机2020-04-08",
        "href": "https://v.aniu.tv/video/play/id/46633/t/2.shtml"
      },
      {
        "innerHTML": "主题投资2020-04-08",
        "href": "https://v.aniu.tv/video/play/id/46639/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-04-08",
        "href": "https://v.aniu.tv/video/play/id/46680/t/2.shtml"
      },
      {
        "innerHTML": "林整华：弱势行情下，一种简单的方法或可帮你降低成本！",
        "href": "https://v.aniu.tv/video/play/id/46540/t/2.shtml"
      },
      {
        "innerHTML": "林整华:多空力量平衡 基本面等待经济数据",
        "href": "https://v.aniu.tv/video/play/id/46541/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-04-07",
        "href": "https://v.aniu.tv/video/play/id/46591/t/2.shtml"
      },
      {
        "innerHTML": "薛松：均线使用法则",
        "href": "https://v.aniu.tv/video/play/id/46509/t/2.shtml"
      },
      {
        "innerHTML": "薛松：均值回归下A股上涨最慢的根本原因",
        "href": "https://v.aniu.tv/video/play/id/46481/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-04-02",
        "href": "https://v.aniu.tv/video/play/id/46465/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-04-02",
        "href": "https://v.aniu.tv/video/play/id/46466/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-04-02",
        "href": "https://v.aniu.tv/video/play/id/46467/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-04-01",
        "href": "https://v.aniu.tv/video/play/id/46442/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-03-31",
        "href": "https://v.aniu.tv/video/play/id/46414/t/2.shtml"
      },
      {
        "innerHTML": "薛松：现在还是牛市，牛市杀人更快",
        "href": "https://v.aniu.tv/video/play/id/46341/t/2.shtml"
      },
      {
        "innerHTML": "股谈老司机2020-03-27",
        "href": "https://v.aniu.tv/video/play/id/46345/t/2.shtml"
      },
      {
        "innerHTML": "短线客2020-03-27",
        "href": "https://v.aniu.tv/video/play/id/46346/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2020-03-27",
        "href": "https://v.aniu.tv/video/play/id/46343/t/2.shtml"
      },
      {
        "innerHTML": "择时有技2020-03-27",
        "href": "https://v.aniu.tv/video/play/id/46344/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-03-27",
        "href": "https://v.aniu.tv/video/play/id/46353/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-03-26",
        "href": "https://v.aniu.tv/video/play/id/46330/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-03-26",
        "href": "https://v.aniu.tv/video/play/id/46331/t/2.shtml"
      },
      {
        "innerHTML": "薛松：北上资金流动和A股关系",
        "href": "https://v.aniu.tv/video/play/id/46333/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-03-26",
        "href": "https://v.aniu.tv/video/play/id/46332/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-03-25",
        "href": "https://v.aniu.tv/video/play/id/46289/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-03-24",
        "href": "https://v.aniu.tv/video/play/id/46251/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2020-03-23",
        "href": "https://v.aniu.tv/video/play/id/46170/t/2.shtml"
      },
      {
        "innerHTML": "择时有技2020-03-23",
        "href": "https://v.aniu.tv/video/play/id/46171/t/2.shtml"
      },
      {
        "innerHTML": "薛松：下跌中继还是底部？仓位控制才是关键",
        "href": "https://v.aniu.tv/video/play/id/46209/t/2.shtml"
      },
      {
        "innerHTML": "股谈老司机2020-03-23",
        "href": "https://v.aniu.tv/video/play/id/46172/t/2.shtml"
      },
      {
        "innerHTML": "短线客2020-03-23",
        "href": "https://v.aniu.tv/video/play/id/46173/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-03-19",
        "href": "https://v.aniu.tv/video/play/id/46067/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-03-19",
        "href": "https://v.aniu.tv/video/play/id/46068/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "股市诊疗2020-02-20",
        "href": "https://v.aniu.tv/video/play/id/45017/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-02-20",
        "href": "https://v.aniu.tv/video/play/id/45018/t/2.shtml"
      },
      {
        "innerHTML": "薛松：成功一点点 被风吹得不知道姓什么",
        "href": "https://v.aniu.tv/video/play/id/45049/t/2.shtml"
      },
      {
        "innerHTML": "林整华：这种攻击态势最强势",
        "href": "https://v.aniu.tv/video/play/id/45044/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-02-20",
        "href": "https://v.aniu.tv/video/play/id/45031/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-02-19",
        "href": "https://v.aniu.tv/video/play/id/44986/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-02-18",
        "href": "https://v.aniu.tv/video/play/id/44940/t/2.shtml"
      },
      {
        "innerHTML": "财经点将台2020-02-15",
        "href": "https://v.aniu.tv/video/play/id/44841/t/2.shtml"
      },
      {
        "innerHTML": "公告追踪2020-02-15",
        "href": "https://v.aniu.tv/video/play/id/44842/t/2.shtml"
      },
      {
        "innerHTML": "薛松：放开价格，市场会解决问题",
        "href": "https://v.aniu.tv/video/play/id/44819/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-02-14",
        "href": "https://v.aniu.tv/video/play/id/44795/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2020-02-14",
        "href": "https://v.aniu.tv/video/play/id/44796/t/2.shtml"
      },
      {
        "innerHTML": "首席投顾2020-02-14",
        "href": "https://v.aniu.tv/video/play/id/44797/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2020-02-14",
        "href": "https://v.aniu.tv/video/play/id/44798/t/2.shtml"
      },
      {
        "innerHTML": "择时有技2020-02-14",
        "href": "https://v.aniu.tv/video/play/id/44799/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-02-13",
        "href": "https://v.aniu.tv/video/play/id/44753/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-02-13",
        "href": "https://v.aniu.tv/video/play/id/44754/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-02-13",
        "href": "https://v.aniu.tv/video/play/id/44755/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-02-13",
        "href": "https://v.aniu.tv/video/play/id/44756/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2020-02-13",
        "href": "https://v.aniu.tv/video/play/id/44757/t/2.shtml"
      },
      {
        "innerHTML": "首席投顾2020-02-13",
        "href": "https://v.aniu.tv/video/play/id/44758/t/2.shtml"
      },
      {
        "innerHTML": "薛松：新能源汽车层层挖掘 锂电供应链的机会",
        "href": "https://v.aniu.tv/video/play/id/44781/t/2.shtml"
      },
      {
        "innerHTML": "薛松：形成交易系统 发挥自己天赋",
        "href": "https://v.aniu.tv/video/play/id/44780/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-02-13",
        "href": "https://v.aniu.tv/video/play/id/44768/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-02-12",
        "href": "https://v.aniu.tv/video/play/id/44710/t/2.shtml"
      },
      {
        "innerHTML": "薛松：跑赢大盘的方法！",
        "href": "https://v.aniu.tv/video/play/id/44743/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2020-02-12",
        "href": "https://v.aniu.tv/video/play/id/44711/t/2.shtml"
      },
      {
        "innerHTML": "首席投顾2020-02-12",
        "href": "https://v.aniu.tv/video/play/id/44712/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2020-02-12",
        "href": "https://v.aniu.tv/video/play/id/44713/t/2.shtml"
      },
      {
        "innerHTML": "择时有技2020-02-12",
        "href": "https://v.aniu.tv/video/play/id/44714/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-02-12",
        "href": "https://v.aniu.tv/video/play/id/44722/t/2.shtml"
      },
      {
        "innerHTML": "纪宗逸：反弹告一段落，回调没有空间",
        "href": "https://v.aniu.tv/video/play/id/44697/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-02-11",
        "href": "https://v.aniu.tv/video/play/id/44671/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2020-02-11",
        "href": "https://v.aniu.tv/video/play/id/44672/t/2.shtml"
      },
      {
        "innerHTML": "首席投顾2020-02-11",
        "href": "https://v.aniu.tv/video/play/id/44673/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2020-02-11",
        "href": "https://v.aniu.tv/video/play/id/44674/t/2.shtml"
      },
      {
        "innerHTML": "择时有技2020-02-11",
        "href": "https://v.aniu.tv/video/play/id/44675/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-02-11",
        "href": "https://v.aniu.tv/video/play/id/44683/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-02-10",
        "href": "https://v.aniu.tv/video/play/id/44653/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "财富广角2020-03-19",
        "href": "https://v.aniu.tv/video/play/id/46069/t/2.shtml"
      },
      {
        "innerHTML": "薛松：单因子指标以到位，底部或能浮现",
        "href": "https://v.aniu.tv/video/play/id/46078/t/2.shtml"
      },
      {
        "innerHTML": "薛松：北上资金流动和A股关系",
        "href": "https://v.aniu.tv/video/play/id/46076/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-03-19",
        "href": "https://v.aniu.tv/video/play/id/46089/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-03-18",
        "href": "https://v.aniu.tv/video/play/id/46028/t/2.shtml"
      },
      {
        "innerHTML": "薛松：散户行情就老鼠会 只看一样东西",
        "href": "https://v.aniu.tv/video/play/id/46001/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-03-17",
        "href": "https://v.aniu.tv/video/play/id/46012/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2020-03-16",
        "href": "https://v.aniu.tv/video/play/id/45950/t/2.shtml"
      },
      {
        "innerHTML": "择时有技2020-03-16",
        "href": "https://v.aniu.tv/video/play/id/45951/t/2.shtml"
      },
      {
        "innerHTML": "股谈老司机2020-03-16",
        "href": "https://v.aniu.tv/video/play/id/45952/t/2.shtml"
      },
      {
        "innerHTML": "短线客2020-03-16",
        "href": "https://v.aniu.tv/video/play/id/45953/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2020-03-13",
        "href": "https://v.aniu.tv/video/play/id/45849/t/2.shtml"
      },
      {
        "innerHTML": "择时有技2020-03-13",
        "href": "https://v.aniu.tv/video/play/id/45850/t/2.shtml"
      },
      {
        "innerHTML": "股谈老司机2020-03-13",
        "href": "https://v.aniu.tv/video/play/id/45851/t/2.shtml"
      },
      {
        "innerHTML": "短线客2020-03-13",
        "href": "https://v.aniu.tv/video/play/id/45852/t/2.shtml"
      },
      {
        "innerHTML": "薛松：外盘滑跌，深度回落时间有限，本周有望出现底部机会",
        "href": "https://v.aniu.tv/video/play/id/45787/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-03-12",
        "href": "https://v.aniu.tv/video/play/id/45808/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-03-12",
        "href": "https://v.aniu.tv/video/play/id/45809/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-03-12",
        "href": "https://v.aniu.tv/video/play/id/45810/t/2.shtml"
      },
      {
        "innerHTML": "薛松：赚钱最重要的是指数环境",
        "href": "https://v.aniu.tv/video/play/id/45769/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-03-11",
        "href": "https://v.aniu.tv/video/play/id/45798/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-03-10",
        "href": "https://v.aniu.tv/video/play/id/45791/t/2.shtml"
      },
      {
        "innerHTML": "股市有财2020-03-09",
        "href": "https://v.aniu.tv/video/play/id/45690/t/2.shtml"
      },
      {
        "innerHTML": "择时有技2020-03-09",
        "href": "https://v.aniu.tv/video/play/id/45691/t/2.shtml"
      },
      {
        "innerHTML": "老牛啃嫩股2020-03-08",
        "href": "https://v.aniu.tv/video/play/id/45640/t/2.shtml"
      },
      {
        "innerHTML": "股票一招先2020-03-08",
        "href": "https://v.aniu.tv/video/play/id/45641/t/2.shtml"
      },
      {
        "innerHTML": "解码公司2020-03-06",
        "href": "https://v.aniu.tv/video/play/id/45591/t/2.shtml"
      },
      {
        "innerHTML": "薛松：干货！短线投机的五大核心理念！",
        "href": "https://v.aniu.tv/video/play/id/45614/t/2.shtml"
      },
      {
        "innerHTML": "张津铭：多空双方都在等周末消息",
        "href": "https://v.aniu.tv/video/play/id/45604/t/2.shtml"
      },
      {
        "innerHTML": "张津铭：高送转炒作已经落下神坛",
        "href": "https://v.aniu.tv/video/play/id/45603/t/2.shtml"
      },
      {
        "innerHTML": "行业预测2020-03-06",
        "href": "https://v.aniu.tv/video/play/id/45592/t/2.shtml"
      },
      {
        "innerHTML": "首席投顾2020-03-06",
        "href": "https://v.aniu.tv/video/play/id/45593/t/2.shtml"
      },
      {
        "innerHTML": "薛松：特斯拉 很可能就是一个局",
        "href": "https://v.aniu.tv/video/play/id/45574/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-03-05",
        "href": "https://v.aniu.tv/video/play/id/45554/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-03-05",
        "href": "https://v.aniu.tv/video/play/id/45555/t/2.shtml"
      },
      {
        "innerHTML": "薛松：海龟交易系统 关键在执行纪律",
        "href": "https://v.aniu.tv/video/play/id/45573/t/2.shtml"
      },
      {
        "innerHTML": "林整华：行情修复关注21天线位置的买入点！",
        "href": "https://v.aniu.tv/video/play/id/45543/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2020-03-05",
        "href": "https://v.aniu.tv/video/play/id/45556/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-03-05",
        "href": "https://v.aniu.tv/video/play/id/45572/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：真正利好就两字 白酒体现得淋漓尽致",
        "href": "https://v.aniu.tv/video/play/id/66736/t/2.shtml"
      },
      {
        "innerHTML": "薛松：盘点ETF格局 含证券、科创",
        "href": "https://v.aniu.tv/video/play/id/66743/t/2.shtml"
      },
      {
        "innerHTML": "薛松：对于4000点我又有话要说",
        "href": "https://v.aniu.tv/video/play/id/66744/t/2.shtml"
      },
      {
        "innerHTML": "薛松：真正利好就两字，白酒体现得淋漓尽致",
        "href": "https://v.aniu.tv/video/play/id/66763/t/2.shtml"
      },
      {
        "innerHTML": "薛松：小富由俭，大富由天",
        "href": "https://v.aniu.tv/video/play/id/66231/t/2.shtml"
      },
      {
        "innerHTML": "薛松：如何应对流动性问题",
        "href": "https://v.aniu.tv/video/play/id/66126/t/2.shtml"
      },
      {
        "innerHTML": "薛松：比特币不是事儿 通过成交量判断机会",
        "href": "https://v.aniu.tv/video/play/id/65602/t/2.shtml"
      },
      {
        "innerHTML": "薛松：多做多错 知易行难！",
        "href": "https://v.aniu.tv/video/play/id/65603/t/2.shtml"
      },
      {
        "innerHTML": "薛松：何为有量市场？何为市场头部?",
        "href": "https://v.aniu.tv/video/play/id/65604/t/2.shtml"
      },
      {
        "innerHTML": "薛松：小富由俭 大富由天",
        "href": "https://v.aniu.tv/video/play/id/65607/t/2.shtml"
      },
      {
        "innerHTML": "薛松：真相居然被你发现了",
        "href": "https://v.aniu.tv/video/play/id/65608/t/2.shtml"
      },
      {
        "innerHTML": "薛松：一切皆有可能 认可南松老师的4136",
        "href": "https://v.aniu.tv/video/play/id/65609/t/2.shtml"
      },
      {
        "innerHTML": "薛松：行情到六一？来一探究竟！",
        "href": "https://v.aniu.tv/video/play/id/65487/t/2.shtml"
      },
      {
        "innerHTML": "薛松：非常期待此类事件出现！",
        "href": "https://v.aniu.tv/video/play/id/65488/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美国印钱引通胀！如何应对?",
        "href": "https://v.aniu.tv/video/play/id/65489/t/2.shtml"
      },
      {
        "innerHTML": "薛松：再谈大宗商品定价权",
        "href": "https://v.aniu.tv/video/play/id/65494/t/2.shtml"
      },
      {
        "innerHTML": "薛松：如何应对流动性问题？",
        "href": "https://v.aniu.tv/video/play/id/65532/t/2.shtml"
      },
      {
        "innerHTML": "薛松：调整时间基本到位 仍有人在割肉！",
        "href": "https://v.aniu.tv/video/play/id/64896/t/2.shtml"
      },
      {
        "innerHTML": "薛松：券商、军工板块见底？",
        "href": "https://v.aniu.tv/video/play/id/64897/t/2.shtml"
      },
      {
        "innerHTML": "薛松：坚信下方没有空间 可惜大家定力有限",
        "href": "https://v.aniu.tv/video/play/id/64898/t/2.shtml"
      },
      {
        "innerHTML": "薛松：穷要张狂富要稳，博弈求稳看个人",
        "href": "https://v.aniu.tv/video/play/id/64930/t/2.shtml"
      },
      {
        "innerHTML": "薛松：如何应对复杂市场",
        "href": "https://v.aniu.tv/video/play/id/64268/t/2.shtml"
      },
      {
        "innerHTML": "薛松：无量涨则抛 一手筹码一手现金",
        "href": "https://v.aniu.tv/video/play/id/64160/t/2.shtml"
      },
      {
        "innerHTML": "薛松：穷要张狂富要稳 博弈求稳看个人",
        "href": "https://v.aniu.tv/video/play/id/64162/t/2.shtml"
      },
      {
        "innerHTML": "薛松：股票投资重点并非业绩 而是这些数据",
        "href": "https://v.aniu.tv/video/play/id/64197/t/2.shtml"
      },
      {
        "innerHTML": "薛松：趋势买点的秘密",
        "href": "https://v.aniu.tv/video/play/id/64224/t/2.shtml"
      },
      {
        "innerHTML": "薛松：阅读重要事项的技巧",
        "href": "https://v.aniu.tv/video/play/id/64227/t/2.shtml"
      },
      {
        "innerHTML": "薛松：格局小了！中长线不应在乎一时波动",
        "href": "https://v.aniu.tv/video/play/id/64089/t/2.shtml"
      },
      {
        "innerHTML": "薛松：什么股票不会涨100倍？",
        "href": "https://v.aniu.tv/video/play/id/64149/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投机的要领",
        "href": "https://v.aniu.tv/video/play/id/64150/t/2.shtml"
      },
      {
        "innerHTML": "薛松：吃饭行情值得期待",
        "href": "https://v.aniu.tv/video/play/id/64151/t/2.shtml"
      },
      {
        "innerHTML": "薛松：抄底要与众不同",
        "href": "https://v.aniu.tv/video/play/id/63901/t/2.shtml"
      },
      {
        "innerHTML": "薛松：食之无味！预测周四收阴",
        "href": "https://v.aniu.tv/video/play/id/63735/t/2.shtml"
      },
      {
        "innerHTML": "薛松：千鹤老师“节奏大师” 论长阳后表现",
        "href": "https://v.aniu.tv/video/play/id/63736/t/2.shtml"
      },
      {
        "innerHTML": "薛松：电视人注孤生",
        "href": "https://v.aniu.tv/video/play/id/63796/t/2.shtml"
      },
      {
        "innerHTML": "薛松：核按钮的秘密",
        "href": "https://v.aniu.tv/video/play/id/63797/t/2.shtml"
      },
      {
        "innerHTML": "薛松：“配赚钱”才能市场长存",
        "href": "https://v.aniu.tv/video/play/id/63658/t/2.shtml"
      },
      {
        "innerHTML": "薛松：如何应对复杂市场？",
        "href": "https://v.aniu.tv/video/play/id/63659/t/2.shtml"
      },
      {
        "innerHTML": "薛松：周三应收阳线！",
        "href": "https://v.aniu.tv/video/play/id/63660/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：鸡肋行情 各凭本事",
        "href": "https://v.aniu.tv/video/play/id/73184/t/2.shtml"
      },
      {
        "innerHTML": "薛松：农产品缘何看好？要从甘蔗汁说起！",
        "href": "https://v.aniu.tv/video/play/id/73188/t/2.shtml"
      },
      {
        "innerHTML": "薛松：黄金需求缩减 印度最爱黄金！",
        "href": "https://v.aniu.tv/video/play/id/73191/t/2.shtml"
      },
      {
        "innerHTML": "薛松：别问PCB！输变电已强势！",
        "href": "https://v.aniu.tv/video/play/id/73233/t/2.shtml"
      },
      {
        "innerHTML": "薛松：盐湖股份前期投资者 应感谢自己的信仰！",
        "href": "https://v.aniu.tv/video/play/id/73239/t/2.shtml"
      },
      {
        "innerHTML": "薛松：时间仍未结束！空间已到位！",
        "href": "https://v.aniu.tv/video/play/id/73052/t/2.shtml"
      },
      {
        "innerHTML": "薛松：何为赛道？待在道上",
        "href": "https://v.aniu.tv/video/play/id/72967/t/2.shtml"
      },
      {
        "innerHTML": "薛松：跌时重质涨时重势 何时选银行",
        "href": "https://v.aniu.tv/video/play/id/72828/t/2.shtml"
      },
      {
        "innerHTML": "薛松：大阴线不会出现 最后买单的或是群众",
        "href": "https://v.aniu.tv/video/play/id/72484/t/2.shtml"
      },
      {
        "innerHTML": "薛松：散户无所畏惧 庄家痛失筹码",
        "href": "https://v.aniu.tv/video/play/id/72485/t/2.shtml"
      },
      {
        "innerHTML": "薛松：试问泡沫放过谁！",
        "href": "https://v.aniu.tv/video/play/id/72486/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投机要趁早",
        "href": "https://v.aniu.tv/video/play/id/72489/t/2.shtml"
      },
      {
        "innerHTML": "薛松：幸存者偏差揭秘事实",
        "href": "https://v.aniu.tv/video/play/id/72339/t/2.shtml"
      },
      {
        "innerHTML": "薛松：重点说三件事！最后一件尤为重要！",
        "href": "https://v.aniu.tv/video/play/id/72341/t/2.shtml"
      },
      {
        "innerHTML": "薛松：止损不宜幅度过小！论两种容易赔钱模式！",
        "href": "https://v.aniu.tv/video/play/id/72343/t/2.shtml"
      },
      {
        "innerHTML": "薛松：自检相关产品问世 题材相应爆发",
        "href": "https://v.aniu.tv/video/play/id/72363/t/2.shtml"
      },
      {
        "innerHTML": "薛松：封测并不看空 离乖离过远或将发生何类情形？",
        "href": "https://v.aniu.tv/video/play/id/72404/t/2.shtml"
      },
      {
        "innerHTML": "薛松：中信证券并非空头",
        "href": "https://v.aniu.tv/video/play/id/72065/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这家机构为股市输送了大量人才！",
        "href": "https://v.aniu.tv/video/play/id/72066/t/2.shtml"
      },
      {
        "innerHTML": "薛松：为自己的股票找利好是非理性的！",
        "href": "https://v.aniu.tv/video/play/id/72067/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你不知道的机构估值真相",
        "href": "https://v.aniu.tv/video/play/id/72068/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不看好医疗 就怕报表“见公婆”",
        "href": "https://v.aniu.tv/video/play/id/72095/t/2.shtml"
      },
      {
        "innerHTML": "薛松：高手盈利博弈 低手成本博弈",
        "href": "https://v.aniu.tv/video/play/id/72096/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你不挣钱就是熊市！",
        "href": "https://v.aniu.tv/video/play/id/72098/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不怕错，只怕拖",
        "href": "https://v.aniu.tv/video/play/id/72024/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你幸福吗？",
        "href": "https://v.aniu.tv/video/play/id/72030/t/2.shtml"
      },
      {
        "innerHTML": "薛松：做好提前量 后期如何演绎？",
        "href": "https://v.aniu.tv/video/play/id/71979/t/2.shtml"
      },
      {
        "innerHTML": "薛松：股价是预期，预期改变则改变",
        "href": "https://v.aniu.tv/video/play/id/71982/t/2.shtml"
      },
      {
        "innerHTML": "薛松：耐点心，不然赚不到钱",
        "href": "https://v.aniu.tv/video/play/id/72008/t/2.shtml"
      },
      {
        "innerHTML": "薛松：上证50打对折？你想的真美",
        "href": "https://v.aniu.tv/video/play/id/72011/t/2.shtml"
      },
      {
        "innerHTML": "薛松：试问敢不敢看多？",
        "href": "https://v.aniu.tv/video/play/id/71797/t/2.shtml"
      },
      {
        "innerHTML": "薛松：美国市场的A股相关产品早已反映走势！",
        "href": "https://v.aniu.tv/video/play/id/71798/t/2.shtml"
      },
      {
        "innerHTML": "薛松：众人无惧的反弹点不是机会！",
        "href": "https://v.aniu.tv/video/play/id/71799/t/2.shtml"
      },
      {
        "innerHTML": "薛松：皆是低位起 论当下操作关键点",
        "href": "https://v.aniu.tv/video/play/id/71800/t/2.shtml"
      },
      {
        "innerHTML": "薛松：假如时光倒流 他们可能不会重走老路",
        "href": "https://v.aniu.tv/video/play/id/71813/t/2.shtml"
      },
      {
        "innerHTML": "薛松：政策的意义 如何用好美股市场工具？",
        "href": "https://v.aniu.tv/video/play/id/71863/t/2.shtml"
      },
      {
        "innerHTML": "薛松：有泡沫不一定破 有干货不一定涨",
        "href": "https://v.aniu.tv/video/play/id/71864/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不着则已一着惊人",
        "href": "https://v.aniu.tv/video/play/id/71865/t/2.shtml"
      },
      {
        "innerHTML": "薛松：创业板处于泡沫期 而非消退期",
        "href": "https://v.aniu.tv/video/play/id/71867/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：还能期待后市有强势行情吗？",
        "href": "https://v.aniu.tv/video/play/id/6638/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-10-20",
        "href": "https://v.aniu.tv/video/play/id/6644/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-10-20",
        "href": "https://v.aniu.tv/video/play/id/6646/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-10-20",
        "href": "https://v.aniu.tv/video/play/id/6647/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-10-20",
        "href": "https://v.aniu.tv/video/play/id/6658/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-10-19",
        "href": "https://v.aniu.tv/video/play/id/6624/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-10-18",
        "href": "https://v.aniu.tv/video/play/id/6589/t/2.shtml"
      },
      {
        "innerHTML": "薛松：市场怎么又开始了“横”了？",
        "href": "https://v.aniu.tv/video/play/id/6446/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-10-13",
        "href": "https://v.aniu.tv/video/play/id/6437/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-10-13",
        "href": "https://v.aniu.tv/video/play/id/6438/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-10-13",
        "href": "https://v.aniu.tv/video/play/id/6439/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-10-13",
        "href": "https://v.aniu.tv/video/play/id/6465/t/2.shtml"
      },
      {
        "innerHTML": "薛松：当前只能把目光放短",
        "href": "https://v.aniu.tv/video/play/id/6379/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-10-11",
        "href": "https://v.aniu.tv/video/play/id/6385/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-09-29",
        "href": "https://v.aniu.tv/video/play/id/6239/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-09-29",
        "href": "https://v.aniu.tv/video/play/id/6238/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-09-29",
        "href": "https://v.aniu.tv/video/play/id/6237/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投资过程中最重要的是什么？",
        "href": "https://v.aniu.tv/video/play/id/6242/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-09-28",
        "href": "https://v.aniu.tv/video/play/id/6228/t/2.shtml"
      },
      {
        "innerHTML": "布林线下轨反弹已过",
        "href": "https://v.aniu.tv/video/play/id/6214/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-09-27",
        "href": "https://v.aniu.tv/video/play/id/6218/t/2.shtml"
      },
      {
        "innerHTML": "今天反弹的意义是什么",
        "href": "https://v.aniu.tv/video/play/id/6179/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-09-22",
        "href": "https://v.aniu.tv/video/play/id/6071/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-09-22",
        "href": "https://v.aniu.tv/video/play/id/6072/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-09-22",
        "href": "https://v.aniu.tv/video/play/id/6073/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-09-22",
        "href": "https://v.aniu.tv/video/play/id/6080/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-09-21",
        "href": "https://v.aniu.tv/video/play/id/6053/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-09-20",
        "href": "https://v.aniu.tv/video/play/id/6036/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-09-14",
        "href": "https://v.aniu.tv/video/play/id/6002/t/2.shtml"
      },
      {
        "innerHTML": "昨夜华尔街2016-09-13",
        "href": "https://v.aniu.tv/video/play/id/5933/t/2.shtml"
      },
      {
        "innerHTML": "股往金来2016-09-13",
        "href": "https://v.aniu.tv/video/play/id/5934/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-09-13",
        "href": "https://v.aniu.tv/video/play/id/5956/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-09-08",
        "href": "https://v.aniu.tv/video/play/id/5871/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2016-09-08",
        "href": "https://v.aniu.tv/video/play/id/5872/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2016-09-08",
        "href": "https://v.aniu.tv/video/play/id/5873/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-09-08",
        "href": "https://v.aniu.tv/video/play/id/5883/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-09-07",
        "href": "https://v.aniu.tv/video/play/id/5860/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2016-09-06",
        "href": "https://v.aniu.tv/video/play/id/5844/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2016-09-01",
        "href": "https://v.aniu.tv/video/play/id/5759/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "热点公告2020-10-14",
        "href": "https://v.aniu.tv/video/play/id/53223/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-10-13",
        "href": "https://v.aniu.tv/video/play/id/53169/t/2.shtml"
      },
      {
        "innerHTML": "薛松：另外找其他大黑马吧",
        "href": "https://v.aniu.tv/video/play/id/53059/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-09-29",
        "href": "https://v.aniu.tv/video/play/id/52997/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-09-23",
        "href": "https://v.aniu.tv/video/play/id/52773/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-09-22",
        "href": "https://v.aniu.tv/video/play/id/52725/t/2.shtml"
      },
      {
        "innerHTML": "薛松：错sha 也是sha",
        "href": "https://v.aniu.tv/video/play/id/52618/t/2.shtml"
      },
      {
        "innerHTML": "薛松：凭什么 不接",
        "href": "https://v.aniu.tv/video/play/id/52491/t/2.shtml"
      },
      {
        "innerHTML": "薛松：蓝筹的未来",
        "href": "https://v.aniu.tv/video/play/id/52413/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-09-16",
        "href": "https://v.aniu.tv/video/play/id/52442/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-09-15",
        "href": "https://v.aniu.tv/video/play/id/52370/t/2.shtml"
      },
      {
        "innerHTML": "薛松：图表 债券 能预测我倒影吗",
        "href": "https://v.aniu.tv/video/play/id/52228/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-09-10",
        "href": "https://v.aniu.tv/video/play/id/52211/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-09-10",
        "href": "https://v.aniu.tv/video/play/id/52212/t/2.shtml"
      },
      {
        "innerHTML": "薛松：下半年操作",
        "href": "https://v.aniu.tv/video/play/id/52200/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-09-09",
        "href": "https://v.aniu.tv/video/play/id/52187/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你别吓我",
        "href": "https://v.aniu.tv/video/play/id/52170/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-09-08",
        "href": "https://v.aniu.tv/video/play/id/52154/t/2.shtml"
      },
      {
        "innerHTML": "薛松：21年前 一样的-故事",
        "href": "https://v.aniu.tv/video/play/id/52113/t/2.shtml"
      },
      {
        "innerHTML": "薛松-如果这5天还没挣上钱",
        "href": "https://v.aniu.tv/video/play/id/52076/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-09-02",
        "href": "https://v.aniu.tv/video/play/id/51964/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-09-01",
        "href": "https://v.aniu.tv/video/play/id/51911/t/2.shtml"
      },
      {
        "innerHTML": "交易封神榜2020-08-30",
        "href": "https://v.aniu.tv/video/play/id/51915/t/2.shtml"
      },
      {
        "innerHTML": "下周透视2020-08-30",
        "href": "https://v.aniu.tv/video/play/id/51916/t/2.shtml"
      },
      {
        "innerHTML": "强势追踪2020-08-30",
        "href": "https://v.aniu.tv/video/play/id/51917/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你有本事 请别作",
        "href": "https://v.aniu.tv/video/play/id/51764/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-08-27",
        "href": "https://v.aniu.tv/video/play/id/51703/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-08-27",
        "href": "https://v.aniu.tv/video/play/id/51704/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-08-25",
        "href": "https://v.aniu.tv/video/play/id/51649/t/2.shtml"
      },
      {
        "innerHTML": "薛松：黄金未来如此确定  只做一天你就错了",
        "href": "https://v.aniu.tv/video/play/id/51632/t/2.shtml"
      },
      {
        "innerHTML": "好的回踩，正常的回踩",
        "href": "https://v.aniu.tv/video/play/id/51547/t/2.shtml"
      },
      {
        "innerHTML": "薛说：三天一回踩，但是不能老踩",
        "href": "https://v.aniu.tv/video/play/id/51546/t/2.shtml"
      },
      {
        "innerHTML": "薛松：等凉了",
        "href": "https://v.aniu.tv/video/play/id/51545/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2020-08-13",
        "href": "https://v.aniu.tv/video/play/id/51282/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2020-08-13",
        "href": "https://v.aniu.tv/video/play/id/51283/t/2.shtml"
      },
      {
        "innerHTML": "薛松：走不走 你要何时走！",
        "href": "https://v.aniu.tv/video/play/id/51265/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-08-12",
        "href": "https://v.aniu.tv/video/play/id/51245/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-08-11",
        "href": "https://v.aniu.tv/video/play/id/51200/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2020-08-05",
        "href": "https://v.aniu.tv/video/play/id/50985/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "股市诊疗2019-08-29",
        "href": "https://v.aniu.tv/video/play/id/39691/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-08-29",
        "href": "https://v.aniu.tv/video/play/id/39692/t/2.shtml"
      },
      {
        "innerHTML": "薛松：主升浪如何判断？",
        "href": "https://v.aniu.tv/video/play/id/39686/t/2.shtml"
      },
      {
        "innerHTML": "严明阳：九月三大重点板块！",
        "href": "https://v.aniu.tv/video/play/id/39687/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-08-28",
        "href": "https://v.aniu.tv/video/play/id/39688/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-08-27",
        "href": "https://v.aniu.tv/video/play/id/39646/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-08-21",
        "href": "https://v.aniu.tv/video/play/id/39436/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-08-20",
        "href": "https://v.aniu.tv/video/play/id/39392/t/2.shtml"
      },
      {
        "innerHTML": "薛松：抄底三部曲",
        "href": "https://v.aniu.tv/video/play/id/39235/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-08-15",
        "href": "https://v.aniu.tv/video/play/id/39177/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-08-15",
        "href": "https://v.aniu.tv/video/play/id/39178/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-08-15",
        "href": "https://v.aniu.tv/video/play/id/39179/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-08-14",
        "href": "https://v.aniu.tv/video/play/id/39182/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-08-13",
        "href": "https://v.aniu.tv/video/play/id/39117/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-08-07",
        "href": "https://v.aniu.tv/video/play/id/38914/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-08-06",
        "href": "https://v.aniu.tv/video/play/id/38876/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-08-01",
        "href": "https://v.aniu.tv/video/play/id/38666/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-08-01",
        "href": "https://v.aniu.tv/video/play/id/38667/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-08-01",
        "href": "https://v.aniu.tv/video/play/id/38668/t/2.shtml"
      },
      {
        "innerHTML": "薛松：如何看资金流出判断是否会继续下跌",
        "href": "https://v.aniu.tv/video/play/id/38659/t/2.shtml"
      },
      {
        "innerHTML": "林整华：创业板有压力，量能不足",
        "href": "https://v.aniu.tv/video/play/id/38652/t/2.shtml"
      },
      {
        "innerHTML": "薛松：新三驾反转怎么判别？",
        "href": "https://v.aniu.tv/video/play/id/38651/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-07-31",
        "href": "https://v.aniu.tv/video/play/id/38671/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-07-30",
        "href": "https://v.aniu.tv/video/play/id/38672/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-07-25",
        "href": "https://v.aniu.tv/video/play/id/38458/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-07-25",
        "href": "https://v.aniu.tv/video/play/id/38459/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-07-25",
        "href": "https://v.aniu.tv/video/play/id/38460/t/2.shtml"
      },
      {
        "innerHTML": "薛松：这些指标的实际含义！",
        "href": "https://v.aniu.tv/video/play/id/38444/t/2.shtml"
      },
      {
        "innerHTML": "薛松：大票何时表现强？",
        "href": "https://v.aniu.tv/video/play/id/38442/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-07-25",
        "href": "https://v.aniu.tv/video/play/id/38449/t/2.shtml"
      },
      {
        "innerHTML": "画说基本面2019-07-24",
        "href": "https://v.aniu.tv/video/play/id/38398/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-07-24",
        "href": "https://v.aniu.tv/video/play/id/38435/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-07-23",
        "href": "https://v.aniu.tv/video/play/id/38406/t/2.shtml"
      },
      {
        "innerHTML": "量化热点股2019-07-18",
        "href": "https://v.aniu.tv/video/play/id/38252/t/2.shtml"
      },
      {
        "innerHTML": "股市诊疗2019-07-18",
        "href": "https://v.aniu.tv/video/play/id/38253/t/2.shtml"
      },
      {
        "innerHTML": "财富广角2019-07-18",
        "href": "https://v.aniu.tv/video/play/id/38254/t/2.shtml"
      },
      {
        "innerHTML": "吴道鹏：大盘呈现中继底分型建议观望",
        "href": "https://v.aniu.tv/video/play/id/38241/t/2.shtml"
      },
      {
        "innerHTML": "吴道鹏：如何用中枢理论判断行情未来走势？",
        "href": "https://v.aniu.tv/video/play/id/38239/t/2.shtml"
      },
      {
        "innerHTML": "热点公告2019-07-17",
        "href": "https://v.aniu.tv/video/play/id/38243/t/2.shtml"
      }
    ],
    [
      {
        "innerHTML": "薛松：以静制动 动则难逃苦网！",
        "href": "https://v.aniu.tv/video/play/id/69720/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不食最后一口的智慧",
        "href": "https://v.aniu.tv/video/play/id/69721/t/2.shtml"
      },
      {
        "innerHTML": "薛松：分歧是机会 一致是风险",
        "href": "https://v.aniu.tv/video/play/id/69723/t/2.shtml"
      },
      {
        "innerHTML": "薛松：小目标之路！我表示充分理解！",
        "href": "https://v.aniu.tv/video/play/id/69724/t/2.shtml"
      },
      {
        "innerHTML": "薛松：动便是有情绪！人往往不愿意接受现实！",
        "href": "https://v.aniu.tv/video/play/id/69740/t/2.shtml"
      },
      {
        "innerHTML": "薛松：古今中外事迹论价值投资真谛！",
        "href": "https://v.aniu.tv/video/play/id/69758/t/2.shtml"
      },
      {
        "innerHTML": "薛松：投资市场的目标与偶像",
        "href": "https://v.aniu.tv/video/play/id/69512/t/2.shtml"
      },
      {
        "innerHTML": "薛松：给年轻小伙的有效建议",
        "href": "https://v.aniu.tv/video/play/id/69164/t/2.shtml"
      },
      {
        "innerHTML": "薛松：第一批片仔癀交易者给人的启示",
        "href": "https://v.aniu.tv/video/play/id/69165/t/2.shtml"
      },
      {
        "innerHTML": "薛松：太过细化则历史统计片面化",
        "href": "https://v.aniu.tv/video/play/id/69166/t/2.shtml"
      },
      {
        "innerHTML": "薛松：冒险不是事儿",
        "href": "https://v.aniu.tv/video/play/id/69168/t/2.shtml"
      },
      {
        "innerHTML": "薛松：十倍梦想应从何说起",
        "href": "https://v.aniu.tv/video/play/id/69169/t/2.shtml"
      },
      {
        "innerHTML": "薛松：王亚伟的重组时代",
        "href": "https://v.aniu.tv/video/play/id/69178/t/2.shtml"
      },
      {
        "innerHTML": "薛松：万物归零理犹在，待看前世今生",
        "href": "https://v.aniu.tv/video/play/id/69092/t/2.shtml"
      },
      {
        "innerHTML": "薛松：人很奇怪，一定要找背锅的",
        "href": "https://v.aniu.tv/video/play/id/69105/t/2.shtml"
      },
      {
        "innerHTML": "薛松：当你足够分散，就可无畏洗盘",
        "href": "https://v.aniu.tv/video/play/id/69110/t/2.shtml"
      },
      {
        "innerHTML": "薛松：量价时空有规律",
        "href": "https://v.aniu.tv/video/play/id/69113/t/2.shtml"
      },
      {
        "innerHTML": "薛松：经历过风雨才能成长",
        "href": "https://v.aniu.tv/video/play/id/69122/t/2.shtml"
      },
      {
        "innerHTML": "薛松：你想要利息 他想要你本金！",
        "href": "https://v.aniu.tv/video/play/id/69126/t/2.shtml"
      },
      {
        "innerHTML": "薛松：A股研报哪家强？",
        "href": "https://v.aniu.tv/video/play/id/69127/t/2.shtml"
      },
      {
        "innerHTML": "薛松：所谓冠军 实则坏得很！",
        "href": "https://v.aniu.tv/video/play/id/69129/t/2.shtml"
      },
      {
        "innerHTML": "薛松：好基金不进前三甲 宽基可能更靠谱",
        "href": "https://v.aniu.tv/video/play/id/69131/t/2.shtml"
      },
      {
        "innerHTML": "薛松：上涨未放量 阳线无意义",
        "href": "https://v.aniu.tv/video/play/id/69133/t/2.shtml"
      },
      {
        "innerHTML": "薛松：三个指数 大不相同",
        "href": "https://v.aniu.tv/video/play/id/69136/t/2.shtml"
      },
      {
        "innerHTML": "薛松：老板泪目！地产行业竟成“背锅侠”！",
        "href": "https://v.aniu.tv/video/play/id/69143/t/2.shtml"
      },
      {
        "innerHTML": "薛松：不愿陪玩 包输不包赢",
        "href": "https://v.aniu.tv/video/play/id/69145/t/2.shtml"
      },
      {
        "innerHTML": "薛松：格局小的人是挣不到钱的！",
        "href": "https://v.aniu.tv/video/play/id/69146/t/2.shtml"
      },
      {
        "innerHTML": "薛松：给自己的相信设个“度” 拒绝消磨时间与金钱",
        "href": "https://v.aniu.tv/video/play/id/69147/t/2.shtml"
      },
      {
        "innerHTML": "薛松：现在牛的 都是以前跌很惨的",
        "href": "https://v.aniu.tv/video/play/id/69156/t/2.shtml"
      },
      {
        "innerHTML": "薛松：细化则废 框好则立",
        "href": "https://v.aniu.tv/video/play/id/69026/t/2.shtml"
      },
      {
        "innerHTML": "薛松：牛市往往是加价买的",
        "href": "https://v.aniu.tv/video/play/id/69028/t/2.shtml"
      },
      {
        "innerHTML": "薛松：花间悟道 改变自己！",
        "href": "https://v.aniu.tv/video/play/id/69030/t/2.shtml"
      },
      {
        "innerHTML": "薛松：猫吃鱼狗吃肉 奥特曼打小怪兽",
        "href": "https://v.aniu.tv/video/play/id/69031/t/2.shtml"
      },
      {
        "innerHTML": "薛松：关键是如何在优势中下注",
        "href": "https://v.aniu.tv/video/play/id/69034/t/2.shtml"
      },
      {
        "innerHTML": "薛松：再话炒筷子原理！",
        "href": "https://v.aniu.tv/video/play/id/69036/t/2.shtml"
      },
      {
        "innerHTML": "薛松：过分关注指数涨跌 反而是负担",
        "href": "https://v.aniu.tv/video/play/id/69038/t/2.shtml"
      },
      {
        "innerHTML": "薛松：赢了指数 却输了全世界",
        "href": "https://v.aniu.tv/video/play/id/69039/t/2.shtml"
      },
      {
        "innerHTML": "薛松：创业板指绝对强势 上证指数探低回升",
        "href": "https://v.aniu.tv/video/play/id/69047/t/2.shtml"
      },
      {
        "innerHTML": "薛松：知道大盘要调 还会加仓吗？",
        "href": "https://v.aniu.tv/video/play/id/69048/t/2.shtml"
      }
    ]
  ],
  "分页最大数": 57,
  "缓冲时间(秒)": 10
}

function md5(inputString) {
  var hc = "0123456789abcdef";
  function rh(n) { var j, s = ""; for (j = 0; j <= 3; j++) s += hc.charAt((n >> (j * 8 + 4)) & 0x0F) + hc.charAt((n >> (j * 8)) & 0x0F); return s; }
  function ad(x, y) { var l = (x & 0xFFFF) + (y & 0xFFFF); var m = (x >> 16) + (y >> 16) + (l >> 16); return (m << 16) | (l & 0xFFFF); }
  function rl(n, c) { return (n << c) | (n >>> (32 - c)); }
  function cm(q, a, b, x, s, t) { return ad(rl(ad(ad(a, q), ad(x, t)), s), b); }
  function ff(a, b, c, d, x, s, t) { return cm((b & c) | ((~b) & d), a, b, x, s, t); }
  function gg(a, b, c, d, x, s, t) { return cm((b & d) | (c & (~d)), a, b, x, s, t); }
  function hh(a, b, c, d, x, s, t) { return cm(b ^ c ^ d, a, b, x, s, t); }
  function ii(a, b, c, d, x, s, t) { return cm(c ^ (b | (~d)), a, b, x, s, t); }
  function sb(x) {
    var i; var nblk = ((x.length + 8) >> 6) + 1; var blks = new Array(nblk * 16); for (i = 0; i < nblk * 16; i++) blks[i] = 0;
    for (i = 0; i < x.length; i++) blks[i >> 2] |= x.charCodeAt(i) << ((i % 4) * 8);
    blks[i >> 2] |= 0x80 << ((i % 4) * 8); blks[nblk * 16 - 2] = x.length * 8; return blks;
  }
  var i, x = sb(inputString), a = 1732584193, b = -271733879, c = -1732584194, d = 271733878, olda, oldb, oldc, oldd;
  for (i = 0; i < x.length; i += 16) {
    olda = a; oldb = b; oldc = c; oldd = d;
    a = ff(a, b, c, d, x[i + 0], 7, -680876936); d = ff(d, a, b, c, x[i + 1], 12, -389564586); c = ff(c, d, a, b, x[i + 2], 17, 606105819);
    b = ff(b, c, d, a, x[i + 3], 22, -1044525330); a = ff(a, b, c, d, x[i + 4], 7, -176418897); d = ff(d, a, b, c, x[i + 5], 12, 1200080426);
    c = ff(c, d, a, b, x[i + 6], 17, -1473231341); b = ff(b, c, d, a, x[i + 7], 22, -45705983); a = ff(a, b, c, d, x[i + 8], 7, 1770035416);
    d = ff(d, a, b, c, x[i + 9], 12, -1958414417); c = ff(c, d, a, b, x[i + 10], 17, -42063); b = ff(b, c, d, a, x[i + 11], 22, -1990404162);
    a = ff(a, b, c, d, x[i + 12], 7, 1804603682); d = ff(d, a, b, c, x[i + 13], 12, -40341101); c = ff(c, d, a, b, x[i + 14], 17, -1502002290);
    b = ff(b, c, d, a, x[i + 15], 22, 1236535329); a = gg(a, b, c, d, x[i + 1], 5, -165796510); d = gg(d, a, b, c, x[i + 6], 9, -1069501632);
    c = gg(c, d, a, b, x[i + 11], 14, 643717713); b = gg(b, c, d, a, x[i + 0], 20, -373897302); a = gg(a, b, c, d, x[i + 5], 5, -701558691);
    d = gg(d, a, b, c, x[i + 10], 9, 38016083); c = gg(c, d, a, b, x[i + 15], 14, -660478335); b = gg(b, c, d, a, x[i + 4], 20, -405537848);
    a = gg(a, b, c, d, x[i + 9], 5, 568446438); d = gg(d, a, b, c, x[i + 14], 9, -1019803690); c = gg(c, d, a, b, x[i + 3], 14, -187363961);
    b = gg(b, c, d, a, x[i + 8], 20, 1163531501); a = gg(a, b, c, d, x[i + 13], 5, -1444681467); d = gg(d, a, b, c, x[i + 2], 9, -51403784);
    c = gg(c, d, a, b, x[i + 7], 14, 1735328473); b = gg(b, c, d, a, x[i + 12], 20, -1926607734); a = hh(a, b, c, d, x[i + 5], 4, -378558);
    d = hh(d, a, b, c, x[i + 8], 11, -2022574463); c = hh(c, d, a, b, x[i + 11], 16, 1839030562); b = hh(b, c, d, a, x[i + 14], 23, -35309556);
    a = hh(a, b, c, d, x[i + 1], 4, -1530992060); d = hh(d, a, b, c, x[i + 4], 11, 1272893353); c = hh(c, d, a, b, x[i + 7], 16, -155497632);
    b = hh(b, c, d, a, x[i + 10], 23, -1094730640); a = hh(a, b, c, d, x[i + 13], 4, 681279174); d = hh(d, a, b, c, x[i + 0], 11, -358537222);
    c = hh(c, d, a, b, x[i + 3], 16, -722521979); b = hh(b, c, d, a, x[i + 6], 23, 76029189); a = hh(a, b, c, d, x[i + 9], 4, -640364487);
    d = hh(d, a, b, c, x[i + 12], 11, -421815835); c = hh(c, d, a, b, x[i + 15], 16, 530742520); b = hh(b, c, d, a, x[i + 2], 23, -995338651);
    a = ii(a, b, c, d, x[i + 0], 6, -198630844); d = ii(d, a, b, c, x[i + 7], 10, 1126891415); c = ii(c, d, a, b, x[i + 14], 15, -1416354905);
    b = ii(b, c, d, a, x[i + 5], 21, -57434055); a = ii(a, b, c, d, x[i + 12], 6, 1700485571); d = ii(d, a, b, c, x[i + 3], 10, -1894986606);
    c = ii(c, d, a, b, x[i + 10], 15, -1051523); b = ii(b, c, d, a, x[i + 1], 21, -2054922799); a = ii(a, b, c, d, x[i + 8], 6, 1873313359);
    d = ii(d, a, b, c, x[i + 15], 10, -30611744); c = ii(c, d, a, b, x[i + 6], 15, -1560198380); b = ii(b, c, d, a, x[i + 13], 21, 1309151649);
    a = ii(a, b, c, d, x[i + 4], 6, -145523070); d = ii(d, a, b, c, x[i + 11], 10, -1120210379); c = ii(c, d, a, b, x[i + 2], 15, 718787259);
    b = ii(b, c, d, a, x[i + 9], 21, -343485551); a = ad(a, olda); b = ad(b, oldb); c = ad(c, oldc); d = ad(d, oldd);
  }
  return rh(a) + rh(b) + rh(c) + rh(d);
}

class Show {

  constructor(params = {}) {
    let defaultValue = {
      页面链接: undefined,
      结果集: undefined,
      目标对象: undefined,
    }

    params = Object.assign(defaultValue, params)
    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }
  }

  get() {
    if (this.结果集['视频链接']) return new Promise((resolve, reject) => {
      return resolve([true, this])
    });
    return this.get_document().then(function (res) {
      if (!res[0]) return res
      let tag_name = 'tag' + md5(this.页面链接)
      let template = document.createElement(tag_name)
      template.innerHTML = res[1]
      document.body.appendChild(template)
      this.目标对象 = template
    }.bind(this))
      .then(function () {
        return this.get_result()
      }.bind(this))
  }

  get_document() {
    return fetch(this.页面链接, {
      "headers": {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
        "cache-control": "max-age=0",
        "sec-ch-ua": "\"Chromium\";v=\"104\", \" Not A;Brand\";v=\"99\", \"Google Chrome\";v=\"104\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"macOS\"",
        "sec-fetch-dest": "document",
        "sec-fetch-mode": "navigate",
        "sec-fetch-site": "none",
        "sec-fetch-user": "?1",
        "upgrade-insecure-requests": "1",
        "Cookie": document.cookie
      },
      "referrerPolicy": "strict-origin-when-cross-origin",
      "body": null,
      "method": "GET",
      "mode": "cors",
      "credentials": "include"
    }).then(response => response.text())
      .then(result => {
        return [true, result]
      })
      .catch(error => { return [false, error] });
  }

  get_result() {
    if (!this.目标对象) return new Promise((resolve, reject) => {
      return resolve([false, '目标对象为空',this])
    });

    try {
      if (this.结果集['视频链接']) return this.结果集

      let childrens = this.目标对象.getElementsByClassName('videoPlate')[0].children
      this.结果集['视频链接'] = childrens[0].getAttribute('data-src')
      if (!this.结果集['视频链接']) this.结果集['视频链接'] = childrens[1].getAttribute('data-src')
  
      if (!this.结果集['视频链接']) debugger
      return this.结果集      
    } catch (error) {
      debugger
    }

  }

}

// let show = new Show({页面链接: 'https://v.aniu.tv/video/play/id/102028/t/2.shtml'})
// show.get()

// index_result = {
//   "页面链接": "https://zjt.aniu.tv/experts_index_type_4_eid_288656_p_1.shtml",
//   "结果集":
//     [[
//       {
//         "innerHTML": "薛松：切勿被情绪左右！被他人操控！",
//         "href": "https://v.aniu.tv/video/play/id/102027/t/2.shtml"
//       },
//       {
//         "innerHTML": "薛松：基建方向会是长久之计吗？",
//         "href": "https://v.aniu.tv/video/play/id/102028/t/2.shtml"
//       },
//     ]]
// }

let length = index_result['结果集'].flat().length
let current_index = 0

for (object of index_result['结果集'].flat()){
  if (object.视频链接) continue
  if (!object.innerHTML.match(/.*薛松.*/)) continue
  
  let sleep = (index, object) => {
    setTimeout(function () {
      let show = new Show({ 页面链接: object.href, 结果集: object })
      show.get().then((res)=>{
        console.log(JSON.stringify({ 结果: show, 时间: (new Date().toLocaleString()), 进度: index + '/' + length }, null, 2))
      })
    }.bind(object), index * 2 * 10000)
  }

  sleep(current_index += 1, object);
}
