// 该代码需在列表页面的console里面执行
class List {

  constructor(params = {}) {
    let defaultValue = {
      页面链接: undefined,
      结果集: undefined,
      专辑对象: undefined,
    }

    params = Object.assign(defaultValue, params)
    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }
  }

  get() {
    return this.get_document().then(function (res) {
      if (!res[0]) return res
      let template = document.createElement('space')
      let id = Math.random()

      template.innerHTML = res[1].replace(/zhuanjilink/g, id)
      document.body.appendChild(template)
      this.专辑对象 = document.getElementById(id)
    }.bind(this))
      .then(function () {
        return this.get_result()
      }.bind(this))
  }

  get_document() {
    return fetch(this.页面链接, { method: 'GET', redirect: 'follow' })
      .then(response => response.text())
      .then(result => {
        return [true, result]
      })
      .catch(error => { return [false, error] });
  }

  get_result() {
    if (this.结果集) return this.结果集
    this.结果集 = []

    let lis = this.专辑对象.children

    for (let li of lis) {
      let a_video = li.children[1]
      let json = JSON.parse(JSON.stringify(a_video, ["innerHTML", "href"]))
      this.结果集.push(json)
    }

    return this.结果集
  }

}


class Result {

  constructor(params = {}) {
    let defaultValue = {
      页面链接: undefined,
      结果集: undefined,
      分页最大数: undefined,
      '缓冲时间(秒)': 20,
    }

    params = Object.assign(defaultValue, params)
    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }
  }

  get() {
    return this.get_maxPages().then(function (res) {
      if (!res[0]) return res
    }.bind(this)).then(function () {
      this.get_pages()
    }.bind(this))
  }

  get_maxPages() {
    if (this.分页最大数) return new Promise((resolve, reject) => {
      return resolve([true, ''])
    });
    return fetch(this.页面链接, { method: 'GET', redirect: 'follow' })
      .then(response => response.text())
      .then(result => {
        let template = document.createElement('space')
        template.innerHTML = result
        let div_page = template.getElementsByClassName('paging')[0]
        this.分页最大数 = parseInt(div_page.lastChild.href.replace(/.*_p_(\d{1,}).shtml/, "$1"))
        return [true, '']
      })
      .catch(error => { return [false, error] });
  }

  get_pages() {
    if (this.结果集) return [true, this.结果集]

    this.结果集 = []
    for (let index of [...Array(this.分页最大数).keys()]) {
      index += 1

      setTimeout(function () {
        let url = this.页面链接.replace(/_p_\d{1,}/, '_p_' + index.toString())
        let list = new List({ 页面链接: url })
        list.get().then(function (res) {
          console.log(JSON.stringify({ 结果: res, 时间: (new Date().toLocaleString()), 进度: index + '/' + this.分页最大数 }, null, 2))
          this.结果集.push(res)
        }.bind(this))
      }.bind(this), this['缓冲时间(秒)'] * 1000)

    }

    return [true, this.结果集]
  }

}

// let result = new Result({ 页面链接: "https://zjt.aniu.tv/experts_index_type_4_eid_288656_p_1.shtml", 分页最大数: 2 })
let result = new Result({ 页面链接: "https://zjt.aniu.tv/experts_index_type_4_eid_288656_p_1.shtml" })
result.get()
result.结果集