require 'pry'

module Finance
  extend self

  $market_datas = [
    {market_name: '', code: '', datas:[
      {date: '2020-04-03', price: 0.8899},
      {date: '2020-04-02', price: 0.8920},
      {date: '2020-04-01', price: 0.8806},
      {date: '2020-03-31', price: 0.8866},
      {date: '2020-03-30', price: 0.8703},
      {date: '2020-03-27', price: 0.8871},
      {date: '2020-03-26', price: 0.8670},
      {date: '2020-03-25', price: 0.8678},
      {date: '2020-03-24', price: 0.8433},
      {date: '2020-03-23', price: 0.8126},
      {date: '2020-03-20', price: 0.8375},
      {date: '2020-03-19', price: 0.8110},
      {date: '2020-03-18', price: 0.8394},
      {date: '2020-03-17', price: 0.8623},
      {date: '2020-03-16', price: 0.8808},
      {date: '2020-03-13', price: 0.9177},
      {date: '2020-03-12', price: 0.9348},
      {date: '2020-03-11', price: 0.9573},
      {date: '2020-03-10', price: 0.9597},
      {date: '2020-03-09', price: 0.9331},
      {date: '2020-03-06', price: 0.9694},
      {date: '2020-03-05', price: 0.9920}
      ]
    }
  ]

  $hold_datas = [
    {
      scope_name: '', datas: [
        {
          code: '', datas: [
            {date: '', num: 10000, price: 0.900, type: :init},
            {date: '2020-03-16', num: 10000},
            {date: '2020-03-24', num: 10000},
          ]
        }
      ]
    }
  ]

  module Deduction
    extend self

    # Finance::Deduction.deduction
    def deduction(market_datas: $market_datas, hold_datas: $hold_datas)
      tmp = []
      hold_datas.each do |scope|
        res = HoldData.handle_scope(scope: scope)
        return res unless res[0]
        tmp << res[1]
      end 

      [true, tmp]
    end 

    # Finance::Deduction.print_deduction
    def print_deduction(print_type: 'cmd')
      res = deduction
      return res unless res[0]
      res[1].each do |hash|
        case print_type
        when 'cmd'
          print hash.to_s.gsub('},', "},\n")
        when 'page'
          print JSON.pretty_generate(hash)
        end 
      end 
    end 

    module HoldData
      extend self

      def handle_scope(scope: )
        tmp = {scope_name: scope[:scope_name], datas: []}
        scope[:datas].each do |data|
          res = handle_scope_datas(datas: data)
          return res unless res[0]
          tmp[:datas] << {code: data[:code], datas: res[1r]}
        end 
        [true, tmp]
      end   

      def handle_scope_datas(datas:)
        return [false, 'code不能为空'] unless datas[:code]
        res = MarketData.find_by_code(code: datas[:code])
        return res unless res[0]
        market_datas = res[1]

        hold_cost = 0.0
        hold_num = 0
        res = group_datas_by_type(datas: datas[:datas])
        return res unless res[0]
        datas = res[1]
        hold_cost += datas[:init_datas].map{|hash| (hash[:price]*hash[:num])}.inject(0, :+)

				hold_num += datas[:init_datas].map{|hash| hash[:num]}.inject(0, :+)
        tmp = []
        market_datas[:datas].each do |date_data|
					res = find_date_datas_by_date(date_datas: datas[:date_datas], date: date_data[:date])
					if res[0]
						hold_cost += res[1].map{|hash| (hash[:num]* date_data[:price])}.inject(0, :+)
						hold_num += res[1].map{|hash| hash[:num]}.inject(0, :+)
					end 
					market_cost = hold_num*date_data[:price]
					
					tmp << {date: date_data[:date].to_s, date_rofit: (hold_cost - market_cost) }
        end 

        [true, tmp]
      end 

      def group_datas_by_type(datas: )
        tmp = {init_datas: [], date_datas: []}
        datas.each do |hash|
          case hash[:type]
          when :init
            tmp[:init_datas] << hash
          else
            tmp[:date_datas] << hash
          end 
        end 
        [true, tmp]
      end 

      def find_date_datas_by_date(date_datas:, date:)
        date_datas = date_datas.select{|hash| Date.parse(hash[:date]) == date}
				return [false, '未找到对应数据'] unless date_datas
				[true, date_datas]
      end 
    end
    
    module MarketData
      extend self
      def find_by_code(code:, market_datas: $market_datas)
        target = market_datas.select{|hash| hash[:code] == code}[0]
        return [false, "通过code: #{code}未找到目标数据"] unless target
        format_market_data(market_data: target)
      end 

      def format_market_data(market_data: $market_data)
        market_data[:datas].map{|hash| hash.merge!({date: Date.parse(hash[:date])})}
        return [true, market_data]
      end 

      def find_date_data_by_date(market_data:, date:)
        date_data = market_data[:datas].select{|hash| hash[:date] == Date.parse(date)}[0]
        return [false, "根据日期: #{date}, 未找到相关数据"] unless date_data
        [true, date_data]
      end 
    end
  end

end

# Finance::Deduction.deduction
Finance::Deduction.print_deduction