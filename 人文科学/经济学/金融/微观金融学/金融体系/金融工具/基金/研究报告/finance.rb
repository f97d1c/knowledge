require 'pry'

module Finance

  module Fund

    class HashSql
      class << self

        # 根据时间对数据排序
        # order(month: :asc, year: :desc)
        def order(*hash_conditions)
          # array = [{"year": "2120","month": "10","date": "16","num": 10000},{"year": "3020","month": "03","date": "16","num": 10000},{"year": "2019","month": "04","date": "24","num": 10000}]
          @object_datas ||= self.all
        
          hash_conditions[0].each do |condition|
            @object_datas = @object_datas.sort do |x,y| 
              case condition[1]
              when :asc
                x[condition[0].to_sym] <=> y[condition[0].to_sym]
              when :desc
                y[condition[0].to_sym] <=> x[condition[0].to_sym]
              else
                return [false, '错误排序条件']
              end  
            end
          end
        
          [true, @object_datas]
        end

        # 查找指定时间范围内的数据
        # where(month: '04', year: '2019')
        def where(*hash_conditions)
          @object_datas ||= self.all

          def not(*hash_conditions)
            flag = false
          end 
        
          @object_datas.select do |hash|
            result_flag = []
            hash_conditions[0].each do |condition|
              if (flag ||= true)
                result_flag << (hash[condition[0].to_sym] == condition[1])
              else
                result_flag << (hash[condition[0].to_sym] != condition[1])
              end 
            end 
            result_flag.uniq == [true]
          end 

          @object_datas
        end

        def all
          data_content = JSON.parse(File.open('/home/ff4c00/Space/fund_data.json').read)
          
          # TODO: ruby应该提供了更好的解决方法
          class_data = data_content
          self.name.split('::').each do |scope|
            class_data = (class_data[scope] || (raise "scope: #{scope}数据为空"))
          end 

          tmp = []
          class_data.each do |hash|
            tmp << OpenStruct.new(hash)
          end
          [true, tmp]
        end 
      end
    end 

    # 数据分析
    # 根据提供的持有基金相关数据进行各项演算
    class Deduction
      class << self
        def deduction(type: :profits, hold_funds:)
          send(type, {hold_funds: hold_funds})
          # send(type, *[hold_funds: hold_funds])
          # profit(hold_funds: hold_funds)
        end 

        private
          # 持有收益演算
          def profits(hold_funds:)
            tmp = []
            hold_funds.each do |hold_fund|
              tmp << profit(hold_fund: hold_funds)
            end
            [true, tmp]
          end 

          def profit(hold_fund:)
            return [false, '基金编码为空'] unless hold_fund.code
            market_funds = MarketFund.where(code: hold_fund.code).first
            return [false, '对应市场数据为空'] unless market_funds
            
            hold_cost = 0.0
            hold_num = 0
            init_changes = hold_fund.changes.select{|hash| hash[:type] == 'init'}
            hold_num += init_changes.map{|hash| hash[:num]}.inject(0, :+)
            hold_cost += init_changes.map{|hash| (hash[:price]*hash[:num])}.inject(0, :+)
            tmp = []
            market_funds.each do |market_fund|
              
              same_date_datas = hold_fund.changes.select do |hash| 
                (hash[:year] == market_fund[:year]) &&
                (hash[:month] == market_fund[:month]) &&
                (hash[:date] == market_fund[:date])
              end

              if same_date_datas
                hold_cost += same_date_datas.map{|hash| (hash[:num]* market_fund[:price])}.inject(0, :+)
                hold_num += same_date_datas.map{|hash| hash[:num]}.inject(0, :+)
              end
              market_cost = hold_num*market_fund[:price]

				    	tmp << {date: market_fund[:date].to_s, date_rofit: (hold_cost - market_cost) }

            end
            
            [true, tmp]
          end
      end
    end  

    class HoldFund < HashSql
      
    end

    class MarketFund < HashSql
      
    end

  end


end

Finance::Fund::Deduction.deduction(hold_funds: Finance::Fund::HoldFund.all)