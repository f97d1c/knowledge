
<!-- TOC -->

- [关于](#关于)
  - [目的](#目的)
  - [下一步](#下一步)
- [Elasticsearch](#elasticsearch)
  - [基金每日数据](#基金每日数据)
    - [索引映射](#索引映射)
    - [导入数据](#导入数据)
  - [基金基本信息](#基金基本信息)
    - [索引映射](#索引映射-1)
    - [导入数据](#导入数据-1)
- [参考资料](#参考资料)

<!-- /TOC -->

# 关于

## 目的

本数据研究旨在利用Elasticsearch等工具针对研究对象的历史表现加以研究分析.<br>

## 下一步

0. 导入数据 -排查-> 目前读取文件导入总会漏掉最后一行数据
0. requestES -优化-> 返回异常结果时记录或中断程序

# Elasticsearch

## 基金每日数据

### 索引映射

```bash
requestES DELETE 'daily_market_funds_20210512'
requestES PUT 'daily_market_funds_20210512'
requestES PUT 'daily_market_funds_20210512/_alias/daily_market_funds'

requestES PUT 'daily_market_funds_20210512/_mapping' '
{
  "properties": {
    "基金简称": {
      "type": "keyword"
    },
    "基金代码": {
      "type": "keyword"
    },
    "净值日期": {
      "type": "date"
    },
    "单位净值": {
      "type": "double"
    },
    "累计净值": {
      "type": "double"
    },
    "日增长率": {
      "type": "double"
    }
  } 
}'
```

### 导入数据

```sh
while IFS= read -r line || [ -n "$line" ]; do
  md5=$(echo -n "$line" |md5sum)
  requestES POST "daily_market_funds/_doc/${md5:0:32}" "$line"
done < /home/$USER/Space/知识体系/人文科学/经济学/金融/微观金融学/金融体系/金融工具/基金/数据研究/基础数据/377240.txt
```

## 基金基本信息

### 索引映射

```sh
requestES DELETE 'fund_infos_20210808'
requestES PUT 'fund_infos_20210808'
requestES PUT 'fund_infos_20210808/_alias/fund_infos'

requestES PUT 'fund_infos_20210808/_mapping' '
{
  "properties": {
    "基金全称": {
      "type": "keyword"
    },
    "基金简称": {
      "type": "keyword"
    },
    "基金代码": {
      "type": "keyword"
    },
    "基金类型": {
      "type": "keyword"
    },
    "发行日期": {
      "type": "date"
    },
    "成立日期": {
      "type": "date"
    },
    "成立规模(亿份)": {
      "type": "double"
    },
    "资产规模(亿元)": {
      "type": "double"
    },
    "份额规模(亿份)": {
      "type": "double"
    },
    "基金经理人": {
      "type": "keyword"
    },
    "基金托管人": {
      "type": "keyword"
    },
    "每份累计分红(元)": {
      "type": "double"
    },
    "分红次数": {
      "type": "integer"
    },
    "管理费率(每年/%)": {
      "type": "double"
    },
    "托管费率(每年/%)": {
      "type": "double"
    },
    "销售服务费率(每年/%)": {
      "type": "double"
    },
    "最高认购费率(前端/%)": {
      "type": "double"
    },
    "业绩比较基准": {
      "type": "text"
    },
    "跟踪标的": {
      "type": "keyword"
    }
  }
}'
```

### 导入数据

```sh
while IFS= read -r line || [ -n "$line" ]; do
  md5=$(echo -n "$line" |md5sum)
  requestES POST "fund_infos/_doc/${md5:0:32}" "$line"
done < /home/$USER/Space/知识体系/人文科学/经济学/金融/微观金融学/金融体系/金融工具/基金/数据研究/基础数据/fund_infos.txt
```

# 参考资料

> [](http://fundf10.eastmoney.com/jbgk_code.html)