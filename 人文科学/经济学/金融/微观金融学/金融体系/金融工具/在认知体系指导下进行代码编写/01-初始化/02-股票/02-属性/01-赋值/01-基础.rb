遍历文件夹赋值 = lambda do |属性名:, 关键词:nil, 路径:, 层级: 0|
  return [true, ''] if @股票.send(属性名)

  unless File::directory?(路径)
    关键词 ||= 属性名
    return [false, 路径+'路径不存在'] unless 路径.include?(关键词)
    内容 = File.read(路径)
    值 = (JSON.parse(内容) rescue 内容)
    @股票.instance_variable_set("@#{属性名}", 值)
  end

  Dir["#{路径}/*"].sort.each do |d|
    遍历文件夹赋值.call(属性名:属性名, 关键词: 关键词, 路径: d, 层级:(层级+1))
    break if @股票.send(属性名)
  end

  if 层级 == 0
    return [true, ''] if @股票.send(属性名)
    return [false, '搜索结果为空']
  end
end

@股票.def_attributes.each do |属性名, 详情|
  next if 详情['来源']
  res = 遍历文件夹赋值.call(属性名: 属性名, 路径: @股票.params['数据存储路径'])
  next if res[0]

  断点调试(binding) unless 详情['别名']

  详情['别名'].each do |别名|
    res = 遍历文件夹赋值.call(属性名: 属性名, 关键词: 别名, 路径: @股票.params['数据存储路径'])
    break res if res[0]
  end
end
