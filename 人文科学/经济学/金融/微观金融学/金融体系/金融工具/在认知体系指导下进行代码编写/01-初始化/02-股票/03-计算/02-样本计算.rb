@股票.量化标准.each do |名称, 标准|
  unless 标准['计算公式']
    标准['计算结果'] = 标准['样本']
    next
  end

  res = 公式计算(步骤: 标准['计算公式'], 变量: 标准['样本'])
  raise (res << 名称).to_json unless res[0]
  标准['计算结果'] = res[1]
end