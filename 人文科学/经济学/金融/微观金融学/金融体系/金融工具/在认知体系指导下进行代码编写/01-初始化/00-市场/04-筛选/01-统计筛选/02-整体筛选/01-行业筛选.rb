各指标筛选结果中所属行业 = @市场.量化统计.map{|k,v| [k, v['筛选结果'].keys]}.to_h
行业计数 =  Hash.new(0)

各指标筛选结果中所属行业.each do |指标名称, 筛选行业|
  筛选行业.each do |行业名称|    
    行业计数[行业名称] +=1
  end
end

@筛选结果 = 行业计数.select{|k,v| v == @市场.量化统计.keys.size}