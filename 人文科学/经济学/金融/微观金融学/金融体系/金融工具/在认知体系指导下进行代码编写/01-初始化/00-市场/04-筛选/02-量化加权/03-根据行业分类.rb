分类结果 = {}

分类结果['行业加权平均值'] = @市场.量化加权['筛选结果']['样本'].map{|k,v| [k, v.values.sum/v.keys.size]}.sort{|x,y| y[1] <=> x[1]}.to_h

入选数量占总体比重 = @市场.量化加权['筛选结果']['样本'].map{|k,v| [k, [v.keys.size,@市场.行业分类[k]['行业股票'].keys.size].join('/')]}

分类结果['入选数量占总体比重'] = 入选数量占总体比重.sort do |x,y|
  a,b = x[1].split('/')
  c,d = y[1].split('/')
  
  比重比较 = c.to_f/d.to_f <=> a.to_f/b.to_f
  if 比重比较 == 0
    d.to_f <=> b.to_f
  else
    比重比较
  end
end.to_h

@市场.量化加权['筛选结果']['根据行业划分'] = 分类结果