dirs = Dir["#{@市场.params['数据存储路径']}/*"].select{|d| File.directory?(d)}
根据中文拼音首字母排序 = dirs.sort_by{|dir|(PinYin.of_string(dir.split('/')[-1][0])[0][0]) || dir}

根据中文拼音首字母排序.each do |路径|
  next unless Dir["#{路径}/*"].map{|d| /.*\/\d{6}\.[a-z A-Z]{2}$/ =~ d}.uniq.include?(0)

  arguments = JSON.parse(@市场.arguments.to_json)
  arguments['参数']['数据存储路径'] = 路径
  res = 执行Ruby脚本(参数: arguments)
  next unless res[0]
  @市场.行业分类[路径.split('/')[-1]] ||= res[1]
end
