股票筛选结果 = {}

@筛选结果.keys.each do |行业名称|
  股票筛选结果[行业名称] = {}
  @市场.量化统计.each do |量化指标名称, 详情|
    详情['筛选结果'][行业名称].each do |股票名称, 指标值|
      (股票筛选结果[行业名称][股票名称]||={})[量化指标名称] = 指标值
    end
  end
end

@筛选结果 = 股票筛选结果.map do |k,v| 
  [k, v.select{|k,v| v.keys.size == @市场.量化统计.keys.size}]
end.select{|k,v| v != {}}.to_h