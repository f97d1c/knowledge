筛选结果 = @市场.量化加权['筛选结果']['样本'].values.map{|h| h.to_a}.flatten(1).sort{|x,y| y[1] <=> x[1]}.to_h

result = T统计.new(数组: 筛选结果.values).statistics
差值 = result['最大值'] - result['最小值']
间隔 = ([10,5,1].select{|i| i if 差值 > i})[0]||1

根据权值划分 = {}
(0..(result['最大值'].ceil/间隔).ceil).to_a.reverse.each do |number|
  max = (number+1)*间隔
  min = number*间隔
  结果 = 筛选结果.select{|k,v| max > v && v > min}
  next if 结果 == {}
  根据权值划分["#{max}~#{min}"] = 结果
end

@市场.量化加权['筛选结果']['根据权值划分'] = 根据权值划分
