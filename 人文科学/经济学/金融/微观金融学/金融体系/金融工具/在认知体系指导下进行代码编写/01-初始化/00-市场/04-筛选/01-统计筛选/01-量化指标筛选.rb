@市场.量化统计.each do |量化指标名称, 详情|
  筛选结果 = 详情['样本'].map do |行业名称, 股票|
    [[行业名称, 股票.select{|k,v| v > 详情['平均数']}]]
  end.flatten(1).select{|k,v| v != {}}.to_h
  详情['筛选结果'] = 筛选结果
end