结果倒置 = lambda do |对象:, 当前层级:0, 最大层级: 2|
  return 对象 unless 对象.is_a?(Hash)
  return 对象 unless 当前层级 < 最大层级

  对象 = 对象.to_a.reverse.to_h
  对象.each do |k,v|
    对象[k] = 结果倒置.call(对象: v, 当前层级: 当前层级+1, 最大层级: 最大层级)
  end

  return 对象
end

@output = 结果倒置.call(对象: @output)