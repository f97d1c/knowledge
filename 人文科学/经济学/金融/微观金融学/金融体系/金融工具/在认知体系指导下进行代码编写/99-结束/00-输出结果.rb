@output = {}
@object = [@市场, @行业, @股票].reject(&:nil?).first

@object.instance_variables.map{|var|var[1..-1]}.each do |属性名|
  next if /^[a-z _]*$/ =~ 属性名
  @output[属性名] = @object.send(属性名)
end