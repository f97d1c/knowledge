def 公式计算(步骤:, 变量:)
  return [false, '存在为空变量', 变量] if 变量.values.include?(nil)

  变量 = 变量.clone
  实际步骤 = [步骤].flatten
  结果 = nil

  实际步骤.each do |步骤|
    if 步骤.include?('=')
      变量名, 表达式 = 步骤.split('=')
    else
      变量名 = 变量名
      表达式 = 步骤
    end

    变量.each do |键,值|
      表达式 = 表达式.gsub(键, 值.to_s)
    end

    begin
      # 步骤内只进行单步运算 这里或者匹配加减乘除再处理
      运算结果 = eval(表达式)
    rescue => exception
      断点调试(binding)
    end

    变量[变量名] = 运算结果
    结果 = 运算结果 if 实际步骤[-1] == 步骤
  end

  断点调试(binding) unless 结果
  [true, 结果]
end