
def 深度搜索文件(文件夹:, 关键词:, 层级:0, 结果: [])
  return [false, '给定路径非文件夹'] if !File.directory?(文件夹) && 层级 == 0
  return unless File.directory?(文件夹)

  搜索结果 = Dir["#{文件夹}/#{关键词}"]
  结果 << 搜索结果.select{|i| File.file?(i)} unless 搜索结果 == []

  if File.file?(文件夹)
    if 关键词.is_a?(String)
      return (结果 << 文件夹) if 文件夹.include?(关键词)
    else
      断点调试(binding)
    end
  end

  Dir["#{文件夹}/*"].sort.each do |路径|
    深度搜索文件(文件夹: 路径, 关键词: 关键词, 层级: (层级+1), 结果: 结果)
  end

  return [false, '结果为空'] if 层级 == 0 && 结果 == []
  [true, 结果.flatten]
end