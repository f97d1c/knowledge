def 存储结构转对象属性(对象:, 属性文件夹:, 默认值: {})
  对象.params['属性文件夹'] = 属性文件夹
  定义属性 = {}

  遍历文件夹 = lambda do |路径|
    return unless File::directory?(路径)
    return Dir["#{路径}/*"].sort.each{|d| 遍历文件夹.call(d)} unless Dir["#{路径}/*"] == []
    属性名 = 路径.split('/')[-1].gsub(/\d{2,}\-/,'')
    
    定义属性[属性名] = {'定义路径' => 路径.gsub(属性文件夹,'.')}
    
    Dir.entries(路径).each do |文件名|
      next if ['.', '..', '.gitkeep'].include?(文件名)
      文件路径 = [路径, 文件名].join('/')
      断点调试(binding) unless File::file?(文件路径)
      内容 = File.read(文件路径)
      定义属性[属性名][文件名.gsub(/\./,'')] = (JSON.parse(内容) rescue 内容)
    end

  end

  遍历文件夹.call(属性文件夹)

  对象.class.attr_accessor "def_attributes"
  对象.def_attributes = 定义属性

  对象.def_attributes.each do |键,值|
    对象.class.attr_accessor "#{键}"
    对象.instance_variable_set("@#{键}", 默认值.clone)
  end
end