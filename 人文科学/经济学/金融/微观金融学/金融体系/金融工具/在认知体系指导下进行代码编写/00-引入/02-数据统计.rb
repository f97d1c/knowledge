class T统计
  attr_accessor :数组

  def initialize(数组:)
    self.数组 = 数组.sort!
  end

  def statistics
    result = {}
    self.class.instance_methods(false).select{|name| /[^ a-z 数组 =]+/ =~ name}.each do |method|
      result[method.to_s] = self.send(method)
    end
    result
  end

  def 平均数
    self.数组.sum / self.数组.size
  end

  def 截尾均值
    (self.数组[1..-2]).sum / self.数组.size-2
  end
  
  def 方差
    m = self.数组.inject(:+) / self.数组.size
    s = self.数组.map { |i| (i - m) ** 2 }.inject(:+).to_f / self.数组.size
  end

  def 中位数
    n = self.数组.size
    if n % 2 == 1
      return self.数组[n / 2]
    else
      return (self.数组[n / 2] + self.数组[n / 2 - 1]) / 2
    end
  end

  def 平均差
    Math.sqrt(self.数组.sum { |element| (element - (self.数组.sum / self.数组.size))**2 } / self.数组.size)
  end

  def 标准差
    Math.sqrt(self.数组.sum { |element| (element - (self.数组.sum / self.数组.size))**2 } / self.数组.size)
  end

  def 上四分位数
    self.数组[(self.数组.size * 0.75).floor]
  end

  def 四分位差
    # 获取第25%和第75%位置的数据
    q1 = self.数组[(self.数组.size * 0.25).floor]
    q3 = self.上四分位数

    q3 - q1
  end

  def 偏度
    sum = 0
    self.数组.each do |element|
      sum += (element - (self.数组.sum / self.数组.size))**3
    end
    
    (sum / self.数组.size) / ((self.数组.sum / self.数组.size)**3)
  end

  def 峰度
    sum = 0
    self.数组.each do |element|
      sum += (element - (self.数组.sum / self.数组.size))**4
    end
    (sum / self.数组.size) / ((self.数组.sum / self.数组.size)**4)
  end

  def 最大值
    self.数组.max
  end
  
  def 最小值
    self.数组.min
  end
end