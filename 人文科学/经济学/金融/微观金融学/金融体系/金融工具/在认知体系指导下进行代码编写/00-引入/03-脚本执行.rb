def 执行Ruby脚本(路径: 'ruby.rb', 参数:)
  唯一标识 = Digest::MD5.hexdigest(参数.to_s)
  存储结果 = Dir["#{参数['参数']['数据存储路径']}/.*#{唯一标识}*"]
  return [true, (JSON.parse(File.read(存储结果[0])) rescue 断点调试(binding))] if 存储结果[0]

  参数['参数']['结果存储文件'] = ".临时存储-#{唯一标识}.json"

  stdout_str, stderr_str, status = Open3.capture3("ruby #{路径} '#{参数.to_json}'")  
  if status.success?
    return (JSON.parse(stdout_str.split("\n")[-1]) rescue 断点调试(binding))
  else  
    关键信息 = stderr_str.split("\n")[0]
    断点调试(binding) unless 关键信息.include?('[false,')
    错误说明 = 关键信息.gsub(/.*(\[false,.*\]).*/, '\1')
    return (JSON.parse(错误说明) rescue 断点调试(binding))
  end

  return [true, res[1]]
end