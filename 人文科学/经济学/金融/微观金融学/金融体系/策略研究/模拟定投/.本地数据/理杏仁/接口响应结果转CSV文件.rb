
@字段 = {
  "日期"=>"date", 
  "指数代码"=>"stockCode", 
  "动态市盈率"=>"pe_ttm",
  "PB"=>"pb",
  "动态市销率"=>"ps_ttm",
  "股息率"=>"dyr",
  "成交量"=>"tv",
  "成交金额"=>"ta",
  "换手率"=>"to_r",
  "收盘点位"=>"cp",
  "涨跌幅"=>"cpc",
  "全收益收盘点位(元)"=>"r_cp",
  "全收益收盘点位涨跌幅"=>"r_cpc",
  "市值(元)"=>"mc",
  "流通市值(元)"=>"cmc",
  "自由流通市值(元)"=>"ecmc",
  "融资余额"=>"fb",
  "融券余额"=>"sb",
  "陆股通持仓金额"=>"ha_shm",
  "陆股通净买入金额"=>"mm_nba",
  "市值加权"=>"mcw",
  "等权"=>"ew",
  "正数等权"=>"ewpvo",
  "平均值"=>"avg",
  "中位数"=>"median",
}

def 遍历对象(对象:, 结果:, 前缀: '')
  对象.each do |键,值|
    名称 = @字段.invert[键]
    puts "[#{键}]映射为空" && exit(255) unless 名称

    case 值
    when Hash
      遍历对象(对象: 值, 结果: 结果, 前缀: 名称)
    else
      结果[前缀+名称] = 值 
    end
  end

  结果
end

Dir[".本地数据/理杏仁/接口响应/*.json"].each do |路径|
  内容 = JSON.parse(File.open(路径){|f|f.read})

  数据 = {}
  内容.each do |返回结果|
    binding.pry unless 返回结果['code'] == 1

    返回结果['data'].each do |单条数据|
      结果 = 遍历对象(对象: 单条数据, 结果: {})
      结果['日期'] = 结果['日期'].split('T')[0]
      binding.pry if 结果.values.include?(nil)
      数据[结果['日期']] = 结果
    end
  end

  数据 = 数据.values
  输出文件路径 = 路径.gsub('接口响应', '回测数据').gsub('json', 'csv')
  表头 = 数据.first.keys

  CSV.open(输出文件路径, 'w', write_headers: true, headers: 表头) do |csv|
    数据.each do |row|
      csv << row.values_at(*表头)
    end
  end
end
