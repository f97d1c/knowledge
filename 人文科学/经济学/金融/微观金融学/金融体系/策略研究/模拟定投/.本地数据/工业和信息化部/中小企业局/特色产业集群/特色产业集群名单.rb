# https://wap.miit.gov.cn/jgsj/qyj/wjfb/art/2023/art_1f927db0a980491183d1959bd6d1e53c.html

数据目录='.本地数据/工业和信息化部/中小企业局/特色产业集群'

集群名单 = JSON.parse(File.open("#{数据目录}/特色产业集群年度名单.json"){|f| f.read()})

格式化 = {}

def 遍历赋值(数组:,对象:)

  键 = 数组.delete_at(0)
  if 数组.size != 1
    对象[键] ||= {}
    return 遍历赋值(数组: 数组, 对象: 对象[键])
  end

  对象[键] ||= []
  对象[键] << 数组[0] rescue binding.pry
end

集群名单.each do |年份, 名单|
  名单.each do |名称|
    名称 = 名称.gsub('产业集群', '')
    拆分项 = 名称.match(Regexp.new(['省', '自治区', '生产建设兵团', '市', '区', '县'].map{|i| "(.*#{i}){0,1}"}.join()+'(.+)'))
    binding.pry unless 拆分项

    拆分项 = 拆分项.to_a[1..-1]
    拆分项.delete(nil)
    # binding.pry if 名称.include?('厦门市思明区智慧城市')
    遍历赋值(数组: 拆分项, 对象: 格式化)
  end
end

File.open("#{数据目录}/特色产业集群名单.json", 'w'){|f| f.write(JSON.pretty_generate(格式化))}