# https://www.lixinger.com/open/api/doc?api-key=cn/index/fundamental

已有回测数据 = Dir[@回测数据路径]
所有指数信息 = JSON.parse(File.open('.本地数据/理杏仁/所有指数信息.json'){|f|f.read})

# 生成结果存储文件
@回测指数范围.each do |回测指数|
  next if 已有回测数据.map{|n| n.include?(回测指数)}.include?(true)

  存储文件 = Dir[".本地数据/理杏仁/接口响应/#{回测指数}*.json"][0]
  next if 存储文件 && File.exist?(存储文件)

  指数信息 = 所有指数信息['data'].select{|h|h['name'] == 回测指数}
  binding.pry unless 指数信息.size == 1
  指数信息 = 指数信息[0]

  成立日期 = 指数信息['launchDate'].split('T')[0]
  截止日期 = Date.today.prev_day

  File.open(".本地数据/理杏仁/接口响应/#{回测指数}_#{成立日期}_#{截止日期}.json",'w'){|f|f.write('')}
end

# 生成请求参数
Dir[".本地数据/理杏仁/接口响应/*.json"].select{|n| File.size(n) == 0}.each do |存储文件|
  回测指数, 成立日期, 截止日期 = 存储文件.split('/')[-1].split('.')[0].split('_')
  成立日期 = Date.parse(成立日期)
  截止日期 = Date.parse(截止日期)

  指数信息 = 所有指数信息['data'].select{|h|h['name'] == 回测指数}
  binding.pry unless 指数信息.size == 1
  指数信息 = 指数信息[0]

  请求参数 = []

  当前日期 = 成立日期.clone
  # 循环直到当前日期超过结束日期
  while 当前日期 < 截止日期
    开始日期 = 当前日期.clone
    # 将当前日期增加10年
    当前日期 = 当前日期 >> 12*10
    当前日期 = 截止日期 if 当前日期 >= 截止日期
    
    请求参数 << {  
      "token": "5e0ca765-32d2-46d6-9e93-db6edfd8c631",
      "startDate": 开始日期,
      "endDate": 当前日期,
      "stockCodes": [指数信息['stockCode']],
      "metricsList": ["pe_ttm","pb","ps_ttm","dyr","tv","ta","to_r","cp","cpc","r_cp","r_cpc","mc","cmc","ecmc","fb","sb","ha_shm","mm_nba"
      ]
    }
  end

  文件对象 = File.open(存储文件, 'w')
  文件对象.write("[\n")
  请求参数.each{|i| 文件对象.write("#{i.to_json}\n")}
  文件对象.write("\n]")
end



