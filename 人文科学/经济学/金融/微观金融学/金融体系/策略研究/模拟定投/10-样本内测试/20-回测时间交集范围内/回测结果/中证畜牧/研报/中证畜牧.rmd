---
title: "中证畜牧"
# date: "`r format(Sys.time(), '%Y-%m-%d %H:%M:%OS3')`"

# 以html或word格式输出
output: 
  html_document:
    theme: dark
    toc: true
    toc_depth: 6
    toc_float: false
    number_sections: true
---

<link rel="stylesheet" type="text/css" href="/home/ubuntu/test/00-代码模块/01-语言/R/配置/body.css">
<link rel="stylesheet" type="text/css" href="/home/ubuntu/test/00-代码模块/01-语言/R/配置/dygraph.css">
<link rel="stylesheet" type="text/css" href="/home/ubuntu/test/00-代码模块/01-语言/R/配置/dataTables.css">

<script src="/home/ubuntu/test/00-代码模块/01-语言/R/配置/body.js"/>

# 中证畜牧


## 概览

### 期末基准值比较
```{r, echo=FALSE, out.width='100%'}
绘制('概览/期末基准值比较')
```

### PB
```{r, echo=FALSE, out.width='100%'}
绘制('概览/PB')
```

### 绝对收益率
```{r, echo=FALSE, out.width='100%'}
绘制('概览/绝对收益率')
```
<img src='/home/ubuntu/test/10-样本内测试/20-回测时间交集范围内/回测结果/中证畜牧/图片/绝对收益率.png'>

### 加权平均成本
```{r, echo=FALSE, out.width='100%'}
绘制('概览/加权平均成本')
```
<img src='/home/ubuntu/test/10-样本内测试/20-回测时间交集范围内/回测结果/中证畜牧/图片/加权平均成本.png'>

### 卖出收益
```{r, echo=FALSE, out.width='100%'}
绘制('概览/卖出收益')
```
<img src='/home/ubuntu/test/10-样本内测试/20-回测时间交集范围内/回测结果/中证畜牧/图片/卖出收益.png'>

### 总资产
```{r, echo=FALSE, out.width='100%'}
绘制('概览/总资产')
```
<img src='/home/ubuntu/test/10-样本内测试/20-回测时间交集范围内/回测结果/中证畜牧/图片/总资产.png'>

### 盈利金额
```{r, echo=FALSE, out.width='100%'}
绘制('概览/盈利金额')
```
<img src='/home/ubuntu/test/10-样本内测试/20-回测时间交集范围内/回测结果/中证畜牧/图片/盈利金额.png'>

### 累计投入资金
```{r, echo=FALSE, out.width='100%'}
绘制('概览/累计投入资金')
```
<img src='/home/ubuntu/test/10-样本内测试/20-回测时间交集范围内/回测结果/中证畜牧/图片/累计投入资金.png'>

### 市值
```{r, echo=FALSE, out.width='100%'}
绘制('概览/市值')
```
<img src='/home/ubuntu/test/10-样本内测试/20-回测时间交集范围内/回测结果/中证畜牧/图片/市值.png'>

### 回收资金
```{r, echo=FALSE, out.width='100%'}
绘制('概览/回收资金')
```
<img src='/home/ubuntu/test/10-样本内测试/20-回测时间交集范围内/回测结果/中证畜牧/图片/回收资金.png'>

### 持有股数
```{r, echo=FALSE, out.width='100%'}
绘制('概览/持有股数')
```
<img src='/home/ubuntu/test/10-样本内测试/20-回测时间交集范围内/回测结果/中证畜牧/图片/持有股数.png'>

## 列表

### 基础定期定额法
```{r, echo=FALSE, out.width='100%'}
绘制('列表/基础定期定额法')
```

### 基于中枢市净率的定期定额法
```{r, echo=FALSE, out.width='100%'}
绘制('列表/基于中枢市净率的定期定额法')
```

### 基于中枢市净率和线性增量的定期买入额度不定法
```{r, echo=FALSE, out.width='100%'}
绘制('列表/基于中枢市净率和线性增量的定期买入额度不定法')
```

### 基于中枢市净率和平方增量的定期买入额度不定法
```{r, echo=FALSE, out.width='100%'}
绘制('列表/基于中枢市净率和平方增量的定期买入额度不定法')
```

### 基于中枢市净率和立方增量的定期买入额度不定法
```{r, echo=FALSE, out.width='100%'}
绘制('列表/基于中枢市净率和立方增量的定期买入额度不定法')
```

### 基于中枢市净率和平方增量的定期买入及卖出且额度不定法
```{r, echo=FALSE, out.width='100%'}
绘制('列表/基于中枢市净率和平方增量的定期买入及卖出且额度不定法')
```

### 基于历史市净率均值和平方增量的定期买入及卖出且额度不定法
```{r, echo=FALSE, out.width='100%'}
绘制('列表/基于历史市净率均值和平方增量的定期买入及卖出且额度不定法')
```

### 基于近五年市净率均值和平方增量的定期买入及卖出且额度不定法
```{r, echo=FALSE, out.width='100%'}
绘制('列表/基于近五年市净率均值和平方增量的定期买入及卖出且额度不定法')
```

### 基于近五年市净率均值和立方增量的定期买入及卖出且额度不定法
```{r, echo=FALSE, out.width='100%'}
绘制('列表/基于近五年市净率均值和立方增量的定期买入及卖出且额度不定法')
```

### 基于近五年市净率均值和买入动态步长的定投法
```{r, echo=FALSE, out.width='100%'}
绘制('列表/基于近五年市净率均值和买入动态步长的定投法')
```

### 基于近五年市净率均值和买入动态步长以及无风险收益的定投法
```{r, echo=FALSE, out.width='100%'}
绘制('列表/基于近五年市净率均值和买入动态步长以及无风险收益的定投法')
```

### 基于近五年市净率均值和买入卖出动态步长以及无风险收益的定投法
```{r, echo=FALSE, out.width='100%'}
绘制('列表/基于近五年市净率均值和买入卖出动态步长以及无风险收益的定投法')
```

### 基于近五年市净率均值和立方增量动态步长以及无风险收益的定投法
```{r, echo=FALSE, out.width='100%'}
绘制('列表/基于近五年市净率均值和立方增量动态步长以及无风险收益的定投法')
```