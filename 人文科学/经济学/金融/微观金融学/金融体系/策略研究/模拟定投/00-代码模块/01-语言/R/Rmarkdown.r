# Rscript 00-代码模块/01-语言/R/Rmarkdown.r $PWD'/00-代码模块/01-语言/R/测试/数据统计.json'

source("00-代码模块/01-语言/R/R.r")

必须参数 = list(
  c('运行参数', '绘制', 'set', '绘制内容'),
  c('运行参数', '参数', 'set', '参数内容')
)

for (array in 必须参数){
  tryCatch(
    assign('value', get(array[2], get(array[1]))),
    error = function(e) {
      print(toJSON(c('false', paste0(array[1], '中', array[2], '属性不能为空'))))
      q(status = 255)
    }
  )

  if (array[3] == 'fromJSON'){
    assign(array[4], fromJSON(value))
  }else if (array[3] == 'set'){
    assign(array[4], value)
  }
}

for (输出格式 in 参数内容$文档转换$输出格式){
  rmarkdown::render(
    参数内容$文档转换$路径, 
    output_format = 输出格式,
    encoding="UTF-8",
    output_file=参数内容$文档转换$输出路径
  )
}

print(toJSON(c(TRUE)))
q(status = 0)