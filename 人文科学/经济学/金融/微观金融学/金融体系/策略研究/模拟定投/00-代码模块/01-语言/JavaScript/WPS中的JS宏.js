/*
  用于将特定格式应用于目标Word文档.
*/

var 标题_字体 = '微软雅黑';

var 正文_字体 = "宋体";
var 正文_字号 = 11;

var 页边距_上 = 2.54;
var 页边距_下 = 2.54;
var 页边距_左 = 1.91;
var 页边距_右 = 1.91;

function 运行_全部() {
  换行_特定标识符替换();
  页面_页边距();
  标题();
  表格();
  表格_头部();
  表格_序号();
  表格_宽度_自适应窗口大小();
  alert('格式化完成');
}

function 页面_页边距() {
  // 获取活动文档
  var doc = Application.ActiveDocument;

  // 设置页边距
  doc.PageSetup.TopMargin = 页边距_上 * 28.3464567; // 将厘米转换为磅
  doc.PageSetup.BottomMargin = 页边距_下 * 28.3464567;
  doc.PageSetup.LeftMargin = 页边距_左 * 28.3464567;
  doc.PageSetup.RightMargin = 页边距_右 * 28.3464567;
}

function 标题() {
  // 获取活动文档
  var doc = Application.ActiveDocument;

  // 遍历文档中的所有段落
  for (var i = 1; i <= doc.Paragraphs.Count; i++) {
    var para = doc.Paragraphs(i);
    if (!para.Style.NameLocal.includes("标题")) continue;

    // 检查段落样式是否为 "Heading 4"
    if (para.Style.NameLocal == "标题 4") {
      // 设置字号
      para.Range.Font.Size = 14;
    }

    // 设置字体
    para.Range.Font.Name = 标题_字体;
    // 设置文字颜色为黑色
    para.Range.Font.Color = wdColorBlack;

    // 确保文字不是斜体
    para.Range.Font.Italic = false;
  }
}

function 表格() {
  for (table of Application.ActiveDocument.Tables) {
    for (row of table.Rows) {
      // 遍历第一行中的每个单元格
      for (cell of row.Cells) {
        // 设置单元格的字体,字号
        cell.Range.Font.Name = 正文_字体;
        cell.Range.Font.Size = 正文_字号;
      }
    }
  }
}

function 表格_头部() {
  for (table of Application.ActiveDocument.Tables) {
    var headerRow = table.Rows(1);

    // 遍历第一行中的每个单元格
    for (var j = 1; j <= headerRow.Cells.Count; j++) {
      var cell = headerRow.Cells(j);
      cell.Range.Font.Bold = true // 加粗

      // 设置单元格的文字居中
      cell.VerticalAlignment = wdCellAlignVerticalCenter; // 垂直居中
      cell.Range.ParagraphFormat.Alignment = wdAlignParagraphCenter; // 水平居中

      // 获取单元格的段落格式
      var paragraphFormat = cell.Range.ParagraphFormat;
      paragraphFormat.WordWrap = false; // 禁用自动换行
    }
  }
}

function 表格_序号() {
  for (table of Application.ActiveDocument.Tables) {
    var headerRow = table.Rows(1);
    var cell = headerRow.Cells(1);

    // 获取单元格中的文字
    var cellText = cell.Range.Text;
    // 去除文本末尾的换行符
    if (cellText.endsWith("\r")) {
      cellText = cellText.slice(0, -1);
    }

    if (!cellText in ["序号", "#"]) continue;

    for (var j = 2; j <= table.Rows.Count; j++) {
      var cell = table.Rows(j).Cells(1);
      cell.Range.ParagraphFormat.Alignment = wdAlignParagraphRight; // 水平靠右对齐
    }

  }
}

function 表格_宽度_自适应窗口大小() {
  for (table of Application.ActiveDocument.Tables) {
    table.AutoFitBehavior(wdAutoFitWindow);
  }
}

function 换行_特定标识符替换() {
  find = Selection.Find

  find.Text = "↩";
  find.Forward = true;
  find.Wrap = wdFindContinue;
  find.MatchCase = false;
  find.MatchByte = true;
  find.MatchWildcards = false;
  find.MatchWholeWord = false;
  find.MatchFuzzy = false;
  find.Replacement.Text = "^p";

  Selection.Find.Execute(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, wdReplaceAll, undefined, undefined, undefined, undefined);
}
