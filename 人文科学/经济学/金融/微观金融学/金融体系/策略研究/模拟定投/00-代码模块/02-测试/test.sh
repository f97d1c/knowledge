#!/bin/bash --login
source $(find . -type f -name 'Bash.sh')
SCRIPT_PATH=$(readlink -f "${BASH_SOURCE[0]}")
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")
result_path=$(jq -r '.["参数"]["根路径_回测结果"]' "$1")
indices=($(jq -r '.["参数"]["回测指数范围"][]' "$1"))
