require_relative '../../Ruby.rb'

文件内容 = File.open("#{@项目根路径}/索引.json"){|f| JSON.parse(f.read())}
文件 = File.open("#{@项目根路径}/索引.md", 'w')

def 遍历生成目录(对象:,内容:, 层级: 1)
  对象.write("<!-- TOC -->\n") if 层级 == 1
  内容.each do |键,值|
    next unless 值.is_a?(Hash)
    对象.write("#{Array.new(层级, '  ').join}- [#{键}](##{键})\n")
    遍历生成目录(对象: 对象,内容: 值, 层级: 层级+1)
  end

  对象.write("<!-- /TOC -->\n\n") if 层级 == 1
end

遍历生成目录(对象: 文件, 内容: 文件内容)


def 遍历生成内容(对象:,内容:, 层级: 1)
  内容.each do |键,值|
    next unless 值.is_a?(Hash)
    对象.write("#{Array.new(层级, '#').join} #{键}\n\n")
    遍历生成内容(对象: 对象,内容: 值, 层级: 层级+1)
  end

  return unless 内容.values.map(&:class).uniq == [Array]

  列名 = 内容.values.map{|i| i.map(&:first)}.uniq[0]
  对象.write(列名.map{|i| "|#{i}|"}.join('').gsub('||', '|')+"\n")
  对象.write("#{Array.new(列名.size, '|:-:|').join('').gsub('||', '|')}\n")

  内容.each do |键,值|
    字段 = []
    值.each do |k,v|
      if (File::file?(v) || File::directory?(v))
        字段 << "<a href='#{v}' target='_blank'>#{键}</a>"
      else
        字段 << v
      end
    end

    对象.write(字段.map{|i| "|#{i}|"}.join('').gsub('||', '|')+"\n")
  end

  对象.write("\n\n")
end

遍历生成内容(对象: 文件, 内容: 文件内容)
文件.close()