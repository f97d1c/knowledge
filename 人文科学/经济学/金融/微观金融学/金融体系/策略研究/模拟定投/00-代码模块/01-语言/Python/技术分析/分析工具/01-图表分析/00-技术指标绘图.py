def 技术指标绘图(params):
  前缀 = params['前缀'].upper()
  df = params['df'].copy(deep=True)
  df['year'] = df.index.year

  相关字段 = [col for col in params['df'].columns if col.startswith(f"{前缀}_")]

  if params.get('绘图-追加字段'):
    追加字段 = params['绘图-追加字段']
    相关字段.insert(追加字段[0], 追加字段[1])

  for 年份, 分组数据 in df.groupby('year'):
    相关数据 = {}
    for index, row in 分组数据.iterrows():
      for column in 相关字段:
        相关数据[column] = 相关数据.get(column, [])
        相关数据[column].append({'日期': row['日期'].strftime('%Y-%m-%d'), '数值': row[column]})

    存储路径 = os.path.dirname(f"{回测结果路径}/图片/")
    参数 = {
      '标题': f"{params['指标中文名']}_{前缀}({年份}年)-对比图",
      'X轴': '日期',
      'Y轴': '数值',
      '保存路径': f"{存储路径}/{params['指标中文名']}/{params['指标中文名']}_{前缀}_{年份}.png",
      "数据": 相关数据
    }

    图表_趋势图(参数)