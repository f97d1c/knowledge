#!/bin/bash --login
instance=$(jq -r '.["参数"]["回测实例"]' "$1")
[[ $instance == 'true' ]] && exit 8

source $(find . -type f -name 'Bash.sh')
SCRIPT_PATH=$(readlink -f "${BASH_SOURCE[0]}")
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")
result_path=$(jq -r '.["参数"]["根路径_回测结果"]' "$1")
indices=($(jq -r '.["参数"]["回测指数范围"][]' "$1"))

files=()
for path in $(find "$result_path/参数" -type f -name "*.json" | sort -n); do
  for index in ${indices[@]};do
    if [[ "$path" =~ .*$index.json ]];then
      files+=($path)
    fi
  done
done

if [[ ${#files[@]} -eq 1 ]];then
  for file in ${files[@]}; do
    bash "$SCRIPT_DIR/91-运行单例.sh" $file
    code=$?
    if [[ ! $code -eq 0 ]];then echo '[false, "返回值异常", '$code']' && exit $code;fi
  done
else
  # 使用75%的可用线程并行运行并显示进度条及预计时间,当其中某一线程报错时立即结束运行
  parallel -j 75% --bar --eta --halt now,fail=1 "bash $SCRIPT_DIR/91-运行单例.sh {} > /dev/null 2>&1" ::: ${files[@]}
  code=$?
  if [[ ! $code -eq 0 ]];then echo '[false, "并行运行返回值异常", '$code']' && exit $code;fi
fi

find $SCRIPT_DIR/../ -maxdepth 1 -type d -name "[2-8][0-9]-*" | sort -n | tr '\n' '\0' | while IFS= read -r -d $'\0' path; do
  dirname=${path##*/}
  modulename=${dirname##*-}

  output '开始执行: '$modulename'模块'
  遍历执行脚本 "$path" "$1"
  code=$?
  if [[ ! $code -eq 0 ]];then echo '[false, "'$modulename'模块返回值异常", '$code']' && exit $code;fi
done

code=$?
if [[ ! $code -eq 0 ]];then exit $code;fi

exit 233