def 运行脚本命令(命令: )
  # 使用 IO.popen 执行命令并实时输出
  io = IO.popen(命令)
  pid = io.pid  # 获取进程 ID

  # 逐行读取并打印标准输出
  while (line = io.gets)
    print line
  end

  # 等待进程完成并获取返回码
  _, status = Process.wait2(pid)

  # 获取并输出返回码
  exit_code = status.exitstatus

  exit(exit_code) if exit_code != 0
end