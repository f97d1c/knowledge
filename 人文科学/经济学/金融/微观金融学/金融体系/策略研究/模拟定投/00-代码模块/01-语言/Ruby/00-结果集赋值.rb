def 结果集赋值(key, value)
  原始结果集 = JSON.parse(File.open(ARGV[0]){|f| f.read()})
  @结果集[key] = value
  begin
    File.open(ARGV[0], 'w'){|f| f.write(JSON.pretty_generate(@结果集))}
  rescue => e
    print("数据已回滚: #{e}\n")
    File.open('异常结果集.json', 'w'){|f| f.write(@结果集)}
    File.open(ARGV[0], 'w'){|f| f.write(JSON.pretty_generate(原始结果集))}
    exit(255)
  end
end