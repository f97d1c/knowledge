require_relative '../../../../Ruby.rb'

配置文件地址 = File.join([@回测研报存储路径, "#{@当前回测指数}.json"])
内容 = JSON.parse(File.open(配置文件地址){|f|f.read})

rmd文件路径 = File.join([@回测研报存储路径, "#{@当前回测指数}.rmd"])
rmd文件 = File.open(rmd文件路径, 'w')

模板文件内容 = File.open(Dir["**/头部.rmd"][0]){|f|f.read}
模板文件内容 = 模板文件内容.gsub('{项目根路径}', @当前路径)

内容['参数']['文档'].each do |关键字, 替换内容|
  模板文件内容 = 模板文件内容.gsub("{#{关键字}}", 替换内容)
end
rmd文件.write(模板文件内容)


当前上级标题 = nil
内容['绘制'].each do |键,值|
  上级标题,当前标题 = 键.split('/')
  unless 当前上级标题 == 上级标题
    当前上级标题 = 上级标题
    rmd文件.write("\n\n## #{上级标题}") 
  end

  rmd文件.write("\n\n### #{当前标题}")
  rmd文件.write("\n```{r, echo=FALSE, out.width='100%'}")
  rmd文件.write("\n绘制('#{键}')")
  rmd文件.write("\n```")
  
  if ['概览'].include?(当前上级标题)
    文件路径 = "#{@回测结果路径}/图片/#{当前标题}.png"
    rmd文件.write("\n<img src='#{文件路径}'>") if File.file?(文件路径)
  end

end

rmd文件.close

内容 = File.open(rmd文件路径){|f| f.read}

File.open(rmd文件路径, 'w'){|f| f.write(内容)}