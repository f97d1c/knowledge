require_relative '../../../../Ruby.rb'

配置文件地址 = File.join([@回测研报存储路径, "#{@当前回测指数}.json"])
内容 = JSON.parse(File.open(配置文件地址){|f| f.read})

数据格式 = {
  "标题" => nil,
  "数据格式": ["AHDKV"],
  "时间": "日期",
  "数据" => []
}

@评估指标.keys.each do |属性|
  # 浅拷贝：使用 clone 方法创建的对象与原对象共享相同的实例变量引用，修改嵌套对象会影响原对象。
  # 深拷贝：通过递归复制或使用 Marshal 库创建完全独立的新对象，修改嵌套对象不会影响原对象。
  # 需要对值造成更改的用深拷贝
  值 = Marshal.load(Marshal.dump(数据格式))
  值['标题'] = "#{属性}-对比图"
  
  日期 = @结果集['定投详情'].values.flatten.map(&:values).flatten.map{|i| i['日期']}.uniq

  日期.each do |date|
    数据 = {
      '日期': date
    }

    @结果集['定投详情'].each do |模型名, 记录|
      数据[模型名] = 记录[date][属性]
    end

    值['数据'] << 数据
  end

  内容['绘制']["概览/#{属性}"] = 值
end

File.open(配置文件地址, 'w'){|f| f.write(JSON.pretty_generate(内容))}