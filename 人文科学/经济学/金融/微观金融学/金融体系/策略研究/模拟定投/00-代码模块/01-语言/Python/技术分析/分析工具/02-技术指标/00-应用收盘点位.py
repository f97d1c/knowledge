import itertools

def ta_close(params):
  前缀 = params['前缀'].upper()
  时间窗口字段 = [col for col in params['df'].columns if col.startswith(前缀)]

  # 添加的简单移动平均线
  if not 时间窗口字段:
    for 名称, 数组 in params['时间窗口长度'].items():
      for 长度 in 数组:
        result = params['方法'](params['df']['收盘点位'], window=长度)
        # 保留四位小数
        params['df'][f'{前缀}_{名称}_{长度}'] = result.round(4)

    时间窗口字段 = [col for col in params['df'].columns if col.startswith(前缀)]

  return [True, {'时间窗口字段': 时间窗口字段}]

def 指标信号整体写入(params):
  前缀 = params['前缀'].upper()

  for 时间窗口类型 in params['时间窗口长度'].keys():
    if 时间窗口类型 in ['短期', '中期', '长期']:
      pass
    else:
      print(f"异常时间窗口类型: {时间窗口类型}")
      sys.exit(255)

    相关字段 = [col for col in params['df'].columns if col.startswith(f"{前缀}_{时间窗口类型}_")]

    for 字段 in sorted(相关字段):
      无效字段值 = ['null', None]
      信号名 = f"信号_{字段}"
      params['df'][信号名] = '未知'

      params['df'].loc[(params['df'][字段].isna()), 信号名] = None
      params['df'].loc[(params['df'][字段] == None), 信号名] = None
      params['df'].loc[(params['df'][字段] == 'null'), 信号名] = None

      对象 = params['df'].query(f"{字段} not in @无效字段值")
      yield {'对象': 对象, '字段': 字段, '信号名': 信号名}
      params['df'].update(对象)
  
  for index, row in params['df'].iterrows():
    for 时间窗口类型 in params['时间窗口长度'].keys():
      signals = 趋势判断({'对象': params['df'], '行': row, '前缀': 前缀, '时间窗口类型': 时间窗口类型})
      params['df'].loc[index, f"信号_{前缀}_{时间窗口类型}"] = ('/'.join(signals) or None)

    signals = 趋势判断({'对象': params['df'], '行': row, '前缀': 前缀})
    params['df'].loc[index, f"信号_{前缀}"] = ('/'.join(signals) or None)

def 趋势判断_短期强于长期(params):
  相关字段 = [col for col in params['对象'].columns if col.startswith(f"{params['前缀']}_")]
  相关字段.sort(key=根据字段后缀数字排序)
  数值 = np.array([params['行'][col] for col in 相关字段])
  相关数值 = 数值[~np.isnan(数值)]
  判断结果 = [x > y for x, y in zip(相关数值, 相关数值[1:])]

  if (False in 判断结果):
    params['信号'].remove(params['误判信号'])
    if params['信号'] == []:
      params['信号'] = ['横盘']

  return params['信号']

def 趋势判断_MA(params):
  params['误判信号'] = '上行'
  return 趋势判断_短期强于长期(params)

def 趋势判断_RSI(params):
  params['误判信号'] = '多头'
  return 趋势判断_短期强于长期(params)

def 趋势判断_ROC(params):
  params['误判信号'] = '上升'
  return 趋势判断_短期强于长期(params)

def 趋势判断(params):
  对象 = params.get('对象')
  行 = params.get('行')
  前缀 = params.get('前缀')
  时间窗口类型 = params.get('时间窗口类型')

  if 时间窗口类型:
    result = [v for k,v in 行.items() if (f"信号_{前缀}_{时间窗口类型}_" in k) and v]
    signals = set(result)
  else:
    result = [v.split('/') for k,v in 行.items() if (f"信号_{前缀}_" in k) and v]
    signals = set(itertools.chain.from_iterable(result))

  signals = set(filter(lambda x:x, signals))
  signals = sorted(signals)

  params['信号'] = signals

  if ('上行' in signals) and ('MA' in 前缀):
    return 趋势判断_MA(params)
  elif ('多头' in signals) and ('RSI' in 前缀):
    return 趋势判断_RSI(params)
  elif ('上升' in signals) and ('ROC' in 前缀):
    return 趋势判断_ROC(params)
  else:
    return signals

def 根据字段后缀数字排序(s):
    match = re.search(r'(\d+)$', s)  # 匹配字符串末尾的数字
    return int(match.group(1)) if match else float('inf')  # 如果没有匹配到数字，默认值为无穷大