require_relative '../../../../Ruby.rb'

配置文件地址 = File.join([@回测研报存储路径, "#{@当前回测指数}.json"])
内容 = JSON.parse(File.open(配置文件地址){|f| f.read})

值 = {
  "标题" => "PB-对比图",
  "数据格式": ["AHDKV"],
  "时间": "日期",
  "数据" => []
}

@结果集['定投详情'].values.flatten.map(&:values).flatten.map{|i| i['日期']}.uniq.each do |date|
  数据 = {
    '日期': date
  }

  当日记录 = @结果集['样本数据'][date].select{|k,v| k =~ /^PB.*/}
  exit(255) unless 当日记录

  数据.merge!(当日记录)
  值['数据'] << 数据
end

内容['绘制']["概览/PB"] = 值

File.open(配置文件地址, 'w'){|f| f.write(JSON.pretty_generate(内容))}