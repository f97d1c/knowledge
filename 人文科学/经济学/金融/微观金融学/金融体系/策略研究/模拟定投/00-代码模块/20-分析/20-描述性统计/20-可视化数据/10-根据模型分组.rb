require_relative '../../../Ruby.rb'

# 生成参数文件
内容格式 = {
  "绘制" => {
  },
  "参数" => {
    "文档转换" => {
      "路径"=>"",
      "输出格式" => [
        "html_document"
      ],
      "输出路径"=>""
    },
    "文档" => {
      "标题" => "#{文件名(__FILE__)}分析报告"
    }
  }
}

Dir["#{@分析结果_基础数据}/**/#{文件名(__FILE__)}.json"].each do |数据文件|
  内容 = Marshal.load(Marshal.dump(内容格式))
  内容['参数']['文档转换']['路径'] = File.join(@分析结果_可视化数据, "#{文件名(__FILE__)}.rmd")
  内容['参数']['文档转换']['输出路径'] = File.join(@分析结果_可视化数据, "#{文件名(__FILE__)}.html")

  File.open(数据文件){|f|JSON.parse(f.read)}.each do |模型名, 指标集合|
    指标集合.each do |指标名, 哈希|
      内容['绘制']["#{模型名}/#{指标名}"] = {
        "标题": "#{指标名}-对比图",
        "数据格式": [
          "AHDKV"
        ],
        "时间": "日期",
        "数据": 哈希.map{|k,v| {'日期': k}.merge!(v)}
      }
    end
  end

  写入文件(路径: File.join(@分析结果_可视化数据, "#{文件名(__FILE__)}.json"), 内容: 内容)
end

# 生成rmd文件
Dir["#{@分析结果_可视化数据}/**/#{文件名(__FILE__)}.json"].each do |路径|
  生成RMD文件(参数路径: 路径)
end

# 绘制
Dir["#{@分析结果_可视化数据}/**/#{文件名(__FILE__)}.json"].each do |路径|
  运行脚本命令(命令: "Rscript $(find $PWD -type f -name 'Rmarkdown.r') '#{路径}'")
end