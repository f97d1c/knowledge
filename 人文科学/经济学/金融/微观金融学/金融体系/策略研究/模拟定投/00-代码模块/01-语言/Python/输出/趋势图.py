# pip install matplotlib

# 中文字体缺失解决:
# sudo apt-get update
# sudo apt-get install ttf-wqy-zenhei
import glob
import hashlib
from PIL import Image
from datetime import datetime
import matplotlib.pyplot as plt

def 图表_趋势图_md5(params):
    md5_content = []
    for 标签, 数据 in params.get('数据').items():
        xlabels = [datetime.strptime(item[params.get('X轴')], "%Y-%m-%d") for item in 数据]
        yvalues = [item[params.get('Y轴')] for item in 数据]
        md5_content.append(xlabels)
        md5_content.append(yvalues)

    md5_content.append(params.get('标题'))
    md5_content.append(params.get('版本号'))
    md5_contents = ','.join(map(str, md5_content)).encode('utf-8')
    md5_hash = hashlib.md5(md5_contents).hexdigest()
    params['md5_hash'] = md5_hash
    return params

plt.rcParams['font.sans-serif'] = ['WenQuanYi Zen Hei']  # 使用 WenQuanYi Zen Hei
plt.rcParams['axes.unicode_minus'] = False  # 解决负号显示问题

图表_标记样式 = {
    '.' : '点',
    ',' : '像素',
    'o' : '圆圈',
    'v' : '向下三角形',
    '^' : '向上三角形',
    '<' : '向左三角形',
    '>' : '向右三角形',
    '1' : '三脚朝下三角形',
    '2' : '三脚朝上三角形',
    '3' : '三脚朝左三角形',
    '4' : '三脚朝右三角形',
    's' : '正方形',
    'p' : '五边形',
    '*' : '星号',
    'h' : '六边形1',
    'H' : '六边形2',
    '+' : '加号',
    'x' : '叉号',
    'D' : '菱形',
    'd' : '细菱形',
    '|' : '垂直线',
    '_' : '水平线',
}

def 图表_趋势图(params):
    params['版本号'] = params.get('版本号', '202412170958') # 当进行较大调整希望全部保存图片重新生成时调整该值
    if not params.get('md5_hash', False):
        图表_趋势图_md5(params)

    保存路径 = params.get('保存路径')
    目录名 = os.path.dirname(保存路径)
    文件名, 后缀 = os.path.splitext(os.path.basename(保存路径))
    保存路径_md5 = f"{目录名}/.{文件名}_{params.get('md5_hash')}{后缀}"
    点位标记 = params.get('点位标记', False)


    if not os.path.exists(目录名):
        os.makedirs(目录名)

    if os.path.exists(保存路径_md5): 
        if not os.path.exists(保存路径): 
            os.link(保存路径_md5, 保存路径)

        # 验证图片是否完整
        try:
            with Image.open(保存路径) as img:
                img.verify()
            return True
        except Exception as e:
            print(f"[完整性校验未通过]: {保存路径}({e})")

    if os.path.exists(保存路径): os.remove(保存路径)
    for file in glob.glob(f"{目录名}/.{文件名}_*"):
        os.remove(file)
    
    图表标点 = list(图表_标记样式.copy().keys())

    结果集数量 = [len(values) for values in params.get('数据').values()]

    # 根据数据点的数量动态调整图形的大小
    fig_width = max(30, max(结果集数量) * 0.25)
    fig_width = min(fig_width, 90)
    fig_height = fig_width / 2

    文字颜色 = '#38f30c'

    # 创建图形
    fig, ax = plt.subplots(figsize=(fig_width, fig_height))

    # 设置背景颜色
    ax.set_facecolor('black')
    fig.patch.set_facecolor('black')

    # 设置标题和标签
    plt.title(params.get('标题'), color=文字颜色)
    plt.xlabel(params.get('X轴'), color=文字颜色)
    plt.ylabel(params.get('Y轴'), color=文字颜色)

    for 标签, 数据 in params.get('数据').items():
        xlabels = [datetime.strptime(item[params.get('X轴')], "%Y-%m-%d") for item in 数据]
        yvalues = [item[params.get('Y轴')] for item in 数据]

        # 绘制折线图
        plt.plot(xlabels, yvalues, marker=(图表标点.pop() if 点位标记 else None), label=标签)

    # 格式化日期显示
    plt.gca().xaxis.set_major_formatter(plt.matplotlib.dates.DateFormatter('%Y-%m'))

    # 显示网格
    plt.grid(True, linestyle='--', alpha=0.7, color=文字颜色)
    
    # 设置刻度颜色
    ax.tick_params(axis='x', colors=文字颜色)
    ax.tick_params(axis='y', colors=文字颜色)

    # 自动调整日期标签以避免重叠
    plt.gcf().autofmt_xdate()

    # 添加图例，并设置背景颜色
    legend = plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), fancybox=True, shadow=True, ncol=2)
    frame = legend.get_frame()
    frame.set_facecolor('black')  # 设置背景颜色为浅灰色
    frame.set_edgecolor('black')  # 设置边框颜色为黑色

        
    # 遍历图例中的所有文本对象，并设置字体颜色
    for text in legend.get_texts():
        text.set_color(文字颜色)  # 将字体颜色设置为蓝色

    # 保存图形到文件
    plt.savefig(保存路径_md5)
    os.link(保存路径_md5, 保存路径)
    # os.symlink(保存路径_md5, 保存路径) # 软链接可以减少存储 但是读取存在问题 后期优化
    plt.close(fig)
