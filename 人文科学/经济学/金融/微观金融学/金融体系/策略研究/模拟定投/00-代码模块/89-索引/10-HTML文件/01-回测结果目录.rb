require_relative '../../Ruby.rb'

删减关键字 = [@项目根路径, '(\/)[0-9]{2}\-', '回测结果', '研报', '可视化数据']
过滤结果 = Dir["#{@项目根路径}/**/回测结果/*"]
配置文件 = "#{@项目根路径}/索引.json"

def 遍历赋值(对象:, 键:, 值:)
  unless 键.size == 1
    对象 = 对象[键.delete_at(0)] ||= {}
    return 遍历赋值(对象: 对象, 键: 键, 值: 值)
  end
  
  if 对象[键[0]]
    binding.pry
    puts '[false, "存在覆盖风险", "'+键+'"]'
    exit(255)
  end

  对象[键[0]] = 值
end

结果 = {}
过滤结果.sort.each do |路径| 
  删减结果 = 路径.gsub(Regexp.new(删减关键字.join('|')), '\1').split('/').delete_if{|i| i == ''}
  遍历赋值(对象: 结果, 键: 删减结果, 值: [['目录', 路径.gsub(@项目根路径, '.')]])
end

内容 = File.open(配置文件){|f|JSON.parse(f.read)}
内容['回测结果目录'] = 结果
File.open(配置文件, 'w'){|f| f.write(JSON.pretty_generate(内容))}