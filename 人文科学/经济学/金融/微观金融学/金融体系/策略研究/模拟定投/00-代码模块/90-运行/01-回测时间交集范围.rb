require_relative '../Ruby.rb'
exit(8) if @回测实例

结果 = {
  '最早' => [],
  '最晚' => [],
}

Dir[@回测数据路径].each do |文件路径|
  next unless @回测指数范围.map{|i| 文件路径.include?(i)}.include?(true)
  日期 = []

  CSV.foreach(文件路径, headers: true) do |row|
    next if row['日期'] == nil
    日期 << row['日期']
  end

  next if 日期 == []

  日期 = 日期.map{|date|Date.parse(date)}
  结果['最晚'] << 日期.max
  结果['最早'] << 日期.min
end

@结果集['参数']['回测时间交集范围'] = [结果['最早'].max.to_s, 结果['最晚'].min.to_s]

结果集赋值('参数', @结果集['参数'])