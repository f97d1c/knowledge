def technical_analysis_rsi(params):
  params['前缀'] = 'rsi'
  params['方法'] = ta.momentum.rsi
  params['指标中文名'] = '相对强弱指数'

  res = ta_close(params)

  for result in 指标信号整体写入(params):
    对象 = result.get('对象')
    字段 = result.get('字段')
    信号名 = result.get('信号名')

    # 判断标准
    # TODO: 30 70 不是不变的 对于一些波动大的可以设置在 20 80等
    对象.loc[(对象[字段] > 70), 信号名] = '超买'
    对象.loc[(对象[字段] < 30), 信号名] = '超卖'
    对象.loc[(30 < 对象[字段]) & (对象[字段] < 50), 信号名] = '空头'
    对象.loc[(50 < 对象[字段]) & (对象[字段] < 70), 信号名] = '多头'

  for 名称, 数组 in params['时间窗口长度'].items():
    for 长度 in 数组:
      指标字段名 = f"RSI_{名称}_{长度}"

      for prefix in ['顶背离', '底背离']:
        背离字段名 = f"{prefix}_{指标字段名}"
        params['df'][背离字段名] = '未知'
        params['df'].loc[(params['df'][指标字段名].isna()), 背离字段名] = None
        params['df'].loc[(params['df'][指标字段名] == None), 背离字段名] = None
        params['df'].loc[(params['df'][指标字段名] == 'null'), 背离字段名] = None

        for index, row in params['df'].head(长度).iterrows():
          params['df'].loc[index, 背离字段名] = None

      window = 长度
      for i in range(window, len(params['df'])):
        # 检测顶背离
        if all([
          params['df']['收盘点位'].iloc[i] > params['df']['收盘点位'].iloc[i-window:i].max(),  # 价格创新高
          params['df'][指标字段名].iloc[i] < params['df'][指标字段名].iloc[i-window:i].max()  # 指标未创新高
        ]):
          flag = True
        else:
          flag = False

        params['df'].at[params['df'].index[i], f"顶背离_{指标字段名}"] = flag

        # 检测底背离
        if all([
          params['df']['收盘点位'].iloc[i] < params['df']['收盘点位'].iloc[i-window:i].min(),  # 价格创新低
          params['df'][指标字段名].iloc[i] > params['df'][指标字段名].iloc[i-window:i].min()  # 指标未创新低
        ]):
          flag = True
        else:
          flag = False

        params['df'].at[params['df'].index[i], f"底背离_{指标字段名}"] = flag

  技术指标绘图(params)
  return [True, '']