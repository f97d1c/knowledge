def 自然语句判断(变量:, 条件:, 结果: {})
  判断变量 = Marshal.load(Marshal.dump(变量))
  判断条件 = [Marshal.load(Marshal.dump(条件))].flatten

  关系运算符 = {
    '大于' => '>',
    '等于' => '==',
    '小于' => '<',
  }
  判断变量.merge!(关系运算符)

  判断条件.each do |自然语句|
    # 元素根据长度排序避免出现 PB等权在PB等权-历史均值 前面 错误匹配情况
    表达式 = 自然语句.gsub(/#{判断变量.keys.sort{|a, b| b.length <=> a.length}.map{|k| Regexp.escape(k)}.join('|')}/) do |match|
      判断变量[match]
    end

    表达式 = 表达式.gsub('@', '')

    unless 表达式.match(/^[a-z \d \s \. \( \) \+ \- \* \/ \> \< \=]+$/)
      print "#{表达式} 未通过校验\n"
      exit(255)
    end

    结果[表达式] = eval(表达式)
  end

  [!结果.values.include?(false), 结果]
end