require_relative '../../Ruby.rb'

过滤关键字 = ['.*代码模块.*', '索引']
过滤正则 = Regexp.new(过滤关键字.join('|'))
过滤结果 = Dir["#{@项目根路径}/**/*.html"].select{|i| !(i=~ 过滤正则)}.sort
删减关键字 = [@项目根路径, '(\/)[0-9]{2}\-', '回测结果', '研报', '可视化数据']
配置文件 = "#{@项目根路径}/索引.json"

def 文件大小(size)
  units = %w[B KB MB GB TB]
  unit_index = 0

  while size >= 1024 && unit_index < units.length - 1
    size /= 1024.0
    unit_index += 1
  end

  "%.2f %s" % [size, units[unit_index]]
end

def 遍历赋值(对象:, 键:, 值:)
  unless 键.size == 1
    对象 = 对象[键.delete_at(0)] ||= {}
    return 遍历赋值(对象: 对象, 键: 键, 值: 值)
  end
  
  if 对象[键[0]]
    puts '[false, "存在覆盖风险", "'+键+'"]'
    binding.pry
    exit(255)
  end

  对象[键[0]] = 值
end

结果 = {}
过滤结果.each do |路径| 
  删减结果 = 路径.gsub(Regexp.new(删减关键字.join('|')), '\1').split('/').delete_if{|i| i == ''}
  遍历赋值(对象: 结果, 键: 删减结果[0...-1], 值: [['文件', 路径.gsub(@项目根路径, '.')], ['大小', 文件大小(File.size(路径))]])
end

内容 = File.open(配置文件){|f|JSON.parse(f.read)}
内容['研报'] = 结果
File.open(配置文件, 'w'){|f| f.write(JSON.pretty_generate(内容))}