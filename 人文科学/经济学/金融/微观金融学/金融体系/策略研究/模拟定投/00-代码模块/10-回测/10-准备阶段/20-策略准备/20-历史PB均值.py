from __init__ import *

df = pd.DataFrame.from_dict(结果集['样本数据'], orient='index')
df.index = pd.to_datetime(df.index)
df.sort_index(inplace=True)

df['日期'] = pd.to_datetime(df['日期'])

for index, row in df.iterrows():
  筛选后样本数据 = df[(df['日期'] <= row['日期'])]
  市净率均值 = 筛选后样本数据['PB等权'].mean().round(4)
  df.loc[index, 'PB等权-历史均值'] = 市净率均值

df.index = df.index.strftime('%Y-%m-%d')
结果集赋值('样本数据', df.to_dict(orient='index'))