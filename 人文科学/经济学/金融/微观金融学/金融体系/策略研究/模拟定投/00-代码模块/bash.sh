#!/bin/bash --login

SCRIPT_PATH=$(readlink -f "${BASH_SOURCE[0]}")
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")

source $(find . -type f -name 'Bash.sh')

rm -f $SCRIPT_DIR/Python.py && touch $SCRIPT_DIR/Python.py

find $SCRIPT_DIR/**/Python -type f -name "*.py" | sort -n | tr '\n' '\0' | while IFS= read -r -d $'\0' path; do
  cat "$path" >> $SCRIPT_DIR/Python.py
  echo '' >> $SCRIPT_DIR/Python.py
done

find . -type f -name "__init__.py" | sort -n | tr '\n' '\0' | while IFS= read -r -d $'\0' path; do
  cp $SCRIPT_DIR/Python.py  $path
done


if [ "$1" == 'test' ];then
  file=$PWD'/10-样本内测试/00-基准数据/参数.json'
  # file=$PWD'/10-样本内测试/00-基准数据/回测结果/参数/上证指数.json'
  bash $SCRIPT_DIR/02-测试/test.sh $file
  ruby $SCRIPT_DIR/02-测试/test.rb $file
  python -B $SCRIPT_DIR/02-测试/test.py $file
  Rscript $SCRIPT_DIR/02-测试/test.r $file
  # exit 0
elif [ "$1" == '1' ];then
  file=$PWD'/10-样本内测试/10-基准回测时间范围内/参数.json'
elif [ "$1" == '2' ];then
  file=$PWD'/10-样本内测试/20-回测时间交集范围内/参数.json'
fi

files=()
if [ $# -eq 0 ]; then
  while IFS= read -r -d '' file; do
    files+=("$file")
  done < <(find . -type f -name '参数.json' | sort -n | tr '\n' '\0' )
else
  files+=($file)
fi

for file in "${files[@]}"; do
  遍历执行脚本 "$SCRIPT_DIR/90-运行" $file
  if [[ $? -ne 0 ]]; then exit $code; fi
done