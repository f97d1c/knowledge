#!/usr/bin/env ruby

@当前路径 = File.expand_path(File.dirname(__FILE__))
@项目根路径 = File.dirname(File.dirname(__FILE__))

requires = {
  "断点调试(binding)"  => "pry",
  # "Http请求"         => "rest-client",
  "JSON处理"           => "json",
  # "文字转拼音"        => "ruby-pinyin",
  # "Html文本解析"      => "nokogiri",
  "文件系统工具"        => "fileutils",
  "uri"                => "uri",
  "base64"             => "base64",
  "日期"               => "date",
  "CSV库"              => "csv",
  # "CGI接口实现"       => "cgi", # 主要用于链接内容转义
  # gruff的默认字体不支持中文
  # 依赖ImageMagick: sudo apt-get install -fy libmagickwand-dev
  # "绘制图表"          => "gruff",
  # '生成图像'          => 'rmagick',
  # '后台任务队列系统'   => 'resque',
  # '终端工具'          => 'irb',
  # '命令行选项分析'     => 'optparse',
  # 'JSON转Ruby对象'    => 'ostruct',
  # '格式化打印JSON数据' => 'awesome_print',
}

requires.each do |desc, gem_name|
  begin
    require gem_name
  rescue LoadError => e
    puts "缺少Gem: #{gem_name}(用于#{desc}), 正在尝试安装该Gem."
    `gem install #{gem_name}`
    断点调试(binding, 备注: "#{gem_name}安装失败") unless $?.exitstatus == 0
  end
end

@结果集 = JSON.parse(File.open(ARGV[0]){|f| f.read()})

(@结果集['参数']||={}).each do |键,值|
  instance_variable_set("@#{键}", 值)
end

Dir["#{@当前路径}/**/Ruby/**/*.rb"].sort.each do |路径|
  require_relative 路径
end

def 文件名(file)
  name = File.basename(file).split('.')[0]
  name.gsub(/^\d{2,}\-/, '')
end

def 写入文件(路径:, 内容:)
  FileUtils.mkdir_p(File.dirname(路径)) unless File.directory?(File.dirname(路径))
  File.open(路径, 'w'){|f| f.write(JSON.pretty_generate(内容))}
end

def 格式化(数字:)
  sprintf('%.2f', 数字).gsub(/\B(?=(\d{3})+(?!\d))/, ',')
end