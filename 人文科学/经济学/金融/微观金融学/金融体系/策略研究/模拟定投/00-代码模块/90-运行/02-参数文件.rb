require_relative '../Ruby.rb'
exit(8) if @回测实例

存储路径 = File.join(@根路径_回测结果, '参数')
FileUtils.mkdir_p(存储路径) unless File.directory?(存储路径)

@回测指数范围.each do |回测指数|
  结果集 = {
    '参数' => Marshal.load(Marshal.dump(@结果集['参数']))
  }

  结果集['参数']['回测实例'] = true
  结果集['参数']['当前回测指数'] = 回测指数

  当前回测数据文件 = Dir[@回测数据路径].select{|f| f.include?(回测指数)}.sort.last
  if !!! 当前回测数据文件
    puts '[false, "未找到数据文件"]'
    exit(255)
  end
  结果集['参数']['当前回测数据文件'] = 当前回测数据文件

  File.open(File.join(存储路径, "#{回测指数}.json"), 'w'){|f| f.write(JSON.pretty_generate(结果集))}
end