import os
import re
import sys
import copy
import json
import numpy as np
import pandas as pd
import akshare as ak # pip install akshare

# 不知道有什么用 但是可以消除df.replace的警告
# 设置 Pandas 选项以启用未来行为
pd.set_option('future.no_silent_downcasting', True)

with open(sys.argv[1], 'r', encoding='utf-8') as file:
  try:
    结果集 = json.load(file)
  except Exception as e:
    print("加载错误:", e)
    sys.exit(255)

# 动态设置运行变量
for key, value in 结果集['参数'].items():
    locals()[key] = value


# 自定义编码器
class MyJSONEncoder(json.JSONEncoder):
  def default(self, obj):
    # 将Timestamp 能够正确地转换为 JSON 格式的字符串
    if isinstance(obj, pd.Timestamp):
      hour, minute, second = obj.hour, obj.minute, obj.second
      if (hour == 0 and minute == 0 and second == 0):
        return obj.strftime('%Y-%m-%d')
      else: 
        return obj.strftime('%Y-%m-%d %H:%M:%S')
       
    return super(MyJSONEncoder, self).default(obj)


# 通用方法
def 结果集赋值(key, value):
  with open(sys.argv[1], 'r', encoding='utf-8') as file:
    原始结果集 = json.load(file)

  if isinstance(value, pd.DataFrame):
    # value = value.fillna('null')
    value = value.replace({np.nan: 'null', None: 'null'})

    # 不要用reset_index 在设置索引时不删除原字段
    # df.set_index('日期', inplace=True, drop=False)
    # 结果集[key] = value.reset_index().to_dict('records')
    结果集[key] = value.to_dict('records')
  else:
    结果集[key] = value

  with open(sys.argv[1], 'w') as f:
    try:
      f.write(json.dumps(结果集, indent=2, ensure_ascii=False, cls=MyJSONEncoder))
    except Exception as e:
      print(f"数据已回滚: {e}")
      f.write(json.dumps(原始结果集, indent=2, ensure_ascii=False))
      sys.exit(255)