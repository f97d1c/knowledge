require_relative '../../../Ruby.rb'

排序结果 = Dir["#{@根路径_回测结果}/参数/**.json"].sort_by do |i|
  对应指数 = @回测指数范围.select{|名称| i.include?(名称)}[0]
  @回测指数范围.index(对应指数) || @回测指数范围.size
end

结果集 = {}

排序结果.each do |回测指数|
  指数名称 = 回测指数.split('/')[-1].split('.')[0]
  内容 = File.open(回测指数){|f| JSON.parse(f.read)}
  内容['定投详情'].each do |模型名, 记录|
    @评估指标.keys.each do |指标名|
      记录.each do |日期, 详情|
        存储单元 = ((结果集[模型名]||={})[指标名]||={})[详情['日期']]||={}
        存储单元[指数名称] = 详情[指标名]
      end
    end
  end
end

存储路径 = File.join(@分析结果_基础数据)
FileUtils.mkdir_p(存储路径) unless File.directory?(存储路径)

File.open(File.join(存储路径, "#{文件名(__FILE__)}.json"), 'w'){|f| f.write(JSON.pretty_generate(结果集))}