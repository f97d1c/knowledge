from __init__ import *
if not 结果集.get("样本数据"): sys.exit(255)

df = pd.DataFrame(结果集['样本数据'])

df['日期'] = pd.to_datetime(df['日期'])
df.set_index('日期', inplace=True, drop=False)
日期筛选后数据 = df[(df['日期'] >= 回测开始时间) & (df['日期'] <= 回测结束时间)]

结果集赋值('样本数据', 日期筛选后数据)