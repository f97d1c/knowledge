#!/bin/bash --login
result_path=$(jq -r '.["参数"]["回测研报存储路径"]' "$1")
current_index=$(jq -r '.["参数"]["当前回测指数"]' "$1")

Rscript $(find $PWD -type f -name 'Rmarkdown.r') "$result_path/$current_index.json"
code=$?
if [[ ! $code -eq 0 ]];then echo '[false, "返回码异常", '$code']' && exit $code;fi