require_relative '../../../../Ruby.rb'

配置文件地址 = File.join([@回测研报存储路径, "#{@当前回测指数}.json"])
内容 = JSON.parse(File.open(配置文件地址){|f| f.read})

数据结构 = {
  "数据格式": ["AKV"],
  "分页条数": 10,
  "日期字段": ["日期"],
  "数据" => []
}

@结果集['定投详情'].each do |模型名, 记录|
  值 = Marshal.load(Marshal.dump(数据结构))
  展示字段 = ['日期', '买卖价格', '买卖股数', '买卖金额', @评估指标.keys].flatten
  值['数据'] = 记录.map do |日期, 单条记录|
    展示字段.map do |字段|
      [字段, 单条记录[字段]]
    end.to_h
  end

  内容['绘制']["列表/#{模型名}"] = 值
end

File.open(配置文件地址, 'w'){|f| f.write(JSON.pretty_generate(内容))}