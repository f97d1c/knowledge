from __init__ import *
# if 结果集.get("样本数据"): sys.exit(8)
if '当前回测数据文件' not in vars(): sys.exit(255)

df = pd.read_csv(当前回测数据文件)
df['日期'] = pd.to_datetime(df['日期'])
df.set_index('日期', inplace=True, drop=False)
df.sort_index(inplace=True)

格式化数据 = []
for 下标, item in df.iterrows():
  格式化数据.append({
    '日期': item['日期'],
    '收盘点位': item['收盘点位'],
    'PB等权': item['PB等权'],
  })

结果集赋值('样本数据', 格式化数据)