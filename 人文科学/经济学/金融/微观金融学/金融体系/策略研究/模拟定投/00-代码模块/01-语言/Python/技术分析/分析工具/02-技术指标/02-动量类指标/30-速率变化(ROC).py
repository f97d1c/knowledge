def technical_analysis_roc(params):
  前缀 = 'roc'
  params['前缀'] = 前缀
  params['方法'] = ta.momentum.roc
  params['指标中文名'] = '速率变化'

  res = ta_close(params)

  for result in 指标信号整体写入(params):
    对象 = result.get('对象')
    字段 = result.get('字段')
    信号名 = result.get('信号名')

    # 判断标准
    对象.loc[(对象[字段] >= 10), 信号名] = '超买'
    对象.loc[(0 < 对象[字段]) & (对象[字段] < 10), 信号名] = '上升'

    对象.loc[(对象[字段] <= -10), 信号名] = '超卖'
    对象.loc[(-10 < 对象[字段]) & (对象[字段] <= 0), 信号名] = '下降'

    对象.loc[(对象[字段].shift(1) < 0) & (对象[字段] >= 0), 信号名] = '买入'  # 零线交叉买入
    对象.loc[(对象[字段].shift(1) > 0) & (对象[字段] <= 0), 信号名] = '卖出'  # 零线交叉卖出

  技术指标绘图(params)
  return [True, '']