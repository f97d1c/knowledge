from __init__ import *
if not 结果集.get("定投策略"): sys.exit(255)
if not 结果集.get("样本数据"): sys.exit(255)

df = pd.DataFrame.from_dict(结果集['样本数据'], orient='index')
df.index = pd.to_datetime(df.index)
df.sort_index(inplace=True)

for 模型名,策略 in 结果集['定投策略'].items():
  if 策略['怎么买']['定投日'] == '每月最后一个交易日':
    # 这种写法会出现索引值会有03-29变为03-31
    # 每月最后一天 = df.resample('ME').last()
    每月最后一天 = df.groupby(df.index.to_period('M')).tail(1)
    结果集['定投详情'][模型名] = {index.strftime('%Y-%m-%d'):{'日期': row['日期']} for index, row in 每月最后一天.iterrows()}

结果集赋值('定投详情', 结果集['定投详情'])
