import ta
import sys
import copy
import inspect
import pandas as pd

def technical_analysis(params):
  df = pd.DataFrame.from_dict(结果集['样本数据'], orient='index')
  df.index = pd.to_datetime(df.index)
  df.sort_index(inplace=True)
  df['日期'] = pd.to_datetime(df['日期'])

  params['时间窗口长度'] = {
    '短期': [5, 10, 20],
    '中期': [50],
    '长期': [100, 200],
  }

  # 信号字段 = [col for col in df.columns if col == '信号']
  # if 信号字段: return [True, '']

  for name, method in inspect.getmembers(sys.modules[__name__], predicate=inspect.isfunction):
    if (not name.startswith('technical_analysis_')): continue
    argv = copy.deepcopy(params)
    argv['df'] = df
    method(argv)

  for index, row in df.iterrows():
    temp = []
    for 时间窗口类型 in params['时间窗口长度'].keys():
      signals = [v.split('/') for k,v in row.items() if re.match(f"^信号_[A-Z]+_{时间窗口类型}$", k) and v]
      signals = set(itertools.chain.from_iterable(signals))
      signals = set(filter(lambda x: x, signals))
      signal = ('/'.join(sorted(signals)) or None)
      temp.append(signal)
      df.loc[index, f"信号_{时间窗口类型}"] = signal

    signals = [i.split('/') for i in temp if i]
    signals = set(itertools.chain.from_iterable(signals))
    signals = set(filter(lambda x:x, signals))
    df.loc[index, f"信号"] = ('/'.join(sorted(signals)) or None)

  df.index = df.index.strftime('%Y-%m-%d')
  df['日期'] = df['日期'].dt.strftime('%Y-%m-%d')
  json_str = df.to_json(orient='index', force_ascii=False, date_format='iso', lines=False, default_handler=str)
  结果集赋值('样本数据', json.loads(json_str))
  return [True, '']