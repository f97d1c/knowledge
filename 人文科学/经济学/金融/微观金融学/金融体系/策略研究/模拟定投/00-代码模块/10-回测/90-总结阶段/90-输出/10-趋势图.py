from __init__ import *
if not 结果集.get("定投详情"): sys.exit(255)
if not 结果集.get("样本数据"): sys.exit(255)

定投详情 = list(结果集.get("定投详情").values())
定投详情_键 = list(定投详情[-1].values())
定投详情_键 = list(定投详情_键[0].keys())

附加值 = 定投详情_键

跳过值 = ['买卖价格', '日期']

存储路径 = os.path.dirname("{a}/图片/".format(a=回测结果路径))
if not os.path.exists(存储路径):
  os.makedirs(存储路径)

for 键 in 附加值:
  if 键 in 跳过值: continue

  Y轴 = 键
  参数 = {
    '标题': Y轴+'-对比图',
    'X轴': '日期',
    'Y轴': Y轴,
    '保存路径': "{a}/{b}.png".format(a=存储路径, b=Y轴),
    "数据": {模型名:list(详情.values()) for 模型名, 详情 in 结果集.get("定投详情").items()}
  }
  图表_趋势图(参数)