import ta
import sys
import copy
import inspect
import pandas as pd

def technical_analysis(params):
  df = pd.DataFrame.from_dict(结果集['样本数据'], orient='index')
  df.index = pd.to_datetime(df.index)
  df.sort_index(inplace=True)
  df['日期'] = pd.to_datetime(df['日期'])

  params['时间窗口长度'] = {
    '短期': [5, 10, 20],
    '中期': [50],
    '长期': [100, 200],
  }

  # 信号字段 = [col for col in df.columns if col == '信号']
  # if 信号字段: return [True, '']

  for name, method in inspect.getmembers(sys.modules[__name__], predicate=inspect.isfunction):
    if (not name.startswith('technical_analysis_')): continue
    argv = copy.deepcopy(params)
    argv['df'] = df
    method(argv)

  for index, row in df.iterrows():
    temp = []
    for 时间窗口类型 in params['时间窗口长度'].keys():
      signals = [v.split('/') for k,v in row.items() if re.match(f"^信号_[A-Z]+_{时间窗口类型}$", k) and v]
      signals = set(itertools.chain.from_iterable(signals))
      signals = set(filter(lambda x: x, signals))
      signal = ('/'.join(sorted(signals)) or None)
      temp.append(signal)
      df.loc[index, f"信号_{时间窗口类型}"] = signal

    signals = [i.split('/') for i in temp if i]
    signals = set(itertools.chain.from_iterable(signals))
    signals = set(filter(lambda x:x, signals))
    df.loc[index, f"信号"] = ('/'.join(sorted(signals)) or None)

  df.index = df.index.strftime('%Y-%m-%d')
  df['日期'] = df['日期'].dt.strftime('%Y-%m-%d')
  json_str = df.to_json(orient='index', force_ascii=False, date_format='iso', lines=False, default_handler=str)
  结果集赋值('样本数据', json.loads(json_str))
  return [True, '']
def 技术指标绘图(params):
  前缀 = params['前缀'].upper()
  df = params['df'].copy(deep=True)
  df['year'] = df.index.year

  相关字段 = [col for col in params['df'].columns if col.startswith(f"{前缀}_")]

  if params.get('绘图-追加字段'):
    追加字段 = params['绘图-追加字段']
    相关字段.insert(追加字段[0], 追加字段[1])

  for 年份, 分组数据 in df.groupby('year'):
    相关数据 = {}
    for index, row in 分组数据.iterrows():
      for column in 相关字段:
        相关数据[column] = 相关数据.get(column, [])
        相关数据[column].append({'日期': row['日期'].strftime('%Y-%m-%d'), '数值': row[column]})

    存储路径 = os.path.dirname(f"{回测结果路径}/图片/")
    参数 = {
      '标题': f"{params['指标中文名']}_{前缀}({年份}年)-对比图",
      'X轴': '日期',
      'Y轴': '数值',
      '保存路径': f"{存储路径}/{params['指标中文名']}/{params['指标中文名']}_{前缀}_{年份}.png",
      "数据": 相关数据
    }

    图表_趋势图(参数)
import itertools

def ta_close(params):
  前缀 = params['前缀'].upper()
  时间窗口字段 = [col for col in params['df'].columns if col.startswith(前缀)]

  # 添加的简单移动平均线
  if not 时间窗口字段:
    for 名称, 数组 in params['时间窗口长度'].items():
      for 长度 in 数组:
        result = params['方法'](params['df']['收盘点位'], window=长度)
        # 保留四位小数
        params['df'][f'{前缀}_{名称}_{长度}'] = result.round(4)

    时间窗口字段 = [col for col in params['df'].columns if col.startswith(前缀)]

  return [True, {'时间窗口字段': 时间窗口字段}]

def 指标信号整体写入(params):
  前缀 = params['前缀'].upper()

  for 时间窗口类型 in params['时间窗口长度'].keys():
    if 时间窗口类型 in ['短期', '中期', '长期']:
      pass
    else:
      print(f"异常时间窗口类型: {时间窗口类型}")
      sys.exit(255)

    相关字段 = [col for col in params['df'].columns if col.startswith(f"{前缀}_{时间窗口类型}_")]

    for 字段 in sorted(相关字段):
      无效字段值 = ['null', None]
      信号名 = f"信号_{字段}"
      params['df'][信号名] = '未知'

      params['df'].loc[(params['df'][字段].isna()), 信号名] = None
      params['df'].loc[(params['df'][字段] == None), 信号名] = None
      params['df'].loc[(params['df'][字段] == 'null'), 信号名] = None

      对象 = params['df'].query(f"{字段} not in @无效字段值")
      yield {'对象': 对象, '字段': 字段, '信号名': 信号名}
      params['df'].update(对象)
  
  for index, row in params['df'].iterrows():
    for 时间窗口类型 in params['时间窗口长度'].keys():
      signals = 趋势判断({'对象': params['df'], '行': row, '前缀': 前缀, '时间窗口类型': 时间窗口类型})
      params['df'].loc[index, f"信号_{前缀}_{时间窗口类型}"] = ('/'.join(signals) or None)

    signals = 趋势判断({'对象': params['df'], '行': row, '前缀': 前缀})
    params['df'].loc[index, f"信号_{前缀}"] = ('/'.join(signals) or None)

def 趋势判断_短期强于长期(params):
  相关字段 = [col for col in params['对象'].columns if col.startswith(f"{params['前缀']}_")]
  相关字段.sort(key=根据字段后缀数字排序)
  数值 = np.array([params['行'][col] for col in 相关字段])
  相关数值 = 数值[~np.isnan(数值)]
  判断结果 = [x > y for x, y in zip(相关数值, 相关数值[1:])]

  if (False in 判断结果):
    params['信号'].remove(params['误判信号'])
    if params['信号'] == []:
      params['信号'] = ['横盘']

  return params['信号']

def 趋势判断_MA(params):
  params['误判信号'] = '上行'
  return 趋势判断_短期强于长期(params)

def 趋势判断_RSI(params):
  params['误判信号'] = '多头'
  return 趋势判断_短期强于长期(params)

def 趋势判断_ROC(params):
  params['误判信号'] = '上升'
  return 趋势判断_短期强于长期(params)

def 趋势判断(params):
  对象 = params.get('对象')
  行 = params.get('行')
  前缀 = params.get('前缀')
  时间窗口类型 = params.get('时间窗口类型')

  if 时间窗口类型:
    result = [v for k,v in 行.items() if (f"信号_{前缀}_{时间窗口类型}_" in k) and v]
    signals = set(result)
  else:
    result = [v.split('/') for k,v in 行.items() if (f"信号_{前缀}_" in k) and v]
    signals = set(itertools.chain.from_iterable(result))

  signals = set(filter(lambda x:x, signals))
  signals = sorted(signals)

  params['信号'] = signals

  if ('上行' in signals) and ('MA' in 前缀):
    return 趋势判断_MA(params)
  elif ('多头' in signals) and ('RSI' in 前缀):
    return 趋势判断_RSI(params)
  elif ('上升' in signals) and ('ROC' in 前缀):
    return 趋势判断_ROC(params)
  else:
    return signals

def 根据字段后缀数字排序(s):
    match = re.search(r'(\d+)$', s)  # 匹配字符串末尾的数字
    return int(match.group(1)) if match else float('inf')  # 如果没有匹配到数字，默认值为无穷大
def ta_ma(params):
  前缀 = params['前缀'].upper()
  params['方法'] = getattr(ta.trend, f'{前缀.lower()}_indicator')
  params['绘图-追加字段'] = [0, '收盘点位']

  res = ta_close(params)

  for result in 指标信号整体写入(params):
    对象 = result.get('对象')
    字段 = result.get('字段')
    信号名 = result.get('信号名')
    # 判断趋势：如果最新收盘价高于指标值，则认为是上升趋势；否则为下降趋势
    对象.loc[(对象['收盘点位'] > 对象[字段]), 信号名] = '上行'
    对象.loc[(对象['收盘点位'] < 对象[字段]), 信号名] = '下行'
  
  技术指标绘图(params)
  return [True, '']
def technical_analysis_sma(params):
  params['前缀'] = 'sma'
  params['指标中文名'] = '简单移动平均线'
  return ta_ma(params)
def technical_analysis_ema(params):
  params['前缀'] = 'ema'
  params['指标中文名'] = '指数移动平均线'
  return ta_ma(params)
def technical_analysis_macd(params):
  pass
def technical_analysis_rsi(params):
  params['前缀'] = 'rsi'
  params['方法'] = ta.momentum.rsi
  params['指标中文名'] = '相对强弱指数'

  res = ta_close(params)

  for result in 指标信号整体写入(params):
    对象 = result.get('对象')
    字段 = result.get('字段')
    信号名 = result.get('信号名')

    # 判断标准
    # TODO: 30 70 不是不变的 对于一些波动大的可以设置在 20 80等
    对象.loc[(对象[字段] > 70), 信号名] = '超买'
    对象.loc[(对象[字段] < 30), 信号名] = '超卖'
    对象.loc[(30 < 对象[字段]) & (对象[字段] < 50), 信号名] = '空头'
    对象.loc[(50 < 对象[字段]) & (对象[字段] < 70), 信号名] = '多头'

  for 名称, 数组 in params['时间窗口长度'].items():
    for 长度 in 数组:
      指标字段名 = f"RSI_{名称}_{长度}"

      for prefix in ['顶背离', '底背离']:
        背离字段名 = f"{prefix}_{指标字段名}"
        params['df'][背离字段名] = '未知'
        params['df'].loc[(params['df'][指标字段名].isna()), 背离字段名] = None
        params['df'].loc[(params['df'][指标字段名] == None), 背离字段名] = None
        params['df'].loc[(params['df'][指标字段名] == 'null'), 背离字段名] = None

        for index, row in params['df'].head(长度).iterrows():
          params['df'].loc[index, 背离字段名] = None

      window = 长度
      for i in range(window, len(params['df'])):
        # 检测顶背离
        if all([
          params['df']['收盘点位'].iloc[i] > params['df']['收盘点位'].iloc[i-window:i].max(),  # 价格创新高
          params['df'][指标字段名].iloc[i] < params['df'][指标字段名].iloc[i-window:i].max()  # 指标未创新高
        ]):
          flag = True
        else:
          flag = False

        params['df'].at[params['df'].index[i], f"顶背离_{指标字段名}"] = flag

        # 检测底背离
        if all([
          params['df']['收盘点位'].iloc[i] < params['df']['收盘点位'].iloc[i-window:i].min(),  # 价格创新低
          params['df'][指标字段名].iloc[i] > params['df'][指标字段名].iloc[i-window:i].min()  # 指标未创新低
        ]):
          flag = True
        else:
          flag = False

        params['df'].at[params['df'].index[i], f"底背离_{指标字段名}"] = flag

  技术指标绘图(params)
  return [True, '']

def technical_analysis_roc(params):
  前缀 = 'roc'
  params['前缀'] = 前缀
  params['方法'] = ta.momentum.roc
  params['指标中文名'] = '速率变化'

  res = ta_close(params)

  for result in 指标信号整体写入(params):
    对象 = result.get('对象')
    字段 = result.get('字段')
    信号名 = result.get('信号名')

    # 判断标准
    对象.loc[(对象[字段] >= 10), 信号名] = '超买'
    对象.loc[(0 < 对象[字段]) & (对象[字段] < 10), 信号名] = '上升'

    对象.loc[(对象[字段] <= -10), 信号名] = '超卖'
    对象.loc[(-10 < 对象[字段]) & (对象[字段] <= 0), 信号名] = '下降'

    对象.loc[(对象[字段].shift(1) < 0) & (对象[字段] >= 0), 信号名] = '买入'  # 零线交叉买入
    对象.loc[(对象[字段].shift(1) > 0) & (对象[字段] <= 0), 信号名] = '卖出'  # 零线交叉卖出

  技术指标绘图(params)
  return [True, '']


# pip install matplotlib

# 中文字体缺失解决:
# sudo apt-get update
# sudo apt-get install ttf-wqy-zenhei
import glob
import hashlib
from PIL import Image
from datetime import datetime
import matplotlib.pyplot as plt

def 图表_趋势图_md5(params):
    md5_content = []
    for 标签, 数据 in params.get('数据').items():
        xlabels = [datetime.strptime(item[params.get('X轴')], "%Y-%m-%d") for item in 数据]
        yvalues = [item[params.get('Y轴')] for item in 数据]
        md5_content.append(xlabels)
        md5_content.append(yvalues)

    md5_content.append(params.get('标题'))
    md5_content.append(params.get('版本号'))
    md5_contents = ','.join(map(str, md5_content)).encode('utf-8')
    md5_hash = hashlib.md5(md5_contents).hexdigest()
    params['md5_hash'] = md5_hash
    return params

plt.rcParams['font.sans-serif'] = ['WenQuanYi Zen Hei']  # 使用 WenQuanYi Zen Hei
plt.rcParams['axes.unicode_minus'] = False  # 解决负号显示问题

图表_标记样式 = {
    '.' : '点',
    ',' : '像素',
    'o' : '圆圈',
    'v' : '向下三角形',
    '^' : '向上三角形',
    '<' : '向左三角形',
    '>' : '向右三角形',
    '1' : '三脚朝下三角形',
    '2' : '三脚朝上三角形',
    '3' : '三脚朝左三角形',
    '4' : '三脚朝右三角形',
    's' : '正方形',
    'p' : '五边形',
    '*' : '星号',
    'h' : '六边形1',
    'H' : '六边形2',
    '+' : '加号',
    'x' : '叉号',
    'D' : '菱形',
    'd' : '细菱形',
    '|' : '垂直线',
    '_' : '水平线',
}

def 图表_趋势图(params):
    params['版本号'] = params.get('版本号', '202412170958') # 当进行较大调整希望全部保存图片重新生成时调整该值
    if not params.get('md5_hash', False):
        图表_趋势图_md5(params)

    保存路径 = params.get('保存路径')
    目录名 = os.path.dirname(保存路径)
    文件名, 后缀 = os.path.splitext(os.path.basename(保存路径))
    保存路径_md5 = f"{目录名}/.{文件名}_{params.get('md5_hash')}{后缀}"
    点位标记 = params.get('点位标记', False)


    if not os.path.exists(目录名):
        os.makedirs(目录名)

    if os.path.exists(保存路径_md5): 
        if not os.path.exists(保存路径): 
            os.link(保存路径_md5, 保存路径)

        # 验证图片是否完整
        try:
            with Image.open(保存路径) as img:
                img.verify()
            return True
        except Exception as e:
            print(f"[完整性校验未通过]: {保存路径}({e})")

    if os.path.exists(保存路径): os.remove(保存路径)
    for file in glob.glob(f"{目录名}/.{文件名}_*"):
        os.remove(file)
    
    图表标点 = list(图表_标记样式.copy().keys())

    结果集数量 = [len(values) for values in params.get('数据').values()]

    # 根据数据点的数量动态调整图形的大小
    fig_width = max(30, max(结果集数量) * 0.25)
    fig_width = min(fig_width, 90)
    fig_height = fig_width / 2

    文字颜色 = '#38f30c'

    # 创建图形
    fig, ax = plt.subplots(figsize=(fig_width, fig_height))

    # 设置背景颜色
    ax.set_facecolor('black')
    fig.patch.set_facecolor('black')

    # 设置标题和标签
    plt.title(params.get('标题'), color=文字颜色)
    plt.xlabel(params.get('X轴'), color=文字颜色)
    plt.ylabel(params.get('Y轴'), color=文字颜色)

    for 标签, 数据 in params.get('数据').items():
        xlabels = [datetime.strptime(item[params.get('X轴')], "%Y-%m-%d") for item in 数据]
        yvalues = [item[params.get('Y轴')] for item in 数据]

        # 绘制折线图
        plt.plot(xlabels, yvalues, marker=(图表标点.pop() if 点位标记 else None), label=标签)

    # 格式化日期显示
    plt.gca().xaxis.set_major_formatter(plt.matplotlib.dates.DateFormatter('%Y-%m'))

    # 显示网格
    plt.grid(True, linestyle='--', alpha=0.7, color=文字颜色)
    
    # 设置刻度颜色
    ax.tick_params(axis='x', colors=文字颜色)
    ax.tick_params(axis='y', colors=文字颜色)

    # 自动调整日期标签以避免重叠
    plt.gcf().autofmt_xdate()

    # 添加图例，并设置背景颜色
    legend = plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), fancybox=True, shadow=True, ncol=2)
    frame = legend.get_frame()
    frame.set_facecolor('black')  # 设置背景颜色为浅灰色
    frame.set_edgecolor('black')  # 设置边框颜色为黑色

        
    # 遍历图例中的所有文本对象，并设置字体颜色
    for text in legend.get_texts():
        text.set_color(文字颜色)  # 将字体颜色设置为蓝色

    # 保存图形到文件
    plt.savefig(保存路径_md5)
    os.link(保存路径_md5, 保存路径)
    # os.symlink(保存路径_md5, 保存路径) # 软链接可以减少存储 但是读取存在问题 后期优化
    plt.close(fig)

import os
import re
import sys
import copy
import json
import numpy as np
import pandas as pd
import akshare as ak # pip install akshare

# 不知道有什么用 但是可以消除df.replace的警告
# 设置 Pandas 选项以启用未来行为
pd.set_option('future.no_silent_downcasting', True)

with open(sys.argv[1], 'r', encoding='utf-8') as file:
  try:
    结果集 = json.load(file)
  except Exception as e:
    print("加载错误:", e)
    sys.exit(255)

# 动态设置运行变量
for key, value in 结果集['参数'].items():
    locals()[key] = value


# 自定义编码器
class MyJSONEncoder(json.JSONEncoder):
  def default(self, obj):
    # 将Timestamp 能够正确地转换为 JSON 格式的字符串
    if isinstance(obj, pd.Timestamp):
      hour, minute, second = obj.hour, obj.minute, obj.second
      if (hour == 0 and minute == 0 and second == 0):
        return obj.strftime('%Y-%m-%d')
      else: 
        return obj.strftime('%Y-%m-%d %H:%M:%S')
       
    return super(MyJSONEncoder, self).default(obj)


# 通用方法
def 结果集赋值(key, value):
  with open(sys.argv[1], 'r', encoding='utf-8') as file:
    原始结果集 = json.load(file)

  if isinstance(value, pd.DataFrame):
    # value = value.fillna('null')
    value = value.replace({np.nan: 'null', None: 'null'})

    # 不要用reset_index 在设置索引时不删除原字段
    # df.set_index('日期', inplace=True, drop=False)
    # 结果集[key] = value.reset_index().to_dict('records')
    结果集[key] = value.to_dict('records')
  else:
    结果集[key] = value

  with open(sys.argv[1], 'w') as f:
    try:
      f.write(json.dumps(结果集, indent=2, ensure_ascii=False, cls=MyJSONEncoder))
    except Exception as e:
      print(f"数据已回滚: {e}")
      f.write(json.dumps(原始结果集, indent=2, ensure_ascii=False))
      sys.exit(255)
