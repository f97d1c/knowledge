require_relative '../../../../Ruby.rb'
绝对路径 = File.join([@当前路径, @回测结果路径])

FileUtils.mkdir_p(@回测研报存储路径) unless File.directory?(@回测研报存储路径)

配置 = {
  "绘制" => {
  },
  "参数" => {
    "文档" => {
      "标题" => @当前回测指数,
    },
    "文档转换" => {
      "路径" => File.join([@回测研报存储路径, "#{@当前回测指数}.rmd"]),
      "输出格式" => [
        "html_document"
      ],
      "输出路径" => File.join([@回测研报存储路径, "#{@当前回测指数}.html"])
    }
  }
}

File.open(File.join([@回测研报存储路径, "#{@当前回测指数}.json"]), 'w'){|f| f.write(JSON.pretty_generate(配置))}