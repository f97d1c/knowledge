def ta_ma(params):
  前缀 = params['前缀'].upper()
  params['方法'] = getattr(ta.trend, f'{前缀.lower()}_indicator')
  params['绘图-追加字段'] = [0, '收盘点位']

  res = ta_close(params)

  for result in 指标信号整体写入(params):
    对象 = result.get('对象')
    字段 = result.get('字段')
    信号名 = result.get('信号名')
    # 判断趋势：如果最新收盘价高于指标值，则认为是上升趋势；否则为下降趋势
    对象.loc[(对象['收盘点位'] > 对象[字段]), 信号名] = '上行'
    对象.loc[(对象['收盘点位'] < 对象[字段]), 信号名] = '下行'
  
  技术指标绘图(params)
  return [True, '']