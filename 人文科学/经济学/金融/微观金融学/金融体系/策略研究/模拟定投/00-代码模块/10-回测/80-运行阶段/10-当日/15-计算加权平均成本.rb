require_relative '../../../Ruby.rb'

@结果集['定投详情'].each do |模型名, 记录|
  记录.each_with_index do |item, 下标|
    日期, 详情 = item
    总买入记录 = 记录.values()[0..下标].select{|h| h['买卖金额'] > 0}
    总买入股数 = 总买入记录.map{|i| i['买卖股数']}.sum
    总买入金额 = 总买入记录.map{|i| i['买卖金额']}.sum

    if 总买入金额 != 0
      详情['加权平均成本'] = (总买入金额.to_f / 总买入股数).round(4)
    else
      详情['加权平均成本'] = 0
    end
  end
end

结果集赋值('定投详情', @结果集['定投详情'])