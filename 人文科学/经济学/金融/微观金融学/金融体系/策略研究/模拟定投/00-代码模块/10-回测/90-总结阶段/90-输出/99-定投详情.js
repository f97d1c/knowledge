// npm install cli-table3
const args = process.argv.slice(2);
const Table = require('cli-table3');
const fs = require('fs');
var table;

// 递归函数，用于处理嵌套对象
function 打印表格(obj, prefix = '', level=0) {
  if (level == 0) {
    table = new Table({
      style: {
        head: ['green'],
        border: ['grey']
      }
    });
  }
 
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      const value = obj[key];
      if (typeof value === 'object' && value !== null && !Array.isArray(value)) {
        // 如果是嵌套对象，递归处理
        打印表格(value, `${prefix}${key}.`, level+1);
      } else if (Array.isArray(value)) {
        // 如果是数组，将其转换为字符串
        table.push([`${prefix}${key}`, value.join(', ')]);
      } else {
        // 普通键值对
        table.push([`${prefix}${key}`, value]);
      }
    }
  }
}

fs.readFile(args[0], 'utf8', (err, data) => {
  if (err) process.exit(255);

  try {
    const jsonData = JSON.parse(data);
    Object.entries(jsonData.定投详情).forEach(([模型名, 记录]) => {
      console.log("\n"+模型名);
      打印表格(Object.values(记录).slice(-1)[0]);
      console.log(table.toString());
    });
  } catch (parseErr) {
    console.error('Error parsing JSON:', parseErr);
    process.exit(255);
  }
});
