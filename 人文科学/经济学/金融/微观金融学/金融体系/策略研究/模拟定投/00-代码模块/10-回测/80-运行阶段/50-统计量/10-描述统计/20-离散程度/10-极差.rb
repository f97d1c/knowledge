require_relative '../../../../../Ruby.rb'

统计属性 = ['买卖金额', '总资产']

统计属性.each do |属性名|
  @结果集['定投详情'].each do |模型名, 记录|
    策略 = @结果集['定投策略'][模型名]
    values = 记录.values()
    记录.each do |日期, 详情|
      详情["#{属性名}-最小值"] = values[0..values.index(详情)].map{|元素| 元素[属性名]}.min
      详情["#{属性名}-最大值"] = values[0..values.index(详情)].map{|元素| 元素[属性名]}.max
    end
  end
end

结果集赋值('定投详情', @结果集['定投详情'])