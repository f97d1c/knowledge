#!/bin/bash --login

function output (){
  printf "[$(date '+%Y-%m-%dT%H:%M:%S.%3N')] "
  printf "$*"
  printf "\n"
}

function 遍历执行脚本(){
  find $1 -type f -name "[0-9][0-9]-*" | sort -n | tr '\n' '\0' | while IFS= read -r -d $'\0' path; do
    output '开始执行: '$path
    params_file=$2
    if [[ $path =~ \.py$ ]]; then
      python -B $path $params_file
      code=$?
    elif [[ $path =~ \.rb$ ]]; then
      ruby $path $params_file
      code=$?
    elif [[ $path =~ \.js$ ]]; then
      node $path $params_file
      code=$?
    elif [[ $path =~ \.sh$ ]]; then
      bash $path $params_file
      code=$?
    elif [[ $path =~ \.var$ || $path =~ \.rmd$ ]]; then
      output '跳过执行: 非执行文件'
    else
      echo '[false, "未匹配该后缀格式", "'$path'"]' && exit 255
    fi

    if [[ $code -eq 8 ]];then output '跳过执行: 逻辑不符合未执行';fi
    if [[ $code -eq 233 ]];then output '跳出执行: 后续逻辑不应执行'&& echo '[true, ""]' && exit 0;fi
    # 避免使用没有if关键字形式的逻辑判断
    # 如果在最后一行并且不符合条件 返回码会为1
    # ([[ ! $code -eq 0 ]] && [[ ! $code -eq 8 ]]) && echo '[false, "返回值异常", '$code']' && exit $code;
    if [[ ! $code -eq 0 ]] && [[ ! $code -eq 8 ]];then 
      output '跳出执行: 返回值异常'
      output '当前执行: '$path
      echo '[false, "返回值异常", '$code']'
      exit $code;
    fi
  done
}