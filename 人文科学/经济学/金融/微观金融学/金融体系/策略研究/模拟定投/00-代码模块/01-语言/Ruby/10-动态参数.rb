
def 动态参数(路径:, 结果集:{} , 非覆盖参数: [])
  结果集['参数'] ||= {}

  Dir[路径].sort.each do |路径|
    变量名 = 路径.split('/')[-1].split('.')[0]
    变量名 = 变量名.gsub(/^\d{2,}\-/, '')

    值 = File.open(路径){|f| f.read()}
    变量值 = JSON.parse(值) rescue 值

    if 变量值.to_s.include?('@')
      # 结果集['参数']["#{变量名}_原始"] = 变量值.clone      
      instance_variables.sort_by{|n| -n.length}.each do |实例变量|
        next unless 变量值.include?(实例变量.to_s)
        实例值 = instance_variable_get(实例变量)
        case 变量值
        when /^ruby:.*/
          变量值 = eval(变量值.gsub(/^ruby:(.*)/, '\1'))
        else
          变量值 = 变量值.gsub(实例变量.to_s, 实例值)
        end
      end
    end

    if !非覆盖参数.include?(变量名) || !!!结果集['参数'][变量名]
      结果集['参数'][变量名] = 变量值
      instance_variable_set("@#{变量名}", 变量值)
    end
  end

  return 结果集
end

result = 动态参数(路径: "#{@根路径||File.dirname(ARGV[0])}/**/*.var")
result = 动态参数(路径: "#{@当前路径}/*-参数/**/*.var", 结果集: Marshal.load(Marshal.dump(result)), 非覆盖参数: result['参数'].keys)

@结果集['参数'].merge!(result['参数'])

结果集赋值('参数', @结果集['参数'])