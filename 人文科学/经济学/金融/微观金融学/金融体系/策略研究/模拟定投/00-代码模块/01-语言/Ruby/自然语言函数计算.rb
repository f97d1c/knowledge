def 自然语言函数计算(对象:, 公式:, 变量:, 结果:{})
  计算变量 = Marshal.load(Marshal.dump(变量))
  计算公式 = [Marshal.load(Marshal.dump(公式))].flatten

  函数变量 = {
    '向上取整' => 'ceil',
    '向下取整' => 'floor',
  }
  计算变量.merge!(函数变量)

  动态变量 = 计算变量.select{|键,值| 值.to_s.match(/^@.*/)}

  unless 动态变量.empty?
    动态变量.each do |键,值|
      计算变量[键] = 对象[值.split('@')[1]]
    end
  end

  计算公式.each do |等式|
    赋值变量名, 表达式 = 等式.split('=')

    表达式 = 表达式.gsub(/#{计算变量.keys.sort{|a, b| b.length <=> a.length}.map{|k| Regexp.escape(k)}.join('|')}/) do |match|
      计算变量[match]
    end

    unless 表达式.match(/^[a-z \d \s \. \( \) \+ \- \* \/]+$/)
      print "#{表达式} 未通过校验\n"
      exit(255)
    end

    运算结果 = case (运算结果 = eval(表达式))
    when Float
      运算结果.round(4)
    else
      运算结果
    end

    结果[赋值变量名.gsub(' ', '')] = 运算结果
    计算变量.merge!(结果)
  end

  结果
end