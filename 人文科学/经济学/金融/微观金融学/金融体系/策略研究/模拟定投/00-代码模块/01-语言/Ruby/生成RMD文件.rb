def 生成RMD文件(参数路径: )
  内容 = File.open(参数路径){|f|JSON.parse(f.read)}
  rmd文件路径 = 内容['参数']['文档转换']['路径']
  rmd文件 = File.open(rmd文件路径, 'w')

  模板文件内容 = File.open(Dir["**/头部.rmd"][0]){|f|f.read}
  # 在下面全文档gsub替换会出现乱码 找不到原因
  模板文件内容 = 模板文件内容.gsub('{项目根路径}', @当前路径)
  内容['参数']['文档'].each do |关键字, 替换内容|
    模板文件内容 = 模板文件内容.gsub("{#{关键字}}", 替换内容)
  end
  rmd文件.write(模板文件内容)

  当前上级标题 = nil
  内容['绘制'].each do |键,值|
    上级标题,当前标题 = 键.split('/')
    unless 当前上级标题 == 上级标题
      当前上级标题 = 上级标题
      rmd文件.write("\n\n## #{上级标题}") 
    end

    rmd文件.write("\n\n### #{当前标题}")
    rmd文件.write("\n```{r, echo=FALSE, out.width='100%'}")
    rmd文件.write("\n绘制('#{键}')")
    rmd文件.write("\n```")
  end

  # 文件没有及时关闭会出现下面使用该文件时产生的结果不一致
  # 但是运行结束后再去查找问题文件又写入了找不到问题
  rmd文件.close()
end