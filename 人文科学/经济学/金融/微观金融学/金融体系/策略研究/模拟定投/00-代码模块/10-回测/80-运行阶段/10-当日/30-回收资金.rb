require_relative '../../../Ruby.rb'

@结果集['定投详情'].each do |模型名, 记录|
  记录.each do |日期, 详情|
    if 详情['买卖金额'] > 0
      详情['回收资金'] = 0
      next
    end

    详情['回收资金'] = -详情['买卖金额'].round(4)
  end
end

结果集赋值('定投详情', @结果集['定投详情'])