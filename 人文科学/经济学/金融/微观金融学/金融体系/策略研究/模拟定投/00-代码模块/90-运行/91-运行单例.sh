#!/bin/bash --login
instance=$(jq -r '.["参数"]["回测实例"]' "$1")
[[ $instance != 'true' ]] && exit 8

source $(find . -type f -name 'Bash.sh')
SCRIPT_PATH=$(readlink -f "${BASH_SOURCE[0]}")
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")

遍历执行脚本 "$SCRIPT_DIR/../10-回测" $1
code=$?
if [[ ! $code -eq 0 ]];then echo '[false, "实例运行中返回值异常", '$code']' && exit $code;fi

exit 0;