// 在页面加载完成后执行该函数
window.onload = function() {
  // 将所有表格中带有http或https的内容改为a标签
  tds = document.querySelectorAll("td");
  tds.forEach((element) => {
    if (/http[s]*\:\/\/.*/.test(element.textContent)) {
      a = document.createElement("a");
      a.href = element.textContent;
      a.target = '_blank'
      a.textContent = element.textContent;
      element.innerHTML = a.outerHTML;
    }
  });
};