
if (!require('rjson', character.only = TRUE)) {
  install.packages('rjson', repos = "https://mirrors.ustc.edu.cn/CRAN/")
}
library(rjson)
# rjson 和 jsonlite fromJSON 存在冲突!!!!
# 在R中，rjson 和 jsonlite 都提供了处理JSON数据的功能，但它们的函数名称和行为可能有所不同。
# 如果同时加载了这两个包，并且都使用了 fromJSON 函数，可能会导致命名冲突。
# 在下面加载jsonlite后按照jsonlite写法读写

RootPath = getwd()

requires = fromJSON(file=file.path(RootPath, '00-代码模块/01-语言/R/配置/依赖库.json'))

for (key in names(requires)) {
  current_time = format(Sys.time(), "[%Y-%m-%d %H:%M:%OS3]")
  cat(current_time, '')

  lib = requires[[key]]

  if (!require(lib, character.only = TRUE)) {
    install.packages(lib, repos = "https://mirrors.ustc.edu.cn/CRAN/")
  }

  if (!is.loaded(lib)) {
    library(lib, character.only = TRUE)
  }
}

# 加载通用方法
for (dir in list.dirs()) {
  # 设置工作目录
  setwd(file.path(RootPath, dir))
  for(file in list.files(pattern = "*.R$")){
    source(file)
  }
}
setwd(RootPath)
Methods = ls()

args = commandArgs(T)
运行参数 = fromJSON(args[1])

list2env(运行参数$参数, envir = .GlobalEnv)
