# 参考资料
## https://juejin.cn/post/7129493981842538503
## https://wiki.mbalib.com/wiki/%E6%A0%87%E5%87%86%E5%B7%AE

def 标准差(参数)
  结果 = {}
  净值数组 = 参数['历史净值'].values
  单位净值数组 = 净值数组.map{|单日数据| 单日数据["单位净值"].to_f }

  mean = 单位净值数组.sum(0.0) / 单位净值数组.size
  sum = 单位净值数组.sum(0.0) { |element| (element - mean) ** 2 }
  variance = sum / (单位净值数组.size - 1)
  standard_deviation = Math.sqrt(variance)

  结果['标准差'] = standard_deviation
  # 结果['标准差'] = sprintf("%.#{参数['小数位数']}f", standard_deviation)
  
  结果['说明'] = [
    '数值的准确程度有待进一步验证',
    '一般而言,标准差愈大,表示净值的涨跌较剧烈,风险程度也较大.',
  ]

  结果['下一步'] = '这里可以列举行业较优及垫底标准差及平均标准差.'
  [true, 结果]
end