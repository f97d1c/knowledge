def 阶段状态(参数)
  结果 = {}
  净值数组 = 参数['历史净值'].values
  起始净值 = 净值数组.first["单位净值"].to_f
  结束净值 = 净值数组.last["单位净值"].to_f
  阶段涨幅 = 结束净值 - 起始净值

  结果['状态'] = (阶段涨幅 > 0 ? '上涨' : '下跌')
  # 结果['涨幅'] = sprintf("%.#{参数['小数位数']}f", 阶段涨幅)
  结果['涨幅'] = 阶段涨幅

  日增长 = 阶段涨幅 / 净值数组.size
  正增长率 = 净值数组.select{|hash| hash["日增长率"].to_f > 0}.size.to_f / 净值数组.size
  # 结果['日增长'] = sprintf("%.#{参数['小数位数']}f", 日增长)
  结果['日增长'] = 日增长
  # 结果['年利率'] = sprintf("%.#{参数['小数位数']}f", ((日增长*365*正增长率 / 起始净值)*100))
  结果['年利率'] = (日增长*365*正增长率 / 起始净值)*100
  # 结果['年利率'] = (日增长*365*正增长率 / 起始净值)

  [true, 结果]
end