require File.expand_path("../自描述.rb", __FILE__)

def 目录参数检查(路径: nil, 传入参数: {})
  目标路径 = File::join([路径, "参数.json"])
  return [true, 传入参数] unless File::file?(目标路径)

  begin
    定义参数 = JSON.parse(File.read(目标路径))
  rescue JSON::ParserError => e
    return [false, "参数非JSON格式", "#{ARGV[0]}"]
  end

  res = 参数校验(定义参数: 定义参数, 传入参数: 传入参数)
  return res if res[0]
  (res[2] ||= {}).merge!({当前路径: 路径})
  return res
end

def 执行文件内容(路径: nil, 运行参数: {})
  文件名 = 路径.split("/")[-1]
  方法名 = 文件名.split("/")[-1].gsub(/[\d{1,}\.|]*(.*)\..*/,'\1')

  begin
    case 文件名
    when /.*\.js$/
      res = `node 语言/Js/嘉裕.js '{"文件路径": "#{File.expand_path(路径)}", "运行参数": #{运行参数.to_json}}'`
    when /.*\.rb$/
      require File.expand_path(路径)
      res = send(方法名, 运行参数)
      return res
    when /.*\.json$/
      File.open(路径) do |file|
        res = [true, JSON.parse(file.read)] 
      end
      return res
    else
      res = '[false, "未匹配执行模式"]'
    end
    res = (JSON.parse(res.gsub(/\n$/, "")) rescue [false, "[解析异常]: #{res || "返回结果为空"}"])
  rescue => e
    return [false, "[运行时异常] #{e.to_s}"]
  end

  return res
end

def 迭代目录(路径: nil, 结果: {}, 参数: {}, 历史运行参数: {})
  key = 路径.split("/")[-1].gsub(/\d{1,}\.(.*)/,'\1')
  res = 目录参数检查(路径: 路径, 传入参数: 参数.clone())
  return res unless res[0]
  运行参数 = res[1]
  历史运行参数[key] = 运行参数

  Dir.foreach(路径).sort_by{|x| x.gsub(/(\d{1,})\..*/,'\1').to_i }.to_a.each do |name|
    next if [".", "..", "参数.json"].include?(name)
    item_path = File::join([路径, name])
    item_key = name.split("/")[-1].gsub(/[\d{1,}\.|]*(.*)\..*/,'\1')

    if File::directory?(item_path)
      res = 迭代目录(路径: item_path, 结果: (结果[key] ||= {}), 参数: 运行参数, 历史运行参数: 历史运行参数)
      return res unless res[0]
    elsif File::file?(item_path)
      res = 执行文件内容(路径: item_path, 运行参数: 运行参数)
      return [false, "[返回结果为空] #{item_path}"] if res.nil?
      return res unless res[0]

      # instance_variable_set("@#{key}", res[1])
      # ((结果[key]||={})[key] ||= []) << res[1]

      instance_variable_set("@#{item_key}", res[1])
      (结果[key]||={})[item_key] = res[1]

    end
  end

  case 结果[key]
  when NilClass
    结果[key] = nil
  when Array
    结果[key] = 结果[key][0] if 结果[key].size == 1
  when Hash
    结果[key] = 结果[key][key] if 结果[key].keys.size == 1 && 结果[key][key]
  end

  return [true, 结果, {运行参数: 历史运行参数}]
end
