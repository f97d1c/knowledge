
def 变量类型(变量) 
  case 变量
  when String
    return '文本'
  when Integer
    return '整数'
  when Float
    return '小数'
  else
    return 变量.class
  end
end

def 变量类型校验(变量: nil, 预期类型: nil, 代入参数: nil, 参数键: nil)

  case 预期类型
  when '键值对'
    return [false, '非键值对类型'] unless (变量.class == Hash)
  when '数组'
    return [false, '非数组类型'] unless (变量.class == Array)
  when '整数', '数字'
    return [false, '非整数类型'] unless (变量.class == Integer)
  when '小数'
    return [false, '非整数类型'] unless (变量.class == Float)
  when '文本'
    return [false, '非文本类型'] unless (变量.class == String)
  when '年份'
    return [false, '非整数类型'] unless (变量.class == Integer)
    return [false, '超出支持年份范围'] unless ((1970 <= 变量) && (变量 <= Time.now.year))
  when '月份'
    return [false, '非整数类型'] unless (变量.class == Integer)
    return [false, '超出月份范围'] unless (1..12).include?(变量)
  when '日历日'
    return [false, '非整数类型'] unless (变量.class == Integer)
    return [false, '超出日历范围'] unless (1..31).include?(变量)
  when '日期' 
    return [false, '非文本类型'] unless (变量.class == String)
    return [false, '日期格式错误(年-月-日)'] unless (变量 =~ /\d{4}\-\d{1,2}\-\d{1,2}/)

    begin
      DateTime.new(*变量.split('-').map(&:to_i))
    rescue
      return [false, "非法日期(#{变量})"]
    end

  when '年龄'
    return [false, '非整数类型'] unless (变量.class == Integer)
    return [false, '超出支持年龄范围'] unless ((1 <= 变量) && (变量 <= 122))
  when '代码块'
    return [false, '代码格式未通过校验(^lambda{|.*|.*}$)'] unless (变量 =~ /^lambda{|.*|.*}$/)
    代码块 = eval 变量
    代入参数[参数键] = 代码块.call(代入参数)
  else
    return [false, "未定义预期类型(#{预期类型})"]
  end
  
  [true, '']
end

def 参数一致性校验(定义参数: {}, 实际参数: {})
  定义参数.each do |key, value|
    types = [value['类型']].flatten
    types.each_with_index do |预期类型, 下标|
      res = 变量类型校验(变量: 实际参数[key], 预期类型: 预期类型, 代入参数: 实际参数, 参数键: key)
      next unless (types.size == 下标+1)
      next if res[0]
      return [false, "参数类型不符(#{key})", value.merge!({传入值: 实际参数[key], 实际类型: 变量类型(实际参数[key]), 说明: res[1]})]
    end
  end

  return [true, 实际参数]
end

def 参数校验(定义参数: {}, 传入参数: {})
  实际参数 = {}

  if 定义参数["参数继承"]
    res = 参数一致性校验(定义参数: {"参数继承" => 定义参数["参数继承"]}, 实际参数: {"参数继承" => (传入参数["参数继承"] || 定义参数["参数继承"]['默认值'])})
    return res unless res[0]

    tmp = {}
    [res[1]["参数继承"]].flatten.each do |path|
      路径 = File.expand_path(path, "#{__FILE__}/../../../") rescue binding.pry
      return [false, "继承路径不存在(#{路径})"] unless File::exist?(路径)
      return [false, "继承路径非普通文件(#{路径})"] unless File::file?(路径)
      File.open(路径) do |文件|
        begin
          tmp.merge!(JSON::parse(文件.read))
        rescue => e
          return [false, "[JSON格式化异常](#{路径})"]
        end
      end
    end

    定义参数 = tmp.merge!(定义参数)
  end

  定义参数.each do |key, value|
    实际参数[key] = (传入参数[key] || value['默认值'])
  end

  res = 参数一致性校验(定义参数: 定义参数, 实际参数: 实际参数)
  return res unless res[0]

  return [true, res[1]]
end