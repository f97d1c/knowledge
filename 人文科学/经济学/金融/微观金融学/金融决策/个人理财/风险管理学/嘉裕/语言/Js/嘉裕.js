fs = require('fs')

let argvs = ''
for (i = 2; i < process.argv.length; i++) {
  argvs += process.argv[i]
}

let 参数 = JSON.parse(argvs);
if (!参数.文件路径) return console.log(JSON.stringify([false, "文件路径不能为空"]))

if (!fs.existsSync(参数.文件路径)) {
  return console.log(JSON.stringify([false, "非法文件路径"]))
}

// console.log(参数.运行参数)
// process.exit(255)

for (let [key, value] of Object.entries(参数.运行参数||{})) {
  this[key] = value
}

let result = require(参数.文件路径)(this)

return console.log(JSON.stringify(result))