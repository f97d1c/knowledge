#!/bin/bash --login

function expect () {
  result="$(echo "$1" | tail -n 1)"
  jq -e '.' <<< $result 1>/dev/null
  if [[ ! $? -eq 0 ]];then echo '[false, "返回结果格式异常", "'$1'"]'; exit 255; fi
  if [ ! "$(jq '.[0]' <<< $result)" == "$2" ]; then 
    jq -e '.' <<< $result
    echo '[false, "返回结果与预期不符", "'$result'"]'
    exit 255
  fi
}

function expect_false () {
  expect "$1" 'false'
}

function expect_true () {
  expect "$1" 'true'
}

ruby 语言/Ruby/嘉裕.rb '{"目标路径": "测试/金融态势分析/基金"}'
# ruby 语言/Ruby/嘉裕.rb '{"目标路径": "测试/人物画像推测", "出生年": 1976, "出生月": 5, "出生日": 10}'
exit 255

# expect_false "$()"
# expect_false "$()"
# expect_false "$()"
# expect_false "$()"
# expect_false "$()"
# expect_false "$()"
expect_true "$(ruby 语言/Ruby/嘉裕.rb '{"目标路径": "测试/人物画像推测", "出生年": 1976, "出生月": 5, "出生日": 10}')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb '{"目标路径": "测试", "出生年": 1976, "出生月": 5, "出生日": 10, "年龄": 500}')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb '{"目标路径": "测试", "出生年": 1976, "出生月": 2, "出生日": 30}')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb '{"目标路径": "测试", "出生年": 1976, "出生月": 5, "出生日": 100, "年龄": 25}')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb '{"目标路径": "测试", "出生年": 1976, "出生月": 13, "出生日": 10, "年龄": 25}')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb '{"目标路径": "测试", "出生年": 197, "出生月": 5, "出生日": 10, "年龄": 25}')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb '{"目标路径": "测试", "年龄": 12.4}')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb '{"目标路径": "测试"}')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb '{"目标路径": "嘉裕.sh"}')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb '{"目标路径": "嘉裕.sh"}')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb '{"目标路径": "嘉裕"}')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb '{"key": "嘉裕"}')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb '{key: "嘉裕"}')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb '嘉裕')"
expect_false "$(ruby 语言/Ruby/嘉裕.rb)"

echo '[true, ""]'