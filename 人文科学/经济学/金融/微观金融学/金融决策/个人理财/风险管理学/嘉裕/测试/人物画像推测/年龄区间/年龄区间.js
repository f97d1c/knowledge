module.exports = function(参数) {
  if (参数.年龄 >= 0 && 参数.年龄 <= 17) return [true, "未成年人"]
  if (参数.年龄 >= 18 && 参数.年龄 <= 65) return [true, "青年人"]
  if (参数.年龄 >= 66 && 参数.年龄 <= 79) return [true, "中年人"]
  if (参数.年龄 >= 80 && 参数.年龄 <= 99) return [true, "老年人"]
  if (参数.年龄 >= 100) return [true, "长寿老人"]
  return [false, "年龄要求不符"]
}