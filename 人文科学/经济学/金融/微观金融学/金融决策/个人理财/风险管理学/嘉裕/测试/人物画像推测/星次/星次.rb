
# https://github.com/lanvige/chinese_lunar/blob/master/spec/chinese_lunar/convert_spec.rb
require 'chinese_lunar'
require 'date'
require 'pry'

def 星次(参数)
  农历对象 =  ChineseLunar::Lunar.new(Date.new(参数['出生年'], 参数['出生月'], 参数['出生日']))
  农历月,农历日 = 农历对象.lunar_date.gsub(/\d{4}\-/, '').split('-').map(&:to_i)

  星次 = {  
    "1-1--1-5" => "星纪",
    "12-7--12-31" => "星纪",
    "1-6--2-3" => "玄枵",
    "2-4--3-5" => "娵訾",
    "3-6--4-4" => "降娄",
    "4-5--5-5" => "大梁",
    "5-6--6-5" => "实沈",
    "6-6--7-6" => "鹑首",
    "7-7--8-7" => "鹑火",
    "8-8--9-7" => "鹑尾",
    "9-8--10-7" => "寿星",
    "10-8--11-8" => "大火",
    "11-9--12-6" => "析木",
  }

  星次.each do |范围, 名称|
    起始月, 起始日, 结束月, 结束日 = 范围.split('-').delete_if { |item| item == '' }.map(&:to_i)
    next unless (起始月 <= 农历月) && (农历月 <= 结束月)

    case 农历月
    when 起始月
      next unless 农历日 >= 起始日
    when 起始月 + 1
      next unless 农历日 <= 结束日
    else
      return [false, '超出匹配范围']
    end

    return [true, 名称]
  end

  return [false, nil]
end

# def datetime_sequence(start, stop, step)
#   dates = [start]
#   while dates.last < (stop - step)
#     dates << (dates.last + 1)
#   end 
#   return dates
# end 

# datetime_sequence(Date.new(2022, 1, 1), Date.new(2023, 1, 22), 1).each do |date|
#   puts date.to_s
#   puts 星次({'出生年' => date.year,'出生月' => date.month,'出生日' => date.day})
# end
