requires = {
  'pry': "断点调试",
  'rest-client': "Http请求", # gem install rest-client -v 1.8.0
  'json': "JSON数据处理",
  'ostruct': "JSON转Ruby对象",
  'chinese_lunar': "农历日期工具", # gem install chinese_lunar
  
}

requires.each do |gem_name, desc|
  begin
    require gem_name.to_s
  rescue LoadError => e
    puts "缺少Gem: #{gem_name}(用于#{desc}), 请先手动安装该Gem."
  end
end

require File.expand_path("../目录及文件相关逻辑.rb", __FILE__)

require File.expand_path("../基础参数校验.rb", __FILE__)

res = 基础参数校验
unless res[0]
  puts res.to_json
  exit 255
else
  参数 = res[1]
end

result = { 传入参数: 参数, 运行参数: {}, 结果: {} }

res = 迭代目录(路径: 参数["目标路径"], 参数:参数)
unless res[0]
  puts res.to_json
  exit 255
end

result[:结果].merge!(res[1])
# result[:运行参数].merge!((res[2]||{})[:运行参数]||{})

def 扁平化结果(hash)
  res = []
  hash.each do |key, value|
    res << (value.instance_of?(Hash) ? 扁平化结果(value) : value)
  end
  res.flatten
end

if (扁平化结果(result[:结果]).include?(nil))
  输出 = [false, '存在为空结果', result]
  puts JSON.pretty_generate(输出)
  puts ""
  puts 输出.to_json
  exit 255
else
  puts JSON.pretty_generate([true, result])
  puts ""
  puts [true, result].to_json  
end

