
def 基础参数校验
  return[false, "参数不能为空"] unless ARGV[0]

  begin
    参数 = JSON.parse(ARGV[0])
  rescue JSON::ParserError => e  
    return [false, "参数非JSON格式", "#{ARGV[0]}"]
  end
  
  return [false, "目标路径不能为空", "#{ARGV[0]}"] unless 参数['目标路径']
  
  return [false, "目标路径不存在", "#{ARGV[0]}"] unless File::exist?(参数['目标路径'])
  
  return [false, "目标路径非文件夹", "#{ARGV[0]}"] unless File::directory?(参数['目标路径'])

  return [true, 参数]
end