class Cookie {

  get(sKey) {
    let tmp = decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[-.+*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
    if (!tmp) return tmp;

    try {
      tmp = JSON.parse(tmp)
    } catch (e) {
    }
    return tmp;
  }

  set(sKey, sValue, vEnd, sPath, sDomain, bSecure) {
    if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
    var sExpires = "";
    if (vEnd) {
      switch (vEnd.constructor) {
        case Number:
          sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
          break;
        case String:
          sExpires = "; expires=" + vEnd;
          break;
        case Date:
          sExpires = "; expires=" + vEnd.toUTCString();
          break;
      }
    }
    document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
    return true;
  }

  remove(sKey, sPath, sDomain) {
    if (!sKey || !this.hasItem(sKey)) { return false; }
    document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
    return true;
  }

  has(sKey) {
    return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[-.+*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
  }

  keys() {
    var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
    for (var nIdx = 0; nIdx < aKeys.length; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
    return aKeys;
  }

}

// 默认参数项
class Config extends Cookie {
  // cookie存储作用域
  cookieScope = ''
  // 配置文件存储路径
  filePath = ''

  constructor(params) {
    super()

    let defaultValue = {
      cookieScope: 'configScope',
      filePath: undefined,
    }
    params = Object.assign(defaultValue, params)

    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }

  }


  async load(params) {
    let defaultValue = {
      reload: false
    }
    params = Object.assign(defaultValue, params)
    let cookieValue = this.get(this.cookieScope)
    if (!params.reload) {
      if (!!cookieValue && typeof cookieValue == 'object') {
        return cookieValue
      }
    }

    var configScope_
    await fetch(this.filePath, { method: 'get' })
      .then(response => response.text())
      .then(result => {
        configScope_ = JSON.parse(result)
      })
      .catch(error => console.log('error', error));

    this.set(this.cookieScope, JSON.stringify(configScope_))
    return configScope_
  }

  async reload() {
    await this.load({ reload: true })
    window.location.reload()
  }

  getItem(key, params) {
    let defaultValue = {
      reload: false
    }
    params = Object.assign(defaultValue, params)
    if (params.reload) this.load(params)

    this.load()
    let all = this.get(this.cookieScope)
    if (!key) return all;
    return all[key]
  }

  loadGraphic(params) {
    var parentElement = document.getElementById(params.parentId)
    var res = graphic.setting.createControlPanel(this.getItem())
    if (res[0]) parentElement.appendChild(res[1]);
  }

  updateItemValue(originalObject, updateValue) {
    if ((typeof originalObject.value == 'object') && originalObject.type == 'radioList'){
      for (let [key, value] of Object.entries(originalObject.value)) {
        let check = (value.value.toString() == updateValue)
        value.checked = check
      }
      return
    }

    if ((typeof originalObject.value == 'object') && (typeof updateValue == 'object')) {
      for (let [key, value] of Object.entries(updateValue)) {
        // TODO: 关于自自调用这里有些问题 有时间再看
        // return this.updateItemValue(originalObject.value[key], value)
        originalObject.value[key].value = value
      }
      return
    }
    originalObject.value = updateValue
  }

  async saveThreshold(params) {
    let defaultValue = {
      reload: true
    }
    params = Object.assign(defaultValue, params)

    // let formData = new FormData(document.querySelector('#'+params.formId))
    // let params = Object.fromEntries(formData.entries())

    // TODO: 后期使用原生js实现
    let datas = $('#' + params.formId).serializeJSON()['setting']
    let originalDatas = this.get(this.cookieScope)

    for await (let [key, value] of Object.entries(datas)) {
      this.updateItemValue(originalDatas[key], value)
    }

    this.set(this.cookieScope, JSON.stringify(originalDatas))
    if (params.reload) window.location.reload()
  }
}
