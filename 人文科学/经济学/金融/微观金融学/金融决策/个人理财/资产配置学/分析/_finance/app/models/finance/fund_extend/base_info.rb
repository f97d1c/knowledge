module Finance
  module FundExtend
    class BaseInfo < Finance::ApplicationRecord
      self.table_name = 'finance_fund_base_infos'
      belongs_to :fund, class_name: 'Finance::Fund', foreign_key: 'code', primary_key: 'code'
      get_columns(base_except_add: ['code'])

    end 
  end 
end