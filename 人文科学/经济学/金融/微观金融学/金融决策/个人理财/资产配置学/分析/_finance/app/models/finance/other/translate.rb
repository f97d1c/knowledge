module Finance
  module Other
    module Translate
      extend self

      def to_chinese(word)
        res = Dictionary.find_entry(word: word)
        return word unless res[0]
        res[1].chinese
      end 

      def hash_keys_to_chinese(hash: nil, array: nil)
        tmp_ = []
        [hash, array].flatten.compact.each do |hash_|
          tmp = {}
          hash_.each do |key, value|
            tmp.merge!({(to_chinese(key)) => value})
          end 
          return tmp if hash
          tmp_ << tmp
        end

        [true, tmp_]
      end 

    end
  end
end