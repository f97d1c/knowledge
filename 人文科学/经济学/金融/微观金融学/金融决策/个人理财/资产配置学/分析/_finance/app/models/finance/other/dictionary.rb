module Finance
  module Other
    module Dictionary
      extend self

      def find_entry(word:)
        entry = dictionary[word]
        return [false, "未找到与#{word}有关的词条"] if entry.nil?
        [true, OpenStruct.new({word: word}.merge!(entry))]
      end

      private

        def dictionary
          return @dictionary_hash if @dictionary_hash

          @dictionary_hash = {
            date: {chinese: '日期', color: '#00BFFF'},
            hold_cost: {chinese: '持仓成本', color: '#4682B4'},
            hold_num: {chinese: '持仓数量', color: '#ADFF2F'},
            date_profit: {chinese: '日收益', color: '#FF69B4'},
            hold_profit: {chinese: '持有收益', color: '#FF6347'},
            profit_ratio: {chinese: '收益率', color: '#ADFF2F'},
            name: {chinese: '名称', color: '#F08080'},
            product_name: {chinese: '产品名称', color: ''},
            record_date: {chinese: '时间', color: ''},
            code: {chinese: '股票/基金代码', color: ''},
            unit_worth: {chinese: '单位净值', color: ''},
            history_worth: {chinese: '历史净值', color: ''},
            amplitude: {chinese: '振幅', color: ''},
            fund_type: {chinese: '基金类型', color: ''},
            established_at: {chinese: '成立时间', color: ''},
            deal_at: {chinese: '交易时间', color: ''},
            deal_type: {chinese: '交易类型', color: ''},
            deal_num: {chinese: '交易份额', color: ''},
            deal_unit_worth: {chinese: '交易成本价', color: ''},
          }

          @dictionary_hash.merge!(@dictionary_hash.deep_stringify_keys)
          @dictionary_hash.merge!(@dictionary_hash.deep_symbolize_keys)
        end 
    end 
  end
end 