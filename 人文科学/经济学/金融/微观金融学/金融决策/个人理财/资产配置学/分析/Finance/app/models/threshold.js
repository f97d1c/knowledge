import ApplicationRecord from './applicationRecord.js';
export default class Threshold extends ApplicationRecord {

  constructor(params) {
    let defaultValue = {
      keyPath: '别名',
    }
    params = Object.assign(defaultValue, params)
    super(params)
  }

  static schema() {
    return {
      structure: [
        { name: "别名", type: 'string', unique: true, keyPath: true },
        { name: "配置参数", type: 'string' },
        { name: "创建时间", type: 'string', default: () => { return new Date().toJSON() } },
        { name: "更新时间", type: 'string', default: () => { return new Date().toJSON() }, sortBy: 'prev' },
      ],
      version: 20200121,
    }
  }

  static async loadServerData(params, callBack){
    let defaultValue = {
    }
    params = Object.assign(defaultValue, params)
    
    let newDatas
    await fetch('/config/threshold.json', { method: 'get' })
      .then(response => response.text())
      .then(result => {
        newDatas = JSON.parse(result)
      })
      .catch(error => console.log('error', error));

    Threshold.create({objects: newDatas})

    if(!!callBack) return callBack([true, newDatas])
  }


}