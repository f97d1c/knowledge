# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_18_143350) do

  create_table "finance_fund_base_infos", force: :cascade do |t|
    t.string "code", null: false
    t.datetime "established_at", null: false
    t.string "fund_type", null: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_finance_fund_base_infos_on_code"
    t.index ["deleted_at"], name: "index_finance_fund_base_infos_on_deleted_at"
    t.index ["established_at"], name: "index_finance_fund_base_infos_on_established_at"
    t.index ["fund_type"], name: "index_finance_fund_base_infos_on_fund_type"
  end

  create_table "finance_fund_date_worths", force: :cascade do |t|
    t.string "code", null: false
    t.decimal "unit_worth", precision: 20, scale: 4, null: false
    t.decimal "history_worth", precision: 20, scale: 4, null: false
    t.decimal "amplitude", precision: 20, scale: 4, null: false
    t.string "record_date", null: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["amplitude"], name: "index_finance_fund_date_worths_on_amplitude"
    t.index ["code", "record_date"], name: "index_finance_fund_date_worths_on_code_and_record_date", unique: true
    t.index ["code"], name: "index_finance_fund_date_worths_on_code"
    t.index ["deleted_at"], name: "index_finance_fund_date_worths_on_deleted_at"
    t.index ["history_worth"], name: "index_finance_fund_date_worths_on_history_worth"
    t.index ["record_date"], name: "index_finance_fund_date_worths_on_record_date"
    t.index ["unit_worth"], name: "index_finance_fund_date_worths_on_unit_worth"
  end

  create_table "finance_fund_deal_records", force: :cascade do |t|
    t.string "code", null: false
    t.string "deal_type", null: false
    t.decimal "deal_num", precision: 20, scale: 4, null: false
    t.decimal "deal_unit_worth", precision: 20, scale: 4, null: false
    t.datetime "deal_at", null: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_finance_fund_deal_records_on_code"
    t.index ["deal_at"], name: "index_finance_fund_deal_records_on_deal_at"
    t.index ["deal_num"], name: "index_finance_fund_deal_records_on_deal_num"
    t.index ["deal_type"], name: "index_finance_fund_deal_records_on_deal_type"
    t.index ["deal_unit_worth"], name: "index_finance_fund_deal_records_on_deal_unit_worth"
    t.index ["deleted_at"], name: "index_finance_fund_deal_records_on_deleted_at"
  end

  create_table "finance_funds", force: :cascade do |t|
    t.string "name", null: false
    t.string "code", null: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_finance_funds_on_code", unique: true
    t.index ["deleted_at"], name: "index_finance_funds_on_deleted_at"
    t.index ["name"], name: "index_finance_funds_on_name", unique: true
  end

end
