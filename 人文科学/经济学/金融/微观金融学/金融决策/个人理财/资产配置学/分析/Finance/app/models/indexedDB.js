export default class IndexedDB {

  static open(params, callBack) {
    let defaultValue = {
      databaseName: this.name, // 每个库里面只允许有一张表 数据库名称重复就会报错
      tableName: this.name,
      openMode: undefined,
      keyPath: this.keyPath(),
      structure: this.structure,
      dbVersion: this.schema().version,
      class: this,
      sortIndex: this.defaultSort().indexName, // 排序索引名
      sortType: this.defaultSort().type, // 排序方式
    }
    let self = Object.assign(defaultValue, params)

    let request = window.indexedDB.open(self.databaseName, self.dbVersion)
    request.onerror = function (event) {
      callBack([false, "数据库连接失败", event])
    }

    // 数据库不存在 或版本高于已存在版本是执行 相当于自动执行数据库迁移文件
    request.onupgradeneeded = function (event) {
      let db = event.target.result
      let objectStore = db.createObjectStore(self.tableName, { keyPath: self.keyPath, autoIncrement: true })

      self.class.schema().structure.forEach(structure => {
        objectStore.createIndex(structure.name, structure.name, { unique: (structure.unique || false) })
      })
    }

    request.onsuccess = function (event) {
      let db = event.target.result
      let transaction = db.transaction([self.tableName], self.openMode)
      let objectStore = transaction.objectStore(self.tableName)
      callBack([true, objectStore])
    }

  }
}