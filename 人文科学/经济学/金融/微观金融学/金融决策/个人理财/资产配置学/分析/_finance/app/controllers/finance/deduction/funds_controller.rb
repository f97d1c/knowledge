module Finance
  module Deduction
    class FundsController < ApplicationController

      # 应该为高抛低吸模式
      def high_decline_scale_in

        res = Finance::Deduction.high_decline_scale_in

        # 选取各情况最终收益
        final_incomes = []
        final_date = nil
        max_profit_ratio = 0
        @income_max = {}
        res[1].each do |hash|
          object = hash[:objects].last
          final_date = object[:date] if final_date.nil?
          raise '分析结果中最终数据不一致' unless (final_date == object[:date])
          final_incomes << {name: hash[:sheet_name]}.merge!(object)
          @income_max = hash if object[:profit_ratio] > max_profit_ratio
        end 

        @final_incomes = []
        final_incomes.each do |final_income|
          @final_incomes << Translate.hash_keys_to_chinese(hash: final_income)
        end 

        # 从最终收益率中选取最高的进行详细分析
        @params = {type: "line", options: {}}
        @params.merge!({data: {labels: @income_max[:objects].map{|hash| hash[:date]}, datasets: []}})

        ["hold_cost", "date_profit", "hold_num", "hold_profit", "profit_ratio"].each do |key, color|
          @params[:data][:datasets] << {
            label: Translate.to_chinese(key),
            borderColor: Other::Color.get_word_color(word: key),
            data: @income_max[:objects].map{|hash| hash[key.to_sym]}
          }
        end 

=begin
TODO:
0. 页面文字分析完善
0. 部分内容动态填充
=end

        color_params = [
          {"type": "greater_than_zero", "index": 4, "color": "table-danger:table-success", "target": "tr"},
          {"type": "greater_than_zero", "index": 4, "color": "table-danger:table-success", "target": "td"},
          {"type": "greater_than_previous", "index": 3, "color": "table-danger:table-success", "target": "td"}
        ]

        fund_datas = Finance::Trend.all.select("product_name", "record_date", "code", "unit_worth", "amplitude")

        @fund_datas = []
        fund_datas.each do |fund_data| 
          hash = fund_data.slice("product_name", "record_date", "code", "unit_worth", "amplitude")
          @fund_datas << Translate.hash_keys_to_chinese(hash: hash)
        end

        @fund_datas_params = {table_datas: @fund_datas, condictions: color_params}

      end 

    end
  end
end
