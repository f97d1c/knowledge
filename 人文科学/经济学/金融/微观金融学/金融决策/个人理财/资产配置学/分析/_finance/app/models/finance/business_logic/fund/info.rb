module Finance
  module BusinessLogic
    module Fund
      module Info
        extend self
        
        def subject
          method_name = '详细信息'
          explain = '返回查询基金的详细信息'
          parts = [:base_info, :date_worth_list, :date_worth_chart_bronken_line, :position_structure_info, :position_structure_proportion_in_net_worth]

          described = SubjectDescribed.new(bind: binding)
          described.set_attribute(key: 'code', name: '基金代码', explain: '', example: '161725', value_type: :string)
          described.get_described
        end 

        def base_info(show_how: false, **args)
          method_name = '基本信息'
          explain = '返回查询基金的基本信息'

          described = PartDescribed.new(bind: binding)
          self_described = described.get_described
          return self_described if show_how

          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]

          res = Wireway::Spark.satcom(api_code: :finance_fund_base_info, code: args[:code])
          return res unless res[0]

          params = {
            api_code: :list_portrait_base_table,
            base_params: res[1]
          }
          
          [true, params]
        end

        def date_worth_list(show_how: false, **args)
          method_name = '每日净值列表'
          explain = '返回查询日期内基金每日净值的列表展示形式'
          described = PartDescribed.new(bind: binding)
          described.set_attribute(key: 'current_page', name: '当前页数', explain: '', example: nil, default: 1, value_type: :string)
          self_described = described.get_described
          return self_described if show_how
          
          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]

          params = args.clone
          params.merge!({api_code: :finance_fund_daily_market})
          res = Wireway::Spark.satcom(params)
          return res unless res[0]

          params = {
            api_code: :list_horizontal_base_table,
            base_params: res[1]
          }
          
          [true, params]
        end 

        def date_worth_chart_bronken_line(show_how: false, **args)
          method_name = '每日净值折线图'
          explain = '返回查询日期内基金每日净值的图表展示形式'
          described = PartDescribed.new(bind: binding)
          described.set_attribute(key: 'per', name: '每页条数', explain: '每页返回数据条数,以20的倍数为传值标准,小数将向下取整', example: nil, default: 20, value_type: :integer, necessary: false)
          described.set_attribute(key: 'current_page', name: '当前页数', explain: '配合每页条数获取目标范围数据,小数将向下取整', example: nil, default: 1, value_type: :integer, necessary: false)
          described.set_attribute(key: 'object_colors', name: '数据项颜色', explain: '', example: ["red"], value_type: :array)
          
          self_described = described.get_described
          return self_described if show_how
          
          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]

          params = args.clone
          params.merge!({api_code: :finance_fund_daily_market, key_type: 'en'})
          
          res = Wireway::Spark.satcom(params)
          return res unless res[0]
          
          tmp = {}
          line_data = res[1].reverse.map{|hash| tmp.merge!({hash[:net_value_date] => hash[:unit_net_value]})}
          line_datas = {args[:code] => tmp}

          tmp = {
            api_code: :chart_line_broken_line,
            line_datas: line_datas,
            object_colors: args[:object_colors]
          }
          
          [true, tmp]
        end

        def position_structure_info(show_how: false, **args)
          method_name = '持仓结构-详细信息'
          explain = '返回基金所持仓结构-详细信息'

          described = PartDescribed.new(bind: binding)
          self_described = described.get_described
          return self_described if show_how

          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]

          res = Wireway::Spark.satcom(api_code: :finance_fund_position_structure, code: args[:code])

          return res unless res[0]

          params = {
            api_code: :list_horizontal_base_table,
            base_params: res[1],
          }
          
          [true, params]
        end 


        def position_structure_proportion_in_net_worth(show_how: false, **args)
          method_name = '持仓结构-占净值比例'
          explain = '返回基金所持仓结构-占净值比例'

          described = PartDescribed.new(bind: binding)
          self_described = described.get_described
          return self_described if show_how

          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          
          res = Wireway::Spark.satcom(api_code: :finance_fund_position_structure, code: args[:code], data_format: ["stock_name", "proportion_in_net_worth"].to_json)

          return res unless res[0]
          
          tmp = {}
          res[1].map{|hash| tmp.merge!([hash.values].to_h)}
          
          colors = ["#007bff","#6610f2","#6f42c1","#e83e8c","#dc3545","#fd7e14","#ffc107","#28a745","#20c997","#17a2b8"]
          part_colors = []
          tmp.values.size.times{ part_colors << colors.sample}

          params = {
            api_code: :chart_other_doughnut,
            parts: tmp.to_json,
            part_colors: part_colors,
          }
          
          [true, params]
        end 

      end
    end
  end
end