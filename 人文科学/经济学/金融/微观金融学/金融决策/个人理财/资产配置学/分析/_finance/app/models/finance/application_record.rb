module Finance
  class ApplicationRecord < ActiveRecord::Base
    self.abstract_class = true

    include Other::GetColumns

    # 界定提供数据中给出字段的极端值范围
    def self.column_datas_range(datas:, column:)
      
      # 无法对字符串进行排序
      # min_data = datas.select(column).min
      # max_data = datas.select(column).max

      column_datas = datas.map(&column.to_sym)
      min_value = column_datas.min
      max_value = column_datas.max
      [true, (min_value..max_value)]
    end 

    def self.generate_column_scopes(columns: )
      self.class_eval do 
        columns.each do |colunn_name|
          [:asc, :desc].each do |order_rule|
            # scope "order_by_#{colunn_name}_#{order_rule}", -> (size = nil) {unscoped.order("#{colunn_name} #{order_rule}").limit(size)}
            scope "order_by_#{colunn_name}_#{order_rule}", -> (size = nil) {order("#{colunn_name} #{order_rule}").limit(size)}
          end 
        end
      end 
    end 

    # 导出excel通用方法
    # export_excel(worksheets: [{sheet_name: 'sheet名称', columns: {'列名' => lambad表达式}}], file_name: '文件名称')
    # {worksheets: [{objects: objects, sheet_name: '订单统计', columns: {"采购部门" => lambda{|order| order.dep_name}}], file_name: "tmp/files" }
    def self.export_excel(worksheets: , file_name: , header:{text_color: :blue, text_weight: :bold, text_size: 14, height: 30}, send_data_type: 'text/excel;charset=utf-8; header=present')
      xls_report = StringIO.new
      Spreadsheet.client_encoding = "UTF-8"
      book = Spreadsheet::Workbook.new

      worksheets.each_with_index do |sheet_hash, index|
        sheet_name    = sheet_hash[:sheet_name]
        column_names  = sheet_hash[:columns].keys

        # 初始化表头
        sheet = book.create_worksheet :name => sheet_name
        sheet.row(0).concat column_names
        format = Spreadsheet::Format.new(:color => header[:text_color], :weight => header[:text_weight], :size => header[:text_size])
        sheet.row(0).default_format = format
        sheet.row(0).height = header[:height]

        # 填充表格内容
        row_number = 0
        column_max_size = column_names.map(&:size)
        sheet_hash[:objects].each do |object|
          row_number += 1
          contents = []
          sheet_hash[:columns].values.each_with_index do |lam, index| 
            content = lam.call(object)
            # 比较寻找各列最大数据长度
            column_max_size[index] = [column_max_size[index], content.to_s.size].max
            contents << content
          end 
          sheet.row(row_number).concat(contents)
        end 

        # 设定列宽
        column_max_size.each_with_index do |width,index|
          sheet.column(index).width = width*1.7
        end

      end

      book.write xls_report

      # 根据给定的文件名称是否包含路径以及路径是否存在判定数据移交方式
      dir_path = file_name.split('/')[0...-1].join('/')

      unless dir_path.present?
        send_data(
          xls_report.string,
          :type => send_data_type,
          :filename => "#{Time.now.strftime("%Y-%m-%d_%H%M")}_#{file_name}.xls"
        )
        return [true, '']
      end 

      dir_path = Rails.root.join(dir_path)
      return [false, '未找到给定文件名称所对应的目录', xls_report.string] unless File::directory?(dir_path)
      file_name_ = "#{Time.now.strftime("%Y-%m-%d_%H%M")}_#{file_name.split('/')[-1]}.xls"

      full_path = "#{dir_path}/#{file_name_}"
      File::open(full_path, 'wb') do |file|
        file.write(xls_report.string)
      end 
      
      [true, '已写入文件', {file_path: full_path, worksheets_count: book.worksheets.count}]
    end

    def self.export_markdown(worksheets:, file_name:)

      # 根据给定的文件名称是否包含路径以及路径是否存在判定数据移交方式
      dir_path = file_name.split('/')[0...-1].join('/')

      dir_path = Rails.root.join(dir_path)
      return [false, '未找到给定文件名称所对应的目录'] unless File::directory?(dir_path)
      file_name_ = "#{Time.now.strftime("%Y-%m-%d_%H%M")}_#{file_name.split('/')[-1]}.md"

      full_path = "#{dir_path}/#{file_name_}"
      write_counts = 0
      File::open(full_path, 'wb') do |file|
        worksheets.each_with_index do |sheet_hash, index|
          sheet_name    = sheet_hash[:sheet_name]
          column_names  = sheet_hash[:columns].keys
          # 写入标题
          file.write("# #{sheet_name}\n")
          # 写入表头
          file.write("#{column_names.join('|')}\n")

          # 根据首条记录判断数据的对齐格式
          tmp = []
          object = sheet_hash[:objects].first
          sheet_hash[:columns].values.each do |lam|
            case lam.call(object)
            when BigDecimal, Integer, Float
              tmp << '---:|'
            else
              tmp << '---|'
            end 
          end
          file.write("#{tmp.join('')}\n")

          sheet_hash[:objects].each do |object|
            contents = sheet_hash[:columns].values.map{|lam| lam.call(object)}.join('|')
            file.write("#{contents}\n")
          end 
          write_counts+=1
        end 
      end 
      
      [true, '已写入文件', {file_path: full_path, worksheets_count: write_counts}]
    end 

  end
end
