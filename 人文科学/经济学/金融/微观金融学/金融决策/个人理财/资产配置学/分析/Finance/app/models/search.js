import ApplicationRecord from './applicationRecord.js';
export default class Search extends ApplicationRecord {

  constructor(params) {
    let defaultValue = {
      keyPath: '身份证号',
    }
    params = Object.assign(defaultValue, params)
    super(params)
  }

  static schema() {
    return {
      structure: [
        { name: "对象名", type: 'string' },
        { name: "别名", type: 'string', unique: true, keyPath: true },
        { name: "查询条件", type: 'string' },
        { name: "查询内容", type: 'string' },
        { name: "创建时间", type: 'string', default: () => { return new Date().toJSON() } },
        { name: "更新时间", type: 'string', default: () => { return new Date().toJSON() }, sortBy: 'prev' },
      ],
      version: 20200121,
    }
  }

  static formatForm(params) {
    let defaultValue = {
      form: document.querySelector('form'),
    }
    params = Object.assign(defaultValue, params)

    let formData = new FormData(params.form)
    let formatData = { ransack: {}, search: {}, mix: {} }
    for (let formKey of formData.keys()) {
      let formKeys = formKey.split(/(search|ransack)\[(.*)\]/).filter(item => item != '')
      if (formKeys.length != 2) continue;
      formatData[formKeys[0]][formKeys[1]] = formData.get(formKey)
    }

    for (let [key, value] of Object.entries(formatData.ransack)) {
      formatData['mix'][key + '_' + value] = (formatData.search[key] || '')
    }

    return [true, formatData]
  }

  static saveCondiction(params) {
    let conditionName = prompt("输入条件名称", '')
    if (conditionName == null || conditionName == "") return alert('条件名称不能为空.')

    let defaultValue = {
      objectName: undefined,
    }
    params = Object.assign(defaultValue, params)

    let res = this.formatForm()
    if (!res[0]) return alert(res[1])

    let object = {
      "对象名": params.objectName,
      "别名": conditionName,
      "查询条件": res[1].ransack,
      "查询内容": res[1].search,
    }
    this.create({ object: object }, result => {
    })

  }

  async fillingForm(params) {
    let defaultValue = {
      onchange: undefined,
    }
    params = Object.assign(defaultValue, params)

    for await (let [key, value] of Object.entries(this.查询内容)) {
      document.getElementsByName('search[' + key + ']').forEach(input => {
        // input.setAttribute('value', value) // 这种赋值 form.reset() 无法清除
        input.value = value
      })
    }

    for await (let [key, value] of Object.entries(this.查询条件)) {
      document.getElementsByName('ransack[' + key + ']').forEach(select => {
        // select.setAttribute('value', value) 这种赋值没反应
        select.value = value
      })
    }

    if (params.onchange) params.onchange
  }

}

