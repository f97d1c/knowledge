module Finance
  module Template
    module Params
      extend self

      def chart_doughnut(params:, div: {style: "width: 50%; height: 550px; float: left;"})
        base_dashboard(classify: 'chart', type: 'other', method_name: 'doughnut', div: div, params: params)
      end

      def dark_list(params:, div: {})
        base_dashboard(classify: 'list', type: 'base', method_name: 'dark_list', div: div, params: params)
      end

      private
        def base_dashboard(classify:, type:, method_name:, div: nil, params: )
          {
            classify: classify, 
            type: type, 
            method_name: method_name, 
            div: div,
            params: params
          }
        end 
    end
  end
end