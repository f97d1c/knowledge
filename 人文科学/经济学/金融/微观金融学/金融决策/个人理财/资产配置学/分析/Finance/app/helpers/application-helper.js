class ApplicationHelper {
  static loadAssetTag(str) {
    let array_ = str.split(/\n/)
    let array = array_.filter(function (obj) { return (obj != ' ') })
    for (let item of array) {
      let attributes = item.split(/ |\>/)
      let tagName = attributes[0].split('<')[1]
      let target = document.createElement(tagName)

      let usefulAttributes = attributes.filter(function (str) { return (/href|rel|media|src/.test(str)) })
      for (let attributes of usefulAttributes) {
        let arr = attributes.split('=')
        target.setAttribute(arr[0], arr[1].replace(/\"/g, ''))
      }
      document.head.appendChild(target)
    }
  }

  static async waitLoadAssets(callBack) {
    if (callBack()) return;
    for (let i = 0; i < 100; i++) {
      let sleepThreshold = 1
      await new Promise(resolve => setTimeout(resolve, sleepThreshold))
      console.log('waiting assets ready: ' + (i + 1) * sleepThreshold + 'ms')
      if (callBack()) break;
    }
  }

  static async requestRelayAssets(rely_assets, judgeLoadedCallBack, callBack) {
    let body = {
      api_code: 'publish_other_request_assets',
      rely_assets: rely_assets,
      response_type: 'json',
    }

    let requestOptions = {
      method: 'POST',
      redirect: 'follow',
      mode: 'cors',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(body),
    }

    await fetch("http://123.57.155.17:3111/dashboard/restful/request_api", requestOptions)
      .then(response => response.text())
      .then(result => {
        let resObj = JSON.parse(result)
        if (!resObj.success) return [false, (resObj.result || '返回内容为空'), resObj]
        ApplicationHelper.loadAssetTag(resObj.result)
        return [true, '']
      })
      .catch(error => console.log('error', error))

    await ApplicationHelper.waitLoadAssets(judgeLoadedCallBack)
    if (typeof (callBack) == 'function') callBack()
  }

  static changeWidth(id, width) {
    document.getElementById(id).setAttribute('style', 'width: ' + width + '%;')
  }

  static async layout(params, callBack) {
    let defaultValue = {
      templatePath: undefined,
    }
    params = Object.assign(defaultValue, params)

    await fetch(params.templatePath, { method: 'get' })
      .then(response => response.text())
      .then(result => {
        let parser = new DOMParser()
        let doc = parser.parseFromString(result, 'text/html')
        if (typeof callBack == 'function') return callBack([true, doc])

        let element = doc.getElementById('space')
        for (let template of document.getElementsByTagName("template")) {
          let clone = template.content.cloneNode(true)
          element.appendChild(clone)
        }

        let html = document.getElementsByTagName('html')[0]
        html.parentNode.replaceChild(doc.documentElement, html)
      })
      .catch(error => console.log('error', error));
  }

}