import IndexedDB from './indexedDB.js'
export default class ApplicationRecord extends IndexedDB {

  constructor(params) {
    super()

    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }

    let object = this
    let keyPathValue = this[this.constructor.keyPath()]
    if (keyPathValue) {
      this.constructor.find(keyPathValue, result => {
        object.newRecord = !result[0]
      })
    } else {
      this.newRecord = true
    }

  }

  static keyPath() {
    let array = this.schema().structure.filter(hash => { return hash.keyPath })
    if (array.length == 0) return 'id'
    return array[0].name
  }

  static defaultSort() {
    let array = this.schema().structure.filter(hash => { return hash.sortBy })
    if (array.length == 0) return { indexName: this.keyPath(), type: 'next' }

    return { indexName: array[0].name, type: array[0].sortBy }
  }

  static all(params, callBack) {
    let defaultValue = {
      sortIndex: this.defaultSort().indexName, // 排序索引名
      sortType: this.defaultSort().type, // 排序方式
      f5:false,
      class: this,
      loadMethod: this.loadServerData,
    }
    params = Object.assign(defaultValue, params)

    this.open(params, result => {
      if (!result[0]) return callBack(result)
      result[1].index(params.sortIndex).openCursor(null, params.sortType).onerror = function (event) {
        callBack([false, '数据获取时异常', event])
      }

      let datas = []
      result[1].index(params.sortIndex).openCursor(null, params.sortType).onsuccess = function (event) {
        let result = event.target.result
        if (result) {
          datas.push(result.value)
          result.continue()
        } else {
          if (params.f5){
            params.loadMethod({}, result => {
              if (result[0]) Array.prototype.push.apply(datas, result[1]);
              return callBack([true, datas])
            })
          }
          return callBack([true, datas])
        }
      }
      // return callBack([true, datas])
      // loadServerData
    })
  }

  static find(keyPathValue, callBack) {
    this.open({}, result => {
      if (!result[0]) return callBack(result)
      let request = result[1].get(keyPathValue);
      request.onerror = function (event) {
        callBack([false, '数据获取时异常', event])
      }
      request.onsuccess = function (event) {
        callBack([true, request.result])
      };
    })
  }

  // 根据属性索引查找结果(多条仅返回键值最小的那个) 前提: 该属性有创建索引
  static findBy(params, callBack) {
    let defaultValue = {
    }
    let keys = Object.keys(params).filter(key => { return !Object.keys(defaultValue).includes(key) })
    let key = keys[0]
    let value = params[key]

    params = Object.assign(defaultValue, params)

    this.open(params, result => {
      if (!result[0]) return callBack(result)
      let index = result[1].index(key)

      index.get(value).onerror = function (event) {
        callBack([false, '数据获取时异常', event])
      }

      index.get(value).onsuccess = function (event) {
        callBack([true, event.target.result])
      }

    })

  }

  // 根据属性索引查找结果(所有) 前提: 该属性有创建索引
  static where(params, callBack) {
    let defaultValue = {
      openMode: undefined,
    }
    let keys = Object.keys(params).filter(key => { return !Object.keys(defaultValue).includes(key) })
    let key = keys[0]
    let value = params[key]
    params = Object.assign(defaultValue, params)

    this.open(params, result => {
      if (!result[0]) return callBack(result)
      let index = result[1].index(key)
      let singleKeyRange = IDBKeyRange.only(value);

      index.openCursor(singleKeyRange).onerror = function (event) {
        callBack([false, '数据获取时异常', event])
      }

      let datas = []
      index.openCursor(singleKeyRange).onsuccess = function (event) {
        let result = event.target.result
        if (result) {
          datas.push(result.value)
          result.continue()
        } else {
          return callBack([true, datas])
        }
      }
    })

  }

  // 如果数据库初始化的时候要调用这个方法需要跳过回调
  static create(params, callBack = () => { }) {
    let defaultValue = {
      openMode: 'readwrite',
      objects: [],
      object: undefined,
      parent: this,
      async: false,
    }
    params = Object.assign(defaultValue, params)

    this.open(params, async result => {
      if (!result[0]) return callBack(result)

      result[1].openCursor().onerror = function (event) {
        callBack([false, '数据获取时异常', event])
      }

      let ejection = false
      if (params.object) params.objects.push(params.object)

      if(params.async){
        for await (let object of params.objects ){
          params.parent.beforeCreate(object)
          let request = result[1].add(object)
  
          request.onsuccess = function (event) {
  
          }
          request.onerror = function (event) {
            ejection = true
            return callBack([false, '创建过程中异常', event, object])
          }
        }
      }else{
        params.objects.forEach(function (object) {
          params.parent.beforeCreate(object)
          let request = result[1].add(object)
  
          request.onsuccess = function (event) {
  
          }
          request.onerror = function (event) {
            ejection = true
            return callBack([false, '创建过程中异常', event, object])
          }
          // if (ejection) break
        })
      }
      
      if (!ejection) callBack([true, ''])

    })

  }

  // 根据keyPath删除 前提: 表结构中有指定keypath
  static delete(params, callBack) {
    let defaultValue = {
      openMode: 'readwrite',
      onlyOne: true,
      allowEmpty: true,
    }
    let keys = Object.keys(params).filter(key => { return !Object.keys(defaultValue).includes(key) })
    let key = keys[0]
    let value = params[key]

    params = Object.assign(defaultValue, params)

    this.open(params, result => {
      if (!result[0]) return callBack(result)

      let index = result[1].index(key)
      let singleKeyRange = IDBKeyRange.only(value);

      index.openCursor(singleKeyRange).onerror = function (event) {
        callBack([false, '数据获取时异常', event])
      }

      let objects = []
      index.openCursor(singleKeyRange).onsuccess = function (event) {
        let result_ = event.target.result
        if (result_) {
          objects.push(result_.value)
          result_.continue()
        } else {
          if (params.onlyOne && (objects.length > 1)) return callBack([false, '给定条件筛选结果不唯一', objects])
          if (params.allowEmpty && (objects.length == 0)) return callBack([true, ''])
          if (!params.allowEmpty && (objects.length == 0)) return callBack([false, '给定条件筛选结果为空'])

          let ejection = false
          objects.forEach(object => {
            let request = result[1].delete(object[result[1].keyPath])
            request.onerror = function (event) {
              ejection = true
              return callBack([false, '对象删除时异常', event, object])
            }
          })

          if (!ejection) return callBack([true, ''])
        }
      }
    })

  }

  // 根据keyPath删除(所有) 前提: 表结构中有指定keypath
  static deleteAll(params, callBack) {
    let defaultValue = {
      onlyOne: false,
    }
    params = Object.assign(defaultValue, params)
    this.delete(params, result => {
      callBack(result)
    })
  }

  update(attributes, callBack) {
    let self = this
    this.constructor.open({ openMode: 'readwrite' }, result => {
      if (!result[0]) return callBack(result)

      let request = result[1].get(self[self.keyPath])
      request.onerror = function (event) {
        callBack([false, '数据获取时异常', event])
      }
      request.onsuccess = function (event) {
        let object = event.target.result
        for (let [key, value] of Object.entries(attributes)) {
          object[key] = value
        }

        let objectUpdate = result[1].put(object);
        objectUpdate.onerror = function (event) {
          callBack([false, '数据更新失败', event])
        }
        objectUpdate.onsuccess = function (event) {
          callBack([true, ''])
        }

      }
    })
  }

  // 根据keyPath删除 前提: 表结构中有指定keypath
  delete(callBack) {
    let self = this
    if (this.newRecord || this.newRecord == undefined) return callBack([false, '对象尚未完成初始化, 无法获取主键信息'])

    this.constructor.open({ openMode: 'readwrite' }, result => {
      if (!result[0]) return callBack(result)
      let request = result[1].delete(self[self.keyPath])
      request.onerror = function (event) {
        callBack([false, '数据删除时异常', event])
      }
      request.onsuccess = function (event) {
        callBack([true, ''])
      }

    })

  }

  static async loadServerData(params) {
  }

  static beforeCreate(object) {
    let defaultColumns = this.schema().structure.filter(hash => { return hash.default })
    if (!defaultColumns) return;

    for (let defaultColumn of defaultColumns) {
      if (object[defaultColumn.name]) continue
      if (typeof defaultColumn.default == 'function') {
        object[defaultColumn.name] = defaultColumn.default()
        continue
      }
      object[defaultColumn.name] = defaultColumn.default
    }

  }

}