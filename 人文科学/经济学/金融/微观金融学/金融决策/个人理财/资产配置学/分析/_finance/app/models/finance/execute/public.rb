module Finance
  module Execute
    module Public
      extend self

      # Finance::Execute::Public::exposed_api
      def exposed_api
        tmp = {}
        Finance::BusinessLogic::Public::exposed_api.each do |key, value|
          extra_params = [
            {key: 'api_code', name: '接口标识', explain: '访问该接口的对应唯一标识', regular: nil, default: nil, example: key, necessary: true, value_type: 'string'},
            {key: 'response_type', name: '响应类型', explain: '根据请求内容返回html/json类型响应内容', regular: /^[^\s]+$/, default: nil, example: 'html', necessary: true, value_type: 'string'},
          ]

          value[:attributes] = (value[:attributes] + extra_params)
          tmp.merge!({key => value})
        end
        tmp
      end 

    end 
  end
end