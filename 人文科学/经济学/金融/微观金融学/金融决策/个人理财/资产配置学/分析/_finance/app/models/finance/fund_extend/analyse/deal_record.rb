module Finance
  module FundExtend
    module Analyse
      module DealRecord
        extend self

        def columns(object: Finance::Fund)
          res = base_datas(object: object)
          return res unless res[0]

          base = {
            title: '基础数据列表',
            displays: [
              Finance::Template::Params.dark_list(params: {base_params: Other::Translate.hash_keys_to_chinese(array: res[1])[1]}),
            ]
          }

          group_by_deal_type = res[1].group_by{|hash| hash['deal_type']}
          group_by_deal_type.values.each do |array|
            base[:displays] << Finance::Template::Params.dark_list(params: {base_params: Other::Translate.hash_keys_to_chinese(array: array)[1]})
          end 

          [base, deal_type(object: object), deal_num(object: object)]
        end 

        private

          def base_datas(object: )
            return @base_datas if @base_datas
            @base_datas = object.get_deal_records
          end 

          def deal_type(object: )
            res = base_datas(object: object)
            return res unless res[0]
            group_by_deal_type = res[1].group_by{|hash| hash['deal_type']}

            {
              title: '交易类型分析',
              displays: [
                Finance::Template::Params.chart_doughnut(params: {base_params: group_by_deal_type.map{|k, v| [k+'次数', [['data', v.size]].to_h]}.to_h}),
                Finance::Template::Params.chart_doughnut(params: {base_params: group_by_deal_type.map{|k, v| [k+'金额', [['data', v.map{|record| record['deal_num'] * record['deal_unit_worth']}.sum.round(4)]].to_h]}.to_h})
              ]
            }
          end 

          def deal_num(object: )
            res = base_datas(object: object)
            return res unless res[0]
            group_by_deal_type = res[1].group_by{|hash| hash['deal_type']}

            {
              title: '交易数量分析',
              displays: [
                Finance::Template::Params.chart_doughnut(params: {base_params: group_by_deal_type.map{|k, v| [k+'数量总数', [['data', v.map{|record| record['deal_num']}.sum]].to_h]}.to_h})
              ]
            }
          end 

          def deal_unit_worth(object: )

          end 

      end
    end
  end
end