import LocalStorage from './localStorage.js';
export default class Fund extends LocalStorage {
  // 存储作用域

  constructor(params) {
    let defaultValue = {
      storeScope: 'Fund',
      storeRange: '-1weeks',
    }
    params = Object.assign(defaultValue, params)
    super({
      storeScope: params.storeScope,
      storeRange: params.storeRange,
    })

  }

  packageItems(objects, params) {
    let defaultValue = {
      rowCount: 5
    }
    params = Object.assign(defaultValue, params)

    let parentElement = document.getElementById(params.parentId)
    parentElement.innerHTML = ""

    let rowCount = parseInt(params.rowCount)

    for (let i = 0, len = objects.length; i < len; i += rowCount) {
      let objects_ = objects.slice(i, i + rowCount)
      var div = document.createElement('div')
      div.setAttribute('style', 'margin: 0 auto; display: inline-block; width: 100%;')
      div.setAttribute('class', 'mt-1')

      for (let object of objects_) {
        let number = parseInt((object['日增长率'] || object['daily_growth_rate'] || '').split('.')[0])
        let title = object['基金代码']
        let clas
        if (title) {
          title = (object['基金代码']||object['fund_code'])+'('+(object['净值日期']||object['net_value_date'])+')'
          clas = ['bg-success', 'bg-danger'][(number >= 0 ? 1 : 0)]
        }else{
          clas = 'bg-dark'
        }

        let res = graphic.cardTable.renderCardTable(object, { title: title, divBgClass: clas, rowCount: rowCount })
        if (res[0]) div.appendChild(res[1]);
      }
      parentElement.appendChild(div)
    }
  }

  loadItems(params){
    let requestCallBack = async (callBack) => {
      
      if (params.workMode == 0){
        await wireway.satcom({api_code: "other_test_data_people", data_size: 100}, function(result){
          if (!result[0] || !result[1].success) return callBack([false, "原始数据请求失败", result]);
          let datas = result[1].result
          callBack([true, datas, result])
        })
      }

      if (params.workMode == 1){
        await wireway.satcom({api_code: "finance_fund_daily_market", code: '377240'}, async function(result){
          if (!result[0] || !result[1].success) return;
          let datas = result[1].result
          callBack([true, datas, result])
        })
      }

    }

    let packageCallBack = (datas) => {
      this.packageItems(datas, {parentId: params.parentId, rowCount: params.rowCount})
    }

    this.autoLoad(params, packageCallBack, requestCallBack)
  }

}