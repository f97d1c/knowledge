module Finance
  module FundExtend
    class AnalysesController < ApplicationController

      def overview
        @overview = {
          '基金基本信息分析' => Finance::FundExtend::Analyse::BaseInfo.columns,
          '基金交易记录分析' => Finance::FundExtend::Analyse::DealRecord.columns,
        }
      end 

      def base_info
        @analyse_through_base_info =  Finance::FundExtend::Analyse::BaseInfo.columns
      end 

      def deal_record
        @analyse_through_deal_record =  Finance::FundExtend::Analyse::DealRecord.columns
      end 

    end
  end
end