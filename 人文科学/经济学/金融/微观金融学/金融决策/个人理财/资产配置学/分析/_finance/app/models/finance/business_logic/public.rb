module Finance
  module BusinessLogic
    module Public
      extend self

      # Finance::BusinessLogic::Public::exposed_api
      def exposed_api
        exposed_api_flat_
      end 

      private
        def exposed_api_
          {
            finance: {
              fund: {
                info: Fund::Info::subject,
              }
            }
          }
        end

        # 将多层级Hash结构变为浅层
        def exposed_api_flat_
          tmp = {}
          exposed_api_.each do |key, value|
            value.each do |ke, val|
              val.each do |k,v|
                tmp.merge!({"#{key}_#{ke}_#{k}".to_sym => v})
              end
            end 
          end
          tmp
        end

    end 
  end
end