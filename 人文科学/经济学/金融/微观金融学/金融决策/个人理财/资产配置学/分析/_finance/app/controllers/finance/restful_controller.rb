module Finance
  class RestfulController < ApplicationController
    
    def example
      @exposed_api = Finance::Execute::Public::exposed_api
    end

    def request_api_params
      @api_code = params[:api_code]
      exposed_api = Finance::Execute::Public::exposed_api
      @current_api = exposed_api[params[:api_code].to_sym]
      
      return render html: (render_to_string "finance/restful/api_form")
    end 

    def request_api
      # TODO 初步了解为rails5的安全机制 去除存在风险 暂时先这样
      args = params.permit!.to_hash.reject!{|key| ["action", "controller"].include?(key)}

      res = Finance::Execute::Entrance::run(args.deep_symbolize_keys)
      
      case params[:response_type]
      when 'json', 'html'
        send("request_api_response_#{params[:response_type]}", {res: res})
      else
        render html: '未知response_type' 
      end
      
    end 

    def test

    end 

    private

      def request_api_response_json(res:)
        hash = {success: res[0]}.merge!(res[1].is_a?(Hash) ? res[1] : {result: res[1]})
        render json: hash.merge!(res[2] || {})
      end

      def request_api_response_html(res:)
        return (render html: res[1]) unless res[0]
        render html: (res[1].html_safe)
      end
  end
end