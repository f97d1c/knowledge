module Finance
  class Fund < ApplicationRecord
    # 基金主体 
    # 该文件内没有具体逻辑实现 只是对通用关系定义及相关内容集成

    # 基金基本信息
    has_one :base_info, class_name: 'Finance::FundExtend::BaseInfo', foreign_key: 'code', primary_key: 'code'
    # 基金每日净值等信息
    has_many :date_worths, class_name: 'Finance::FundExtend::DateWorth', foreign_key: 'code', primary_key: 'code'
    # 基金交易记录
    has_many :deal_records, class_name: 'Finance::FundExtend::DealRecord', foreign_key: 'code', primary_key: 'code'
    # 定义展示字段
    get_columns(relations: ['base_info_zh', 'date_worths_zh', 'deal_records_zh'])

    class << self
 

    end 

  end
end