module Finance
  module Deduction
    extend self

    def check_records(records:)
      return [false, '数据不能为空'] unless records.present?

      return [false, '选取数据类型及编号不唯一'] unless records.select('code, mold').distinct.count == 1

      res = Record.column_datas_range(datas: records, column: :record_date)
      return res unless res[0]
      date_range = res[1]

      return [false, '数据完整性异常, 存在缺失'] unless (date_range.to.count == records.size)

      [true, '']
    end

    def deduction(records:)
      res = check_records(records: records)
      return res unless res[0]
      # Finance::Trend.order_by_unit_worth_desc(4).size
      
    end 

    # 在高幅下跌时按一定比例进行加仓
    # Finance::Deduction.high_decline_scale_in
    def high_decline_scale_in(hold_num: 1000, deduction_trends: Finance::Trend.all, situations: [])

      # 分母为10的结果太大了
      # [5, 10].each do |denominator|
      [5].each do |denominator|
        (1..denominator).to_a.each do |molecule|
          situations << [molecule, denominator]
        end 
      end if situations == []

      tmp = []
      situations.each_with_index do |array, index|
        molecule, denominator = array
        action_trends = Finance::Trend.order_by_amplitude_asc(denominator)

        res = deduction_base(action_trends: action_trends, deduction_trends: deduction_trends, hold_num: hold_num) do |deduction, action_trend|
          add_num = ((molecule/denominator.to_f).round(6) * deduction.hold_num).round
          deduction.hold_cost += (action_trend.unit_worth * add_num)
          deduction.hold_num += add_num
        end 

        return res unless res[0]
        worksheet = {
          objects: res[1], 
          sheet_name: "根据下跌幅度分析-#{molecule}\/#{denominator}", 
        }
        tmp << worksheet
        # tmp << export_excel(worksheet: worksheet, export_now: (situations.size == index+1 ? true : false))
        # tmp << export_markdown(worksheet: worksheet, export_now: (situations.size == index+1 ? true : false))
      end
      
      [true, tmp]
    end 


    def deduction_base(action_trends:, deduction_trends:, hold_num: )
      tmp = []
      deduction = OpenStruct.new(hold_cost:0.0, hold_profit: 0.0, hold_num: hold_num)
      prior_trend = nil

      deduction_trends.order_by_record_date_asc.each_with_index do |trend, index|
        begin
          if index == 0
            deduction.hold_cost += (trend.unit_worth * deduction.hold_num)
            prior_trend = trend
          end 
  
          date_profit = (trend.unit_worth - prior_trend.unit_worth)*deduction.hold_num
          deduction.hold_profit += date_profit

          tmp << {
            date: trend.record_date, 
            hold_cost: deduction.hold_cost.to_f.round(6), 
            date_profit: date_profit.to_f.round(6), 
            hold_num: deduction.hold_num, 
            hold_profit: deduction.hold_profit.to_f.round(6), 
            profit_ratio: ((deduction.hold_profit.to_f/deduction.hold_cost.to_f)*100).round(4)
          }
  
          if action_trends.include?(trend)
            yield(deduction, trend) if block_given?
          end 
          prior_trend = trend
        rescue
          return [false, '运行时异常', {errors: {message: $!, path: $@}}]
        end
      end  
      [true, tmp] 
    end 

    private
      def export_excel(worksheet:, export_now: false)
        columns_hash = {columns: export_columns_hash}

        (@worksheets_excel||=[]) << worksheet.merge!(columns_hash)
        return [true, "#{worksheet[:sheet_name]}已加入队列,等待触发导出"] unless export_now

        params = {
          worksheets: @worksheets_excel, 
          file_name: "tmp/files/订单统计" 
        }

        res = ApplicationRecord.export_excel(params)
      end 

      def export_markdown(worksheet:, export_now: false)
        columns_hash = {columns: export_columns_hash}

        (@worksheets_markdown||=[]) << worksheet.merge!(columns_hash)
        return [true, "#{worksheet[:sheet_name]}已加入队列,等待触发导出"] unless export_now

        params = {
          worksheets: @worksheets_markdown, 
          file_name: "tmp/files/订单统计" 
        }
        res = ApplicationRecord.export_markdown(params)
      end 

      def export_columns_hash
        {
          "时间" => lambda{|hash| hash[:date]},
          "持仓成本" => lambda{|hash| hash[:hold_cost]},
          "日收益" => lambda{|hash| hash[:date_profit]},
          "持有数量" => lambda{|hash| hash[:hold_num]},
          "持有收益" => lambda{|hash| hash[:hold_profit]},
          "收益率" => lambda{|hash| hash[:profit_ratio]},
        }
      end 

  end
end
