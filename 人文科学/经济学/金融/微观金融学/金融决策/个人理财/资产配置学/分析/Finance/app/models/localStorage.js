String.prototype.hashCode = function () {
  let h;
  for(let i = 0; i < this.length; i++) 
    h = Math.imul(31, h) + this.charCodeAt(i) | 0;
  return h;
}

export default class LocalStorage {
  // 存储记录作用域
  storeScope = ''
  // 存储时间范围
  storeRange = ''

  constructor(params) {
    let defaultValue = {
      storeScope: 'default',
      storeRange: '-1weeks',
    }
    params = Object.assign(defaultValue, params)

    for (let [key, value] of Object.entries(params)) {
      if (key == 'storeScope') value = '[' + value + ']'
      this[key] = value
    }

  }

  timeStandard(time) {
    return {
      string: time.toLocaleString(),
      json: time.toJSON(),
      year: time.getFullYear(),
      month: time.getMonth(),
      date: time.getDate(),
      hour: time.getHours(),
      min: time.getMinutes(),
    }
  }

  timeFormat(word) {
    let afterFormat
    let today = new Date()

    let aroundToday = (word, multiple) => {
      let number = word.split(/([\+\-][\d\.]*)/g).filter(d => d)[0]
      let days = parseFloat(number) * multiple
      return new Date(new Date().valueOf() + (days * 24 * 60 * 60 * 1000))
    }

    switch (true) {
      case /^[\+\-][\d\.]*min/.test(word):
        afterFormat = aroundToday(word, 1 / 24 / 60)
        break
      case /^[\+\-][\d\.]*hour/.test(word):
        afterFormat = aroundToday(word, 1 / 24)
        break
      case /^[\+\-][\d\.]*day/.test(word):
        afterFormat = aroundToday(word, 1)
        break
      case /^[\+\-][\d\.]*week/.test(word):
        afterFormat = aroundToday(word, 7)
        break
      default:
        afterFormat = today
        break
    }

    return [true, afterFormat]
  }

  select(params) {
    let defaultValue = {
      checkFresh: true,
    }
    params = Object.assign(defaultValue, params)

    if (!params.key) return [false, "参数key不能为空"]
    let storeValue = localStorage.getItem(this.storeScope + params.key)
    if (!storeValue) return [false, '无相关存储记录']

    let object = JSON.parse(storeValue)
    let createdAt = new Date(object.createdAt.json)
    let res = this.timeFormat(params.storeRange)

    if (!params.checkFresh && object.value) {
      let flag = createdAt > res[1]
      if (flag) return [true, object]
      // 存在本地记录但已过期
      return [true, 504, object]
    }

    if (createdAt > res[1] && object.value) return [true, object]

    // 过期后不做删除 等新值存储时比较哈希码
    // localStorage.removeItem(params.key)
    return [false, "存储记录已过期,请重新获取"]
  }

  setValue(key, value) {
    if (!value) return [false, '存储值不能为空']
    let res = this.select({ key: key, checkFresh: false })
    let changed = true
    let newHashCode
    if (typeof value == 'object'){
      newHashCode = JSON.stringify(value).hashCode()
    }else if (typeof value == 'string'){
      newHashCode = value.hashCode()
    }else{
      newHashCode = value.toString().hashCode()
    }

    if (res[0]) {
      let oldObject = (res[2] || res[1]);
      changed = !(oldObject.hashCode == newHashCode)
    }

    let result = JSON.stringify({
      type: (typeof value),
      createdAt: (this.timeStandard(new Date())),
      hashCode: newHashCode,
      value: value,
    })

    localStorage.setItem(this.storeScope + key, result)
    return [changed, value]
  }

  load(params, callBack) {
    if (!typeof callBack == 'function') return [false, "请传递回调函数"]
    let res

    let defaultValue = {
      key: undefined,
      storeRange: this.storeRange,
    }
    params = Object.assign(defaultValue, params)

    res = this.select(params)
    if (res[0] && res[1] != 504) return callBack(res)

    let setValue = (value, callBack) => {
      let res = this.setValue(params.key, value)
      if (typeof callBack == 'function') callBack(res)
    }
    if (res[0] && res[1] == 504) return callBack([true, 504, {value: res[2].value, func: setValue }])

    return callBack([false, setValue])
  }

  autoLoad(params, packageCallBack, requestCallBack)  {
    let defaultValue = {
      key: undefined,
      storeRange: this.storeRange,
      checkFresh: false
    }
    params = Object.assign(defaultValue, params)

    this.load(params, async function (res) {
      if (res[0] && (res[1] != 504)) return packageCallBack(res[1].value);

      if (res[0] && (res[1] == 504)) {
        packageCallBack(res[2].value)
        var setValueFunc = res[2].func

        return requestCallBack(res_ => {
          if (!res_[0]) return
          setValueFunc(res_[1], (result) => {
            if (result[0]) packageCallBack(result[1])
          })
        })
      }
    
      var setValueFunc = res[1]
      requestCallBack(res => {
        if (!res[0]) return
        setValueFunc(res[1], (result) => {
          if (result[0]) packageCallBack(result[1])
        })
      })

    })

  }

  


}