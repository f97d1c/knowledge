module Finance
  class HomeController < ApplicationController

    def guide
      # 获取所有路由
      # [[dir_path, action_name, rails_path]]
      res = _routes.routes.anchored_routes.uniq.map{|hash| [hash.defaults.values.join(','), hash.name, hash.parts].join(',')}

      # 过滤当前所有路由
      array = ['rails', 'active_storage', 'set_per_page_cookies', 'id', 'create']
      reg = Regexp.new(array.join('|'))
      res.delete_if{|string| reg.match(string)}
      routes = res.map{|string| string.split(',')[0..2]}

      # 根据路由controller分组
      route_infos = Hash.new([])
      routes.each do |array|
        route_infos.merge!({array[0] => ([route_infos[array[0]], {'action_name' => array[1], 'path' => "#{array[2]}_path"}].flatten)})
      end

      # 获取全部映射名称
      route_maps = get_route_maps

      @routes = []
      route_infos.each do |k,array|
        # 映射控制器名称
        controller_name = route_maps['controller_names'][k]
        array_ = []
        array.each do |action_info|
          # 映射action名称
          action_name = (route_maps['action_names'][k][action_info['action_name']] rescue next)
          action_name ||= action_info['action_name'].capitalize
          action_info['action_name'] = action_name
          array_ << action_info
        end
        next unless array_.present?
        @routes << { controller_name => array_}
      end
    end 

    private
      def get_route_maps
        maps = YAML.load(File.open(Rails.root.join('../../config/locales/route_map.yml')))
      end
  end
end

