module Finance
  module FundExtend
    class DateWorth < Finance::ApplicationRecord
      self.table_name = 'finance_fund_date_worths'
      belongs_to :fund, class_name: 'Finance::Fund', foreign_key: 'code', primary_key: 'code'
      get_columns
    end 
  end 
end