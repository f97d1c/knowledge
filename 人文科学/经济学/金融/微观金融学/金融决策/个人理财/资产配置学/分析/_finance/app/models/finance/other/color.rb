module Finance
  module Other
    module Color
      extend self

      def get_word_color(word:, default_color: '#4169E1')
        res = Dictionary.find_entry(word: word)
        return default_color unless res[0]
        res[1].color || default_color
      end

    end 
  end 
end