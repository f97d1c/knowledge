Finance::Engine.routes.draw do

  resources :restful, only:[] do 
    collection do
      get :example
      post :request_api
      get :request_api_params
    end 
  end

  resources :home, only:[] do
    collection do
      get :guide
    end 
  end 

  namespace :deduction do
    resources :funds, only:[] do
      collection do
        get :high_decline_scale_in
      end 
    end 
  end

  namespace :fund_extend do
    resources :analyses, only:[] do
      collection do
        get :overview
        get :base_info
        get :deal_record
      end 
    end 
  end

end
