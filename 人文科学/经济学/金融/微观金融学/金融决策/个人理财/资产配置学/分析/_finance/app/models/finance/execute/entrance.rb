module Finance
  module Execute
    module Entrance
      extend self

      # Finance::Execute::Entrance::run
      def run(**args)
        begin
          # 检查接口服务
          exposed_api = Public::exposed_api
          current_api = exposed_api[args[:api_code].to_sym]
          return [false, "根据api_code(#{args[:api_code]})未找到相关授权方法"] unless current_api

          # 检查传参
          res = BusinessLogic::Inspect::inspect_all(self_described: current_api, reality: args)
          return res unless res[0]

          part_codes = (args[:part_codes]&.map(&:to_sym))
          return [false, '请传递part_codes(数组)参数,以请求具体部分'] if part_codes.nil?
          parts = current_api[:parts].select{|key, part| part_codes.include?(key)}.values

          html_contents = []
          parts.each do |part|
            res = part[:method_path].call(args)
            return res unless res[0]
            res = Wireway::Spark.dashboard(res[1].deep_symbolize_keys)
            return res unless res[0]

            html_contents << res[1]
          end 

          [true, html_contents.join(" <!-- split -->\n")]
        rescue
          info = {errors: {message: $!.to_s, path: $@}}
          print info
          return [false, '请求异常', info]
        end
      end

      private
        def exec_realizations
          @realizations.each do |realization|
            @realization = realization
            res = realization[:method_path].call(@args.deep_symbolize_keys)
            return res if res[0]
          end
          [false, '接口所有具体实现均无法正常处理请求']
        end 

    end 
  end
end