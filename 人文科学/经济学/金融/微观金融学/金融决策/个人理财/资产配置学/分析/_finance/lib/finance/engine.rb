module Finance
  class Engine < ::Rails::Engine
    isolate_namespace Finance

    require 'ancestry'
    require 'paranoia'
    require 'pry-byebug'
    require 'colorize'
    require 'spreadsheet'
    require 'rest-client'
    require 'wireway'
  
  end
end
