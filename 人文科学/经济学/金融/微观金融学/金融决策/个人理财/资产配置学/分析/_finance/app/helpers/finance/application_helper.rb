module Finance
  module ApplicationHelper

    def generate_chart(type:, method_name:, params:, div:{})
      base_request_dashboard(classify: 'chart', type: type, method_name: method_name, params: params, div: div)
    end 

    def generate_list(type: 'base', method_name: 'dark_list', params:, div:{})
      base_request_dashboard(classify: 'list', type: type, method_name: method_name, params: params, div: div)
    end 
    
    def word_process(type:, method_name:, params:, div:{})
      base_request_dashboard(classify: 'publish', type: type, method_name: method_name, params: params, div: div)
    end 

    def load_rely_on(rely_on:)
      rely_on_assets_params = request_assets(rely_on_assets: [rely_on].flatten)
      result = RestClient.post "http://172.28.81.156:3111/dashboard/others/rely_on", rely_on_assets_params
      result.body.html_safe
    end

    def base_request_dashboard(classify:, type:, method_name:, params:, div:{})
      begin
        request_params = {classify: classify, type: type, method_name: method_name}
        result_ = RestClient.post "http://172.28.81.156:3111/dashboard/guide_routes", request_params

        result = JSON.parse(result_.body)
        return [false, result['message']] unless result['success']
        rely_on_assets_params = request_assets(rely_on_assets: result['rely_on_assets'])

        res = RestClient.post "http://172.28.81.156:3111#{result['url']}", params.merge!(rely_on_assets_params)
        return [false, '接口未返回内容'] unless res

        html = %Q|
          <div #{(div.to_a.map{|k, v| "#{k} = '#{v}'"}.join(' ')rescue "error='#{$!.to_s.gsub(/\"|\'|\n|\`|  /, '')}'")}>
            #{res.body}
          </div>
        |.html_safe
        return [true, html]
      rescue
        print "错误说明: #{$!} \n发生位置: #{$@[0..10].join(" \n")}"
        return [false, '运行时异常', {errors: {message: $!, path: $@}}]
      end
    end 

    # 判断是否需要请求依赖样式/js文件
    # 同一请求中对于相同的依赖项只请求一次
    def request_assets(rely_on_assets:)
      tmp = []
      rely_on_assets.each do |rely_on_asset|
        at_var_name = "rely_on_assets_#{rely_on_asset}"
        next if self.instance_variable_get("@#{at_var_name}")
        self.instance_variable_set("@#{at_var_name}", true)
        tmp << rely_on_asset
      end 

      {rely_on_assets: tmp.present? ? tmp : false}
    end 

    # 页面生成导航栏
    # Finance/基金分析/详情页
    def route_map
      # TODO 后期加载慢了可以考虑加缓存 估计也用不到
      maps = YAML.load(File.open(Rails.root.join('../../config/locales/route_map.yml')))

      reg = Regexp.new("\/#{controller_name}$")
      
      full_controller_name, controller_name_zh = maps['controller_names'].select{|controller_name, zh|reg.match(controller_name)}.to_a.flatten

      return [false, "尚未配置当前控制器(#{controller_name})对应控制器名称"] unless current_controller_hash
  
      current_controller_actions = maps['action_names'][full_controller_name]
      return [false, "尚未配置当前控制器(#{controller_name})对应行动列表"] unless current_controller_actions
  
      current_action_name = current_controller_actions[action_name]
      return [false, "尚未配置当前页面(#{controller_name}/#{action_name})对应行动名称"] unless current_action_name
  
      controller_index_link = (send("#{full_controller_name.split('/')[1..-1].join('_')}_path") rescue 'javascript:void(0)')
      controller_index_link = (controller_index_link == (request.fullpath.split('?')[0])) ? 'javascript:void(0)' : controller_index_link
  
      [true, [{name:'Finance', link: '/home/guide'}, {name: current_controller_name, link: controller_index_link}, {name: current_action_name, link: 'javascript:void(0)'}]]
    end


    def format_time(time: )
      return nil unless time

      surplus_seconds = Time.now.to_i - time.to_i
      mark = (surplus_seconds >= 0) ? '前' : '后'
      surplus_seconds = surplus_seconds.abs

      case surplus_seconds
      when 0...60 # 60秒内
        "#{surplus_seconds}秒#{mark}"
      when 60...60*60 # 1小时内
        "#{surplus_seconds / 60}分钟#{mark}"
      when 60*60...60*60*24 # 1天内
        "#{surplus_seconds / (60*60)}小时#{mark}"
      when 60*60*24...60*60*24*7 # 7天内
        "#{surplus_seconds / (60*60*24)}天#{mark}"
      when 60*60*24*7...60*60*24*7*4 # 1个月内
        "#{surplus_seconds / (60*60*24*7)}周#{mark}"
      when 60*60*24*7*4...60*60*24*7*4*12 # 一年内
        "#{surplus_seconds / (60*60*24*7*4)}个月#{mark}"
      else
        time.strftime("%Y年%m月%d日")
      end
    end



  end
end