module Finance
  module Other
    module GetColumns
      def self.included(target)
        target.class_eval do 
          def self.get_columns(only: nil, except: ['id', 'deleted_at', 'created_at', 'updated_at'], base_except_add: [], relations: [])
            only ||= self.column_names - (except + base_except_add)
      
            define_method :get_columns do
              [true, self.attributes.slice(*only)]
            end 
      
            define_method :get_columns_zh do |hash: nil|
              if hash.nil?
                res = self.get_columns
                return res unless res[0]
                hash = res[1]
              end 

              hash.each do |key, value|
                hash.merge!({key => value.to_s}) if value.is_a?(Time)
                hash.merge!({key => value.to_f.round(6)}) if value.is_a?(BigDecimal)
              end 

              [true, Other::Translate.hash_keys_to_chinese(hash: hash)]
            end 

            singleton_class.define_method :get_columns_sql do 
              tmp = {columns: only, sql: only.map{|column| "#{self.table_name}.#{column} as #{column}"}.join(', ')}
              [true, OpenStruct.new(tmp)]
            end 
          
            generate_relation_selects = lambda do |klass:,  | 
              
            end


            relations.map(&:to_sym).each do |relation_|
              model_relation_names = self.reflect_on_all_associations.map(&:name)

              relation = (model_relation_names & [relation_, relation_.to_s.gsub(/_zh$/, '').to_sym])[0]
              raise '关联关系查询失败' if relation.nil?

              relation_info = self.reflect_on_all_associations.select{|ref| ref.name == relation}[0]
              need_zh = !(relation == relation_)
              
              mapping_models = {
                self.to_s.split('::').last.downcase.to_sym => {klass: self, columns: self.get_columns_sql[1].columns},
                relation.to_sym => {klass: relation_info.klass, columns: relation_info.klass.get_columns_sql[1].columns}
              }

              define_method "get_#{relation}" do
                # 有可能_zh是原关联关系一部分 这里可以容错
                # binding.pry
                relation_datas = self.send(relation)
                return [false, '关联数据为空'] unless relation_datas.present?
                
                relation_data = lambda do |data: |
                  res = self.get_columns
                  return res unless res[0]
                  tmp = (data.get_columns[1] rescue data.attributes)
                  [true, res[1].merge!(tmp)]
                end

                # 判断relation_datas是集合还是单一对象
                return relation_data.call(data: relation_datas) if (relation_datas.id rescue false)

                tmp = []
                relation_datas.each do |data|
                  res = relation_data.call(data: data)
                  return res unless res[0]
                  tmp << res[1]
                end
                [true, tmp]      
              end 

              # TODO: 除了单个与主表关联查询 最后把整个relations数组建个方法
              singleton_class.define_method "get_#{relation}" do |sql_lambda: lambda{|base_condiction| base_condiction.call}, select_columns: nil|
                join_tables = lambda{self.joins(relation)}
                
                # TODO: 这种变量可以用一元星号操作符赋值
                select_columns ||= mapping_models.map{|k, v| [k, v[:columns]]}.to_h

                # 处理select条件
                select_tmp = []
                select_columns.each do |k, v|
                  select_tmp << v.map{|column| "#{mapping_models[k][:klass].table_name}.#{column} as #{column}"}.join(', ')
                end 
                select_sql ||= lambda{join_tables.call.select(select_tmp.join(', '))}
      
                # 处理结果数据
                tmp = []
                sql_lambda.call(select_sql).each do |object|
                  hash = {}
                  select_columns.values.flatten.map{|column| hash.merge!({ column => object.send(column)})}
                  tmp << hash
                end 

                [true, tmp.flatten]                
              end 

              next unless need_zh

              define_method "get_#{relation}_zh" do
                res = self.send("get_#{relation}")
                return res unless res[0]

                if res[1].is_a?(Array)
                  tmp = []
                  res[1].each do |hash|
                    res = get_columns_zh(hash: hash)
                    return res unless res[0]
                    tmp << res[1]
                  end
                  [true, tmp]
                else
                  get_columns_zh(hash: res[1])
                end
              end

              singleton_class.define_method "get_#{relation}_zh" do |sql_lambda: lambda{|base_condiction| base_condiction.call}, select_columns: nil|
                res = self.send("get_#{relation}", {sql_lambda: sql_lambda, select_columns: select_columns})
                return res unless res[0]
                res[1].map{|hash| Other::Translate.hash_keys_to_chinese(hash: hash) }
              end 

            end 
      
          end
        end
      end
    end
  end
end