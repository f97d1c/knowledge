# 针对基金情况进行分析
# 分析以基金相关表结构中字段或组合字段为基础进行分析
module Finance
  module FundExtend
    module Analyse
      module BaseInfo
        extend self

        def columns
          res = base_datas
          return res unless res[0]

          base = {
            title: '基础数据列表',
            displays: [
              Finance::Template::Params.dark_list(params: {base_params: Other::Translate.hash_keys_to_chinese(array: res[1])[1]}),
            ]
          }

          [base, fund_type, established_at]
        end 

        private

          def base_datas
            return @base_datas if @base_datas
            @base_datas = Finance::Fund.get_base_info
          end 
        
          # 各类型基金持有数量分析
          def fund_type
            res = base_datas
            return res unless res[0]

            group_funds = res[1].group_by{|hash| hash['fund_type']}
            datas = group_funds.map{|k, v| [k,v.size]}.to_h

            {
              title: '各类型基金持有数量分析',
              displays: [
                Finance::Template::Params.chart_doughnut(params: {base_params: datas.map{|k, v| [k, [['data', v]].to_h]}.to_h}),
                Finance::Template::Params.dark_list(params: {base_params: group_funds.map{|k, v| [['基金类型', k], ['基金名称', v.map{|hash|hash['name']}.join(', ')]].to_h}}, div: {style: "width: 50%; height: 550px; float: left;"}),
              ]            
            }
          end 

          # 基金成立时间分析
          def established_at
            # res = Finance::Fund.get_base_info(select_columns: {fund: ['name', 'code'],base_info: ["established_at"]})
            res = base_datas
            return res unless res[0]
            
            datas = res[1].group_by{|hash| hash['established_at'].gsub(/((\-\d{2}){2}) ((\d{2}\:){2}\d{2})/, '')}.sort{|x,y| x[0] <=> y[0]}

            {
              title: '各基金成立年份分析',
              displays: [
                Finance::Template::Params.chart_doughnut(params: {base_params: datas.map{|k, v| [k, [['data', v.size]].to_h]}.to_h}),
                Finance::Template::Params.dark_list(params: {base_params: datas.map{|k, v| [['成立年份', k], ['基金名称', v.map{|hash|hash['name']}.join(', ')]].to_h}}, div: {style: "width: 50%; height: 550px; float: left;"}),
              ]           
            }
          end 
      end 
    end
  end
end