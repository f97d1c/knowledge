$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "finance/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "finance"
  spec.version     = Finance::VERSION
  spec.authors     = ["ff4c00"]
  spec.email       = ["ff4c00@gmail.com"]
  spec.homepage    = "https://gitlab.com/ff4c00/finance"
  spec.summary     = "用于金融相关分析"
  spec.description = "用于金融相关分析"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 5.2.4", ">= 5.2.4.1"
  # 软删除
  spec.add_dependency "paranoia", "~> 2.4.2"
  # 树结构
  spec.add_dependency "ancestry", "~> 3.0.7"
  # 终端打印字体颜色
  spec.add_dependency "colorize", '0.8.1'
  # 导出excel
  spec.add_dependency "spreadsheet", '~> 1.1', '>= 1.1.4'
  # Http请求
  spec.add_dependency "rest-client", '2.1.0'
  # C919组件组网协作
  spec.add_dependency 'wireway'

  spec.add_development_dependency "sqlite3"
  # 断点调试
  spec.add_development_dependency "pry-byebug", "~> 3.9.0"
end
