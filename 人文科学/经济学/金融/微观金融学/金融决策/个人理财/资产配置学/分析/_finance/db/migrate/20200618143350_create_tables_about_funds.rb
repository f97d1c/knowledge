class CreateTablesAboutFunds < ActiveRecord::Migration[5.2]
  def change
    # 基金主表
    create_table :finance_funds do |t|
      t.string :name, null: false, comment: '基金名称'
      t.index :name, unique: true
      
      t.string :code, null: false, comment: '编码'
      t.index :code, unique: true
      
      t.datetime :deleted_at
      t.index :deleted_at
      
      t.timestamps
    end

    # 基金基本信息
    create_table :finance_fund_base_infos do |t|
      t.string :code, null: false, comment: '编码'
      t.index :code

      t.datetime :established_at, null: false, comment: '成立时间'
      t.index :established_at

      t.string :fund_type, null: false, comment: '基金类型'
      t.index :fund_type

      t.datetime :deleted_at
      t.index :deleted_at
      
      t.timestamps
    end

    # 每日净值
    create_table :finance_fund_date_worths do |t|

      t.string :code, null: false, comment: '编码'
      t.index :code

      t.decimal :unit_worth, null: false, precision: 20, scale: 4, comment: '单位净值'
      t.index :unit_worth

      t.decimal :history_worth, null: false, precision: 20, scale: 4, comment: '历史净值'
      t.index :history_worth
      
      t.decimal :amplitude, null: false, precision: 20, scale: 4, comment: '当日振幅'
      t.index :amplitude

      t.string :record_date, null: false, comment: '记录所属时间'
      t.index :record_date
      
      t.index [:code, :record_date], unique: true

      t.datetime :deleted_at
      t.index :deleted_at
      
      t.timestamps
    end

    # 交易记录
    create_table :finance_fund_deal_records do |t|

      t.string :code, null: false, comment: '编码'
      t.index :code

      t.string :deal_type, null: false, comment: '交易类型 买入/卖出'
      t.index :deal_type

      t.decimal :deal_num, null: false, precision: 20, scale: 4, comment: '交易数量'
      t.index :deal_num

      t.decimal :deal_unit_worth, null: false, precision: 20, scale: 4, comment: '交易单位净值'
      t.index :deal_unit_worth

      t.datetime :deal_at, null: false
      t.index :deal_at

      t.datetime :deleted_at
      t.index :deleted_at
      
      t.timestamps
    end

  end
end
