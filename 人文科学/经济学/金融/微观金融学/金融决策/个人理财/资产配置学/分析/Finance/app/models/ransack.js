class Ransack {
  space = {}
  constructor(params) {
    let defaultValue = {
      conditions: {}, // 查询条件 {姓名_cont: '飞', 性别_eq: '男', 年龄_gt: 23}
      objects: [], // 原始数据集
    }
    params = Object.assign(defaultValue, params)

    for (let [key, value] of Object.entries(params)) {
      this.space[key] = value
    }
  }

  generateCondition(params) {
    let lambda
    switch (params.type) {
      case 'cont':
        lambda = (object) => {return object[params.key].includes(params.value) }
        break;
      case 'not_cont':
        lambda = (object) => {return !(object[params.key].includes(params.value)) }
        break;
      case 'eq':
        lambda = (object) => { return object[params.key] == params.value }
        break;
      case 'not_eq':
        lambda = (object) => { return object[params.key] != params.value }
        break;
      case 'gt':
        lambda = (object) => { return object[params.key] > params.value }
        break;
      case 'lt':
        lambda = (object) => { return object[params.key] < params.value }
        break;
      default:
        lambda = (object) => { return false }
        break;
    }
    return [true, lambda]
  }

  generateConditions(params) {
    let defaultValue = {
      conditions: this.space.conditions,
    }
    params = Object.assign(defaultValue, params)

    let lambdas = []

    for (let [key, value] of Object.entries(params.conditions)) {
      if ((!value) || (value == '')) continue

      let column_type = key.split(/([^_]*)_(not_cont|cont|eq|not_eq|gt|lt)/).filter(item => item != '')
      if (!column_type.length == 2) continue
      let res = this.generateCondition({ key: column_type[0], type: column_type[1], value: value })
      if (res[0]) lambdas.push(res[1])
    }

    return [true, lambdas]
  }

  async result(params, callBack) {
    let defaultValue = {
      conditions: this.space.conditions,
      objects: this.space.objects,
    }
    params = Object.assign(defaultValue, params)
    if (typeof callBack != 'function') return;
    let objects = []
    Object.assign(objects, params.objects)

    let res = this.generateConditions(params.conditions)
    if (!res[0]) callBack(res)

    for await (let object of objects) {
      res[1].forEach(lambda => {
        if (!lambda(object)) {
          let index = objects.indexOf(object)
          delete objects[index]
        }

      })
    }

    let result = objects.filter(object => object != undefined)
    callBack([true, result])
  }
}