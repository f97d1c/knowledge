
<!-- TOC -->

- [音乐分类](#音乐分类)
  - [流行音乐](#流行音乐)
    - [按地区](#按地区)
      - [中华流行音乐(C-pop)](#中华流行音乐c-pop)
        - [粤语流行音乐(Cantopop)](#粤语流行音乐cantopop)
        - [普通话流行音乐(Mandopop)](#普通话流行音乐mandopop)
        - [台语流行音乐(Hokkien pop)](#台语流行音乐hokkien-pop)
        - [中国摇滚(Chinese rock)](#中国摇滚chinese-rock)
      - [加勒比音乐(Caribbean music)](#加勒比音乐caribbean-music)
        - [雷鬼(Reggae)](#雷鬼reggae)
    - [前卫音乐(Avant-garde music)](#前卫音乐avant-garde-music)
    - [蓝调(Blues)](#蓝调blues)
    - [电子音乐(electronic music)](#电子音乐electronic-music)
    - [嘻哈音乐(Hip hop music)](#嘻哈音乐hip-hop-music)
    - [爵士乐(Jazz)](#爵士乐jazz)
    - [节奏布鲁斯(Rhythm and blues)](#节奏布鲁斯rhythm-and-blues)
    - [摇滚乐(Rock music)](#摇滚乐rock-music)
    - [重金属音乐(Heavy metal music)](#重金属音乐heavy-metal-music)
    - [朋克摇滚(Punk rock)](#朋克摇滚punk-rock)
- [参考资料](#参考资料)

<!-- /TOC -->

# 音乐分类

## 流行音乐

> 流行音乐是具有广泛吸引力的音乐,通常会通过音乐行业分发给广大受众.<br>
很少或没有音乐训练的人们可以享受和演奏这些形式和风格.<br>
它与西方古典音乐或印度古典音乐之类的艺术音乐和传统或 *民间* 音乐形成鲜明对比.

### 按地区

#### 中华流行音乐(C-pop)

C-pop(Chinese pop的简称)被定义的音乐风格来自大中华地区的艺术家.<br>
其中包括台湾,新加坡和新加坡等大多数人口使用中文的国家/地区马来西亚.<br>
尽管1990年代初期中国摇滚乐有所不同,但C-pop有时被用作笼统术语,<br>
不仅涵盖中国流行乐,还包括R＆B,民谣,中国摇滚,中国嘻哈和中国环境音乐.

##### 粤语流行音乐(Cantopop)

Cantopop(*粤语流行音乐 * 的缩写)或HK-pop(*香港流行音乐* 的缩写)是一种用标准中文写成并用粤语演唱的流行音乐.<br>
Cantopop也用于指代其生产和消费的文化背景.该类型始于1970年代,并从该年代中期开始与香港流行音乐联系在一起.<br>
然后,Cantopop在1980年代和1990年代达到顶峰,然后在2000年代缓慢下降,并在2010年代略有复苏.<br>
Cantopop* 一词本身是在1974年首次使用 *Cantorock* 后于1978年创造的.<br>
Cantopop的粉丝群达到了最高荣耀,演唱会遍及中国大陆,台湾,新加坡,马来西亚,韩国,日本,特别是涌入了香港电影中的歌曲.

除西方流行音乐外,Cantopop还受到其他国际流派的影响,包括爵士,摇滚,R＆B,迪斯科,电子等.<br>
Cantopop歌曲几乎总是以粤语演唱.<br>
在东南亚国家(例如越南,泰国,新加坡,马来西亚和印度尼西亚)以及韩国,日本,台湾的东亚地区以及中国大陆东南部,<br>
香港的广东和广西等省份中拥有跨国粉丝群(有时是澳门)仍然是该类型中最重要的枢纽.

一些在粤语流行曲业界最显著的例子包括:

 * 徐小凤
 * 许冠杰
 * 罗文
 * 甄妮
 * 林子祥
 * 谭咏麟
 * 张国荣
 * 陈百强
 * 梅艳芳
 * 张学友
 * 刘德华
 * 林忆莲
 * 王菲
 * 黎明
 * 郭富城
 * 郑秀文
 * 陈慧琳
 * 陈奕迅
 * 容祖儿

##### 普通话流行音乐(Mandopop)

Mandopop是普通话流行音乐.<br>
这种音乐流派起源于1930年代上海受爵士乐影响的流行音乐,称为《时代曲》,<br>
后来受到日本enka,香港的Cantopop,台湾的福建流行音乐以及特别是1970年代的校园歌谣民间运动的影响.<br>
在当代,它已经受到K-pop的强烈影响.<br>
*Mandopop* 可以用作描述用普通话表演的流行歌曲的总称.<br>
英语术语是在 *Cantopop* 之后不久于1980年创造的.<br>
*Cantopop* 成为用粤语描述流行歌曲的流行术语,<br>
*Mandopop* 用于描述当时的普通话流行歌曲,<br>
其中一些是同一歌手演唱的Cantopop歌曲的版本,歌词不同,以适应不同的韵律和汉语的语调模式.

##### 台语流行音乐(Hokkien pop)

##### 中国摇滚(Chinese rock)

#### 加勒比音乐(Caribbean music)

> 加勒比音乐流派多种多样.它们都是非洲,欧洲,印度和土著影响的综合,主要是由非洲奴隶的后代以及其他社区的贡献(例如印度加勒比音乐)创造的.<br>
在加勒比海地区以外获得广泛欢迎的一些样式包括:<br>
bachata,merenque,palo,mombo,denbo,baithak gana,bouyon,cadence-lypso,calypso,chutney,chutney-soca,compas,dancehall,jing ping,parang,pichakaree,punta,ragga,reggae,reggaeton,salsa,soca,zouk.<br>
加勒比海地区也与中美洲和南美音乐有关.

加勒比音乐史源于加勒比本身的历史.包括外来者入侵故土,暴力,奴役甚至种族灭绝的因素.

克里斯托弗·哥伦布(Christopher Columbus)1492年登陆后,西班牙宣布拥有整个地区.<br>
这与当地人或西班牙的欧洲邻国的关系都不佳.<br>
几年之内,由西班牙,法国,英国,丹麦和荷兰进行的血腥战斗横渡了加勒比海诸岛.<br>
所有这些斗争(以及从欧洲带来的疾病)使土著部落灭绝,整个文化被消灭了.

因此,加勒比海被殖民为欧洲各个帝国的一部分.<br>
当欧洲人进口非洲奴隶在岛上殖民地种植糖和咖啡时,当地文化进一步受到侵蚀.<br>
在许多情况下,土著文化和土著音乐被非洲带来的文化所取代.

在这一点上,任何普通的加勒比文化都被分裂了.<br>
每个欧洲大国都在各自的岛屿上雕刻出自己的文化.<br>
即使殖民时期结束了,今天也就是拥有的加勒比海-各个岛屿之间的一系列微妙的文化差异.

这种特定于岛屿的文化还传播了加勒比海的音乐.<br>
每个岛屿都有其独特的音乐风格,它们都受到了非洲奴隶带来的音乐的不同程度的启发.<br>
因此,大多数加勒比海音乐,无论其自身的岛屿文化如何独特,都包含非洲音乐的元素-大量使用打击乐器,复杂的节奏模式和呼唤响应人声.<br>
就是说,重要的是要认识到每个岛屿独特的音乐风格.<br>
在许多情况下,一种风格与另一种风格之间的差异归结于每种音乐所采用的节奏;每个岛屿都有几乎不同的节奏.

##### 雷鬼(Reggae)

雷鬼是一种音乐流派,起源于牙买加在60年代末期.<br>
该术语还表示牙买加及其散居国外的现代流行音乐.<br>
由Toots and Maytals于1968年发行的单曲《Do Reggay》是第一首使用 *reggae* 一词的流行歌曲,<br>
有效地命名了该流派并将其介绍给全球观众.<br>
虽然有时广义上用来指代大多数类型的流行牙买加舞蹈音乐,<br>
但雷鬼一词更恰当地表示受传统门托音乐以及美国爵士,节奏和蓝调强烈影响的一种特定音乐风格,<br>
尤其是Fats Domino和Allen Toussaint所实践的,源自早期流派ska和rocksteady的新奥尔良R＆B .<br>
雷鬼通常与新闻,社交八卦和政治评论相关.<br>
雷鬼舞进入了商业化的爵士乐领域,首先被称为 *鲁迪布鲁斯*,然后被称为 *ska*,后来被称为*blue beat*和*rock steady* .<br>
从低音和鼓的低拍和非节奏的节奏部分之间的对立点可以立即识别出它.
从风格上讲,雷鬼音乐融合了节奏和布鲁斯,爵士乐,门托的音乐元素(一种庆祝性的乡村民俗形式,<br>
在很大程度上为农村听众提供舞蹈音乐,并替代了当地教堂演唱的赞美诗和改编的圣歌),calypso,还从传统的非洲民间韵律中汲取影响.<br>
节拍节奏是最容易识别的元素之一.<br>
吉他或钢琴(或两者)在小节拍下弹奏的断奏和弦.<br>
雷鬼摇摆乐的节奏通常比ska和rock稳定.<br>
呼叫和响应的概念可以在雷鬼音乐中找到.<br>
雷鬼音乐的类型由鼓,低音吉他和贝斯主导.

### 前卫音乐(Avant-garde music)

> 前卫音乐是被认为在其领域中处于创新前沿的音乐,<br>
*前卫*一词表示对现有美学惯例的批评,<br>
对现状的拒绝是对独特或原始元素的支持,以及故意挑战或疏远观众的想法.<br>
前卫音乐可以通过在某种传统中占据极端地位而与实验音乐区分开,而实验音乐则不在传统中.

### 蓝调(Blues)

布鲁斯(Blues)是一种音乐流派和一种音乐形式,<br>
它是由非裔美国人起源于1860年代在美国的深南地区,源于非洲音乐传统,非裔美国人的工作歌曲和精神修养.<br>
布鲁斯融入了精神,工作歌曲,田野咆哮,喊叫,吟唱和押韵的简单叙事歌谣.<br>
布鲁斯形式,在爵士乐,节奏和布鲁斯以及摇滚乐中无处不在,其特征在于呼叫响应模式,蓝调音阶和特定的和弦进行,其中最常见的是十二格蓝调.

蓝调作为一种流派,其特征还在于其歌词,低音线条和乐器.<br>
早期的传统布鲁斯诗歌由重复的四行组成.<br>
直到20世纪前几十年,最常见的当前结构才成为标准:AAB模式,包括在前四个小节上歌唱的线条,<br>
在接下来的四个小节上重复的线条,以及随后的更长的结尾线条最后的酒吧.<br>
早期的忧郁症常常表现为散漫的叙事形式,通常与种族歧视和非裔美国人所经历的其他挑战相关.

许多元素,例如呼叫响应格式和使用蓝调音符,都可以追溯到非洲音乐.<br>
布鲁斯音乐的起源也与美国黑人社区的宗教音乐,精神音乐密切相关.<br>
布鲁斯的初次出现通常可追溯到奴隶制结束之后,后来发展为柔道关节.<br>
它与前奴隶新获得的自由有关.<br>
编年史家开始报道20世纪初的布鲁斯音乐.<br>
布鲁斯活页乐谱的第一本出版物于1908年发行.<br>
布鲁斯从无伴奏的声乐和奴隶的口述传统发展成为各种各样的风格和子流派.<br>
布鲁斯子流派包括乡村布鲁斯(如Delta布鲁斯和皮埃蒙特布鲁斯),以及城市布鲁斯风格(如芝加哥布鲁斯和西海岸布鲁斯).<br>
第二次世界大战标志着从声学布鲁斯到电子布鲁斯的过渡,以及布鲁斯音乐逐渐向更广泛的听众开放,尤其是白人听众.<br>
在1960年代和1970年代,一种叫做布鲁斯摇滚的混合形式得以发展,它将布鲁斯风格与摇滚音乐融合在一起.

### 电子音乐(electronic music)

电子音乐是音乐,它采用电子乐器,数字仪表或基于电路-音乐技术.<br>
可以将使用机电手段产生的声音(电声音乐)与仅使用电子设备产生的声音区分开.<br>
机电仪器具有机械元件,例如弦,锤子和电气元件,例如磁拾音器,功率放大器和扬声器.<br>
机电发声设备的例子包括:电颤音,哈蒙德风琴,电子钢琴和电吉他,通常它们的声音足够大,表演者和听众可以通过乐器放大器和扬声器箱听到.<br>
纯电子乐器没有振动弦,锤子或其他发声机构.<br>
设备,如特雷门,合成器和计算机能产生电声.

最早的用于演奏音乐的电子设备是在19世纪末开发的,不久之后,意大利未来主义者就探索了未被视为音乐的声音.<br>
在1920年代和1930年代,引入了电子乐器,并制造了第一批用于电子乐器的合成物.<br>
到1940年代,磁性录音带使音乐家可以录音,然后通过改变录音带的速度或方向来对其进行修改,从而在1940年代在埃及和法国发展了电声录音带音乐.<br>
音乐博物馆于1948年在巴黎创立,其基础是将录制的自然和工业声音片段进行编辑.<br>
仅由电子发电机产生的音乐最早于1953年在德国生产.<br>
电子音乐也在1950年代在日本和美国创作.<br>
一个重要的新发展是计算机音乐创作的出现.

### 嘻哈音乐(Hip hop music)

嘻哈音乐(又称说唱音乐)是1970年代纽约市布朗克斯区的市内非洲裔美国人和拉丁美洲人在美国开发的一种流行音乐.<br>
它由通常伴随说唱的风格化节奏音乐,被高喊的节奏性和韵律性语音组成.<br>
它开发作为其一部分嘻哈文化中,传代培养通过四个关键风格元素定义:MCing /振打,打碟/刮伤与转盘,霹雳舞,涂鸦写作.<br>
其它元件包括采样节拍或低音线从记录(或合成的节拍和声音),和节奏口技.<br>
虽然经常被用来仅是说唱,*嘻哈*是指整个亚文化的实践.<br>
术语嘻哈音乐有时与术语同义使用说唱乐,尽管说唱并不是嘻哈音乐的必要组成部分;<br>
该类型也可能包含嘻哈文化的其他元素,包括DJ,弹唱,划伤,节拍和器乐曲目.

嘻哈既是一种音乐类型,又是一种文化,它是在1970年代形成的,当时在纽约市,特别是在居住在布朗克斯区的非洲裔美国年轻人中,集体聚会日益盛行.<br>
在集体聚会上,DJ使用两个转盘和DJ混音器播放流行歌曲的打击乐,以播放同一唱片的两个副本的乐曲,从一个到另一个交替播放,并扩展了 *break* .<br>
嘻哈音乐的早期发展发生在采样技术和鼓机变得广泛可用和负担得起的时候.<br>
伴随着休息和牙买加音乐发展了诸如刮擦和节奏匹配之类的转盘手技术节拍中使用了敬酒,一种高呼的人声风格.<br>
敲击发展成为一个唱腔其中艺术家说话或沿节奏的圣歌与乐器或合成节拍.

直到1979年,嘻哈音乐才正式在广播或电视上录制下来,这主要是由于该流派的诞生期间的贫困和贫民区附近人们的不接受.<br>
老派嘻哈音乐是该流派的第一个主流浪潮,以其迪斯科影响力和面向派对的歌词为标志.<br>
1980年代标志着嘻哈的多元化,因为该类型发展出更复杂的风格并在世界范围内传播.<br>
新派嘻哈音乐是该类型的第二波音乐,以其电声为特征,并进入了黄金时代的嘻哈音乐,这是1980年代中期至1990年代中期的创新时期.<br>
该帮派说唱体裁专注于城市中心非裔美国青年的暴力生活方式和贫困状况,这时已广受欢迎.<br>
在1990年代中期早期,西海岸的嘻哈音乐由G-funk主导,而东海岸的嘻哈音乐则由爵士说唱乐,另类嘻哈音乐和铁杆说唱音乐主导.<br>
随着其他区域风格的出现,例如南部说唱和亚特兰大嘻哈音乐,嘻哈音乐在这个时期继续多元化.<br>
嘻哈音乐在1990年代中期成为最畅销的音乐类型,到1999年成为最畅销的音乐类型.

嘻哈音乐的流行一直持续到1990年代末至2000年代中期,嘻哈影响力越来越多地进入其他流行音乐类型,例如neo soul,nu metal和R＆B.<br>
美国也看到了地方风格的成功,例如杂种音乐,一种南方风格的音乐,其强调节拍和音乐而不是歌词,另类嘻哈音乐也开始在主流音乐中占据一席之地,部分原因是其跨界音乐的成功艺术家.<br>
在2000年代末和2010年代初的 *博客时代* ,说唱歌手能够通过在线音乐分发方法(例如社交媒体和博客)建立追随者,以及黑帮说唱在商业上的衰落之后,主流嘻哈音乐的旋律更加敏感.<br>
该陷阱和咕哝说唱其他风格已成为在2010年代中后期和21世纪20年代早期街舞的最普遍的形式.<br>
2017年,摇滚音乐被嘻哈音乐所取代,成为美国最受欢迎的音乐类型.
### 爵士乐(Jazz)

爵士是一种音乐流派起源于,美国路易斯安那州新奥尔良非洲裔社区,在19世纪末和20世纪初,其根布鲁斯和拉格泰姆.<br>
自1920年代的爵士时代以来,它被认为是传统音乐和流行音乐中一种主要的音乐表达形式,<br>
并通过非洲裔美国人和欧美音乐人的共同纽带联系在一起.<br>
爵士乐的特点是挥杆和蓝调,复杂的和弦,呼唤和响应人声,节奏和即兴演奏.<br>
爵士乐起源于西非的文化和音乐表现形式以及非裔美国人的音乐传统.

随着爵士乐在世界范围内的传播,它汲取了国家,地区和当地的音乐文化,从而产生了不同的风格.<br>
新奥尔良爵士乐始于20世纪10年代初,早期的铜管乐队游行,法国组合四对,biguine,拉格泰姆和蓝调集体和弦即兴.<br>
在1930年代,以舞蹈为导向的大型摇摆乐队,堪萨斯城爵士乐,<br>
摇摆不定的蓝调即兴演奏风格和吉普赛爵士乐(强调小号华尔兹舞的风格)是突出风格.<br>
比波普爵士乐在1940年代出现,将爵士乐从可跳舞的流行音乐转变为更具挑战性的 *音乐家的音乐* ,以更快的节奏演奏,并使用了更多基于和弦的即兴演奏.<br>
酷爵士乐在1940年代末期发展起来,带来了更沉稳,更流畅的声音以及长长的线性旋律线条.

1950年代中期,出现了硬跳,这引起了节奏和布鲁斯,福音和布鲁斯的影响,特别是在萨克斯管和钢琴演奏中.<br>
莫代尔爵士在50年代后期开发的,采用的模式,或音阶,因为音乐结构和即兴的基础一样,自由爵士,其中探讨打不经常米,节奏和正式结构.<br>
爵士摇滚融合出现在1960年代末和1970年代初,结合了爵士即兴演奏与摇滚音乐的节奏,电子乐器和高度放大的舞台声音.<br>
在1980年代初期,一种叫做 *平滑爵士* 的商业爵士融合形式取得了成功,获得了重要的广播节目.<br>
其他风格和流派在2000年代比比皆是,例如拉丁和非洲裔古巴爵士乐.
### 节奏布鲁斯(Rhythm and blues)

节奏布鲁斯(通常被缩写为R＆B)是一种流行音乐,其起源于1940年代的非洲裔美国人社区.<br>
这个词最初是由唱片公司用来描述主要销售给城市非裔美国人的唱片,而此时 *都市音乐,摇滚音乐,爵士音乐带有沉重而持久的节奏* 正变得越来越流行.<br>
在1950年代到1970年代典型的商业节奏和布鲁斯音乐中,乐队通常由钢琴,一或两把吉他,贝斯,鼓,一或多个萨克斯风组成,有时还包括背景歌手.<br>
R＆B抒情主题通常包含非裔美国人的痛苦经历,对自由与快乐的追求以及在关系,经济和抱负方面的胜利与失败.

术语 *Rhythm and blues* 在含义上发生了许多变化.<br>
在1950年代初,它经常被用于布鲁斯唱片.<br>
从1950年代中期开始,这种音乐风格促进了摇滚乐的发展,*R＆B* 一词被用来指代源自和融合了电布鲁斯音乐,福音和灵魂音乐的音乐风格.<br>
音乐.<br>
在1960年代,一些英国摇滚乐队,例如滚石乐队,《谁和动物》被称为并被推荐为R＆B乐队.<br>
谁在跑马灯俱乐部居住的海报1964年的标语是 *Maximum R＆B* .<br>
摇滚和节奏布鲁斯的混合现在被称为 *英国节奏布鲁斯* .<br>
到1970年代末,*Rhythm and blues*一词再次改变,并被用作灵魂与放克的总称.<br>
在1980年代后期,出现了一种更新的R＆B风格,被称为 *当代R＆B* .

### 摇滚乐(Rock music)

摇滚音乐是一类广泛的流行音乐,其起源于1940年代末和1950年代初的美国 *摇滚乐* ,在1960年代中期及以后发展成一系列不同的风格,尤其是在美国和英国.<br>
它的根源于1940年代和1950年代的摇滚,这种风格在很大程度上源于布鲁斯,节奏和布鲁斯的流派以及乡村音乐.<br>
摇滚音乐也很受其他类型音乐的影响,例如电布鲁斯和民谣,并吸收了爵士乐的影响,古典和其他音乐风格.<br>
为了进行乐器演奏,摇滚音乐一直以电吉他为中心,通常是摇滚乐队的一部分,包括电贝司,鼓和一个或多个歌手.<br>
像流行音乐一样,歌词经常强调浪漫的爱情,但也涉及到其他经常与社会或政治相关的主题.

到1960年代后期的 *古典摇滚* 时期,出现了许多独特的摇滚音乐子流派,包括布鲁斯摇滚,民间摇滚,乡村摇滚,南部摇滚,拉加摇滚和爵士摇滚等杂种音乐,其中许多迷幻摇滚的发展,受到了反文化迷幻和嬉皮场景的影响.<br>
出现的新类型包括渐进摇滚,它扩展了艺术元素,华丽摇滚,强调了表演技巧和视觉风格,以及重金属的多样持久的流派,强调了体积,功率和速度.<br>
在1970年代下半叶,朋克摇滚做出了反应,产生了精简,充满活力的社会和政治评论.<br>
朋克音乐在1980年代影响了新浪潮,后朋克音乐,并最终影响了另类摇滚.<br>
从1990年代开始,另类摇滚开始统治摇滚音乐,并以摇滚,Britpop和独立摇滚的形式进入主流.<br>
此后出现了更多的融合子流派,包括流行朋克,电子摇滚,说唱摇滚,说唱金属以及有意识的尝试重温摇滚乐的历史,包括2000年代初期的车库摇滚/朋克音乐和流行音乐的复兴.<br>
在2000年代末和2010年代末期,摇滚音乐的主流流行度和文化相关性逐渐下降,嘻哈音乐已超过它成为美国最受欢迎的音乐流派.

摇滚音乐还体现并充当了文化和社会运动的工具,导致了主要的亚文化,包括英国的mod和摇滚乐手,以及在1960年代从美国旧金山传播的嬉皮反文化.<br>
同样,1970年代的朋克文化催生了哥特,朋克和emo亚文化.<br>
摇滚音乐继承了抗议歌曲的民俗传统,与政治活动主义以及种族,性别和毒品使用的社会态度的变化相关联,通常被视为青年人对成人消费主义和顺从性的反抗的表达.

### 重金属音乐(Heavy metal music)

重金属(或简称为金属)是摇滚音乐的一种流派,它在1960年代末和1970年代初发展,主要在英国和美国发展.<br>
根源于布鲁斯摇滚,迷幻摇滚和酸性摇滚,重金属乐队散发出浓重而厚重的声音,其特点是失真,吉他独奏,重音和响度.<br>
歌词和表演有时与侵略和大男子主义有关.

1968年,创立了该类型中最著名的三位先驱Led Zeppelin,Black Sabbath和Deep Purple.<br>
尽管它们吸引了广泛的听众,但它们常常遭到评论家的嘲笑.<br>
1970年代,几支美国乐队将重金属改性为更易接近的形式:爱丽丝·库珀(Alice Cooper)和基斯(Kiss)原始的声音和震撼摇滚;Aerosmith的蓝调摇滚乐;还有华丽的吉他指挥和Van Halen狂野的派对摇滚.<br>
在1970年代中期,Judas Priest抛弃了大部分蓝调,从而刺激了音乐流派的发展.<br>
影响力 ,而Motörhead则介绍了朋克摇滚的敏感性,并越来越重视速度.<br>
从1970年代后期开始,新一波的英国重金属乐队如Iron Maiden和Saxon随之而来.

### 朋克摇滚(Punk rock)

朋克摇滚(或简称为朋克)是一种音乐流派,出现于1970年代中期.<br>
朋克乐队起源于1960年代的车库摇滚,拒绝了1970年代主流摇滚的过度感觉.<br>
通常制作短而快节奏的歌曲,这些歌曲具有曲折的边缘和唱歌风格,精简的乐器以及通常带有政治色彩的反传统歌词.<br>
朋克奉行DIY主义; 许多乐队会自己制作唱片,并通过独立的唱片公司进行发行.

美国摇滚评论家在1970年代初首次使用 *朋克摇滚* 一词来形容1960年代的车库乐队.<br>
当该运动的名称从1974年发展到1976年时,诸如电视,帕蒂·史密斯和纽约市的拉蒙斯等人开始行动.<br>
伦敦的Sex Pistols,Clash和Damned;洛杉矶的逃亡者;和圣徒在布里斯班形成自己的先锋队.<br>
朋克在1976年末成为英国的主要文化现象.<br>
它导致朋克亚文化通过独特的服装和装饰风格表现出年轻的叛逆<br>
(例如故意攻击性的T恤,皮夹克,饰有钉子或钉子的带子和珠宝,安全别针,束缚和S＆M衣服)和各种反威权主义的意识形态.

1977年,音乐和亚文化的影响力席卷全球,尤其是在英格兰.<br>
它扎根于经常拒绝与主流派系的广泛的当地场景.<br>
1970年代后期,朋克经历了第二波热潮,因为在其成年时期未活跃的新行为采用了这种风格.<br>
到1980年代初期,速度更快,更具侵略性的子类型,如硬核朋克(例如次要威胁),街头朋克(例如Exploited)和无政府主义朋克(例如Crass)成为了朋克摇滚的主要模式.<br>
认同朋克音乐或受其启发的音乐家也追求其他音乐方向,从而产生了诸如后朋克,新浪潮,以及后来的独立流行音乐,另类摇滚和杂音摇滚.<br>
到1990年代,随着朋克摇滚和流行朋克乐队(例如Green Day,Rancid,The Offspring和Blink-182)的成功,朋克重新成为主流.

# 参考资料

> [维基百科&nbsp;|&nbsp;Listofpopular music genres](https://en.wikipedia.org/wiki/List_of_popular_music_genres)

> [维基百科&nbsp;|&nbsp;C-pop](https://en.wikipedia.org/wiki/C-pop)

> [维基百科&nbsp;|&nbsp;Cantopop](https://en.wikipedia.org/wiki/Cantopop)

> [维基百科&nbsp;|&nbsp;Mandopop](https://en.wikipedia.org/wiki/Mandopop)

> [维基百科&nbsp;|&nbsp;Chineserock](https://en.wikipedia.org/wiki/Chinese_rock)

> [维基百科&nbsp;|&nbsp;Avant-garde music](https://en.wikipedia.org/wiki/Avant-garde_music)

> [维基百科&nbsp;|&nbsp;Blues](https://en.wikipedia.org/wiki/Blues)

> [维基百科&nbsp;|&nbsp;Chutney music](https://en.wikipedia.org/wiki/Chutney_music)

> [维基百科&nbsp;|&nbsp;Reggae](https://en.wikipedia.org/wiki/Reggae)

> [维基百科&nbsp;|&nbsp; Electronic music](https://en.wikipedia.org/wiki/Electronic_music)

> [维基百科&nbsp;|&nbsp;Hip hop music](https://en.wikipedia.org/wiki/Hip_hop_music)

> [维基百科&nbsp;|&nbsp;Jazz](https://en.wikipedia.org/wiki/Jazz)

> [维基百科&nbsp;|&nbsp;Rhythm and blues](https://en.wikipedia.org/wiki/Rhythm_and_blues)

> [维基百科&nbsp;|&nbsp;Rock music](https://en.wikipedia.org/wiki/Rock_music)

> [维基百科&nbsp;|&nbsp;Heavy metal music](https://en.wikipedia.org/wiki/Heavy_metal_music)

> [维基百科&nbsp;|&nbsp;Punk rock](https://en.wikipedia.org/wiki/Punk_rock)

> [维基百科&nbsp;|&nbsp;]()
