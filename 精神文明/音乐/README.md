# 歌单解析

> 将各平台歌单解析为统一的JSON格式.

格式示例:

```json
[
  {
    "plat_id": "26289183",
    "name": "Hotel California (2013 Remaster)",
    "author": "Eagles",
    "poster_url": "http://p1.music.126.net/ESU2My24GG1JjuztMq-7Gw==/109951163940111977.jpg",
    "platform": "163"
  },
  {
    "plat_id": "63612",
    "name": "假行僧",
    "author": "崔健",
    "poster_url": "http://p2.music.126.net/tQgClDZoPP85veIDmY-cOg==/109951163067343573.jpg",
    "platform": "163"
  }
]
```

## 网易云音乐

参数|含义
-|-
table_content_|歌单列表的table标签内容
file_save_path|解析内容存储路径

```ruby
table_content = Nokogiri::HTML.parse(table_content_)
original_datas = table_content.xpath('//span[starts-with(@data-res-id, "")]').map do |x| 
  {
    plat_id: x['data-res-id'], 
    name: x['data-res-name'], 
    author: x['data-res-author'], 
    poster_url: x['data-res-pic'], 
    platform: '163'
  }
end.delete_if{|hash| hash.values.include?(nil)}
File.open(file_save_path, 'w').write(original_datas.to_json)
```