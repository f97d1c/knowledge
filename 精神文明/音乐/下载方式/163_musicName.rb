keywords = ["03/06-Ólafur Arnalds","20/17-Ólafur Arnalds","21/05-Ólafur Arnalds","0048/0729-Ólafur Arnalds","0952-Ólafur Arnalds","1440-Ólafur Arnalds","1995-Ólafur Arnalds,Dagný Arnalds","3055-Ólafur Arnalds","Ágúst-Ólafur Arnalds","Arcade-Ólafur Arnalds","Dalur-Ólafur Arnalds,Brasstríó Mosfellsdals","Day II-Ólafur Arnalds","Day VII-Ólafur Arnalds","For Teda-Ólafur Arnalds","Fyrsta-Ólafur Arnalds","Lag Fyrir Ömmu-Ólafur Arnalds","Life Story-Ólafur Arnalds,Nils Frahm","Love and Glory-Ólafur Arnalds,Nils Frahm","Romance-Ólafur Arnalds","This Place Is a Shelter-Ólafur Arnalds","Tunglið-Ólafur Arnalds"]


file = File.open('tmp/163_infos.txt', 'w')

keywords.each do |keyword|
  url = "https://autumnfish.cn/search"
  full_url = [url, URI.encode_www_form({keywords: keyword})]
  full_url.delete_if{|item| !item.present? }
  res = RestClient.get(full_url.join("?"))
  res_json = JSON.parse(res.body)
  main_song = (OpenStruct.new(res_json['result']['songs'].first) rescue nil)

  if main_song
    file_name = %Q|{name => "#{main_song.name}", tags => [], type => "music", others => {artist => "#{main_song.artists.map{|hash| hash['name']}.join(',')}", id => "#{main_song.id}", source => "163"}, version => 0.0.2}|

    file.write("-----#{keyword}\n")
    file.write("https://music.163.com/#/song?id=#{main_song.id}\n")
    file.write("#{file_name}\n")
    file.write("\n\n")
  end
end

file.close
