# 根据网易云音乐id下载对应音乐
music_ids = ['16139397','16139381','36199955','16139395','16139392','36198994','16139396','16139391','36308043','574935892','35253666','16139406','16139399','16139394','470793902','27933791','36199958','16139404','16139380','1807771','1807649','16139405','16139401','16139398','16139378','426303221','426303224','36307035','16139377','16139379','426303219','1807757','16139402','1807751','27701677','426303222','472219768','26217148','16139376','16139403','16139382','35253667','36198997','16139393','1807765','31053110','1481845416','16139400','16139426','1493084347']
@download_ids = []
allowed_failures_number = 5
runtime_errors = []

def get_music_info_163(music_id:, timeout: 10, open_timeout: 10)
  headers = {
    'authority': 'music.liuzhijin.cn',
    'method': 'POST',
    'path': '/',
    'scheme': 'https',
    'accept': 'application/json, text/javascript, */*; q=0.01',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'zh-CN,zh;q=0.9,en;q=0.8',
    'content-length': '44',
    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'cookie': 'Hm_lvt_50027a9c88cdde04a70f5272a88a10fa=1607508642,1607913045,1607954820,1608169920; Hm_lpvt_50027a9c88cdde04a70f5272a88a10fa=1608200006',
    'origin': 'https://music.liuzhijin.cn',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
    'x-requested-with': 'XMLHttpRequest',
  }
  url = 'https://music.liuzhijin.cn'
  params = {input: music_id, filter: 'id', type: 'netease', page: '1'}
  
  print "加载音乐(#{music_id})详细信息...\n"
  res = RestClient::Request.execute(method: :post,url: url, payload: params, headers: headers, timeout: timeout, open_timeout: open_timeout)
  response = JSON.parse(res.body)
  return [false, response['error']] if response['code'] == 404

  info = OpenStruct.new(response['data'][0])
  [true, info]
end

def download_music(music_id:, timeout: 10, open_timeout: 10)
  res = get_music_info_163(music_id: music_id)
  return res unless res[0]
  info = res[1]

  music_name = %Q|{name => "#{info.title}", tags => [], type => "music", others => {artist => "#{info.author}", id => "#{music_id}", source => "163"}, version => 0.0.2}|
  music_name = music_name.gsub(/\//, "／").gsub(/\:/, "：")

  print "加载音乐(#{info.title}-#{music_id})原始文件...\n"
  response = RestClient::Request.execute(method: :get, url: info.url, timeout: timeout, open_timeout: open_timeout)

  File.open("tmp/163/#{music_name}.mp3", 'wb'){|file| file.write(response.body)}
  @download_ids << music_id

  [true, music_name]
end

(music_ids - @download_ids).each do |music_id|
  begin
    sleep 20
    res = download_music(music_id: music_id)
    runtime_errors << res unless res[0]
  rescue
    allowed_failures_number -= 1
    print "运行时异常: #{$!}\n"
    runtime_errors << {name: $!, info: $@}
    unless allowed_failures_number >= 0
      return [false, '运行时异常次数超过阈值', runtime_errors]
    end
  end
  
  print "#{res[1]} 下载完成, ETA: #{@download_ids.size}/#{music_ids.size}\n" if res
  print "准备进行再次下载\n\n\n"
end

[true, '下载完成']