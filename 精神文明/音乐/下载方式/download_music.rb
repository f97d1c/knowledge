#!/usr/bin/env ruby
require 'optparse'
require 'ostruct'
require 'json'
require 'rest-client'
# require 'pry'
# require 'pry-rescue'
# require 'pry-byebug'

options = {
  platform_codes: ['163'],
  platform_code: '163', # 默认音乐平台代码
  music_ids: nil, # 需下载音乐标识集合
  download_ids: [], # 已下载完成id
  disappear_ids: [], # 没有资源的id
  save_path: nil, # 文件保存路径
  interval_time: 20, # 间隔时间(秒)
  runtime_errors: [], # 运行时异常
  failures_number: 5, # 默认失败重新尝试次数
  timeout: 10, # 默认超时时间(秒)
  file_tags: [], # 默认超时时间(秒)
}

OptionParser.new do |opts|
  opts.banner = '该脚本用于从网易云音乐等网站下载音乐'

  opts.on('--platform_code 平台代码', "音乐网站代码, 可用参数值有: #{options[:platform_codes].join(', ')}, 默认为: #{options[:platform_code]}") do |value|
    unless options[:platform_codes].include?(value)
      p [false, "所提供平台代码(platform_code): #{value}, 未在允许范围内: #{options[:platform_codes].join(', ')}"]
      exit 110
    end
    options[:platform_code] = value
  end

  opts.on('--music_ids 音乐id1,音乐id2', Array, '平台音乐标识,使用英文逗号分隔') do |value|
    options[:music_ids] = value
  end

  opts.on('--save_path 保存路径', '保存音乐的绝对路径') do |value|
    options[:save_path] = value
  end

  opts.on('--interval_time 间隔时间', "批量下载中每项之间的间隔时间, 默认为:#{options[:interval_time]}秒.") do |value|
    options[:interval_time] = value
  end

  opts.on('--failures_number 尝试次数', "失败后重新尝试次数, 默认为:#{options[:failures_number]}次.") do |value|
    options[:failures_number] = value
  end

  opts.on('--timeout 超时时间', "请求超时时间, 默认为:#{options[:timeout]}秒.") do |value|
    options[:timeout] = value
  end

  opts.on('--file_tags 标签1,标签2', Array, '为文件添加标签, 使用英文逗号分隔') do |value|
    options[:file_tags] = value
  end

end.parse!

if options[:music_ids].nil?
  p [false, "音乐标识(music_ids) 不能为空"]
  exit 404
end

if options[:save_path].nil?
  p [false, "保存路径(save_path) 不能为空"]
  exit 404
end

unless Dir.exist?(options[:save_path])
  p [false, "保存路径(save_path) 不存在"]
  exit 404
end

@options = OpenStruct.new(options)

def surplus_ids
  @options.music_ids - @options.download_ids - @options.disappear_ids
end

def done?
  surplus_ids.size == 0
end

def get_music_info_163(music_id:, timeout: @options.timeout, open_timeout: @options.timeout)
  headers = {
    'authority': 'music.liuzhijin.cn',
    'method': 'POST',
    'path': '/',
    'scheme': 'https',
    'accept': 'application/json, text/javascript, */*; q=0.01',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'zh-CN,zh;q=0.9,en;q=0.8',
    'content-length': '44',
    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'cookie': 'Hm_lvt_50027a9c88cdde04a70f5272a88a10fa=1607508642,1607913045,1607954820,1608169920; Hm_lpvt_50027a9c88cdde04a70f5272a88a10fa=1608200006',
    'origin': 'https://music.liuzhijin.cn',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
    'x-requested-with': 'XMLHttpRequest',
  }
  url = 'https://music.liuzhijin.cn'
  params = {input: music_id, filter: 'id', type: 'netease', page: '1'}
  
  print "加载音乐(#{music_id})详细信息...\n"
  res = RestClient::Request.execute(method: :post,url: url, payload: params, headers: headers, timeout: timeout, open_timeout: open_timeout)
  response = JSON.parse(res.body)
  return [false, response['error']] if response['code'] == 404

  info = OpenStruct.new(response['data'][0])
  [true, info]
end

def download_music(music_id:, timeout: @options.timeout, open_timeout: @options.timeout)
  # res = get_music_info_163(music_id: music_id)
  res = send("get_music_info_#{@options.platform_code}", {music_id: music_id})
  return res unless res[0]
  info = res[1]

  music_name = %Q|{name => "#{info.title}", tags => #{@options.file_tags}, type => "music", others => {artist => "#{info.author}", id => "#{music_id}", source => "#{@options.platform_code}"}, version => 0.0.2}|
  music_name = music_name.gsub(/\//, "／").gsub(/\:/, "：")

  short_name = "#{info.title}-#{music_id}"
  print "加载音乐(#{short_name})原始文件...\n"
  response = RestClient::Request.execute(method: :get, url: info.url, timeout: timeout, open_timeout: open_timeout)

  unless response.body.encoding == Encoding::ASCII_8BIT
    @options.disappear_ids << music_id
    return [false, "#{short_name}返回资源非二进制内容(资源不存在)"]
  end

  File.open("#{@options.save_path}/#{music_name}.mp3", 'wb'){|file| file.write(response.body)}
  @options.download_ids << music_id

  [true, "#{music_name} 下载完成"]
end

def request
  print "下载结束, 结果为: #{@options.download_ids.size}/#{@options.music_ids.size}\n"
  print "未完成下载music_ids: #{(@options.music_ids-@options.download_ids).join(', ')}\n"
  print "运行时发生异常有: \n#{@options.runtime_errors.join("\n")}\n" unless (@options.runtime_errors.size == 0)
  print "请选择是否继续执行未完成任务:"
  case gets().gsub("\n", '')
  when 'y', 'Y', 'YES', 'Yes'
    exec_()
  end 
end

def each_download
  surplus_ids.each do |music_id|
    res = download_music(music_id: music_id)
    @options.runtime_errors << res[1..-1] unless res[0]
    print "#{res[1]}, ETA: #{@options.download_ids.size}/#{@options.disappear_ids.size}/#{@options.music_ids.size}\n" if res
    
    unless done?
      print "准备进行再次下载\n\n\n"
      sleep @options.interval_time 
    end
  end
end

def exec_
  begin
    each_download()
  rescue
    @options.failures_number -= 1
    print "运行时异常: #{$!}\n"
    @options.runtime_errors << {name: $!, info: $@}
    unless @options.failures_number >= 0
      p [false, '运行时异常次数超过阈值', @options.runtime_errors]
      exit 110
    end
    each_download()
  ensure
    request() unless done?
  end
end

exec_()

print "运行时发生异常有: \n#{@options.runtime_errors.join("\n")}\n" unless (@options.runtime_errors.size == 0)
print "没有资源的音乐有: #{@options.disappear_ids.join(", ")}\n" unless (@options.disappear_ids.size == 0)

if done?
  p [true, '全部下载完成']
else
  p [false, "执行结束, 但存在未下载完成音乐: #{surplus_ids.join(', ')}"]
end
exit 0