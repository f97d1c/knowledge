
<!-- TOC -->

- [说明](#说明)
  - [安装](#安装)
    - [Ubuntu](#ubuntu)
- [应用](#应用)
  - [根据创建时间重命名(-filename\<CreateDate)](#根据创建时间重命名-filenamecreatedate)
  - [根据创建时间分类(-Directory\<CreateDate)](#根据创建时间分类-directorycreatedate)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

> ExifTool是Phil Harvey以Perl写成的自由开源软件,<br>
> 可读写及处理图像、视频及音频的metadata,<br>
> 例如Exif、IPTC、XMP、JFIF、GeoTIFF、ICC Profile.<br>
> 它是跨平台的,可作为命令列或Perl函式库使用.

## 安装

### Ubuntu

```sh
sudo apt install -y libimage-exiftool-perl
```

# 应用

## 根据创建时间重命名(-filename<CreateDate)

```sh
exiftool '-filename<CreateDate' -d %Y%m%d-%H%M%S%%-03.c.%%le -r 目标路径
```

* -filename<CreateDate 使用图像的创建数据和时间重命名原始图像
* -d设置日期/时间值的格式
*  %%-03.c 相同日期格式的三位顺序编码(唯一性措施, 防止文件名重复)
* .%%e保留原文件扩展名, .%%le（以小写形式重命名扩展名）或.%%ue（以大写形式重命名）
* -r是在源文件夹上递归执行命令

## 根据创建时间分类(-Directory<CreateDate)

```sh
exiftool -o '-Directory<CreateDate' -d ./目标文件夹/%Y/%m/%d -r ./源文件夹
```

* -o制作所有图像的副本（以下称为旧图像）并从中制作新图像
* -Directory<CreateDate 使用图像创建日期将图像移动到新的目标文件夹
* -r是在源文件夹上递归执行命令

# 参考资料

> [geeksforgeeks | Installing and Using Exiftool on Linux](https://www.geeksforgeeks.org/installing-and-using-exiftool-on-linux/)